@echo OFF


set YEAR=%date:~0,4%
set MONTH=%date:~5,2%
set DAY=%date:~8,2%
set DATE=%YEAR%%MONTH%%DAY%
set TEMPTIME=%time: =0%
set HOUR=%TEMPTIME:~0,2%
set MINUTE=%TEMPTIME:~3,2%
set SECONDS=%TEMPTIME:~6,2%
set TIME=%HOUR%%MINUTE%%SECONDS%
set DATETIME=%DATE%_%TIME%

set DIR_HOME=%cd%
set TARGET=%~n0.hex
set NRFJPROG=%DIR_HOME%\TMP\nrfjprog.exe



::Usage:
::-------------------------------------------------------------------------------
::
:: -q  --quiet                 Reduces the stdout info. Must be combined with
::                             another command.
::
:: -h  --help                  Displays this help.
::
:: -v  --version               Displays the nrfjprog and dll versions.
::
:: -i  --ids                   Displays the serial numbers of all the debuggers
::                             connected to the PC.
::
:: -f  --family <family>       Selects the device family for the operation. Valid        <--------------
::                             argument options are NRF51 and NRF52. If --family
::                             option is not given, the default is taken from
::                             nrfjprog.ini. Must be combined with another
::                             command.
::
:: -s  --snr <serial_number>   Selects the debugger with the given serial number
::                             among all those connected to the PC for the
::                             operation. Must be combined with another command.
::
:: -c  --clockspeed <speed>    Sets the debugger SWD clock speed in kHz
::                             resolution for the operation. The valid clockspeed
::                             arguments go from 125 kHz to 50000 kHz. If given
::                             clockspeed is above the maxiumum clockspeed
::                             supported by the emulator, its maximum will be
::                             used instead. If --clockspeed option is not given,
::                             the default is taken from nrfjprog.ini. Must be
::                             combined with another command.
::
::     --recover               Erases all user flash and disables the read back        <--------------
::                             protection mechanism if enabled.
::
::     --rbp <level>           Enables the readback protection mechanism. Valid        <--------------
::                             argument options are CR0 and ALL.
::                             Limitations:
::                             For nRF52 devices, the CR0 argument option is
::                             invalid.
::                             Side effects:
::                             After an --rbp operation is performed, the
::                             available operations are reduced.
::                             For nRF51 devices, and if argument option ALL is
::                             used, --pinreset will not work on certain older
::                             devices.
::                             For nRF52 devices, only --pinreset, --debugreset
::                             or --recover operations are available after --rbp.
::
::     --pinresetenable        For nRF51 devices, command is invalid.
::                             For nRF52 devices, pin reset will be enabled.
::
:: -p  --pinreset              Performs a pin reset. Core will run after the
::                             operation.
::
:: -r  --reset                 Performs a soft reset by setting the SysResetReq        <--------------
::                             bit of the AIRCR register of the core. The core
::                             will run after the operation. Can be combined with
::                             the --program operation. If combined with the
::                             --program operation, the reset will occur after
::                             the flashing has occurred to start execution.
::
:: -d  --debugreset            Performs a soft reset by the use of the CTRL-AP.
::                             The core will run after the operation. Can be
::                             combined with the --program operation. If combined
::                             with the --program operation, the debug reset will
::                             occur after the flashing has occurred to start
::                             execution.
::                             Limitations:
::                             For nRF51 devices, the --debugreset operation is
::                             not available.
::                             For nRF52_FP1_EngA devices, the --debugreset
::                             operation is not available.
::
:: -e  --eraseall              Erases all user available program flash memory and <--------------
::                             the UICR page.
::                             Limitations:
::                             For nRF51 devices, if the device came from Nordic
::                             with a pre-programmed SoftDevice, only the user
::                             available code flash and UICR will be erased.
::
::     --eraseuicr             Erases the UICR page.
::                             Limitations:
::                             For nRF51 devices, the operation is available only
::                             if the device came from Nordic with a
::                             pre-programmed SoftDevice.
::
::     --erasepage <start[-end]>                      Erases the flash pages
::                             starting at start address and ending at end
::                             address (not included in the erase). If end
::                             address is not given, only one flash page will be
::                             erased.
::                             Limitations:
::                             For nRF51 devices, the page will not be erased if
::                             it belongs to region 0.
::
::     --program <hex_file> [--sectorerase | --chiperase | --sectoranduicrerase]        <--------------
::                             Programs the specified hex file into the device.
::                             If the target area to program is not erased, the
::                             --program operation will fail, unless an erase
::                             option is given. Valid erase operations are
::                             --sectorerase, --sectoranduicrerase and
::                             --chiperase. If --chiperase is given, all the        <--------------
::                             available user flash (including UICR) will be
::                             erased before programming. If --sectorerase is
::                             given, the target sectors (excluding UICR) will be
::                             erased. If --sectoranduicrerase is given, the
::                             target sectors (including UICR) will be erased.
::                             Note that the --sectoranduicrerase and
::                             --sectorerase operations normally take
::                             significantly longer time compared to --chiperase,
::                             so use them with caution. Can be combined with the
::                             --verify operation. Can be combined with either        <--------------
::                             the --reset or the --debugreset operations. The
::                             reset will occur after the flashing operation to
::                             start execution.
::                             Limitations:
::                             For nRF51 devices, the --sectoranduicrerase
::                             operation is not available.
::                             For nRF51 devices, if the hex_file provided
::                             contains sectors belonging to region 0, a
::                             --sectorerase operation will fail.
::
::     --memwr <addr> --val <val>                     Writes to memory with the
::                             help of the NVM Controller to the provided
::                             address. If the target address is flash and not
::                             erased, the operation will fail. Can be combined
::                             with the --verify operation.
::
::     --ramwr <addr> --val <val>                     Writes to memory without
::                             the help of the NVM Controller to the provided
::                             address. Can be combined with the --verify
::                             operation.
::
::     --verify [<hex_file>]   The provided hex_file contents are compared with
::                             the contents in the device code flash, RAM and
::                             UICR, and fail if there is a mismatch. It can be
::                             combined with the --program, --memwr and --ramwr
::                             operations if provided without the hex_file
::                             parameter.
::
::     --memrd <addr> [--w <width>] [--n <n>]         Reads n bytes from the
::                             provided address. If width is not given, 32-bit
::                             words will be read if addr is word aligned,
::                             16-bit words if addr is half word aligned, and
::                             8-bit words otherwise. If n is not given, one
::                             word of size width will be read. The address and
::                             n must be aligned to the width parameter. The
::                             maximum number of bytes that can be read is 1 MB.
::                             The width w must be 8, 16 or 32.
::
::     --halt                  Halts the CPU core.
::
::     --run [--pc <pc_addr> --sp <sp_addr>]          Starts the CPU. If --pc and
::                             --sp options are given, the pc_addr and sp_addr
::                             are used as initial PC and stack pointer. For
::                             pc_addr to be valid its last bit must be one. For
::                             sp_addr to be valid it must be word aligned.
::
::     --readuicr <path>       Reads the device UICR and stores it in the given
::                             file path. Can be combined with --readcode and
::                             --readram. If combined, only one instruction can
::                             provide a path.
::
::     --readcode <path>       Reads the device flash and stores it in the given
::                             file path. Can be combined with --readuicr and
::                             --readram. If combined, only one instruction can
::                             provide a path.
::
::     --readram <path>        Reads the device ram and stores it in the given
::                             file path. Can be combined with --readuicr and
::                             --readcode. If combined, only one instruction can
::                             provide a path.

::     --readregs              Reads the cpu registers.







::--program <hex_file> [--sectorerase | --chiperase | --sectoranduicrerase]
::nrfjprog -f NRF52 --program file.hex �-chiperase
::https://devzone.nordicsemi.com/f/nordic-q-a/10135/how-to-program-softdevice-and-firmware-with-new-nrfjprog



::@echo ON
::nrfjprog --rbp ALL -f NRF52
::@echo OFF


goto LOOPSTART







:LOOPSTART
cls

echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
:::::==================================================================
:::::
::::: 
echo.
echo ==================================================================
echo [1.1.1]: recover
:::::==================================================================
@echo ON
%NRFJPROG% --recover --family NRF52
@echo OFF

echo.
echo condition : %ERRORLEVEL%
echo.
if "%ERRORLEVEL%"=="0" (
  echo 1.OK
) else (
  echo 1.NG
  goto ERROROCCUR
)






echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
:::::==================================================================
:::::
::::: 
echo.
echo ==================================================================
echo [1.1.2]: program, verify, chiperase
:::::==================================================================
@echo ON
%NRFJPROG% --program %TARGET% --verify --chiperase --family NRF52
@echo OFF

echo.
echo condition : %ERRORLEVEL%
echo.
if "%ERRORLEVEL%"=="0" (
  echo 2.OK
) else (
  echo 2.NG
  goto ERROROCCUR
)





echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
:::::==================================================================
:::::
::::: 
echo.
echo ==================================================================
echo [1.1.3]: reset
:::::==================================================================
@echo ON
%NRFJPROG% --reset --family NRF52
@echo OFF

echo.
echo condition : %ERRORLEVEL%
echo.
if "%ERRORLEVEL%"=="0" (
  echo 3.OK
) else (
  echo 3.NG
  goto ERROROCCUR
)





echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.

                                            
echo      OOOOOOOOO          KKKKKKKKK    KKKKKKK
echo    OO:::::::::OO        K:::::::K    K:::::K
echo  OO:::::::::::::OO      K:::::::K    K:::::K
echo O:::::::OOO:::::::O     K:::::::K   K::::::K
echo O::::::O   O::::::O     KK::::::K  K:::::KKK
echo O:::::O     O:::::O       K:::::K K:::::K   
echo O:::::O     O:::::O       K::::::K:::::K    
echo O:::::O     O:::::O       K:::::::::::K     
echo O:::::O     O:::::O       K:::::::::::K     
echo O:::::O     O:::::O       K::::::K:::::K    
echo O:::::O     O:::::O       K:::::K K:::::K   
echo O::::::O   O::::::O     KK::::::K  K:::::KKK
echo O:::::::OOO:::::::O     K:::::::K   K::::::K
echo  OO:::::::::::::OO      K:::::::K    K:::::K
echo    OO:::::::::OO        K:::::::K    K:::::K
echo      OOOOOOOOO          KKKKKKKKK    KKKKKKK
                                            
                                            
:::::==================================================================
:::::
::::: 
echo.
echo ==================================================================
echo ========================^[PROCESS DONE^]==========================
echo ==================================================================
echo.

set /p VAR_KEY_NAME=Press Enter To Continue... 
goto LOOPSTART










:ERROROCCUR

                       
                                                  
echo NNNNNNNN        NNNNNNNN             GGGGGGGGGGGGG
echo N:::::::N       N::::::N          GGG::::::::::::G
echo N::::::::N      N::::::N        GG:::::::::::::::G
echo N:::::::::N     N::::::N       G:::::GGGGGGGG::::G
echo N::::::::::N    N::::::N      G:::::G       GGGGGG
echo N:::::::::::N   N::::::N     G:::::G              
echo N:::::::N::::N  N::::::N     G:::::G              
echo N::::::N N::::N N::::::N     G:::::G    GGGGGGGGGG
echo N::::::N  N::::N:::::::N     G:::::G    G::::::::G
echo N::::::N   N:::::::::::N     G:::::G    GGGGG::::G
echo N::::::N    N::::::::::N     G:::::G        G::::G
echo N::::::N     N:::::::::N      G:::::G       G::::G
echo N::::::N      N::::::::N       G:::::GGGGGGGG::::G
echo N::::::N       N:::::::N        GG:::::::::::::::G
echo N::::::N        N::::::N          GGG::::::GGG:::G
echo NNNNNNNN         NNNNNNN             GGGGGG   GGGG

                                            

set /p VAR_KEY_NAME=Press Enter To Continue... 
timeout /t 4
goto LOOPSTART
                                            



