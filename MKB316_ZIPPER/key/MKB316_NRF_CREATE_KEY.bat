::C:\Keil_v5\UV4

::MKB316
::    MKB316_HEX
::        20190529_223230_key_2
::    MKB316_NRF
::        components
::        examples
::    MKB316_STM
::    MKB316_PRD
::    MKB316_ZIPPER
::         key
::             key_1
::             key_2
::         data
::             nrfutil.exe
::             nrf52832_s132_bootloader_project
::                 components
::                 examples


@echo OFF
::MKB316_ZIPPER
set DIR_HOME=%cd%
cd ..
set DIR_PARENT=%cd%
cd %DIR_HOME%


set YEAR=%date:~0,4%
set MONTH=%date:~5,2%
set DAY=%date:~8,2%
set DATE=%YEAR%%MONTH%%DAY%
set TEMPTIME=%time: =0%
set HOUR=%TEMPTIME:~0,2%
set MINUTE=%TEMPTIME:~3,2%
set SECONDS=%TEMPTIME:~6,2%
set TIME=%HOUR%%MINUTE%%SECONDS%
set DATETIME=%DATE%_%TIME%




::================================================================
echo.
echo.
echo.
echo =======================^[WARNING^]===========================
echo THE KEY IS UNIQUE VALUE. ONCE RELEASED, DO NOT CHANGE ANYMORE. 
echo WANT TO CONTINUE?
set /p YN=(y/n)
if /i "%YN%" == "y" goto YN_CONTINUE

goto YN_EXIT




:YN_CONTINUE
:::::==================================================================
:::::
::::: ??? ??
echo.
echo ==================================================================
echo ^[NRF KEY:Step 0^]: Write the new key^'s version ^(KEY_%DATE%_VER_XX^).
echo.
echo ---------------key list----------------
dir %DIR_HOME% /b/on
echo ---------------------------------------
echo.
set /p KEY_VERSION=XX : 
set VAR_KEY_NAME=KEY_%KEY_VERSION%

if exist %DIR_HOME%\%VAR_KEY_NAME% (
  echo never write the already existed name
  echo exit program..
  goto YN_EXIT
) else (
  goto YN_CONTINUE2
)

:YN_CONTINUE2
::================================================================
set DIR_KEYDEST=%DIR_HOME%\%VAR_KEY_NAME%
set FILE_KEYDEST=%VAR_KEY_NAME%.pem
echo.





::================================================================
echo.
echo ==============================================================
echo ^[NRF KEY:Step 1^]: create key.pen

cd %DIR_HOME%
mkdir %DIR_KEYDEST%
cd %DIR_KEYDEST%
%DIR_PARENT%\data\nrfutil.exe keys generate .\%FILE_KEYDEST%
echo.





::================================================================
echo.
echo ==============================================================
echo ^[NRF KEY:Step 3^]: create txt

cd %DIR_KEYDEST%
echo ============================================================== >readme.txt
echo MOKIBO DFU KEY FILE  >>readme.txt
echo ============================================================== >>readme.txt
echo -------------------------------------------------------- >>readme.txt
echo created time: %YEAR% / %MONTH% / %DAY%  %HOUR% : %MINUTE% : %SECONDS% >>readme.txt
echo key version: %VAR_KEY_NAME% >>readme.txt
echo -------------------------------------------------------- >>readme.txt
echo.



::================================================================
echo.
echo ==============================================================
echo ^[NRF KEY:Step 4^]: back up the keys to the dropbox, NAS

set DROPBOX=C:\Users\pingo\Dropbox\Innopresso_src\20190530_MKB_zipper\MKB316_NRF_keyhistory\%VAR_KEY_NAME%
set NAS=\\192.168.1.2\Public\2.Projects\1.Mokibo\2.Firmware\MKB316_NRF_keyhistory\%VAR_KEY_NAME%
::set NAS=ftp://203.233.111.124/Public/2.Projects/1.Mokibo/2.Firmware/MKB316_NRF_keyhistory/%DATETIME%_%VAR_KEY_NAME%
mkdir %DROPBOX%
mkdir %NAS%

copy %DIR_KEYDEST%\%VAR_KEY_NAME%.pem %DROPBOX%
copy %DIR_KEYDEST%\readme.txt %DROPBOX%

copy %DIR_KEYDEST%\%VAR_KEY_NAME%.pem %NAS%
copy %DIR_KEYDEST%\readme.txt %NAS%



::================================================================
:YN_EXIT
echo.
echo ======================^[PROCESS DONE^]========================
@pause


