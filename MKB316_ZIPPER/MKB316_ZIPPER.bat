@echo OFF
@chcp 65001 1> NUL 2> NUL
::한글 테스트

cd ..
set DIR_HOME=%cd%



set YEAR=%date:~0,4%
set MONTH=%date:~5,2%
set DAY=%date:~8,2%
set DATE=%YEAR%%MONTH%%DAY%
set TEMPTIME=%time: =0%
set HOUR=%TEMPTIME:~0,2%
set MINUTE=%TEMPTIME:~3,2%
set SECONDS=%TEMPTIME:~6,2%
set TIME=%HOUR%%MINUTE%%SECONDS%
set DATETIME=%DATE%_%TIME%



::C:\Keil_v5\UV4

::MKB316
::    MKB316_HEX
::        20190529_223230_key_2
::    MKB316_NRF
::        components
::        examples
::    MKB316_NRF_BTLD
::    MKB316_PRD
::    MKB316_STM
::    MKB316_ZIPPER
::         key
::             key_1
::             key_2
::         data
::             nrfutil.exe



::1.1.1 [PEM]
::1.1.2 [PEM]
::1.1.3 [KEY]
::1.1.4 [KEY]

::1.2.1 create[BL]
::1.2.2 copy[BL]

::1.3.1 create[APP]
::1.3.2 copy[APP]

::1.4.1 create[BLS]

::1.5.1 merge [APP_BL_BLS]
::1.5.2 merge [APP_BL_BLS_SDV]
::1.5.2 merge [APP_SDV]

::1.6.1 create[ZIP]

::1.7.1 move results

::2.1.1 STM

::3.1.1 PRD

::4.1.1 readme















timeout /t 1
echo.
echo.
:::::==================================================================
:::::
::::: 1.0.0 [PEM] 
echo.
echo ==================================================================
echo [1.0.0]: select the key version number.
echo 1.DoubleClick the name 
echo 2.Ctrl+C
echo 3.Ctrl+V
echo 4.Enter
echo.
echo ---------------key list----------------
dir %DIR_HOME%\MKB316_ZIPPER\key /b/on
echo ---------------------------------------
echo.
set /p VAR_KEY_NAME=SELECT : 




:::::==================================================================
:::::
::::: 1.0.1 [PEM] ì¡´ìž¬í•˜ëŠ”ì§€ í™•ì�¸
echo.
echo ==================================================================
echo [1.0.1]: check [PEM] file exist
:::::==================================================================
if exist %DIR_HOME%\MKB316_ZIPPER\key\%VAR_KEY_NAME%\%VAR_KEY_NAME%.pem (
  goto YN_CONTINUE
) else (
  echo there is no such key file. exit.
  goto YN_EXIT
)









:YN_CONTINUE

set APP_VERSION=1
set BL_VERSION=1
set BLS_VERSION=1
set ZIP_VERSION=1
::ê²°ê³¼í�´ë�” ë§Œë“¬
set VERSION_NAME=VER_%APP_VERSION%_%BL_VERSION%_%BLS_VERSION%

set DIR_RESULTS=%DIR_HOME%\MKB316_HEX\%DATETIME%_MKB316_RESULTS_%VERSION_NAME%_%VAR_KEY_NAME%
mkdir %DIR_RESULTS%

::ìž‘ì—…ìš© ìž„ì‹œ ë””ë ‰í† ë¦¬ ë§Œë“¬
set DIR_TMP=%DIR_RESULTS%\TMP
mkdir %DIR_TMP%

set BLS_APP_BL=%DIR_TMP%\%DATETIME%_BLS_APP_BL_%VERSION_NAME%_%VAR_KEY_NAME%
set SDV_BLS_APP_BL=%DIR_RESULTS%\%DATETIME%_SDV_BLS_APP_BL_%VERSION_NAME%_%VAR_KEY_NAME%


::ìž„ì‹œ ë””ë ‰í† ë¦¬ì—� flahser ë³µì‚¬. í”Œëž˜ì…”ì�˜ ëª©ì � íŒŒì�¼ ì�´ë¦„ì�´ ê³§ íƒ€ê²Ÿì�˜ ì�´ë¦„ì�´ ë�œë‹¤. ì�¼ë‹¨ temp ì—� ë„£ì–´ë†“ê³  mergeë‹¨ê³„ì—�ì„œ ë³µì‚¬í•œë‹¤.
set DIR_SRC_FLASHER=%DIR_TMP%
set OBJ_SRC_FLASHER=MKB316_NRF_FLAHSER.bat
copy %DIR_HOME%\MKB316_ZIPPER\data\MKB316_NRF_FLAHSER.bat %DIR_SRC_FLASHER%\%OBJ_SRC_FLASHER%

::ìž„ì‹œ ë””ë ‰í† ë¦¬ì—� mergehex ë³µì‚¬
set DIR_SRC_MERGEHEX=%DIR_TMP%
set OBJ_SRC_MERGEHEX=mergehex.exe
copy %DIR_HOME%\MKB316_ZIPPER\data\mergehex.exe %DIR_SRC_MERGEHEX%\%OBJ_SRC_MERGEHEX%

::ìž„ì‹œ ë””ë ‰í† ë¦¬ì—� nrfutil ë³µì‚¬
set DIR_SRC_NRFUTIL=%DIR_TMP%
set OBJ_SRC_NRFUTIL=nrfutil.exe
copy %DIR_HOME%\MKB316_ZIPPER\data\nrfutil.exe %DIR_SRC_NRFUTIL%\%OBJ_SRC_NRFUTIL%

::ìž„ì‹œ ë””ë ‰í† ë¦¬ì—� nrfjprog ë³µì‚¬
set DIR_SRC_JPROG=%DIR_TMP%
set OBJ_SRC_JPROG=nrfjprog.exe
copy %DIR_HOME%\MKB316_ZIPPER\data\nrfjprog.exe %DIR_SRC_JPROG%\%OBJ_SRC_JPROG%
copy %DIR_HOME%\MKB316_ZIPPER\data\nrfjprog.dll %DIR_SRC_JPROG%\nrfjprog.dll
copy %DIR_HOME%\MKB316_ZIPPER\data\nrfjprog.ini %DIR_SRC_JPROG%\nrfjprog.ini
copy %DIR_HOME%\MKB316_ZIPPER\data\jlinkarm_nrf52_nrfjprog.dll %DIR_SRC_JPROG%\jlinkarm_nrf52_nrfjprog.dll

::ìž„ì‹œ ë””ë ‰í† ë¦¬ì—� SDV ë³µì‚¬
set DIR_SRC_SDV=%DIR_TMP%
::set OBJ_SRC_SDV=s132_nrf52_5.0.0_softdevice.hex
set OBJ_SRC_SDV=SDV.hex
copy %DIR_HOME%\MKB316_ZIPPER\data\s132_nrf52_5.0.0_softdevice.hex %DIR_SRC_SDV%\%OBJ_SRC_SDV%

::í‚¤ íŒŒì�¼,readme.txt ìž„ì‹œë””ë ‰í† ë¦¬ë¡œ ë³µì‚¬
set DIR_SRC_PEM=%DIR_HOME%\MKB316_ZIPPER\key\%VAR_KEY_NAME%
set OBJ_SRC_PEM=%VAR_KEY_NAME%.pem
set DIR_DST_PEM=%DIR_TMP%
set OBJ_DST_PEM=%VAR_KEY_NAME%.pem
copy %DIR_SRC_PEM%\%OBJ_SRC_PEM% %DIR_DST_PEM%\%OBJ_SRC_PEM%
copy %DIR_SRC_PEM%\readme.txt %DIR_DST_PEM%\readme.txt

::ì™¸ë¶€ë””ë ‰í„°ë¦¬
set DIR_SRC_BL_PRJ=%DIR_HOME%\MKB316_NRF_BTLD\examples\dfu\bootloader_secure_ble\pca10040_debug\arm5_no_packs
set DIR_SRC_APP_PRJ=%DIR_HOME%\MKB316_NRF\examples\ble_peripheral\ble_app_hids_keyboard\pca10040\s132\arm5_no_packs
set DIR_KEIL5=C:\Keil_v5\UV4

::[KEY]
set DIR_SRC_KEY=%DIR_TMP%
set OBJ_SRC_KEY=dfu_public_key.c
set DIR_DST_KEY=%DIR_HOME%\MKB316_NRF_BTLD\examples\dfu\dfu_req_handling
set OBJ_DST_KEY=dfu_public_key.c









timeout /t 1
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.

cd %DIR_DST_PEM%
:::::==================================================================
:::::
::::: 1.1.3 [KEY] ìƒ�ì„±

echo ==================================================================
echo [1.1.1]: create [KEY] dfu_public_key.c
:::::==================================================================
@echo ON

%DIR_SRC_NRFUTIL%\%OBJ_SRC_NRFUTIL% keys display --key pk --format code .\%OBJ_DST_PEM% --out_file %DIR_SRC_KEY%\%OBJ_SRC_KEY%

@echo OFF
:::::==================================================================
:::::
::::: 1.1.4 [KEY] ë¶€íŠ¸ë¡œë�”í”„ë¡œì �íŠ¸ì—� ë¶™ì—¬ë„£ê¸°
echo.
echo ==================================================================
echo [1.1.2]: copy [KEY] to bootloader prj
:::::==================================================================
@echo ON
copy %DIR_SRC_KEY%\%OBJ_SRC_KEY% %DIR_DST_KEY%\%OBJ_DST_KEY%
@echo OFF


















timeout /t 1
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
:::::==================================================================
:::::
::::: 1.2.1 BOOTLOADER ë¹Œë“œí•´ì„œ [BL]ìƒ�ì„±
echo.
echo ==================================================================
echo [1.2.1]: create [BL]
:::::==================================================================
echo bootloader build start...
cd %DIR_KEIL5%
@echo ON
::UV4 -cr %DIR_SRC_BL_PRJ%\secure_dfu_ble_s132_pca10040_debug.uvprojx -t"nrf52832_xxaa_s132"
UV4 -cr %DIR_SRC_BL_PRJ%\secure_dfu_ble_s132_pca10040_debug.uvprojx -t"nrf52832_xxaa_s132"
@echo OFF
timeout /t 1


:::::==================================================================
:::::
::::: 1.2.2 [BL]ë³µì‚¬í•´ì„œ TMPë¡œ
echo.
echo ==================================================================
echo [1.2.2]: copy [BL]
:::::==================================================================
@echo ON
copy %DIR_SRC_BL_PRJ%\_build\nrf52832_xxaa_s132.hex %DIR_TMP%\BL.hex
@echo OFF



















timeout /t 1
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
:::::==================================================================
:::::
::::: 1.3.1 [APP] ë¹Œë“œ
echo.
echo ==================================================================
echo [1.3.1]: Build [APP]
:::::==================================================================
echo application build start...
cd %DIR_KEIL5%
@echo ON
::UV4 -cr %DIR_SRC_APP_PRJ%\ble_app_hids_keyboard_pca10040_s132.uvprojx -t"nrf52832_xxaa"
UV4 -cr %DIR_SRC_APP_PRJ%\ble_app_hids_keyboard_pca10040_s132.uvprojx -t"nrf52832_xxaa"
@echo OFF
timeout /t 1

:::::==================================================================
:::::
::::: 1.3.2 [APP] ë³µì‚¬
echo.
echo ==================================================================
echo [1.3.2]: copy [APP]
:::::==================================================================
cd %DIR_HOME%
@echo ON
copy %DIR_SRC_APP_PRJ%\_build\nrf52832_xxaa.hex %DIR_TMP%\APP.hex
@echo OFF


















timeout /t 1
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
:::::==================================================================
:::::
::::: 1.4.1 [BLS] ìƒ�ì„±
echo.
echo ==================================================================
echo [1.4.1]: create [BLS] from  application
:::::==================================================================
@echo ON
%DIR_SRC_NRFUTIL%\%OBJ_SRC_NRFUTIL% settings generate --family NRF52 --application %DIR_TMP%\APP.hex --application-version %APP_VERSION% --bootloader-version %BL_VERSION% --bl-settings-version %BLS_VERSION% %DIR_TMP%\BLS.hex
@echo OFF

















timeout /t 1
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
:::::==================================================================
:::::
::::: 1.5.1 BOOTLOADER _ MKB316 _ BOOTLOADER_SETTING = MKB316_NRF_app.hex
echo.
echo ==================================================================
echo [1.5.1]: [BL] + [APP] + [BLS]  =  [APP+BL+BLS]
:::::==================================================================

@echo ON
%DIR_SRC_MERGEHEX%\%OBJ_SRC_MERGEHEX% -m %DIR_TMP%\BLS.hex %DIR_TMP%\APP.hex %DIR_TMP%\BL.hex -o %BLS_APP_BL%.hex
copy %DIR_SRC_FLASHER%\%OBJ_SRC_FLASHER% %BLS_APP_BL%.bat

%DIR_SRC_MERGEHEX%\%OBJ_SRC_MERGEHEX% -m %DIR_SRC_SDV%\%OBJ_SRC_SDV% %BLS_APP_BL%.hex -o %SDV_BLS_APP_BL%.hex
copy %DIR_SRC_FLASHER%\%OBJ_SRC_FLASHER% %SDV_BLS_APP_BL%.bat

@echo OFF

echo status: %ERRORLEVEL%







timeout /t 1
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
:::::==================================================================
:::::
::::: 1.6.1 generate zip
echo.
echo ==================================================================
echo [1.6.1]: turn to [ZIP]
:::::==================================================================
set ZIP=%DIR_RESULTS%\%DATETIME%_APP_%VERSION_NAME%_%VAR_KEY_NAME%

@echo ON
%DIR_SRC_NRFUTIL%\%OBJ_SRC_NRFUTIL% pkg generate --hw-version 52 --sd-req 0x9D --application-version %ZIP_VERSION% --application %DIR_TMP%\APP.hex --key-file %DIR_DST_PEM%\%OBJ_DST_PEM% %ZIP%.zip
@echo OFF

echo status: %ERRORLEVEL%












timeout /t 1
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
:::::==================================================================
::::: 
::::: STM
echo.
echo ==================================================================
echo [2.1.1]: move STM to destination folder
:::::==================================================================
@echo ON
copy %DIR_HOME%\MKB316_STM\Source\Release\mokiob_prj.s19 %DIR_RESULTS%\MKB316_STM_app.s19
@echo OFF












timeout /t 1
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
:::::==================================================================
:::::
::::: PRD
echo.
echo ==================================================================
echo [3.1.1]: parade
:::::==================================================================
@echo ON
copy %DIR_HOME%\MKB316_PRD\Source\Release\result.hex %DIR_RESULTS%\MKB316_PRD_app.hex
@echo OFF















timeout /t 1
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
:::::==================================================================
:::::
::::: readme
echo.
echo ==================================================================
echo ^[4.1.1]: create readme.txt
cd %DIR_RESULTS%

@echo ON
echo ============================================================== >readme.txt
echo MOKIBO HEX FILE PACKAGE >>readme.txt
echo ============================================================== >>readme.txt
echo -------------------------------------------------------- >>readme.txt
echo created time: %YEAR% / %MONTH% / %DAY%  %HOUR% : %MINUTE% : %SECONDS% >>readme.txt
echo key version: %VAR_KEY_NAME% >>readme.txt
echo -------------------------------------------------------- >>readme.txt
echo. >>readme.txt
echo flashing softdevice: s132_nrf52_5.0.0_softdevice.hex   >>readme.txt
echo flashing only application: MKB316_NRF_app.hex  >>readme.txt
echo flashing app with bootloader: MKB316_NRF_package_app.hex >>readme.txt
echo flashing app for OTA update: MKB316_NRF_package.zip >>readme.txt
@echo OFF
echo.













timeout /t 1
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
::================================================================
:YN_EXIT
echo.
echo =======================^[PROCESS DONE^]=========================


@pause







