------------------------------------------------------------------------------------
File Revision History (top to bottom: first revision to last revision)

 Date          user    (Description on following lines: changes, etc.)
 ----------- --------
 2-Nov-2017  MJ.Kim
 + Created initial version.

 27-Nov-2017  MJ.Kim
 + Added STMTouch Driver 
 + Tested in STM8L Touch evaluation baord: STMT/8L-EV1 (MB931) 

 26-Dec-2017  MJ.Kim
 + Re-defined I2C message format: version: 3.4
 + Removed LED module

 19-Jan-2018  MJ.Kim
 + Changed GPIO according to "H/W Rev 1.02 released in Thu, Dec 21, 2017"

------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
README
이 패키지는 (주) 이노프레소의 모키보 제품의 ST8L 펌웨어 소스와 
개발 환경 설정 파일로 구성되어 있다. 
소스는 (주) 이노프레소의 재산임. 


------------------------------------------------------------------------------------
환경 
CPU: STM8L151F3
IDE: ST Visual Develop Version 4.3.11
Compiler: COSMIC STM8 C Compiler Special Edition Version: 4.4.7
Debugger: ST-LINK/V2
//Evaluation board: STM8L-DISCOVERY
Evaluation board: Touch EV baord(STMT/8L-EV1), MCU STM8L151C6
Target: Mokibo clibar PCB v.x.x.x


------------------------------------------------------------------------------------
디렉토리
Source: Source files for mokibo
Workspace: STVD project files
Peripherals Library Drivers (StdPeriph_Driver) : V1.6.1 / 30-September-2014
STMTouch Driver: V1.4.3 / 24-February-2014

