/// \file timer.c
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 2-Nov-2017  MJ.Kim
* + Created initial version.
*
* 1-Dec-2017  MJ.Kim
* + Removed MOKIBO_TIMER_SRC, 
* + Application timer is TIM1
* + TIM4 shall be used for STM8 Touch library
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/
/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/
#include "mokibo_config.h"
#include "tsl_time_stm8l.h"
#include "tsl_time.h"
#include "timer.h"        

/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/             

/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/

/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/

/*============================================================================*/
/*                       PROTOTYPES OF LOCAL FUNCTIONS                        */
/*============================================================================*/
static void CLK_Config(void);
//static void Timer_Init(void);


/*============================================================================*/
/*                              CLASS VARIABLES                               */
/*============================================================================*/
static TIMER_TICK Timers[TIMER_ID_MAX] = {0, };


/*============================================================================*/
/*                              LOCAL FUNCTIONS                               */
/*============================================================================*/
/*
================================================================================
   Function name : CLK_Config()
================================================================================
*/
static void CLK_Config(void)
{
	/* Select HSE as system clock source */
	CLK_SYSCLKSourceSwitchCmd(ENABLE);

	CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_1);		// 16 MHz

// Stanley:
	/* Enable LSI clock */
	CLK_LSICmd(ENABLE);
	// Stanley:

	CLK_SYSCLKSourceConfig(CLK_SYSCLKSource_HSI);
	//CLK_SYSCLKSourceConfig(CLK_SYSCLKSource_LSI);

	/* Wait until clock is stablized */
	while (CLK_GetSYSCLKSource() != CLK_SYSCLKSource_HSI)
	{ ; }

	CLK_PeripheralClockConfig(CLK_Peripheral_TIM1, ENABLE);
}

/*
================================================================================
   Function name : Timer_Init()
================================================================================
*/
void Timer_Init(void)
{
  	TIM1_DeInit();
  	
	/* Time base configuration */
	// 1ms at 16MHz: 16M/(128*125) = 1KHz => 1ms
	TIM1_TimeBaseInit(128, TIM1_CounterMode_Up, 125, 0);

  	/* Clear update flag */
  	TIM1_ClearFlag(TIM1_FLAG_Update);

	/* Enable Update interrupt */
	TIM1_ITConfig(TIM1_IT_Update, ENABLE);

	/* Enable TIM1 */
  	TIM1_Cmd(ENABLE);
}

/*
================================================================================
   Function name : Timer_Init_for_wakeup()
================================================================================
*/
void Timer_Init_for_wakeup(void)
{
	//
	TIM1_DeInit();

	/* Time base configuration */
	// 1ms at 16MHz: 16M/(128*500) = 0.25 KHz => 4ms
//	TIM1_TimeBaseInit(128, TIM1_CounterMode_Up, 500, 0);

	// 16M/(1280*5000): => 4ms * 100 = 400ms
//	TIM1_TimeBaseInit(1280, TIM1_CounterMode_Up, 5000, 0);

	// 16M/(2560*5000): => 8ms * 100 = 800ms
//	TIM1_TimeBaseInit(2560, TIM1_CounterMode_Up, 5000, 0);

	// 16M/(2560*10000): => 8ms * 100 = 1600ms = 1.6sec
//	TIM1_TimeBaseInit(2560, TIM1_CounterMode_Up, 10000, 0);


		// for sysclock source = LSI !!  10일때  대략 750 ms
//		TIM1_TimeBaseInit(2560, TIM1_CounterMode_Up, 10, 0);

		// for sysclock source = LSI !!  20일때  대략 1.5sec
		TIM1_TimeBaseInit(2560, TIM1_CounterMode_Up, 20, 0);


	/* Clear update flag */
	TIM1_ClearFlag(TIM1_FLAG_Update);

	/* Enable Update interrupt */
	TIM1_ITConfig(TIM1_IT_Update, ENABLE);

	/* Enable TIM1 */
	TIM1_Cmd(ENABLE);

}
/*
================================================================================
   Function name : Timer_Init_for_wakeup()
================================================================================
*/
void Timer_Init_for_wakeup_disable(void)
{
	//
	TIM1_DeInit();

	/* Time base configuration */
	// 1ms at 16MHz: 16M/(128*1250) = 0.1 KHz => 10ms
	TIM1_TimeBaseInit(128, TIM1_CounterMode_Up, 1250, 0);

	/* Clear update flag */
	TIM1_ClearFlag(TIM1_FLAG_Update);

	/* Enable Update interrupt */
	TIM1_ITConfig(TIM1_IT_Update, DISABLE);

	/* Enable TIM1 */
	TIM1_Cmd(DISABLE);

}

/*============================================================================*/
/*                              GLOBAL FUNCTIONS                              */
/*============================================================================*/
/*
================================================================================
   Function name : timer_init()
================================================================================
*/
void timer_init(void)
{
	u16 i;

	for(i=0; i<TIMER_ID_MAX; i++)
	{
		Timers[i] = 0;
	}

	// Initialize neccessary ST8L151 drivers
	
	// Init Clock
	CLK_Config();

	// Initialize neccessary ST8L151 drivers
	Timer_Init();	// init the 'TIM1'
}

/*
================================================================================
   Function name : timer_register()
================================================================================
*/
void timer_register(TIMER_ID_TYPE id, TIMER_TICK cnt)
{
	assert_param(TIMER_ID_MAX>id);
	Timers[id] = cnt;
}

/*
================================================================================
   Function name : timer_is_expired()
================================================================================
*/
bool timer_is_expired(TIMER_ID_TYPE id)
{
	assert_param(TIMER_ID_MAX>id);
	return !Timers[id] ? 1 : 0;
}

/*
================================================================================
   Function name : timer_update()
================================================================================
*/
void timer_update(void)
{
	// at every 1ms

	u16 i;

	for(i=0; i<TIMER_ID_MAX; i++)
	{
		if(Timers[i])
		{
			--Timers[i];
		}
	}
}

/*
================================================================================
   Function name : timer_cmd()
================================================================================
*/
void timer_cmd(FunctionalState cmd)
{
	// ENABLE/DISABLE
  	TIM1_Cmd(cmd);
}

/*
================================================================================
   Function name : timer_interrupt_handler()
================================================================================
*/
void timer_interrupt_handler(void)
{
	// at every 1ms
	timer_update();
  	
	TIM1_ClearITPendingBit(TIM1_IT_Update);
}

/*
================================================================================
   Function name : timer_touch_interrupt_handler()
================================================================================
*/
void timer_touch_interrupt_handler(void)
{
  TIM4->SR1 &= (uint8_t)(~TIM4_SR1_UIF);
  TSL_tim_ProcessIT();
}








/**
  * @brief  Wait 1 sec for LSE stabilisation .
  * @param  None.
  * @retval None.
  * Note : TIM4 is configured for a system clock = 2MHz
  */
void LSI_StabTime(void)
{

	CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, ENABLE);

	/* Configure TIM4 to generate an update event each 1 s */
	TIM4_TimeBaseInit(TIM4_Prescaler_16384, 123);
	/* Clear update flag */
	TIM4_ClearFlag(TIM4_FLAG_Update);

	/* Enable TIM4 */
	TIM4_Cmd(ENABLE);

	/* Wait 1 sec */
	while (TIM4_GetFlagStatus(TIM4_FLAG_Update) == RESET);

	TIM4_ClearFlag(TIM4_FLAG_Update);

	/* Disable TIM4 */
	TIM4_Cmd(DISABLE);
	CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, DISABLE);
}

/**
  * @brief  Configure RTC peripheral
  * @param  None
  * @retval None
  */
void RTC_Config(void)
{
	/* Enable RTC clock */
	CLK_RTCClockConfig(CLK_RTCCLKSource_LSI, CLK_RTCCLKDiv_1);

	/* Wait for LSE clock to be ready */
//	while (CLK_GetFlagStatus(CLK_FLAG_LSERDY) == RESET);

	/* wait for 1 second for the LSE Stabilisation */
//	LSI_StabTime();
	CLK_PeripheralClockConfig(CLK_Peripheral_RTC, ENABLE);

	/* Configures the RTC wakeup timer_step = RTCCLK/16 = LSE/16 = 488.28125 us */
	RTC_WakeUpClockConfig(RTC_WakeUpClock_RTCCLK_Div16);

	/* Enable wake up unit Interrupt */
	RTC_ITConfig(RTC_IT_WUT, ENABLE);

	/* Enable general Interrupt*/
	//enableInterrupts();
}


#if 0
void main(void)
{
	/* RTC configuration -------------------------------------------*/
	RTC_Config();

	/* RTC wake-up event every 500 ms (timer_step x (1023 + 1) )*/
	RTC_SetWakeUpCounter(1023);

	RTC_WakeUpCmd(ENABLE);

}
#endif


// End of file 
