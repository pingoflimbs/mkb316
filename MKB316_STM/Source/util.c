/// \file util.c
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 7-Dec-2017  MJ.Kim
* + Created initial version.
*
* 11-Dec-2017  MJ.Kim
* + Changed RESET_LONG_PRESS from 7s to 5s during development
* + Corrected bugs for long press detection		
*
* 19-Feb-2018  MJ.Kim
* + Created util_enbale_detect_wakeup(), util_detect_wakeup()
*
* 20-Feb-2018  MJ.Kim
* + Created Watch dog module: util_reload_WDT()
*
* 6-Mar-2018  MJ.Kim
* + Implemented util_enbale_detect_wakeup(), util_detect_wakeup()
*
* 8-Mar-2018  MJ.Kim
* + Changed util_detect_wakeup() to detect_wakeup()
*
* 14-Mar-2018  MJ.Kim
* + Created util_detect_wakeup() for test module
*
* 11-Apr-2018  MJ.Kim
* + Created util_memcpy(), util_memset()
*
* 20-Apr-2018  MJ.Kim
* + Changed open dratin to push-pull for nRF52 power conrol pin state 
* + This is guided by HW
*
* 11-Jun-2018  MJ.Kim
* + Changed WDT time 214 ms to 858 ms
*
* 20-Jun-2018  MJ.Kim
* + Changed WDT time 858 ms to 214 ms
* + Added IWDG_ReloadCounter() in a waiting loop
* + Changed WAKE_UP_DURATION from 1s to 3s
*
* Note: Mention Issue number for  ery change here after.
*
================================================================================
*/
/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/
#include "button.h"        
#include "timer.h"        
#include "exchange.h"        
#include "rgb_led.h"        
#include "util.h"        
#include "touch.h"	// to reset the clickbar touch
#include "stm8l15x_pwr.h"

extern int m_reset_by_wakeup;

void RTC_Config(void);

/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/             
//#define GPIO_HIGH(a,b) 		(a->ODR|=b)
//#define GPIO_LOW(a,b)		(a->ODR&=~b)
//#define GPIO_TOGGLE(a,b) 	(a->ODR^=b)

/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/
#if (MOKIBO_TARGET==MOKIBO_STMT_8L_EV1)
	#define BLEPWR_PORT	    	GPIOE
	#define BLE_PWR_PIN     	GPIO_Pin_6
#elif (MOKIBO_TARGET==MOKIBO_REAL)
	#define BLEPWR_PORT   	GPIOC		// GPIO Port-C
	#define BLE_PWR_PIN    	GPIO_Pin_3	// 24_PC3: [out] EN_BLE (GPIO_PIN_3)
#else
	#error "What happen!!"
#endif

#define KEYMATRIX_PORT 		GPIOA		// GPIO Port-A
#define DEL_IN_PIN     		GPIO_Pin_2	// 02_PA2:	[in]	DEL key (KSI_1)
#define FN_IN_PIN      		GPIO_Pin_3	// 03_PA3:	[in]	Fn key (KSI_7)
#define DEL_OUT_PIN       	GPIO_Pin_4	// 04_PA4:	[out]	DEL key (KSO_2)
#define FN_OUT_PIN      	GPIO_Pin_5	// 05_PA5:	[out]	Fn key (KSO_14)

#define NO_OPERATION		0xFF			//
#define RESET_HOLD			500				// 500ms
#define RESET_LONG_PRESS	5800			// for 7s, Optimized ( 5,800 ms )

#define WDT_RELOAD_VALUE   	254				//
#define WAKE_SLEEP_DURATION	3000			// 3s To prevent from repeated power cycle
#define WAKE_UP_DURATION	3000			// 3s

/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/

/*============================================================================*/
/*                       PROTOTYPES OF LOCAL FUNCTIONS                        */
/*============================================================================*/
static void update(void);
static void detect_wakeup(void);

/*============================================================================*/
/*                              CLASS VARIABLES                               */
/*============================================================================*/
static GPIO_TypeDef* BLE_PWR_PORT	= BLEPWR_PORT;		// GPIOC
static GPIO_TypeDef* KEY_MATRIX_PORT= KEYMATRIX_PORT;	// GPIOA
static uint8_t Delay_sec; 
static util_keys_info_t Wakeup;

/*============================================================================*/
/*============================================================================*/
void ble_pwr_init(void)
{
	GPIO_Init(BLE_PWR_PORT, BLE_PWR_PIN, GPIO_Mode_Out_PP_High_Fast);// 24_PC3: [out] EN_BLE (GPIO_PIN_3)
}
void ble_pwr_on(void)
{
	// Turn-on BLE Power
	GPIO_HIGH(BLE_PWR_PORT, BLE_PWR_PIN);// 24_PC3: [out] EN_BLE (GPIO_PIN_3)
}

void ble_pwr_off(void)
{
	// Turn-off BLE Power
	GPIO_LOW(BLE_PWR_PORT, BLE_PWR_PIN);// 24_PC3: [out] EN_BLE (GPIO_PIN_3)
}




/*
================================================================================
   Function name : update()
================================================================================
*/
static void update(void)
{
	//////////////////////////////////////////////////////////////////////
	// Process of Power cycle
	//////////////////////////////////////////////////////////////////////
	if(NO_OPERATION != Delay_sec)
	{
		if(timer_is_expired(TIMER_ID_UTIL))
		{
			// Reset signal out
			ble_pwr_off();
			//GPIO_LOW(BLE_PWR_PORT, BLE_PWR_PIN);// 24_PC3: [out] EN_BLE (GPIO_PIN_3)

			// Hold reset signal during RESET_HOLD time
			timer_register(TIMER_ID_UTIL, RESET_HOLD);	// 약 500ms
			while(!timer_is_expired(TIMER_ID_UTIL))
			{  IWDG_ReloadCounter(); }

			// Reset signal back
			ble_pwr_on();
			//GPIO_HIGH(BLE_PWR_PORT, BLE_PWR_PIN);// 24_PC3: [out] EN_BLE (GPIO_PIN_3)

			// 본래 BLE가-->STM8에게 보내는 FW_CMD중 0(Sleep), 1(Run), 2(Test), 3(Reset)가 있는데,
			// 그중 'Reset'의 의미는 'STM8 네가 나(BLE)를 (delay후에...약7초후에) Reset 시켜줘' 라는 의미이다.
			timer_register(TIMER_ID_UTIL_RESET, RESET_LONG_PRESS);	// 약 7초
			Delay_sec = NO_OPERATION;
			
			// Reset ME(STM8L)
			exchange_set_host_cmd(EXCHANGE_HOST_CMD_RESET);
		}
	}
	else
	{
		if((BUTTON_STATUS_RELEASE == GetClickBarButtonStat(BUTTON_TYPE_LEFT)) 
				|| (BUTTON_STATUS_RELEASE == GetClickBarButtonStat(BUTTON_TYPE_RIGHT)))
		{
			timer_register(TIMER_ID_UTIL_RESET, RESET_LONG_PRESS);	// 약 7초
		}
		
		#if 0
		// Hold press left/right buttons for RESET_LONG_PRESS
		if(timer_is_expired(TIMER_ID_UTIL_RESET))
		{
			// Reset signal out			
			ble_pwr_off();
			//GPIO_LOW(BLE_PWR_PORT, BLE_PWR_PIN);// 24_PC3: [out] EN_BLE (GPIO_PIN_3)

			// Hold reset signal during RESET_HOLD time
			timer_register(TIMER_ID_UTIL_RESET, RESET_HOLD);

			while(!timer_is_expired(TIMER_ID_UTIL_RESET))
			{  IWDG_ReloadCounter(); }
		
			// Reset signal back
			ble_pwr_on();
			//GPIO_HIGH(BLE_PWR_PORT, BLE_PWR_PIN);// 24_PC3: [out] EN_BLE (GPIO_PIN_3)

			timer_register(TIMER_ID_UTIL_RESET, RESET_LONG_PRESS);
			Delay_sec = NO_OPERATION;

			// Reset ME(STM8L)
			exchange_set_host_cmd(EXCHANGE_HOST_CMD_RESET);
		}
		#else	//flimbs 리셋제거
		// Hold press left/right buttons for RESET_LONG_PRESS
		if(timer_is_expired(TIMER_ID_UTIL_RESET))
		{
			/*
			// Reset signal out			
			ble_pwr_off();
			//GPIO_LOW(BLE_PWR_PORT, BLE_PWR_PIN);// 24_PC3: [out] EN_BLE (GPIO_PIN_3)

			// Hold reset signal during RESET_HOLD time
			timer_register(TIMER_ID_UTIL_RESET, RESET_HOLD);

			while(!timer_is_expired(TIMER_ID_UTIL_RESET))
			{  IWDG_ReloadCounter(); }
		
			// Reset signal back
			ble_pwr_on();
			//GPIO_HIGH(BLE_PWR_PORT, BLE_PWR_PIN);// 24_PC3: [out] EN_BLE (GPIO_PIN_3)

			timer_register(TIMER_ID_UTIL_RESET, RESET_LONG_PRESS);
			Delay_sec = NO_OPERATION;

			// Reset ME(STM8L)
			exchange_set_host_cmd(EXCHANGE_HOST_CMD_RESET);|
			*/
		}
		#endif

	}
	
	//////////////////////////////////////////////////////////////////////
	// Process of Wake up 
	//////////////////////////////////////////////////////////////////////

	// Detect wake up cmd  in sleep mode
	if(EXCHANGE_HOST_CMD_SLEEP == exchange_get_host_cmd())
	{
		// wakeup key 감지를 '시작'하라는 명령이 있고 나서, 3초가 지난 후, key-polling을 시작한다.
		//	[ Wakeup.key.started --> Wakeup.key.detect --> ... ] 등의 단계를 거친다.
		detect_wakeup();

		//
		if(Wakeup.key.on_off && Wakeup.key.fn)
		{
			rgb_led_set_status(RBG_LED_STATUS_DIMMING);
		}
		else
		{
			rgb_led_set_status(RBG_LED_STATUS_OFF);
		}

		// Wake-up을 하라는 신호가 감지되었다. [Fn+Del]이 3초(WAKE_UP_DURATION)이상 눌려졌다.
		//	(BLE측에서 감지?  STM8에서 감지? ---> STM8에서 감지)
		if(Wakeup.key.detect)
		{

			// BLE로부터 'RUN' command가 전달되어 온 것처럼, History를 기억하고
			exchange_set_host_cmd(EXCHANGE_HOST_CMD_RUN);

			// 지금부터 1초 후에....host(BLE)의 전원을 Reset한다 ( ...-->LOW-->HIGH )
			// 그러기 위해	TIM1 timer에, (TIMER_ID_UTIL, 1sec)를 등록한다.
			util_reset_host(1);

			// 처리했다....
			Wakeup.key.detect = 0;
		}
	}
}

/*
================================================================================
   Function name : IWDG_Config()
================================================================================
*/
// Refer to STM8L15x-16x-05x-AL31-L_StdPeriph_Lib\Project\STM8L15x_StdPeriph_Examples\IWDG\IWDG_Example
static void IWDG_Config(void)
{

//Stanley: disable....
return;


  /* Enable IWDG (the LSI oscillator will be enabled by hardware) */
  IWDG_Enable(); 
  
  /* IWDG timeout equal to 858 ms (the timeout may varies due to LSI frequency dispersion) */
  /* Enable write access to IWDG_PR and IWDG_RLR registers */
  IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
 
  /* IWDG configuration: IWDG is clocked by LSI = 38KHz */
  #if 0 
  // IWDG timeout = (RELOAD_VALUE + 1) * Prescaler / LSI 
  // For 858 ms = (254 + 1) * 128 / 38 000
  IWDG_SetPrescaler(IWDG_Prescaler_128);
  // For 1.7 ms = (254 + 1) * 256 / 38 000
  IWDG_SetPrescaler(IWDG_Prescaler_256);
  #else
  // For 214 ms = (254 + 1) * 32 / 38 000
  //IWDG_SetPrescaler(IWDG_Prescaler_32);

  // For 858 ms = (254 + 1) * 128 / 38 000
  IWDG_SetPrescaler(IWDG_Prescaler_128);

#endif

  IWDG_SetReload((uint8_t)WDT_RELOAD_VALUE);
 
  /* Reload IWDG counter */
  IWDG_ReloadCounter();
}

/*
================================================================================
   Function name :		is_fn_del_pressed
================================================================================
*/
int is_fn_del_pressed(void)
{
	/// Fn-out을 'High'로 출력한 상태에서, 접점이 생기면, 그 접점 input (Fn-in)으로 'High'가 전달된다.
	/// 즉 Fn-key를 눌렀는지의 여부가 결정된다.
	GPIO_HIGH(KEY_MATRIX_PORT, FN_OUT_PIN);														// 05_PA5:	[out]	Fn key (KSO_14)
	util_soft_delay(20);
	Wakeup.key.fn = GPIO_ReadInputDataBit(KEY_MATRIX_PORT, (GPIO_Pin_TypeDef)FN_IN_PIN) ? 1 : 0;// 03_PA3:	[in]	Fn key (KSI_7)
	util_soft_delay(20);
	GPIO_LOW(KEY_MATRIX_PORT, FN_OUT_PIN);														// 05_PA5:	[out]	Fn key (KSO_14)

	/// Del-out을 'High'로 출력한 상태에서, 접점이 생기면, 그 접점 input(DEL-in)으로 'High'가 전달된다.
	/// 즉 DEL-key를 눌렀는지의 여부가 결정된다.
	GPIO_HIGH(KEY_MATRIX_PORT, DEL_OUT_PIN);														 // 04_PA4:	[out]	DEL key (KSO_2)
	util_soft_delay(20);
	Wakeup.key.on_off = GPIO_ReadInputDataBit(KEY_MATRIX_PORT, (GPIO_Pin_TypeDef)DEL_IN_PIN) ? 1 : 0;// 02_PA2:	[in]	DEL key (KSI_1)
	util_soft_delay(20);
	GPIO_LOW(KEY_MATRIX_PORT, DEL_OUT_PIN);															 // 04_PA4:	[out]	DEL key (KSO_2)

	//
	if ((1 == Wakeup.key.on_off) && (1 == Wakeup.key.fn))
	{
		return 1; // yes, both are pressed
	}
	return 0;
}
/*
================================================================================
   Function name :		actions_entering_low_power_mode
================================================================================
*/
void actions_entering_low_power_mode(void)
{
	#if 0
	// Clickbar Touch
	GPIO_Init(GPIOB, GPIO_Pin_2, GPIO_Mode_Out_PP_High_Fast);
	GPIO_Init(GPIOB, GPIO_Pin_3, GPIO_Mode_Out_PP_High_Fast);
	GPIO_Init(GPIOB, GPIO_Pin_5, GPIO_Mode_Out_PP_High_Fast);
	GPIO_Init(GPIOB, GPIO_Pin_6, GPIO_Mode_Out_PP_High_Fast);

	// CLickbar Button
	GPIO_Init(GPIOD, GPIO_Pin_0, GPIO_Mode_Out_PP_High_Fast);
	GPIO_Init(GPIOD, GPIO_Pin_1, GPIO_Mode_Out_PP_High_Fast);

	// BAT Level, BAT Stat
	GPIO_Init(GPIOB, GPIO_Pin_7, GPIO_Mode_Out_PP_High_Fast);
	GPIO_Init(GPIOB, GPIO_Pin_0, GPIO_Mode_Out_PP_High_Fast);

	// LED_DI, LED_CO
	GPIO_Init(GPIOC, GPIO_Pin_4, GPIO_Mode_Out_PP_High_Fast);
	GPIO_Init(GPIOC, GPIO_Pin_5, GPIO_Mode_Out_PP_High_Fast);

	// PB1, PB4
	GPIO_Init(GPIOB, GPIO_Pin_1, GPIO_Mode_Out_PP_High_Fast);
	GPIO_Init(GPIOB, GPIO_Pin_4, GPIO_Mode_Out_PP_High_Fast);
	#else
	// Clickbar Touch
	GPIO_Init(GPIOB, GPIO_Pin_2, GPIO_Mode_Out_PP_High_Fast);
	GPIO_Init(GPIOB, GPIO_Pin_3, GPIO_Mode_Out_PP_High_Fast);
	GPIO_Init(GPIOB, GPIO_Pin_5, GPIO_Mode_Out_PP_High_Fast);
	GPIO_Init(GPIOB, GPIO_Pin_6, GPIO_Mode_Out_PP_High_Fast);

	// CLickbar Button
	GPIO_Init(GPIOD, GPIO_Pin_0, GPIO_Mode_Out_PP_High_Fast);
	GPIO_Init(GPIOD, GPIO_Pin_1, GPIO_Mode_Out_PP_High_Fast);

	// BAT Level, BAT Stat
	GPIO_Init(GPIOC, GPIO_Pin_0, GPIO_Mode_Out_PP_High_Fast);
	GPIO_Init(GPIOC, GPIO_Pin_6, GPIO_Mode_Out_PP_High_Fast);
	GPIO_Init(GPIOC, GPIO_Pin_7, GPIO_Mode_Out_PP_High_Fast);

	// LED_DI, LED_CO
	GPIO_Init(GPIOC, GPIO_Pin_4, GPIO_Mode_Out_PP_High_Fast);
	GPIO_Init(GPIOC, GPIO_Pin_5, GPIO_Mode_Out_PP_High_Fast);

	// PB1, PB4
	GPIO_Init(GPIOB, GPIO_Pin_1, GPIO_Mode_Out_PP_High_Fast);
	GPIO_Init(GPIOB, GPIO_Pin_4, GPIO_Mode_Out_PP_High_Fast);
	#endif
}
/*
================================================================================
   Function name :		actions_exiting_from_low_power_mode
================================================================================
*/
void actions_exiting_from_low_power_mode(void)
{





}

/*
================================================================================
   Function name :		wakeup_by_fn_del
================================================================================
*/
#if WAKEUP_BY == BY_FN_DEL
void wakeup_by_fn_del(void)
{
	int wfi_loop = 0;
	int press_count = 0;

	//
	wfi_loop = 0;
	press_count = 0;

	//
	disableInterrupts();

	/* Disable update interrupt */
	TIM1_ITConfig(TIM1_IT_Update, DISABLE);
	TIM1_Cmd(DISABLE);

	// touch interrupt로 사용되고 있었다.
	TIM4_Cmd(DISABLE);
	TIM4_ITConfig(TIM4_IT_Update, DISABLE);

	// 10ms periodic
	//enableInterrupts();

#ifdef USING_RTC_WAKE_UP
	/* RTC configuration -------------------------------------------*/
	RTC_Config();
#else //USING_TIM1_WAKE_UP
	Timer_Init_for_wakeup();
#endif

	//actions_entering_low_power_mode();
	while (1)
	{
#ifdef USING_RTC_WAKE_UP
		/* RTC configuration -------------------------------------------*/
	//	RTC_Config();
		/* RTC wake-up event every 500 ms (timer_step x (1023 + 1) )*/
		RTC_SetWakeUpCounter(1023);
		//RTC_SetWakeUpCounter(40230);
		RTC_WakeUpCmd(ENABLE);

#else //USING_TIM1_WAKE_UP
		// Make slow timing... duration= 1.5 sec
		Timer_Init_for_wakeup();
		TIM1_Cmd(ENABLE);
#endif

		//=====================================================
		CLK_SYSCLKSourceConfig(CLK_SYSCLKSource_LSI);
		wfi();
		CLK_SYSCLKSourceConfig(CLK_SYSCLKSource_HSI);
		//=====================================================

		// restore the original timing.... DUration= 1ms
		Timer_Init();

		util_soft_delay(500);

#ifdef USING_RTC_WAKEUP
		RTC_WakeUpCmd(DISABLE);
#else //USING_TIM1_WAKE_UP
		TIM1_Cmd(DISABLE);
#endif


		wfi_loop++;
		//if (wfi_loop > 10000)	// 10000일 때 약 18초   <-- 1ms timer interrupt 기준
		//if (wfi_loop > 5000)	//  5000일 때 약 9초   <-- 1ms timer interrupt 기준
		//if (wfi_loop > 1600)	//  1600일 때 약 3초   <-- 1ms timer interrupt 기준
		//if(wfi_loop > 8) // 400ms x 8 = 3.2 sec
		if (wfi_loop >= 2) // 1600ms x 2 = 3.2 sec
		{
			wfi_loop = 0;
			if (is_fn_del_pressed())
			{
				press_count++;
				if (press_count > 0)
				{
					press_count = 0;
					break;
				}
			}
		}

	}//while(1)

	// reset하는 것이 아니라, touch만 init한다.
	//m_reset_by_wakeup = 1;

	// Restore the original TIM1 settings
	Timer_Init();
	actions_exiting_from_low_power_mode();
	//
	Wakeup.key.detect = 1; // Wake-up을 하라는 신호가 감지되었다. 즉, (Fn+Del)을 3초 이상 눌렀다.
	
	//
	return;
}
#endif // BY_FN_DEL

/*
================================================================================
   Function name :		wakeup_by_left_button
================================================================================
*/
#if WAKEUP_BY== BY_CLICKBAR_BUTTON
void wakeup_by_left_button(void)
{
	// jaehong
	PWR_FastWakeUpCmd(ENABLE);
	PWR_UltraLowPowerCmd(ENABLE);
	
	CLK_LSICmd(DISABLE);
	while ((CLK->ICKCR & 0x04) != 0x00);
	CLK_PeripheralClockConfig(CLK_Peripheral_TIM1, DISABLE);
	TIM1_Cmd(DISABLE);
	CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, DISABLE);
	TIM4_Cmd(DISABLE);
	// jaehong
	
	// LED 전원이 BLE에 같이 묶여 있어서, 전원이 off(GND)되는 경우
	//  LED_DI/LED_CO를 통하여 전류가 역류한다. 그래서 Float 또는 HiZ 시킨다.
	make_float_rgb_led();

	// Button Interrupt로 살아나기...
	button_init_for_button_interrupt();
	
	// Turn-off BLE Power
	ble_pwr_off();
	//GPIO_LOW(BLE_PWR_PORT, BLE_PWR_PIN);// 24_PC3: [out] EN_BLE (GPIO_PIN_3)

	// Enable Interrupts Globally
	enableInterrupts();

	// Halt() remebers the last clock source
	CLK_SYSCLKSourceConfig(CLK_SYSCLKSource_HSI);

	// Re-starts by an external interrupt: Left-Button
	halt();

	//
	disableInterrupts();

	// for sure
	CLK_SYSCLKSourceConfig(CLK_SYSCLKSource_HSI);

	// Turn-on BLE Power
	ble_pwr_on();
	//GPIO_HIGH(BLE_PWR_PORT, BLE_PWR_PIN);// 24_PC3: [out] EN_BLE (GPIO_PIN_3)
	
	// 원래 LED 세팅으로 복귀....
	rgb_led_init(RGB_LED_RESET_ALL);

	// 원래 button 세팅으로 복귀....
	button_init();

	// Restore the original TIM1 settings
	Timer_Init();
	
	// jaehong
	CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, ENABLE);
	TIM4_Cmd(ENABLE);
	// jaehong
	
	//
	actions_exiting_from_low_power_mode();
	//

	// Wake-up을 하라는 신호가 감지되었다. 즉, (Left-button)을 눌렀다 뗐다.
	Wakeup.key.detect = 1;


#ifdef SOFT_RESET_ON_WAKEUP
	//------------------------------------//
	// goto  soft_reset_by_wakeup:        //
	//------------------------------------//
	//enableInterrupts();
	ble_pwr_off();
	m_reset_by_wakeup = 1;
#endif //SOFT_RESET_ON_WAKEUP

}
#endif // WAKEUP_BY== BY_CLICKBAR_BUTTON

/*
================================================================================
   Function name : detect_wakeup()
================================================================================
*/

//#define	USING_RTC_WAKE_UP
#define USING_TIM1_WAKE_UP

// wakeup key를 감지를 '시작'하라는 명령이 있고 나서, 3초가 지난 후, key-polling을 시작한다.
//	[ Wakeup.key.started --> Wakeup.key.detect --> ... ] 등의 단계를 거친다.
static void detect_wakeup(void)
{
	int wfi_loop = 0;
	int press_count = 0;

	//
	if(Wakeup.key.started)
	{	// wakeup key를 감지를 '시작'하라는 명령이 있으면....

		Wakeup.key.fn 		= 0;	// <-- from Fn-key
		Wakeup.key.on_off 	= 0;	// <-- from DEL-key
		
		if(timer_is_expired(TIMER_ID_DETECT_WAKE_UP))
		{
			// 시작 명령이 있고 나서, 3초(WAKE_UP_DURATION)가 지났으므로,
			// 이제부터 정말 key polling을 시작한다.
			Wakeup.key.started = 0;
		}
	}
	else
	{
#if 1
		//--------------------------------------------------------------------------			
		// !!SHOW TIME !! - Do key-polling 
		//--------------------------------------------------------------------------			
		#if WAKEUP_BY == BY_FN_DEL
			wakeup_by_fn_del();
		#elif WAKEUP_BY == BY_CLICKBAR_BUTTON
			wakeup_by_left_button();
		#else
		#endif

#else
		/// Fn-out을 'High'로 출력한 상태에서, 접점이 생기면, 그 접점 input (Fn-in)으로 'High'가 전달된다.
		/// 즉 Fn-key를 눌렀는지의 여부가 결정된다.
		GPIO_HIGH(KEY_MATRIX_PORT, FN_OUT_PIN);														// 05_PA5:	[out]	Fn key (KSO_14)
		Wakeup.key.fn = GPIO_ReadInputDataBit(KEY_MATRIX_PORT, (GPIO_Pin_TypeDef)FN_IN_PIN) ? 1 : 0;// 03_PA3:	[in]	Fn key (KSI_7)
		GPIO_LOW(KEY_MATRIX_PORT, FN_OUT_PIN);														// 05_PA5:	[out]	Fn key (KSO_14)

		/// Del-out을 'High'로 출력한 상태에서, 접점이 생기면, 그 접점 input(DEL-in)으로 'High'가 전달된다.
		/// 즉 DEL-key를 눌렀는지의 여부가 결정된다.
		GPIO_HIGH(KEY_MATRIX_PORT, DEL_OUT_PIN);														 // 04_PA4:	[out]	DEL key (KSO_2)
		Wakeup.key.on_off = GPIO_ReadInputDataBit(KEY_MATRIX_PORT, (GPIO_Pin_TypeDef)DEL_IN_PIN) ? 1 : 0;// 02_PA2:	[in]	DEL key (KSI_1)
		GPIO_LOW(KEY_MATRIX_PORT, DEL_OUT_PIN);															 // 04_PA4:	[out]	DEL key (KSO_2)

		// Fn-key나 Del-key중 하나라도 눌리지 않은 상태이면, 시간 기점을 다시 잡는다.
		// 즉, 두개의 키가 모두 눌린 시점부터 3초(WAKE_UP_DURATION)동안 '계속' 눌려있는 지를 검사한다.
		if((0 == Wakeup.key.on_off) || (0 == Wakeup.key.fn))
		{
			// 하나라도 눌려있지 않으면, 시간은 계속 연장된다.
			timer_register(TIMER_ID_DETECT_WAKE_UP, WAKE_UP_DURATION);
		}

		// [Fn+Del]이 모두 눌린지 3초(WAKE_UP_DURATION)가 지났다면...Wake-up을 하라는 신호가 감지된 것이다.
		if(timer_is_expired(TIMER_ID_DETECT_WAKE_UP))
		{
			Wakeup.key.detect = 1; // Wake-up을 하라는 신호가 감지되었다.(Fn+Del)
		}
#endif

	}
}


/*============================================================================*/
/*                              GLOBAL FUNCTIONS                              */
/*============================================================================*/
/*
================================================================================
   Function name : util_init()
================================================================================
*/
void util_init(void)
{
	// clear
	Wakeup.data = 0;
	Delay_sec = NO_OPERATION;
	
	//GPIO_Init(BLE_PWR_PORT, BLE_PWR_PIN, GPIO_Mode_Out_OD_Low_Fast);// 24_PC3: [out] EN_BLE (GPIO_PIN_3)
	GPIO_Init(BLE_PWR_PORT, BLE_PWR_PIN, GPIO_Mode_Out_PP_High_Fast);// 24_PC3: [out] EN_BLE (GPIO_PIN_3)
	
	ble_pwr_on();
	//GPIO_HIGH(BLE_PWR_PORT, BLE_PWR_PIN);// 24_PC3: [out] EN_BLE (GPIO_PIN_3)

	timer_register(TIMER_ID_UTIL_RESET, RESET_LONG_PRESS);

	/* Check if the system has resumed from IWDG reset */
	if (RST_GetFlagStatus(RST_FLAG_IWDGF))
	{
		#if (MOKIBO_TEST==MOKIBO_CONFIG_ON)
		rgb_led_set_color(RGB_LED_COLOR_RSVD2);
		rgb_led_set_status(RBG_LED_STATUS_ON);
		
		timer_register(TIMER_ID_MAIN, 1000);

		while(!timer_is_expired(TIMER_ID_MAIN))
		{
			rgb_led_update();
		}
		#endif 

		/* Clear IWDGF Flag */
		RST_ClearFlag(RST_FLAG_IWDGF);
	}

	// Enable WDT
	IWDG_Config();	// 이 루틴 안에서 그냥 리턴하도록 수정되어 있다. stanley

}

/*
================================================================================
   Function name : util_update()
================================================================================
*/
void util_update(void)
{
	update();
}

/*
================================================================================
   Function name : util_ctrl_reset_host()
================================================================================
*/
void util_ctrl_reset_host(u8 state)
{
	// 24_PC3: [out] EN_BLE (GPIO_PIN_3)
	//state ? GPIO_HIGH(BLE_PWR_PORT, BLE_PWR_PIN) : GPIO_LOW(BLE_PWR_PORT, BLE_PWR_PIN) ;
	state ? ble_pwr_on() : ble_pwr_off();
}

/*
================================================================================
   Function name : util_reset_host()
================================================================================
*/
void util_reset_host(u8 delay_sec)
{
	// Max 7
	if(delay_sec > 7) { delay_sec = 7; };

	Delay_sec = delay_sec;
	
	timer_register(TIMER_ID_UTIL, (delay_sec *1000));
}

/*
================================================================================
   Function name : util_enbale_detect_wakeup()
================================================================================
*/
void util_enbale_detect_wakeup(u8 flag)
{
	if(flag)
	{	// Non-sleeping --> Sleeping 전환시...

		// enable GPIO for detect
		GPIO_Init(KEY_MATRIX_PORT, DEL_IN_PIN, GPIO_Mode_In_FL_No_IT);		// 02_PA2:	[in]	DEL key (KSI_1)
		GPIO_Init(KEY_MATRIX_PORT, FN_IN_PIN, GPIO_Mode_In_FL_No_IT);		// 03_PA3:	[in]	Fn key (KSI_7)
		GPIO_Init(KEY_MATRIX_PORT, DEL_OUT_PIN, GPIO_Mode_Out_PP_High_Fast);// 04_PA4:	[out]	DEL key (KSO_2)
		GPIO_Init(KEY_MATRIX_PORT, FN_OUT_PIN, GPIO_Mode_Out_PP_High_Fast);	// 05_PA5:	[out]	Fn key (KSO_14)

		// Set KSO pin low
		GPIO_LOW(KEY_MATRIX_PORT, DEL_OUT_PIN);	// 04_PA4:	[out]	DEL key (KSO_2)
		GPIO_LOW(KEY_MATRIX_PORT, FN_OUT_PIN);	// 05_PA5:	[out]	Fn key (KSO_14)
		// sleep mode에 들어갈 것이므로...
		// 앞으로 wakeup key를 받아야 한다는 의미?
		Wakeup.key.started = 1;
		// 약 3초 - to prevent from repeated power cycle
		timer_register(TIMER_ID_DETECT_WAKE_UP, WAKE_SLEEP_DURATION);
	}
	else
	{	// Sleeping --> Non-Sleeping 전환시...

		// disable GPIO for detect
		GPIO_Init(KEY_MATRIX_PORT, DEL_IN_PIN, GPIO_Mode_In_FL_No_IT);	// 02_PA2:	[in]	DEL key (KSI_1)
		GPIO_Init(KEY_MATRIX_PORT, FN_IN_PIN, GPIO_Mode_In_FL_No_IT);	// 03_PA3:	[in]	Fn key (KSI_7)
		//
		GPIO_Init(KEY_MATRIX_PORT, DEL_OUT_PIN, GPIO_Mode_In_FL_No_IT);	// 04_PA4:	[out]	DEL key (KSO_2)
		GPIO_Init(KEY_MATRIX_PORT, FN_OUT_PIN, GPIO_Mode_In_FL_No_IT);	// 05_PA5:	[out]	Fn key (KSO_14)
		
		// 이미 깨어났으므로....wakeup key를 받을 필요가 없음의 의미?
		Wakeup.key.started = 0;
	}
}

/*
================================================================================
   Function name : util_reload_WDT()
================================================================================
*/
void util_reload_WDT(void)
{
//Stanley:
	return; // debugiing purpose only Stanley SH YOON

	if(EXCHANGE_HOST_CMD_RESET != exchange_get_host_cmd())
	{
		IWDG_ReloadCounter();
	}
}

/*
================================================================================
   Function name : util_detect_wakeup()
================================================================================
*/
util_keys_info_t util_detect_wakeup()
{
	// wakeup key를 감지를 '시작'하라는 명령이 있고 나서, 3초가 지난 후, key-polling을 시작한다.
	//	[ Wakeup.key.started --> Wakeup.key.detect --> ... ] 등의 단계를 거친다.
	detect_wakeup();

	return Wakeup;
}

/*
================================================================================
   Function name : util_memcpy()
================================================================================
*/
void util_memcpy(u8 *to, u8* from, u8 size)
{
	while(size--)
	{
		*to++ = *from++;
	}
}

/*
================================================================================
   Function name : util_memset()
================================================================================
*/
void util_memset(u8 *to, u8 val, u8 size)
{
	while(size--)
	{
		*to++ = val;
	}
}

/*
================================================================================
   Function name : util_soft_delay()
================================================================================
*/
void util_soft_delay(__IO uint32_t nCount)
{
	for ( ; nCount != 0; nCount--);
}

// End of file 
