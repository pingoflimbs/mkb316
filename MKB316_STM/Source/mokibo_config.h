/// \file mokibo_config.h
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 2-Nov-2017  MJ.Kim
* + Created initial version.
*
* 27-Nov-2017  MJ.Kim
* + Changed MOKIBO_STM8L_DISCOVERY to Touch EV baord(STMT/8L-EV1)
* + Added MOKIBO_TIMER_SRC code because TIM4 is reserved for Touch library
*
* 1-Dec-2017  MJ.Kim
* + Removed MOKIBO_TIMER_SRC, 
* + Application timer is TIM1
* + TIM4 shall be used for STM8 Touch library
*
* 28-Dec-2017  MJ.Kim
* + Start manage MOKIBO_ST8L_SW_VER
*
* 19-Jan-2018  MJ.Kim
* + Set the MOKIBO_ST8L_SW_VER	0x04
*
* 19-Feb-2018  MJ.Kim
* + Set the MOKIBO_ST8L_SW_VER	0x05
*
* 6-Mar-2018  MJ.Kim
* + Set the MOKIBO_ST8L_SW_VER	0x06
*
* 8-Mar-2018  MJ.Kim
* + Set the MOKIBO_ST8L_SW_VER	0x07
*
* 10-Apr-2018  MJ.Kim
* + Set the MOKIBO_ST8L_SW_VER	0x08
*
* 20-Apr-2018  MJ.Kim
* + Set the MOKIBO_ST8L_SW_VER	0x09
*
* 8-Jun-2018  MJ.Kim
* + Set the MOKIBO_ST8L_SW_VER	0x0A
*
* 11-Jun-2018  MJ.Kim
* + Set the MOKIBO_ST8L_SW_VER	0x0B
*
* 20-Jun-2018  MJ.Kim
* + Set the MOKIBO_ST8L_SW_VER	0x0C
*
* 19-Jul-2018  MJ.Kim
* + Set the MOKIBO_ST8L_SW_VER	0x0D
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/

/*============================================================================*/
/*                            COMPILER DIRECTIVES                             */
/*============================================================================*/
#ifndef __MOKIBO_CONFIG_H
#define __MOKIBO_CONFIG_H

/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/

/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/
// TODO: DO NOT REMOVE HISTORY, ADD NEW LINE
//#define	MOKIBO_ST8L_SW_VER		0x01		// 2017.12.28
//#define	MOKIBO_ST8L_SW_VER		0x02		// 2017.12.28, Use MSB 3bit of rgb brightness
//#define	MOKIBO_ST8L_SW_VER		0x03		// 2018.1.11, Set White rbg color value by testing
//#define	MOKIBO_ST8L_SW_VER		0x04		// 2018.1.19, Changed GPIO according to "H/W Rev 1.02 released in Thu, Dec 21, 2017"
//#define	MOKIBO_ST8L_SW_VER		0x05		// 2018.2.19, Changed GPIO according to "H/W Rev 6.00(FAB09) released in Mon, Feb 19, 2018"
//#define	MOKIBO_ST8L_SW_VER		0x06		// 2018.3.6, Added sleep handler
//#define	MOKIBO_ST8L_SW_VER		0x07		// 2018.3.8, Implemented wake up by FN+On_off key
//#define	MOKIBO_ST8L_SW_VER		0x08		// 2018.4.10, Added Battery, according to "H/W Rev 6.26(FAB10) released in Apr 10, 2018"
//#define	MOKIBO_ST8L_SW_VER		0x09		// 2018.4.20, Corrted reft/right, "H/W Rev 6.26(FAB10) released in Apr 10, 2018" --> Revised to ??
//#define	MOKIBO_ST8L_SW_VER		0x0A		// 2018.6.8, "H/W Rev 6.35(FAB11) released in June 8, 2018 SECOND" 
//#define	MOKIBO_ST8L_SW_VER		0x0B		// 2018.6.14, Changed Touch left/right according to REAL TARGET, NOT schematic
//#define	MOKIBO_ST8L_SW_VER		0x0C		// 2018.6.20, Optimized power-cycle time
//#define	MOKIBO_ST8L_SW_VER		0x0D		// 2018.7.19, Applied tested smd led values
//#define	MOKIBO_ST8L_SW_VER			0b00100000 //v1.0.0 [001 000 00]	// 2019.1.8 Certification Release 1.0.0 Stanley

//#define	MOKIBO_ST8L_SW_VER			0b00100100 //v1.1.0 [001 001 00]	// 2019.7.9 LOW POWER MODE, JAEHONG
// <-- (battery level과 version 정보는, 1-byte 그대로 전달된다)
//    Maj(3bits):Min(3bits):Rev(2bits) = MMM:mmm:rr

//#define	MOKIBO_ST8L_SW_VER			0b00100101 //change yellow led color
//#define	MOKIBO_ST8L_SW_VER			0b00100111 // fix battery issue
//#define	MOKIBO_ST8L_SW_VER			0b00101000 // fix power save issue. pcbvversion 6.51
//#define	MOKIBO_ST8L_SW_VER			0b00101001 // fix power save issue. pcbvversion 6.72

#define	MOKIBO_ST8L_SW_VER			0b00101100 // add charger state value 6.72


#define MOKIBO_CONFIG_OFF			0
#define MOKIBO_CONFIG_ON			1

#define MOKIBO_STMT_8L_EV1			1
#define MOKIBO_REAL					2
#define MOKIBO_TARGET				MOKIBO_REAL

#define MOKIBO_ASSERT				MOKIBO_CONFIG_OFF
#define MOKIBO_TEST					MOKIBO_CONFIG_OFF


// Stanley:
#define	BY_FN_DEL			1
#define	BY_CLICKBAR_BUTTON	2

//#define	WAKEUP_BY		BY_FN_DEL
#define	WAKEUP_BY	BY_CLICKBAR_BUTTON // lef-button

#define SOFT_RESET_ON_WAKEUP


/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/
#define GPIO_HIGH(a,b) 		(a->ODR|=b)
#define GPIO_LOW(a,b)		(a->ODR&=~b)
#define GPIO_TOGGLE(a,b) 	(a->ODR^=b)



/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/


/*============================================================================*/
/*                               GLOBAL FUNCTIONS                             */
/*============================================================================*/

#endif // __MOKIBO_CONFIG_H 
