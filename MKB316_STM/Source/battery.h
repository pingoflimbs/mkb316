/// \file battery.h
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 20-Feb-2018  MJ.Kim
* + Created initial version.
*
* 6-Apr-2018  MJ.Kim
* + Added battery_get_charger_status
*
* 11-Apr-2018  MJ.Kim
* + Changed battery_get_charger_status() into battery_get_status()
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/

/*============================================================================*/
/*                            COMPILER DIRECTIVES                             */
/*============================================================================*/
#ifndef __BATTERY_H
#define __BATTERY_H

/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/    
#include "mokibo_config.h"
#include "stm8l15x.h"

/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/

/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/

/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/
typedef struct
{
    uint8_t charger		: 1;
    uint8_t update_req	: 1;
    uint8_t read_high	: 1;
    uint8_t rsvd 		: 5;
} battery_status_t;

/*============================================================================*/
/*                               GLOBAL FUNCTIONS                             */
/*============================================================================*/
void battery_init(void);
void battery_update(void);
void battery_update_request(void);
u8 battery_get_level(void);
battery_status_t battery_get_status(void);

#endif // __BATTERY_H 

