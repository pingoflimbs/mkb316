/// \file battery.c
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*j
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 20-Feb-2018  MJ.Kim
* + Created initial version.
*
* 13-Apr-2018  MJ.Kim
* + Implemented module
* + Added some delay in battery_update()
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/
/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/
#include "util.h"        
#include "battery.h"        

/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/             
//#define GPIO_HIGH(a,b) 			(a->ODR|=b)
//#define GPIO_LOW(a,b)			(a->ODR&=~b)

/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/
#define BATTERY_STATUS_PORT            	GPIOB				// Port-B
#define BATTERY_STATUS_PIN             	GPIO_Pin_0			// Port-B 12_PB0(PortB bit0)
#define BATTERY_CHECK_PORT            	GPIOC				// Port-C
#define BATTERY_CHECK_PIN             	GPIO_Pin_6			// Port-C 27_PC6(PortC bit6)  <--> 주의: Port-D 20_PD4도 함께 CHK_BAT로 사용해야 한다.
#define BATTERY_VOLTAGE_CHANNEL 		ADC_Channel_11		// PORT_B_7 19_PB7 (PortB bit7)

// PC6(CHK_BAT)와 PD4가 HW적으로 연결(Short)되어 있다.
#define BATTERY_CHECK_PORT2           	GPIOD				// Port-D
#define BATTERY_CHECK_PIN2             	GPIO_Pin_4			// bit4 <-->주의: Port-D 20_PD4도 함께 CHK_BAT로 사용해야 한다.

#define START_UPDATE 				 	0x80
#define READ_HIGH_BYTE					0x01

/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/

/*============================================================================*/
/*                       PROTOTYPES OF LOCAL FUNCTIONS                        */
/*============================================================================*/

/*============================================================================*/
/*                              CLASS VARIABLES                               */
/*============================================================================*/
static u16 ADCData = 0;
static battery_status_t battery_status;

/*============================================================================*/
/*                              LOCAL FUNCTIONS                               */
/*============================================================================*/

/*============================================================================*/
/*                              GLOBAL FUNCTIONS                              */
/*============================================================================*/
/*
================================================================================
   Function name : battery_init()
================================================================================
*/
void battery_init(void)
{
	// Set BATTERY_STATUS_PIN to input
	GPIO_Init(BATTERY_STATUS_PORT, BATTERY_STATUS_PIN, GPIO_Mode_In_FL_No_IT);


	// 주의: PC6와 PD4는 항상 동일하게 동작하도록 하여야 한다.
	// Set BATTERY_CHECK_PIN to open drain in normal
	GPIO_Init(BATTERY_CHECK_PORT, BATTERY_CHECK_PIN, GPIO_Mode_Out_OD_HiZ_Slow);
	// Set BATTERY_CHECK_PIN2 to open drain in normal
	GPIO_Init(BATTERY_CHECK_PORT2, BATTERY_CHECK_PIN2, GPIO_Mode_Out_OD_HiZ_Slow);


	/* Enable ADC1 clock */
  	CLK_PeripheralClockConfig(CLK_Peripheral_ADC1, ENABLE);

	/* Initialize and configure ADC1 */
	ADC_Init(ADC1, ADC_ConversionMode_Single, ADC_Resolution_12Bit, ADC_Prescaler_1);

	//ADC_SamplingTimeConfig(ADC1, ADC_Group_SlowChannels, ADC_SamplingTime_384Cycles);
	ADC_SamplingTimeConfig(ADC1, ADC_Group_SlowChannels, ADC_SamplingTime_192Cycles);

  	/* Enable ADC1 */
  	ADC_Cmd(ADC1, ENABLE);

	/* Enable ADC1 Channel used for battery voltage measurement */
	ADC_ChannelCmd(ADC1, BATTERY_VOLTAGE_CHANNEL, ENABLE);

	ADCData = 0;

	//util_memset((u8 *)&battery_status, 0, sizeof(battery_status));
}

/*
================================================================================
   Function name : battery_update()
================================================================================
*/
//----<Old>---------------------------------------------------------------------
//                     |
//	 Vbat------R175K---+---R124K---+---GND
//                     |
//                     |
//                    ADC
//		
//		Vadc= Vbat * 124/(175+124)= Vbat * 124/299 = 0.42 * Vbat
//		Vcut = 0.42 * 2.5 = 1.05 [volt]
//

//----<New>----------------------------------------------------------------------
//                     |
//	 Vbat------R174K---+---R124K---+---GND
//                     |
//                     |
//                    ADC
//		
//		Vadc= Vbat * 124/(174+124)= Vbat * 124/298 = 0.42 * Vbat
//		Vcut = 0.42 * 3.3 = 1.39 [volt]
//
//------------------------------------------------------------------------------
void battery_update(void)
{
	if(battery_status.update_req)
	{
		battery_init();
		
		// 주의: PC6와 PD4는 항상 동일하게 동작해야 한다. (HW적으로 short되어 있다). 
		// Step 1) Set BATTERY_CHECK_PIN to push pull
		GPIO_Init(BATTERY_CHECK_PORT, BATTERY_CHECK_PIN, GPIO_Mode_Out_PP_High_Fast);
		// Step 1) Set BATTERY_CHECK_PIN2 to push pull
		GPIO_Init(BATTERY_CHECK_PORT2, BATTERY_CHECK_PIN2, GPIO_Mode_Out_PP_High_Fast);

		// Step 2) Set BATTERY_CHECK_PIN to zero
		GPIO_LOW(BATTERY_CHECK_PORT, BATTERY_CHECK_PIN);
		// Step 2) Set BATTERY_CHECK_PIN2 to zero
		GPIO_LOW(BATTERY_CHECK_PORT2, BATTERY_CHECK_PIN2);

		// Some delay
		util_soft_delay(100);

		// Step 3) Start ADC1 Conversion using Software trigger
		ADC_SoftwareStartConv(ADC1);

		/* Wait until ADC Channel end of conversion */
		while (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET)
		{ ; }

		// Step 4) read ADC convertion result 
		ADCData = ADC_GetConversionValue(ADC1);

		// Step 5) Set back BATTERY_CHECK_PIN to OPEN_DRAIN
		GPIO_Init(BATTERY_CHECK_PORT, BATTERY_CHECK_PIN, GPIO_Mode_Out_OD_HiZ_Slow);
		// Step 5) Set back BATTERY_CHECK_PIN to OPEN_DRAIN
		GPIO_Init(BATTERY_CHECK_PORT2, BATTERY_CHECK_PIN2, GPIO_Mode_Out_OD_HiZ_Slow);

		// Step 6) Clear request flag
		battery_status.update_req = 0;
		
	/* Disable ADC 1 for reduce current */
  ADC_Cmd(ADC1, DISABLE);

  CLK_PeripheralClockConfig(CLK_Peripheral_ADC1, DISABLE);
	}
}

/*
================================================================================
   Function name : battery_update_request()
================================================================================
*/
void battery_update_request(void)
{
	ADCData = 0;
	battery_status.update_req = 1;
	battery_status.read_high = 1;
}

/*
================================================================================
   Function name : battery_get_level()
================================================================================
*/
u8 battery_get_level(void)
{
	u8 adc = 0;

	if(battery_status.read_high)
	{
		// Return high byte
		adc = (u8)((ADCData & 0xFF00)>>8);
		battery_status.read_high = 0;
	}
	else
	{
		// Return low byte
		adc = (u8)(ADCData & 0x00FF);
		battery_status.read_high = 1;
	}

	return adc;
}

/*
================================================================================
   Function name : battery_get_charger_status()
================================================================================

#define BATTERY_STATUS_PORT            	GPIOB				// Port-B
#define BATTERY_STATUS_PIN             	GPIO_Pin_0			// Port-B 12_PB0(PortB bit0)
#define BATTERY_CHECK_PORT            	GPIOC				// Port-C
#define BATTERY_CHECK_PIN             	GPIO_Pin_6			// Port-C 27_PC6(PortC bit6)  <--> 주의: Port-D 20_PD4도 함께 CHK_BAT로 사용해야 한다.
#define BATTERY_VOLTAGE_CHANNEL 		ADC_Channel_11		// PORT_B_7 19_PB7 (PortB bit7)

// PC6(CHK_BAT)와 PD4가 HW적으로 연결(Short)되어 있다.
#define BATTERY_CHECK_PORT2           	GPIOD				// Port-D
#define BATTERY_CHECK_PIN2             	GPIO_Pin_4			// bit4 <-->주의: Port-D 20_PD4도 함께 CHK_BAT로 사용해야 한다.
*/
battery_status_t battery_get_status(void)
{
	//if(GPIO_ReadInputDataBit(BATTERY_STATUS_PORT, (GPIO_Pin_TypeDef)BATTERY_STATUS_PIN))
	if(GPIO_ReadInputDataBit(BATTERY_CHECK_PORT, (GPIO_Pin_TypeDef)BATTERY_CHECK_PIN))
	{
		battery_status.charger = 1;
	}
	else
	{
		battery_status.charger = 0;
	}

	return battery_status;
}

// End of file 
