/// \file main.c
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/* 
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 2-Nov-2017  MJ.Kim
* + Created initial version.
*
* 7-Dec-2017  MJ.Kim
* + Added util
*
* 18-Jan-2018  MJ.Kim
* + Removed LED module
*
* 20-Feb-2018  MJ.Kim
* + Added battery module
* + Removed bug() in main loop : rgb_led_set_status(RBG_LED_STATUS_ON)
*
* 6-Mar-2018  MJ.Kim
* + Added sleep mode handler
*
* 13-Apr-2018  MJ.Kim
* + Placed battery_update outside of UPDATE_INTERVAL loop to update ASAP
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/
/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/
#include "mokibo_config.h"
#include "timer.h"
#include "button.h"
#include "touch.h"
#include "exchange.h"
#include "rgb_led.h"
#include "util.h"
#include "test.h"
#include "battery.h"

/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/             
int m_reset_by_wakeup = 0;

/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/
#define UPDATE_INTERVAL		1		// 1ms

/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/
/*============================================================================*/
/*                       PROTOTYPES OF LOCAL FUNCTIONS                        */
/*============================================================================*/


/*============================================================================*/
/*                              CLASS VARIABLES                               */
/*============================================================================*/

/*============================================================================*/
/*                              LOCAL FUNCTIONS                               */
/*============================================================================*/
/*============================================================================*/
/*                              GLOBAL FUNCTIONS                              */
/*============================================================================*/
/*
================================================================================
   Function name : main()
================================================================================
*/
extern void ble_pwr_init(void);
extern void ble_pwr_on(void);
extern void ble_pwr_off(void);

void main(void)
{
	int i, j;
soft_reset_by_wakeup:

#if 1
	// Wait until the User lift his finger off the button
	for (i = 0; i < 100; i++)
	{
		for (j = 0; j < 10; j++)
		{
			util_reload_WDT();
		}
	}
#endif

	// Initialize all neccessary modules here
	timer_init();
	button_init();
	exchange_init();
	rgb_led_init(RGB_LED_RESET_ALL);
//jaehong	battery_init();
	
	/* Enable Interrupts */
  	enableInterrupts();

  	/* Enable Application timer */
	timer_cmd(ENABLE);	// = TIM1_Cmd(cmd);

	util_init(); // after rgb_led_init() to indicate reset
	//
	touch_init();
	
	#if (MOKIBO_TEST==MOKIBO_CONFIG_ON)
	test_init();
	#endif 
	
	// main 1ms interval
	timer_register(TIMER_ID_MAIN, UPDATE_INTERVAL);
  	
	while(1)
	{
#ifdef SOFT_RESET_ON_WAKEUP
		if (m_reset_by_wakeup)
		{
			//============================
			// FOR re-calibration !!!
			//============================
			m_reset_by_wakeup = 0;
			// Turn-on BLE Power
			//ble_pwr_on();
			//util_reload_WDT();
			goto soft_reset_by_wakeup;
		}
#endif//SOFT_RESET_ON_WAKEUP

		// Update touch
      	if(touch_update())
    	{
			;
		}

		// Usually blocked, should be updated ASAP when needed 
		battery_update();

		// Update main function
		if(timer_is_expired(TIMER_ID_MAIN))
		{
			timer_register(TIMER_ID_MAIN, UPDATE_INTERVAL);
			button_update();
			rgb_led_update();
			exchange_update();
			util_update();

			#if (MOKIBO_TEST==MOKIBO_CONFIG_ON)
			test_update();
			#endif 
		
			// Reload Watch Dog Timer
			util_reload_WDT(); 

		}
	}
}

#ifdef USE_FULL_ASSERT
/*
================================================================================
   Function name : main()
================================================================================
*/
void assert_failed(uint8_t* file, uint32_t line)
{
	/* User can add his own implementation to report the file name and line number,
	ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	// For esay debugging
	uint16_t i, num;
	#define FILE_STRING_SIZE	32
	uint8_t string[FILE_STRING_SIZE] = {0, };
	
	num = line;

	for( i=0; i < FILE_STRING_SIZE; i++)
	{
		string[i] = *file++;

		if(!*file)
			break;
	}

	/* Infinite loop */
	while (1)
	{ ; }
}
#endif

// End of file 
