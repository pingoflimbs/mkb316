/// \file button.h
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 2-Nov-2017  MJ.Kim
* + Created initial version.
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/

/*============================================================================*/
/*                            COMPILER DIRECTIVES                             */
/*============================================================================*/
#ifndef __BUTTON_H
#define __BUTTON_H

/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/    
#include "mokibo_config.h"
#include "stm8l15x.h"

/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/

/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/

/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/
typedef enum {
    BUTTON_TYPE_LEFT = 0,           			// BUTTON LEFT
    BUTTON_TYPE_RIGHT,          				// BUTTON RIGHT
    BUTTON_TYPE_MAX        						// BUTTON Number
} BUTTON_TYPE;

typedef enum {
    BUTTON_STATUS_RELEASE = 0,            		// BUTTON RELEASE
    BUTTON_STATUS_PRESS,             			// BUTTON PRESS
    BUTTON_STATUS_MAX
} BUTTON_STATUS;

/*============================================================================*/
/*                               GLOBAL FUNCTIONS                             */
/*============================================================================*/
void button_init(void);
void button_init_for_button_interrupt(void);

BUTTON_STATUS GetClickBarButtonStat(BUTTON_TYPE button);

void button_update(void);

#endif // __BUTTON_H 
