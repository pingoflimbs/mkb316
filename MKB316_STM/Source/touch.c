/// \file touch.c
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 2-Nov-2017  MJ.Kim
* + Created initial version.
*
* 27-Nov-2017  MJ.Kim
* + Changed MOKIBO_STM8L_DISCOVERY to Touch EV baord(STMT/8L-EV1)
*
* 18-Jan-2018  MJ.Kim
* + Removed TOUCH_TYPE_CENTER
* + Created TOUCH_TYPE_CENTER_LEFT, TOUCH_TYPE_CENTER_RIGHT
*
* 19-Mar-2018  MJ.Kim
* + Changed TSLPRM_USE_DXS from 1 to 0 to enable detect touch independently!!!
*
* 20-Arp-2018  MJ.Kim
* + Chagned GPIO based on HW 6.26(FAB10)(Schematic left right reversed!!!)
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/
/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/
#include "mokibo_config.h"
#include "tsl_user.h"
#include "touch.h"        

/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/             

/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/
#define TEST_TKEY(NB) (MyTKeysB[(NB)].p_Data->StateId == TSL_STATEID_DETECT ||\
                       MyTKeysB[(NB)].p_Data->StateId == TSL_STATEID_DEB_RELEASE_DETECT)

/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/

/*============================================================================*/
/*                       PROTOTYPES OF LOCAL FUNCTIONS                        */
/*============================================================================*/

/*============================================================================*/
/*                              CLASS VARIABLES                               */
/*============================================================================*/


/*============================================================================*/
/*                              LOCAL FUNCTIONS                               */
/*============================================================================*/
/**
  * @brief  Executed when a sensor is in Error state (example)
  * @param  None
  * @retval None
  */
void MyTKeys_ErrorStateProcess(void)
{
  // Application decision to set the sensor in Off state
  TSL_tkey_SetStateOff();
}

/**
  * @brief  Executed when a sensor is in Off state
  * @param  None
  * @retval None
  */
void MyTKeys_OffStateProcess(void)
{
  // Add here your own processing when a sensor is in Off state
}

/*============================================================================*/
/*                              GLOBAL FUNCTIONS                              */
/*============================================================================*/
/*
================================================================================
   Function name : touch_init()
================================================================================
*/
void touch_init(void)
{
	// Init STMTouch driver
  	TSL_user_Init();
}

/*
================================================================================
   Function name : touch_get()
================================================================================
*/
TOUCH_STATUS touch_get(TOUCH_TYPE i)
{
	TOUCH_STATUS st;

	assert_param(TOUCH_TYPE_MAX>i);

	switch(i)
	{
		case TOUCH_TYPE_LEFT :
			st = (TEST_TKEY(0)) ? TOUCH_STATUS_ON : TOUCH_STATUS_OFF;
			break;

		case TOUCH_TYPE_RIGHT : 
			st = (TEST_TKEY(1)) ? TOUCH_STATUS_ON : TOUCH_STATUS_OFF;
			break;

		case TOUCH_TYPE_CENTER_LEFT :
			st = (TEST_TKEY(2)) ? TOUCH_STATUS_ON : TOUCH_STATUS_OFF;
			break;
		
		case TOUCH_TYPE_CENTER_RIGHT :
			st = (TEST_TKEY(3)) ? TOUCH_STATUS_ON : TOUCH_STATUS_OFF;
			break;

		default :
			// What happen??
			st = TOUCH_STATUS_OFF;
			break;
	}

	return st;
}

/*
================================================================================
   Function name : touch_update()
================================================================================
*/
u8 touch_update(void)
{	    
  	// Execute STMTouch Driver state machine
	return (TSL_STATUS_OK  == TSL_user_Action()) ? 1 : 0;
}

// End of file 
