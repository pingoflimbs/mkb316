/// \file rgb_led.c
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 8-Nov-2017  MJ.Kim
* + Created initial version.
*
* 27-Dec-2017  MJ.Kim
* + Rewrote code 
*
* 28-Dec-2017  MJ.Kim
* + Changed table according to "message.txt version 3.5"
* + refer to LED_COLOR_WRITE value
* + Added option in  
* + Used only bit 2, 3, 4 of RGB_LED_bright, bit 0, 1 should be always zero
*
* 20-Feb-2018  MJ.Kim
* + Changed table according to "message.txt version 3.7"
* + Created RGB_LED_COLOR_RSVD1/RGB_LED_COLOR_RSVD2
*
* 11-Apr-2018  MJ.Kim
* + Replaced Memcpy into util_memcpy() to share other module
*
* 19-July-2018  MJ.Kim
* + Issue: https://app.asana.com/0/644344618071463/657324836788372/f
* + Result: https://app.asana.com/0/644344618071463/663769309337242
* + Updated Color_Table value according to Issue Result
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/
/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/
#include "mokibo_config.h"
#include "timer.h"   
#include "util.h"
#include "rgb_led.h"     

/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/             
//#define GPIO_HIGH(a,b) 			(a->ODR|=b)
//#define GPIO_LOW(a,b)			(a->ODR&=~b)
//#define GPIO_TOGGLE(a,b) 		(a->ODR^=b)

/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/
#define DELAY_VAL				20		// Temp value
#define RGB_LED_BRIGHT_DEFAULT	0x1C	// TODO: 
#define DIMMING_TICK			20 		// 20ms
#define SLOW_BLINK_TICK			500 	// 500ms
#define FAST_BLINK_TICK			100 	// 100ms

#if (MOKIBO_TARGET==MOKIBO_STMT_8L_EV1)
#define SDI_PORT           	GPIOB		// Pin for Key button 8
#define SDI_PIN            	GPIO_Pin_4
#define CLK_PORT           	GPIOB		// Pin for Key button 9
#define CLK_PIN            	GPIO_Pin_6
#elif (MOKIBO_TARGET==MOKIBO_REAL)
#define SDI_PORT           	GPIOC
#define SDI_PIN            	GPIO_Pin_4
#define CLK_PORT           	GPIOC
#define CLK_PIN            	GPIO_Pin_5
#else
	#error "What happen!!"
#endif

#define START_FRAMAE_SIZE			4
#define DATA_FIELD_SIZE				4
#define END_FRAMAE_SIZE				4
#define DATA_FIELD_GLOBAL			0
#define DATA_FIELD_BLUE				1
#define DATA_FIELD_GREEN			2
#define DATA_FIELD_RED				3

#define PCB_VERSION_0_00 0	//original pcb
#define PCB_VERSION_6_51 1	// flimbs_20180817
#define PCB_VERSION_6_72 2		// flimbs_20190910
#define LED_VERSION PCB_VERSION_6_51


#if (LED_VERSION == PCB_VERSION_0_00)

// All this value is tested
// Refer to "https://app.asana.com/0/644344618071463/663769309337242"
	
	
	#define	RED_STATUS_COLOR_GREEN		0
	#define	GREEN_STATUS_COLOR_GREEN	56
	#define	BLUE_STATUS_COLOR_GREEN		2
	
	#define	RED_STATUS_COLOR_RED		16
	#define	GREEN_STATUS_COLOR_RED		0
	#define	BLUE_STATUS_COLOR_RED		0
	
	#define	RED_STATUS_COLOR_BLUE		0
	#define	GREEN_STATUS_COLOR_BLUE		0
	#define	BLUE_STATUS_COLOR_BLUE		148
	
	#define	RED_STATUS_COLOR_WHITE		8	// Tested value
	#define	GREEN_STATUS_COLOR_WHITE	40	
	#define	BLUE_STATUS_COLOR_WHITE		18
	
	#define	RED_STATUS_COLOR_RSVD2		15	// Yellow, tested value
	#define	GREEN_STATUS_COLOR_RSVD2	46
	#define	BLUE_STATUS_COLOR_RSVD2		0
	
	#define	RED_STATUS_COLOR_OFF		0x00
	#define	GREEN_STATUS_COLOR_OFF		0x00
	#define	BLUE_STATUS_COLOR_OFF		0x00
	
	/////////////////////unused
	
	#define	RED_STATUS_COLOR_RSVD1		10	// Purple, tested value
	#define	GREEN_STATUS_COLOR_RSVD1	0
	#define	BLUE_STATUS_COLOR_RSVD1		64
	
	#define	RED_STATUS_COLOR_ORANGE		17		
	#define	GREEN_STATUS_COLOR_ORANGE	20
	#define	BLUE_STATUS_COLOR_ORANGE	0
	

#elif (LED_VERSION == PCB_VERSION_6_51)
		//flimbs_20180910 
		//modified for 20180910 arrived LED  
		//informs at https://app.asana.com/0/762416392987824/778933611916499
		//red 00 00 72
		//green 0 75 0
		//yellow 0 78 68
		//blue 140 00 00
		//white 74 74 54
		
		
		//green
		#define	RED_STATUS_COLOR_GREEN		0
		#define	GREEN_STATUS_COLOR_GREEN	75
		#define	BLUE_STATUS_COLOR_GREEN		0
		
		//red
		#define	RED_STATUS_COLOR_RED		0
		#define	GREEN_STATUS_COLOR_RED		0
		#define	BLUE_STATUS_COLOR_RED		72
		
		//blue
		#define	RED_STATUS_COLOR_BLUE		140
		#define	GREEN_STATUS_COLOR_BLUE		0
		#define	BLUE_STATUS_COLOR_BLUE		0
		
		//white
		#define	RED_STATUS_COLOR_WHITE		74	// Tested value
		#define	GREEN_STATUS_COLOR_WHITE	74	
		#define	BLUE_STATUS_COLOR_WHITE		54
				
		//yellow
		#define	RED_STATUS_COLOR_RSVD2		0
		#define	GREEN_STATUS_COLOR_RSVD2	78
		#define	BLUE_STATUS_COLOR_RSVD2		68
		
		//off
		#define	RED_STATUS_COLOR_OFF		0x00
		#define	GREEN_STATUS_COLOR_OFF		0x00
		#define	BLUE_STATUS_COLOR_OFF		0x00
		
		/////////////////////unused
		//purple
		#define	RED_STATUS_COLOR_RSVD1		110	// Purple, tested value
		#define	GREEN_STATUS_COLOR_RSVD1	0
		#define	BLUE_STATUS_COLOR_RSVD1		58

		//orange
		#define	RED_STATUS_COLOR_ORANGE		0		
		#define	GREEN_STATUS_COLOR_ORANGE	57
		#define	BLUE_STATUS_COLOR_ORANGE	72
		


#elif (LED_VERSION == PCB_VERSION_6_72)
	//flimbs_20190807
	//modified for 20190807 arrived LED  
	
	//red 00 00 8
	//green 0 12 0
	//yellow 0 78 68 // not use
	//blue 90 00 00
	//white 74 74 54 // not use
	
	//green
	#define	RED_STATUS_COLOR_GREEN		0
	#define	GREEN_STATUS_COLOR_GREEN	12
	#define	BLUE_STATUS_COLOR_GREEN		0
	
	//red
	#define	RED_STATUS_COLOR_RED		0
	#define	GREEN_STATUS_COLOR_RED	0
	#define	BLUE_STATUS_COLOR_RED		8
	
	//blue
	#define	RED_STATUS_COLOR_BLUE			90
	#define	GREEN_STATUS_COLOR_BLUE		0
	#define	BLUE_STATUS_COLOR_BLUE		0
	
	//white
	#define	RED_STATUS_COLOR_WHITE		34
	#define	GREEN_STATUS_COLOR_WHITE	20	
	#define	BLUE_STATUS_COLOR_WHITE		7
	
	//yellow
	#define	RED_STATUS_COLOR_RSVD2		0
	#define	GREEN_STATUS_COLOR_RSVD2	8
	#define	BLUE_STATUS_COLOR_RSVD2		4
	
	//off
	#define	RED_STATUS_COLOR_OFF		0x00
	#define	GREEN_STATUS_COLOR_OFF		0x00
	#define	BLUE_STATUS_COLOR_OFF		0x00
	
	/////////////unused
	//purple
	#define	RED_STATUS_COLOR_RSVD1		50
	#define	GREEN_STATUS_COLOR_RSVD1	0
	#define	BLUE_STATUS_COLOR_RSVD1		4
	
	//orange
	#define	RED_STATUS_COLOR_ORANGE		0
	#define	GREEN_STATUS_COLOR_ORANGE	4
	#define	BLUE_STATUS_COLOR_ORANGE	6
	

#endif



/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/
typedef enum {
    CTRL_IO_SDI = 0,             		// Data
    CTRL_IO_CLK,             			// Clock
    CTRL_IO_NUM         				// I/O Number
} CTRL_IO_TYPE;

/*============================================================================*/
/*                       PROTOTYPES OF LOCAL FUNCTIONS                        */
/*============================================================================*/
static void SoftDelay(uint16_t val);
static void Send(u8 *buf, u8 len);
static void Update(void);
static void Activate(u8 *buf, u8 size);

/*============================================================================*/
/*                              CLASS VARIABLES                               */
/*============================================================================*/
static GPIO_TypeDef* CTRL_IO_PORT[CTRL_IO_NUM] =
{
    SDI_PORT,
    CLK_PORT,
};

static const uint8_t CTRL_IO_PIN[CTRL_IO_NUM] =
{ 
    SDI_PIN,
    CLK_PIN,
};

static const uint8_t BitMask[8] = {0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01};
static const uint8_t Start[START_FRAMAE_SIZE] = {0x0, 0x0, 0x0, 0x0};
static const uint8_t End[END_FRAMAE_SIZE]= {0xFF, 0xFF, 0xFF, 0xFF};

static const uint8_t Color_Table[RGB_LED_COLOR_CUSTOM][RBG_LED_PRIMARY_MAX] = \
{ 	\
{	RED_STATUS_COLOR_ORANGE,	GREEN_STATUS_COLOR_ORANGE, 	BLUE_STATUS_COLOR_ORANGE	},	\
{	RED_STATUS_COLOR_RED, 		GREEN_STATUS_COLOR_RED, 	BLUE_STATUS_COLOR_RED    	},	\
{	RED_STATUS_COLOR_GREEN,		GREEN_STATUS_COLOR_GREEN, 	BLUE_STATUS_COLOR_GREEN  	}, 	\
{	RED_STATUS_COLOR_BLUE, 		GREEN_STATUS_COLOR_BLUE, 	BLUE_STATUS_COLOR_BLUE   	},	\
{	RED_STATUS_COLOR_WHITE,		GREEN_STATUS_COLOR_WHITE,	BLUE_STATUS_COLOR_WHITE  	}, 	\
{	RED_STATUS_COLOR_RSVD1,		GREEN_STATUS_COLOR_RSVD1,	BLUE_STATUS_COLOR_RSVD1  	}, 	\
{	RED_STATUS_COLOR_RSVD2,		GREEN_STATUS_COLOR_RSVD2,	BLUE_STATUS_COLOR_RSVD2  	}, 	\
};

static const uint8_t Off[RBG_LED_PRIMARY_MAX] = \
{BLUE_STATUS_COLOR_OFF, GREEN_STATUS_COLOR_OFF, RED_STATUS_COLOR_OFF};

// TODO: Optimize to save RAM!!
static RGB_LED_COLOR_T Color 			= RGB_LED_COLOR_CUSTOM;
static RGB_LED_COLOR_T Color_physical 	= RGB_LED_COLOR_WHITE;
static u8 RGB_LED_bright 				= RGB_LED_BRIGHT_DEFAULT;
static u8 RGB_LED_bright_physical		= 0;
static RBG_LED_STATUS_T Status 			= RBG_LED_STATUS_OFF;
static RBG_LED_STATUS_T Status_physical = RBG_LED_STATUS_ON;
static u8 Custom_nibble 				= 0;
static u8 Custom[RBG_LED_PRIMARY_MAX]   = {0, };

/*============================================================================*/
/*                              LOCAL FUNCTIONS                               */
/*============================================================================*/

/*
================================================================================
   Function name : SoftDelay()
================================================================================
*/
static void SoftDelay(uint16_t val)
{
  	uint16_t idx;

  	for (idx = val; idx > 0; idx--)
  	{
    	nop();
  	}
}

/*
================================================================================
   Function name : Send()
================================================================================
*/
static void Send(u8 *buf, u8 len)
{
	u8 i, bit;

	// Send Start frame
	for(i=0; i<len; i++)
	{
		for(bit=0; bit<8; bit++) // 1byte(8bits)
		{
			// SDI is valid when CLK is "rising edge"

			// Set CLK to low
			GPIO_LOW(CTRL_IO_PORT[CTRL_IO_CLK], CTRL_IO_PIN[CTRL_IO_CLK]);
		
			// Set SDI depending on data
			if(buf[i] & BitMask[bit])
			{
				GPIO_HIGH(CTRL_IO_PORT[CTRL_IO_SDI], CTRL_IO_PIN[CTRL_IO_SDI]);
			}
			else
			{
				GPIO_LOW(CTRL_IO_PORT[CTRL_IO_SDI], CTRL_IO_PIN[CTRL_IO_SDI]);
			}

			// Some delay
			SoftDelay(DELAY_VAL);

			// Set CLK to high
			GPIO_HIGH(CTRL_IO_PORT[CTRL_IO_CLK], CTRL_IO_PIN[CTRL_IO_CLK]);
		}
	}
}

/*
================================================================================
   Function name : Activate()
================================================================================
*/
static void Activate(u8 *buf, u8 size)
{

	Send(Start, sizeof(Start));
	Send(buf, size);
	Send(End, sizeof(End));

	// Some delay
	SoftDelay(DELAY_VAL*10);
}

/*
================================================================================
   Function name : Update()
================================================================================
*/
static void Update(void)
{
	static u8 mem = 0;

	u8 data[DATA_FIELD_SIZE] = {0, };

	switch(Status)
	{
		case RBG_LED_STATUS_OFF :
			if((Status_physical != Status) 
					|| (Color_physical != Color) 
					|| (RGB_LED_bright_physical != RGB_LED_bright))
			{
				Status_physical = Status;
				Color_physical = Color;
				RGB_LED_bright_physical = RGB_LED_bright;
				data[DATA_FIELD_GLOBAL] = 0xE0 | (0x1F & RGB_LED_bright);
				util_memcpy(&data[DATA_FIELD_BLUE], &Off[0], RBG_LED_PRIMARY_MAX);
				Activate(data, DATA_FIELD_SIZE);
			}
		break;

		case RBG_LED_STATUS_ON :
			if((Status_physical != Status) 
					|| (Color_physical != Color) 
					|| (RGB_LED_bright_physical != RGB_LED_bright))
			{
				Status_physical = Status;
				Color_physical = Color;
				RGB_LED_bright_physical = RGB_LED_bright;
				data[DATA_FIELD_GLOBAL] = 0xE0 | (0x1F & RGB_LED_bright);
				
				if(RGB_LED_COLOR_CUSTOM == Color)
				{
					util_memcpy(&data[DATA_FIELD_BLUE], &Custom[0], RBG_LED_PRIMARY_MAX);
				}
				else
				{
					util_memcpy(&data[DATA_FIELD_BLUE], &Color_Table[Color][0], RBG_LED_PRIMARY_MAX);
				}

				Activate(data, DATA_FIELD_SIZE);
			}
		break;
		
		case RBG_LED_STATUS_DIMMING :
			
			if((Status_physical != Status) 
					|| (Color_physical != Color) 
					|| (RGB_LED_bright_physical != RGB_LED_bright))
			{
				Status_physical = Status;
				Color_physical = Color;
				RGB_LED_bright_physical = RGB_LED_bright;
				mem = RGB_LED_bright;
				timer_register(TIMER_ID_LED_SLOW_BLNIK, 0);
			}
		
			if(timer_is_expired(TIMER_ID_LED_SLOW_BLNIK))
			{
				timer_register(TIMER_ID_LED_SLOW_BLNIK, DIMMING_TICK);

				if(mem)
				{
					mem--;

					data[DATA_FIELD_GLOBAL] = 0xE0 | (0x1F & mem);

					if(RGB_LED_COLOR_CUSTOM == Color)
					{
						util_memcpy(&data[DATA_FIELD_BLUE], &Custom[0], RBG_LED_PRIMARY_MAX);
					}
					else
					{
						util_memcpy(&data[DATA_FIELD_BLUE], &Color_Table[Color][0], RBG_LED_PRIMARY_MAX);
					}

					Activate(data, DATA_FIELD_SIZE);
				}
			}
		break;
		
		case RBG_LED_STATUS_SLOW_BLINK :
		case RBG_LED_STATUS_FAST_BLINK :
			if((Status_physical != Status) 
					|| (Color_physical != Color) 
					|| (RGB_LED_bright_physical != RGB_LED_bright))
			{
				Status_physical = Status;
				Color_physical = Color;
				RGB_LED_bright_physical = RGB_LED_bright;
				mem = 1;
				timer_register(TIMER_ID_LED_SLOW_BLNIK, 0);
			}

			if(timer_is_expired(TIMER_ID_LED_SLOW_BLNIK))
			{
				timer_register(TIMER_ID_LED_SLOW_BLNIK, 
						(RBG_LED_STATUS_SLOW_BLINK == Status) ? SLOW_BLINK_TICK : FAST_BLINK_TICK);
				
				data[DATA_FIELD_GLOBAL] = 0xE0 | (0x1F & RGB_LED_bright);

				if(mem)
				{
					if(RGB_LED_COLOR_CUSTOM == Color)
					{
						util_memcpy(&data[DATA_FIELD_BLUE], &Custom[0], RBG_LED_PRIMARY_MAX);
					}
					else
					{
						util_memcpy(&data[DATA_FIELD_BLUE], &Color_Table[Color][0], RBG_LED_PRIMARY_MAX);
					}
				}
				else
				{
					util_memcpy(&data[DATA_FIELD_BLUE], &Off[0], RBG_LED_PRIMARY_MAX);
				}
				
				Activate(data, DATA_FIELD_SIZE);

				mem = mem ? 0 : 1; // toggle
			}
		break;
		
		case RBG_LED_STATUS_RESET : 
		default :
			// Do nothing
		break;
	}
}

/*============================================================================*/
/*                              GLOBAL FUNCTIONS                              */
/*============================================================================*/

void make_float_rgb_led(void)
{
	u8 i;

	for (i = 0; i < CTRL_IO_NUM; i++)
	{
		GPIO_Init(CTRL_IO_PORT[i], CTRL_IO_PIN[i], GPIO_Mode_In_FL_No_IT);
	}
}



/*
================================================================================
   Function name : rgb_led_init()
================================================================================
*/
void rgb_led_init(u8 option)
{	
	u8 i;

	if(RGB_LED_RESET_ALL == option) // 0 includes H/W reset
	{
		for( i = 0; i<CTRL_IO_NUM; i++)
		{
			// Initialize neccessary ST8L151 drivers
			GPIO_Init(CTRL_IO_PORT[i], CTRL_IO_PIN[i], GPIO_Mode_Out_PP_High_Fast);

			// Wait in high
			GPIO_HIGH(CTRL_IO_PORT[i], CTRL_IO_PIN[i]);
		}
	}

	Color 						= RGB_LED_COLOR_CUSTOM;
	Color_physical 				= RGB_LED_COLOR_WHITE;
	
	RGB_LED_bright 				= RGB_LED_BRIGHT_DEFAULT;
	RGB_LED_bright_physical		= 0;
	
	Status 						= RBG_LED_STATUS_OFF;
	Status_physical 			= RBG_LED_STATUS_ON;
	
	Custom_nibble 				= 0;

	// Clear
	Custom[0] = 0; 
	Custom[1] = 0; 
	Custom[2] = 0; 
}

/*
================================================================================
   Function name : rgb_led_get_color()
================================================================================
*/
RGB_LED_COLOR_T rgb_led_get_color(void)
{
	return Color;
}

/*
================================================================================
   Function name : rgb_led_set_color()
================================================================================
*/
void rgb_led_set_color(RGB_LED_COLOR_T color)
{
	assert_param(RGB_LED_COLOR_MAX>color);
	Color = color;
}

/*
================================================================================
   Function name : rgb_led_set_status()
================================================================================
*/
void rgb_led_set_status(RBG_LED_STATUS_T status)
{
	assert_param(RBG_LED_STATUS_MAX>status);

	if(RBG_LED_STATUS_RESET == status)
	{
		rgb_led_init(RGB_LED_RESET_DB);
	}
	else
	{
		Status = status;
	}
}

/*
================================================================================
   Function name : rgb_led_get_status()
================================================================================
*/
RBG_LED_STATUS_T rgb_led_get_status(void)
{
	return Status;
}

/*
================================================================================
   Function name : rgb_led_set_brightness()
================================================================================
*/
void rgb_led_set_brightness(u8 level)
{
	// Used only bit 2, 3, 4 of RGB_LED_bright, bit 0, 1 should be always zero
	level <<= 2; 

	RGB_LED_bright = level;
}

/*
================================================================================
   Function name : rgb_led_set_custom_color()
================================================================================
*/
void rgb_led_set_custom_color(RBG_LED_PRIMARY_T type, u8 value)
{
	assert_param(RBG_LED_PRIMARY_MAX>type);

	// Set high nibble
	if(Custom_nibble)
	{
		// clear bit5 ~ bit7
		Custom[type] &= 0x1F;

		// get bit5 ~ bit7
		Custom[type] |= (value<<5);
	}
	else
	{
		// clear bit2 ~ bit4
		Custom[type] &= 0xE3;

		// get bit2 ~ bit4
		Custom[type] |= (value<<2);
	}
}

/*
================================================================================
   Function name : rgb_led_get_custom_color()
================================================================================
*/
u8 rgb_led_get_custom_color(RBG_LED_PRIMARY_T type)
{
	u8 val;	

	assert_param(RBG_LED_PRIMARY_MAX>type);
	
	if(Custom_nibble)
	{
		// get bit5 ~ bit7
		val = Custom[type] & 0xE0;
		val >>= 5;
	}
	else
	{
		// get bit2 ~ bit4
		val = Custom[type] & 0x1C;
		val >>= 2;
	}
	
	return val;
}
/*
================================================================================
   Function name : rgb_led_set_custom_color_nibble()
================================================================================
*/
void rgb_led_set_custom_color_nibble(u8 value)
{
	Custom_nibble = value ? 1 : 0;
}

/*
================================================================================
   Function name : rgb_led_get_custom_color_nibble()
================================================================================
*/
u8 rgb_led_get_custom_color_nibble(void)
{
	return Custom_nibble;
}

/*
================================================================================
   Function name : rgb_led_test_send()
================================================================================
*/
void rgb_led_test_send(u8 opt)
{
	#if 0
	switch(opt)
	{
		case 0 :
			data[DATA_FIELD_GLOBAL] = 0xE0 | (0x1F & RGB_LED_bright);
			util_memcpy(&data[DATA_FIELD_BLUE], &Off[0], RBG_LED_PRIMARY_MAX);
			Activate(data, DATA_FIELD_SIZE);
		break;

		case 1 :
			data[DATA_FIELD_GLOBAL] = 0xE0 | (0x1F & RGB_LED_bright);
			util_memcpy(&data[DATA_FIELD_BLUE], &Color_Table[RGB_LED_COLOR_ORANGE][0], RBG_LED_PRIMARY_MAX);
			Activate(data, DATA_FIELD_SIZE);
		break;
	}
	#endif
}

/*
================================================================================
   Function name : rgb_led_update()
================================================================================
*/
void rgb_led_update(void)
{
	Update();
}

// End of file 
