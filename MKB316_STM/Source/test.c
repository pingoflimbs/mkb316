/// \file test.c
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 1-Dec-2017  MJ.Kim
* + Created initial version.
*
* 18-Jan-2018  MJ.Kim
* + Removed LED module
*
* 10-Apr-2018  MJ.Kim
* + Added battery test module
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/
/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/
#include "mokibo_config.h"
#include "timer.h"
#include "button.h"
#include "touch.h"
#include "exchange.h"
#include "rgb_led.h"
#include "tsl_user.h"
#include "util.h"
#include "battery.h"
#include "test.h"

#if (MOKIBO_TEST==MOKIBO_CONFIG_ON)
/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/             
#define TEST_TKEY(NB) (MyTKeysB[(NB)].p_Data->StateId == TSL_STATEID_DETECT ||\
                       MyTKeysB[(NB)].p_Data->StateId == TSL_STATEID_DEB_RELEASE_DETECT)

/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/
#define TEST_INTERVAL			500		// 500ms

/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/

/*============================================================================*/
/*                       PROTOTYPES OF LOCAL FUNCTIONS                        */
/*============================================================================*/


/*============================================================================*/
/*                              CLASS VARIABLES                               */
/*============================================================================*/


/*============================================================================*/
/*                              LOCAL FUNCTIONS                               */
/*============================================================================*/

/*============================================================================*/
/*                              GLOBAL FUNCTIONS                              */
/*============================================================================*/
//static BUTTON_STATUS now, prev;
/*
================================================================================
   Function name : test_init()
================================================================================
*/
void test_init(void)
{
	#if (MOKIBO_TARGET==MOKIBO_STMT_8L_EV1)
	#elif (MOKIBO_TARGET==MOKIBO_REAL)
	#else
		#error "What happen!!"
	#endif
	util_enbale_detect_wakeup(1);
}

/*
================================================================================
   Function name : test_update()
================================================================================
*/
void test_update(void)
{
	#if (MOKIBO_TARGET==MOKIBO_STMT_8L_EV1)
	#elif (MOKIBO_TARGET==MOKIBO_REAL)
	//////////////////  Click bar touch test //////////////////////////////////
	if((TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_CENTER_LEFT)))
	{ 
		rgb_led_set_color(RGB_LED_COLOR_BLUE);
	}
	
	if((TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_CENTER_RIGHT)))
	{ 
		rgb_led_set_color(RGB_LED_COLOR_GREEN);
	}
	
	if((TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_CENTER_LEFT)) 
			&& (TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_CENTER_RIGHT)))
	{
		//exchange_set_host_cmd(EXCHANGE_HOST_CMD_RESET);
		rgb_led_set_color(RGB_LED_COLOR_RED);
	}

	if((TOUCH_STATUS_OFF == touch_get(TOUCH_TYPE_CENTER_LEFT)) 
			&& (TOUCH_STATUS_OFF == touch_get(TOUCH_TYPE_CENTER_RIGHT)))
	{ 
		rgb_led_set_color(RGB_LED_COLOR_WHITE);
	}
	//------------------------------------------------------------------------
	if(TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_LEFT) )
	{ 
		rgb_led_set_status(RBG_LED_STATUS_FAST_BLINK);
	}
	
	if(TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_RIGHT)) 
	{ 
		rgb_led_set_status(RBG_LED_STATUS_SLOW_BLINK);
	}

	if((TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_LEFT)) && (TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_RIGHT)))
	{ 
		rgb_led_set_status(RBG_LED_STATUS_DIMMING);
	}

	if((TOUCH_STATUS_OFF == touch_get(TOUCH_TYPE_LEFT)) && (TOUCH_STATUS_OFF == touch_get(TOUCH_TYPE_RIGHT)))
	{ 
		rgb_led_set_status(RBG_LED_STATUS_ON);
	}

	#if 1
	////////////////////////////////////////////////////////////
	if( (BUTTON_STATUS_PRESS == GetClickBarButtonStat(BUTTON_TYPE_LEFT))
			&& (BUTTON_STATUS_PRESS == GetClickBarButtonStat(BUTTON_TYPE_RIGHT)) )
	{
		rgb_led_set_color(RGB_LED_COLOR_RSVD1);
		exchange_set_host_cmd(EXCHANGE_HOST_CMD_RESET);
	}
	#endif	

	#if 0
	// Left button test
	if(BUTTON_STATUS_PRESS == GetClickBarButtonStat(BUTTON_TYPE_LEFT))
	{ 
		rgb_led_set_color(RGB_LED_COLOR_BLUE);
	}
	else
	{
		rgb_led_set_color(RGB_LED_COLOR_GREEN);
	}
	
	// right button test
	if(BUTTON_STATUS_PRESS == GetClickBarButtonStat(BUTTON_TYPE_RIGHT))
	{ 
		rgb_led_set_status(RBG_LED_STATUS_FAST_BLINK);
	}
	else
	{
		rgb_led_set_status(RBG_LED_STATUS_SLOW_BLINK);
	}


	// Left button test
	if(BUTTON_STATUS_PRESS == GetClickBarButtonStat(BUTTON_TYPE_LEFT))
	{ 
		rgb_led_set_color(RGB_LED_COLOR_BLUE);
	}
	else
	{
		rgb_led_set_color(RGB_LED_COLOR_GREEN);
	}
	
	// right button test
	if(BUTTON_STATUS_PRESS == GetClickBarButtonStat(BUTTON_TYPE_RIGHT))
	{ 
		rgb_led_set_status(RBG_LED_STATUS_FAST_BLINK);
	}
	else
	{
		rgb_led_set_status(RBG_LED_STATUS_SLOW_BLINK);
	}

	//////////////////  Battery ADC test //////////////////////////////////
	static u8 high, low;
	
	high = battery_get_level();
	low = battery_get_level();

	if(battery_get_charger_status())
	{ 
		rgb_led_set_color(RGB_LED_COLOR_BLUE);
	}
	else
	{
		rgb_led_set_color(RGB_LED_COLOR_GREEN);
	}

	if(battery_get_ADC())
	{
		rgb_led_set_status(RBG_LED_STATUS_FAST_BLINK);
	}
	else
	{
		rgb_led_set_status(RBG_LED_STATUS_SLOW_BLINK);
	}
	
	battery_update_request();


	//////////////////  Wake up key test     //////////////////////////////////
	// all touch test
	util_keys_info_t input;

	input = util_detect_wakeup();

	if(input.key.fn || input.key.on_off)
	{
		rgb_led_set_color(RGB_LED_COLOR_BLUE);
	}
	else
	{
		rgb_led_set_color(RGB_LED_COLOR_GREEN);
	}
	
	if(input.key.detect)
	{
		rgb_led_set_color(RGB_LED_COLOR_RED);
	}

	if((TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_CENTER_LEFT)))
	{ 
		rgb_led_set_status(RBG_LED_STATUS_FAST_BLINK);
	}
	else
	{
		rgb_led_set_status(RBG_LED_STATUS_SLOW_BLINK);
	}

	//////////////////  Left button test  /////////////////////////////////////
	if((TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_CENTER_LEFT)))
	{ 
		rgb_led_set_color(RGB_LED_COLOR_BLUE);
	}
	
	if((TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_CENTER_RIGHT)))
	{ 
		rgb_led_set_color(RGB_LED_COLOR_GREEN);
	}
	
	if((TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_CENTER_LEFT)) && (TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_CENTER_RIGHT)))
	{ 
		rgb_led_set_color(RGB_LED_COLOR_RED);
	}

	if((TOUCH_STATUS_OFF == touch_get(TOUCH_TYPE_CENTER_LEFT)) && (TOUCH_STATUS_OFF == touch_get(TOUCH_TYPE_CENTER_RIGHT)))
	{ 
		rgb_led_set_color(RGB_LED_COLOR_WHITE);
	}

	///////////////////////////////////////////////////////////////////////
	if((TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_LEFT)))
	{ 
		rgb_led_set_status(RBG_LED_STATUS_SLOW_BLINK);
	}
	
	if((TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_RIGHT)))
	{ 
		rgb_led_set_status(RBG_LED_STATUS_FAST_BLINK);
	}
	
	if((TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_LEFT)) && (TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_RIGHT)))
	{ 
		rgb_led_set_status(RBG_LED_STATUS_DIMMING);
	}

	if((TOUCH_STATUS_OFF == touch_get(TOUCH_TYPE_LEFT)) && (TOUCH_STATUS_OFF == touch_get(TOUCH_TYPE_RIGHT)))
	{ 
		rgb_led_set_status(RBG_LED_STATUS_ON);
	}

	// led status blink touch test
	if( TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_LEFT))
	{ 
		rgb_led_set_color(RGB_LED_COLOR_BLUE);
		rgb_led_set_status(RBG_LED_STATUS_SLOW_BLINK);
	}
	else
	{
		rgb_led_set_color(RGB_LED_COLOR_GREEN);
		rgb_led_set_status(RBG_LED_STATUS_DIMMING);
	}
	if( TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_LEFT))
	{ 
		rgb_led_set_brightness(0x1f);
		//rgb_led_set_color(RGB_LED_COLOR_CUSTOM);
		rgb_led_set_color(RGB_LED_COLOR_WHITE);
	}
	else
	{
		rgb_led_set_brightness(0x1f);
		rgb_led_set_color(RGB_LED_COLOR_GREEN);
	}

	if( TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_RIGHT))
	{ 
		rgb_led_set_status(RBG_LED_STATUS_DIMMING);
	}
	else
	{
		rgb_led_set_status(RBG_LED_STATUS_ON);
	}

	// Left button test
	if( BUTTON_STATUS_PRESS == GetClickBarButtonStat(BUTTON_TYPE_LEFT))
	{ 
		rgb_led_set_color(RGB_LED_COLOR_BLUE);
	}
	else
	{
		rgb_led_set_color(RGB_LED_COLOR_GREEN);
	}
	
	// right button test
	if( BUTTON_STATUS_PRESS == GetClickBarButtonStat(BUTTON_TYPE_RIGHT))
	{ 
		rgb_led_set_color(RGB_LED_COLOR_BLUE);
	}
	else
	{
		rgb_led_set_color(RGB_LED_COLOR_GREEN);
	}
	
	// Left touch test
	if( TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_LEFT))
	{ 
		rgb_led_set_color(RGB_LED_COLOR_BLUE);
	}
	else
	{
		rgb_led_set_color(RGB_LED_COLOR_GREEN);
	}
	// right touch test
	if( TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_RIGHT))
	{ 
		rgb_led_set_color(RGB_LED_COLOR_BLUE);
	}
	else
	{
		rgb_led_set_color(RGB_LED_COLOR_GREEN);
	}
	
	// center left touch test
	if(TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_LEFT))
	{ 
		rgb_led_set_color(RGB_LED_COLOR_BLUE);
	}
	else
	{
		rgb_led_set_color(RGB_LED_COLOR_GREEN);
	}
	
	// WDT test
	if( BUTTON_STATUS_PRESS == GetClickBarButtonStat(BUTTON_TYPE_RIGHT))
	{ 
		exchange_set_host_cmd(EXCHANGE_HOST_CMD_RESET);
	}
	#endif
	#else
		#error "What happen!!"
	#endif // (MOKIBO_TARGET==MOKIBO_STM8L_DISCOVERY)
}

#endif  // End of (MOKIBO_TEST==MOKIBO_CONFIG_ON)

// End of file 
