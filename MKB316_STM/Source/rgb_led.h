/// \file rgb_led.h
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 8-Nov-2017  MJ.Kim
* + Created initial version.
*
* 27-Dec-2017  MJ.Kim
* + Rewrote code 
* + Created rgb_led_set_color()/rgb_led_control()
*
* 28-Dec-2017  MJ.Kim
* + Changed table according to "message.txt version 3.5"
* + Swap RGB_LED_COLOR_ORANGE and RGB_LED_COLOR_WHITE
*
* 20-Feb-2018  MJ.Kim
* + Changed table according to "message.txt version 3.7"
* + Created RGB_LED_COLOR_RSVD1/RGB_LED_COLOR_RSVD2
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/

/*============================================================================*/
/*                            COMPILER DIRECTIVES                             */
/*============================================================================*/
#ifndef __RGB_LED_H
#define __RGB_LED_H

/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/    
#include "mokibo_config.h"
#include "stm8l15x.h"

/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/
#define	RGB_LED_RESET_ALL	0
#define	RGB_LED_RESET_DB	1

/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/

/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/
typedef enum {
    RGB_LED_COLOR_ORANGE,
    RGB_LED_COLOR_RED,
    RGB_LED_COLOR_GREEN,
    RGB_LED_COLOR_BLUE,
    RGB_LED_COLOR_WHITE,
    RGB_LED_COLOR_RSVD1,		// Temp Purple
    RGB_LED_COLOR_RSVD2,		// Temp Yellow
    RGB_LED_COLOR_CUSTOM,		// Do not exceed 7, refer to message.txt
    RGB_LED_COLOR_MAX
} RGB_LED_COLOR_T;

typedef enum {
    RBG_LED_STATUS_OFF,                			// LED off
    RBG_LED_STATUS_ON,             				// LED on
    RBG_LED_STATUS_SLOW_BLINK,              	// LED slow blink
    RBG_LED_STATUS_FAST_BLINK,              	// LED fast blink
    RBG_LED_STATUS_DIMMING,              		// LED Dimming-out
    RBG_LED_STATUS_RESET,              			// LED Reset driver
    RBG_LED_STATUS_MAX
} RBG_LED_STATUS_T;

typedef enum {
    RBG_LED_PRIMARY_RED, 
    RBG_LED_PRIMARY_GREEN, 
    RBG_LED_PRIMARY_BLUE, 
    RBG_LED_PRIMARY_MAX 
} RBG_LED_PRIMARY_T;

/*============================================================================*/
/*                               GLOBAL FUNCTIONS                             */
/*============================================================================*/
void make_float_rgb_led(void);
void rgb_led_init(u8 option);

void rgb_led_set_color(RGB_LED_COLOR_T color);
RGB_LED_COLOR_T rgb_led_get_color(void);

void rgb_led_set_status(RBG_LED_STATUS_T status);
RBG_LED_STATUS_T rgb_led_get_status(void);

void rgb_led_set_custom_color(RBG_LED_PRIMARY_T type, u8 value);
u8 rgb_led_get_custom_color(RBG_LED_PRIMARY_T type);

void rgb_led_set_custom_color_nibble(u8 value);
u8 rgb_led_get_custom_color_nibble(void);

void rgb_led_set_brightness(u8 level);

void rgb_led_update(void);

void rgb_led_test_send(u8 opt);
#endif // __RGB_LED_H 
