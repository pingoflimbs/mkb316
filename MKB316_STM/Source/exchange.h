/// \file exchange.h
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 6-Nov-2017  MJ.Kim
* + Created initial version.
*
* 19-Feb-2018  MJ.Kim
* + Created exchange_get_host_cmd(), exchange_set_host_cmd()
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/

/*============================================================================*/
/*                            COMPILER DIRECTIVES                             */
/*============================================================================*/
#ifndef __EXCHAGE_H
#define __EXCHAGE_H

/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/    
#include "mokibo_config.h"
#include "stm8l15x.h"

/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/
// Refer to message.txt
#define	EXCHANGE_HOST_CMD_SLEEP				0
#define	EXCHANGE_HOST_CMD_RUN				1
#define	EXCHANGE_HOST_CMD_TEST				2
#define	EXCHANGE_HOST_CMD_RESET				3
#define	EXCHANGE_HOST_CMD_INITIIALIZED		7

/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/

/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/

/*============================================================================*/
/*                               GLOBAL FUNCTIONS                             */
/*============================================================================*/
void exchange_init(void);

void exchange_update(void);

void exchange_interrupt_handler(void);

u8 exchange_get_host_cmd(void);

void exchange_set_host_cmd(u8 cmd);

#endif // __EXCHAGE_H
