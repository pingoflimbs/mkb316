/// \file timer.h
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 2-Nov-2017  MJ.Kim
* + Created initial version.
*
* 8-Mar-2018  MJ.Kim
* + Created TIMER_ID_DETECT_WAKE_UP
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/

/*============================================================================*/
/*                            COMPILER DIRECTIVES                             */
/*============================================================================*/
#ifndef __TIMER_H
#define __TIMER_H

/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/    
#include "mokibo_config.h"
#include "stm8l15x.h"

/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/

/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/

/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/
typedef enum {
	TIMER_ID_MAIN,
	TIMER_ID_LED_SLOW_BLNIK,
	TIMER_ID_UTIL,
	TIMER_ID_UTIL_RESET,
	TIMER_ID_TEST,
	TIMER_ID_DETECT_WAKE_UP,
	#if (MOKIBO_TARGET==MOKIBO_STMT_8L_EV1)
	TIMER_ID_DEBUG,
	#endif
	TIMER_ID_MAX
} TIMER_ID_TYPE;

typedef u16  TIMER_TICK;

/*============================================================================*/
/*                               GLOBAL FUNCTIONS                             */
/*============================================================================*/
void timer_init(void);

void timer_register(TIMER_ID_TYPE id, TIMER_TICK cnt);

bool timer_is_expired(TIMER_ID_TYPE id);

void timer_update(void);	// at every 1ms

void timer_cmd(FunctionalState cmd);	// TIM1_Cmd(ENABLE/DISABLE)

void timer_interrupt_handler(void);	// TIM1 interrupt handler


void Timer_Init(void);
void Timer_Init_for_wakeup(void);
void Timer_Init_for_wakeup_disable(void);


#endif // __TIMER_H 
