/// \file util.h
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 2-Nov-2017  MJ.Kim
* + Created initial version.
*
* 19-Feb-2018  MJ.Kim
* + Created util_enbale_detect_wakeup(), util_detect_wakeup()
*
* 20-Feb-2018  MJ.Kim
* + Created util_reload_WDT()
*
* 6-Mar-2018  MJ.Kim
* + Created util_keys_status_t, util_keys_info_t
*
* 8-Mar-2018  MJ.Kim
* + Added started filed in util_keys_status_t
* + Changed util_detect_wakeup() to detect_wakeup()
*
* 14-Mar-2018  MJ.Kim
* + Created util_detect_wakeup() for test module
*
* 11-Apr-2018  MJ.Kim
* + Created util_memcpy(), util_memset()
*
* 13-Apr-2018  MJ.Kim
* + Created util_soft_delay()
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/

/*============================================================================*/
/*                            COMPILER DIRECTIVES                             */
/*============================================================================*/
#ifndef __UTIL_H
#define __UTIL_H

/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/    
#include "mokibo_config.h"
#include "stm8l15x.h"

/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/

/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/

/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/
typedef struct
{
    uint8_t fn 			: 1;	// function key pressed
    uint8_t on_off		: 1;	// on_off key pressed
    uint8_t detect		: 1;	// 최종 Wake up detect 됐을때
    uint8_t started		: 1;	// 맨처음 Sleep모드로 갔을때
    uint8_t rsvd 		: 4;
} util_keys_status_t;

typedef union 
{
    uint8_t 			data;
	util_keys_status_t	key;
} util_keys_info_t;

/*============================================================================*/
/*                               GLOBAL FUNCTIONS                             */
/*============================================================================*/
void util_init(void);
void util_ctrl_reset_host(u8 state);
void util_reset_host(u8 delay_sec);
void util_update(void);
void util_enbale_detect_wakeup(u8 flag);
util_keys_info_t util_detect_wakeup(void);
void util_reload_WDT(void);		// Reload Watch Dog Timer
void util_memcpy(u8 *to, u8* from, u8 size);
void util_memset(u8 *to, u8 val, u8 size);
void util_soft_delay(__IO uint32_t nCount);

#endif // __UTIL_H 

