/// \file button.c
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 2-Nov-2017  MJ.Kim
* + Created initial version.
*
* 27-Nov-2017  MJ.Kim
* + Changed MOKIBO_STM8L_DISCOVERY to Touch EV baord(STMT/8L-EV1)
*
* 1-Dec-2017  MJ.Kim
* + Assign port/pin based on Rev1.02 BLE board
*
* 7-Dec-2017  MJ.Kim
* + Changed left/right pin based on Rev1.02 BLE FPCB design
*
* 12-Dec-2017  MJ.Kim
* + Assign port/pin based on 12 Dec 2012 release version
*
* 28-Dec-2017  MJ.Kim
* + blocked gpio control, H/W is not ready!!!!
*
* 18-Jan-2018  MJ.Kim
* + unblocked gpio control, H/W is ready
*
* 20-Arp-2018  MJ.Kim
* + Changed GPIO based on HW 6.26(FAB10)(Schematic left right reversed!!!)
*
* 8-June-2018  MJ.Kim
* + Changed GPIO based on HW 6.35(FAB11) Second
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/
/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/
#include "mokibo_config.h"
#include "button.h"        
#include "stm8l15x_exti.h"

/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/             
#if (MOKIBO_TARGET==MOKIBO_STMT_8L_EV1)
	#define LEFT_BUTTON_PORT            GPIOE			// Jostick left button
	#define LEFT_BUTTON_PIN             GPIO_Pin_0
	#define RIGHT_BUTTON_PORT           GPIOE			// Jostick right button
	#define RIGHT_BUTTON_PIN            GPIO_Pin_7
#elif (MOKIBO_TARGET==MOKIBO_REAL)
	#define LEFT_BUTTON_PORT            GPIOD			//
	#define LEFT_BUTTON_PIN             GPIO_Pin_0		// 08_PD0
	#define RIGHT_BUTTON_PORT           GPIOD			//
	#define RIGHT_BUTTON_PIN            GPIO_Pin_1		// 09_PD1
#else
	#error "What happen!!"
#endif

//#define GPIO_HIGH(a,b) 		(a->ODR|=b)
//#define GPIO_LOW(a,b)		(a->ODR&=~b)


static GPIO_TypeDef* CLICKBAR_BUTTON_PORT = RIGHT_BUTTON_PORT;	// GPIOD

/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/

/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/

/*============================================================================*/
/*                       PROTOTYPES OF LOCAL FUNCTIONS                        */
/*============================================================================*/
static void Update(void);

/*============================================================================*/
/*                              CLASS VARIABLES                               */
/*============================================================================*/
static GPIO_TypeDef* BUTTON_PORT[BUTTON_TYPE_MAX] =
{
    LEFT_BUTTON_PORT, 
	RIGHT_BUTTON_PORT,
};

static const uint8_t BUTTON_PIN[BUTTON_TYPE_MAX] =
{ 
	LEFT_BUTTON_PIN, 
	RIGHT_BUTTON_PIN,
};

//@near static BUTTON_STATUS Buttons[BUTTON_TYPE_MAX] = {BUTTON_STATUS_RELEASE, };
static BUTTON_STATUS Buttons[BUTTON_TYPE_MAX] = {BUTTON_STATUS_RELEASE, };

/*============================================================================*/
/*                              LOCAL FUNCTIONS                               */
/*============================================================================*/
/*
================================================================================
   Function name : Update()
================================================================================
*/
static void Update(void)
{	
	u16 i;
	
	for(i=0; i<BUTTON_TYPE_MAX; i++)
	{
		Buttons[i] = 
			GPIO_ReadInputDataBit(BUTTON_PORT[i],(GPIO_Pin_TypeDef)BUTTON_PIN[i])
			? BUTTON_STATUS_RELEASE : BUTTON_STATUS_PRESS;
	}
}

/*============================================================================*/
/*                              GLOBAL FUNCTIONS                              */
/*============================================================================*/

/*
================================================================================
   Function name : button_init()
================================================================================
*/
void button_init(void)
{
	u16 i;

	for(i=0; i<BUTTON_TYPE_MAX; i++)
	{
		// Initialize DB
		Buttons[i] = BUTTON_STATUS_RELEASE;
		
		// Initialize neccessary ST8L151 drivers
		GPIO_Init(BUTTON_PORT[i], BUTTON_PIN[i], GPIO_Mode_In_FL_No_IT);
	}
}

/*
================================================================================
   Function name : button_init_for_button_interrupt
================================================================================
*/
void button_init_for_button_interrupt(void)
{
	//
	GPIO_Init(LEFT_BUTTON_PORT, LEFT_BUTTON_PIN, GPIO_Mode_In_FL_IT); // floating + Interrupt
	EXTI_SetPinSensitivity(EXTI_Pin_0, EXTI_Trigger_Rising);
	//
	GPIO_Init(RIGHT_BUTTON_PORT, RIGHT_BUTTON_PIN, GPIO_Mode_In_FL_IT); // floating + Interrupt
	EXTI_SetPinSensitivity(EXTI_Pin_1, EXTI_Trigger_Rising);

}

/*
================================================================================
   Function name : GetClickBarButtonStat()
================================================================================
*/
BUTTON_STATUS GetClickBarButtonStat(BUTTON_TYPE index)
{
	assert_param(BUTTON_TYPE_MAX>index);
	return Buttons[index];
}

/*
================================================================================
   Function name : button_update()
================================================================================
*/
void button_update(void)
{
	Update();
}

// End of file
