/**
  ******************************************************************************
  * @file    STM8L152C6_Ex01\src\tsl_user.c 
  * @author  MCD Application Team
  * @version V1.1.0
  * @date    24-February-2014
  * @brief   Touch-Sensing Application user configuration file.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 
#include "tsl_conf_stm8L.h"
#include "tsl_user.h"

//==============================================================================
// Channels
//==============================================================================
// Channel Source and Configuration: Always placed in ROM
CONST TSL_ChannelSrc_T MyChannels_Src[TSLPRM_TOTAL_CHANNELS] =
{
  { CHANNEL_0_SRC, CHANNEL_0_SAMPLE, CHANNEL_0_CHANNEL },
  { CHANNEL_1_SRC, CHANNEL_1_SAMPLE, CHANNEL_1_CHANNEL },
  { CHANNEL_2_SRC, CHANNEL_2_SAMPLE, CHANNEL_2_CHANNEL },
  { CHANNEL_3_SRC, CHANNEL_3_SAMPLE, CHANNEL_3_CHANNEL },
};

// Channel Destination: Always placed in ROM
CONST TSL_ChannelDest_T MyChannels_Dest[TSLPRM_TOTAL_CHANNELS] =
{
  { CHANNEL_0_DEST },
  { CHANNEL_1_DEST },
  { CHANNEL_2_DEST },
  { CHANNEL_3_DEST },
};

// Channel Data: Always placed in RAM
TSL_ChannelData_T MyChannels_Data[TSLPRM_TOTAL_CHANNELS];

//------
// Banks
//------
#if 1
// Always placed in ROM
CONST TSL_Bank_T MyBanks[TSLPRM_TOTAL_BANKS] =
{
  {&MyChannels_Src[0], &MyChannels_Dest[0], MyChannels_Data, BANK_0_NBCHANNELS, BANK_0_SHIELD_SAMPLE, BANK_0_SHIELD_CHANNEL},
  {&MyChannels_Src[1], &MyChannels_Dest[1], MyChannels_Data, BANK_0_NBCHANNELS, BANK_0_SHIELD_SAMPLE, BANK_0_SHIELD_CHANNEL},
  {&MyChannels_Src[2], &MyChannels_Dest[2], MyChannels_Data, BANK_0_NBCHANNELS, BANK_0_SHIELD_SAMPLE, BANK_0_SHIELD_CHANNEL},
  {&MyChannels_Src[3], &MyChannels_Dest[3], MyChannels_Data, BANK_0_NBCHANNELS, BANK_0_SHIELD_SAMPLE, BANK_0_SHIELD_CHANNEL},
};
#else
CONST TSL_Bank_T MyBanks[TSLPRM_TOTAL_BANKS] =
{
  {&MyChannels_Src[0], &MyChannels_Dest[0], MyChannels_Data, BANK_0_NBCHANNELS, BANK_0_SHIELD_SAMPLE, BANK_0_SHIELD_CHANNEL},
};
#endif

//----------
// TouchKeys
//----------

// TouchKey Data: always placed in RAM (state, counter, flags, ...)
TSL_TouchKeyData_T MyTKeysData[TSLPRM_TOTAL_TKEYS];

// TouchKey Parameters: placed in RAM (thresholds, debounce counters, ...)
TSL_TouchKeyParam_T MyTKeysParams[TSLPRM_TOTAL_TKEYS];

// State Machine (ROM)

void MyTKeys_ErrorStateProcess(void);
void MyTKeys_OffStateProcess(void);

CONST TSL_State_T MyTKeys_StateMachine[] =
{
  // Calibration states
  /*  0 */ { TSL_STATEMASK_CALIB,              TSL_tkey_CalibrationStateProcess },
  /*  1 */ { TSL_STATEMASK_DEB_CALIB,          TSL_tkey_DebCalibrationStateProcess },
  // Release states 
  /*  2 */ { TSL_STATEMASK_RELEASE,            TSL_tkey_ReleaseStateProcess },
#if TSLPRM_USE_PROX > 0
  /*  3 */ { TSL_STATEMASK_DEB_RELEASE_PROX,   TSL_tkey_DebReleaseProxStateProcess },
#else
  /*  3 */ { TSL_STATEMASK_DEB_RELEASE_PROX,   0 },
#endif
  /*  4 */ { TSL_STATEMASK_DEB_RELEASE_DETECT, TSL_tkey_DebReleaseDetectStateProcess },
  /*  5 */ { TSL_STATEMASK_DEB_RELEASE_TOUCH,  TSL_tkey_DebReleaseTouchStateProcess },
#if TSLPRM_USE_PROX > 0
  // Proximity states
  /*  6 */ { TSL_STATEMASK_PROX,               TSL_tkey_ProxStateProcess },
  /*  7 */ { TSL_STATEMASK_DEB_PROX,           TSL_tkey_DebProxStateProcess },
  /*  8 */ { TSL_STATEMASK_DEB_PROX_DETECT,    TSL_tkey_DebProxDetectStateProcess },
  /*  9 */ { TSL_STATEMASK_DEB_PROX_TOUCH,     TSL_tkey_DebProxTouchStateProcess },  
#else
  /*  6 */ { TSL_STATEMASK_PROX,               0 },
  /*  7 */ { TSL_STATEMASK_DEB_PROX,           0 },
  /*  8 */ { TSL_STATEMASK_DEB_PROX_DETECT,    0 },
  /*  9 */ { TSL_STATEMASK_DEB_PROX_TOUCH,     0 },  
#endif
  // Detect states
  /* 10 */ { TSL_STATEMASK_DETECT,             TSL_tkey_DetectStateProcess },
  /* 11 */ { TSL_STATEMASK_DEB_DETECT,         TSL_tkey_DebDetectStateProcess },
  // Touch state
  /* 12 */ { TSL_STATEMASK_TOUCH,              TSL_tkey_TouchStateProcess },
  // Error states
  /* 13 */ { TSL_STATEMASK_ERROR,              MyTKeys_ErrorStateProcess },
  /* 14 */ { TSL_STATEMASK_DEB_ERROR_CALIB,    TSL_tkey_DebErrorStateProcess },
  /* 15 */ { TSL_STATEMASK_DEB_ERROR_RELEASE,  TSL_tkey_DebErrorStateProcess },
  /* 16 */ { TSL_STATEMASK_DEB_ERROR_PROX,     TSL_tkey_DebErrorStateProcess },
  /* 17 */ { TSL_STATEMASK_DEB_ERROR_DETECT,   TSL_tkey_DebErrorStateProcess },
  /* 18 */ { TSL_STATEMASK_DEB_ERROR_TOUCH,    TSL_tkey_DebErrorStateProcess },
  // Other states
  /* 19 */ { TSL_STATEMASK_OFF,                MyTKeys_OffStateProcess }
};

CONST TSL_TouchKeyMethods_T MyTKeys_Methods =
{
  TSL_tkey_Init,
  TSL_tkey_Process
};

// TouchKeys: Always placed in ROM

CONST TSL_TouchKeyB_T MyTKeysB[TSLPRM_TOTAL_TOUCHKEYS_B] =
{
  { &MyTKeysData[0], &MyTKeysParams[0], &MyChannels_Data[CHANNEL_0_DEST]},
  { &MyTKeysData[1], &MyTKeysParams[1], &MyChannels_Data[CHANNEL_1_DEST]},
  { &MyTKeysData[2], &MyTKeysParams[2], &MyChannels_Data[CHANNEL_2_DEST]},
  { &MyTKeysData[3], &MyTKeysParams[3], &MyChannels_Data[CHANNEL_3_DEST]},
};

//----------------
// Generic Objects
//----------------

// Objects: Always placed in ROM
// List (ROM)
CONST TSL_Object_T MyObjects[TSLPRM_TOTAL_OBJECTS] =
{
  { TSL_OBJ_TOUCHKEYB, (TSL_TouchKeyB_T *)&MyTKeysB[0] },
  { TSL_OBJ_TOUCHKEYB, (TSL_TouchKeyB_T *)&MyTKeysB[1] },
  { TSL_OBJ_TOUCHKEYB, (TSL_TouchKeyB_T *)&MyTKeysB[2] },
  { TSL_OBJ_TOUCHKEYB, (TSL_TouchKeyB_T *)&MyTKeysB[3] },
};

// Group of objects: placed in RAM due to state variables
TSL_ObjectGroup_T MyObjGroup = { &MyObjects[0], TSLPRM_TOTAL_OBJECTS, 0x00, TSL_STATE_NOT_CHANGED };

//-------------------------------------------
// TSL Common Parameters placed in RAM or ROM
// --> external declaration in tsl_conf.h
//-------------------------------------------

TSL_Params_T TSL_Params =
{
  TSLPRM_ACQ_MIN,
  TSLPRM_ACQ_MAX,
  TSLPRM_CALIB_SAMPLES,
  TSLPRM_DTO,
#if TSLPRM_TOTAL_TKEYS > 0  
  MyTKeys_StateMachine,   // Default state machine for TKeys
  &MyTKeys_Methods,       // Default methods for TKeys
#endif
#if TSLPRM_TOTAL_LNRTS > 0
  MyLinRots_StateMachine, // Default state machine for LinRots
  &MyLinRots_Methods      // Default methods for LinRots
#endif
};

/* Private functions prototype -----------------------------------------------*/
 void TSL_user_SetThresholds(void);

/* Global variables ----------------------------------------------------------*/
TSL_tTick_ms_T ECS_last_tick; // Hold the last time value for ECS


/**
  * @brief  Initialize the STMTouch Driver
  * @param  None
  * @retval None
  */
void TSL_user_Init(void)
{
  TSL_obj_GroupInit(&MyObjGroup); // Init Objects
  
  TSL_Init(MyBanks); // Init timing and acquisition modules

  TSL_user_SetThresholds(); // Init thresholds for each object individually
}


/**
  * @brief  Execute STMTouch Driver main State machine
  * @param  None
  * @retval status Return TSL_STATUS_OK when ALL banks acquisitions are done
  */
TSL_Status_enum_T TSL_user_Action(void)
{
  static uint8_t idx_bank = 0;
  static uint8_t config_done = 0;
  TSL_Status_enum_T status;
  
  if (!config_done)
  {
    // Configure Bank
    TSL_acq_BankConfig(idx_bank);
 
    // Start Bank acquisition
    TSL_acq_BankStartAcq();
    
    // Set flag
    config_done = 1;
  }
  
  // Check Bank End of Acquisition (Count)
  if (TSL_acq_BankWaitEOC() == TSL_STATUS_OK)
  {
    STMSTUDIO_LOCK;
    TSL_acq_BankGetResult(idx_bank, 0, 0); // Get Bank Result
    STMSTUDIO_UNLOCK;
    config_done = 0; // To restart a new bank configuration
    idx_bank++;
  }
  
  if (idx_bank == TSLPRM_TOTAL_BANKS) // All banks have been acquired
  {
    idx_bank = 0; // To restart again with the first bank
    
    // Process Objects
    TSL_obj_GroupProcess(&MyObjGroup);
    
    // DxS processing
    // Don't forget to set also the TSLPRM_USE_DXS parameter in the tsl_conf_stm8l.h file
    TSL_dxs_FirstObj(&MyObjGroup);
   
    // ECS every 100ms
    if (TSL_tim_CheckDelay_ms(100, &ECS_last_tick) == TSL_STATUS_OK)
    {
      TSL_ecs_Process(&MyObjGroup);
    }
   
    status = TSL_STATUS_OK; // All banks have been acquired and sensors processed
  }
  else
  {
    status = TSL_STATUS_BUSY;
  }

  return status;
}


/**
  * @brief  Set thresholds for each object (optional).
  * @param  None
  * @retval None
  */
void TSL_user_SetThresholds(void)
{
#ifndef BILLY // -- original
	// K0 thresholds
	#if TSLPRM_USE_PROX > 1
	  MyTKeysB[0].p_Param->ProxInTh = 10;
	  MyTKeysB[0].p_Param->ProxOutTh = 5; 
	#endif  
	  MyTKeysB[0].p_Param->DetectInTh = 25;
	  MyTKeysB[0].p_Param->DetectOutTh = 20;
	  MyTKeysB[0].p_Param->CalibTh = 25;
#else
	//Billy
	// K0 : Right /6.51
	#if TSLPRM_USE_PROX > 1
	  MyTKeysB[0].p_Param->ProxInTh = 10; 		//10
	  MyTKeysB[0].p_Param->ProxOutTh = 5; 		//5
	#endif  
	  MyTKeysB[0].p_Param->DetectInTh = 17;		//30; 	//8
	  MyTKeysB[0].p_Param->DetectOutTh = 12;	//25;		//4
	  MyTKeysB[0].p_Param->CalibTh = 15;			//30; 	//3
	// LEFT /6.51
	#if TSLPRM_USE_PROX > 1
	  MyTKeysB[1].p_Param->ProxInTh = 10;			//10
	  MyTKeysB[1].p_Param->ProxOutTh = 5; 		//5
	#endif  
	  MyTKeysB[1].p_Param->DetectInTh = 22;		//20; 	//10
	  MyTKeysB[1].p_Param->DetectOutTh = 17;	//15; 	//5
	  MyTKeysB[1].p_Param->CalibTh = 20; 			//4
	// Center+Left
	#if TSLPRM_USE_PROX > 1
	  MyTKeysB[2].p_Param->ProxInTh = 30;			//80
	  MyTKeysB[2].p_Param->ProxOutTh = 25; 		//70
	#endif  
	  MyTKeysB[2].p_Param->DetectInTh = 55;		//40;		//100
	  MyTKeysB[2].p_Param->DetectOutTh = 45;	//35;		//90
	  MyTKeysB[2].p_Param->CalibTh = 50;			//25;		//100
	// Center-Right
	#if TSLPRM_USE_PROX > 1
	  MyTKeysB[3].p_Param->ProxInTh = 30;			//80
	  MyTKeysB[3].p_Param->ProxOutTh = 25; 		//70
	#endif  
	  MyTKeysB[3].p_Param->DetectInTh = 55;		//40;		//100
	  MyTKeysB[3].p_Param->DetectOutTh = 45;	//35;		//90
	  MyTKeysB[3].p_Param->CalibTh = 50;			//25;		//100
#endif


			
  // K9 thresholds
#if TSLPRM_USE_PROX > 1
  MyTKeysB[9].p_Param->ProxInTh = 10;
  MyTKeysB[9].p_Param->ProxOutTh = 5;
#endif  
  MyTKeysB[9].p_Param->DetectInTh = 25;
  MyTKeysB[9].p_Param->DetectOutTh = 20;
  MyTKeysB[9].p_Param->CalibTh = 25;
}


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/