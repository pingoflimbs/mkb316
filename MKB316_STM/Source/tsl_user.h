/**
  ******************************************************************************
  * @file    STM8L152C6_Ex01\inc\tsl_user.h
  * @author  MCD Application Team
  * @version V1.1.0
  * @date    24-February-2014
  * @brief   Touch-Sensing Application user configuration file.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/// \file tsl_user.h
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2018
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 8-June-2018  MJ.Kim
* + Changed GPIO based on HW 6.35(FAB11) Second
*
* 8-June-2018  MJ.Kim
* + Changed CHANNEL_0_CHANNEL/CHANNEL_1_CHANNEL according to REAL TAERET
* + Schematic HW 6.35(FAB11) should be revised
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TSL_USER_H
#define __TSL_USER_H

#include "tsl.h"

// STMStudio software usage (0=No, 1=Yes)
// The low-power mode must be disabled when STMStudio is used.
#define USE_STMSTUDIO (0)

#if USE_STMSTUDIO > 0
#include "stmCriticalSection.h"
#define STMSTUDIO_LOCK {enterLock();}
#define STMSTUDIO_UNLOCK {exitLock();}
#else
#define STMSTUDIO_LOCK
#define STMSTUDIO_UNLOCK
#endif


//==============================================================================
// Channel IOs definition
//==============================================================================
#if 1 
	///////////// MJ.KIM //////////////////
	// K0 LEFT TOUCH
	#define CHANNEL_0_SRC       ((uint8_t)(GR5))
	#define CHANNEL_0_DEST      (0)
	#define CHANNEL_0_SAMPLE    (PB4)
	#define CHANNEL_0_CHANNEL   (PB6)
	
	// K1 RIGHT TOUCH
	#define CHANNEL_1_SRC       ((uint8_t)(GR5))
	#define CHANNEL_1_DEST      (1)
	#define CHANNEL_1_SAMPLE    (PB4)
	#define CHANNEL_1_CHANNEL   (PB5)
	
	// K2 CENTER Left
	#define CHANNEL_2_SRC       ((uint8_t)(GR6))
	#define CHANNEL_2_DEST      (2)
	#define CHANNEL_2_SAMPLE    (PB1)
	#define CHANNEL_2_CHANNEL   (PB2)
	
	// K3 CENTER Right
	#define CHANNEL_3_SRC       ((uint8_t)(GR6))
	#define CHANNEL_3_DEST      (3)
	#define CHANNEL_3_SAMPLE    (PB1)
	#define CHANNEL_3_CHANNEL   (PB3)

#else
	// K0
	#define CHANNEL_0_SRC       ((uint8_t)(GR1))
	#define CHANNEL_0_DEST      (0)
	#define CHANNEL_0_SAMPLE    (PA5)
	#define CHANNEL_0_CHANNEL   (PA4)
	
	// K1
	#define CHANNEL_1_SRC       ((uint8_t)(GR1))
	#define CHANNEL_1_DEST      (1)
	#define CHANNEL_1_SAMPLE    (PA5)
	#define CHANNEL_1_CHANNEL   (PA6)
	
	// K2
	#define CHANNEL_2_SRC       ((uint8_t)(GR8))
	#define CHANNEL_2_DEST      (2)
	#define CHANNEL_2_SAMPLE    (PA5)
	#define CHANNEL_2_CHANNEL   (PE5)
	
	// K3
	#define CHANNEL_3_SRC       ((uint8_t)(GR8))
	#define CHANNEL_3_DEST      (3)
	#define CHANNEL_3_SAMPLE    (PD0)
	#define CHANNEL_3_CHANNEL   (PD1)
	
	// K4
	#define CHANNEL_4_SRC       ((uint8_t)(GR7))
	#define CHANNEL_4_DEST      (4)
	#define CHANNEL_4_SAMPLE    (PD3)
	#define CHANNEL_4_CHANNEL   (PD2)
	
	// K5
	#define CHANNEL_5_SRC       ((uint8_t)(GR7))
	#define CHANNEL_5_DEST      (5)
	#define CHANNEL_5_SAMPLE    (PD3)
	#define CHANNEL_5_CHANNEL   (PB0)
	
	// K6
	#define CHANNEL_6_SRC       ((uint8_t)(GR6))
	#define CHANNEL_6_DEST      (6)
	#define CHANNEL_6_SAMPLE    (PB2)
	#define CHANNEL_6_CHANNEL   (PB1)
	
	// K7
	#define CHANNEL_7_SRC       ((uint8_t)(GR6))
	#define CHANNEL_7_DEST      (7)
	#define CHANNEL_7_SAMPLE    (PB2)
	#define CHANNEL_7_CHANNEL   (PB3)
	
	// K8
	#define CHANNEL_8_SRC       ((uint8_t)(GR5))
	#define CHANNEL_8_DEST      (8)
	#define CHANNEL_8_SAMPLE    (PB5)
	#define CHANNEL_8_CHANNEL   (PB4)
	
	// K9
	#define CHANNEL_9_SRC       ((uint8_t)(GR5))
	#define CHANNEL_9_DEST      (9)
	#define CHANNEL_9_SAMPLE    (PB5)
	#define CHANNEL_9_CHANNEL   (PB6)
#endif //////////////////////////////////////////////////////////////// MJ.KIM

//==============================================================================
// Banks definition
//==============================================================================

#define BANK_0_NBCHANNELS     (1)
#define BANK_0_SHIELD_SAMPLE  (0)
#define BANK_0_SHIELD_CHANNEL (0)

#define BANK_1_NBCHANNELS     (1)
#define BANK_1_SHIELD_SAMPLE  (0)
#define BANK_1_SHIELD_CHANNEL (0)

//==============================================================================
// External declarations
//==============================================================================

extern CONST TSL_Bank_T MyBanks[];
extern CONST TSL_TouchKeyB_T MyTKeysB[];
extern CONST TSL_Object_T MyObjects[];
extern TSL_ObjectGroup_T MyObjGroup;

void MyTKeys_ErrorStateProcess(void);
void MyTKeys_OffStateProcess(void);

void TSL_user_Init(void);
TSL_Status_enum_T TSL_user_Action(void);

#endif /* __TSL_USER_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
