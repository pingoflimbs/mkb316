/// \file touch.h
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 2-Nov-2017  MJ.Kim
* + Created initial version.
*
* 11-Dec-2017  MJ.Kim
* + Changed TOUCH_STATUS_OFF value from 1 to 0
*
* 18-Jan-2018  MJ.Kim
* + Removed TOUCH_TYPE_CENTER
* + Created TOUCH_TYPE_CENTER_LEFT, TOUCH_TYPE_CENTER_RIGHT
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/

/*============================================================================*/
/*                            COMPILER DIRECTIVES                             */
/*============================================================================*/
#ifndef __TOUCH_H
#define __TOUCH_H

/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/    
#include "mokibo_config.h"
#include "stm8l15x.h"

/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/

/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/

/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/
typedef enum {
    TOUCH_TYPE_LEFT = 0,           			// TOUCH LEFT
    TOUCH_TYPE_RIGHT,          				// TOUCH RIGHT
    TOUCH_TYPE_CENTER_LEFT,        			// TOUCH CENTER_LEFT
    TOUCH_TYPE_CENTER_RIGHT,       			// TOUCH CENTER_RIGHT
    TOUCH_TYPE_MAX          				// TOUCH Number
} TOUCH_TYPE;

typedef enum {
    TOUCH_STATUS_OFF = 0,             		// TOUCH OFF
    TOUCH_STATUS_ON,            			// TOUCH ON
    TOUCH_STATUS_MAX
} TOUCH_STATUS;

/*============================================================================*/
/*                               GLOBAL FUNCTIONS                             */
/*============================================================================*/
void touch_init(void);

TOUCH_STATUS touch_get(TOUCH_TYPE touch);

u8 touch_update(void);

#endif // __TOUCH_H 
