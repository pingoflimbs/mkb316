/*	BASIC INTERRUPT VECTOR TABLE FOR STM8 devices
 *	Copyright (c) 2007 STMicroelectronics
 */
#include "mokibo_config.h"
#include "tsl_time_stm8l.h"
#include "tsl_time.h"
#include "stm8l15x_exti.h"

extern void _stext();     /* startup routine */
extern void timer_interrupt_handler(void);
extern void exchange_interrupt_handler(void);

typedef void @far (*interrupt_handler_t)(void);

struct interrupt_vector {
	unsigned char interrupt_instruction;
	interrupt_handler_t interrupt_handler;
};


@far @interrupt void NonHandledInterrupt (void)
{
	/* in order to detect unexpected events during development, 
	   it is recommended to set a breakpoint on the following instruction
	*/
	return;
}

@far @interrupt void MokiboTimerInterrupt (void)
{
	/* in order to detect unexpected events during development, 
	   it is recommended to set a breakpoint on the following instruction
	*/
	timer_interrupt_handler();

	return;
}

@far @interrupt void MokiboTouchTimerInterrupt(void)
{
	/* in order to detect unexpected events during development, 
	   it is recommended to set a breakpoint on the following instruction
	*/
	TIM4->SR1 &= (uint8_t)(~TIM4_SR1_UIF);
  	TSL_tim_ProcessIT();

	return;
}

@far @interrupt void MokiboI2CInterrupt (void)
{
	/* in order to detect unexpected events during development, 
	   it is recommended to set a breakpoint on the following instruction
	*/
	exchange_interrupt_handler();

	return;
}

@far @interrupt void MokiboRTCInterrupt(void)
{
	/* in order to detect unexpected events during development,
	   it is recommended to set a breakpoint on the following instruction
	*/

	/* Clear the periodic wakeup unit flag */
	RTC_ClearITPendingBit(RTC_IT_WUT);

	return;
}

@far @interrupt void ClickbarLeftButtonInterrupt(void)
{
	// button_init_for_button_interrupt();
	// Clickbar Leftt Button : PortD Pin0 
	// Clickbar Right Button : PortD Pin1 
	// _Pin0/1/2/3/4/5/6/7, _PortB/D/E/F/G/H
	EXTI_ClearITPendingBit(EXTI_IT_Pin0);
	//
	return;
}

@far @interrupt void ClickbarRightButtonInterrupt(void)
{
	// button_init_for_button_interrupt();
	// Clickbar Leftt Button : PortD Pin0 
	// Clickbar Right Button : PortD Pin1 
	// _Pin0/1/2/3/4/5/6/7, _PortB/D/E/F/G/H
	EXTI_ClearITPendingBit(EXTI_IT_Pin1);
	//
	return;
}

struct interrupt_vector const _vectab[] = {
	
	{0x82, (interrupt_handler_t)_stext}, /* reset */

	{0x82, NonHandledInterrupt}, /* trap  */

	{0x82, NonHandledInterrupt}, /* irq0  */
	{0x82, NonHandledInterrupt}, /* irq1  */
	{0x82, NonHandledInterrupt}, /* irq2  */
	{0x82, NonHandledInterrupt}, /* irq3  */
	//
	{0x82, MokiboRTCInterrupt}, /* irq4  */
	//
	{0x82, NonHandledInterrupt}, /* irq5  */
	{0x82, NonHandledInterrupt}, /* irq6  */
	{0x82, NonHandledInterrupt}, /* irq7  */
	//
	{0x82, ClickbarLeftButtonInterrupt},  /* irq8  */ /* EXTI 0 */
	{0x82, ClickbarRightButtonInterrupt}, /* irq9  */ /* EXTI 1 */
	//
	{0x82, NonHandledInterrupt}, /* irq10 */
	{0x82, NonHandledInterrupt}, /* irq11 */
	{0x82, NonHandledInterrupt}, /* irq12 */
	{0x82, NonHandledInterrupt}, /* irq13 */
	{0x82, NonHandledInterrupt}, /* irq14 */
	{0x82, NonHandledInterrupt}, /* irq15 */
	{0x82, NonHandledInterrupt}, /* irq16 */
	{0x82, NonHandledInterrupt}, /* irq17 */
	{0x82, NonHandledInterrupt}, /* irq18 */
	{0x82, NonHandledInterrupt}, /* irq19 */
	{0x82, NonHandledInterrupt}, /* irq20 */
	{0x82, NonHandledInterrupt}, /* irq21 */
	{0x82, NonHandledInterrupt}, /* irq22 */
	
	{0x82, MokiboTimerInterrupt}, /* irq23 */

	{0x82, NonHandledInterrupt}, /* irq24 */

	{0x82, MokiboTouchTimerInterrupt}, /* irq25 */

	{0x82, NonHandledInterrupt}, /*irq26 */
	{0x82, NonHandledInterrupt}, /* irq27 */
	{0x82, NonHandledInterrupt}, /* irq28 */

	{0x82, MokiboI2CInterrupt}, /* irq29 */
};

