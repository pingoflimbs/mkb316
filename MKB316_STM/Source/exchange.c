/// \file exchange.c
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 6-Nov-2017  MJ.Kim
* + Created initial version.
*
* 6-Dec-2017  MJ.Kim
* + Corrected mask bit in Copy the data field
* + Corrected i2c interrupt handler
*
* 7-Dec-2017  MJ.Kim
* + Added cmd "9 Host mcu reset" based on version: 3.2
*
* 7-Dec-2017  MJ.Kim
* + Added cmd "10 LED_CENTER_BR" based on version: 3.3
*
* 26-Dec-2017  MJ.Kim
* + Updated I2C register implementation based on version: 3.4
*
* 28-Dec-2017  MJ.Kim
* + Corrected REQ_READ_TOUCH_RIGHT_STATUS
*
* 18-Jan-2018  MJ.Kim
* + Implemented REQ_READ_TOUCH_CENTER_RIGHT_STATUS
* + Removed LED module
*
* 19-Feb-2018  MJ.Kim
* + Created exchange_get_host_cmd(), exchange_set_host_cmd()
*
* 20-Feb-2018  MJ.Kim
* + Changed according to "message.txt version 3.7"
*
* 10-Apr-2018  MJ.Kim
* + Changed according to "message.txt version 3.8"
*
* 11-Apr-2018  MJ.Kim
* + Changed according to "message.txt version 3.9"
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/
/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/
#include "mokibo_config.h"
#include "button.h"
#include "touch.h"
#include "rgb_led.h"
#include "util.h"
#include "battery.h"
#include "exchange.h"        

/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/             

/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/
#if (MOKIBO_TARGET==MOKIBO_STMT_8L_EV1)
#elif (MOKIBO_TARGET==MOKIBO_REAL)
#else
	#error "What happen!!"
#endif

#define SLAVE_ADDRESS 		0x30
#define FREQUENCY 			400000	// 400K

#define REQ_READ			0
#define REQ_WRITE			1

/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/
// Refer to "messagex.txt" in source folder
// Be carefurl this type should not exceed 15
typedef enum
{
	REQ_WRITE_NOP   					= 0,
 	REQ_WRITE_FW_CMD					= 1,
 	REQ_WRITE_LED_CUSTOM_RED_WRITE		= 2,
 	REQ_WRITE_LED_CUSTOM_GREEN_WRITE	= 3,
 	REQ_WRITE_LED_CUSTOM_BLUE_WRITE		= 4,
 	REQ_WRITE_LED_CUSTOM_NIBBLE_WRITE	= 5,
 	REQ_WRITE_LED_COLOR_WRITE			= 6,
 	REQ_WRITE_LED_STATUS_WRITE			= 7,
 	REQ_WRITE_HOST_MCU_RESET 			= 8,
 	REQ_WRITE_LED_CENTER_BR 			= 9,
 	REQ_WRITE_BATTERY_LEVEL_UPDATE 		= 10
} REQ_WRITE_TypeDef;

//// Be carefurl this type should not exceed 15
typedef enum
{
 	REQ_READ_VERSION					= 0,
 	REQ_READ_FW_STATUS					= 1,
 	REQ_READ_LED_CUSTOM_RED_READ		= 2,
 	REQ_READ_LED_CUSTOM_GREEN_READ		= 3,
 	REQ_READ_LED_CUSTOM_BLUE_READ		= 4,
 	REQ_READ_LED_CUSTOM_NIBBLE_READ		= 5,
 	REQ_READ_LED_COLOR_READ				= 6,
 	REQ_READ_LED_STATUS_READ			= 7,
 	REQ_READ_TOUCH_CENTER_LEFT_STATUS	= 8,
 	REQ_READ_TOUCH_CENTER_RIGHT_STATUS	= 9,
 	REQ_READ_TOUCH_LEFT_STATUS			= 10,
 	REQ_READ_TOUCH_RIGHT_STATUS			= 11,
 	REQ_READ_BUTTON_LEFT_STATUS			= 12,
 	REQ_READ_BUTTON_RIGHT_STATUS		= 13, 
 	REQ_READ_BATTERY_LEVEL_READ			= 14,	
 	REQ_READ_BATTERY_STATUS				= 15	
} REQ_READ_TypeDef;

/*============================================================================*/
/*                       PROTOTYPES OF LOCAL FUNCTIONS                        */
/*============================================================================*/
static void Update(void);

/*============================================================================*/
/*                              CLASS VARIABLES                               */
/*============================================================================*/
static __IO uint16_t I2C_Event = 0x00;

//@near static u8 Request = 0;		// Requested data from master
//@near static u8 Response = 0;		// Response data to master
static u8 Request = 0;		// Requested data from master
static u8 Response = 0;		// Response data to master
static u8 Host_cmd = EXCHANGE_HOST_CMD_INITIIALIZED;	

/*============================================================================*/
/*                              LOCAL FUNCTIONS                               */
/*============================================================================*/
/*
================================================================================
   Function name : Update()
================================================================================
*/
static void Update(void)
{
	;
}

// For debugging purpose only !  by Stanley
//#define	READ_CLICKBARTOUCH_AS_IS

/*
================================================================================
   Function name : Process()
================================================================================
*/
static void Process(u8 req)
{
	u8 cmd, data, rw;
	battery_status_t rv;

	rw  	= (req & 0x80) ? REQ_WRITE : REQ_READ;
	cmd  	= ((req & 0x78) >> 3); 	// Get command

	#if (MOKIBO_TEST==MOKIBO_CONFIG_OFF)
	if(REQ_WRITE == rw)
	{
		data = (req&0x07); 			// Get data
		
		switch(cmd)
		{
			case REQ_WRITE_NOP :
				break;
			
			case REQ_WRITE_FW_CMD : 
				exchange_set_host_cmd(data);
				break;

			case REQ_WRITE_LED_CUSTOM_RED_WRITE : 
				rgb_led_set_custom_color(RBG_LED_PRIMARY_RED, data);
				break;

			case REQ_WRITE_LED_CUSTOM_GREEN_WRITE : 
				rgb_led_set_custom_color(RBG_LED_PRIMARY_GREEN, data);
				break;

			case REQ_WRITE_LED_CUSTOM_BLUE_WRITE : 
				rgb_led_set_custom_color(RBG_LED_PRIMARY_BLUE, data);
				break;

			case REQ_WRITE_LED_CUSTOM_NIBBLE_WRITE : 
				rgb_led_set_custom_color_nibble(data);
				break;

			case REQ_WRITE_LED_COLOR_WRITE : 
				rgb_led_set_color(data);
				break;
			
			case REQ_WRITE_LED_STATUS_WRITE : 
				rgb_led_set_status(data);
				break;
			
			case REQ_WRITE_HOST_MCU_RESET : 
				util_reset_host(data);
				break;
			
			case REQ_WRITE_BATTERY_LEVEL_UPDATE : 
				battery_update_request();
				break;
			
			default :
				break;
		}
	}
	else // (REQ_READ == rw)
	{
		data = 0;
		
		switch(cmd)
		{
			case REQ_READ_VERSION :
				data = MOKIBO_ST8L_SW_VER;
				break;
			
			case REQ_READ_FW_STATUS :
				data = Host_cmd;
				break;
			
			case REQ_READ_LED_CUSTOM_RED_READ :
				data = rgb_led_get_custom_color(RBG_LED_PRIMARY_RED);
				break;
			
			case REQ_READ_LED_CUSTOM_GREEN_READ :
				data = rgb_led_get_custom_color(RBG_LED_PRIMARY_GREEN);
				break;
			
			case REQ_READ_LED_CUSTOM_BLUE_READ :
				data = rgb_led_get_custom_color(RBG_LED_PRIMARY_BLUE);
				break;
			
			case REQ_READ_LED_CUSTOM_NIBBLE_READ :
				data = rgb_led_get_custom_color_nibble();
				break;
			
			case REQ_READ_LED_COLOR_READ :
				data = rgb_led_get_color();
				break;
			
			case REQ_READ_LED_STATUS_READ :
				data = rgb_led_get_status();
				break;

#ifndef READ_CLICKBARTOUCH_AS_IS
			//------------------------------------------------------------------------------
			case REQ_READ_TOUCH_CENTER_LEFT_STATUS :
				data = (TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_CENTER_LEFT)) ? 1 : 0;
				break;
			
			case REQ_READ_TOUCH_CENTER_RIGHT_STATUS :
				data = (TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_CENTER_RIGHT)) ? 1 : 0;
				break;
			//------------------------------------------------------------------------------
			case REQ_READ_TOUCH_LEFT_STATUS :
				data = (TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_LEFT)) ? 1 : 0;
				break;
			
			case REQ_READ_TOUCH_RIGHT_STATUS :
				data = (TOUCH_STATUS_ON == touch_get(TOUCH_TYPE_RIGHT)) ? 1 : 0;
				break;
			//------------------------------------------------------------------------------
			case REQ_READ_BUTTON_LEFT_STATUS :
				data = (BUTTON_STATUS_PRESS == GetClickBarButtonStat(BUTTON_TYPE_LEFT)) ? 1 : 0;
				break;
			
			case REQ_READ_BUTTON_RIGHT_STATUS :
				data = (BUTTON_STATUS_PRESS == GetClickBarButtonStat(BUTTON_TYPE_RIGHT)) ? 1 : 0;
				break;
			//------------------------------------------------------------------------------
#else
			//------------------------------------------------------------------------------
			case REQ_READ_TOUCH_CENTER_LEFT_STATUS :
				data = touch_get(TOUCH_TYPE_CENTER_LEFT);
				break;
			
			case REQ_READ_TOUCH_CENTER_RIGHT_STATUS :
				data = touch_get(TOUCH_TYPE_CENTER_RIGHT);
				break;
			//------------------------------------------------------------------------------
			case REQ_READ_TOUCH_LEFT_STATUS :
				data = touch_get(TOUCH_TYPE_LEFT);
				break;
			
			case REQ_READ_TOUCH_RIGHT_STATUS :
				data = touch_get(TOUCH_TYPE_RIGHT);
				break;
			//------------------------------------------------------------------------------
			case REQ_READ_BUTTON_LEFT_STATUS :
				data = touch_get(BUTTON_TYPE_LEFT);
				break;
			
			case REQ_READ_BUTTON_RIGHT_STATUS :
				data = touch_get(BUTTON_TYPE_RIGHT);
				break;
			//------------------------------------------------------------------------------
#endif//READ_CLICKBARTOUCH_AS_IS

			case REQ_READ_BATTERY_LEVEL_READ :
				data = battery_get_level();
				break;

 			case REQ_READ_BATTERY_STATUS :
				rv = battery_get_status();
				util_memcpy(&data, (u8 *)&rv, sizeof(data));
				break;

			default :
				break;
		}

		if((REQ_READ_VERSION == cmd) || (REQ_READ_BATTERY_LEVEL_READ == cmd))
		{
			// <-- (battery level과 version 정보는, 1-byte 그대로 전달된다)
			Response = data;
		}
		else
		{
			Response = (req&0xF8); 		// Copy the RW & command field (상위 5-bits)
			Response |= (data&0x07);	// Copy the data field (하위 3-bits)
		}
	}
	#endif
}

/*
================================================================================
   Function name : mokibo_I2C_init()
================================================================================
*/
static void mokibo_I2C_init(void)
{
	/* I2C  clock Enable*/
  	CLK_PeripheralClockConfig(CLK_Peripheral_I2C1, ENABLE);

	I2C_DeInit(I2C1);

  	I2C_Init(I2C1, FREQUENCY, SLAVE_ADDRESS,
           I2C_Mode_I2C, I2C_DutyCycle_2,
           I2C_Ack_Enable, I2C_AcknowledgedAddress_7bit);

  	/* Enable Error Interrupt*/
  	I2C_ITConfig(I2C1, (I2C_IT_TypeDef)(I2C_IT_ERR | I2C_IT_EVT | I2C_IT_BUF), ENABLE);
}

/*============================================================================*/
/*                              GLOBAL FUNCTIONS                              */
/*============================================================================*/
/*
================================================================================
   Function name : exchange_init()
================================================================================
*/
void exchange_init(void)
{
	Request 	= 0;
	Response 	= 0;
	
	mokibo_I2C_init();
}

/*
================================================================================
   Function name : exchange_update()
================================================================================
*/
void exchange_update(void)
{
	// TODO: Is neceesary?
	Update();
}

/*
================================================================================
   Function name : exchange_get_host_cmd()
================================================================================
*/
u8 exchange_get_host_cmd(void)
{
	return Host_cmd;
}

/*
================================================================================
   Function name : exchange_set_host_cmd()
================================================================================
*/
// 
void exchange_set_host_cmd(u8 cmd)
{
	if(Host_cmd != cmd)
	{
		// Non sleep --> Sleep mode 변경될때
		if(EXCHANGE_HOST_CMD_SLEEP == cmd)
		{
			// wakeup
			util_enbale_detect_wakeup(1);
			rgb_led_set_color(RGB_LED_COLOR_ORANGE);
		}

		// Sleep --> Non sleep mode 변경될때
		if(EXCHANGE_HOST_CMD_SLEEP != cmd)
		{
			util_enbale_detect_wakeup(0);
		}

		Host_cmd = cmd;
	}
}


/*
================================================================================
   Function name : exchange_interrupt_handler()
================================================================================
*/
void exchange_interrupt_handler(void)
{
	static u8 sr1, sr2, sr3;

	// save the I2C registers configuration
	sr1 = I2C1->SR1;
	sr2 = I2C1->SR2;
	sr3 = I2C1->SR3;

	/* Communication error? */
	if (sr2 & (I2C_SR2_WUFH | I2C_SR2_OVR |I2C_SR2_ARLO |I2C_SR2_BERR))
	{		
		I2C1->CR2|= I2C_CR2_STOP;  // stop communication - release the lines
		I2C1->SR2= 0;					    // clear all error flags
	}

	/* More bytes received ? */
	if ((sr1 & (I2C_SR1_RXNE | I2C_SR1_BTF)) == (I2C_SR1_RXNE | I2C_SR1_BTF))
	{
		Request = I2C_ReceiveData(I2C1);
		Process(Request);
	}

	/* Byte received ? */
	if (sr1 & I2C_SR1_RXNE)
	{
		Request = I2C_ReceiveData(I2C1);
		Process(Request);
	}

	/* NAK? (=end of slave transmit comm) */
	if (sr2 & I2C_SR2_AF)
	{	
		I2C1->SR2 &= ~I2C_SR2_AF;	  // clear AF
	}

	/* Stop bit from Master  (= end of slave receive comm) */
	if (sr1 & I2C_SR1_STOPF) 
	{
		I2C1->CR2 |= I2C_CR2_ACK;	  // CR2 write to clear STOPF
	}

	/* Slave address matched (= Start Comm) */
	if (sr1 & I2C_SR1_ADDR)
	{
		;
	}

	/* More bytes to transmit ? */
	if ((sr1 & (I2C_SR1_TXE | I2C_SR1_BTF)) == (I2C_SR1_TXE | I2C_SR1_BTF))
	{
		I2C1->DR = Response;
	}

	/* Byte to transmit ? */
	if (sr1 & I2C_SR1_TXE)
	{
		I2C1->DR = Response;
	}	
}

// End of file 
