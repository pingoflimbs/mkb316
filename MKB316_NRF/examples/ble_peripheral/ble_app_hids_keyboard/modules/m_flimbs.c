#include "sdk_config.h" // mokibo_config.h && sdk_config.h
#include "m_flimbs.h"
#include "m_timer.h"


//============================================================================*/
//      FLIMBS_20181210_POWERONOFF_LIGHTING_CHANGE_TO_WHITE       
//============================================================================*/
#ifdef FLIMBS_20181210_POWERONOFF_LIGHTING_CHANGE_TO_WHITE
//: 전원 ONOFF 시 흰색으로 , 새로운 패턴으로 점멸
#endif

//============================================================================*/
//      FLIMBS_20181210_ONE_POINT_TOUCH_ADJUST_BY_HEATMAP       
//============================================================================*/
#ifdef FLIMBS_20181210_ONE_POINT_TOUCH_ADJUST_BY_HEATMAP
//: 포인팅시 센서 빈공간 도약현상 제거위해 heatmap.c 파일 가져와서 매핑하는 코드
#include "m_heatmap.h"

#include <math.h>

void m_flimbs_adjust_onepointXY_flimbs(uint16_t posX, uint16_t posY, uint8_t *p_heatmap_counter,	int16_t *p_diffX, int16_t *p_diffY)	// pdiffX pdiffY: signed 16-bit
{
    uint8_t heatmap_counter= *p_heatmap_counter;
    
    uint8_t posXSensitive = m_heatmap_get_touch_sensitive_x(posX, posY);
    uint8_t posYSensitive = m_heatmap_get_touch_sensitive_y(posX, posY);
                
    int16_t newDiffX = *p_diffX; // 주의: signed 16-/
    int16_t newDiffY = *p_diffY; // 주의: signed 16-bit

    uint8_t dividerX = 1;
    uint8_t dividerY = 1;

    if (posXSensitive <= 10)
    {//0~10:(367)
        dividerX = 2;
        dividerY = 2;
        if (heatmap_counter % 4 == 0)
        {
            newDiffX = 0;
            newDiffY = 0;					
        }
    }
    else if (posXSensitive <= 18)
    {//11~18(445)
        dividerX = 3;
        dividerY = 3;
        if (heatmap_counter % 2 == 0)
        {
            newDiffX = 0;
            newDiffY = 0;					
        }
    }
    else if (posXSensitive <= 24)
    {//19~24(1111)
        dividerX = 2;
        dividerY = 2;
        if (heatmap_counter % 3 == 0)
        {
            newDiffX = 0;
            newDiffY = 0;					
        }
    }
    else if (posXSensitive <= 34)
    {//25~34(483)
        if (heatmap_counter % 6 == 0)
        {
            newDiffX = 0;
            newDiffY = 0;					
        }
    }
    else if (posXSensitive <= 40)
    {//34~40(883)
        if (heatmap_counter % 12 == 0)
        {
            newDiffX = 0;
            newDiffY = 0;					
        }
    }
    else
    {//41~255(45)

    }

    if (newDiffX != 0)
    {
        newDiffX = abs(newDiffX) < dividerX ? abs(newDiffX) / newDiffX : newDiffX / dividerX;
    }
    if (newDiffY != 0)
    {
        newDiffY = abs(newDiffY) < dividerY ? abs(newDiffY) / newDiffY : newDiffY / dividerY;
    }
    // return the results
    *p_heatmap_counter = (heatmap_counter >= 24) ? 1 : heatmap_counter + 1;
    *p_diffX = newDiffX;
    *p_diffY = newDiffY;
}
#endif

//============================================================================*/
//      FLIMBS_20181210_TWO_POINT_SCROLL_ADJUST_BY_SPEED_CONTEXTUAL
//============================================================================*/
#ifdef FLIMBS_20181210_TWO_POINT_SCROLL_ADJUST_BY_SPEED_CONTEXTUAL
//: 스크롤시 스크롤 속도에 따라 느릴수록 감속시킴
#include "m_touch.h"
//uint8_t scroll_map[SCROLL_MAP_MAX] = { 1,1,1,2,4,4,8,8 };
uint8_t scroll_map[SCROLL_MAP_MAX] = { 1,1,1,1,1,1,2,2 };

uint16_t m_flimbs_adjust_scroll_two(uint16_t speed)
{    
    uint16_t index = speed /10;	
    index = index <= SCROLL_MAP_MAX ? index : SCROLL_MAP_MAX - 1;    
    return scroll_map[index]; 

}
#endif



//============================================================================*/
//      FLIMBS_20190110_CHANGE_LUI_LALT_ON_CENTRAL_MAC
//============================================================================*/
#ifdef FLIMBS_20190110_CHANGE_LUI_LALT_ON_CENTRAL_MAC
//:central mac 변경시 CMD, ALT 키위치 변경

#include "m_comm.h"
#include "drv_keyboard.h"

void m_flimbs_change_lookup_table_when_read(uint8_t *buf)
{        
    if(m_comm_current_central_type() == M_COMM_CENTRAL_MAC || M_COMM_CENTRAL_IOS)
    {
        //window UI     ALT /// ALT     CTRL
        //MAC    ALT    UI  /// UI      ALT
        if(*buf == S2L(L_UI))
        {
            *buf = S2L(L_ALT);            
        }
        if(*buf == S2L(L_ALT))
        {            
            *buf = S2L(L_UI);            
        }

        if(*buf == S2L(R_ALT))
        {
            *buf = S2L(R_UI);
        }
        if(*buf == S2L(R_CTRL))
        {
            *buf = S2L(R_ALT);
        }
    }
    else
    {        
    }					
}
#endif


//============================================================================
//     FLIMBS_20190111_CHANGE_LED_COLOR_GBR 
//============================================================================
#ifdef FLIMBS_20190111_CHANGE_LED_COLOR_GBR
//동글이 빠짐에 따라 컬러를 BT1 BT2 BT3 각 GBR로 변경
#endif

//============================================================================
//      FLIMBS_20190102_TOUCH_LOCK_LED_DEPENDS_ON_BTCH
//============================================================================
#ifdef FLIMBS_20190102_TOUCH_LOCK_LED_DEPENDS_ON_BTCH
//터치락 모드 들어가면 LED가 현재 채널색으로 토글된다
#endif


//============================================================================
//      FLIMBS_20190110_THREE_POINT_GESTURE_LEFTRIGHT_WITHOUT_OPTION
//============================================================================
#ifdef FLIMBS_20190110_THREE_POINT_GESTURE_LEFTRIGHT_WITHOUT_OPTION
//3점 터치 Left Right 제스처 사용시 three.option 으로 제스처고정하는걸 해제하고 
//1번만 제스처보내도록 변경
#endif





//============================================================================
//     AREA_PROPERTIE_FLiMBS_20190121_MOD_TOUCH_WHOLE_AREA_WITH_FILTER_KEYS
//============================================================================
#if (AREA_PROPERTIE == AREA_PROPERTIE_FLiMBS_20190121_MOD_TOUCH_WHOLE_AREA_WITH_FILTER_KEYS)
//터치 영역을 ALWAYS_FORCE_TO_WHOLE_AREA 로 할때 새로 추가한 필터키들에 맞춰서 영역조절
// AREA_PROPERTIE 에서 스위
ret_code_t m_flimbs_touch_clipping_filterkey(uint16_t x, uint16_t y)
{   
    ret_code_t result;
	#define X_UNIT_BUTTON 179//179
	#define Y_UNIT_BUTTON 170
	
	#define Y_BOT_MARGIN 0

	#define ESC 144
	#define TILDE 54
	#define TAB 144
	#define CAPSLOCK 192
	#define SHIFT 284
	#define CONTROL 192

	#define X_CLIPPING 20
	#define Y_CLIPPING 10
	
    
	result = NRF_ERROR_NOT_SUPPORTED;	
	if(Y_BOT_MARGIN + (Y_UNIT_BUTTON*6) < y )
	{//Top Margin				
	}
	else if(Y_BOT_MARGIN + (Y_UNIT_BUTTON*5) - Y_CLIPPING < y )
	{//Esc row
		//---|-- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- -----				
	}
	else if(Y_BOT_MARGIN + (Y_UNIT_BUTTON*4) < y )
	{//tilde row12*4 48
		//---| ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- -------
		if(TILDE +X_CLIPPING < x && x < TILDE + X_UNIT_BUTTON * 12 - X_CLIPPING)
		{
			result = NRF_SUCCESS;			
		}		
	}
	else if(Y_BOT_MARGIN + (Y_UNIT_BUTTON*3) < y )
	{//tab row
		//---|-- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- -----
		if(TAB + X_CLIPPING < x && x < TAB + X_UNIT_BUTTON * 12 - X_CLIPPING)
		{
			result = NRF_SUCCESS;			
		}		
	}
	else if(Y_BOT_MARGIN + (Y_UNIT_BUTTON*2) < y )
	{//Capslock row
		//---|--- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- --------
		if(CAPSLOCK + X_CLIPPING < x && x < CAPSLOCK + X_UNIT_BUTTON * 11 - X_CLIPPING)
		{
			result = NRF_SUCCESS;			
		}		
	}
	else if(Y_BOT_MARGIN + (Y_UNIT_BUTTON*1) < y )
	{//shift row
		//---|----- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----------
		if(SHIFT + X_CLIPPING < x && x < SHIFT + X_UNIT_BUTTON * 10 - X_CLIPPING)
		{
			result = NRF_SUCCESS;			
		}
	}
	else if(Y_BOT_MARGIN + Y_CLIPPING < y )
	{//ctrl Row
        if(CAPSLOCK + X_UNIT_BUTTON*3 + X_CLIPPING < x && x < CAPSLOCK + X_UNIT_BUTTON*3 + X_UNIT_BUTTON*4 + X_UNIT_BUTTON*2 - X_CLIPPING)
        {
            result = NRF_SUCCESS;
        }
		//---|--- ---- ---- ---- ----.----.----.---- ---- ---- ---- ---- ---- ----		
		
	}

    return result;
}
#endif











