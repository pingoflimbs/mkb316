/// \file m_dongle.h
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2018
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 7-Mar-2017  MJ.Kim
* + Created initial version
*
* 9-Apr-2018  MJ.Kim
* + Removed m_dongle_get_config(), m_dongle_set_config_default()
* + Added m_dongle_is_link_ok()
*
* 13-Apr-2018  MJ.Kim
* + Created m_dongle_handle_long_key()
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/

/*============================================================================*/
/*                            COMPILER DIRECTIVES                             */
/*============================================================================*/
#ifndef __M_DONGLE_H
#define __M_DONGLE_H

/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/    


/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/
// Ensure that we try all channels before giving up
//#define M_DONGLE_MAX_TX_ATTEMPTS (NRF_GZLL_DEFAULT_TIMESLOTS_PER_CHANNEL_WHEN_DEVICE_OUT_OF_SYNC * \
                         NRF_GZLL_DEFAULT_CHANNEL_TABLE_SIZE)
#define M_DONGLE_MAX_TX_ATTEMPTS         100  /**< Maximum number of Transmit attempts.*/
#define M_DONGLE_DUMMY_PACKET            0x80 /**< First payload.*/
#define M_DONGLE_NB_TX_WITH_SAME_PAYLOAD 3    /**< Number of times each packet is sent (this avoid changing the payload too quickly and allows to have a visible LED pattern on the receiver side).*/
#define M_DONGLE_PAYLOAD_SIZE            NRF_GZLL_CONST_MAX_PAYLOAD_LENGTH    /**< Size of the payload to send over Gazell.*/
#define M_DONGLE_PIPE_TO_HOST            0    /**< Pipe number. */

#define M_DONGLE_KEY			0
#define M_DONGLE_TOUCH_BUTTON	1
#define M_DONGLE_TOUCH_MOVE		2
#define M_DONGLE_CONSUMER		3

/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/

/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/

/*============================================================================*/
/*                               GLOBAL FUNCTIONS                             */
/*============================================================================*/
ret_code_t m_dongle_init(void);
ret_code_t m_dongle_start(void);
ret_code_t m_dongle_stop(void);
ret_code_t m_dongle_send(uint8_t rep_idx, uint8_t *p_data, uint8_t len);
bool m_dongle_is_link_ok(void);
ret_code_t m_dongle_pairing_req(bool flag);
void m_dongle_handle_long_key(void);

#endif // __M_DONGLE_H 
