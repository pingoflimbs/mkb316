/// \file m_led.c
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 27-Oct-2017  MJ.Kim
* + Created initial version
*
* 27-Dec-2017  MJ.Kim
* + Rewrote code according to "171218_Mokibo 동작시나리오.docx"
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/

/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/
#include "sdk_config.h" // mokibo_config.h && sdk_config.h
#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
	// should be placed after "sdk_config.h"
	#include "SEGGER_RTT.h"
	#include "m_timer.h"
#endif
#include "sdk_common.h"
#include "app_error.h"
#include "nrf_log.h"
#include "nrf_gpio.h"
#include "drv_stm8.h"
#include "m_led.h"        

/*============================================================================*/
/*                              CLASS VARIABLES                               */
/*============================================================================*/
static M_LED_COLOR_TYPE_t 	m_led_color; 
static M_LED_STATUS_TYPE_t 	m_led_status;

/*============================================================================*/
/*                              LOCAL FUNCTIONS                               */
/*============================================================================*/

/*============================================================================*/
/*                              GLOBAL FUNCTIONS                              */
/*============================================================================*/
/*
================================================================================
   Function name : m_led_init()
================================================================================
*/
ret_code_t m_led_init(void)
{	
	#if	(M_LED_TRACE==MOKIBO_DEBUG_ON)
	DEBUG("m_led_init()...Called");
	#endif

	#if (MOKIBO_LED_TRACE_ENABLED==1)
	nrf_gpio_cfg_output(25);
	nrf_gpio_cfg_input(26, NRF_GPIO_PIN_PULLUP);
	#endif

	// Set the off
	m_led_status 	= M_LED_ON;
	m_led_set_status(M_LED_OFF);
	
	// Set the off
	drv_stm8_write(STM8_WRITE_LED_BR, 3);
	m_led_color 	= M_LED_COLOR_WHITE; 
	m_led_set_color(M_LED_COLOR_GREEN);

    return NRF_SUCCESS;
}

/*
================================================================================
   Function name : m_led_set_color()
================================================================================
*/
ret_code_t m_led_set_color(M_LED_COLOR_TYPE_t color)
{
	ret_code_t rv = NRF_SUCCESS;

	#ifdef M_LED_TRACE
		DEB("m_led_set_color(): %d \n", color);
	#endif

	if(m_led_color != color)
	{
		m_led_color = color;

		if(M_LED_COLOR_TEST == color)
		{
			// Custom Color Format: [HHH LLL 00]

			// High-nibble RGB Value
			drv_stm8_write(STM8_WRITE_LED_CUSTOM_NIBBLE, 1);
			drv_stm8_write(STM8_WRITE_LED_CUSTOM_RED, 0);
			drv_stm8_write(STM8_WRITE_LED_CUSTOM_GREEN, 2);
			drv_stm8_write(STM8_WRITE_LED_CUSTOM_BLUE, 3);

			// Low-nibble RGB Value
			drv_stm8_write(STM8_WRITE_LED_CUSTOM_NIBBLE, 0);
			drv_stm8_write(STM8_WRITE_LED_CUSTOM_RED, 0);
			drv_stm8_write(STM8_WRITE_LED_CUSTOM_GREEN, 0);
			drv_stm8_write(STM8_WRITE_LED_CUSTOM_BLUE, 0);

			//drv_stm8_write(STM8_WRITE_LED_BR, 3);
			rv = drv_stm8_write(STM8_WRITE_LED_COLOR, M_LED_COLOR_CUSTOM);
		}
		else
		{
			rv = drv_stm8_write(STM8_WRITE_LED_COLOR, color);
		}
	}

	return rv;
}

/*
================================================================================
   Function name : m_led_get_color()
================================================================================
*/
M_LED_COLOR_TYPE_t m_led_get_color(void)
{
	return m_led_color;
}

/*
================================================================================
   Function name : m_led_set_status()
================================================================================
*/
ret_code_t m_led_set_status(M_LED_STATUS_TYPE_t status)
{
	ret_code_t rv = NRF_SUCCESS;
	
	#ifdef M_LED_TRACE
		DEB("m_led_set_status(): %d", status);
	#endif

	if(m_led_status != status)
	{
		rv = drv_stm8_write(STM8_WRITE_LED_STATUS, status);

		if(NRF_SUCCESS == rv)
		{
			m_led_status = status;
		}
	}

	return rv;
}

/*
================================================================================
   Function name : m_led_get_status()
================================================================================
*/
M_LED_STATUS_TYPE_t m_led_get_status(void)
{
	return m_led_status;
}

#if (MOKIBO_LED_TRACE_ENABLED==1)
/*
================================================================================
   Function name : m_led_trace_set()
================================================================================
*/
void m_led_trace_set(uint8_t on_off)
{
	on_off ? nrf_gpio_pin_clear(25) : nrf_gpio_pin_set(25);
}

/*
================================================================================
   Function name : m_led_trace_set()
================================================================================
*/
void m_led_trace_toggle(void)
{
	static uint8_t toggle = 0; 
	toggle ? nrf_gpio_pin_clear(25) : nrf_gpio_pin_set(25);
	toggle ^= 1;
}

/*
================================================================================
   Function name : m_led_get_stm8_INT()
================================================================================
*/
uint8_t m_led_get_stm8_INT(void)
{
	return nrf_gpio_pin_read(26) ? 1 : 0;
}
#endif

#if 0 // Currently, LED is managed by STM8L
/*
================================================================================
   Function name : m_led_update()
================================================================================
*/
void m_led_update(void)
{
	// TODO: controll on/off led by driver here 
}
#endif

// End of file 
