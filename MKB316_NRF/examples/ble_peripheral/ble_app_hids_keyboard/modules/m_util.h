/// \file m_util.h
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 13-Feb-2018  MJ.Kim
* + Created initial version
*
* 27-Feb-2018  MJ.Kim
* + Created m_util_get_manufacturer()
*
* 28-Feb-2018  MJ.Kim
* + Created m_util_get_unique_device_id() 
*
* 11-Apr-2018  MJ.Kim
* + Created m_util_get_battery_level()
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/

/*============================================================================*/
/*                            COMPILER DIRECTIVES                             */
/*============================================================================*/
#ifndef __M_UTIL_H
#define __M_UTIL_H

/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/    
#include "ble.h"

/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/
#define M_UTIL_ATAN2DEG_MAX 	(1456)
#define M_UTIL_ATAN2DEG_MIN 	(-1456)

/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/

/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/
typedef enum
{
    m_util_OUI_others = 0,
    m_util_oui_APPLE,
    m_util_oui_SAMSUNG,
    m_util_OUI_list_max,
} m_util_OUI_list_t; // Organizational Unique Identifier

/*============================================================================*/
/*                               GLOBAL FUNCTIONS                             */
/*============================================================================*/
uint16_t m_util_atan2deg(int16_t x, int16_t y);
void m_util_vector_to_coordinate(int16_t* x, int16_t *y, uint16_t speed, uint16_t angle);
m_util_OUI_list_t m_util_get_manufacturer(ble_gap_addr_t *p_mac_addr);
void m_util_print_picture(void);
uint32_t m_util_get_unique_device_id(void);
void m_util_get_mac_address(uint8_t ch, ble_gap_addr_t *pAddr);	// add by jaehong, 2019/06/28
ret_code_t	m_util_get_battery_level(uint8_t *level);
ret_code_t m_util_get_battery_status(uint8_t *BtrState);
ret_code_t m_util_get_battery_level_test(uint8_t *pFwStatus, uint8_t *pVal , uint8_t *pStatus, uint8_t *pHigh, uint8_t *pLow);

#endif // __M_UTIL_H 
