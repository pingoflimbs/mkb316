/// \file m_util.c
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 13-Feb-2018  MJ.Kim
* + Created initial version
*
* 27-Feb-2018  MJ.Kim
* + Created m_util_get_manufacturer()
*
* 28-Feb-2018  MJ.Kim
* + Created m_util_get_unique_device_id() 
*
* 7-Mar-2018  MJ.Kim
* + Added condition checking BLE_GAP_ADDR_TYPE_PUBLIC in m_util_get_manufacturer
*
* 11-Apr-2018  MJ.Kim
* + Created m_util_get_battery_level()
*
* 3-May-2018  MJ.Kim
* + Implemented get_bat_level()
* + Added m_bat_adc_to_percent
* + Changed type of m_util_get_battery_level
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/

/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/
#include "sdk_config.h" // mokibo_config.h && sdk_config.h
//
#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
	// should be placed after "sdk_config.h"
	#include "SEGGER_RTT.h"
	#include "m_timer.h"
#endif
#include "sdk_common.h"
#include "app_error.h"
#include "nrf_log.h"
#include "nrf_delay.h"

#include "drv_stm8.h"

#include "OUI_APPLE_TABLE.h"        
#include "m_util.h"        

/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/             

/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/

/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/
typedef struct {
	int8_t 	x;
	int8_t 	y;
	int8_t 	x_meduim;
	int8_t 	y_meduim;
	int8_t 	x_large;
	int8_t 	y_large;
} angle_to_coordinate_t;

// ADC to % of battery level structure
typedef struct {		
	uint16_t adc;
	uint8_t  percent;
} bat_adc_to_percent_t;

/*============================================================================*/
/*                       PROTOTYPES OF LOCAL FUNCTIONS                        */
/*============================================================================*/
static uint8_t get_bat_level(uint16_t ADC);

/*============================================================================*/
/*                              CLASS VARIABLES                               */
/*============================================================================*/
const static angle_to_coordinate_t m_angle_to_coordinate[36] = { \
{1,		0,		4,		0,		8,		0},
{1,		0,		4,		1,		8,		1},
{1,		0,		4,		1,		8,		3},
{1,		1,		3,		2,		7,		4},
{1,		1,		3,		3,		6,		5},
{1,		1,		3,		3,		5,		6},
{1,		1,		2,		3,		4,		7},
{0,		1,		1,		4,		3,		8},
{0,		1,		1,		4,		1,		8},
{0,		1,		0,		4,		0,		8},
{0,		1,		-1,		4,		-1,		8},
{0,		1,		-1,		4,		-3,		8},
{-1,	1,		-2,		3,		-4,		7},
{-1,	1,		-3,		3,		-5,		6},
{-1,	1,		-3,		3,		-6,		5},
{-1,	1,		-3,		2,		-7,		4},
{-1,	0,		-4,		1,		-8,		3},
{-1,	0,		-4,		1,		-8,		1},
{-1,	0,		-4,		0,		-8,		0},
{-1,	0,		-4,		-1,		-8,		-1},
{-1,	0,		-4,		-1,		-8,		-3},
{-1,	-1,		-3,		-2,		-7,		-4},
{-1,	-1,		-3,		-3,		-6,		-5},
{-1,	-1,		-3,		-3,		-5,		-6},
{-1,	-1,		-2,		-3,		-4,		-7},
{0,		-1,		-1,		-4,		-3,		-8},
{0,		-1,		-1,		-4,		-1,		-8},
{0,		-1,		0,		-4,		0,		-8},
{0,		-1,		1,		-4,		1,		-8},
{0,		-1,		1,		-4,		3,		-8},
{1,		-1,		2,		-3,		4,		-7},
{1,		-1,		3,		-3,		5,		-6},
{1,		-1,		3,		-3,		6,		-5},
{1,		-1,		3,		-2,		7,		-4},
{1,		0,		4,		-1,		8,		-3},
{1,		0,		4,		-1,		8,		-1},
};

// 2018.5.3 Based on Rev1.1				
const static bat_adc_to_percent_t m_bat_adc_to_percent_table[] = { \
{	 1631 	, 	0 	}, //
{	 1649 	, 	2 	}, //**
{	 1667 	, 	4 	}, //****
{	 1686 	, 	6 	}, //
{	 1704 	, 	8 	}, 
{	 1722 	, 	10 	}, //min 0%
{	 1741 	, 	12 	}, 
{	 1759 	, 	14 	}, 
{	 1777 	, 	16 	}, 
{	 1796 	, 	18 	}, 
{	 1814 	, 	20 	}, 
{	 1833 	, 	22 	}, 
{	 1851 	, 	24 	}, 
{	 1869 	, 	26 	}, 
{	 1888 	, 	28 	}, 
{	 1906 	, 	30 	}, 
{	 1924 	, 	32 	}, 
{	 1943 	, 	34 	}, 
{	 1961 	, 	36 	}, 
{	 1979 	, 	38 	}, 
{	 1998 	, 	40 	}, 
{	 2016 	, 	42 	}, 
{	 2034 	, 	44 	}, 
{	 2053 	, 	46 	}, 
{	 2071 	, 	48 	}, 
{	 2089 	, 	50 	}, 
{	 2108 	, 	52 	}, 
{	 2126 	, 	54 	}, //max 100%
{	 2144 	, 	56 	}, 
{	 2163 	, 	58 	}, 
{	 2181 	, 	60 	}, 
{	 2199 	, 	62 	}, 
{	 2218 	, 	64 	}, 
{	 2236 	, 	66 	}, 
{	 2254 	, 	68 	}, 
{	 2273 	, 	70 	}, 
{	 2291 	, 	72 	}, 
{	 2310 	, 	74 	}, 
{	 2328 	, 	76 	}, 
{	 2346 	, 	78 	}, 
{	 2365 	, 	80 	}, 
{	 2383 	, 	82 	}, 
{	 2401 	, 	84 	}, 
{	 2420 	, 	86 	}, 
{	 2438 	, 	88 	}, 
{	 2456 	, 	90 	}, 
{	 2475 	, 	92 	}, 
{	 2493 	, 	94 	}, 
{	 2511 	, 	96 	}, 
{	 2530 	, 	98 	}, 
{	 2548 	, 	100	}, 
};				

/*============================================================================*/
/*                              LOCAL FUNCTIONS                               */
/*============================================================================*/
/*
================================================================================
   Function name : get_bat_level()
================================================================================
*/
static uint8_t get_bat_level(uint16_t ADC)
{
	uint16_t index;
	uint8_t level = 100U; 

	for(index=0; index<(sizeof(m_bat_adc_to_percent_table)/sizeof(bat_adc_to_percent_t)); index++)	
	{
		if(ADC < m_bat_adc_to_percent_table[index].adc)
		{
			level = m_bat_adc_to_percent_table[index].percent;
			break;
		}
	}

	DEBUG("ADC:%d, index:%d, level:%d", ADC, index, level);

	return level;
}

/*============================================================================*/
/*                              GLOBAL FUNCTIONS                              */
/*============================================================================*/
/*
================================================================================
   Function name : m_util_atan2deg()
================================================================================
*/
// 출처: https://github.com/joeferner/stm32-utils/blob/master/math.c
// Unit test 필요함, 간단히 Unit test 했음
/**
 * http://www.romanblack.com/integer_degree.htm
 *
 * Fast XY vector to integer degree algorithm - Jan 2011 www.RomanBlack.com
 * Converts any XY values including 0 to a degree value that should be
 * within +/- 1 degree of the accurate value without needing
 * large slow trig functions like ArcTan() or ArcCos().
 * NOTE! at least one of the X or Y values must be non-zero!
 * This is the full version, for all 4 quadrants and will generate
 * the angle in integer degrees from 0-360.
 * Any values of X and Y are usable including negative values provided
 * they are between -1456 and 1456 so the 16bit multiply does not overflow.
 */
//uint16_t trig_int16_atan2deg(int16_t y, int16_t x) 
uint16_t m_util_atan2deg(int16_t x, int16_t y) 
{
	uint8_t negflag, tempdegree, comp;
	uint32_t degree, uy,  ux;

	if (y == 0 && x == 0) 
	{
		return 0;
	}

	// Added by Mj.kim
	if(y > M_UTIL_ATAN2DEG_MAX) // y > 1456
	{
		//DEBUG("y:%d", y);
		y = M_UTIL_ATAN2DEG_MAX; 
	}
	
	if(y < M_UTIL_ATAN2DEG_MIN) // y < -1456
	{
		//DEBUG("y:%d", y);
		y = M_UTIL_ATAN2DEG_MIN; 
	}
	
	if(x > M_UTIL_ATAN2DEG_MAX) // x > 1456
	{
		//DEBUG("x:%d", x);
		x = M_UTIL_ATAN2DEG_MAX; 
	}
	
	if(x < M_UTIL_ATAN2DEG_MIN) // x < -1456
	{
		//DEBUG("x:%d", x);
		x = M_UTIL_ATAN2DEG_MIN; 
	}
	// end of Added by Mj.kim

	// Save the sign flags then remove signs and get XY as unsigned ints
	negflag = 0;
	if (y < 0) {
		negflag += 0x01;    // y flag bit
		y = (0 - y);        // is now +
	}
	uy = y;                // copy to unsigned var before multiply
	if (x < 0) {
		negflag += 0x02;    // x flag bit
		x = (0 - x);        // is now +
	}
	ux = x;                // copy to unsigned var before multiply

	// 1. Calc the scaled "degrees"
	if (uy > ux) {
		degree = (ux * 45) / uy;   // degree result will be 0-45 range
		negflag += 0x10;    // octant flag bit
	} else {
		degree = (uy * 45) / ux;   // degree result will be 0-45 range
	}

	// 2. Compensate for the 4 degree error curve
	comp = 0;
	tempdegree = degree;    // use an unsigned char for speed!
	if (tempdegree > 22) {   // if top half of range
		if (tempdegree <= 44) {
			comp++;
		}
		if (tempdegree <= 41) {
			comp++;
		}
		if (tempdegree <= 37) {
			comp++;
		}
		if (tempdegree <= 32) {
			comp++;  // max is 4 degrees compensated
		}
	} else { // else is lower half of range
		if (tempdegree >= 2) {
			comp++;
		}
		if (tempdegree >= 6) {
			comp++;
		}
		if (tempdegree >= 10) {
			comp++;
		}
		if (tempdegree >= 15) {
			comp++;  // max is 4 degrees compensated
		}
	}
	degree += comp;   // degree is now accurate to +/- 1 degree!

	// Invert degree if it was X>Y octant, makes 0-45 into 90-45
	if (negflag & 0x10) {
		degree = (90 - degree);
	}

	// 3. Degree is now 0-90 range for this quadrant,
	// need to invert it for whichever quadrant it was in
	if (negflag & 0x02) { // if -Y
		if (negflag & 0x01) { // if -Y -X
			degree = (180 + degree);
		} else       { // else is -Y +X
			degree = (180 - degree);
		}
	} else { // else is +Y
		if (negflag & 0x01) { // if +Y -X
			degree = (360 - degree);
		}
	}

	return degree;
}

/*==============================================================================
   Function name : m_util_vector_to_coordinate()
==============================================================================*/
void m_util_vector_to_coordinate(int16_t* x, int16_t *y, uint16_t speed, uint16_t angle) 
{
	if((NULL == x) || (NULL == x))
	{
		DEBUG("OOPS!!");
		return;
	}

	if(angle>360)
	{
		DEBUG("OOPS[%d]!!", angle);
		angle %= 360;
	}

	angle /= 10;

	//*x = (int16_t)m_angle_to_coordinate[angle].x;
	//*y = (int16_t)m_angle_to_coordinate[angle].y;
	*x = (int16_t)m_angle_to_coordinate[angle].x_large;
	*y = (int16_t)m_angle_to_coordinate[angle].y_large;
}

/*
================================================================================
   Function name : m_util_get_manufacturer()
================================================================================
*/
m_util_OUI_list_t m_util_get_manufacturer(ble_gap_addr_t *p_mac_addr)
{
	int32_t	i, size;

	m_util_OUI_list_t manufacturer = m_util_OUI_others;

	size = sizeof(OUI_APPLE_TABLE)/sizeof(oui_addr_t);

	#if defined(M_UTIL_MAC_TRACE)
	DEBUG("OUI_APPLE_TABLE:%d, TOTAL:%d", size, sizeof(OUI_APPLE_TABLE));
	#endif

	// Address is available only when BLE_GAP_ADDR_TYPE_PUBLIC
	if(BLE_GAP_ADDR_TYPE_PUBLIC == p_mac_addr->addr_type)
	{
		if(NULL != p_mac_addr)
		{
			for(i=0; i<size; i++)
			{
				// Nordic SDK addr order: LSB format
				// TABLE from IEEE OUI list: MSB format
				if((p_mac_addr->addr[5] == OUI_APPLE_TABLE[i].addr[0] ) 
						&& (p_mac_addr->addr[4] == OUI_APPLE_TABLE[i].addr[1] ) 
						&& (p_mac_addr->addr[3] == OUI_APPLE_TABLE[i].addr[2]))
				{
					manufacturer = m_util_oui_APPLE;
					#if defined(M_UTIL_MAC_TRACE)
					DEBUG("APPLE MAC Found[%d][0x%02x][0x%02x][0x%02x]"
							, i , OUI_APPLE_TABLE[i].addr[0]
							, OUI_APPLE_TABLE[i].addr[1]
							, OUI_APPLE_TABLE[i].addr[2]);
					#endif
					break;
				}
			}
		}
		else
		{
			DEBUG("OOPS!!!");
		}
	}

	return manufacturer;
}

static const char *Picture[] = \
{ \
"                 .               ",\
"             /\\ /l               ",\
"            ((.Y(!               ",\
"             \\ |/                ",\
"             /  6~6,             ",\
"             \\ _    +-.          ",\
"              \\`-=--^-'          ",\
"               \\ \\               ",\
"              _/  \\              ",\
"             (  .  Y             ",\
"            /'\\ `--^--v--.		  ",\
"           / _ `--'T~\\/~\\/       ",\
"          / ' ~\\.  !             ",\
"    _    Y      Y./'             ",\
"   Y^|   |      |~~7             ",\
"   | l   |     / ./'             ",\
"   | `L  | Y .^/~T				  ",\
"   |  l  ! | |/| |               ",\
"   | .`\\/' | Y | !               ",\
"   l  '~   j l j_L______         ",\
"    \\,____{ __'~ __ ,\\_,\\_       ",\
"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    ",\
"",\
"",\
};

/*
================================================================================
   Function name : m_util_print_picture()
================================================================================
*/
void m_util_print_picture(void)
{
	int32_t	i;

	for(i=0; Picture[i] != NULL; i++)
	{
		DEBUGS("%s", Picture[i]);
	}

}

/*
================================================================================
   Function name : m_util_get_unique_device_id()
================================================================================
*/
uint32_t m_util_get_unique_device_id(void)
{
	return (uint32_t)NRF_FICR->DEVICEID[0]; // Random at Nordic factory
}

/*
================================================================================
   Function name : m_util_get_mac_address()
================================================================================
*/
void m_util_get_mac_address(uint8_t ch, ble_gap_addr_t *pAddr)
{
	pAddr->addr_type     = BLE_GAP_ADDR_TYPE_RANDOM_STATIC;
	pAddr->addr[0] = (uint8_t)NRF_FICR->DEVICEADDR[0];
	pAddr->addr[1] = (uint8_t)(NRF_FICR->DEVICEADDR[0] >> 8);
	pAddr->addr[2] = (uint8_t)(NRF_FICR->DEVICEADDR[0] >> 16);
	pAddr->addr[3] = (uint8_t)(NRF_FICR->DEVICEADDR[0] >> 24);
	pAddr->addr[4] = (uint8_t)NRF_FICR->DEVICEADDR[1];
	pAddr->addr[5] = (uint8_t)((NRF_FICR->DEVICEADDR[1] >> 8) | 0xC0); // 2MSB must be set 11
	pAddr->addr[5] += ch;
}

/*
================================================================================
   Function name : m_util_get_battery_level()
================================================================================
*/

/*
외부전압
4.6 
4.5 
4.4 38
4.3 32
4.2 54 <GREEN
4.1 50
4.0 44 
3.9 38 <YELLOW
3.8 32 <RED
3.7 28
3.6 22
3.5 16
3.4 10
3.3 10
3.2 10 < clickbar Error
3.1 10
3.0 10
2.9 10
2.8 10
2.7 10
2.6 10
2.5 10
2.4 10 < die
2.3 10
2.2 10
// billy: 20%=3.8v, 70%=3.9v
*/


ret_code_t m_util_get_battery_level(uint8_t *pVal)
{
	uint8_t high, low, status;
	uint16_t ADC;
	ret_code_t rv;

	// Clear local variable
	ADC = high = low = status = 0U;

	// Step 1) Request battery check
	drv_stm8_write(STM8_WRITE_BATTERY_LEVEL_UPDATE, 0);	

	// Some delay
	nrf_delay_ms(8); 

	// Step 2) Read 16bit? ADC and Status
	drv_stm8_read(STM8_READ_BATTERY_STATUS, &status);
	drv_stm8_read(STM8_READ_BATTERY_LEVEL, &high);
	DEBUG("LevelHighByte:0x%02x ADCStatus:0x%02x", high, status);
	
	drv_stm8_read(STM8_READ_BATTERY_STATUS, &status);
	drv_stm8_read(STM8_READ_BATTERY_LEVEL, &low);
	
	DEBUG("LevelLowByte:0x%02x ADCStatus:0x%02x", low, status);

	#define ADC_IN_CHARGING 	0x01	 // TODO: Low Active
	#define ADC_IN_PROGRESS 	0x02
	#define ADC_ADC_HIGH_BYTE   0x04

	if(status & ADC_IN_PROGRESS)
	{
		rv = NRF_ERROR_BUSY;
	}
	else
	{
		ADC = (uint16_t)((high<<8) + low);
	
		if(ADC)
		{
			rv = NRF_SUCCESS;
			*pVal = get_bat_level(ADC);
		}
		else
		{
			rv = NRF_ERROR_INVALID_DATA;
		}
	}

	return rv;
}

ret_code_t m_util_get_battery_status(uint8_t *BtrState)
{
	ret_code_t rv;

	drv_stm8_read(STM8_READ_BATTERY_STATUS, BtrState);
	
	if(*BtrState & ADC_IN_PROGRESS)
	{
		rv = NRF_ERROR_BUSY;
	}
	else
	{
		rv = NRF_SUCCESS;
	}

	return rv;
}
ret_code_t m_util_get_battery_level_test(uint8_t *pFwStatus ,uint8_t *pVal , uint8_t *pStatus, uint8_t *pHigh, uint8_t *pLow)
{
	uint8_t high, low, status;
	uint8_t fwStatus=0;
	uint16_t ADC;
	ret_code_t rv;

	// Clear local variable
	ADC = high = low = status = 0U;


	drv_stm8_read(STM8_READ_FW_STATUS, &fwStatus);
	// Step 1) Request battery check
	drv_stm8_write(STM8_WRITE_BATTERY_LEVEL_UPDATE, 0);	

	// Some delay
	nrf_delay_ms(8); 
	
	// Step 2) Read 16bit? ADC and Status
	drv_stm8_read(STM8_READ_BATTERY_STATUS, &status);
	drv_stm8_read(STM8_READ_BATTERY_LEVEL, &high);
	DEBUG("LevelHighByte:0x%02x ADCStatus:0x%02x", high, status);
	
	drv_stm8_read(STM8_READ_BATTERY_STATUS, &status);
	drv_stm8_read(STM8_READ_BATTERY_LEVEL, &low);
	
	DEBUG("LevelLowByte:0x%02x ADCStatus:0x%02x", low, status);

	#define ADC_IN_CHARGING 	0x01	 // TODO: Low Active
	#define ADC_IN_PROGRESS 	0x02
	#define ADC_ADC_HIGH_BYTE   0x04

	*pStatus = status;
	*pHigh = high;
	*pLow = low;
	*pFwStatus = fwStatus;

	if(status & ADC_IN_PROGRESS)
	{
		rv = NRF_ERROR_BUSY;
	}
	else
	{		
		ADC = (uint16_t)((high<<8) + low);
	
		if(ADC)
		{
			rv = NRF_SUCCESS;
			*pVal = get_bat_level(ADC);
		}
		else
		{
			rv = NRF_ERROR_INVALID_DATA;
		}
	}

	return rv;
}
// End of file 
