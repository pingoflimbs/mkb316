/// \file m_comm_adv.c
/// This file contains all required configurations for mokibo
/*==============================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
==============================================================================*/





/*\      $$\  $$$$$$\  $$$$$$$\  $$\   $$\ $$\       $$$$$$$$\  $$$$$$\  
$$$\    $$$ |$$  __$$\ $$  __$$\ $$ |  $$ |$$ |      $$  _____|$$  __$$\ 
$$$$\  $$$$ |$$ /  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$ /  \__|
$$\$$\$$ $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$$$$\    \$$$$$$\  
$$ \$$$  $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$  __|    \____$$\ 
$$ |\$  /$$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$\   $$ |
$$ | \_/ $$ | $$$$$$  |$$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$$\ \$$$$$$  |
\__|     \__| \______/ \_______/  \______/ \________|\________| \______/ 
//============================================================================//
//                              LOCAL MODULEL USED                            //
//============================================================================*/
#include "sdk_config.h" // mokibo_config.h && sdk_config.h
#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
	// should be placed after "sdk_config.h"
	#include "SEGGER_RTT.h"
#endif

#include "ble_conn_params.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "ble_conn_state.h"
#include "ble_advertising.h"
#include "app_timer.h"

#if MOKIBO_NFC_ENABLED
#include "nrf_ble_gatt.h"
#endif

#include "nrf_log.h"

#include "m_led.h"        
#include "m_comm.h"   
#include "m_mokibo.h"   
#include "m_util.h"   
#include "m_timer.h"   
#include "m_comm_adv.h"
#if MOKIBO_NFC_ENABLED
#include "m_nfc.h"
#endif







 /*$$$$\   $$$$$$\  $$\   $$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$\   $$\ $$$$$$$$\ 
$$  __$$\ $$  __$$\ $$$\  $$ |$$  __$$\\__$$  __|$$  __$$\ $$$\  $$ |\__$$  __|
$$ /  \__|$$ /  $$ |$$$$\ $$ |$$ /  \__|  $$ |   $$ /  $$ |$$$$\ $$ |   $$ |   
$$ |      $$ |  $$ |$$ $$\$$ |\$$$$$$\    $$ |   $$$$$$$$ |$$ $$\$$ |   $$ |   
$$ |      $$ |  $$ |$$ \$$$$ | \____$$\   $$ |   $$  __$$ |$$ \$$$$ |   $$ |   
$$ |  $$\ $$ |  $$ |$$ |\$$$ |$$\   $$ |  $$ |   $$ |  $$ |$$ |\$$$ |   $$ |   
\$$$$$$  | $$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$ |  $$ |$$ | \$$ |   $$ |   
 \______/  \______/ \__|  \__| \______/   \__|   \__|  \__|\__|  \__|   \__|   
//============================================================================//
//                                LOCAL CONSTANTS                             //
//============================================================================*/





/*$$$$$$\ $$\     $$\ $$$$$$$\  $$$$$$$$\ $$$$$$$\  $$$$$$$$\ $$$$$$$$\  $$$$$$\  
\__$$  __|\$$\   $$  |$$  __$$\ $$  _____|$$  __$$\ $$  _____|$$  _____|$$  __$$\ 
   $$ |    \$$\ $$  / $$ |  $$ |$$ |      $$ |  $$ |$$ |      $$ |      $$ /  \__|
   $$ |     \$$$$  /  $$$$$$$  |$$$$$\    $$ |  $$ |$$$$$\    $$$$$\    \$$$$$$\  
   $$ |      \$$  /   $$  ____/ $$  __|   $$ |  $$ |$$  __|   $$  __|    \____$$\ 
   $$ |       $$ |    $$ |      $$ |      $$ |  $$ |$$ |      $$ |      $$\   $$ |
   $$ |       $$ |    $$ |      $$$$$$$$\ $$$$$$$  |$$$$$$$$\ $$ |      \$$$$$$  |
   \__|       \__|    \__|      \________|\_______/ \________|\__|       \______/ 
//============================================================================//
//                                LOCAL TYPEDEFS                              //
//============================================================================*/
 





/*\    $$\  $$$$$$\  $$$$$$$\  $$$$$$\  $$$$$$\  $$$$$$$\  $$\       $$$$$$$$\ 
$$ |   $$ |$$  __$$\ $$  __$$\ \_$$  _|$$  __$$\ $$  __$$\ $$ |      $$  _____|
$$ |   $$ |$$ /  $$ |$$ |  $$ |  $$ |  $$ /  $$ |$$ |  $$ |$$ |      $$ |      
\$$\  $$  |$$$$$$$$ |$$$$$$$  |  $$ |  $$$$$$$$ |$$$$$$$\ |$$ |      $$$$$\    
 \$$\$$  / $$  __$$ |$$  __$$<   $$ |  $$  __$$ |$$  __$$\ $$ |      $$  __|   
  \$$$  /  $$ |  $$ |$$ |  $$ |  $$ |  $$ |  $$ |$$ |  $$ |$$ |      $$ |      
   \$  /   $$ |  $$ |$$ |  $$ |$$$$$$\ $$ |  $$ |$$$$$$$  |$$$$$$$$\ $$$$$$$$\ 
    \_/    \__|  \__|\__|  \__|\______|\__|  \__|\_______/ \________|\_______/
//============================================================================//
//                                   VARIABLES                                //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                                GLOBAL VARIABLES                            //
//----------------------------------------------------------------------------*/
ble_uuid_t 					m_adv_uuids[] = {{BLE_UUID_HUMAN_INTERFACE_DEVICE_SERVICE, BLE_UUID_TYPE_BLE}};
m_comm_adv_status_t			m_adv_status = M_COMM_ADV_STATUS_NONE;
m_comm_adv_config_t			m_adv_config;
m_comm_adv_channel_type_t 	m_adv_channel; 
/*----------------------------------------------------------------------------//
//                                 LOCAL VARIABLES                            //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                                 DEBUG VARIABLES                            //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                                UNUSED VARIABLES                            //
//----------------------------------------------------------------------------*/





/*$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\  $$\   $$\ $$$$$$$$\  $$$$$$\  $$$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\ $$ |  $$ |$$  _____|$$  __$$\ $$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|$$ |  $$ |$$ |      $$ /  $$ |$$ |  $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |      $$$$$$$$ |$$$$$\    $$$$$$$$ |$$ |  $$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |      $$  __$$ |$$  __|   $$  __$$ |$$ |  $$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\ $$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |$$ |  $$ |$$$$$$$$\ $$ |  $$ |$$$$$$$  |
\__|       \______/ \__|  \__| \______/ \__|  \__|\________|\__|  \__|\_______/ 
//============================================================================//
//                                   FUNCHEAD                                 //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                            GLOBAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
ret_code_t m_comm_adv_init(void);
ret_code_t m_comm_adv_start(m_comm_adv_channel_type_t ch);
ret_code_t m_comm_adv_disconnect(void);
ret_code_t m_comm_adv_clear_bonds(void);
ret_code_t m_comm_adv_ready_bond(m_comm_adv_channel_type_t ch);

m_comm_adv_status_t m_comm_adv_get_status(void);
void m_comm_adv_set_status(m_comm_adv_status_t status);

pm_peer_id_t m_comm_adv_get_addr(m_comm_adv_channel_type_t ch);
void m_comm_adv_set_addr(m_comm_adv_channel_type_t ch, pm_peer_id_t);

ret_code_t m_comm_adv_set_config_default(void);

#if MOKIBO_NFC_ENABLED
ble_advertising_t * m_comm_adv_instance_ptr_get(void);
#endif
/*----------------------------------------------------------------------------//
//                             LOCAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
static void bond_set(m_comm_adv_channel_type_t ch, pm_peer_id_t val);
static bool is_valid_ch(m_comm_adv_channel_type_t ch);
static void set_adv_status(m_comm_adv_status_t status, const char *caller);
static void adv_init(void);
static ret_code_t adv_start(m_comm_adv_channel_type_t ch);
static void peer_manager_init(void);
static void pm_evt_handler(pm_evt_t const * p_evt);
static void adv_error_handler(uint32_t nrf_error);
static void on_adv_evt(ble_adv_evt_t ble_adv_evt);


/*----------------------------------------------------------------------------//
//                            POINTER FUNCTION HEADS                          //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                             DEBUG FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
void debshow_comm_adv_config(const char *msg, m_comm_adv_config_t comm_adv_config);
void debshow_mokibo_manufacture_info(const char *msg,m_mokibo_manufacture_info_t factory_data);
void debshow_mokibo_test_info(const char *msg,m_mokibo_test_info_t test_data);
/*----------------------------------------------------------------------------//
//                             UNUSED FUNCTION HEADS                          //
//----------------------------------------------------------------------------*/








/*\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ 
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\_*/

 /*$$$$\  $$\       $$$$$$\  $$$$$$$\   $$$$$$\  $$\                         
$$  __$$\ $$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                        
$$ /  \__|$$ |     $$ /  $$ |$$ |  $$ |$$ /  $$ |$$ |                        
$$ |$$$$\ $$ |     $$ |  $$ |$$$$$$$\ |$$$$$$$$ |$$ |                        
$$ |\_$$ |$$ |     $$ |  $$ |$$  __$$\ $$  __$$ |$$ |                        
$$ |  $$ |$$ |     $$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |                        
\$$$$$$  |$$$$$$$$\ $$$$$$  |$$$$$$$  |$$ |  $$ |$$$$$$$$\                   
 \______/ \________|\______/ \_______/ \__|  \__|\________|                  
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\ 
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__|
//============================================================================//
//                                 GLOBAL FUNCTIONS                           //
//============================================================================*/
BLE_ADVERTISING_DEF(m_advertising);   /**< Advertising module instance. */
	//NRF_SDH_BLE_OBSERVER(m_advertising_ble_obs, BLE_ADV_BLE_OBSERVER_PRIO, ble_advertising_on_ble_evt, &m_advertising);
	//NRF_SDH_SOC_OBSERVER(m_advertising_soc_obs, BLE_ADV_SOC_OBSERVER_PRIO, ble_advertising_on_sys_evt, &m_advertising);





//-----------------------------------------------------------------------------------------------------------------------------------
//			m_comm_adv_init
//-----------------------------------------------------------------------------------------------------------------------------------
ret_code_t m_comm_adv_init(void)
{			
	________________________________LOG_mCommAdv_funcHeads(FUNCHEAD_FLAG,"");
	adv_init();
	peer_manager_init();
	
	if(sizeof(m_comm_adv_config_t)>DRV_FDS_M_COMM_ADV_SIZE)
	{
		DEB("m_comm_adv_init: ERROR: m_comm_adv_config_t(%d) > DRV_FDS_M_COMM_ADV_SIZE(%d)", sizeof(m_comm_adv_config_t), DRV_FDS_M_COMM_ADV_SIZE);
	}

	//-----------------------------------------------------------------------------
	// m_comm_adv_init() 안에 있는 다음 함수호출을 막고, 'RestoreInfoFromFlash()'로 대체함.
	//-----------------------------------------------------------------------------
	//	memset(&m_adv_config, 0, sizeof(m_comm_adv_config_t));
	//
	//	//rv = drv_fds_read(DRV_FDS_PAGE_M_COMM_ADV_CONFIG, (uint8_t *)&m_adv_config, sizeof(m_adv_config));
	//	//PRINT_IF_ERROR("drv_fds_read", rv);
	//	rv = fds_read_stanley(FDS_FILE_ID_STANLEY, FDS_RECORD_KEY_M_COMM_ADV_CONFIG, sizeof(m_adv_config), (uint8_t *)&m_adv_config);
// <--- main.c/config_init()으로 옮김 -->
	
#if 0	
	// TODO: m_adv_config 값을 체크하여 이상하면 초기화
	// 주소가 이상하다든지, peer id 값이 이상하다든지
	DEB("m_comm_adv_init: TODO: ....\n");
	for(uint32_t index=M_COMM_ADV_CHANNEL_BT1; index<M_COMM_ADV_CHANNEL_MAX; index++)
	{
		DEB("\t m_adv_config.config[%d].id = %d :",index, m_adv_config.config[index].id);
		for(int i=0; i < 6; i++)
		{
			DEB(" %02x", m_adv_config.config[index].g_addr.addr[i]);
		}
		DEB("\n");
	}
#endif
		
	set_adv_status(M_COMM_ADV_STATUS_READY, __func__);
	m_adv_channel = M_COMM_ADV_CHANNEL_MAX; 		
	return NRF_SUCCESS;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			m_comm_adv_start
//-----------------------------------------------------------------------------------------------------------------------------------
ret_code_t m_comm_adv_start(m_comm_adv_channel_type_t ch)
{
	________________________________LOG_mCommAdv_funcHeads(FUNCHEAD_FLAG,"");
	ret_code_t err_code = NRF_SUCCESS;
	
	//DEB("m_comm_adv_start: TODO: ????? \n");

	//	/* Often, when securing fails, it shouldn't be restarted, for security reasons.
	//         * Other times, it can be restarted directly.
	//         * Sometimes it can be restarted, but only after changing some Security Parameters.
	//         * Sometimes, it cannot be restarted until the link is disconnected and reconnected.
	//         * Sometimes it is impossible, to secure the link, or the peer device does not support it.
	//         * How to handle this error is highly application dependent.
	//		   */

	DEB("m_comm_adv_start: (ch=%d, peer_id=%d) \n", ch, m_adv_config.config[ch].id);

	// 연결이 되어 있으면 (즉, m_conn_handle != INVALID 이면)
	if( m_comm_ble_get_status() != BLE_CONN_HANDLE_INVALID) 
	{
		DEB("m_comm_adv_start: #### m_conn_handle != BLE_CONN_HANDLE_INVALID #### \n");
		
		// Disconnect. Disconnected event will trigger advertising.
		DEB("m_comm_adv_start: set_adv_status(M_COMM_ADV_WAITE_GAP_DISCONNECT) \n");
		set_adv_status(M_COMM_ADV_WAITE_GAP_DISCONNECT, __func__);
		//
		//DEB("m_comm_adv_start: conn_handle= m_comm_ble_get_status() \n");
		uint16_t conn_handle_tmp = m_comm_ble_get_status();

		DEB("m_comm_adv_start: sd_ble_gap_disconnect(conn_handle=0x%x) \n",conn_handle_tmp);
		err_code = sd_ble_gap_disconnect( conn_handle_tmp, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION );
		//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(err_code);
	}
	else
	{
		DEB("m_comm_adv_start: #### m_conn_handle==BLE_CONN_HANDLE_INVALID #### \n");

		// Re_init for device name change
		DEB("m_comm_adv_start: adv_init() - Re_init for device name change \n");
		adv_init();
		DEB("m_comm_adv_start: (ch=%d, peer_id=%d) \n", ch, m_adv_config.config[ch].id);

		//if(m_adv_status == M_COMM_ADV_STATUS_IN_ADV)
		{
		// Not connected; can start advertising right away.
			DEB("m_comm_adv_start: sd_ble_gap_adv_stop() - Not connected; can start advertising right away. \n");
		sd_ble_gap_adv_stop();
		}

		//
		DEB("m_comm_adv_start: set_adv_status(M_COMM_ADV_STATUS_READY) \n");
		set_adv_status(M_COMM_ADV_STATUS_READY, __func__);
		DEB("m_comm_adv_start: (ch=%d, peer_id=%d) \n", ch, m_adv_config.config[ch].id);
		//
		DEB("m_comm_adv_start: adv_start(%d)\n",ch);
		adv_start(ch);
	}

	//DEB("m_comm_adv_start: END \n");
	return err_code;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			m_comm_adv_disconnect
//-----------------------------------------------------------------------------------------------------------------------------------
ret_code_t m_comm_adv_disconnect(void)
{	
	________________________________LOG_mCommAdv_funcHeads(FUNCHEAD_FLAG,"");
	ret_code_t err_code = NRF_SUCCESS;

	//DEB("m_comm_adv_disconnect: BEGIN");
	
	if(m_comm_ble_get_status() != BLE_CONN_HANDLE_INVALID) // 연결이 되어 있다면 
	{
		//DEB("m_comm_adv_disconnect: 'Connected' if(m_conn_handle != BLE_CONN_HANDLE_INVALID) \n");
		
		// Disconnect. Disconnected event will trigger advertising.
		//DEB("m_comm_adv_disconnect: sd_ble_gap_disconnect() - Disconnect. Disconnected event will trigger advertising. \n");
		err_code = sd_ble_gap_disconnect(m_comm_ble_get_status(), BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
		//APP_ERROR_CHECK(err_code);
	}
	else
	{
		//DEB("m_comm_adv_disconnect: 'Not Connected' if(m_conn_handle == BLE_CONN_HANDLE_INVALID) \n");

		// Not connected; can start advertising right away.
		//DEB("m_comm_adv_disconnect: sd_ble_gap_adv_stop() - Can start advertising right away.\n");
		sd_ble_gap_adv_stop(); // <--- sd_ble_gap_adv_start() ???????????????????
	}

	//DEB("m_comm_adv_disconnect: set_adv_status(M_COMM_ADV_STATUS_STOP, %s) \n",__func__);
	set_adv_status(M_COMM_ADV_STATUS_STOP, __func__);
	
	//DEB("m_comm_adv_disconnect: END");
	
	return err_code;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			m_comm_adv_clear_bonds
//-----------------------------------------------------------------------------------------------------------------------------------
ret_code_t m_comm_adv_clear_bonds(void)
{	
	________________________________LOG_mCommAdv_funcHeads(FUNCHEAD_FLAG,"");
    ret_code_t rv = NRF_SUCCESS;
	//m_comm_adv_channel_type_t ch;
    
	DEB("m_comm_adv_clear_bonds: BEGIN\n");

	// DO NOT change the step order

	// Step1) Stop application timer for pending
	// This timer will start by Peer Manager PM_EVT_PEERS_DELETE_SUCCEEDED
	DEB("m_comm_adv_clear_bonds: <step1> m_timer_stop() \n");
//jaehong	m_timer_stop();

	// Step2) deletes all bonding data
	DEB("m_comm_adv_clear_bonds: <step2> pm_peers_delete() \n");
	rv = pm_peers_delete();
	//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(rv);
	
	// Step3) deletes all mokibo channel data
	//DEB("m_comm_adv_clear_bonds: <step3> m_comm_adv_set_config_default() \n");
//jaehong	m_comm_adv_set_config_default();
	
	// Step4) save all mokibo channel data
	//DEB("m_comm_adv_clear_bonds: <step4> save all mokibo channel data \n");
	
//	rv = drv_fds_save_immediately(); PRINT_IF_ERROR("drv_fds_save_immediately()", rv);
//	rv= fdw_write_stanley(....);

	//DEB("m_comm_adv_clear_bonds: END\n");
	return rv;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			m_comm_adv_ready_bond
//-----------------------------------------------------------------------------------------------------------------------------------
ret_code_t m_comm_adv_ready_bond(m_comm_adv_channel_type_t ch)
{	
	________________________________LOG_mCommAdv_funcHeads(FUNCHEAD_FLAG,"");
    ret_code_t rv;
	//pm_peer_id_t            peer_id;

	//DEB("m_comm_adv_ready_bond: BEGIN\n");

	// Bonding 정보가 저장되어 있다면 지움
	// PM_PEER_ID_INVALID              0xFFFF
	if((M_COMM_ADV_BOND_INVALID != m_adv_config.config[ch].id) &&(  M_COMM_ADV_BOND_READY != m_adv_config.config[ch].id) )
	{
		DEB("m_comm_adv_ready_bond(%d): pm_peer_delete(%d) \n", ch, m_adv_config.config[ch].id );
		rv = pm_peer_delete(m_adv_config.config[ch].id); PRINT_IF_ERROR("pm_peer_delete()", rv);
		
		DEB("m_comm_adv_start: set_adv_status(M_COMM_ADV_WAITE_GAP_DISCONNECT) \n");
		set_adv_status(M_COMM_ADV_WAITE_GAP_DISCONNECT, __func__);
	}
	else
		set_adv_status(M_COMM_ADV_STATUS_READY, __func__);
	
	DEB("m_comm_adv_ready_bond: bond_set(ch=%d, M_COMM_ADV_BOND_READY) \n", ch);	
	bond_set(ch, M_COMM_ADV_BOND_READY);

	//
	//DEB("m_comm_adv_ready_bond: END\n");
	return NRF_SUCCESS;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			m_comm_adv_set_status
//-----------------------------------------------------------------------------------------------------------------------------------
void m_comm_adv_set_status(m_comm_adv_status_t status)
{	
	________________________________LOG_mCommAdv_funcHeads(FUNCHEAD_FLAG,"");
#if 0 /////////	
	DEB("m_comm_adv_set_status: set_adv_status(status=");
	switch(status)
	{
		case M_COMM_ADV_STATUS_NONE: 			DEB("M_COMM_ADV_STATUS_NONE"); break;
		case M_COMM_ADV_STATUS_READY:			DEB("M_COMM_ADV_STATUS_READY"); break;
		case M_COMM_ADV_WAITE_GAP_DISCONNECT:	DEB("M_COMM_ADV_WAITE_GAP_DISCONNECT"); break;
		case M_COMM_ADV_STATUS_IN_ADV:			DEB("M_COMM_ADV_STATUS_IN_ADV"); break;
		case M_COMM_ADV_STATUS_CONNECTED:		DEB("M_COMM_ADV_STATUS_CONNECTED"); break;
		case M_COMM_ADV_STATUS_STOP:			DEB("M_COMM_ADV_STATUS_STOP"); break;
	}
	DEB(",%s)\n",__func__);
#endif
	
	//
	set_adv_status(status, __func__);
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			m_comm_adv_get_status
//-----------------------------------------------------------------------------------------------------------------------------------
m_comm_adv_status_t m_comm_adv_get_status(void)
{
	________________________________LOG_mCommAdv_funcHeads(FUNCHEAD_FLAG,"");
	return m_adv_status;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			m_comm_adv_get_addr
//-----------------------------------------------------------------------------------------------------------------------------------
pm_peer_id_t m_comm_adv_get_addr(m_comm_adv_channel_type_t ch)
{	
	________________________________LOG_mCommAdv_funcHeads(FUNCHEAD_FLAG,"");
	return m_adv_config.config[ch].id;
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			m_comm_adv_get_addr
//-----------------------------------------------------------------------------------------------------------------------------------
void m_comm_adv_set_addr(m_comm_adv_channel_type_t ch, pm_peer_id_t new)
{	
	________________________________LOG_mCommAdv_funcHeads(FUNCHEAD_FLAG,"");
	//DEB("m_comm_adv_set_addr: BEGIN\n");
	
	// New
	if(new != m_adv_config.config[ch].id)
	{
		// Update & save
		//DEB("m_comm_adv_set_addr: bond_set(ch=%d, peer_id=0x%x)\n",ch,new);
		bond_set(ch, new);
	}
	else 
	{
		// nothing
		//DEB("m_comm_adv_set_addr: same! --> DO NOT 'bond_set(ch,peer_id)' \n");
	}
	//DEB("m_comm_adv_set_addr: END\n");
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			m_comm_adv_set_config_default
//-----------------------------------------------------------------------------------------------------------------------------------
ret_code_t m_comm_adv_set_config_default(void)
{	
	________________________________LOG_mCommAdv_funcHeads(FUNCHEAD_FLAG,"");
    ret_code_t rv;
	uint32_t  val;

	m_comm_adv_config_t adv_config;

	//DEB("m_comm_adv_set_config_default: BEGIN\n");
	
	// Set to zero
	memset(&adv_config, 0, sizeof(adv_config));
	for(val=0; val<M_COMM_ADV_CHANNEL_MAX; val++)
	{
		adv_config.config[val].id = PM_PEER_ID_INVALID;
	}
	//
	//DEB("m_comm_adv_set_config_default: GLOBAL: m_adv_config <--- LOCAL: adv_config ( !! by Stanley !! )\n");
	memcpy(&m_adv_config, &adv_config, sizeof(m_comm_adv_config_t));
	
	//
	//debshow_comm_adv_config("m_comm_adv_set_config_default:",adv_config);


	//rv = drv_fds_write(DRV_FDS_PAGE_M_COMM_ADV_CONFIG, (uint8_t *)&adv_config,  sizeof(adv_config));
	//PRINT_IF_ERROR("drv_fds_write()", rv);
//	rv= fds_write_stanley(FDS_FILE_ID_STANLEY, FDS_RECORD_KEY_M_COMM_ADV_CONFIG, (uint8_t *)&adv_config, sizeof(adv_config) );
//	rv=FDS_SUCCESS;


	//DEB("m_comm_adv_set_config_default: END\n");
	return rv;
}


#if MOKIBO_NFC_ENABLED
//-----------------------------------------------------------------------------------------------------------------------------------
//			m_comm_adv_get_status
//-----------------------------------------------------------------------------------------------------------------------------------
ble_advertising_t * m_comm_adv_instance_ptr_get(void)
{
    return &m_advertising;
}

#endif//MOKIBO_NFC_ENABLED























/*\       $$$$$$\   $$$$$$\   $$$$$$\  $$\                                   
$$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                                  
$$ |     $$ /  $$ |$$ /  \__|$$ /  $$ |$$ |                                  
$$ |     $$ |  $$ |$$ |      $$$$$$$$ |$$ |                                  
$$ |     $$ |  $$ |$$ |      $$  __$$ |$$ |                                  
$$ |     $$ |  $$ |$$ |  $$\ $$ |  $$ |$$ |                                  
$$$$$$$$\ $$$$$$  |\$$$$$$  |$$ |  $$ |$$$$$$$$\                             
\________|\______/  \______/ \__|  \__|\________|                            
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\ 
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__|
//============================================================================//
//                                 LOCAL FUNCTIONS                            //
//============================================================================*/

//-----------------------------------------------------------------------------------------------------------------------------------
//			bond_set
//-----------------------------------------------------------------------------------------------------------------------------------
static void bond_set(m_comm_adv_channel_type_t ch, pm_peer_id_t val)
{
	________________________________LOG_mCommAdv_funcHeads(FUNCHEAD_FLAG,"");
	//ret_code_t rv;

	//DEB("bond_set: BEGIN\n");
	
	if(ch < M_COMM_ADV_CHANNEL_MAX)
	{
		//DEB("bond_set: m_adv_config.config[ch=%d].id = %d \n", ch, val);
		m_adv_config.config[ch].id = val;

		//rv = drv_fds_write(DRV_FDS_PAGE_M_COMM_ADV_CONFIG, (uint8_t *)&m_adv_config,  sizeof(m_adv_config));
		//PRINT_IF_ERROR("drv_fds_write()", rv);
		//fds_write_stanley(FDS_FILE_ID_STANLEY, FDS_RECORD_KEY_M_COMM_ADV_CONFIG, (uint8_t *)&m_adv_config, sizeof(m_adv_config) );
		if( is_BT_channel(ch) )
		{
			fds_RequestToSaveAfterThisTime(ch,1000);
		}
		
	}
	else
	{
		//DEB("bond_set: invalid ch=%d \n", ch);
	}
	//DEB("bond_set: END\n");
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			is_valid_ch
//-----------------------------------------------------------------------------------------------------------------------------------
static bool is_valid_ch(m_comm_adv_channel_type_t ch)
{
	________________________________LOG_mCommAdv_funcHeads(FUNCHEAD_FLAG,"");
	bool rv = false;

	if(	   (M_COMM_ADV_CHANNEL_BT1 == ch) 
		|| (M_COMM_ADV_CHANNEL_BT2 == ch)
		|| (M_COMM_ADV_CHANNEL_BT3 == ch))
	{
		rv = true;
	}

	return rv;
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			set_adv_status
//-----------------------------------------------------------------------------------------------------------------------------------
static void set_adv_status(m_comm_adv_status_t status, const char *caller)
{
	________________________________LOG_mCommAdv_funcHeads(FUNCHEAD_FLAG,"");
	#if defined(M_COMM_ADV_TRACE)
		DEB("set_adv_status: BEGIN\n");
	#endif

	//if(status != m_adv_status)
	{
		#if defined(M_COMM_ADV_TRACE)
		// TODO: Use stringizing(#) of preprocessor macros
		static const char *String[] = \
		{ \
			"ADV_NONE", \
			"ADV_READY", \
			"ADV_WAITE_GAP_DISCONNECT", \
			"ADV_IN_ADV", \
			"ADV_CONNECTED",\
			"ADV_STOP", \
		};
		DEB("set_adv_status: m_adv_status[%s===>%s] called by %s \n", String[m_adv_status], String[status], caller);
		#endif

		// Advertising이 시작할 경우, blink start
		if( (M_COMM_ADV_STATUS_IN_ADV != m_adv_status) &&  (M_COMM_ADV_STATUS_IN_ADV == status) )
		{
			m_led_set_status(M_LED_FAST_BLINK);
		}

		m_adv_status = status;
	}

	#if defined(M_COMM_ADV_TRACE)
		DEB("set_adv_status: END\n");
	#endif
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			adv_init
//-----------------------------------------------------------------------------------------------------------------------------------
//	Function for initializing the Advertising functionality.
static void adv_init(void)
{	
	________________________________LOG_mCommAdv_funcHeads(FUNCHEAD_FLAG,"");
    uint32_t               err_code;
    uint8_t                adv_flags;
	ble_advertising_init_t init;

	// clear buffer
    memset(&init, 0, sizeof(init));

    adv_flags                            = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
//jaehong    	adv_flags                            = BLE_GAP_ADV_FLAGS_LE_ONLY_LIMITED_DISC_MODE;
    init.advdata.name_type               = BLE_ADVDATA_FULL_NAME;
    init.advdata.include_appearance      = true;
    init.advdata.flags                   = adv_flags;
    init.advdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    init.advdata.uuids_complete.p_uuids  = m_adv_uuids;

    //init.config.ble_adv_whitelist_enabled      = true;
    init.config.ble_adv_whitelist_enabled      = false;
    init.config.ble_adv_directed_enabled       = true;
    init.config.ble_adv_directed_slow_enabled  = false;
    init.config.ble_adv_directed_slow_interval = 0;
    init.config.ble_adv_directed_slow_timeout  = 0;
    init.config.ble_adv_fast_enabled           = true;
    init.config.ble_adv_fast_interval          = APP_ADV_FAST_INTERVAL;
    init.config.ble_adv_fast_timeout           = APP_ADV_FAST_TIMEOUT;
    init.config.ble_adv_slow_enabled           = false;
    init.config.ble_adv_slow_interval          = APP_ADV_SLOW_INTERVAL;
    init.config.ble_adv_slow_timeout           = APP_ADV_SLOW_TIMEOUT;

    init.evt_handler   = on_adv_evt;
    init.error_handler = adv_error_handler;

    err_code = ble_advertising_init(&m_advertising, &init);
    //deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(err_code);

    ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);
	
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			adv_start
//-----------------------------------------------------------------------------------------------------------------------------------
// Function for starting advertising.
static ret_code_t adv_start(m_comm_adv_channel_type_t ch)
{
	________________________________LOG_mCommAdv_funcHeads(FUNCHEAD_FLAG,"");
	ret_code_t rv;
	//
	if(!is_valid_ch(ch))
	{
		DEB("adv_start[%d]: NRF_ERROR_INVALID_PARAM\n",ch);
		rv = NRF_ERROR_INVALID_PARAM;
	}
	else
	{
		switch(m_adv_status)
		{
			case M_COMM_ADV_STATUS_READY :
		DEB("adv_start[%d]: M_COMM_ADV_STATUS_READY\n",ch);

			#if 0
				if(M_COMM_ADV_BOND_INVALID == m_adv_config.config[ch].id)
				{
					DEB("adv_start[%d]: NRF_ERROR_NOT_FOUND\n",ch);
					rv = NRF_ERROR_NOT_FOUND;
				}
				else if(M_COMM_ADV_BOND_READY == m_adv_config.config[ch].id)
			#else
				if(M_COMM_ADV_BOND_READY == m_adv_config.config[ch].id ||
				M_COMM_ADV_BOND_INVALID == m_adv_config.config[ch].id)
			#endif
				{
					// Start new bonding advertising here
					m_adv_channel = ch; 
					
					//static ble_gap_addr_t new_addr =
					ble_gap_addr_t new_addr =
					{
						/* Possible values for addr_type:
						   BLE_GAP_ADDR_TYPE_PUBLIC,
						   BLE_GAP_ADDR_TYPE_RANDOM_STATIC,
						   BLE_GAP_ADDR_TYPE_RANDOM_PRIVATE_RESOLVABLE,
						   BLE_GAP_ADDR_TYPE_RANDOM_PRIVATE_NON_RESOLVABLE. */
						.addr_type = BLE_GAP_ADDR_TYPE_RANDOM_STATIC,
						// last two upper bits must be "1"
						//.addr      = {0x8D, 0xFE, 0x23, 0x86, 0x00, 0xC0}
						.addr      = {0x00, 0x00, 0x00, 0x00, 0xA0, 0xC0}
					};

					#if defined(M_COMM_ADV_NEW_ADDR)
						uint32_t unique_id;
					
					// Use fixed address
					unique_id = m_util_get_unique_device_id();

					// Use 4-bytes random chip id as mac address	
					memcpy(&(new_addr.addr), &unique_id, sizeof(unique_id));
					
						// 4th address used for new	address
					
						#if M_COMM_ADV_STATIC_ADDR == 1
							
							uint16_t random = (uint16_t)(0x11 % 0xFFFF);
							// change last 2-bytes by random
							memcpy(&(new_addr.addr[2]), &random, sizeof(random));
							
							new_addr.addr[4] += (uint8_t)ch;							
							________________________________DBG_flimbs_20190419_mCommAdv_staticAddr(6,"macAddr:%x %x %x - %x %x %x ",new_addr.addr[0], new_addr.addr[1],new_addr.addr[2],new_addr.addr[3],new_addr.addr[4],new_addr.addr[5]);
						#else
							uint16_t random = (uint16_t)(m_timer_get_counter()%0xFFFF);
							// change last 2-bytes by random	
							memcpy(&(new_addr.addr[2]), &random, sizeof(random));
							new_addr.addr[4] += (uint8_t)ch;
							
						#endif

					#else
						m_util_get_mac_address(ch, &new_addr);	// add by jaehong, 2019/06/28
					#endif

					//
					//	DEB("adv_start: Setting ch=%d, new_addr=",ch);
					//	for(int i=0; i < 6; i++)
					//	{
					//		DEB(" %02x",new_addr.addr[i]);
					//	}
					//	DEB("\n");

					// Set the local address.
					rv = pm_id_addr_set(&new_addr);	PRINT_IF_ERROR("pm_id_addr_set()", rv);
					if (rv != NRF_SUCCESS)
					{
						//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(rv);
           				//DEBUG("TODO!!!!error[%d] handle", rv);
					}

					// Clear whiltelist
					pm_whitelist_set(NULL, 0);
					
					DEB("adv_start: ble_advertising_start(%d) to 0x%X by BLE_ADV_MODE_FAST\n",ch, new_addr);

					set_adv_status(M_COMM_ADV_STATUS_IN_ADV, __func__);
					
					// BLE_ADV_MODE_FAST: Fast advertising will connect to any peer device, or filter with a whitelist if one exists.
					rv = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
					//
					//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(rv);
				}
				else // Start already bonding advertising here 
				{
					ble_gap_addr_t addr;
					m_adv_channel = ch; 
					//
					memcpy(&addr, &(m_adv_config.config[ch].g_addr), sizeof(ble_gap_addr_t));
					rv = pm_id_addr_set(&addr);	PRINT_IF_ERROR("pm_id_addr_set()", rv);
					DEB("adv_start: ble_advertising_start(%d) to 0x%X by BLE_ADV_MODE_DIRECTED\n",ch, addr);
					
					rv = ble_advertising_start(&m_advertising, BLE_ADV_MODE_DIRECTED);

					set_adv_status(M_COMM_ADV_STATUS_IN_ADV, __func__);
					
					if (NRF_SUCCESS != rv)
					{
						//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(rv);
						//DEBUG("TODO: Error handle[%d]", rv);
					}
				}
				break;

			case M_COMM_ADV_STATUS_NONE :
			case M_COMM_ADV_STATUS_IN_ADV :
			case M_COMM_ADV_STATUS_CONNECTED :
			case M_COMM_ADV_STATUS_STOP :
		DEB("adv_start[%d]: NRF_ERROR_INVALID_STATE\n",ch);
				rv = NRF_ERROR_INVALID_STATE;
				break;

			default :
				rv = NRF_ERROR_INVALID_STATE;
			break;
		}
	}

	return rv;
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			peer_manager_init
//-----------------------------------------------------------------------------------------------------------------------------------
// Function for the Peer Manager initialization.
static void peer_manager_init(void)
{	
	________________________________LOG_mCommAdv_funcHeads(FUNCHEAD_FLAG,"");
    ret_code_t           err_code;
	ble_gap_sec_params_t sec_param;
	
	//
    err_code = pm_init(); //APP_ERROR_CHECK(err_code);

    memset(&sec_param, 0, sizeof(ble_gap_sec_params_t));

	//#define SEC_PARAM_BOND                      1                               /**< Perform bonding. */
	//#define SEC_PARAM_MITM                      1                               /**< Man In The Middle protection not required. */
	//#define SEC_PARAM_LESC                      0                               /**< LE Secure Connections not enabled. */
	//#define SEC_PARAM_KEYPRESS                  0                               /**< Keypress notifications not enabled. */
	////#define SEC_PARAM_IO_CAPABILITIES         BLE_GAP_IO_CAPS_NONE            /**< No I/O capabilities. */
	//#define SEC_PARAM_IO_CAPABILITIES           BLE_GAP_IO_CAPS_KEYBOARD_ONLY   /**< Keyboard Only. */
	//#define SEC_PARAM_OOB                       0                               /**< Out Of Band data not available. */
	//#define SEC_PARAM_MIN_KEY_SIZE              7                               /**< Minimum encryption key size. */
	//#define SEC_PARAM_MAX_KEY_SIZE              16                              /*< Maximum encryption key size. */

    // Security parameters to be used for all security procedures.
    sec_param.bond           = SEC_PARAM_BOND;
    sec_param.mitm           = SEC_PARAM_MITM;
    sec_param.lesc           = SEC_PARAM_LESC;
    sec_param.keypress       = SEC_PARAM_KEYPRESS;
    sec_param.io_caps        = SEC_PARAM_IO_CAPABILITIES;
    sec_param.oob            = SEC_PARAM_OOB;
    sec_param.min_key_size   = SEC_PARAM_MIN_KEY_SIZE;
    sec_param.max_key_size   = SEC_PARAM_MAX_KEY_SIZE;
    sec_param.kdist_own.enc  = 1;
    sec_param.kdist_own.id   = 1;
    sec_param.kdist_peer.enc = 1;
    sec_param.kdist_peer.id  = 1;

#if 0
		DEB("sec_param: bond=%, mitm=%d, lesc=%d, keypress=%d, io_caps=%d, oob=%d, minkeysize=%d, maxkeysize=%d,",
			sec_param.bond,
			sec_param.mitm,
			sec_param.lesc,
			sec_param.keypress,
			sec_param.io_caps,
			sec_param.oob,
			sec_param.min_key_size,
			sec_param.max_key_size
		);
		DEB(" kdist_own.enc=%d, kdist_own.id=%d, kdist_peer.enc=%d, kdist_peer.id=%d",
			sec_param.kdist_own.enc,
			sec_param.kdist_own.id,
			sec_param.kdist_peer.enc,
			sec_param.kdist_peer.id
		);
		DEB("\n");
#endif

    err_code = pm_sec_params_set(&sec_param);
	//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(err_code);

    err_code = pm_register(pm_evt_handler);
	//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(err_code);
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			pm_evt_handler
//-----------------------------------------------------------------------------------------------------------------------------------
// Function for handling Peer Manager events.
static void pm_evt_handler(pm_evt_t const * p_evt)
{	
	________________________________LOG_mCommAdv_funcHeads(FUNCHEAD_FLAG,"");
	ble_gap_addr_t addr;
    ret_code_t err_code;

	________________________________LOG_mCommadv_peerManagerEvt(0,"p_evt->evt_id:%d(CON:%d,SUC:%d,FAIL:%d,C_REQ:%d,P_REQ:%d,FULL:%d,DELSUC:%d,%d,PEEROK:%d,%d,%d,%d,STRT:%d,%d,DB:%d,%d,%d,GBG:19)",
	p_evt->evt_id,PM_EVT_BONDED_PEER_CONNECTED,PM_EVT_CONN_SEC_SUCCEEDED,PM_EVT_CONN_SEC_FAILED,PM_EVT_CONN_SEC_CONFIG_REQ,PM_EVT_CONN_SEC_PARAMS_REQ,PM_EVT_STORAGE_FULL,PM_EVT_PEERS_DELETE_SUCCEEDED,PM_EVT_LOCAL_DB_CACHE_APPLY_FAILED,PM_EVT_PEER_DATA_UPDATE_SUCCEEDED,PM_EVT_PEER_DATA_UPDATE_FAILED,PM_EVT_PEER_DELETE_FAILED,PM_EVT_ERROR_UNEXPECTED,PM_EVT_CONN_SEC_START,PM_EVT_PEER_DELETE_SUCCEEDED,PM_EVT_LOCAL_DB_CACHE_APPLIED,PM_EVT_SERVICE_CHANGED_IND_SENT,PM_EVT_SERVICE_CHANGED_IND_CONFIRMED)
    switch (p_evt->evt_id)
    {
        case PM_EVT_BONDED_PEER_CONNECTED:					
			DEB("pm_evt_handler: PM_EVT_BONDED_PEER_CONNECTED === Connected to a previously bonded device.[%d] ===\n", p_evt->peer_id);
			//PMDEB("pm_evt_handler: pm_peer_rank_highest(peer_id=%d)\n",p_evt->peer_id);
			pm_peer_rank_highest(p_evt->peer_id);
			break;

        case PM_EVT_CONN_SEC_SUCCEEDED:
			DEB("pm_evt_handler: PM_EVT_CONN_SEC_SUCCEEDED - Connection secured: role: %d, conn_handle: 0x%x, peer_id: %d, procedure: %d.\n",
							 ble_conn_state_role(p_evt->conn_handle),
							 p_evt->conn_handle, p_evt->peer_id,
							 p_evt->params.conn_sec_succeeded.procedure
				);

			DEB("ble_evt_handler: m_comm_adv_set_status(M_COMM_ADV_STATUS_CONNECTED)\n");
			m_comm_adv_set_status(M_COMM_ADV_STATUS_CONNECTED);
#if 1
			switch(m_mokibo_get_status())
			{
				case M_MOKIBO_KEYBOARD_STATUS :
					// 연결이 되는 순간 키보드 모드이면
					// 현재 채널 Color + Dimming out으로 표시한다
					//DEB("ble_evt_handler: m_mokibo_set_touch_color() \n");
					m_mokibo_set_touch_color();

					//DEB("ble_evt_handler: m_led_set_status(M_LED_DIMMING_OUT) \n");
					m_led_set_status(M_LED_DIMMING_OUT);
					break;
			
				case M_MOKIBO_TOUCH_STATUS: 
					//DEB("ble_evt_handler: m_mokibo_set_touch_color()\n");
					m_mokibo_set_touch_color();

					//DEB("ble_evt_handler: m_led_set_status(M_LED_ON) \n");
					m_led_set_status(M_LED_ON);
					break;
			}
#endif			
			if(M_COMM_ADV_CHANNEL_MAX != m_adv_channel)
			{
				if(M_COMM_ADV_BOND_READY == m_adv_config.config[m_adv_channel].id)
				{
					DEB("pm_evt_handler: pm_id_addr_get() \n");
					err_code = pm_id_addr_get(&addr);
					if(NRF_SUCCESS != err_code)
					{
						DEB("pm_evt_handler: pm_id_addr_get() fail:%d \n", err_code);
					}
					else
					{
						DEB("pm_evt_handler: m_adv_config.config[m_adv_channel(%d)].id = newbond peer_id(0x%d) \n",m_adv_channel,p_evt->peer_id);
						m_adv_config.config[m_adv_channel].id = p_evt->peer_id;
						
						DEB("Adding m_adv_config.config[%d].id <--- New Bond[%d],", m_adv_channel, p_evt->peer_id);
						DEB("Loaded Addr= [%02x][%02x][%02x][%02x][%02x][%02x]",
										addr.addr[0], addr.addr[1], addr.addr[2],
										addr.addr[3], addr.addr[4], addr.addr[5]);
						//
						memcpy(&(m_adv_config.config[m_adv_channel].g_addr), &addr, sizeof(ble_gap_addr_t));
						//
						//debshow_comm_adv_config("pm_evt_handler:toSave",m_adv_config);
						
						fds_RequestToSaveAfterThisTime(m_adv_channel,1000);

						//err_code = drv_fds_write(DRV_FDS_PAGE_M_COMM_ADV_CONFIG, (uint8_t *)&m_adv_config,  sizeof(m_adv_config));
						//err_code= fds_write_stanley(FDS_FILE_ID_STANLEY, FDS_RECORD_KEY_M_COMM_ADV_CONFIG, (uint8_t *)&m_adv_config, sizeof(m_adv_config) );
						//if(NRF_SUCCESS != err_code)
						//{
						//	PRINT_IF_ERROR("drv_fds_write(DRV_FDS_ACCESS_M_COMM_ADV_CONFIG)", err_code);
						//}
						//else
						//{
						//	PMDEB("pm_evt_handler: Stored Addr= ");
						//	for(int i=0; i < 6; i++) PMDEB(" %02x",m_adv_config.config[m_adv_channel].g_addr.addr[i]);
						//	PMDEB("\n");
						//}
					}
				}//M_COMM_ADV_BOND_READY
				else
				{
						DEB("pm_evt_handler: m_adv_config.config[m_adv_channel(%d)].id = newbond peer_id(0x%d) \n",m_adv_channel,p_evt->peer_id);
						m_adv_config.config[m_adv_channel].id = p_evt->peer_id;
				}
			}//M_COMM_ADV_CHANNEL_MAX
			else
			{
				//PMDEB("pm_evt_handler: Invalid channel[%d], Skip saving bond[%d]\n", m_adv_channel, p_evt->peer_id);
			}
			//
			break;

        case PM_EVT_CONN_SEC_FAILED: //3
				m_mokibo_set_touch_color();

					//DEB("ble_evt_handler: m_led_set_status(M_LED_DIMMING_OUT) \n");
				m_led_set_status(M_LED_DIMMING_OUT);
			/* Often, when securing fails, it shouldn't be restarted, for security reasons.
			 * Other times, it can be restarted directly.
			 * Sometimes it can be restarted, but only after changing some Security Parameters.
			 * Sometimes, it cannot be restarted until the link is disconnected and reconnected.
			 * Sometimes it is impossible, to secure the link, or the peer device does not support it.
			 * How to handle this error is highly application dependent. */
			DEB("pm_evt_handler: PM_EVT_CONN_SEC_FAILED\n");
			DEB("pm_evt_handler: procedure(0x%x), error(0x%x), error_src(%d)\n",
					p_evt->params.conn_sec_failed.procedure,
					p_evt->params.conn_sec_failed.error,
					p_evt->params.conn_sec_failed.error_src);
#if 0		
			switch (p_evt->params.conn_sec_failed.error)
			{
				case PM_CONN_SEC_ERROR_PIN_OR_KEY_MISSING:
					// Rebond if one party has lost its keys.
					DEB("pm_evt_handler: Rebond if one party has lost its keys.\n");
					err_code = pm_conn_secure(p_evt->conn_handle, true);
					if (err_code != NRF_ERROR_INVALID_STATE)
					{
						//APP_ERROR_CHECK(err_code);
					}
					break;//PM_CONN_SEC_ERROR_PIN_OR_KEY_MISSING
				default:
					break;
			}	
#endif			
			break;

        case PM_EVT_CONN_SEC_CONFIG_REQ: //4
			{// this block is required for the local variable 'conn_sec_config' initialization
				DEB("pm_evt_handler: PM_EVT_CONN_SEC_CONFIG_REQ\n");
				
				// Allow pairing request from an already bonded peer.
				//PMDEB("pm_evt_handler: conn_sec_config.allow_repairing=true; (Allow pairing request from an already bonded peer.) \n");				
				pm_conn_sec_config_t conn_sec_config = {.allow_repairing = true};
					// Or Reject pairing request from an already bonded peer.
					// PMDEB("pm_evt_handler: conn_sec_config.allow_repairing=false; (Reject pairing request from an already bonded peer.)\n");
					//pm_conn_sec_config_t conn_sec_config = {.allow_repairing = false};

				DEB("pm_evt_handler: pm_conn_sec_config_reply(conn_handle=0x%x, conn_sec_config) \n");
				pm_conn_sec_config_reply(p_evt->conn_handle, &conn_sec_config);
			}
			//
			break;

		
		
		case PM_EVT_CONN_SEC_PARAMS_REQ:
			m_mokibo_set_touch_color();
			m_led_set_status(M_LED_ON);

			#if MOKIBO_NFC_ENABLED
			DEB("pm_evt_handler: PM_EVT_CONN_SEC_PARAMS_REQ\n");
			if( m_nfc_state_run() )
			{
				//PMDEB("pm_evt_handler: m_nfc_pm_params_req(p_evt)\n");
				m_nfc_pm_params_req(p_evt);
			}
		#endif
		break;
					
		

        case PM_EVT_STORAGE_FULL: //6
			//PMDEB("pm_evt_handler: ===== PM_EVT_STORAGE_FULL: ===== \n");

            // Run garbage collection on the flash.
            err_code = fds_gc();
            if (err_code == FDS_ERR_BUSY || err_code == FDS_ERR_NO_SPACE_IN_QUEUES)
            {
                // Retry.
            }
            else
            {
                //deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(err_code);
            }
			//
			break;
			
        case PM_EVT_PEERS_DELETE_SUCCEEDED:
			DEB("pm_evt_handler: PM_EVT_PEERS_DELETE_SUCCEEDED\n");
			
			// Call back handler of pm_peers_delete()
			//PMDEB("pm_evt_handler: m_timer_start() \n");
//jaehong			m_timer_start();

			//
			break;
		
        case PM_EVT_LOCAL_DB_CACHE_APPLY_FAILED: //15
			DEB("pm_evt_handler: PM_EVT_LOCAL_DB_CACHE_APPLY_FAILED\n");
			
			// The local database has likely changed, send service changed indications.
			//PMDEB("pm_evt_handler: pm_local_database_has_changed() - The local database has likely changed, send service changed indications.\n");
            pm_local_database_has_changed();
			//
			break;
		
        case PM_EVT_PEER_DATA_UPDATE_SUCCEEDED: //8
			
			DEB("pm_evt_handler: PM_EVT_PEER_DATA_UPDATE_SUCCEEDED\n");
            
			if (     p_evt->params.peer_data_update_succeeded.flash_changed
                 && (p_evt->params.peer_data_update_succeeded.data_id == PM_PEER_DATA_ID_BONDING))
            {
				;
				DEB("pm_evt_handler: flash_changed && data_id==PM_PEER_DATA_ID_BONDING \n");
            }
			break;
			
        case PM_EVT_PEER_DATA_UPDATE_FAILED:
			DEB("pm_evt_handler: PM_EVT_PEER_DATA_UPDATE_FAILED\n");
            
			// Assert.
            //deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(p_evt->params.peer_data_update_failed.error);
			//
			break;

        case PM_EVT_PEER_DELETE_FAILED:
			DEB("pm_evt_handler: PM_EVT_PEER_DELETE_FAILED\n");
            
			// Assert.
            //deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(p_evt->params.peer_delete_failed.error);
			break;
		
        case PM_EVT_PEERS_DELETE_FAILED:
			DEB("pm_evt_handler: PM_EVT_PEERS_DELETE_FAILED\n");
		
            // Assert.
            //deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(p_evt->params.peers_delete_failed_evt.error);
			break;
		
        case PM_EVT_ERROR_UNEXPECTED:
			//PMDEB("pm_evt_handler: PM_EVT_ERROR_UNEXPECTED\n");
		
            // Assert.
            //deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(p_evt->params.error_unexpected.error);
			break;
		
        case PM_EVT_CONN_SEC_START:
			DEB("pm_evt_handler: PM_EVT_CONN_SEC_START\n");
			break;

        case PM_EVT_PEER_DELETE_SUCCEEDED:
			DEB("pm_evt_handler: PM_EVT_PEER_DELETE_SUCCEEDED\n");

			if(M_COMM_ADV_WAITE_GAP_DISCONNECT == m_comm_adv_get_status())
			{
				sd_ble_gap_disconnect(m_comm_ble_get_status(), BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
			}
			break;

        case PM_EVT_LOCAL_DB_CACHE_APPLIED:
			//PMDEB("pm_evt_handler: PM_EVT_LOCAL_DB_CACHE_APPLIED\n");
			break;

        case PM_EVT_SERVICE_CHANGED_IND_SENT:
			//PMDEB("pm_evt_handler: PM_EVT_SERVICE_CHANGED_IND_SENT\n");
			break;

        case PM_EVT_SERVICE_CHANGED_IND_CONFIRMED:
			//PMDEB("pm_evt_handler: PM_EVT_SERVICE_CHANGED_IND_CONFIRMED\n");
			break;

        default:
			//PMDEB("pm_evt_handler: UNKNOWN? \n");
            break;
    }

}
//-----------------------------------------------------------------------------------------------------------------------------------
//			adv_error_handler
//-----------------------------------------------------------------------------------------------------------------------------------
// Function for handling advertising errors.
static void adv_error_handler(uint32_t nrf_error)
{
	________________________________LOG_mCommAdv_funcHeads(FUNCHEAD_FLAG,"");
    //deleted by jaehong, 2019/06/28 APP_ERROR_HANDLER(nrf_error);
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			on_adv_evt
//-----------------------------------------------------------------------------------------------------------------------------------
// Function for handling advertising events.
// This function will be called for advertising events which are passed to the application.
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{	
	________________________________LOG_mCommAdv_funcHeads(FUNCHEAD_FLAG,"");
    ret_code_t err_code;

    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_DIRECTED:
			DEB("on_adv_evt: BLE_ADV_EVT_DIRECTED: Directed advertising.\n");
			if(M_COMM_ADV_STATUS_IN_ADV == m_adv_status)
			{
				DEB("on_adv_evt: m_led_set_status(M_LED_FAST_BLINK) \n");
			m_led_set_status(M_LED_FAST_BLINK);
			}
            break;

        case BLE_ADV_EVT_FAST:
			//DEB("on_adv_evt: BLE_ADV_EVT_FAST: Fast advertising.\n");
			if(M_COMM_ADV_STATUS_IN_ADV == m_adv_status)
			{
			//DEB("on_adv_evt: m_led_set_status(M_LED_FAST_BLINK) \n");
			m_led_set_status(M_LED_FAST_BLINK);
			}
            break;

        case BLE_ADV_EVT_SLOW:
			DEB("on_adv_evt: BLE_ADV_EVT_SLOW: Slow advertising.\n");
			if(M_COMM_ADV_STATUS_IN_ADV == m_adv_status)
			{
				DEB("on_adv_evt: m_led_set_status(M_LED_SLOW_BLINK) \n");
			m_led_set_status(M_LED_SLOW_BLINK);
			}
            break;

        case BLE_ADV_EVT_FAST_WHITELIST:
			DEB("on_adv_evt: BLE_ADV_EVT_FAST_WHITELIST: Fast advertising with whitelist.\n");
			if(M_COMM_ADV_STATUS_IN_ADV == m_adv_status)
			{
				DEB("on_adv_evt: m_led_set_status(M_LED_FAST_BLINK) \n");
			m_led_set_status(M_LED_FAST_BLINK);
			}
            break;

        case BLE_ADV_EVT_SLOW_WHITELIST:
			DEB("on_adv_evt: BLE_ADV_EVT_SLOW_WHITELIST: Slow advertising with whitelist.\n");
			if(M_COMM_ADV_STATUS_IN_ADV == m_adv_status)
			{
				DEB("on_adv_evt: m_led_set_status(M_LED_SLOW_BLINK) \n");
			m_led_set_status(M_LED_SLOW_BLINK);
			}
            break;

        case BLE_ADV_EVT_IDLE:
			DEB("on_adv_evt: BLE_ADV_EVT_IDLE: \n");
			if(M_COMM_ADV_STATUS_IN_ADV == m_adv_status)
			{
				DEB("on_adv_evt: m_led_set_status(M_LED_DIMMING_OUT) \n");
			m_led_set_status(M_LED_DIMMING_OUT);
			}
            break;

        case BLE_ADV_EVT_WHITELIST_REQUEST:
			DEB("on_adv_evt: BLE_ADV_EVT_WHITELIST_REQUEST: \n");
		
			{// this block is required for the local variables 
				//
				ble_gap_addr_t whitelist_addrs[BLE_GAP_WHITELIST_ADDR_MAX_COUNT];
				ble_gap_irk_t  whitelist_irks[BLE_GAP_WHITELIST_ADDR_MAX_COUNT];
				uint32_t       addr_cnt = BLE_GAP_WHITELIST_ADDR_MAX_COUNT;
				uint32_t       irk_cnt  = BLE_GAP_WHITELIST_ADDR_MAX_COUNT;

				//DEB("on_adv_evt: pm_whitelist_get()\n");
				err_code = pm_whitelist_get(whitelist_addrs, &addr_cnt,	whitelist_irks,  &irk_cnt);
				//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(err_code);
				//DEB("on_adv_evt: whitelist: addr_cnt=%d, irk_cnt=%d \n", addr_cnt, irk_cnt);

				// Apply the whitelist.
				//DEB("on_adv_evt: Apply_the_white_list: ble_advertising_whitelist_reply() \n");
				err_code = ble_advertising_whitelist_reply(
					&m_advertising, whitelist_addrs, addr_cnt, whitelist_irks, irk_cnt
				);
				//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(err_code);
			}
			break;

        case BLE_ADV_EVT_PEER_ADDR_REQUEST:
			
			DEB("on_adv_evt: BLE_ADV_EVT_PEER_ADDR_REQUEST\n");
			{
				pm_peer_data_bonding_t peer_bonding_data;

				// Only Give peer address if we have a handle to the bonded peer.
				if ((m_adv_config.config[m_adv_channel].id != PM_PEER_ID_INVALID) && (M_COMM_ADV_CHANNEL_MAX != m_adv_channel))
				{
					//
					//DEB("on_adv_evt: m_adv_config.config[m_adv_channel:%d].id=%d \n", m_adv_channel, m_adv_config.config[m_adv_channel].id);
					
					//
					//DEB("on_adv_evt: pm_peer_data_bonding_load()\n");
					err_code = pm_peer_data_bonding_load(m_adv_config.config[m_adv_channel].id, &peer_bonding_data);
					if (err_code != NRF_ERROR_NOT_FOUND)
					{
						//
						//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(err_code);
						
						//DEB("on_adv_evt: p_peer_addr= &peer_bonding_data.peer_ble_id.id_addr_info \n");
						ble_gap_addr_t * p_peer_addr = &(peer_bonding_data.peer_ble_id.id_addr_info);
						//
						DEB("on_adv_evt: ble_advertising_peer_addr_reply(&m_advertising,p_peer_addr=0x%x)\n", *p_peer_addr);
						err_code = ble_advertising_peer_addr_reply(&m_advertising, p_peer_addr);
						//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(err_code);
					}
					else
					{
						//DEB("on_adv_evt: pm_peer_data_bonding_load() fail: errcode=%d, m_adv_channel[%d], id[%d] \n",
						//	err_code,
						//	m_adv_channel,
						//	m_adv_config.config[m_adv_channel].id
						//);
					}
				}
				else
				{
					DEB("on_adv_evt: ???  m_adv_channel(%d), m_adv_config.config[%d].id = %d \n",
						m_adv_channel, m_adv_channel, m_adv_config.config[m_adv_channel].id
					);
				}
			}
			break; //BLE_ADV_EVT_PEER_ADDR_REQUEST

        default:
            break;
    }
}



/*$$$$$\   $$$$$$\  $$$$$$\ $$\   $$\ $$$$$$$$\ $$$$$$$$\ $$$$$$$\           
$$  __$$\ $$  __$$\ \_$$  _|$$$\  $$ |\__$$  __|$$  _____|$$  __$$\          
$$ |  $$ |$$ /  $$ |  $$ |  $$$$\ $$ |   $$ |   $$ |      $$ |  $$ |         
$$$$$$$  |$$ |  $$ |  $$ |  $$ $$\$$ |   $$ |   $$$$$\    $$$$$$$  |         
$$  ____/ $$ |  $$ |  $$ |  $$ \$$$$ |   $$ |   $$  __|   $$  __$$<          
$$ |      $$ |  $$ |  $$ |  $$ |\$$$ |   $$ |   $$ |      $$ |  $$ |         
$$ |       $$$$$$  |$$$$$$\ $$ | \$$ |   $$ |   $$$$$$$$\ $$ |  $$ |         
\__|       \______/ \______|\__|  \__|   \__|   \________|\__|  \__|         
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\ 
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__|
//============================================================================//
//                                 POINTER FUNCTIONS                          //
//============================================================================*/






/*$$$$$\  $$$$$$$$\ $$$$$$$\  $$\   $$\  $$$$$$\                             
$$  __$$\ $$  _____|$$  __$$\ $$ |  $$ |$$  __$$\                            
$$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |$$ /  \__|                           
$$ |  $$ |$$$$$\    $$$$$$$\ |$$ |  $$ |$$ |$$$$\                            
$$ |  $$ |$$  __|   $$  __$$\ $$ |  $$ |$$ |\_$$ |                           
$$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |$$ |  $$ |                           
$$$$$$$  |$$$$$$$$\ $$$$$$$  |\$$$$$$  |\$$$$$$  |                           
\_______/ \________|\_______/  \______/  \______/                            
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\ 
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__|
//============================================================================//
//                                 DEBUG FUNCTIONS                            //
//============================================================================*/




/*\   $$\ $$\   $$\ $$\   $$\  $$$$$$\  $$$$$$$$\ $$$$$$$\                   
$$ |  $$ |$$$\  $$ |$$ |  $$ |$$  __$$\ $$  _____|$$  __$$\                  
$$ |  $$ |$$$$\ $$ |$$ |  $$ |$$ /  \__|$$ |      $$ |  $$ |                 
$$ |  $$ |$$ $$\$$ |$$ |  $$ |\$$$$$$\  $$$$$\    $$ |  $$ |                 
$$ |  $$ |$$ \$$$$ |$$ |  $$ | \____$$\ $$  __|   $$ |  $$ |                 
$$ |  $$ |$$ |\$$$ |$$ |  $$ |$$\   $$ |$$ |      $$ |  $$ |                 
\$$$$$$  |$$ | \$$ |\$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$  |                 
 \______/ \__|  \__| \______/  \______/ \________|\_______/                  
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\ 
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__|
//============================================================================//
//                                 UNUSED FUNCTIONS                           //
//============================================================================*/




// End of file 
