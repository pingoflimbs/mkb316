/// \file m_comm.h
/// This file contains all required configurations for mokibo
/*==============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
==============================================================================*/




/*\   $$\ $$$$$$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$$$$$$\ $$\     $$\          
$$ |  $$ |\_$$  _|$$  __$$\\__$$  __|$$  __$$\ $$  __$$\\$$\   $$  |         
$$ |  $$ |  $$ |  $$ /  \__|  $$ |   $$ /  $$ |$$ |  $$ |\$$\ $$  /          
$$$$$$$$ |  $$ |  \$$$$$$\    $$ |   $$ |  $$ |$$$$$$$  | \$$$$  /           
$$  __$$ |  $$ |   \____$$\   $$ |   $$ |  $$ |$$  __$$<   \$$  /            
$$ |  $$ |  $$ |  $$\   $$ |  $$ |   $$ |  $$ |$$ |  $$ |   $$ |             
$$ |  $$ |$$$$$$\ \$$$$$$  |  $$ |    $$$$$$  |$$ |  $$ |   $$ |             
\__|  \__|\______| \______/   \__|    \______/ \__|  \__|   \__|             
//============================================================================//
//                            REVISION HISTORY                                //
//============================================================================//
//----------------------------------------------------------------------------//

20171026_MJ
	Created initial version
	Based on keyboard code in nordic SDK Version 14

20171219_MJ
	Added dfu_init() worked by JH.Seo

20180115_MJ
	Reject->allow pairing request from an already bonded peer.

20180202_MJ
	Created M_COMM_KEYS_BUF_LEN
	Created M_COMM_BUTTONS_BUF_LEN
	Created M_COMM_MOVEMENT_LBUF_EN
	Created M_COMM_CONSUMER_LBUF_EN
	Removed M_COMM_SEND_KEY_SPECIAL, M_COMM_SEND_TOUCH_PAD
	Created m_comm_click_button_send(), m_comm_click_wheel_send(), m_comm_send_event(), m_comm_zoom_send()

20180207_MJ
	Chagned m_comm_send() into local function send()

20180302_MJ
	Created m_comm_set_config_default()
	Added Passkey code

20180307_MJ
	Changed m_comm_channel_type_t type from enum to uint8_t
	Changed m_comm_central_type_t type from enum to uint8_t
	Changed dongle to m_dongle module
	Chagned m_comm_send() into local function
	Created M_COMM_EVENT_KEYBOARD_UPDATE, M_COMM_EVENT_POINT_UPDATE

20180309_MJ
	Added M_COMM_EVENT_SPECIAL_KEY
	Added M_COMM_EVENT_SPECIAL_KEY
	Created special_key_send()

20180319_MJ
	Changed condition in update_central(): only save in changed!!

20180320_Seo
	Added ble_stack_stop() function for disabling SoftDevice

20180322_Seo
	Added is_BT_channel() to check mode (BLE/DONGLE) at m_comm_send_event()
	Added checking fstorage busy state when softdevice will be stoped.

20180322_Seo
	Added is_DONGLE_channel() check routine at m_comm_send_event

20180308_MJ
	Added code not to proceed if BT is not connected in m_comm_send_event()

20180319_MJ
	Chagned M_COMM_EVENT_MUTI_TOUCH_UPDATE

20180326_MJ
	Implemented MaxOS in multi_touch_send()
	Added void update_device_name(void)

20180404_MJ
	Implemented virtual keyboard

20180412_MJ
	Changed return type of m_comm_ble_start(), m_comm_ble_stop()

20180619_MJ
	Issue: https://app.asana.com/0/650649611668999/747845262960817
	Clear previous new bonding channel after staring another new bonding 

20180620_MJ
	Issue: https://app.asana.com/0/650649611668999/747910809135485
	Added re-connect when link is disconnected in m_comm_send_event()
//----------------------------------------------------------------------------*/






/*$$$$$\  $$\    $$\ $$$$$$$$\ $$$$$$$\  $$\    $$\ $$$$$$\ $$$$$$$$\ $$\      $$\ 
$$  __$$\ $$ |   $$ |$$  _____|$$  __$$\ $$ |   $$ |\_$$  _|$$  _____|$$ | $\  $$ |
$$ /  $$ |$$ |   $$ |$$ |      $$ |  $$ |$$ |   $$ |  $$ |  $$ |      $$ |$$$\ $$ |
$$ |  $$ |\$$\  $$  |$$$$$\    $$$$$$$  |\$$\  $$  |  $$ |  $$$$$\    $$ $$ $$\$$ |
$$ |  $$ | \$$\$$  / $$  __|   $$  __$$<  \$$\$$  /   $$ |  $$  __|   $$$$  _$$$$ |
$$ |  $$ |  \$$$  /  $$ |      $$ |  $$ |  \$$$  /    $$ |  $$ |      $$$  / \$$$ |
 $$$$$$  |   \$  /   $$$$$$$$\ $$ |  $$ |   \$  /   $$$$$$\ $$$$$$$$\ $$  /   \$$ |
 \______/     \_/    \________|\__|  \__|    \_/    \______|\________|\__/     \__|
//============================================================================//
//                        FUNCTION CALLSTACK OVERVIEW                         //
//============================================================================//
//----------------------------------------------------------------------------//

//----------------------------------------------------//TIMERS
//----------------------------------------------------//VARS
//----------------------------------------------------//FUNCS
//----------------------------------------------------//INIT
//----------------------------------------------------//LOOP
static void ble_evt_handler(ble_evt_t const * p_ble_evt,void * p_context){
	________________________________LOG_mComm_FuncHeads(0,"");
    ret_code_t rv;
	ble_gap_addr_t peer_addr;
	//-----------------------------------------------------------------------------------------------------------------------------------
	#if defined(M_COMM_EVT_TRACE)
			DEB("___BLE___:ID=0x%x, Len=0x%x, %s\n",
			p_ble_evt->header.evt_id,
			p_ble_evt->header.evt_len,
			GetStrBLE_EVT(p_ble_evt->header.evt_id)
		);
			// ...no meaning...
			GetStrBLE_SVC(p_ble_evt->header.evt_id);
			GetStrBLE_OPT(p_ble_evt->header.evt_id);
			GetStrBLE_CFG(p_ble_evt->header.evt_id);
	#endif

	//-----------------------------------------------------------------------------------------------------------------------------------
	// BT가 연결된 상태에서, power on reset했을 때, 바로 연결되지 못하는 경우, 재차 reset함.
	// --> BLE_GATTS_EVT_SYS_ATTR_MISSING - A persistent system attribute access is pending. <---
#if 0
	m_ble_evt_count++;
	if(p_ble_evt->header.evt_id == BLE_GATTS_EVT_SYS_ATTR_MISSING)
	{
		m_mokibo_WDT_reset(1);			// Reset by Watchdog
	}
	if(m_ble_evt_count > 200)
	{
		// too much
		m_ble_evt_count=0;
		m_mokibo_soft_reset(0xA3A3);
	}
#endif
	
	//-----------------------------------------------------------------------------------------------------------------------------------
    switch (p_ble_evt->header.evt_id)
    {

		//-----------------------------------------------------------------------------------------------------------------------------------
        case BLE_GAP_EVT_CONNECTED : // Connection established.
			//DEB("ble_evt_handler: BLE_GAP_EVT_CONNECTED \n");

			m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
			peer_addr = p_ble_evt->evt.gap_evt.params.connected.peer_addr;

			//DEB("ble_evt_handler: m_conn_handle= 0x%x\n",m_conn_handle);
			//DEB("ble_evt_handler: peer_addr= 0x%x\n",peer_addr);

			//DEB("ble_evt_handler: update_central(peer_addr=0x%x)\n",peer_addr);
			update_central(&peer_addr);
		
			switch(m_mokibo_get_status())
			{
				case M_MOKIBO_KEYBOARD_STATUS :
					// 연결이 되는 순간 키보드 모드이면
					// 현재 채널 Color + Dimming out으로 표시한다
					//DEB("ble_evt_handler: m_mokibo_set_touch_color() \n");
					m_mokibo_set_touch_color();

					//DEB("ble_evt_handler: m_led_set_status(M_LED_DIMMING_OUT) \n");
					m_led_set_status(M_LED_DIMMING_OUT);
					break;
				
				case M_MOKIBO_TOUCH_STATUS: 
					//DEB("ble_evt_handler: m_mokibo_set_touch_color()\n");
					m_mokibo_set_touch_color();

					//DEB("ble_evt_handler: m_led_set_status(M_LED_ON) \n");
					m_led_set_status(M_LED_ON);
					break;
			}

			#if MOKIBO_NFC_ENABLED
			if(m_nfc_state_start() && !m_nfc_state_run())
			{
				#if defined(M_COMM_CH_TRACE)
				DEB("ble_evt_handler: m_nfc_stop() \n");
				#endif
				m_nfc_stop();
			}
			#endif

			//DEB("ble_evt_handler: m_comm_adv_set_status(M_COMM_ADV_STATUS_CONNECTED)\n");
			m_comm_adv_set_status(M_COMM_ADV_STATUS_CONNECTED);
			//
			
//			//-----------------------------------------------------------------------------------------------------------------------------------
//			// Save any change in BLE states
//			DEB("update_central: SaveInfoToFlash() .... maybe saved far later...\n");
///			if( !is_DONGLE_channel(m_config.config.selected_ch) )
//			{	// Dongle채널은 기억하지 않는다
//				SaveInfoToFlash(m_config.config.selected_ch);
//			}
			
			//DEB("update_central: SaveInfoToFlash() .... maybe saved far later...\n");
			if( is_BT_channel(m_config.config.selected_ch) )
			{	// BT channel 만 기억한다
				SaveInfoToFlash(m_config.config.selected_ch);
			}
			
            break;

		//-----------------------------------------------------------------------------------------------------------------------------------
        case BLE_GAP_EVT_DISCONNECTED :

			// invalidate the handle
			//DEB("ble_evt_handler: m_conn_handle=BLE_CONN_HANDLE_INVALID \n");
			m_conn_handle = BLE_CONN_HANDLE_INVALID;

            // Reset m_caps_on variable. Upon reconnect, the HID host will re-send the Output
            // report containing the Caps lock state.
			//DEB("ble_evt_handler: m_caps_on=false \n");
            m_caps_on = false;

			// Clear Passkey
			//DEB("ble_evt_handler: m_passkey <-- 0 (clear)  ??? \n");
			memset(&m_passkey, 0, sizeof(passkey_t));
			//
			if(M_COMM_ADV_WAITE_GAP_DISCONNECT == m_comm_adv_get_status())
			{
				if(is_BT_channel(m_config.config.selected_ch))
				{
					// TODO: be careful conversion: m_comm_channel_type_t --> m_comm_adv_channel_type_t
					m_comm_adv_channel_type_t adv_ch_typ = (m_comm_adv_channel_type_t)m_comm_get_selected_channel();
					#if defined(M_COMM_EVT_TRACE)
						DEB("ble_evt_handler: m_comm_adv_start(ch=%d)...\n",adv_ch_typ);
					#endif
					m_comm_adv_start(adv_ch_typ);
				}
			}
            break; // BLE_GAP_EVT_DISCONNECTED

		//-----------------------------------------------------------------------------------------------------------------------------------
		#ifndef S140
        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
			{
				//DEB("ble_evt_handler: BLE_GAP_EVT_PHY_UPDATE_REQUEST \n");
				//DEB("ble_evt_handler: sd_ble_gap_phy_update() \n");
				ble_gap_phys_t const phys =
				{
					.rx_phys = BLE_GAP_PHY_AUTO,
					.tx_phys = BLE_GAP_PHY_AUTO,
				};
				rv = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys); APP_ERROR_CHECK(rv);
			}
			break;
		#endif
        
		//-----------------------------------------------------------------------------------------------------------------------------------
		case BLE_GATTS_EVT_HVN_TX_COMPLETE:
            break;

		//-----------------------------------------------------------------------------------------------------------------------------------
        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
			#if defined(M_COMM_EVT_TRACE)
				DEB("ble_evt_handler: BLE_GATTC_EVT_TIMEOUT - sd_ble_gap_disconnect(BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION) \n");
			#endif
            rv = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(rv);
            break;

		//-----------------------------------------------------------------------------------------------------------------------------------
        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
			#if defined(M_COMM_EVT_TRACE)
				DEB("ble_evt_handler: BLE_GATTS_EVT_TIMEOUT - sd_ble_gap_disconnect(BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION)\n");
			#endif
            rv = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(rv);
            break;

		//-----------------------------------------------------------------------------------------------------------------------------------
        case BLE_EVT_USER_MEM_REQUEST:
			#if defined(M_COMM_EVT_TRACE)
				DEB("ble_evt_handler: sd_ble_user_mem_reply(m_conn_handle=0x%x,NULL) \n",m_conn_handle);
			#endif
            rv = sd_ble_user_mem_reply(m_conn_handle, NULL); APP_ERROR_CHECK(rv);
            break;

		//-----------------------------------------------------------------------------------------------------------------------------------
        case BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST:
			{
				ble_gatts_evt_rw_authorize_request_t  req;
				ble_gatts_rw_authorize_reply_params_t auth_reply;
				req = p_ble_evt->evt.gatts_evt.params.authorize_request;

				if (req.type != BLE_GATTS_AUTHORIZE_TYPE_INVALID)
				{
					if ((req.request.write.op == BLE_GATTS_OP_PREP_WRITE_REQ)     ||
						(req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_NOW) ||
						(req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_CANCEL))
					{
						if (req.type == BLE_GATTS_AUTHORIZE_TYPE_WRITE)
						{
							auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_WRITE;
						}
						else
						{
							auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
						}
						auth_reply.params.write.gatt_status = APP_FEATURE_NOT_SUPPORTED;

						#if defined(M_COMM_EVT_TRACE)
							DEB("ble_evt_handler: sd_ble_gatts_rw_authorize_reply()\n");
						#endif
						rv = sd_ble_gatts_rw_authorize_reply(p_ble_evt->evt.gatts_evt.conn_handle,&auth_reply);
						APP_ERROR_CHECK(rv);
					}
				}
			}
			break; // BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST

		//-----------------------------------------------------------------------------------------------------------------------------------
        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
			break; // BLE_GAP_EVT_SEC_PARAMS_REQUEST
        
		//-----------------------------------------------------------------------------------------------------------------------------------
		case BLE_GAP_EVT_AUTH_KEY_REQUEST:
			{
				#if MOKIBO_NFC_ENABLED
				if(!m_nfc_state_run())
					m_passkey.is_ready = true;
				#else
					m_passkey.is_ready = true;
				#endif
				
				//DEB("ble_evt_handler: m_passkey.is_ready <--true \n");

			} 
			break; // BLE_GAP_EVT_AUTH_KEY_REQUEST

		//-----------------------------------------------------------------------------------------------------------------------------------
        case BLE_GAP_EVT_AUTH_STATUS:
			{
				if (p_ble_evt->evt.gap_evt.params.auth_status.auth_status == BLE_GAP_SEC_STATUS_SUCCESS)
				{
					#if defined(M_COMM_EVT_TRACE)
						DEB("ble_evt_handler: Authorization succeeded! \n");
					#endif

					#if MOKIBO_NFC_ENABLED	
					if(m_nfc_state_start())
					{
						#if defined(M_COMM_CH_TRACE)
							DEB(">>>>>>>> BT channel NFC stop! \n");
						#endif
						m_nfc_stop();
					}
					#endif
				}
				else
				{
					#if defined(M_COMM_EVT_TRACE)
						DEB("ble_evt_handler: Authorization failed with code: %u!", p_ble_evt->evt.gap_evt.params.auth_status.auth_status);
						DEB("ble_evt_handler: m_config.config.selected_ch = M_COMM_CENTRAL_OTHER \n");
					#endif
					rv = m_comm_set_central_type(m_config.config.selected_ch, M_COMM_CENTRAL_OTHER); PRINT_IF_ERROR("m_comm_set_central_type()", rv);
				}
			} 
			break;

		//-----------------------------------------------------------------------------------------------------------------------------------
        default:
            // No implementation needed.
			#if defined(M_COMM_EVT_TRACE)
				//DEB("ble_evt_handler(): NOT PROCESSED EVT_ID: 0x%02x \n", p_ble_evt->header.evt_id);
			#endif
			break;

    }//switch (p_ble_evt->header.evt_id)

	
}

//----------------------------------------------------------------------------*/




/*\      $$\  $$$$$$\  $$$$$$$\  $$\   $$\ $$\       $$$$$$$$\  $$$$$$\  
$$$\    $$$ |$$  __$$\ $$  __$$\ $$ |  $$ |$$ |      $$  _____|$$  __$$\ 
$$$$\  $$$$ |$$ /  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$ /  \__|
$$\$$\$$ $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$$$$\    \$$$$$$\  
$$ \$$$  $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$  __|    \____$$\ 
$$ |\$  /$$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$\   $$ |
$$ | \_/ $$ | $$$$$$  |$$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$$\ \$$$$$$  |
\__|     \__| \______/ \_______/  \______/ \________|\________| \______/ 
//============================================================================//
//                                 MODULES USED                               //
//============================================================================*/

#ifndef __M_COMM_H
#define __M_COMM_H
#include "drv_keyboard.h"
#include "m_comm_adv.h"
#include "nrf_ble_gatt.h"



 /*$$$$\   $$$$$$\  $$\   $$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$\   $$\ $$$$$$$$\ 
$$  __$$\ $$  __$$\ $$$\  $$ |$$  __$$\\__$$  __|$$  __$$\ $$$\  $$ |\__$$  __|
$$ /  \__|$$ /  $$ |$$$$\ $$ |$$ /  \__|  $$ |   $$ /  $$ |$$$$\ $$ |   $$ |   
$$ |      $$ |  $$ |$$ $$\$$ |\$$$$$$\    $$ |   $$$$$$$$ |$$ $$\$$ |   $$ |   
$$ |      $$ |  $$ |$$ \$$$$ | \____$$\   $$ |   $$  __$$ |$$ \$$$$ |   $$ |   
$$ |  $$\ $$ |  $$ |$$ |\$$$ |$$\   $$ |  $$ |   $$ |  $$ |$$ |\$$$ |   $$ |   
\$$$$$$  | $$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$ |  $$ |$$ | \$$ |   $$ |   
 \______/  \______/ \__|  \__| \______/   \__|   \__|  \__|\__|  \__|   \__|   
 //============================================================================//
//                                 GLOBAL CONSTANTS                            //
//=============================================================================*/

#define M_COMM_KEYS_BUF_LEN         DRV_KEYBOARD_MAX_NUM_OF_ALL_KEYS
#define M_COMM_BUTTONS_BUF_LEN      3 		/**< Length of Mouse Input Report containing button data. */
#define M_COMM_MOVEMENT_BUF_LEN     3		/**< Length of Mouse Input Report containing movement data. */
#define M_COMM_CONSUMER_BUF_LEN     3		/**< Length of Mouse Input Report containing movement data. */

#define M_COMM_BUTTONS_INDEX		0
#define M_COMM_WHEEL_INDEX			1
#define M_COMM_AC_INDEX				2




/*$$$$$$\ $$\     $$\ $$$$$$$\  $$$$$$$$\ $$$$$$$\  $$$$$$$$\ $$$$$$$$\  $$$$$$\  
\__$$  __|\$$\   $$  |$$  __$$\ $$  _____|$$  __$$\ $$  _____|$$  _____|$$  __$$\ 
   $$ |    \$$\ $$  / $$ |  $$ |$$ |      $$ |  $$ |$$ |      $$ |      $$ /  \__|
   $$ |     \$$$$  /  $$$$$$$  |$$$$$\    $$ |  $$ |$$$$$\    $$$$$\    \$$$$$$\  
   $$ |      \$$  /   $$  ____/ $$  __|   $$ |  $$ |$$  __|   $$  __|    \____$$\ 
   $$ |       $$ |    $$ |      $$ |      $$ |  $$ |$$ |      $$ |      $$\   $$ |
   $$ |       $$ |    $$ |      $$$$$$$$\ $$$$$$$  |$$$$$$$$\ $$ |      \$$$$$$  |
   \__|       \__|    \__|      \________|\_______/ \________|\__|       \______/ 
//============================================================================//
//                                 GLOBAL TYPEDEFS                            //
//============================================================================*/




/*$$$$$$\ $$\     $$\ $$$$$$$\  $$$$$$$$\ $$$$$$$\  $$$$$$$$\ $$$$$$$$\  $$$$$$\  
\__$$  __|\$$\   $$  |$$  __$$\ $$  _____|$$  __$$\ $$  _____|$$  _____|$$  __$$\ 
   $$ |    \$$\ $$  / $$ |  $$ |$$ |      $$ |  $$ |$$ |      $$ |      $$ /  \__|
   $$ |     \$$$$  /  $$$$$$$  |$$$$$\    $$ |  $$ |$$$$$\    $$$$$\    \$$$$$$\  
   $$ |      \$$  /   $$  ____/ $$  __|   $$ |  $$ |$$  __|   $$  __|    \____$$\ 
   $$ |       $$ |    $$ |      $$ |      $$ |  $$ |$$ |      $$ |      $$\   $$ |
   $$ |       $$ |    $$ |      $$$$$$$$\ $$$$$$$  |$$$$$$$$\ $$ |      \$$$$$$  |
   \__|       \__|    \__|      \________|\_______/ \________|\__|       \______/ 
//============================================================================//
//                                 GLOBAL TYPEDEFS                            //
//============================================================================*/
typedef enum
{
    M_COMM_MODE_BLE,   /**< Bluetooth mode. */
    M_COMM_MODE_GAZELL /**< Gazell mode. */
} m_comm_mode_t;

typedef enum
{
    M_COMM_EVENT_VOLUME_CTRL,
    M_COMM_EVENT_CLICK_CTRL,
    M_COMM_EVENT_WHEEL_CTRL,
    M_COMM_EVENT_ZOOM_CTRL,
    M_COMM_EVENT_MUTI_TOUCH_UPDATE,
    M_COMM_EVENT_KEYBOARD_UPDATE,
    M_COMM_EVENT_POINT_UPDATE,
    M_COMM_EVENT_SPECIAL_KEY,
	//
    M_COMM_EVENT_MAX,
} m_comm_event_t;

typedef enum
{
    M_COMM_CMD_START,
    M_COMM_CMD_ERASE_BONDS,
    M_COMM_CMD_CH_CHANGE,
    M_COMM_CMD_NEW_BOND,
    M_COMM_CMD_STOP,
	//
    M_COMM_CMD_MAX,
} m_comm_cmd_type_t;


#define	M_COMM_CHANNEL_BT1 			0
#define	M_COMM_CHANNEL_BT2 			1
#define	M_COMM_CHANNEL_BT3 			2
//#define	M_COMM_CHANNEL_DONGLE 		3
//#define	M_COMM_CHANNEL_MAX 			4
#define	M_COMM_CHANNEL_MAX 			3

typedef uint8_t m_comm_channel_type_t;

#define	M_COMM_CENTRAL_MICROSOFT	0
#define	M_COMM_CENTRAL_MAC			1
#define	M_COMM_CENTRAL_IOS			2
#define	M_COMM_CENTRAL_ANDROID		3
#define	M_COMM_CENTRAL_OTHER		4

typedef uint8_t m_comm_central_type_t;

typedef struct
{
	m_comm_channel_type_t selected_ch;					// Bonded(paired)와 상관없이, Fn+key론 선택한 채널이다 주의 !!
	m_comm_central_type_t central[M_COMM_CHANNEL_MAX];	//
//	bonded_ch[M_COMM_CHANNEL_MAX]; 						// <--- 이것이 필요하다. !!!!!!!
	// Add new member here
} m_comm_attr_t; 

typedef union
{
	m_comm_attr_t 	config;
	uint8_t     	data[DRV_FDS_M_COMM_CONFIG_SIZE];
} m_comm_config_t;

typedef struct
{
    uint16_t tab 		: 1;
    uint16_t scroll_up	: 1;
    uint16_t scroll_down	: 1;
    uint16_t scroll_left	: 1;
    uint16_t scroll_right: 1;
    uint16_t back 		: 1;
    uint16_t option 		: 1;
    uint16_t scroll_trvldist	: 8;
	uint16_t rsvd		: 1;
} m_comm_evt_multi_touch_t;

typedef struct
{
	m_comm_evt_multi_touch_t two;
	m_comm_evt_multi_touch_t three;
	m_comm_evt_multi_touch_t four;
} m_comm_evt_arg_multi_touch_t;

typedef struct
{
    uint8_t up 			: 1;
    uint8_t down 		: 1;
	uint8_t mute		: 1;
	uint8_t rsvd 		: 5;
} m_comm_evt_arg_volume_t;

typedef struct
{
    uint8_t left 		: 1;
    uint8_t center		: 1;
    uint8_t right 		: 1;
    uint8_t rsvd 		: 5;
} m_comm_evt_arg_click_t;

typedef struct
{
    uint16_t up 			: 1;
    uint16_t down 		: 1;
    uint16_t left 		: 1;
    uint16_t right 		: 1;
	uint16_t trvldist	: 8;
    uint16_t rsvd 		: 4;
} m_comm_evt_arg_wheel_t;

typedef struct
{
    uint8_t in 			: 1;
    uint8_t out 		: 1;
    uint8_t rsvd 		: 6;
} m_comm_evt_arg_zoom_t;

typedef struct
{
    uint8_t buf[M_COMM_KEYS_BUF_LEN];
} m_comm_evt_arg_keys_t;

typedef struct
{
    uint8_t buf[M_COMM_MOVEMENT_BUF_LEN];

	// <-- 중복전송을 막기 위하여, timestamp 필요
	// (1) m_comm_send_event(M_COMM_EVENT_POINT_UPDATE,..)를 찾아라!
	// (2) get_point(&val.point, index);
	uint16_t	timestamp;

} m_comm_evt_arg_point_t;


typedef struct
{
	uint32_t home		: 1; 	// home
	uint32_t swtapp		: 1;	// switching App
	uint32_t vkbd 		: 1;	// virtual keyboard
	uint32_t search 		: 1;	// search
	uint32_t capture    	: 1;	// capture
	uint32_t release		: 1;	// release //20181104_flimbs: back->release 
	uint32_t search_opt  : 1;	// search option
	uint32_t backward		: 1;	// reserved //20181104_flimbs: rsvd->backward	
	uint32_t lockscreen		:1;
	uint32_t touchlock		:1;
	uint32_t language_z 	:1;
	uint32_t language_x 	:1;
	uint32_t language_c 	:1;
	uint32_t rsvd 			:18;
} m_comm_evt_arg_special_key_t; 


typedef union 
{
	m_comm_evt_arg_volume_t 		volume;
	m_comm_evt_arg_click_t			click;
	m_comm_evt_arg_wheel_t			wheel;
	m_comm_evt_arg_zoom_t			zoom;
	m_comm_evt_arg_multi_touch_t 	touch;
	m_comm_evt_arg_keys_t			keys;
	m_comm_evt_arg_point_t			point;
	m_comm_evt_arg_special_key_t	sp_key;
	// Should be maximum value of memebers
	uint8_t data[M_COMM_KEYS_BUF_LEN];
} m_comm_evt_arg_t;

//-------------------------------------------------------
// moved from m_comm.c to m_comm.h
typedef enum
{
    SEND_KEY,
    SEND_TOUCH_BUTTON,
    SEND_TOUCH_MOVE,
    SEND_CONSUMER
} send_type_t;

typedef struct
{
	uint8_t		hid;
	uint8_t		ascii;
} hid_to_ascii_t;

typedef struct
{
	bool    is_ready;
	bool    is_enter;
	uint8_t index;
	uint8_t keys[BLE_GAP_PASSKEY_LEN];
} passkey_t; 

//---------------------------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------------------------
typedef struct
{
	// Paired or Not
	// (0..1) Is Paired 
	// (BT1,BT2,BT3는 모두 paired일 수도 있고 아닐수도 있다.
	// Dongle은 존재하는 경우에는 항상 paired로 보면 된다)
	bool	paired[M_COMM_CHANNEL_MAX];	//[3] for history
	
	// _MICROSOFT(0), _MAC(1), _IOS(2), _ANDROID(3), _OTHER(4)
	//m_comm_central_type_t central[M_COMM_CHANNEL_MAX];	//[3]
	
	// Channel# [0=BT1, 1=BT2, 2=BT3, 3=DONGLE]
	// (0..3) Currently connected channel#
	// (4개의 채널중 오로지 하나만 connected, 나머지는 paired인 경우에 한해서 disconnected)
	uint8_t conn_try_ch;		// 연결시도중 채널
	uint8_t connected_ch;		// 연결된 채널 (comm_mode를 이로부터 판단할 수 있다)
	// 이전에 연결되었던 채널
	// (BT와 Dongle에서의 상황이 다르다. CONNECT --> DISCONNECT --> (new) CONNECT를 거치므로 DISCONNECT시에 update되어야 한다)
	uint8_t connected_ch_prev;
	//
	uint16_t conn_handle[M_COMM_CHANNEL_MAX];
	
} conn_stat_t;
//


/*\    $$\  $$$$$$\  $$$$$$$\  $$$$$$\  $$$$$$\  $$$$$$$\  $$\       $$$$$$$$\ 
$$ |   $$ |$$  __$$\ $$  __$$\ \_$$  _|$$  __$$\ $$  __$$\ $$ |      $$  _____|
$$ |   $$ |$$ /  $$ |$$ |  $$ |  $$ |  $$ /  $$ |$$ |  $$ |$$ |      $$ |      
\$$\  $$  |$$$$$$$$ |$$$$$$$  |  $$ |  $$$$$$$$ |$$$$$$$\ |$$ |      $$$$$\    
 \$$\$$  / $$  __$$ |$$  __$$<   $$ |  $$  __$$ |$$  __$$\ $$ |      $$  __|   
  \$$$  /  $$ |  $$ |$$ |  $$ |  $$ |  $$ |  $$ |$$ |  $$ |$$ |      $$ |      
   \$  /   $$ |  $$ |$$ |  $$ |$$$$$$\ $$ |  $$ |$$$$$$$  |$$$$$$$$\ $$$$$$$$\ 
    \_/    \__|  \__|\__|  \__|\______|\__|  \__|\_______/ \________|\________|
/*============================================================================//
//                            GLOBAL  VARIABLES                               //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                                GLOBAL VARIABLES                            //
//----------------------------------------------------------------------------*/
//#include "nrf_ble_gatt.h"
//----------------------------------------------------
//	typedef struct
//	{
//		uint16_t att_mtu_desired;               //!< Requested ATT_MTU size (in bytes).
//		uint16_t att_mtu_effective;             //!< Effective ATT_MTU size (in bytes).
//		bool     att_mtu_exchange_pending;      //!< Indicates that an ATT_MTU exchange request is pending (the call to @ref sd_ble_gattc_exchange_mtu_request returned @ref NRF_ERROR_BUSY).
//		bool     att_mtu_exchange_requested;    //!< Indicates that an ATT_MTU exchange request was made.
//	#if !defined (S112)
//		uint8_t  data_length_desired;           //!< Desired data length (in bytes).
//		uint8_t  data_length_effective;         //!< Requested data length (in bytes).
//	#endif // !defined (S112)
//	} nrf_ble_gatt_link_t;
//----------------------------------------------------
//	struct nrf_ble_gatt_s
//	{
//		uint16_t                   att_mtu_desired_periph;          //!< Requested ATT_MTU size for the next peripheral connection that is established.
//		uint16_t                   att_mtu_desired_central;         //!< Requested ATT_MTU size for the next central connection that is established.
//		uint8_t                    data_length;                     //!< Data length to use for the next connection that is established.
//		nrf_ble_gatt_link_t        links[NRF_BLE_GATT_LINK_COUNT];  //!< GATT related information for all active connections.
//		nrf_ble_gatt_evt_handler_t evt_handler;                     //!< GATT event handler.
//	} nrf_ble_gatt_t;
//----------------------------------------------------
extern nrf_ble_gatt_t   m_gatt;

extern m_comm_config_t	m_config;
extern m_comm_mode_t	m_comm_mode;	// main.c App Scheduler에 영향 있슴.
extern uint16_t         m_conn_handle;

extern bool             m_caps_on;
extern passkey_t	 	m_passkey;
//
extern uint8_t			m_bt_ch;
/*----------------------------------------------------------------------------//
//                             GLOBAL DEBUG VARIABLES                         //
//----------------------------------------------------------------------------*/




/*$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\  $$\   $$\ $$$$$$$$\  $$$$$$\  $$$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\ $$ |  $$ |$$  _____|$$  __$$\ $$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|$$ |  $$ |$$ |      $$ /  $$ |$$ |  $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |      $$$$$$$$ |$$$$$\    $$$$$$$$ |$$ |  $$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |      $$  __$$ |$$  __|   $$  __$$ |$$ |  $$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\ $$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |$$ |  $$ |$$$$$$$$\ $$ |  $$ |$$$$$$$  |
\__|       \______/ \__|  \__| \______/ \__|  \__|\________|\__|  \__|\_______/ 
/*============================================================================//
//                                 GLOBAL FUNCTIONS                           //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                            GLOBAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
bool is_BT_channel(m_comm_channel_type_t ch);
//bool is_DONGLE_channel(m_comm_channel_type_t ch);
bool is_VALID_channel(m_comm_channel_type_t ch);
bool isPaired(uint8_t ch);
//
ret_code_t Change_BT_to_BT_before(uint8_t ch_old, uint8_t ch_new);
ret_code_t Change_BT_to_BT_after(uint8_t ch_old, uint8_t ch_new);
ret_code_t Change_INVALID_to_BT_before(uint8_t ch_new);
ret_code_t Change_INVALID_to_BT_after(uint8_t ch_new);
//
//ret_code_t Change_to_DONGLE(uint8_t ch_new);
//
//ret_code_t Change_DONGLE_to_BT_before(uint8_t ch_old, uint8_t ch_new);
//ret_code_t Change_DONGLE_to_BT_after(uint8_t ch_old, uint8_t ch_new);

//
ret_code_t Refresh_BT(uint8_t ch);
//ret_code_t Refresh_DONGLE(uint8_t ch);


//-----------------------------------------------------------------------------------------------------------------------------------
ret_code_t m_comm_init(void);
m_comm_mode_t m_comm_get_mode(void);
uint16_t m_comm_ble_get_status(void);
ret_code_t m_comm_cmd(m_comm_cmd_type_t cmd, uint32_t argv);
m_comm_channel_type_t m_comm_get_selected_channel(void);
//
ret_code_t m_comm_set_central_type(m_comm_channel_type_t ch, m_comm_central_type_t type);
ret_code_t m_comm_get_central_type(m_comm_channel_type_t ch, m_comm_central_type_t* p_type);
m_comm_central_type_t m_comm_current_central_type(void);
ret_code_t m_comm_send_event(m_comm_event_t evt, const m_comm_evt_arg_t arg);
ret_code_t m_comm_set_config_default(void);
void m_comm_clear_keyboard(void);
ret_code_t m_comm_ble_start(void);
ret_code_t m_comm_ble_stop(void);
//-----------------------------------------------------------------------------------------------------------------------------------
ret_code_t m_comm_cmd(m_comm_cmd_type_t cmd, uint32_t argv);
ret_code_t onCommCmdStart(m_comm_channel_type_t selected);
ret_code_t onCommCmdEraseBonds(void);
//ret_code_t onCommCmdChChange(uint32_t argv);
ret_code_t onCommCmdChChange(uint8_t selected_ch_old, uint8_t selected_ch_new);
ret_code_t onCommCmdNewBond(uint32_t argv);
ret_code_t onCommCmdStop(uint8_t selected);


/*----------------------------------------------------------------------------//
//                         GLOBAL DEBUG FUNCTION HEADS                        //
//----------------------------------------------------------------------------*/




/*\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ 
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\_*/


#endif // __M_COMM_H 

