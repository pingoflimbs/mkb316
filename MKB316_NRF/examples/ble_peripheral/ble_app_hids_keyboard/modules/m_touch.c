/// \file m_touch.c
/// This file contains all required configurations for mokibo
/*==============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
==============================================================================*/


/*\      $$\  $$$$$$\  $$$$$$$\  $$\   $$\ $$\       $$$$$$$$\  $$$$$$\  
$$$\    $$$ |$$  __$$\ $$  __$$\ $$ |  $$ |$$ |      $$  _____|$$  __$$\ 
$$$$\  $$$$ |$$ /  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$ /  \__|
$$\$$\$$ $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$$$$\    \$$$$$$\  
$$ \$$$  $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$  __|    \____$$\ 
$$ |\$  /$$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$\   $$ |
$$ | \_/ $$ | $$$$$$  |$$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$$\ \$$$$$$  |
\__|     \__| \______/ \_______/  \______/ \________|\________| \______/ 
//============================================================================//
//                              LOCAL MODULEL USED                            //
//============================================================================*/
#include "sdk_config.h" // mokibo_config.h && sdk_config.h
#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)	
	#include "SEGGER_RTT.h"	// should be placed after "sdk_config.h"	
#endif
#include <stdlib.h>
#include <math.h>	// for sqrt() by Stanley
#include "sdk_common.h"
#include "sdk_errors.h"
#include "nrf_log.h"
#include "ble_hids.h"
#include "ble_err.h"
#include "nrf_delay.h"
#include "drv_cypress.h"
#include "drv_twi.h"
#include "m_timer.h"
#include "m_comm.h"
#include "m_mokibo.h"
#include "m_util.h"
#include "m_touch.h"
#include "m_flimbs.h"


 /*$$$$\   $$$$$$\  $$\   $$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$\   $$\ $$$$$$$$\ 
$$  __$$\ $$  __$$\ $$$\  $$ |$$  __$$\\__$$  __|$$  __$$\ $$$\  $$ |\__$$  __|
$$ /  \__|$$ /  $$ |$$$$\ $$ |$$ /  \__|  $$ |   $$ /  $$ |$$$$\ $$ |   $$ |   
$$ |      $$ |  $$ |$$ $$\$$ |\$$$$$$\    $$ |   $$$$$$$$ |$$ $$\$$ |   $$ |   
$$ |      $$ |  $$ |$$ \$$$$ | \____$$\   $$ |   $$  __$$ |$$ \$$$$ |   $$ |   
$$ |  $$\ $$ |  $$ |$$ |\$$$ |$$\   $$ |  $$ |   $$ |  $$ |$$ |\$$$ |   $$ |   
\$$$$$$  | $$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$ |  $$ |$$ | \$$ |   $$ |   
 \______/  \______/ \__|  \__| \______/   \__|   \__|  \__|\__|  \__|   \__|   
//============================================================================//
//                                LOCAL CONSTANTS                             //
//============================================================================*/



/*$$$$$$\ $$\     $$\ $$$$$$$\  $$$$$$$$\ $$$$$$$\  $$$$$$$$\ $$$$$$$$\  $$$$$$\  
\__$$  __|\$$\   $$  |$$  __$$\ $$  _____|$$  __$$\ $$  _____|$$  _____|$$  __$$\ 
   $$ |    \$$\ $$  / $$ |  $$ |$$ |      $$ |  $$ |$$ |      $$ |      $$ /  \__|
   $$ |     \$$$$  /  $$$$$$$  |$$$$$\    $$ |  $$ |$$$$$\    $$$$$\    \$$$$$$\  
   $$ |      \$$  /   $$  ____/ $$  __|   $$ |  $$ |$$  __|   $$  __|    \____$$\ 
   $$ |       $$ |    $$ |      $$ |      $$ |  $$ |$$ |      $$ |      $$\   $$ |
   $$ |       $$ |    $$ |      $$$$$$$$\ $$$$$$$  |$$$$$$$$\ $$ |      \$$$$$$  |
   \__|       \__|    \__|      \________|\_______/ \________|\__|       \______/ 
//============================================================================//
//                                LOCAL TYPEDEFS                              //
//============================================================================*/

//----------------------------------------------------//job_state_t
typedef enum
{ //손가락 갯수에 따른 논리적 현재 상태. 물리적 손가락 갯수와 다를수있다.
   job_state_idle = 0,	//탭을 대기하는상태. 더블클릭 외에도 더블탭, 트리플탭을처리한다.
    job_state_one,		//update_move 호출, change_gesture(gstr_one_drag) 호출
    job_state_two,		
    job_state_three,
    job_state_four,	
    job_state_max,
} job_state_t;

typedef enum
{ //현재 수행되고 있는 제스처의 종류
	gstr_state_none = 0,
	gstr_one_move,//1
	gstr_one_drag,//2
	gstr_one_volume_ctrl,//3
	gstr_one_zoom_ctrl,//4
	gstr_one_stroll,//5
	gstr_two_scroll,//6
	gstr_two_zoom,//7
	gstr_three_scroll_up_down,
	gstr_three_scroll_left_right,
	gstr_four_scroll_up_down,
	gstr_four_scroll_left_right,
	gstr_state_max,
} gstr_state_t;

typedef struct
{ //현재 제스처 관리하는 최상위구조체	
	job_state_t 		jobState_old;
	job_state_t			jobState;		//change() 에서 값 바뀜 손가락 갯수. 이 값으로 blabla_handler() 를 호출한다
	gstr_state_t		gestureState;
	gstr_state_t		gestureState_old;
	uint8_t				gestureLevel;	//제스처 몇번 반복했는지
	uint16_t			no_data;	//cypress 에서 데이터가져오다가 실패하면 카운트올라간다.
	uint8_t				tab_number;	//change()에서 값 바뀜 update_tab() 에서 이 값을 기준으로 클릭이벤트 더블탭인지 트리플탭인지 바뀜.
} job_state_db_t;


//----------------------------------------------------//touch_touch_t
typedef struct 
{//안씀
	uint8_t 	valid:      1;		// reserved for later use
	uint8_t		tip: 		1;
	uint8_t		eventID: 	2;
	uint8_t		touchID: 	4;
} touch_attr_t; 

typedef struct 
{// 좌표
	uint16_t 	x;
	uint16_t 	y;
} touch_coordinate_t; 

typedef struct 
{// 1점 터치 정보
	uint16_t			timestamp; 	// timestamp
	touch_attr_t 		attr; 		// attribute
	touch_coordinate_t  loc;		// location
} touch_point_t; 

typedef struct 
{// 벡터
	uint16_t 	speed; //
	uint16_t 	angle; // 
	uint16_t 	delta; //
} touch_vector_t;

typedef struct 
{// 터치 데이터
	uint8_t			w_index; 	// point 의 몇번째 배열에 써야하는지 가리킴.
	touch_vector_t  vector;		// speed, angle, delta
	touch_point_t 	point[M_POINT_BUF_NUM];
} touch_touch_t; //m_touch_db[3]



//----------------------------------------------------//dir_type_t
typedef enum
{
	dir_none,			// 0
	dir_right,			// 1
	dir_up_right,		// 2
	dir_up,				// 3
	dir_up_left,		// 4
	dir_left,			// 5
	dir_down_left,		// 6
	dir_down,			// 7
	dir_down_right,		// 8
} dir_type_t;


//----------------------------------------------------//window_t
#if	defined(SLIDING_WINDOW_SIZE)
	//window_t
	typedef enum
	{
		coordinate_x = 0,
		coordinate_y,
		coordinate_num,
	} coordinate_type_t;

	typedef struct {
		uint8_t	   	is_used;
		uint32_t	write_index;
		uint32_t 	delta[SLIDING_WINDOW_SIZE];
		touch_coordinate_t 	queue[SLIDING_WINDOW_SIZE];
	} window_t;
#endif







/*\    $$\  $$$$$$\  $$$$$$$\  $$$$$$\  $$$$$$\  $$$$$$$\  $$\       $$$$$$$$\ 
$$ |   $$ |$$  __$$\ $$  __$$\ \_$$  _|$$  __$$\ $$  __$$\ $$ |      $$  _____|
$$ |   $$ |$$ /  $$ |$$ |  $$ |  $$ |  $$ /  $$ |$$ |  $$ |$$ |      $$ |      
\$$\  $$  |$$$$$$$$ |$$$$$$$  |  $$ |  $$$$$$$$ |$$$$$$$\ |$$ |      $$$$$\    
 \$$\$$  / $$  __$$ |$$  __$$<   $$ |  $$  __$$ |$$  __$$\ $$ |      $$  __|   
  \$$$  /  $$ |  $$ |$$ |  $$ |  $$ |  $$ |  $$ |$$ |  $$ |$$ |      $$ |      
   \$  /   $$ |  $$ |$$ |  $$ |$$$$$$\ $$ |  $$ |$$$$$$$  |$$$$$$$$\ $$$$$$$$\ 
    \_/    \__|  \__|\__|  \__|\______|\__|  \__|\_______/ \________|\______*/
/*============================================================================//
//                                    VARIABLES                               //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                                GLOBAL VARIABLES                            //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                                 LOCAL VARIABLES                            //
//----------------------------------------------------------------------------*/
static job_state_db_t m_state;
static M_TOUCH_ACTIVE_AREA_T m_mode;
static touch_touch_t m_touch_db[M_TOUCH_COUNT_MAX];
static touch_point_t m_touch_point_begin[M_TOUCH_COUNT_MAX]; // <-- Inserted by Stanley
static uint8_t zoomStartThreshold = 0;//using in change()
#if	defined(SLIDING_WINDOW_SIZE)
	static window_t m_move_window;
#endif



/*----------------------------------------------------------------------------//
//                                 DEBUG VARIABLES                            //
//----------------------------------------------------------------------------*/

#if	defined(M_TOUCH_STATE_TRACE)
const static char *m_jobString[job_state_max] = \
{ \
	"TOUCH_STATE_IDLE", \
	"TOUCH_STATE_ONE", \
	"TOUCH_STATE_TWO", \
	"TOUCH_STATE_THREE", \
	"TOUCH_STATE_FOUR", \
};

const static char *m_gString[gstr_state_max] = \
{ \
	"gstr_state_none", \
	"gstr_one_move", \
	"gstr_one_drag", \
	"gstr_one_volume_ctrl", \
	"gstr_one_zoom_ctrl", \
	"gstr_one_stroll", \
	"gstr_two_scroll", \
	"gstr_two_zoom", \
	"gstr_three_scroll_up_down", \
	"gstr_three_scroll_left_right", \
	"gstr_four_scroll_up_down", \
	"gstr_four_scroll_left_right", \
};
#endif

#if	(defined(M_TOUCH_ZOOM_TRACE) || defined(M_TOUCH_VOLUME_TRACE) || defined(M_TOUCH_SCROLL_ONE_TRACE) || defined(M_TOUCH_SLIDE_TRACE) || defined(M_TOUCH_THREE_TRACE) || defined(M_TOUCH_FOUR_TRACE))
const static char *m_dirString[] = \
{ \
	"dir_none", \
	"right", \
	"up_right", \
	"up", \
	"up_left", \
	"left", \
	"down_left", \
	"down", \
	"down_right", \
};
#endif




/*----------------------------------------------------------------------------//
//                                UNUSED VARIABLES                            //
//----------------------------------------------------------------------------*/






/*$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\  $$\   $$\ $$$$$$$$\  $$$$$$\  $$$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\ $$ |  $$ |$$  _____|$$  __$$\ $$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|$$ |  $$ |$$ |      $$ /  $$ |$$ |  $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |      $$$$$$$$ |$$$$$\    $$$$$$$$ |$$ |  $$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |      $$  __$$ |$$  __|   $$  __$$ |$$ |  $$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\ $$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |$$ |  $$ |$$$$$$$$\ $$ |  $$ |$$$$$$$  |
\__|       \______/ \__|  \__| \______/ \__|  \__|\________|\__|  \__|\______*/ 
/*============================================================================//
//                                   FUNCHEAD                                 //
//============================================================================*/

/*----------------------------------------------------------------------------//
//                            GLOBAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/

ret_code_t m_touch_init(void);
void m_touch_set_init(void);
ret_code_t m_touch_resume(void);
ret_code_t m_touch_scan_handler(void);

M_TOUCH_ACTIVE_AREA_T m_touch_mode_get(void);
void m_touch_mode_set(M_TOUCH_ACTIVE_AREA_T new_mode);

void clear_m_touch_db(void);
void clearAll_CypressTouchData(void);// cf. drv_cypress_req_suspend_scanning()	// 	touch 잔재가 있으면, suspend_scanning()이 듣지 않는다...

void m_MouseMove(int16_t signed_dx, int16_t signed_dy);
void m_KeyswitchDown(int8_t usb_defined_keycode, int8_t CtrlAltShift);
void m_KeyswitchUp(void);
void m_clear_inertiaScroll(void);

/*----------------------------------------------------------------------------//
//                             LOCAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
static ret_code_t scan_data(void);
static ret_code_t update(drv_cypress_info_t *pInfo);
static void change(job_state_t new);
static uint8_t is_jobstate_transition_allowed(job_state_t new);
static void change_gesture(gstr_state_t new);
static ret_code_t get_point(m_comm_evt_arg_point_t* pVal, uint8_t index);
static uint16_t delta_time(uint16_t prv, uint16_t now);
#if 0
	static touch_vector_t get_vector(touch_point_t prev, touch_point_t now);
#else
	static void get_vector_stanley(touch_point_t prev, touch_point_t now, touch_vector_t *p_vector);
#endif
static dir_type_t get_direction(uint16_t angle);
static uint8_t get_touch_num(void);
static ret_code_t get_touch_index(uint8_t* pIndex, uint8_t touch_cnt);


#if	defined(SLIDING_WINDOW_SIZE)
	#if 1
		static void get_new_stanley(uint16_t x, uint16_t y, touch_coordinate_t *p_loc);
	#else
		static touch_coordinate_t get_new(uint16_t x, uint16_t y);
	#endif
	#if	defined(SLIDING_WINDOW_FILTER)
		static void filter(uint16_t x, uint16_t y);
	#endif
#endif

static void send_release_keys(void);
static void send_release_click(void);

void copy_touch_point(touch_point_t *dest, touch_point_t *src);
void try_remove_ghost_on_OffTouch(void);
ret_code_t scan_data_dummy(void);
static void inertia_scroll(void);

/*----------------------------------------------------------------------------//
//                            POINTER FUNCTION HEADS                          //
//----------------------------------------------------------------------------*/

typedef void (*STATE_MACHINE)(void);
//----------------------------------------------------//idle
static void idle_handler(void);
static void check_tab(void);
static void update_tab(void);

//----------------------------------------------------//one_finger
static void one_finger_handler(void);
static ret_code_t check_move(uint8_t index);
static void update_move(uint8_t index);

//----------------------------------------------------//two_finger
static void two_finger_handler(void);
static ret_code_t check_scroll_two(uint8_t first, uint8_t second);
static ret_code_t check_zoom(uint8_t first, uint8_t second);
static void update_scroll_two(uint8_t first, uint8_t second);
static void update_zoom(uint8_t first, uint8_t second);

//----------------------------------------------------//three_finger
static void three_finger_handler(void);
static ret_code_t check_scroll_three(uint8_t first, uint8_t second, uint8_t third);
static void update_scroll_up_down_three(uint8_t first, uint8_t second, uint8_t third);
static void update_scroll_left_right_three(uint8_t first, uint8_t second, uint8_t third);

//----------------------------------------------------//four_finger
static void four_finger_handler(void);
static ret_code_t check_scroll_four(uint8_t first, uint8_t second, uint8_t third);
static void update_scroll_up_down_four(void);
static void update_scroll_left_right_four(void);

static const STATE_MACHINE m_touch_handler[job_state_max] = \
{\
    idle_handler, \
    one_finger_handler, \
    two_finger_handler, \
    three_finger_handler, \
    four_finger_handler, \
};



/*----------------------------------------------------------------------------//
//                             DEBUG FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
static char* m_touch_show_all_touch_db(void);
static char* m_touch_show_touch_db(uint8_t finger);
static char* m_touch_show_all_state(void);
static void GetXYFromPoint(m_comm_evt_arg_point_t *point, int16_t *SignedX, int16_t *SignedY); // by Stanley
static void SetXYToThePoint(m_comm_evt_arg_point_t *point, int16_t SignedX, int16_t SignedY); // by Stanley


/*----------------------------------------------------------------------------//
//                             UNUSED FUNCTION HEADS                          //
//----------------------------------------------------------------------------*/
static ret_code_t check_scroll(uint8_t index);//삭제예정
static void update_scroll_one(uint8_t index);//삭제예정
static void update_zoom_ctrl(uint8_t index);//삭제예정

static ret_code_t check_volume_ctrl(uint8_t index);//삭제예정
static void update_volume_ctrl(uint8_t index);

static ret_code_t check_drag_move(uint8_t index);//삭제예정
static void update_drag(uint8_t index);//삭제예정

typedef void (*m_timer_handler_t)(void);//???




/*\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ 
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\_*/















/*$$$$\  $$\       $$$$$$\  $$$$$$$\   $$$$$$\  $$\                         
$$  __$$\ $$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                        
$$ /  \__|$$ |     $$ /  $$ |$$ |  $$ |$$ /  $$ |$$ |                        
$$ |$$$$\ $$ |     $$ |  $$ |$$$$$$$\ |$$$$$$$$ |$$ |                        
$$ |\_$$ |$$ |     $$ |  $$ |$$  __$$\ $$  __$$ |$$ |                        
$$ |  $$ |$$ |     $$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |                        
\$$$$$$  |$$$$$$$$\ $$$$$$  |$$$$$$$  |$$ |  $$ |$$$$$$$$\                   
 \______/ \________|\______/ \_______/ \__|  \__|\________|                  
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\ 
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__|
//============================================================================//
//                                 GLOBAL FUNCTIONS                           //
//============================================================================*/



/*============================================================================//
//                            m_touch_init()                              
//============================================================================*/
/*----------------------------------------------------------------------------//
20190524_flimbs
	터치코드 개편테스트중	
20190510_flimbs
	UTF 변환/
00000000_flibs
	터치 이니셜라이징
//----------------------------------------------------------------------------*/
ret_code_t m_touch_init(void)
{	
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(void)");
	m_mode 				= M_TOUCH_CYPRESS_ACTIVE_AREA_WHOLE;
	m_state.jobState 	= job_state_idle;
	m_state.jobState_old = job_state_idle;
    
	//drv_twi_init(true);		// MOVE to m_mokibo.c, deleted by jaehong 2019/07/02
	drv_cypress_init();
	drv_cypress_mode_set(DRV_CYPRESS_ACTIVE_AREA_WHOLE);
	
	memset(m_touch_db, 0, sizeof(m_touch_db));
	memset(&m_state, 0, sizeof(job_state_db_t));
	
    return NRF_SUCCESS;
}



/*============================================================================//
//                            m_touch_mode_get()                              
//============================================================================*/
/*----------------------------------------------------------------------------//
20190524_flimbs
	없어도 될거같은 함수인데.. 확인해봐야됨
//----------------------------------------------------------------------------*/
void m_touch_set_init(void)
{	
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(void)");	
	m_mode 				= M_TOUCH_CYPRESS_ACTIVE_AREA_WHOLE;
	m_state.jobState 	= job_state_idle;
	m_state.jobState_old = job_state_idle;	
	
	drv_cypress_mode_set(DRV_CYPRESS_ACTIVE_AREA_WHOLE);
	
	memset(m_touch_db, 0, sizeof(m_touch_db));
	memset(&m_state, 0, sizeof(job_state_db_t));
}



/*============================================================================//
//                            m_touch_resume()                              
//============================================================================*/
/*----------------------------------------------------------------------------//
00000000_MJ
	생성
//----------------------------------------------------------------------------*/
ret_code_t m_touch_resume(void)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(void)");	
	//m_mode 			= M_TOUCH_CYPRESS_ACTIVE_AREA_WHOLE;// <-- 이전의 모드를 유지해야 하므로, 바꾸지 않는다.
	m_state.jobState 	= job_state_idle;
	m_state.jobState_old = job_state_idle;	
	
	//drv_twi_init(true);	// <-- resume이므로 drv_twi_init()을 하지 않는다.
	drv_cypress_init();
	drv_cypress_mode_set(DRV_CYPRESS_ACTIVE_AREA_WHOLE);
	
	memset(m_touch_db, 0, sizeof(m_touch_db));
	memset(&m_state, 0, sizeof(job_state_db_t));

    return NRF_SUCCESS;
}

int8_t m_inertia_scroll_updown = 0;
uint8_t m_inertia_trvldist = 0;
uint8_t m_inertia_repeat = 0;
uint8_t m_inertia_interval = 0;
uint8_t m_first = 0; 
uint8_t m_second = 0;
uint8_t overspeed_cnt=0;
 

void m_clear_inertiaScroll(void)
{
	if(m_inertia_repeat != 0)
	{
		m_inertia_scroll_updown = 0;
		m_inertia_trvldist = 0;
		m_inertia_repeat = 0;
		m_inertia_interval = 0;
		m_first = 0; 
		m_second = 0;
		overspeed_cnt=0;
	}
}

/*============================================================================//
//                            m_touch_scan_handler()	
//============================================================================*/
/*----------------------------------------------------------------------------//
00000000_MJ
	생성
//----------------------------------------------------------------------------*/

ret_code_t m_touch_scan_handler(void)
{
	
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(void)");
	________________________________LOG_mTouch_oneTrace(-1,"\n");
	________________________________LOG_mTouch_oneTrace(FUNCHEAD_FLAG,"(void)");
	ret_code_t rv;
	job_state_t touch_num;
	
	if(m_mokibo_get_status() != M_MOKIBO_TOUCH_STATUS)
	{
		________________________________LOG_mTouch_TTINTtrace(1,"scan_data_dummy();");	
		
		scan_data_dummy();
		return NRF_ERROR_NOT_FOUND;
	}
	
	rv = scan_data();

	if((NRF_SUCCESS != rv) && (NRF_ERROR_NOT_FOUND != rv) && (NRF_ERROR_NOT_SUPPORTED != rv))
	{			
		return rv;
	}

	touch_num = (job_state_t)get_touch_num();

	if(touch_num > M_TOUCH_COUNT_MAX)
	{
		touch_num = job_state_idle;
	}

	change(touch_num); //이전m_state.jobState, new.touch_num 에 따라 , m_state.touch_num, jobState 변경

	

	________________________________LOG_mTouch_touchDb(0,"%s     %s",m_touch_show_all_state(), m_touch_show_all_touch_db());
	
	m_touch_handler[m_state.jobState]();

	inertia_scroll();
	
	

	check_tab();
	return NRF_SUCCESS;	


}




/*============================================================================//
//                            m_touch_mode_get()                              
//============================================================================*/
/*----------------------------------------------------------------------------//
00000000_MJ
	생성
//----------------------------------------------------------------------------*/
M_TOUCH_ACTIVE_AREA_T m_touch_mode_get(void)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(void)");
	return m_mode;
}




/*============================================================================//
//                            m_touch_mode_set()
//============================================================================*/
/*----------------------------------------------------------------------------//
00000000_MJ
	생성
//----------------------------------------------------------------------------*/
void m_touch_mode_set(M_TOUCH_ACTIVE_AREA_T new_mode)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"M_TOUCH_ACTIVE_AREA_T new_mode:%d",new_mode);
	if(m_mode != new_mode )
	{
		m_mode = new_mode;

		#if (AREA_PROPERTIE == AREA_PROPERTIE_ALWAYSFORCE_TO_WHOLE_AREA)
			drv_cypress_mode_set(DRV_CYPRESS_ACTIVE_AREA_WHOLE); // <-- AREA_LEFT를 무시하고, 전체모드로
		#elif (AREA_PROPERTIE == AREA_PROPERTIE_ORIGINAL)
		switch(m_mode)
		{
			case M_TOUCH_CYPRESS_ACTIVE_AREA_WHOLE : 
				drv_cypress_mode_set(DRV_CYPRESS_ACTIVE_AREA_WHOLE);
				break;

			case M_TOUCH_CYPRESS_ACTIVE_AREA_LEFT : 
				drv_cypress_mode_set(DRV_CYPRESS_ACTIVE_AREA_LEFT);
				//drv_cypress_mode_set(DRV_CYPRESS_ACTIVE_AREA_WHOLE); // <-- AREA_LEFT를 무시하고, 전체모드로
				break;

			case M_TOUCH_CYPRESS_ACTIVE_AREA_RIGHT : 
				drv_cypress_mode_set(DRV_CYPRESS_ACTIVE_AREA_RIGHT);
				//drv_cypress_mode_set(DRV_CYPRESS_ACTIVE_AREA_WHOLE); // <-- AREA_RIGHT를 무시하고, 전체모드로
				break;

			default : 
				DEBUG("OOPS[%d]!!", m_mode);
			break;
		}		
		#elif(AREA_PROPERTIE == AREA_PROPERTIE_FLiMBS_20190121_MOD_TOUCH_WHOLE_AREA_WITH_FILTER_KEYS)		
			drv_cypress_mode_set(DRV_CYPRESS_ACTIVE_AREA_WHOLE); // <-- AREA_LEFT를 무시하고, 전체모드로
		#endif		
	}
}




/*============================================================================//
//                            void clear_m_touch_db(void)
//============================================================================*/
/*----------------------------------------------------------------------------//
20190101_stanley
	local db만 clear 된다고 해서 모두 해결되는 것이 아니다.
	cypress touch chip (parade touch chip)에 이미 축적되어 있는 과거 데이터,
	즉, clickbar touch이전에 이미 cypress chip에 쌓여 있던 과거 데이터를 없애야 진정 clear된다.
//----------------------------------------------------------------------------*/

void clear_m_touch_db(void)
{	
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(void)");
	uint8_t i;
	//ret_code_t rv;	
	// 여기에서, 잠깐동안 --- Cypress HW Reset을 하면 어떨까 ---
	//cypress_pwr(1);  // power on과정에서, 200ms정도 reset이 출력되었다가 사라진다.
	
	// Local Buffer를 clear한다.
	for(i=0; i < 4; i++)
	{
		memset(&m_touch_db[i], 0, sizeof(touch_touch_t));
		m_touch_db[i].w_index= 0;
		//
		memset(&m_touch_point_begin[i], 0, sizeof(touch_point_t));
	}
	
	// TT_INT가 Low인 동안은, cypress chip 내에 축적되어 있는 point 정보들을 쓸어 없애야 한다.
	//while(!cypress_read_TT_INT()) // <-- TT_INT단자에 pull-up이 없는 경우에는 infinite loop가 될 수 있다.
	clearAll_CypressTouchData(); // in clear_m_touch_db()	
}




/*============================================================================//
//                            void clearAll_CypressTouchData(void)
//============================================================================*/
/*----------------------------------------------------------------------------//
20190101_stanley
	TT_INT가 Low인 동안은, cypress chip 내에 축적되어 있는 point 정보들을 쓸어 없애야 한다.
	while(!cypress_read_TT_INT()) // <-- TT_INT단자에 pull-up이 없는 경우에는 infinite loop가 될 수 있다.
//============================================================================*/
void clearAll_CypressTouchData(void)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(void)");	
	ret_code_t rv;

	while(1) // <-- TT_INT에 pull-up이 없더라도, 데이터가 존재하는 한은 모두 읽어 없앤다.
	{
		// 
		________________________________LOG_mTouch_TTINTtrace(1,"if(!cypress_read_TT_INT())");	
		//if(!cypress_read_TT_INT())
		{
			________________________________LOG_mTouch_TTINTtrace(1,"rv= scan_data_dummy();");	
			rv= scan_data_dummy();		
			if(NRF_SUCCESS != rv) break;
		}
		
		// 터치 데이터가 정상 반환되었으므로... 추가로 더 존재하는지 확인한다.
		// Watchdog에 걸릴 수도 있으므로, 무시한다.
		Update_WDT();				

		// ghost가 일을 때에도, 이 명령은 전달될 수 있다해서...정말인가?
		#ifdef BASELINEINIT_USE		
			________________________________LOG_mTouch_TTINTtrace(1,"drv_cypress_init_Baseline();");	
			drv_cypress_init_Baseline();
			drv_cypress_init_Baseline();
		#endif

		// TT_INT pin이 low 이라면, 아직도 뭔가 남아 있는 것이다.
		if(!cypress_read_TT_INT())
		{
			// 더 읽어내어 없애 버린다.
			________________________________LOG_mTouch_TTINTtrace(1,"continue;");	
			continue;
		} 
		else 
		{
			// 그 사이 혹시, 다음 터치 데이터가 쌓일 수도 있으므로, 한번 더 기다려 본다.
			nrf_delay_ms(2);
			#ifdef BASELINEINIT_USE
				________________________________LOG_mTouch_TTINTtrace(1,"drv_cypress_init_Baseline();");	
				drv_cypress_init_Baseline();
				//drv_cypress_init_Baseline();
			#endif
		}
	}	
}










/*\       $$$$$$\   $$$$$$\   $$$$$$\  $$\                                             
$$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                                            
$$ |     $$ /  $$ |$$ /  \__|$$ /  $$ |$$ |                                            
$$ |     $$ |  $$ |$$ |      $$$$$$$$ |$$ |                                            
$$ |     $$ |  $$ |$$ |      $$  __$$ |$$ |                                            
$$ |     $$ |  $$ |$$ |  $$\ $$ |  $$ |$$ |                                            
$$$$$$$$\ $$$$$$  |\$$$$$$  |$$ |  $$ |$$$$$$$$\                                       
\________|\______/  \______/ \__|  \__|\________|                                      
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\  $$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |$$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |$$ /  \__|
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |\$$$$$$\  
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ | \____$$\ 
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |$$\   $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |\$$$$$$  |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__| \______/ 
//============================================================================//
//                                 LOCAL FUNCTIONS                            //
//============================================================================*/



/*============================================================================//
//                            scan_data()
//============================================================================*/
/*----------------------------------------------------------------------------//
20190525_flimbs
	스캔.
//----------------------------------------------------------------------------*/
static ret_code_t scan_data(void)
{	
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(void)");
	ret_code_t rv;
	drv_cypress_info_t touchInfo;	
	rv = drv_cypress_read(&touchInfo);
	
	if(NRF_SUCCESS == rv) 
	{				
		m_state.no_data = 0;// Update local DB
		
		rv = update(&touchInfo);
		

		________________________________LOG_mTouch_touchDbTrace(4,"id:%d,w:%d,0:{%04d,%04d,%04d},1:{%04d,%04d,%04d},2:{%04d,%04d,%04d}, dlta:{%04d}, sped:{%04d}, angl:{%04d}",0,m_touch_db[0].w_index,	m_touch_db[0].point[0].loc.x,m_touch_db[0].point[0].loc.y,m_touch_db[0].point[0].timestamp,m_touch_db[0].point[1].loc.x,m_touch_db[0].point[1].loc.y,m_touch_db[0].point[1].timestamp,m_touch_db[0].point[2].loc.x,m_touch_db[0].point[2].loc.y,m_touch_db[0].point[2].timestamp,m_touch_db[0].vector.delta,m_touch_db[0].vector.speed,m_touch_db[0].vector.angle);
		#ifdef IGNORE_SHORT_TERM_RELEASE
			m_timer_set(M_TIMER_ID_ALL_TOUCHES_RELEASED, TOUCH_ALIVE_DURATION);
		#endif
	}
	else
	{	
		________________________________LOG_mTouch_touchDbTrace(9,"//%d::drv_cypress_read(&touchInfo):%d  m_state.no_data:%d",rv,m_state.no_data);
		if(NRF_ERROR_NOT_FOUND == rv || NRF_ERROR_NULL == rv)
		{
			if(++m_state.no_data >= DB_CLEAR_NO_DATA_COUNT)//no_data 의 허용횟수를 초과하면 touchdb를 비운다.
			{														
				memset(&m_touch_db[0], 0, sizeof(m_touch_db));//TODO: 완전히 클리어 해야하지 않을까	
				#if	defined(SLIDING_WINDOW_SIZE)
					memset(&m_move_window, 0, sizeof(window_t));
				#endif				
				change(job_state_idle); //TODO: change() 에서 관리하면 되지않을까
				m_state.no_data = 0;
			}			
			return NRF_ERROR_NOT_FOUND;					
		}
		else
		{
			m_state.no_data = 0;
			return NRF_ERROR_NOT_FOUND;					
		}		
	}
	return rv;
}





/*============================================================================//
//                            update()
//============================================================================*/
/*----------------------------------------------------------------------------//
00000000_MJ
	생성
//----------------------------------------------------------------------------*/
static ret_code_t update(drv_cypress_info_t *pInfo)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(drv_cypress_info_t *pInfo)");
	uint8_t i;
	uint8_t id;//cypress 칩에서 정해준 info[i].touchID
	uint8_t index; // touch_touch_t 의 write index
	int16_t direction=0, mutual_delta=0, mutual_distance=0;
	touch_point_t *p_touch_point; 
	
	if(NULL == pInfo)
	{
		return NRF_ERROR_NULL;
	}

/*위치 옮김
#ifdef IGNORE_SHORT_TERM_RELEASE// (1) -> touchCnt가 zero이더라도(즉, 터치가 모두 release 되었더라도) 특정시간 이상 zero가 아니면, 마치 터치가 계속되고 있는 것처럼 처리한다.		
	{
		static uint16_t timestamp_prev=0;
		int16_t delta;
		//
		delta = pInfo->timestamp - timestamp_prev;
		timestamp_prev= pInfo->timestamp;
		
		if(pInfo->touchCnt  > 0 || delta < TOUCH_ALIVE_DURATION*10)
		{
			//________________________________LOG_mTouch_touchDbTrace(3,"BIGTIMEGAP");
			//m_timer_set(M_TIMER_ID_ALL_TOUCHES_RELEASED, TOUCH_ALIVE_DURATION); // 50ms
		}
	}
#endif //IGNORE_SHORT_TERM_RELEASE*/
	
	if((pInfo->touchCnt == 0) || (M_TOUCH_COUNT_MAX < pInfo->touchCnt)) //손가락이 떨어져있거나 4보다크면
	{
		return NRF_ERROR_INVALID_DATA;
	}

	for(i = 0; i < pInfo->touchCnt; i++)// 손가락 각각에 대하여
	{
		id = pInfo->info[i].touchID; //손가락 이름  예) 1~6점 터치하다가 0, 1번을 띠면 touchID가 2, 3, 4, 5를 받게됨. 즉, id=5 > 4 일 수 있음.

		index = m_touch_db[id].w_index; // DB의 손가락이름의 Write Index
		
		if(index > M_POINT_BUF_NUM) // 손가락 큐 갯수가 비정상인지 체크
		{			
			memset(&m_touch_db[id], 0, sizeof(touch_touch_t));  //비정상이면 초기화
			________________________________LOG_mTouch_touchDbTrace(9,"//if(index > M_POINT_BUF_NUM).. m_touch_db[id] reset");
		}
		
		#ifdef  M_TOUCH_USE_LIFT_OFF_CANCEL // 버퍼에 1개 이상 저장되어 있고, 이전 point가 떨어졌을때면, 초기화// =*= Jeff's idea: DO NOT CLEAR the touch count even when it is decreased!	// =*= Keep the increased touch count untill all of the touches are released.
			if(index)
			{
				if(EVENT_ID_TYPE_LIFTOFF == m_touch_db[id].point[index-1].attr.eventID)
				{
					// Clear
					memset(&m_touch_db[id], 0, sizeof(touch_touch_t));
					index = m_touch_db[id].w_index;					
				}
			}
		#endif 

		if((M_POINT_BUF_NUM) == index) // 손가락 큐가 가득 찼을때
		{ 
			touch_point_t temp[M_POINT_BUF_NUM] = {0, };// 터치 포인트 복사하기위한 임시변수.
			//point<[E][F][G]>[ ]   temp[-][-][-] 
			memcpy(&temp[0], &m_touch_db[id].point[0], sizeof(touch_point_t)*(M_POINT_BUF_NUM)); // 1) temp[-][-][-] 에 m_touch_db[id].point[0][1][2] 복사
			//point<[E][F][G]>[ ] => temp<[E][F][G]>
			memcpy(&m_touch_db[id].point[0], &temp[1], sizeof(touch_point_t)*(M_POINT_BUF_NUM-1));// 2) copy: point[1][2] <--- temp[0][1][2] temp 의 맨앞 하나 자르고 [1]에 넣음
			//point<[F][G]>[G][ ] <= temp[E]<[F][G]>
			m_touch_db[id].w_index--; // w_index=2			// 3) manage index, reload	( writing index도 하나 감소)
			//point[F][G][ ]
			index = m_touch_db[id].w_index; // 인덱스 --
		}

		p_touch_point = &(m_touch_db[id].point[index]);
		
		p_touch_point->timestamp = pInfo->timestamp;
		p_touch_point->attr.tip = pInfo->info[i].tip;
		p_touch_point->attr.eventID = pInfo->info[i].eventID;
		p_touch_point->attr.touchID = pInfo->info[i].touchID;		
		
		________________________________LOG_mTouch_touchDbTrace(2,"id:%d,w:%d,0:{%04d,%04d,%04d},1:{%04d,%04d,%04d},2:{%04d,%04d,%04d}, dlta:{%04d}, sped:{%04d}, angl:{%04d}",0,	m_touch_db[0].w_index,	m_touch_db[0].point[0].loc.x,m_touch_db[0].point[0].loc.y,m_touch_db[0].point[0].timestamp,m_touch_db[0].point[1].loc.x,m_touch_db[0].point[1].loc.y,m_touch_db[0].point[1].timestamp,m_touch_db[0].point[2].loc.x,m_touch_db[0].point[2].loc.y,m_touch_db[0].point[2].timestamp,m_touch_db[0].vector.delta,m_touch_db[0].vector.speed,m_touch_db[0].vector.angle);
		#if	defined(SLIDING_WINDOW_SIZE)
			if(0==id)//손가락 하나일때 windowsize 설정
			{				
				//p_touch_point->loc= get_new(pInfo->info[i].x, pInfo->info[i].y);
				get_new_stanley(pInfo->info[i].x, pInfo->info[i].y, &(p_touch_point->loc) );
			}
			else
			{
				p_touch_point->loc.x = pInfo->info[i].x;
				p_touch_point->loc.y = pInfo->info[i].y;
			}
		#else
			p_touch_point->loc.x = pInfo->info[i].x;
			p_touch_point->loc.y = pInfo->info[i].y;
		#endif
		
		//touch_point_begin 설정
		if(	m_touch_point_begin[id].loc.x == 0 && 
			m_touch_point_begin[id].loc.y == 0 &&
			m_touch_point_begin[id].timestamp == 0)
		{
			if(p_touch_point->loc.x != 0 || p_touch_point->loc.y != 0)
			{
				copy_touch_point( &(m_touch_point_begin[id]),  p_touch_point);
			}
		}
		m_touch_db[id].w_index++;		
	}
	________________________________LOG_mTouch_touchDbTrace(3,"id:%d,w:%d,0:{%04d,%04d,%04d},1:{%04d,%04d,%04d},2:{%04d,%04d,%04d}, dlta:{%04d}, sped:{%04d}, angl:{%04d}",0,	m_touch_db[0].w_index,	m_touch_db[0].point[0].loc.x,m_touch_db[0].point[0].loc.y,m_touch_db[0].point[0].timestamp,m_touch_db[0].point[1].loc.x,m_touch_db[0].point[1].loc.y,m_touch_db[0].point[1].timestamp,m_touch_db[0].point[2].loc.x,m_touch_db[0].point[2].loc.y,m_touch_db[0].point[2].timestamp,m_touch_db[0].vector.delta,m_touch_db[0].vector.speed,m_touch_db[0].vector.angle);	


	for(int j=0; j < M_TOUCH_COUNT_MAX; j++)// j < 4 
	{
		index = m_touch_db[j].w_index;
		if(index >= M_POINT_ENOUGH_NUM) // index >= 
		{
			// 현재의 포인트가 [2]일 때,  [0]--->[2] 사이의 vector를 구함.
			#if 0
				m_touch_db[j].vector = get_vector(m_touch_db[j].point[index-3],   m_touch_db[j].point[index-1]);
				//m_touch_db[j].vector = get_vector(m_touch_point_begin[j], m_touch_db[j].point[index-1]);
			#else				
				get_vector_stanley( m_touch_db[j].point[index-3], m_touch_db[j].point[index-1],  &(m_touch_db[j].vector) );
			#endif			
		}
		else
		{
			m_touch_db[j].vector.speed = 0;
			m_touch_db[j].vector.angle = INVALID_ANGLE;
			m_touch_db[j].point[0].loc.x = p_touch_point->loc.x;
			m_touch_db[j].point[0].loc.y = p_touch_point->loc.y;
		}

		
		#ifdef  M_TOUCH_USE_LIFT_OFF_CANCEL // Intentional blocking .... Stanley// =*= Jeff's idea: DO NOT CLEAR the touch count even when it is decreased!// =*= Keep the increased touch count untill all of the touches are released.
			if(	// (Lift Off Event) or  (i >= Actual Touch Count)
				m_touch_db[j].point[0].attr.eventID == EVENT_ID_TYPE_LIFTOFF ||
				m_touch_db[j].point[1].attr.eventID == EVENT_ID_TYPE_LIFTOFF ||
				m_touch_db[j].point[2].attr.eventID == EVENT_ID_TYPE_LIFTOFF ||
				j >= pInfo->touchCnt
			){
				// (1) Lift-off Clear : ths event is not always received ...
				// (2) Zero-based ID should be less than Actual Touch Count
				if(m_touch_db[j].point[0].attr.eventID != EVENT_ID_TYPE_NO_EVENT)
				{
					#if 1 // seems to make disturbance in tapping ??  No!!!!
						memset(&m_touch_db[j], 0, sizeof(touch_touch_t));
						m_touch_db[j].w_index= 0;
						DEB("==== cleared: [id=%d] === \n",j);
					#endif
				}
			}
		#endif //M_TOUCH_USE_LIFT_OFF_CANCEL
		

	}//for(i=;i<M_TOUCH_COUNT_MAX;)

	return NRF_SUCCESS;
}




/*============================================================================//
//                            change()
//============================================================================*/
/*----------------------------------------------------------------------------//
00000000_MJ
	get_touch_num 으로부터 손가락 갯수를 받아 
	m_state.job_state와 change_gesture(none) 를 한다.
//----------------------------------------------------------------------------*/
static void change(job_state_t new)
{	
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(job_state_t new:%d) zommStartThreashold:%d",new,zoomStartThreshold);
	________________________________LOG_mTouch_oneTrace(FUNCHEAD_FLAG,"(job_state_t new:%d) zommStartThreashold:%d",new,zoomStartThreshold);
	if(m_state.jobState == new)
	{
		return;
	}

	________________________________LOG_mTouch_oneTrace(0,"if(!is_jobstate_transition_allowed(new))");
	if(!is_jobstate_transition_allowed(new))
	{
		________________________________LOG_mTouch_oneTrace(1,"return;");
		________________________________LOG_mTouch_isJobstateTransitionAllowed(1,"________________is_jobstate_transition_allowed == false");
		return;
	}

	if(new ==job_state_idle)
	{
		if(zoomStartThreshold=!0)
		{
			zoomStartThreshold=0;
		}
	}


	if(m_state.jobState == job_state_idle)
	{
		if(new != job_state_idle)
		{// 0 -> N
			m_state.tab_number = (uint8_t)new;	// tab 정보 저장
			m_timer_set(M_TIMER_ID_MULTI_TOUCH_TAB, TAB_ALIVE_TIME);
		}
	}
	else
	{	
		if(new != job_state_idle)
		{// N -> N
			if(!m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_TAB))
			{					
				if((uint8_t)new> m_state.tab_number)
				{							
					m_state.tab_number = (uint8_t)new;//줄어들어도 tab_number 는 안바뀐다
					m_timer_set(M_TIMER_ID_MULTI_TOUCH_TAB, TAB_ALIVE_TIME);
				}					
			}
		}
	}		
	________________________________LOG_mTouch_jobState_trace(1,"jobState:[tab:%d, new:%d, old:%d, oold:%d]",m_state.tab_number,new, m_state.jobState);	
	m_state.jobState_old = m_state.jobState;
	m_state.jobState = new;
	change_gesture(gstr_state_none);// Reset to gstr_state_none		
	return;
}




/*============================================================================//
//                           is_jobstate_transition_allowed()
//============================================================================*/
/*----------------------------------------------------------------------------//
00000000_flimbs
	change 하면 안되는 상황들을 걸러낸다.
	shortTerm Release / gesture lock / 
//----------------------------------------------------------------------------*/
uint8_t is_jobstate_transition_allowed(job_state_t new)
{
	
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(job_state_t new):%d",new);
	uint8_t allow = 0;	// not allowed	
	if(new == job_state_idle)
	{	
		#ifdef IGNORE_SHORT_TERM_RELEASE				
		// 그러나, 이 또한, parade 터치칩에서 일시적으로 모든 터치가 release된 것처럼 되는 경우가 있으므로
		// 모든 터치가 release된 것이 확실할 때까지는 기다려야 한다.
		// 즉, 마지막 터치 포인트가 인지된 시점부터 300ms 이상 아무 터치도 발생하지 않는 경우에
		// 비로소 모든 터치가 release되었다고 판단한다.
		if(!m_timer_is_expired(M_TIMER_ID_ALL_TOUCHES_RELEASED)) // see: m_timer.h
		{
			// 진정으로 모든 터치가 release된 것이 아닐 수 있으므로... jobstate변경 불가.						
			return 0; // NOT allowed
		}
		#endif
		// any transition is allowed in idle state
		return +1; // allowed
	}
	
	if(m_state.jobState == job_state_idle)
	{
		// any transition from the idle state is allowed
		return +1; // allowed
	}

	if(m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_TAB))
	{
		if(m_state.jobState == job_state_one && m_state.jobState < new)
		{
			return 0;
		}
	}

	#ifdef	USE_GESTURE_LOCK
	//-----------------------------------------------------------------------------------------------------------------------------------
	// Stanley:
	//	After the multi-tapping time passed, no change is admitted !!
	if(m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_TAB))
	{
		
	#if 1		
	// Stanley's
	// no more change is admitted !
	DEB("No more touch-change is admitted...\n");
	return 0;			
	#else	

	// MJ's f[]f
	job_state_t touch_num = (job_state_t)get_touch_num();
	// do not change the jobState during the 'gstr_one_move'
	if(m_state.jobState == gstr_one_move && touch_num > 1)
	{
		#ifdef M_TOUCH_STATE_TRACE
		DEB("More touch is not allowed during the mouse moving...\n");
		#endif
		return 0;
	}
	#endif
	}
	#endif //USE_GESTURE_LOCK

	
	
	if(gstr_state_none == m_state.gestureState)
	{
		// transition to any gesture is allowed in gesture-none-state
		allow = 1; 
		//
		#if	defined(M_TOUCH_STATE_TRACE)
		DEB("#AllowTransition: from Job[%s] Gesture(%s)-> Job[%s] \n",
			m_jobString[m_state.jobState], 
			m_gString[m_state.gestureState], 
			m_jobString[new]
		);
		#endif
	}
	
	if(gstr_one_move == m_state.gestureState)
	{
		// transition to any gesture is allowed in pointing mode
		allow = 1;
		
		//
		#if	defined(M_TOUCH_STATE_TRACE)
		DEB("#AllowTransition: from Job[%s] Gesture(%s)-> Job[%s] \n",
			m_jobString[m_state.jobState], 
			m_gString[m_state.gestureState], 
			m_jobString[new]
		);
		#endif
	}

	
	// 3점 스크롤은 처음에 2점 스크롤로 인식 되는 경우가 많음
	// 2점 스크롤이 된지 짧은 시간에서는 3점으로 변경 허용
	if((gstr_two_scroll == m_state.gestureState) && (m_state.gestureLevel < 2))
	{ 
		allow = 1; 
		//
		#if	defined(M_TOUCH_STATE_TRACE)
		DEB("#AllowTransition: from Job[%s] Gesture(%s)-> Job[%s] \n",
			m_jobString[m_state.jobState], 
			m_gString[m_state.gestureState], 
			m_jobString[new]
		);
		#endif
	}
	
	// 4점 좌우 스크롤은 처음에 3점 스크롤로 인식 되는 경우가 많음
	// 3점 좌우 스크롤이 된지 짧은 시간에서는 4점으로 변경 허용
	if((gstr_three_scroll_left_right == m_state.gestureState) 
			&& (!m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_TAB))
			&& (job_state_four == new))
	{ 
		allow = 1; 
		//
		#if	defined(M_TOUCH_STATE_TRACE)
		DEB("#AllowTransition: from Job[%s] Gesture(%s)-> Job[%s] \n",
			m_jobString[m_state.jobState], 
			m_gString[m_state.gestureState], 
			m_jobString[new]
		);
		#endif
	}

	
	#if	defined(M_TOUCH_STATE_TRACE)
	if(!allow) // Touch Change ... Not allowed
	{
		DEB("from Job[%s] Gesture(%s)-> Job[%s].... #--ignored--#\n",
			m_jobString[m_state.jobState], 
			m_gString[m_state.gestureState], 
			m_jobString[new]
		);
	}
	#endif

	
	return allow;
}




/*============================================================================//
//                           change_gesture()
//============================================================================*/
/*----------------------------------------------------------------------------//
00000000_MJ
	생성
//----------------------------------------------------------------------------*/
static void change_gesture(gstr_state_t new)
{			
	
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(gstr_state_t new:%d)",new);

	
	//if(new == gstr_state_none)//Stanley:
	
	if((new == gstr_state_none) && (m_state.gestureState != gstr_state_none) && m_state.gestureState != gstr_two_scroll)// Stanley: 
	{
		// try to remove any possible ghost in the cypress touch device
		// 주의: 'ghost'는 사라질 수도 있지만, 혹여 사람이 터치하고 있는 상태까지도 없어지는 것은 아니다...
		//		(1) gstr_state_none으로 천이하지 않은 상태에���(즉, 터치를 계속 유지하고 있는 상태에서)
		//		(2) 클릭바 터치를 떼는 경우(즉, 터치모드에서 키보드모드로 천이하는 경우)
		//		(3) cypress touch device에는 여전히 터치가 유지되고 있으므로, 이를 해소할 수 있는 방법이 없다(사람이 손을 떼지 않는 이상)
		//		(4) 그래서---> 이때에는 '경고'의 표시로서 LED를 빠르게 Blinking한다?
		//
		//SYSTEM_MSG("=== Try to remove ghost on 'gesture_none'===\n");
		// 주의: clickbar button click timing에 영향을 줄수 있다.
		//drv_cypress_init_Baseline();
		//drv_cypress_init_Baseline();		
		
		________________________________LOG_mTouch_baseLineInitTrace(1,"baselineInit");
		#ifdef BASELINEINIT_USE		
			________________________________LOG_mTouch_baseLineInitTrace(1,"baselineInit");
			drv_cypress_init_Baseline();
		#endif
		drv_cypress_req_suspend_scanning();		
		#ifdef BASELINEINIT_USE
			________________________________LOG_mTouch_baseLineInitTrace(1,"baselineInit");
			drv_cypress_init_Baseline();
		#endif
		drv_cypress_req_resume_scanning();
		
	}
	
	// New state
	if(m_state.gestureState != new)
	{
		#ifdef USE_GESTURE_LOCK	//Stanley inserted...	// cf. is_jobstate_transition_allowed()
		if(m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_TAB))
		{						
			// 일단 Gesture  Lock이 된 상태에서는...			
			if(new == gstr_state_none || m_state.gestureState == gstr_state_none)
			{	
				// none에서 다른 상태로 또는 다른 상태에서 none으로 바뀌는 경우에는 변경 허용.				
				#ifdef IGNORE_SHORT_TERM_RELEASE		
				// 그러나, 이 또한, parade 터치칩에서 일시적으로 모든 터치가 release된 것처럼 되는 경우가 있으므로
				// 모든 터치가 release된 것이 확실할 때까지는 기다려야 한다.
				// 즉, 마지막 터치 포인트가 인지된 시점부터 300ms 이상 아무 터치도 발생하지 않는 경우에
				// 비로소 모든 터치가 release되었다고 판단한다.
				if(!m_timer_is_expired(M_TIMER_ID_ALL_TOUCHES_RELEASED)) // see: m_timer.h
				{					
					return;// 진정으로 모든 터치가 release된 것이 아닐 수 있으므로... 변경 불가.					
				}
			#endif //IGNORE_SHORT_TERM_RELEASE				
			//////////////////////////////////////////////
			// 진정으로 모든 터치가 release되었으므로... 이젠 변경 가능  //
			//////////////////////////////////////////////				
			} 
			else 
			{
				#if	defined(M_TOUCH_STATE_TRACE)
				DEB("## change_gesture('%s') is not allowed ##\n",m_gString[new]);
				#endif				
				return;// 그 외에는 변경 불가.
			}
		}	
		#endif //USE_GESTURE_LOCK
	
		//#if	defined(M_TOUCH_STATE_TRACE)
		//DEBUG("[%s][%d]==>[%s]", m_gString[m_state.gestureState], m_state.gestureLevel, m_gString[new]);
		//DEB("#change_gesture: %s (%d) ==> %s \n", m_gString[m_state.gestureState], m_state.gestureLevel, m_gString[new]);
		//#endif				
		if(gstr_three_scroll_left_right == m_state.gestureState)// gstr_three_scroll_left_right에서 빠져 나갈때
		{
			//if(M_COMM_CENTRAL_MICROSOFT == m_comm_current_central_type())// Windows에서 3점 left or right scroll을 최종 빠져났을때 // 모든 키(alt, shift..)  복귀
			{
				send_release_keys();
			}
		}

		if(gstr_one_drag == m_state.gestureState)// gstr_one_drag에서 빠져 나갈때
		{			
			send_release_click();// 모든 마우스 click off
		}

		if(gstr_two_zoom == m_state.gestureState)
		{
			send_release_keys();
		}
		
		if((gstr_state_none != new) && (gstr_one_move != new) && (gstr_one_drag != new))// 어떤 제스쳐 모드로 가면(gstr_one_move 제외) TAB은 무시
		{						
			m_timer_set(M_TIMER_ID_MULTI_TOUCH_TAB, 0);// Turn-off timer immediately for next use
		}		
		

		________________________________LOG_mTouch_gstrState_trace(1,"change gstState:[tab:%d, new:%d, old:%d, oold:%d, lvl:%d]",m_state.tab_number,new, m_state.jobState,m_state.gestureState_old,m_state.gestureLevel);
		m_state.gestureState_old = m_state.gestureState;
		m_state.gestureState = new;// Store new gstr_state	
		m_state.gestureLevel = 0;// Reset gesture 		
	}
}







/*============================================================================//
//                            get_point()
//============================================================================*/
/*----------------------------------------------------------------------------//
00000000_MJ
	생성
//----------------------------------------------------------------------------*/
static ret_code_t get_point(m_comm_evt_arg_point_t* pVal, uint8_t index)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(m_comm_evt_arg_point_t* pVal, uint8_t index)");
	static uint16_t timestamp_sent;
	int16_t diffX, diffY; // 주의: signed 16-bit
	uint8_t	w_index;	

	if(NULL == pVal)
	{
		DEBUG("NULL!!");
		return NRF_ERROR_NULL;
	}
	
	diffX = diffY = 0;
	
#if 0
	m_util_vector_to_coordinate(&diffX, &diffY, vector.speed, vector.angle);
#else
	w_index = m_touch_db[index].w_index;
	//
	if(w_index>=2)
	{		
		// 주의: signed 16-bit
		diffX = m_touch_db[index].point[w_index-1].loc.x - m_touch_db[index].point[w_index-2].loc.x;
		diffY = m_touch_db[index].point[w_index-1].loc.y - m_touch_db[index].point[w_index-2].loc.y;

		#ifdef FLIMBS_20181210_ONE_POINT_TOUCH_ADJUST_BY_HEATMAP
		{
			static uint8_t heatmap_counter = 1;			
			m_flimbs_adjust_onepointXY_flimbs(m_touch_db[index].point[w_index - 1].loc.x, m_touch_db[index].point[w_index - 1].loc.y, &heatmap_counter, &diffX, &diffY);
		}
		#endif				
	}
	//diffY *= -1; // For Host OS 
	diffY = -diffY; // For Host OS 
	//DEBUG("XY[%d:%d]", diffX, diffY);
#endif

	pVal->buf[0] = diffX & 0x00ff;
	pVal->buf[1] = ((diffY & 0x000f) << 4) | ((diffX & 0x0f00) >> 8);
	pVal->buf[2] = (diffY & 0x0ff0) >> 4;
	// <-- 중복전송을 막기 위하여, timestamp 필요

	pVal->timestamp = m_touch_db[index].point[w_index-1].timestamp;
	
	if(m_touch_db[index].point[w_index-1].timestamp != timestamp_sent)//타임스탬프 여기서 처리 안해도 되는데 맘이 그냥 그래서 다시 옮겨옴. 옮기고 싶으면 event 에서 처리하면 됨.
	{		
		________________________________LOG_mTouch_SendPoint(8,
		"{numb:%d,pos:{x:%04d,y:%04d}}>>"
		,0
		,diffX
		,diffY);

		timestamp_sent = pVal->timestamp;

		return NRF_SUCCESS;	
	}
	else
	{
		return NRF_ERROR_INVALID_DATA;
	}
}





/*============================================================================//
//                            delta_time()
//============================================================================*/
/*----------------------------------------------------------------------------//
00000000_MJ
	생성
//----------------------------------------------------------------------------*/
static uint16_t delta_time(uint16_t prv, uint16_t now)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(m_comm_evt_arg_point_t* pVal, uint8_t index)");
	uint16_t deltaT = 0;

	if(now != prv)
	{
		if(now - prv)
		{
			deltaT = (now-prv);
		}
		else
		{			
			deltaT = (0xFFFF-prv); // This code valid when timestamp size is uint16_t
			deltaT += now;
		}
	}
	return deltaT;
}




/*============================================================================//
//                            get_vector_stanley()
//============================================================================*/
/*----------------------------------------------------------------------------//
00000000_MJ
	생성
//----------------------------------------------------------------------------*/
static void get_vector_stanley(touch_point_t prev, touch_point_t now, touch_vector_t *p_vector)
{	
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(touch_point_t prev, touch_point_t now, touch_vector_t *p_vector)");	
	int16_t deltaX, deltaY, deltaT;
	uint16_t u_deltaX, u_deltaY;
	
	p_vector->speed= 0;
	p_vector->angle= INVALID_ANGLE;
	p_vector->delta= 0;
	
	deltaX = now.loc.x - prev.loc.x;
	deltaY = now.loc.y - prev.loc.y;
	
	if((0 != deltaX) || (0 != deltaY))
	{
		deltaT = delta_time(prev.timestamp, now.timestamp);

		if(deltaT)
		{
			u_deltaX = abs(deltaX);
			u_deltaY = abs(deltaY);
			p_vector->delta = u_deltaX + u_deltaY;
			
			if((u_deltaX>MOVE_MINIMUM) || (u_deltaY>MOVE_MINIMUM))
			{				
				p_vector->speed = (p_vector->delta)*SPEED_ADJUST; 	// Make it bigger to check
				p_vector->speed /= deltaT; 							// Not real speed, simple speed
			}
		}
				
		________________________________LOG_mTouch_angleSetTrace(1,"\nif((p_vector->speed:%d >= ENOUGH_SPEED_FOR_ANGLE) && (p_vector->delta:%d < TOO_MUCH_DELTA))",p_vector->speed,p_vector->delta);
		//if((p_vector->speed >= ENOUGH_SPEED_FOR_ANGLE) && (p_vector->delta < TOO_MUCH_DELTA))
		{
			________________________________LOG_mTouch_angleSetTrace(1,"\n  p_vector->angle = m_util_atan2deg(deltaX, deltaY);");
			p_vector->angle = m_util_atan2deg(deltaX, deltaY);
						
		}					
	}	
	return;
}



/*============================================================================//
//                            get_direction()
//============================================================================*/
/*----------------------------------------------------------------------------//
00000000_MJ
	생성
//----------------------------------------------------------------------------*/
static dir_type_t get_direction(uint16_t angle)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(uint16_t angle:%d)",angle);
	dir_type_t direction = dir_none;

	if(INVALID_ANGLE == angle)
	{	
		return dir_none;
	} 
	else if(angle>360)
	{		
		angle %= 360;
	}
	// Angle이 겹쳐지지 않도록 주의
	//#define MY_MARGIN	20 // Should be less than 2

	if(BETWEEN2(angle, 45, MY_MARGIN))
	{
		direction = dir_up_right;		
	}
	else if(BETWEEN2(angle, 90, MY_MARGIN))
	{
		direction = dir_up;
	}
	else if(BETWEEN2(angle, 135, MY_MARGIN))
	{
		direction = dir_up_left;
	}
	else if(BETWEEN2(angle, 180, MY_MARGIN))
	{
		direction = dir_left;
	}
	else if(BETWEEN2(angle, 225, MY_MARGIN))
	{
		direction = dir_down_left;
	}
	else if(BETWEEN2(angle, 270, MY_MARGIN))
	{
		direction = dir_down;
	}
	else if(BETWEEN2(angle, 315, MY_MARGIN))
	{
		direction = dir_down_right;
	}
	else if((angle<MY_MARGIN) || (angle>(360-MY_MARGIN)))
	{
		direction = dir_right;	
	}
	return direction;
}


/*============================================================================//
//                            get_touch_num()
//============================================================================*/
/*----------------------------------------------------------------------------//
00000000_MJ
	생성
//----------------------------------------------------------------------------*/
static uint8_t get_touch_num(void)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(void)");
	uint8_t index, touched;
	
	for(index = 0, touched = 0; index<M_TOUCH_COUNT_MAX; index++) // < 4
	{
		// 적어도 2개 이상이면 
		//if(m_touch_db[index].w_index >= M_POINT_ENOUGH_NUM) // >= 3
		if(m_touch_db[index].w_index >= 1)
		{
			touched++;
		}
	}
	// Stanley.... let's use the touchNum in the TT-Bridge Info  <--- HERE HERE !!!

	return touched;
}


/*============================================================================//
//                            get_touch_index()
//============================================================================*/
/*----------------------------------------------------------------------------//
00000000_MJ
	생성
//----------------------------------------------------------------------------*/
static ret_code_t get_touch_index(uint8_t* pIndex, uint8_t touch_cnt)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(uint8_t* pIndex, uint8_t touch_cnt)");	
	________________________________LOG_mTouch_twoFingerTrace(0,"");
	uint8_t index, touched;

	if(touch_cnt > M_TOUCH_COUNT_MAX)
	{
		________________________________LOG_mTouch_twoFingerTrace(1,"if(touch_cnt > M_TOUCH_COUNT_MAX)");
		________________________________LOG_mTouch_twoFingerTrace(2,"return NRF_ERROR_INVALID_PARAM;");
		return NRF_ERROR_INVALID_PARAM;
	}

	if(NULL == pIndex)
	{		
		________________________________LOG_mTouch_twoFingerTrace(2,"if(NULL == pIndex)");
		________________________________LOG_mTouch_twoFingerTrace(1,"return NRF_ERROR_NULL;");
		return NRF_ERROR_NULL;
	}

	// 이부분은 좀더 복잡한 상황 처리가 추가 될수도 있음
	for(index = 0, touched = 0; index < M_TOUCH_COUNT_MAX; index++)
	{
		________________________________LOG_mTouch_twoFingerTrace(1,"if(m_touch_db[index].w_index:%d >= M_POINT_ENOUGH_NUM:%d)",m_touch_db[index].w_index,M_POINT_ENOUGH_NUM);
		if(m_touch_db[index].w_index >= M_POINT_ENOUGH_NUM) // <-- 'index'번째 터치에 대하여, '충분히 (3개이상) 데이터가 모아졌을 때'를 의미함.
		{
			touched++;
			________________________________LOG_mTouch_twoFingerTrace(2,"if(touch_cnt:%d == touched:%d)",touch_cnt,touched);
			if(touch_cnt == touched) // multi-touch중에서 마지막에 터치된 시점까지의 index 값'들'을  리턴
			{						 // 
				*pIndex = index;
				return NRF_SUCCESS;
			}
		}
	}	
	return NRF_ERROR_NOT_FOUND;
}



/*============================================================================//
//                            get_new_stanley()
//============================================================================*/
/*----------------------------------------------------------------------------//
00000000_MJ
	생성
//----------------------------------------------------------------------------*/
#if	defined(SLIDING_WINDOW_SIZE)
	static void get_new_stanley(uint16_t x, uint16_t y, touch_coordinate_t *p_loc)
	{
		________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(uint16_t x:%d, uint16_t y:%d, touch_coordinate_t *p_loc)",x,y);
		uint32_t i;
		uint32_t sum[2] = {0, };
		uint32_t non_zero[2] = {0, };
		//
		//touch_coordinate_t val = {0, 0};

		#if	defined(SLIDING_WINDOW_FILTER)
		filter(x, y);
		#endif

		// Update window queue
		i = m_move_window.write_index;
		m_move_window.queue[i].x = x;
		m_move_window.queue[i].y = y;

		// Update window index
		if( ++m_move_window.write_index >= SLIDING_WINDOW_SIZE)
		{
			m_move_window.write_index = 0;
		}
		
		INCREASE(m_move_window.is_used, SLIDING_WINDOW_SIZE);

		if(m_move_window.is_used>=SLIDING_WINDOW_SIZE)
		{
			// Get new value
			for(i=0; i<SLIDING_WINDOW_SIZE; i++)
			{
				if(m_move_window.queue[i].x)
				{
					sum[0] += m_move_window.queue[i].x;
					non_zero[0]++;
				}

				if(m_move_window.queue[i].y)
				{
					sum[1] += m_move_window.queue[i].y;
					non_zero[1]++;
				}
			}

			if(non_zero[0])
			{
				//val.x = (sum[0]/non_zero[0]);
				p_loc->x = (sum[0]/non_zero[0]);
			}

			if(non_zero[1])
			{
				//val.y = (sum[1]/non_zero[1]);
				p_loc->y = (sum[1]/non_zero[1]);
			}
		}

		//return val;
	}
#endif//SLIDING_WINDOW_SIZE


/*============================================================================//
//                            filter()
//============================================================================*/
/*----------------------------------------------------------------------------//
00000000_MJ
	생성
//----------------------------------------------------------------------------*/
#if	defined(SLIDING_WINDOW_SIZE)
	#if	defined(SLIDING_WINDOW_FILTER)
	static void filter(uint16_t x, uint16_t y)
	{
		________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(uint16_t x:%d, uint16_t y:%d)",x,y);
		uint32_t prv_index;
		int32_t delta_x = 0;
		int32_t delta_y = 0;
		touch_coordinate_t prv = {0, 0};
		
		// Get previous data
		prv_index = m_move_window.write_index ? (m_move_window.write_index - 1) : (SLIDING_WINDOW_SIZE -1);
		prv.x = m_move_window.queue[prv_index].x;
		prv.y = m_move_window.queue[prv_index].y;

		if(prv.x)
		{
			delta_x = abs(x - prv.x);
			//delta_x = abs(delta_x);
		}
		
		if(prv.y)
		{
			delta_y = abs(y - prv.y);
			//delta_y = abs(delta_y);
		}

		m_move_window.delta[m_move_window.write_index] = delta_x+delta_y;

		#if	defined(M_TOUCH_WINDOW_TRACE)
			//DEBUGS("prv[%d:%d] now[%d:%d] delta:%d", prv.x, prv.y, x, y, delta_x);
			//DEBUGS("prv[%d:%d] now[%d:%d] delta:%d", prv.x, prv.y, x, y, delta_x+delta_y);
			//DEB(" prv[%d:%d] now[%d:%d] delta:%d+%d", prv.x, prv.y, x, y, delta_x, delta_y);
		#endif

		// FILTER: Clear by delta current delta size
		for(uint32_t i=0; i<SLIDING_WINDOW_SIZE; i++)
		{
			if((m_move_window.delta[i] > 40))
			{
				// Clear oldeast data
				if(i>1) 
				{
					m_move_window.queue[prv_index].x = 0;
					m_move_window.queue[prv_index].y = 0;
				}
			}

			// Get a new previous index
			if(prv_index)
			{
				prv_index--;
			}
			else
			{
				prv_index = (SLIDING_WINDOW_SIZE -1);
			}
		}
	}
	#endif //SLIDING_WINDOW_FILTER
#endif //SLIDING_WINDOW_SIZE



/*============================================================================//
//                            send_release_keys()
//============================================================================*/
/*----------------------------------------------------------------------------//
00000000_MJ
	생성
//----------------------------------------------------------------------------*/
static void send_release_keys(void)
{				
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(void)");
	m_comm_evt_arg_t val;
	#ifdef M_TOUCH_STATE_TRACE
		DEB("..send_release_keys(): ");
	#endif
	memset(&val, 0, sizeof(val));// clear local variable	
	val.touch.three.back = 1;// release command
	m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val); // (m_event, m_arg)
}



/*============================================================================//
//                            send_release_click()
//============================================================================*/
/*----------------------------------------------------------------------------//
00000000_MJ
	생성
//----------------------------------------------------------------------------*/
static void send_release_click(void)
{				
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(void)");
	m_comm_evt_arg_t val;
	DEB("send_release_click: ");		
	memset(&val, 0, sizeof(val));// clear local variable ==> No click button
	m_comm_send_event(M_COMM_EVENT_CLICK_CTRL, val);
}


/*============================================================================//
//                            try_remove_ghost_on_OffTouch()
//============================================================================*/
/*----------------------------------------------------------------------------//
20190525_flimbs
	스탠리가 넣어놓은거같은데 안쓰고 있음. 삭제해도 되는지 확인해야댐
//----------------------------------------------------------------------------*/
void try_remove_ghost_on_OffTouch(void)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(void)");
	if(m_state.gestureState == gstr_state_none)
	{
		#ifdef BASELINEINIT_USE
		{
			________________________________LOG_mTouch_baseLineInitTrace(1,"baselineInit");
			drv_cypress_init_Baseline();  
		}
		#endif
	}
}



/*============================================================================//
//                            scan_data_dummy()
//============================================================================*/
/*----------------------------------------------------------------------------//
20190525_flimbs
	#ifdef SKIP_SCANNING_IF_NOT_TOUCHMODE_OR_TT_INT_HIGH 라는 스위치가 달려있었는데
	용도를 알수없어 삭제함
//----------------------------------------------------------------------------*/
ret_code_t scan_data_dummy(void)
{	
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(void)");
	ret_code_t rv;
	drv_cypress_info_t touchInfo;
	rv = drv_cypress_read(&touchInfo);
	

	//if(NRF_SUCCESS == rv) 
	return rv;
}



/*============================================================================//
//                            copy_touch_point()
//============================================================================*/
/*----------------------------------------------------------------------------//
00000000_MJ
	생성
//----------------------------------------------------------------------------*/
void copy_touch_point(touch_point_t *dest, touch_point_t *src)
{________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(touch_point_t *dest, touch_point_t *src)");
	dest->timestamp = src->timestamp;
	*(uint8_t *)(&dest->attr) = *(uint8_t *)(&src->attr);
	dest->loc.x = src->loc.x;
	dest->loc.y = src->loc.y;
}


/*============================================================================//
//                            m_MouseMove()
//============================================================================*/
/*----------------------------------------------------------------------------//
20190525_flimbs
	삭제할 예정. 인증시에 사용하는 코드.
00000000_stanley
	바이트 세개 쓴다고 뭐 그런얘길 했었는데 스펙 찾아봐야댐.
	하단은 스탠리의 메모.
	------+-------+-------+-------+-------+-------+-------+-------+-------+
		  | Bit 7	Bit 6	Bit 5	Bit 4	Bit 3	Bit 2	Bit 1	Bit 0 
	------+-------+-------+-------+-------+-------+-------+-------+-------+
	Byte 1|													X7:X0
	------+-------+-------+-------+-------+-------+-------+-------+-------+
	Byte 2|					Y3:Y0              |					X11:X8
	------+-------+-------+-------+-------+-------+-------+-------+-------+
	Byte 2|													Y11:Y4
	------+-------+-------+-------+-------+-------+-------+-------+-------+
//----------------------------------------------------------------------------*/
void m_MouseMove(int16_t signed_dx, int16_t signed_dy)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(int16_t signed_dx:%d, int16_t signed_dy:%d)",signed_dx, signed_dy);
	m_comm_evt_arg_t val;
	ret_code_t rv;
	static uint16_t timestamp=0;

	// clear
	memset(&val, 0, sizeof(val));

	// prepare mouse points
	val.point.buf[0]  =  (signed_dx & 0x00ff);
	val.point.buf[1]  = ((signed_dx & 0x0f00) >> 8);
	val.point.buf[1] |= ((signed_dy & 0x000f) << 4); 
	val.point.buf[2]  = ((signed_dy & 0xfff0) >> 4);
	
	// 시간이 매번 달라야만 제대로 전송된다. 시간이 동일한 포인트들은 중복전송으로 이해되어 전송이 제한됨에 주의한다.
	val.point.timestamp= timestamp++;

	// move the mouse point
	rv = m_comm_send_event(M_COMM_EVENT_POINT_UPDATE, val);


	rv=rv;
	//
	return;
}


/*============================================================================//
//                            m_KeyswitchDown()
//============================================================================*/
/*----------------------------------------------------------------------------//
20190525_flimbs
	삭제할 예정. 인증시에 사용하는 코드.
00000000_stanley	
	스탠리의 메모
	------+-------+-------+-------+-------+-------+-------+-------+-------+
		  | Bit 7	Bit 6	Bit 5	Bit 4	Bit 3	Bit 2	Bit 1	Bit 0 
	------+-------+-------+-------+-------+-------+-------+-------+-------+
	Byte 0| R-GUI	R-ALT	R-SHIFT	R-CTRL	L-GUI	L-ALT	L-SHIFT	L-CTRL  
	------+-------+-------+-------+-------+-------+-------+-------+-------+
	Byte 1|													
	------+-------+-------+-------+-------+-------+-------+-------+-------+
	Byte 2|
	------+-------+-------+-------+-------+-------+-------+-------+-------+
	Byte 3|
	------+-------+-------+-------+-------+-------+-------+-------+-------+
	Byte 4|
	------+-------+-------+-------+-------+-------+-------+-------+-------+
	Byte 5|
	------+-------+-------+-------+-------+-------+-------+-------+-------+
	Byte 6|
	------+-------+-------+-------+-------+-------+-------+-------+-------+
	Byte 7|
	------+-------+-------+-------+-------+-------+-------+-------+-------+
//----------------------------------------------------------------------------*/
void m_KeyswitchDown(int8_t usb_defined_keycode, int8_t CtrlAltShift)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(int8_t usb_defined_keycode:%d, int8_t CtrlAltShift:%d)",usb_defined_keycode,CtrlAltShift);
	m_comm_evt_arg_t val;
	ret_code_t rv;
	
	memset(&val, 0, sizeof(val));
	val.keys.buf[0]= CtrlAltShift;				// <---GUI+Ctrl+ALT+Shift
	val.keys.buf[2]= usb_defined_keycode;	// <--- single usb-defined character code
	rv = m_comm_send_event(M_COMM_EVENT_KEYBOARD_UPDATE, val);

	rv=rv;
	return;
}



/*============================================================================//
//                            m_KeyswitchUp()
//============================================================================*/
/*----------------------------------------------------------------------------//
00000000_stanley
	인증용.
//----------------------------------------------------------------------------*/
void m_KeyswitchUp(void)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(void)");
	m_comm_evt_arg_t val;
	ret_code_t rv;
	//
	memset(&val, 0, sizeof(val));
	rv = m_comm_send_event(M_COMM_EVENT_KEYBOARD_UPDATE, val);
	//
	rv=rv;
	return;
}


static void inertia_scroll(void)
{
	ret_code_t rv;
	if(m_state.gestureState == gstr_state_none)
	{
		if(m_inertia_repeat > 0 && m_inertia_trvldist > 0)
		{			
			if(!m_timer_is_expired(M_TiMER_ID_INERTIA_BLOCK))
			{
				m_timer_set(M_TiMER_ID_INERTIA_BLOCK, 400);
				
				if(m_timer_is_expired(M_TIMER_ID_INERTIA_SCROLL))
				{
					m_inertia_trvldist= m_inertia_trvldist <= 1 ? 1 : m_inertia_trvldist-1;
					m_inertia_repeat--;
					m_comm_evt_arg_t inertia_val;
					memset(&inertia_val, 0, sizeof(inertia_val));
					
					inertia_val.touch.two.scroll_trvldist = m_inertia_trvldist;

					if(m_inertia_scroll_updown == 1)
					{					
						inertia_val.touch.two.scroll_up = 1; 					
					}
					else if(m_inertia_scroll_updown == -1)
					{
						inertia_val.touch.two.scroll_down = 1; 
					}

					rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, inertia_val);
					nrf_delay_us(BACK_DELAY);

					memset(&inertia_val, 0, sizeof(inertia_val));
					inertia_val.touch.two.back = 1;
					rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, inertia_val);
					
					overspeed_cnt=0;
					if(m_comm_current_central_type() == M_COMM_CENTRAL_MAC)
					{											
	/*
						nrf_delay_ms(40/m_inertia_trvldist+20);
						rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, inertia_val);
						nrf_delay_us(BACK_DELAY);


						memset(&inertia_val, 0, sizeof(inertia_val));
						inertia_val.touch.two.back = 1;
						rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, inertia_val);

						m_inertia_interval = m_inertia_interval < 80 ? m_inertia_interval+8 : m_inertia_interval;
						m_timer_set(M_TIMER_ID_INERTIA_SCROLL, m_inertia_interval);						
						*/
					//24~80

						m_inertia_interval = m_inertia_interval < 100 ? m_inertia_interval+8 : 100;
						m_timer_set(M_TIMER_ID_INERTIA_SCROLL, m_inertia_interval);

						//m_timer_set(M_TIMER_ID_INERTIA_SCROLL, m_inertia_interval);										
					}
					else
					{//8
						//m_inertia_interval = m_inertia_interval < 80 ? m_inertia_interval+8 : m_inertia_interval;
						//m_timer_set(M_TIMER_ID_INERTIA_SCROLL, m_inertia_interval);

						//m_timer_set(M_TIMER_ID_INERTIA_SCROLL, 100);				

						//m_inertia_interval = m_inertia_interval < 120 ? m_inertia_interval+4 : 120;
						
						//m_timer_set(M_TIMER_ID_INERTIA_SCROLL, m_inertia_interval);
						m_timer_set(M_TIMER_ID_INERTIA_SCROLL, m_inertia_interval - m_inertia_repeat/120);
					}
				}			
			}
		}		
	}
	else if(m_state.gestureState != gstr_two_scroll)
	{
		m_inertia_trvldist=0;
	}
}




















/*$$$$$\   $$$$$$\  $$$$$$\ $$\   $$\ $$$$$$$$\ $$$$$$$$\ $$$$$$$\                     
$$  __$$\ $$  __$$\ \_$$  _|$$$\  $$ |\__$$  __|$$  _____|$$  __$$\                    
$$ |  $$ |$$ /  $$ |  $$ |  $$$$\ $$ |   $$ |   $$ |      $$ |  $$ |                   
$$$$$$$  |$$ |  $$ |  $$ |  $$ $$\$$ |   $$ |   $$$$$\    $$$$$$$  |                   
$$  ____/ $$ |  $$ |  $$ |  $$ \$$$$ |   $$ |   $$  __|   $$  __$$<                    
$$ |      $$ |  $$ |  $$ |  $$ |\$$$ |   $$ |   $$ |      $$ |  $$ |                   
$$ |       $$$$$$  |$$$$$$\ $$ | \$$ |   $$ |   $$$$$$$$\ $$ |  $$ |                   
\__|       \______/ \______|\__|  \__|   \__|   \________|\__|  \__|                   
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\  $$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |$$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |$$ /  \__|
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |\$$$$$$\  
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ | \____$$\ 
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |$$\   $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |\$$$$$$  |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__| \______/ 
//============================================================================//
//                       POINTER FUCTIONS				                      //
//============================================================================*/



/*============================================================================//
//                            idle_handler()
//============================================================================*/
static void idle_handler(void)
{		
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(void)");
	update_tab();
}



/*============================================================================//
//                            check_tab()
//============================================================================*/
static void check_tab(void)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(void)");	
	//________________________________LOG_mTouch_checkTab(1,"check_tab(void)");
    if( !m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_TAB) && m_state.jobState_old == 0 && m_state.jobState != 0)
	{//타이머 도는중				
		uint16_t max = 0; // 벡터중 가장 큰값.. 
		uint16_t ThresholdArr[M_TOUCH_COUNT_MAX+1] = {100,8,1500,500,900};

		for(uint8_t i = 0; i<M_TOUCH_COUNT_MAX; i++)
		{
			//if(m_touch_db[i].w_index < M_POINT_ENOUGH_NUM)
			if(max< m_touch_db[i].w_index <= 1)
			{
				//m_timer_set(M_TIMER_ID_MULTI_TOUCH_TAB, 0);
				//return;
			}

			if(max < m_touch_db[i].vector.speed) 
			{
				//max = m_touch_db[i].vector.speed;
			}
		}
		
		________________________________LOG_mTouch_checkTab(1,"if(max:%d > ThresholdArr[m_state.jobState]:%d)",max, ThresholdArr[m_state.jobState]);
		if(max > ThresholdArr[m_state.jobState])////MOVING_SPEED_THRESHOLD)//if(max > 200)
		{
			________________________________LOG_mTouch_checkTab(2,"m_timer_set(M_TIMER_ID_MULTI_TOUCH_TAB, 0);");
			m_timer_set(M_TIMER_ID_MULTI_TOUCH_TAB, 0); // 
		}
		else
		{	
			________________________________LOG_mTouch_checkTab(2,"{false:M_TIMER_ID_MULTI_TOUCH_TAB:%d ms remains}", m_timer_get(M_TIMER_ID_MULTI_TOUCH_TAB));
		}
				

	}
}


/*============================================================================//
//                            update_tab()
//============================================================================*/
/*----------------------------------------------------------------------------//
	20190301_flimbs
		탭 로직 새로짬
		TIMER	|	JOB		|	LMB		|	SEND	|		
		OFF 	|	10		|	0		|	NA		|
		OFF 	|	00		|	0		|	NA		|
		OFF 	|	00		|	1		|	UP		|
		OFF 	|	00		|	1		|	UP		|
		ON	 	|	10		|	0		|	DN		|
		ON	 	|	10		|	1		|	UPDNUP	|
		각 손가락이 마지막으로 뭘 보냈는지 저장함.
		m_state 에 JobState_old 를 추가하여 jobstate 의 라이징엣지 판별하도록
	00000000_MJ
		생성
//----------------------------------------------------------------------------*/
static void update_tab(void)//손가락이 떨어졌을때 호출
{
	//m_state.jobState 가 job_state_idle 인 상황
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(void)");
	//________________________________LOG_mTouch_checkTab(1,"update_tab(void)");
	ret_code_t rv;
	m_comm_evt_arg_t val;	
	static bool isLmbSended = false;
	static bool isTwoSended = false;
	static bool isThreeSended = false;
	static bool isFourSended = false;	



	//포인트가 이동했다면 state를 idle 로 변경후 취소한다. 사실 이건 여기서 체크할게 아닌데..	
	if(abs(m_touch_db[0].point[0].loc.x - m_touch_db[0].point[1].loc.x) + abs(m_touch_db[0].point[1].loc.x - m_touch_db[0].point[2].loc.x) > 100)
	{
		________________________________LOG_mTouch_updateTab(3,"if(abs(m_touch_db[0].point[0].loc.x - m_touch_db[0].point[1].loc.x) + abs(m_touch_db[0].point[1].loc.x - m_touch_db[0].point[2].loc.x) > 100)");
		________________________________LOG_mTouch_updateTab(3,"{true: return;}");
		m_state.jobState_old = job_state_idle; // 라이징엣지를 읽었음을 나타냄. jobState_old 보다 더 좋은 이름이 있을거같음		
		return;
	}
	if(m_timer_is_expired(M_TIMER_ID_KEYBOARD_INPUT) == false)
	{
		return;
	}

	if(m_timer_is_expired(M_TIMER_ID_KEYBOARD_INPUT) == false)
	{
		return;
	}
	
	if(m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_TAB) == true)
	{//타이머 끝남
	
		//________________________________LOG_mTouch_updateTab(4,"if(m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_TAB) == true)");

		if(isLmbSended == true)
		{ //마우스 보낸적 있으면 UP	
			isLmbSended = false;			
			
			________________________________LOG_mTouch_updateTab(5,"[%d|0|%d], jobstate:%d=>%d, gstr:%d=>%d level:%d",
			isLmbSended,
			isTwoSended,
			m_state.jobState_old,
			m_state.jobState,
			m_state.gestureState_old,
			m_state.gestureState,
			m_state.gestureLevel);

			memset(&val, 0, sizeof(val));
			rv = m_comm_send_event(M_COMM_EVENT_CLICK_CTRL, val);
		}
		if(isTwoSended == true)
		{//오른클릭 마우스 보낸적 있으면 UP
			isTwoSended = false;
			
			________________________________LOG_mTouch_updateTab(5,"[%d|0|%d], jobstate:%d=>%d, gstr:%d=>%d level:%d",			
			isLmbSended,
			isTwoSended,
			m_state.jobState_old,
			m_state.jobState,
			m_state.gestureState_old,
			m_state.gestureState,
			m_state.gestureLevel);

			memset(&val, 0, sizeof(val));
			rv = m_comm_send_event(M_COMM_EVENT_CLICK_CTRL, val);

		}
		if(isThreeSended == true)
		{
			isThreeSended = false;					
			memset(&val, 0, sizeof(val));	
			val.touch.three.back = 1;
			rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);
		}
		if(isFourSended == true)
		{
			isFourSended = false;				
			memset(&val, 0, sizeof(val));		
			val.touch.four.back = 1; 
			rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);					
		}		
	}
	else
	{//타이머 식지않음	
		//________________________________LOG_mTouch_updateTab(4,"if(m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_TAB) == false)");

		if(m_timer_is_expired(M_TIMER_ID_UPDATE_MOVED)==true)
		{
			if(m_state.jobState_old == job_state_one)
			{//JobState:1 -> 0
				if(isLmbSended == false)
				{//마우스 보낸적 없으면 DN	
					isLmbSended = true;
					________________________________LOG_mTouch_updateTab(5,"[%d|0|%d], jobstate:%d=>%d, gstr:%d=>%d level:%d",				
					isLmbSended,
					isTwoSended,
					m_state.jobState_old,
					m_state.jobState,
					m_state.gestureState_old,
					m_state.gestureState,
					m_state.gestureLevel);

					memset(&val, 0, sizeof(val));
					val.click.left = 1;
					rv = m_comm_send_event(M_COMM_EVENT_CLICK_CTRL, val);
				}
				else 
				{//마우스 보낸적 있으면 UPDNUP			
					isLmbSended = false;		

					________________________________LOG_mTouch_updateTab(5,"[%d|0|%d], jobstate:%d=>%d, gstr:%d=>%d level:%d",										
					isLmbSended,
					isTwoSended,
					m_state.jobState_old,
					m_state.jobState,
					m_state.gestureState_old,
					m_state.gestureState,
					m_state.gestureLevel);

					memset(&val, 0, sizeof(val));
					rv = m_comm_send_event(M_COMM_EVENT_CLICK_CTRL, val);				
					val.click.left = 1;// 1) send click				
					rv = m_comm_send_event(M_COMM_EVENT_CLICK_CTRL, val);
					memset(&val, 0, sizeof(val));
					rv = m_comm_send_event(M_COMM_EVENT_CLICK_CTRL, val);
				}
			}		
			else if(m_state.jobState_old == job_state_two)		//else if(m_state.jobState_old == job_state_two && m_state.jobState_oldold == job_state_idle)
			{//JobState: 2 -> 0
				if(isTwoSended == false)
				{//마우스 보낸적 없으면 오른클릭
					isTwoSended = true;		
					________________________________LOG_mTouch_updateTab(4,"[%d|0|%d], jobstate:%d=>%d, gstr:%d=>%d level:%d",
					isLmbSended,
					isTwoSended,
					m_state.jobState_old,
					m_state.jobState,
					m_state.gestureState_old,
					m_state.gestureState,
					m_state.gestureLevel);

					memset(&val, 0, sizeof(val));					
					val.click.right = 1;// 1) send click					
					rv = m_comm_send_event(M_COMM_EVENT_CLICK_CTRL, val);				
				}				
				
			}		
			else if(m_state.jobState_old == job_state_three)
			{//JobState: 3 -> 0
				if(isThreeSended == false)
				{			
					isThreeSended = true;
					memset(&val, 0, sizeof(val));
					val.touch.three.tab = 1;									
					rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);
				}
			}
			else if(m_state.jobState_old == job_state_four)
			{//JobState: 4 -> 0
				if(isFourSended == false)
				{				
					isFourSended = true;
					memset(&val, 0, sizeof(val));
					val.touch.four.tab = 1;									
					rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);								
				}
			}
		}
	}
	m_state.jobState_old = job_state_idle; // 라이징엣지를 읽었음을 나타냄. jobState_old 보다 더 좋은 이름이 있을거같음	
}





/*============================================================================//
//                            one_finger_handler()
//============================================================================*/
/*----------------------------------------------------------------------------//
//----------------------------------------------------------------------------*/
static void one_finger_handler(void)
{			
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(void)");
	________________________________LOG_mTouch_oneTrace(FUNCHEAD_FLAG,"(void)");
	uint8_t index;

	ret_code_t rv = get_touch_index(&index, 1); // touch-1: index[0]
	________________________________LOG_mTouch_oneTrace(0,"	ret_code_t rv:%d = get_touch_index(&index:%d, 1);", rv, index);

	if(NRF_SUCCESS != rv)
	{			
		________________________________LOG_mTouch_oneTrace(1,"return;");
		return;
	}
	________________________________LOG_mTouch_oneTrace(1,"switch(m_state.gestureState:%d)",m_state.gestureState);	
	switch(m_state.gestureState)
	{	
		case gstr_state_none :
			update_move(index);		// m_touch_db[index]
			break;
		
		case gstr_one_move :
			update_move(index);		// m_touch_db[index]
			break;
		
		
		case gstr_one_drag :
			update_drag(index); // 더이상 사용하지 않음
			break;

		case gstr_one_volume_ctrl :
			update_volume_ctrl(index);
			break;
		
		case gstr_one_zoom_ctrl :
			update_zoom_ctrl(index);
			break;
	
		case gstr_one_stroll :
			update_scroll_one(index);
			break;
		
		default :
			break;
	}	
}

/*============================================================================//
//                            check_move()
//============================================================================*/
/*----------------------------------------------------------------------------//
20190524_flimbs
	포인터가 천천히 갈때 움직임 무시하는 문제때문에 
	speed 체크 MOVE_SPEED_THRESHOLD 가 0일때도 통과하도록 했는데 좋은 방법이 아님.
	찌글거리는 현상이랑 천천히갈때 구별해서 찌글거리는건 걸러내고 천천히가는건 
	통과시키는 방법 고안해야함

00000000_MJ
	호출순서. 
	m_touch_scan_handler -> change -> m_touch_handler[1] -> 
	one_finger_handler -> update_move -> check_move -> change_gesture(1) -> m_comm_send_event()
//----------------------------------------------------------------------------*/
static ret_code_t check_move(uint8_t index)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(uint8_t index:%d",index);

	________________________________LOG_mTouch_oneTrace(FUNCHEAD_FLAG,"(uint8_t index:%d)",index);

	________________________________LOG_mTouch_touchDbTrace2(0,0);
	uint16_t threshold;
	static uint8_t touchlock_automode_threshold = 0;
	________________________________LOG_mTouch_oneTrace(0,"if(index:%d>=M_TOUCH_COUNT_MAX:%d)",index, M_TOUCH_COUNT_MAX);
	if(index>=M_TOUCH_COUNT_MAX)
	{
		DEB("#Invalid Touch Index:%d\n", index);
		________________________________LOG_mTouch_oneTrace(1,"return NRF_ERROR_INVALID_PARAM;");
		return NRF_ERROR_INVALID_PARAM;
	}
	else
	{
		if(m_touch_db[index].w_index >= M_POINT_ENOUGH_NUM)
		{
			//#define CHECK_MOVE_LEVEL_THRSHOLD		2U
			//#define FIRST_MOVE_DELTA_THRESHOLD 	2U
			//#define MOVE_DELTA_THRESHOLD 			1U	
			//#define MOVE_SPEED_THRESHOLD 			1U
			//#define MOVE_SPEED_THRESHOLD 1	// Optimized value, be careful when changing this value
			int check_move_level_threshold = 11; // 제스처레벨
			int first_move_delta_threshold = 1;
			int move_delta_threshold = 3;
			int move_too_big_delta = 300;

//			threshold = (stop_count < check_move_level_threshold) ? 
			threshold = (m_state.gestureLevel > check_move_level_threshold) ? first_move_delta_threshold : move_delta_threshold;
			________________________________LOG_mTouch_oneTrace(1, "threshold:%d = (m_state.gestureLevel:%d < CHECK_MOVE_LEVEL_THRSHOLD:%d) ? FIRST_MOVE_DELTA_THRESHOLD:%d : MOVE_DELTA_THRESHOLD:%d;", threshold,m_state.gestureLevel, CHECK_MOVE_LEVEL_THRSHOLD, FIRST_MOVE_DELTA_THRESHOLD, MOVE_DELTA_THRESHOLD);


			//________________________________LOG_mTouch_oneTrace(1, "if(m_touch_db[index].vector.speed:%d > MOVE_SPEED_THRESHOLD:%d)", m_touch_db[index].vector.speed, MOVE_SPEED_THRESHOLD);
			if (1) //(m_touch_db[index].vector.speed > MOVE_SPEED_THRESHOLD)
			{
				//________________________________LOG_mTouch_oneTrace(2, "if((threshold:%d =< m_touch_db[index].vector.delta:%d) && (m_touch_db[index].vector.delta:%d < MOVE_TOO_BIG_DELTA:%d)))", threshold, m_touch_db[index].vector.delta, m_touch_db[index].vector.delta, threshold);
				if ((threshold <= m_touch_db[index].vector.delta) && (m_touch_db[index].vector.delta < move_too_big_delta))
				{
					//________________________________LOG_mTouch_oneTrace(3, "return NRF_SUCCESS;");
					if(m_state.gestureLevel<30)
					{
						m_state.gestureLevel++;
					}
					else
					{
						m_state.gestureLevel=30;
												
					}						

					//movedriven_flag_threshold = movedriven_flag_threshold >= 60 ? 60 : movedriven_flag_threshold++;s

					if(touchlock_automode_threshold++ >= 80)
					{
						m_touchlock_automode = true;
						touchlock_automode_threshold = 0;
					}
				return NRF_SUCCESS;
			}
			else
			{
					if(m_state.gestureLevel>10)
					{m_state.gestureLevel--;}					
					//________________________________LOG_mTouch_oneTrace(3, "return NRF_ERROR_INVALID_DATA;");
					return NRF_ERROR_INVALID_DATA;
				}
			}
			else
			{
				//________________________________LOG_mTouch_oneTrace(3, "return NRF_ERROR_INVALID_DATA;");
				return NRF_ERROR_INVALID_DATA;
			}
		}
	}

	//________________________________LOG_mTouch_oneTrace(0, "return NRF_ERROR_INVALID_DATA;");
	return NRF_ERROR_INVALID_DATA;
}


//===========================================================================
//	name    : update_move()	
//===========================================================================
static void update_move(uint8_t index)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(uint8_t index:%d",index);
	________________________________LOG_mTouch_oneTrace(FUNCHEAD_FLAG,"(uint8_t index:%d",index);
	ret_code_t rv;
	m_comm_evt_arg_t val;
	
	/*
	//클릭 알고리즘 바뀌어서 쓸일이 없어짐
	if(!m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_TAB_ALIVE))
	{		
		change_gesture(gstr_one_drag);		
		m_timer_set(M_TIMER_ID_MULTI_TOUCH_TAB_ALIVE, DRAG_ALIVE_TIME);
	}*/
		
	#if 0
	if(drv_keyboard_is_pressed(S2L(V)))
	{
		change_gesture(gstr_one_volume_ctrl);
	}
	else if(drv_keyboard_is_pressed(S2L(Z)))
	{
		change_gesture(gstr_one_zoom_ctrl);
	}
	else if(drv_keyboard_is_pressed(S2L(S)))
	{
		change_gesture(gstr_one_stroll);
	}
	else if (NRF_SUCCESS == check_move(index)) 
	{
		________________________________LOG_mTouch_oneTrace(1,"change_gesture(gstr_one_move);");
		change_gesture(gstr_one_move);	
		memset(&val, 0, sizeof(val));
		________________________________LOG_mTouch_oneTrace(1,"if(NRF_SUCCESS == get_point(&val.point, index))");
		if(NRF_SUCCESS == get_point(&val.point, index))
		{
			// 시간이 매번 달라야만 제대로 전송된다. 시간이 동일한 포인트들은 중복전송으로 이해되어 전송이 제한됨에 주의한다.
			rv = m_comm_send_event(M_COMM_EVENT_POINT_UPDATE, val);	PRINT_IF_ERROR("m_comm_send_event()", rv);
			//APP_ERROR_CHECK(rv);
			________________________________LOG_mTouch_oneTrace(2,"change_gesture(gstr_one_move);");
		}
		else
		{
			//APP_ERROR_CHECK(NRF_ERROR_INTERNAL);
		}
		INCREASE(m_state.gestureLevel, 0xFF);
	}
	#else
	if (NRF_SUCCESS == check_move(index)) 
	{
		m_timer_set(M_TIMER_ID_UPDATE_MOVED,UPDATE_MOVED);
		________________________________LOG_mTouch_oneTrace(1,"change_gesture(gstr_one_move);");
		change_gesture(gstr_one_move);	
		memset(&val, 0, sizeof(val));
		________________________________LOG_mTouch_oneTrace(1,"if(NRF_SUCCESS == get_point(&val.point, index))");
		if(NRF_SUCCESS == get_point(&val.point, index))
		{
			// 시간이 매번 달라야만 제대로 전송된다. 시간이 동일한 포인트들은 중복전송으로 이해되어 전송이 제한됨에 주의한다.
			rv = m_comm_send_event(M_COMM_EVENT_POINT_UPDATE, val);	PRINT_IF_ERROR("m_comm_send_event()", rv);
			//APP_ERROR_CHECK(rv);
			________________________________LOG_mTouch_oneTrace(2,"change_gesture(gstr_one_move);");
		}
		else
		{
			//APP_ERROR_CHECK(NRF_ERROR_INTERNAL);
		}
		INCREASE(m_state.gestureLevel, 0xFF);
	}
	#endif

	//DEB("update_move: END\n");
}





















//================================================================================
//	name    : two_finger_handler
//================================================================================
static void two_finger_handler(void)
{			
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(void)");
	
	ret_code_t rv;
	uint8_t i, index[2];

	for(i=0; i<2; i++)
	{
		rv = get_touch_index(&index[i], i+1); // touch-1: index[0], touch-2: index[1]
		if(NRF_SUCCESS != rv)
		{			
			return;
		}
	}	

	if(abs((m_touch_db[index[0]].point[0].loc.x - m_touch_db[index[1]].point[0].loc.x) + (m_touch_db[index[0]].point[0].loc.y - m_touch_db[index[1]].point[0].loc.y)) > 1100)
	{
		return;
	}

	switch(m_state.gestureState)
	{				
		case gstr_state_none :
			update_scroll_two(index[0], index[1]);
			update_zoom(index[0], index[1]);
			break;
		
		case gstr_two_scroll :									
			update_scroll_two(index[0], index[1]);
			break;

		case gstr_two_zoom :						
			update_zoom(index[0], index[1]);
			break;

		default :
			DEBUG("OOPS[%d]!!", m_state.gestureState);
			break;
	}
}



//===========================================================================
//	name    : check_scroll
//===========================================================================
static ret_code_t check_scroll(uint8_t index)
{	
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(uint8_t index:%d)",index);
	________________________________LOG_mTouch_twoFingerTrace(0,"");
	//uint32_t delta =( m_touch_db[0].point[0].loc.y) + (m_touch_db[0].point[0].loc.x/8);// + m_touch_db[index].point[0].loc.y;
	static uint16_t delta_old[M_TOUCH_COUNT_MAX]= {0,};
	uint16_t delta[M_TOUCH_COUNT_MAX];
	
	if( (index >= M_TOUCH_COUNT_MAX))
	{
		return NRF_ERROR_INVALID_PARAM;
	}
	else
	{				
		if((m_touch_db[index].w_index >= M_POINT_ENOUGH_NUM))
		{			
			delta[index] = (m_touch_db[index].point[0].loc.y) + (m_touch_db[index].point[0].loc.x/2);// + m_touch_db[index].point[0].loc.y;
			
			________________________________LOG_mTouch_twoFingerTrace(5,"24 < (abs(delta[index] - delta_old[index]):%d",abs(delta[index] - delta_old[index]));

			//속도가 느리면 olddelta를 빠르게 저장하기때문에 속도에따라서 olddelta 의 상한값을 정하면 될듯?	
			
			if(m_comm_current_central_type() == M_COMM_CENTRAL_MAC)
			{
				if(4 < abs(delta[index] - delta_old[index]))//if(14 < abs(delta[index] - delta_old[index]))
				{
					delta_old[index] = delta[index];
					return NRF_SUCCESS;
				}
			}
			else
			{
				if(32 < abs(delta[index] - delta_old[index]))//if(14 < abs(delta[index] - delta_old[index]))
				{
					delta_old[index] = delta[index];
					return NRF_SUCCESS;
				}
			}
		}
	}
	return NRF_ERROR_INVALID_PARAM;
	
}


//================================================================================
//	Function name : check_scroll_two()
//================================================================================

static ret_code_t check_scroll_two(uint8_t first, uint8_t second)
{	

	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(uint8_t first:%d, uint8_t second:%d)",first,second);


	static uint16_t delta_old[M_TOUCH_COUNT_MAX]= {0,};
	uint16_t delta[M_TOUCH_COUNT_MAX];
	uint8_t index = first;
	if( (index >= M_TOUCH_COUNT_MAX))
	{
		//________________________________LOG_mTouch_threeScroll(1,"return NRF_ERROR_INVALID_PARAM;");
		return NRF_ERROR_INVALID_PARAM;
	}
	else
	{				
		if((m_touch_db[index].w_index >= M_POINT_ENOUGH_NUM))
		{			
			delta[index] = (m_touch_db[index].point[0].loc.y) + (m_touch_db[index].point[0].loc.x/2);// + m_touch_db[index].point[0].loc.y;
			
			//________________________________LOG_mTouch_threeScroll(1,"24 < (abs(delta[index] - delta_old[index]):%d",abs(delta[index] - delta_old[index]));

			//속도가 느리면 olddelta를 빠르게 저장하기때문에 속도에따라서 olddelta 의 상한값을 정하면 될듯?	
			
			if(36 < abs(delta[index] - delta_old[index]))
			{
				delta_old[index] = delta[index];
				//________________________________LOG_mTouch_threeScroll(2,"return NRF_SUCCESS");
				return NRF_SUCCESS;
				//________________________________LOG_mTouch_threeScroll(8,"im not return");
			}
		//________________________________LOG_mTouch_threeScroll(8,"im not return");
		}
	//________________________________LOG_mTouch_threeScroll(8,"im not return");
	}
	//________________________________LOG_mTouch_threeScroll(1,"return NRF_ERROR_INVALID_PARAM;");
	return NRF_ERROR_INVALID_PARAM;
	/*
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(uint8_t first:%d, uint8_t second:%d)",first,second);
	ret_code_t rv[2];

	rv[0] = check_scroll(first);
	rv[1] = check_scroll(second);
	// First Detection condition
	if(0 == m_state.gestureLevel)
	{
		if((NRF_SUCCESS == rv[0]) && (NRF_SUCCESS == rv[1]))
		{						
			return NRF_SUCCESS;
		}
	}
	else // On condition
	{
		if((NRF_SUCCESS == rv[0]) || (NRF_SUCCESS == rv[1]))
		{			
			return NRF_SUCCESS;
		}
	}

	return NRF_ERROR_INVALID_DATA;
	*/
}
//===========================================================================
//	name    : update_scroll_two
//===========================================================================
static void update_scroll_two(uint8_t first, uint8_t second)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(uint8_t first:%d, uint8_t second:%d)",first,second);
	________________________________LOG_mTouch_twoFingerTrace(0,"");
	uint16_t speed, repeat;
	uint32_t delay;
	ret_code_t rv = NRF_SUCCESS;
	dir_type_t dir[2];
	m_comm_evt_arg_t val;
	m_comm_evt_arg_t val_reset;
	m_first = first;
	m_second = second;
	rv = check_scroll(first);
	static uint32_t oldtime=0;
	
	/*
	//if(m_inertia_repeat, m_inertia_trvldist,m_touch_db[first].vector.speed + m_touch_db[second].vector.speed> 20)
	//속도 너무 빠르면 취소하기. 여기있어야할게 아님.
	if(m_touch_db[first].vector.speed + m_touch_db[second].vector.speed > 60)
	{
		//m_timer_set(M_TIMER_ID_MULTI_TOUCH_TAB,0);		
	}
	*/
	if (NRF_SUCCESS == rv)//check_scroll_two(first,second))
	{
		
		________________________________LOG_mTouch_twoFingerTrace(1,"if (NRF_SUCCESS:%d == check_scroll(first):%d)",0,0);
		memset(&val, 0, sizeof(val));

		dir[0] = get_direction(m_touch_db[first].vector.angle);
		dir[1] = get_direction(m_touch_db[second].vector.angle);

		if(
			((dir_up         == dir[0]) && ((dir_up == dir[1])  ||  (dir_up_right == dir[1])  ||  (dir_right == dir[1])  ||  (dir_left == dir[1])  ||  (dir_up_left == dir[1])))  ||
			((dir_up_right   == dir[0]) && ((dir_up == dir[1])  ||  (dir_up_right == dir[1])  ||  (dir_right == dir[1])  ||  (dir_up_left == dir[1])))  ||
			((dir_right      == dir[0]) && ((dir_up == dir[1])  ||  (dir_up_right == dir[1])))  ||
			((dir_down_right == dir[0]) && (0))  ||
			((dir_down       == dir[0]) && (0))  ||
			((dir_down_left  == dir[0]) && (0))  ||
			((dir_left       == dir[0]) && ((dir_up == dir[1])  ||  (dir_up_left == dir[1])))  ||
			((dir_up_left    == dir[0]) && ((dir_up == dir[1])  ||  (dir_up_right == dir[1])  ||  (dir_left == dir[1])  ||  (dir_up_left == dir[1])))
		)
		{
			val.touch.two.scroll_up = 1; 
			m_inertia_scroll_updown = 1;
		}

		if(
			((dir_up         == dir[0]) && (0))  ||
			((dir_up_right   == dir[0]) && (0))  ||
			((dir_right      == dir[0]) && ((dir_down_right == dir[1])  ||  (dir_down == dir[1])))  ||
			((dir_down_right == dir[0]) && ((dir_right == dir[1])  ||  (dir_down_right == dir[1])  ||  (dir_down == dir[1])  ||  (dir_down_left == dir[1])))  ||
			((dir_down       == dir[0]) && ((dir_right == dir[1])  ||  (dir_down_right == dir[1])  ||  (dir_down == dir[1])  ||  (dir_down_left == dir[1])  ||  (dir_left == dir[1])))  ||
			((dir_down_left  == dir[0]) && ((dir_down_right == dir[1])  ||  (dir_down == dir[1])  ||  (dir_down_left == dir[1])  ||  (dir_left == dir[1])))  ||
			((dir_left       == dir[0]) && ((dir_down == dir[1])  ||  (dir_down_left == dir[1])))  ||
			((dir_up_left    == dir[0]) && (0))
		)
		{		
			val.touch.two.scroll_down = 1;
			m_inertia_scroll_updown = -1;
		}

		if((dir_left == dir[0]) && (dir_left == dir[1]))
		{
			//val.touch.two.scroll_down = 1;
			val.touch.two.scroll_left = 1;
			m_inertia_scroll_updown = 0;
		}
		if((dir_right == dir[0]) && (dir_right == dir[1]))
		{
			//val.touch.two.scroll_up = 1; 
			val.touch.two.scroll_right = 1;
			m_inertia_scroll_updown = 0;
		}		


		________________________________LOG_mTouch_twoFingerTrace(2,"dir[0]:%d,dir[1]:%d",dir[0],dir[1]);
		if(val.touch.two.scroll_up || val.touch.two.scroll_down || val.touch.two.scroll_left || val.touch.two.scroll_right)
		{
			________________________________LOG_mTouch_twoFingerTrace(3,"change_gesture(gstr_two_scroll);");

			change_gesture(gstr_two_scroll);
		
			if(m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_ADJUST))		
			{	
				//스크롤 시작시 쓰레숄드			
				INCREASE(m_state.gestureLevel, 0xFF);
				
				if(m_state.gestureLevel < 2)
				{
					return;
				}
				m_inertia_interval = m_timer_get_counter()-oldtime;
				m_inertia_interval = m_inertia_interval < 24 ? 24 : m_inertia_interval;	
				oldtime = m_timer_get_counter();

				m_timer_set(M_TiMER_ID_INERTIA_BLOCK, 200);
				static uint16_t oldspd=0;
				uint16_t orgspd = m_touch_db[first].vector.speed + m_touch_db[second].vector.speed;				

				if(oldspd + 100 < orgspd && oldspd != 0)
				{
					oldspd = orgspd;
					return;
				}
				if(orgspd > 200)
				{										
					if(overspeed_cnt==0)
					{
						overspeed_cnt++;
						return;
					}	
				}				
				if(400<orgspd)
				{
					return;
				}
				else
				{
					oldspd = orgspd;
				}
				

				speed = (m_touch_db[first].vector.speed + m_touch_db[second].vector.speed)/16;
				speed = (speed > 15) ? 15 : speed;
				
				uint8_t repeatTable[16] = {1,1,1,2,2,4,5,7,9,13,16,20,20,20,20};
				

				if(m_comm_current_central_type() == M_COMM_CENTRAL_MAC)
				{
					
					repeatTable[0] = 1;
					repeatTable[1] = 1;
					repeatTable[2] = 1;
					repeatTable[3] = 1;
					repeatTable[4] = 1;
					repeatTable[5] = 2;
					repeatTable[6] = 2;
					repeatTable[7] = 2;
					repeatTable[8] = 3;
					repeatTable[9] = 4;
					repeatTable[10] = 6;
					repeatTable[11] = 8;
					repeatTable[12] = 10;
					repeatTable[13] = 12;
					repeatTable[14] = 14;
					repeatTable[15] = 12;
					
				}



				if(m_comm_current_central_type() == M_COMM_CENTRAL_ANDROID)
				{
					
					repeatTable[0] = 1;
					repeatTable[1] = 1;
					repeatTable[2] = 1;
					repeatTable[3] = 1;
					repeatTable[4] = 1;
					repeatTable[5] = 1;
					repeatTable[6] = 2;
					repeatTable[7] = 2;
					repeatTable[8] = 3;
					repeatTable[9] = 4;
					repeatTable[10] = 6;
					repeatTable[11] = 10;
					repeatTable[12] = 12;
					repeatTable[13] = 12;
					repeatTable[14] = 12;
					
				}

				
				m_inertia_repeat = repeatTable[speed] + speed;
				m_inertia_trvldist = repeatTable[speed];

				if(m_comm_current_central_type() == M_COMM_CENTRAL_MAC)
				{
					m_inertia_trvldist = 1 + repeatTable[speed]/2;
					m_inertia_repeat = repeatTable[speed]  + speed;
				}

				val.touch.two.scroll_trvldist = m_inertia_trvldist;
				
				rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);
				nrf_delay_us(BACK_DELAY);

				memset(&val_reset, 0, sizeof(val_reset));
				val_reset.touch.two.back = 1;				
				m_touchlock_automode = true;
				
				if(m_comm_current_central_type() == M_COMM_CENTRAL_MAC)
				{
					/*
					nrf_delay_ms(100/m_inertia_trvldist);
					rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);
					nrf_delay_us(BACK_DELAY);
					rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val_reset);
					m_timer_set(M_TIMER_ID_MULTI_TOUCH_ADJUST, 80);
*/
				/*	//nrf_delay_ms(20);
					nrf_delay_ms(40/repeat+20);
					rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);
					nrf_delay_us(BACK_DELAY);
					rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val_reset);

					m_timer_set(M_TIMER_ID_MULTI_TOUCH_ADJUST, 40/repeat+20);
					*/
					m_timer_set(M_TIMER_ID_MULTI_TOUCH_ADJUST, 28 + 58/repeatTable[speed]);
				}
				else
				{
					m_timer_set(M_TIMER_ID_MULTI_TOUCH_ADJUST, 7);
				}
				//nrf_delay_ms(10);
				
				
				m_timer_set(M_TIMER_ID_UPDATE_MOVED,UPDATE_MOVED);
			}	
		}
		else
		{
			speed = (m_touch_db[first].vector.speed + m_touch_db[second].vector.speed)/16;
			if(speed < 6)
			{
				m_clear_inertiaScroll();
				m_inertia_repeat = 0;
			}			
		}

/*			
			change_gesture(gstr_two_scroll);
			speed = MAX(m_touch_db[first].vector.speed, m_touch_db[second].vector.speed);
			repeat = 1;
			repeat = 1 + (speed / 100);

			if (repeat > 1)
			{m_timer_set(M_TIMER_ID_MULTI_TOUCH_ADJUST, 0);}


			if(m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_ADJUST))
			{
				if(repeat<3)
				{
					repeat =1;
				}
				while(repeat--)
				{//보냄					
					nrf_delay_us(200);
					rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);
				}

				//nrf_delay_us(BACK_DELAY);
				nrf_delay_us(160);

				// 2) 복귀
				// clear local variable
				memset(&val, 0, sizeof(val));
				val.touch.two.back = 1;
				rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);
				PRINT_IF_ERROR("m_comm_send_event()", rv);

				speed = MAX(m_touch_db[first].vector.speed , m_touch_db[second].vector.speed);

				// 천천히 움직이면 띄엄띄엄 보내도록
				if(M_COMM_CENTRAL_MAC == m_comm_current_central_type())
				{
					// Mac은 반응이 느림, 많이 보내야 함
					delay = SLIDE_2_FAST_MAC; 
				}
				else
				{
					// Optimized value, be careful when changing this value
					//delay = (speed < 30U ) ? M_TIMER_TIME_10MS(8) : M_TIMER_TIME_10MS(6);
					delay = 14;
				}
			
				m_timer_set(M_TIMER_ID_MULTI_TOUCH_ADJUST, delay);
				INCREASE(m_state.gestureLevel, 0xFF);
			}

			if(NRF_SUCCESS != rv)
			{
				DEBUG("OOPS[%d]", rv);
			}
			*/
	}
}

/*===========================================================================//
	name    : check_zoom
//===========================================================================*/
static ret_code_t check_zoom(uint8_t first, uint8_t second)
{				
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(uint8_t first:%d, uint8_t second:%d)",first,second);
	dir_type_t dir[2];

	
	if( (first >= M_TOUCH_COUNT_MAX) || (second >= M_TOUCH_COUNT_MAX))
	{
		
		return NRF_ERROR_INVALID_PARAM;
	}	
	else
	{	

		if((m_touch_db[first].w_index >= M_POINT_ENOUGH_NUM) && (m_touch_db[second].w_index >= M_POINT_ENOUGH_NUM))
		{			
			dir[0] = get_direction(m_touch_db[first].vector.angle);//touch_db 기반으로 방향 추적
			dir[1] = get_direction(m_touch_db[second].vector.angle);	
			________________________________LOG_mTouch_RecvZoom(1,"DIR: [%d,%d] (up:%d down:%d left:%d right:%d)", dir[0], dir[1], dir_up,dir_down,dir_left,dir_right);
			/*
			if(dir[0] != dir[1])
				{
				________________________________LOG_mTouch_RecvZoom(2,"SUCCESS");
				return NRF_SUCCESS;
				}*/
				/*
			if(		 ((dir_up == dir[0]) && (dir_down == dir[1]))
					|| ((dir_down == dir[0]) && (dir_up == dir[1]))
					|| ((dir_left == dir[0]) && (dir_right == dir[1]))
					|| ((dir_right == dir[0]) && (dir_left == dir[1])))
			{
				if(dir[0] != dir[1])
				{
				________________________________LOG_mTouch_RecvZoom(2,"SUCCESS");
				return NRF_SUCCESS;
				}
			}*/
			
/*
			if(		   ((dir_up_right == dir[0]) && ((dir_down_left == dir[1]) || (dir_down == dir[1]) || (dir_left== dir[1])))
					|| ((dir_down_left == dir[0]) && ((dir_up_right == dir[1]) || (dir_up == dir[1]) || (dir_right == dir[1])))
					|| ((dir_up_left == dir[0]) && ((dir_down_right == dir[1]) || (dir_down == dir[1]) || (dir_right == dir[1])))
					|| ((dir_down_right == dir[0]) && ((dir_up_left == dir[1]) || (dir_up == dir[1]) || (dir_left == dir[1])))
					|| ((dir_up == dir[0]) && ((dir_down == dir[1]) || (dir_down_left == dir[1]) || (dir_down_right == dir[1])))
					|| ((dir_down == dir[0]) && ((dir_up == dir[1]) || (dir_up_left == dir[1]) || (dir_up_right == dir[1])))
					|| ((dir_left == dir[0]) && ((dir_right == dir[1]) || (dir_up_right == dir[1]) || (dir_down_right == dir[1])))
					|| ((dir_right == dir[0]) && ((dir_left == dir[1]) || (dir_down_left == dir[1]) || (dir_up_left == dir[1])))	)*/
			if(
				((dir_up         == dir[0]) && ((dir_down_right == dir[1])  ||  (dir_down == dir[1])  ||  (dir_down_left == dir[1])))  ||
				((dir_up_right   == dir[0]) && ((dir_down_right == dir[1])  ||  (dir_down == dir[1])  ||  (dir_down_left == dir[1])  ||  (dir_left == dir[1])))  ||
				((dir_right      == dir[0]) && ((dir_down_left == dir[1])  ||  (dir_left == dir[1])  ||  (dir_up_left == dir[1])))  ||
				((dir_down_right == dir[0]) && ((dir_up == dir[1])  ||  (dir_up_right == dir[1])  ||  (dir_left == dir[1])  ||  (dir_up_left == dir[1])))  ||
				((dir_down       == dir[0]) && ((dir_up == dir[1])  ||  (dir_up_right == dir[1])  ||  (dir_up_left == dir[1])))  ||
				((dir_down_left  == dir[0]) && ((dir_up == dir[1])  ||  (dir_up_right == dir[1])  ||  (dir_right == dir[1])  ||  (dir_up_left == dir[1])))  ||
				((dir_left       == dir[0]) && ((dir_up_right == dir[1])  ||  (dir_right == dir[1])  ||  (dir_down_right == dir[1])))  ||
				((dir_up_left    == dir[0]) && ((dir_right == dir[1])  ||  (dir_down_right == dir[1])  ||  (dir_down == dir[1])  ||  (dir_down_left == dir[1])))
			)
			{
				{
					if(++zoomStartThreshold >= 4)
					{
						
						return NRF_SUCCESS;
					}
					else
					{
						
					}
				}			
			}
		}
	}	
	return NRF_ERROR_INVALID_DATA;
}

//===========================================================================
//	name    : update_zoom()
//===========================================================================
static void update_zoom(uint8_t first, uint8_t second)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(uint8_t first:%d, uint8_t second:%d)",first,second);
	ret_code_t rv = NRF_SUCCESS;
	m_comm_evt_arg_t val;
	dir_type_t dir[2];
	static uint16_t distance_old = 0;
	int16_t gapX = 0;
	int16_t gapY = 0;
	uint16_t distance;
	
	if(check_zoom(first,second) != NRF_SUCCESS)
	{
		return;
	}
	
	gapX = m_touch_db[first].point[0].loc.x/8 - m_touch_db[second].point[0].loc.x/8;
	gapY = m_touch_db[first].point[0].loc.y/8 - m_touch_db[second].point[0].loc.y/8;
	distance =  (gapX * gapX) + (gapY * gapY);//두 손가락지점사이의 거리


	if(zoomStartThreshold == 4)
	{
		distance_old = distance;
		
	}

	dir[0] = get_direction(m_touch_db[first].vector.angle);//touch_db 기반으로 방향 추적
	dir[1] = get_direction(m_touch_db[second].vector.angle);	

	if(		   ((dir_up_right == dir[0]) && ((dir_down_left == dir[1]) || (dir_down == dir[1]) || (dir_left== dir[1])))
			|| ((dir_down_left == dir[0]) && ((dir_up_right == dir[1]) || (dir_up == dir[1]) || (dir_right == dir[1])))
			|| ((dir_up_left == dir[0]) && ((dir_down_right == dir[1]) || (dir_down == dir[1]) || (dir_right == dir[1])))
			|| ((dir_down_right == dir[0]) && ((dir_up_left == dir[1]) || (dir_up == dir[1]) || (dir_left == dir[1])))
			|| ((dir_up == dir[0]) && ((dir_down == dir[1]) || (dir_down_left == dir[1]) || (dir_down_right == dir[1])))
			|| ((dir_down == dir[0]) && ((dir_up == dir[1]) || (dir_up_left == dir[1]) || (dir_up_right == dir[1])))
			|| ((dir_left == dir[0]) && ((dir_right == dir[1]) || (dir_up_right == dir[1]) || (dir_down_right == dir[1])))
			|| ((dir_right == dir[0]) && ((dir_left == dir[1]) || (dir_down_left == dir[1]) || (dir_up_left == dir[1])))	)
	{				
		________________________________LOG_mTouch_RecvZoom(1,"LVL: %d", m_state.gestureLevel);
		memset(&val, 0, sizeof(val));// local varialbe clear
		if(m_state.gestureLevel <= 1)
		{			
			________________________________LOG_mTouch_RecvZoom(2,"zoom activating [%d+%d]   RESET:%d => %d", gapX*gapX, gapY*gapY ,distance_old, distance);			
		}
		else 
		{	
			if(1<m_state.gestureLevel) // 줌인것이 확실해지면
			{				
				
				if(distance_old+128 < distance)
				{					
					val.zoom.in = 1;
					________________________________LOG_mTouch_RecvZoom(3,"zoom activating lvl:%d [%d+%d]   + + + + +:%d => %d",m_state.gestureLevel,  gapX*gapX, gapY*gapY , distance_old, distance);								
				}
				else if(distance+128 < distance_old )
				{					
					val.zoom.out = 1;
					________________________________LOG_mTouch_RecvZoom(3,"zoom activating lvl:%d [%d+%d]   ---------:%d => %d",m_state.gestureLevel,   gapX*gapX, gapY*gapY ,distance_old, distance);								
				}
				else
				{
					//distance_old = distance;
					________________________________LOG_mTouch_RecvZoom(3,"zoom activating lvl:%d [%d+%d]   |||||||||:%d => %d",m_state.gestureLevel,   gapX*gapX, gapY*gapY ,distance_old, distance);					
				}
			}
		}
		
		change_gesture(gstr_two_zoom);
		INCREASE(m_state.gestureLevel, 0xFF);

		if(val.zoom.out || val.zoom.in )// We got it!!
		{
			if(m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_ADJUST))
			{
				rv = m_comm_send_event(M_COMM_EVENT_ZOOM_CTRL, val);
				m_timer_set(M_TIMER_ID_MULTI_TOUCH_ADJUST, M_TIMER_TIME_MS(200));
			}			
		}

	}
}


















//================================================================================
//   Function name : three_finger_handler()
//================================================================================
static void three_finger_handler(void)
{	
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(void)");
	ret_code_t rv;
	uint8_t i, index[3];

	for(i=0; i<3; i++)
	{
		rv = get_touch_index(&index[i], i+1); // touch-1: index[0], touch-2: index[1], touch-3: index[2]

		if(NRF_SUCCESS != rv)
		{			
			return;
		}
	}
	________________________________LOG_mTouch_threeScroll(0,"(fingers:%d, %d, %d, gstrState:%d, gstrLvl:%d )",index[0],index[1],index[2],m_state.gestureState,m_state.gestureLevel);

	ret_code_t check_result = check_scroll_three(index[0], index[1], index[2]);
	if (NRF_SUCCESS != check_result)
	{return;}

	switch(m_state.gestureState)
	{		
		case gstr_state_none :
			// Find any gesture 
			//________________________________LOG_mTouch_threeScroll(0,"(uint8_t first:%d, uint8_t second:%d, uint8_t third:%d, gstrState:%d, gstrLvl:%d )",index[0],index[1],index[2],m_state.gestureState,m_state.gestureLevel);
			update_scroll_left_right_three(index[0], index[1], index[2]);
			update_scroll_up_down_three(index[0], index[1], index[2]);		
			break;
		
		case gstr_three_scroll_up_down :
			//________________________________LOG_mTouch_threeScroll(0,"(uint8_t first:%d, uint8_t second:%d, uint8_t third:%d, gstrState:%d, gstrLvl:%d )",index[0],index[1],index[2],m_state.gestureState,m_state.gestureLevel);
			update_scroll_up_down_three(index[0], index[1], index[2]);
			break;

		case gstr_three_scroll_left_right :
			//________________________________LOG_mTouch_threeScroll(0,"(uint8_t first:%d, uint8_t second:%d, uint8_t third:%d, gstrState:%d, gstrLvl:%d )",index[0],index[1],index[2],m_state.gestureState,m_state.gestureLevel);
			update_scroll_left_right_three(index[0], index[1], index[2]);
			break;

		default :
			DEBUG("OOPS[%d]!!", m_state.gestureState);
			break;
	}
}



/*==============================================================================
   Function name : check_scroll_three()
==============================================================================*/
static ret_code_t check_scroll_three(uint8_t first, uint8_t second, uint8_t third)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(uint8_t first:%d, uint8_t second:%d, uint8_t third:%d)",first,second,third);


	static uint16_t delta_old[M_TOUCH_COUNT_MAX]= {0,};
	uint16_t delta[M_TOUCH_COUNT_MAX];
	uint8_t index = first;
	if( (index >= M_TOUCH_COUNT_MAX))
	{
		//________________________________LOG_mTouch_threeScroll(1,"return NRF_ERROR_INVALID_PARAM;");
		return NRF_ERROR_INVALID_PARAM;
	}
	else
	{				
		if((m_touch_db[index].w_index >= M_POINT_ENOUGH_NUM))
		{			
			delta[index] = (m_touch_db[index].point[0].loc.y) + (m_touch_db[index].point[0].loc.x/2);// + m_touch_db[index].point[0].loc.y;
			
			//________________________________LOG_mTouch_threeScroll(1,"24 < (abs(delta[index] - delta_old[index]):%d",abs(delta[index] - delta_old[index]));

			//속도가 느리면 olddelta를 빠르게 저장하기때문에 속도에따라서 olddelta 의 상한값을 정하면 될듯?	
			
			if(24 < abs(delta[index] - delta_old[index]))
			{
				delta_old[index] = delta[index];
				//________________________________LOG_mTouch_threeScroll(2,"return NRF_SUCCESS");
				return NRF_SUCCESS;
				//________________________________LOG_mTouch_threeScroll(8,"im not return");
			}
		//________________________________LOG_mTouch_threeScroll(8,"im not return");
		}
	//________________________________LOG_mTouch_threeScroll(8,"im not return");
	}
	//________________________________LOG_mTouch_threeScroll(1,"return NRF_ERROR_INVALID_PARAM;");
	return NRF_ERROR_INVALID_PARAM;


	/*
	static bool passes[M_TOUCH_COUNT_MAX];
	uint8_t pass = 0;
	uint8_t ok_cnt;
	uint8_t index;	

	for(index=0; index<M_TOUCH_COUNT_MAX; index++)
	{
		if(NRF_SUCCESS == check_scroll(index))
		{			
			pass++;
		}
	}
	
	ok_cnt = (0 == m_state.gestureLevel) ? 2 : 1;
	return NRF_SUCCESS;
	*/
	/* 
	uint8_t pass = 0, ok_cnt;

	if(NRF_SUCCESS == check_scroll(first))
	{
		pass++;
	}
	
	if(NRF_SUCCESS == check_scroll(second))
	{
		pass++;
	}
	
	if(NRF_SUCCESS == check_scroll(third))
	{
		pass++;
	}

	//________________________________LOG_mTouch_threeScroll(2,"pass:%d",pass);
	
	ok_cnt = (0 == m_state.gestureLevel) ? 2 : 1;

	if(pass>=ok_cnt)
	{
		return NRF_SUCCESS;
	}
	else
	{
		return NRF_ERROR_INVALID_DATA;
	}*/
	
}


//================================================================================
//	Function name : update_scroll_up_down_three()
//================================================================================
static void update_scroll_up_down_three(uint8_t first, uint8_t second, uint8_t third)
{	
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(uint8_t first:%d, uint8_t second:%d, uint8_t third:%d)",first,second,third);
	//________________________________LOG_mTouch_threeScroll(FUNCHEAD_FLAG,"(uint8_t first:%d, uint8_t second:%d, uint8_t third:%d)",first,second,third);
	ret_code_t rv;
	dir_type_t dir[3];

	m_comm_evt_arg_t val;
	//ret_code_t check_result = check_scroll_three(first, second, third);



	//if (NRF_SUCCESS == check_result)
	{
		________________________________LOG_mTouch_threeScroll(1,"UD: if (NRF_SUCCESS == check_scroll_three(first, second, third)) ");
		// clear local variable
		memset(&val, 0, sizeof(val));
		
		dir[0] = get_direction(m_touch_db[first].vector.angle);
		dir[1] = get_direction(m_touch_db[second].vector.angle);
		dir[2] = get_direction(m_touch_db[third].vector.angle);
		
		#if	defined(M_TOUCH_THREE_TRACE)
		DEBUG("[%s][%s][%s]", m_dirString[dir[0]]
				, m_dirString[dir[1]], m_dirString[dir[2]]);
		#endif

		if((dir_up == dir[0]) 
			&& (dir_up == dir[1]) 
			&& (dir_up == dir[2]))
		{
			#if	defined(M_TOUCH_THREE_TRACE)
			DEBUG("SCROLL UP");
			#endif
			val.touch.three.scroll_up = 1;
		}
		
		if((dir_down == dir[0]) 
			&& (dir_down == dir[1]) 
			&& (dir_down == dir[2]))
		{
			#if	defined(M_TOUCH_THREE_TRACE)
			DEBUG("SCROLL DOWN");
			#endif
			val.touch.three.scroll_down = 1;
		}

		if(val.touch.three.scroll_up || val.touch.three.scroll_down)
		{
			change_gesture(gstr_three_scroll_up_down);
			
			if(m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_ADJUST))
			{
				// 1) 현재 scroll 보내고
				rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);
				________________________________LOG_mTouch_threeScroll(2,"UD: send 3point-UD");

				// Some delay
				nrf_delay_us(BACK_DELAY);

				// 2) 복귀
				// clear local variable
				memset(&val, 0, sizeof(val));
				val.touch.three.back = 1;
				rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);
				PRINT_IF_ERROR("m_comm_send_event()", rv);

				// Ignore series event
				m_timer_set(M_TIMER_ID_MULTI_TOUCH_ADJUST, M_TIMER_TIME_100MS(4));
			}
		}
	}
}


//================================================================================
//	Function name : update_scroll_left_right_three()
//================================================================================
static void update_scroll_left_right_three(uint8_t first, uint8_t second, uint8_t third)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(uint8_t first:%d, uint8_t second:%d, uint8_t third:%d)",first,second,third);
	//________________________________LOG_mTouch_threeScroll(FUNCHEAD_FLAG,"(uint8_t first:%d, uint8_t second:%d, uint8_t third:%d)",first,second,third);
	ret_code_t rv;
	uint16_t local;
	uint32_t delay;
	dir_type_t dir[3];
	uint8_t right_cnt, left_cnt;

	m_comm_evt_arg_t val;
	//________________________________LOG_mTouch_threeScroll(FUNCHEAD_FLAG,"2");
	
	//ret_code_t check_result = check_scroll_three(first, second, third);
	//________________________________LOG_mTouch_threeScroll(2,"check_result : %d",check_result);
	//if (NRF_SUCCESS == check_result)
	{
		// clear local variable
		________________________________LOG_mTouch_threeScroll(1,"LR: if (NRF_SUCCESS == check_scroll_three(first, second, third)) ");
		memset(&val, 0, sizeof(val));
		dir[0] = get_direction(m_touch_db[first].vector.angle);
		dir[1] = get_direction(m_touch_db[second].vector.angle);
		dir[2] = get_direction(m_touch_db[third].vector.angle);
		
		

		#if	defined(M_TOUCH_THREE_TRACE)
		DEBUG("[%s][%s][%s]", m_dirString[dir[0]]
				, m_dirString[dir[1]], m_dirString[dir[2]]);
		#endif

		for(local = right_cnt = left_cnt = 0; local<3; local++)
		{
			if((dir_right == dir[local]))
			{
				right_cnt++;
			}
			else if((dir_left == dir[local]))
			{
				left_cnt++;
			}
		}

		
		// First Detection condition
		if(0 == m_state.gestureLevel)
		{
			if((right_cnt >= 3) && (!left_cnt)) // Optimized count
			{
				val.touch.three.scroll_right = 1;
			}

			if((left_cnt >= 3) && (!right_cnt)) // Optimized count
			{
				val.touch.three.scroll_left = 1;
			}

		} // On condition
		else 
		{
			if((right_cnt >= 2) && (!left_cnt)) // Optimized count
			{
				val.touch.three.scroll_right = 1;
			}

			if((left_cnt >= 2) && (!right_cnt)) // Optimized count
			{
				val.touch.three.scroll_left = 1;
			}
		}
		

		________________________________LOG_mTouch_threeScroll(2,"LR: left_cnt:%d, right_cnt:%d, val.touch.3.L:%d, val.touch.3.R:%d",right_cnt, left_cnt,val.touch.three.scroll_right ,val.touch.three.scroll_left);


		if(val.touch.three.scroll_right || val.touch.three.scroll_left)
		{
			#if	defined(M_TOUCH_THREE_TRACE)
			if(val.touch.three.scroll_right) { DEBUG("3 SCROLL RIGHT"); }
			if(val.touch.three.scroll_left) { DEBUG("3 SCROLL LEFT"); }
			#endif

			change_gesture(gstr_three_scroll_left_right);
			
			if(m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_ADJUST))
			{
				// 1) 현재 scroll 보내고 with 방향키 on
				val.touch.three.option = 1;
				rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);
				________________________________LOG_mTouch_threeScroll(3,"LR: send 3point-LR");
				// Some delay
				nrf_delay_us(BACK_DELAY);
				
				// 2) scroll 보내면서  with 방향키 off 
				// clear local variable
				val.touch.three.option = 0;
				rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);
				PRINT_IF_ERROR("m_comm_send_event()", rv);

				local = MAX(m_touch_db[0].vector.speed, m_touch_db[1].vector.speed);
				local = MAX(m_touch_db[2].vector.speed, local);
				
				// 천천히 움직이면 띄엄띄엄 보내도록
				// Optimized value, be careful when changing this value 
				delay = (local < 30U ) ? M_TIMER_TIME_10MS(10) : M_TIMER_TIME_10MS(6);

				if(m_comm_current_central_type() == M_COMM_CENTRAL_MAC)
				{
					delay = (local < 30U ) ? M_TIMER_TIME_10MS(60) : M_TIMER_TIME_10MS(60);
				}
			
				#if	defined(M_TOUCH_THREE_TRACE)
				DEBUG("delay:%d", delay);
				#endif
			
				m_timer_set(M_TIMER_ID_MULTI_TOUCH_ADJUST, delay); 
				INCREASE(m_state.gestureLevel, 0xFF);

				// 최종 special key(alt or alt shift)는 change_gesture()에서 처리
			}
			else
			{
				#if	defined(M_TOUCH_THREE_TRACE)
				DEBUG("DISCARD[L:%d][R:%d]", val.touch.three.scroll_left, val.touch.three.scroll_right);
				#endif
			}
		}
	
	}
}























//================================================================================
//   Function name : four_finger_handler()
//================================================================================
static void four_finger_handler(void)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(void)");
	ret_code_t rv;
	uint8_t i, index[3];

	for(i=0; i<3; i++)
	{
		rv = get_touch_index(&index[i], i+1); // touch-1: index[0], touch-2: index[1], touch-3: index[2]

		if(NRF_SUCCESS != rv)
		{			
			return;
		}
	}
	ret_code_t check_result = check_scroll_four(index[0], index[1], index[2]);
	if (NRF_SUCCESS != check_result)
	{return;}
	switch(m_state.gestureState)
	{
		case gstr_state_none :			
			// Find any gesture 
			update_scroll_up_down_four();
			update_scroll_left_right_four();
			break;
		
		case gstr_four_scroll_up_down :
			update_scroll_up_down_four();
			break;

		case gstr_four_scroll_left_right :
			update_scroll_left_right_four();
			break;

		default :
			DEBUG("OOPS[%d]!!", m_state.gestureState);
			break;
	}
}

//===========================================================================//
//			name    : check_scroll_four
//===========================================================================*s//

static ret_code_t check_scroll_four(uint8_t first, uint8_t second, uint8_t third)
{	
	static uint16_t delta_old[M_TOUCH_COUNT_MAX]= {0,};
	uint16_t delta[M_TOUCH_COUNT_MAX];
	uint8_t index = first;
	if( (index >= M_TOUCH_COUNT_MAX))
	{
		//________________________________LOG_mTouch_threeScroll(1,"return NRF_ERROR_INVALID_PARAM;");
		return NRF_ERROR_INVALID_PARAM;
	}
	else
	{				
		if((m_touch_db[index].w_index >= M_POINT_ENOUGH_NUM))
		{			
			delta[index] = (m_touch_db[index].point[0].loc.y) + (m_touch_db[index].point[0].loc.x/2);// + m_touch_db[index].point[0].loc.y;
			
			//________________________________LOG_mTouch_threeScroll(1,"24 < (abs(delta[index] - delta_old[index]):%d",abs(delta[index] - delta_old[index]));

			//속도가 느리면 olddelta를 빠르게 저장하기때문에 속도에따라서 olddelta 의 상한값을 정하면 될듯?	
			
			if(24 < abs(delta[index] - delta_old[index]))
			{
				delta_old[index] = delta[index];
				//________________________________LOG_mTouch_threeScroll(2,"return NRF_SUCCESS");
				return NRF_SUCCESS;
				//________________________________LOG_mTouch_threeScroll(8,"im not return");
			}
		//________________________________LOG_mTouch_threeScroll(8,"im not return");
		}
	//________________________________LOG_mTouch_threeScroll(8,"im not return");
	}
	//________________________________LOG_mTouch_threeScroll(1,"return NRF_ERROR_INVALID_PARAM;");
	return NRF_ERROR_INVALID_PARAM;


/*
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(void)");
	static bool passes[M_TOUCH_COUNT_MAX];
	uint8_t pass = 0;
	uint8_t ok_cnt;
	uint8_t index;

	for(index=0; index<M_TOUCH_COUNT_MAX; index++)
	{
		if(NRF_SUCCESS == check_scroll(index))
		{
			pass++;
		}
	}
	
	ok_cnt = (0 == m_state.gestureLevel) ? 2 : 1;
	return NRF_SUCCESS;
*/	
}


//================================================================================
//   Function name : update_scroll_up_down_four()
//================================================================================
static void update_scroll_up_down_four(void)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(void)");
	ret_code_t rv;
	dir_type_t dir[M_TOUCH_COUNT_MAX];

	m_comm_evt_arg_t val;

	//if (NRF_SUCCESS == check_scroll_four()) 
	{		
		// clear local variable
		memset(&val, 0, sizeof(val));

		for(uint8_t i = 0; i<M_TOUCH_COUNT_MAX; i++)
		{
			dir[i] = get_direction(m_touch_db[i].vector.angle);
			#if	defined(M_TOUCH_FOUR_TRACE)
			DEBUG("[%d][%s]", i, m_dirString[dir[i]]);
			#endif
		}

		if((dir_up == dir[0]) 
			&& (dir_up == dir[1]) 
			&& (dir_up == dir[2]) 
			&& (dir_up == dir[3]))
		{
			#if	defined(M_TOUCH_FOUR_TRACE)
			DEBUG("SCROLL UP");
			#endif
			val.touch.four.scroll_up = 1;
		}
		
		if((dir_down == dir[0]) 
			&& (dir_down == dir[1]) 
			&& (dir_down == dir[2]) 
			&& (dir_down == dir[3]))
		{
			#if	defined(M_TOUCH_FOUR_TRACE)
			DEBUG("SCROLL DOWN");
			#endif
			val.touch.four.scroll_down = 1;
		}

		if(val.touch.four.scroll_up || val.touch.four.scroll_down)
		{
			change_gesture(gstr_four_scroll_up_down);

			if(m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_ADJUST))
			{
				// 1) 현재 scroll 보내고
				rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);

				// Some delay
				nrf_delay_us(BACK_DELAY);

				// 2) 복귀
				// clear local variable
				memset(&val, 0, sizeof(val));
				val.touch.four.back = 1;
				rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);
				PRINT_IF_ERROR("m_comm_send_event()", rv);

				// Ignore series event
				m_timer_set(M_TIMER_ID_MULTI_TOUCH_ADJUST, M_TIMER_TIME_100MS(4));
			}
		}
	}
}


//================================================================================
//	name    : check_scroll_four
//================================================================================
static void update_scroll_left_right_four(void)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(void)");
	ret_code_t rv;
	uint16_t local;
	uint32_t delay;
	uint8_t right_cnt, left_cnt;
	dir_type_t dir[M_TOUCH_COUNT_MAX];

	m_comm_evt_arg_t val;

		
	//if (NRF_SUCCESS == check_scroll_four()) 
	{				
		// clear local variable
		memset(&val, 0, sizeof(val));

		for(local = right_cnt = left_cnt = 0; local<M_TOUCH_COUNT_MAX; local++)
		{
			dir[local] = get_direction(m_touch_db[local].vector.angle);
			
			if(dir_right == dir[local]) 
			{
				right_cnt++;
			}
			if(dir_left == dir[local]) 
			{
				left_cnt++;
			}
		}

		#if	defined(M_TOUCH_FOUR_TRACE)
		DEBUG("[%s][%s][%s][%s]", m_dirString[dir[0]], m_dirString[dir[1]]
				, m_dirString[dir[2]], m_dirString[dir[3]]);
		#endif

		// First Detection condition
		if(0 == m_state.gestureLevel)
		{
			if((right_cnt >= 4) && (!left_cnt)) // Optimized count
			{
				val.touch.four.scroll_right = 1;
			}

			if((left_cnt >= 4) && (!right_cnt)) // Optimized count
			{
				val.touch.four.scroll_left = 1;
			}
		}
		else // On condition
		{
			if((right_cnt >= 2) && (!left_cnt)) // Optimized count
			{
				val.touch.four.scroll_right = 1;
			}

			if((left_cnt >= 2) && (!right_cnt)) // Optimized count
			{
				val.touch.four.scroll_left = 1;
			}
		}
	
		if(val.touch.four.scroll_right || val.touch.four.scroll_left)
		{
			#if	defined(M_TOUCH_FOUR_TRACE)
			if(val.touch.four.scroll_right) { DEBUG("4 SCROLL RIGHT"); }
			if(val.touch.four.scroll_left)  { DEBUG("4 SCROLL LEFT"); }
			#endif

			change_gesture(gstr_four_scroll_left_right);
			
			if(m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_ADJUST))
			{
				// 1) 현재 scroll 보내고
				rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);
				PRINT_IF_ERROR("m_comm_send_event()", rv);

				// Some delay
				nrf_delay_us(BACK_DELAY);

				// 2) 복귀
				// clear local variable
				memset(&val, 0, sizeof(val));
				val.touch.four.back = 1;
				
				rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);
				PRINT_IF_ERROR("m_comm_send_event()", rv);

				local = MAX(m_touch_db[0].vector.speed, m_touch_db[1].vector.speed);
				local = MAX(m_touch_db[2].vector.speed, local);
				local = MAX(m_touch_db[3].vector.speed, local);

				// 천천히 움직이면 띄엄띄엄 보내도록
				// Optimized value, be careful when changing this value 
				//delay = (local < 30U ) ? M_TIMER_TIME_10MS(10) : M_TIMER_TIME_10MS(6);
				delay = (local < 30U ) ? M_TIMER_TIME_10MS(60) : M_TIMER_TIME_10MS(60);
				
				
			
				m_timer_set(M_TIMER_ID_MULTI_TOUCH_ADJUST, delay);

				INCREASE(m_state.gestureLevel, 0xFF);
			}
			else
			{
				#if	defined(M_TOUCH_FOUR_TRACE)
				DEBUG("DISCARD[L:%d][R:%d]", val.touch.four.scroll_left, val.touch.four.scroll_right);
				#endif
			}
		}
	}
}









/*\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ 
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\_*/




























































/*$$$$$\  $$$$$$$$\ $$$$$$$\  $$\   $$\  $$$$$$\                                       
$$  __$$\ $$  _____|$$  __$$\ $$ |  $$ |$$  __$$\                                      
$$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |$$ /  \__|                                     
$$ |  $$ |$$$$$\    $$$$$$$\ |$$ |  $$ |$$ |$$$$\                                      
$$ |  $$ |$$  __|   $$  __$$\ $$ |  $$ |$$ |\_$$ |                                     
$$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |$$ |  $$ |                                     
$$$$$$$  |$$$$$$$$\ $$$$$$$  |\$$$$$$  |\$$$$$$  |                                     
\_______/ \________|\_______/  \______/  \______/          
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\  $$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |$$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |$$ /  \__|
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |\$$$$$$\  
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ | \____$$\ 
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |$$\   $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |\$$$$$$  |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__| \______/ 
//============================================================================//
//                                 DEBUG FUNCTION                             //
//============================================================================*/


//================================================================================
//			m_touch_show_touch_db()
//================================================================================
static char* m_touch_show_all_touch_db(void)
{	
	uint8_t touch_num = (job_state_t)get_touch_num();
	static char result[256] = "";
	strcpy(result,"");
	
	for(int finger=0; finger<touch_num; finger++)
	{
		uint8_t w_index = m_touch_db[finger].w_index;
		uint16_t x0 = m_touch_db[finger].point[0].loc.x;
		uint16_t y0 = m_touch_db[finger].point[0].loc.y;
		uint16_t x1 = m_touch_db[finger].point[1].loc.x;
		uint16_t y1 = m_touch_db[finger].point[1].loc.y;
		uint16_t x2 = m_touch_db[finger].point[2].loc.x;
		uint16_t y2 = m_touch_db[finger].point[2].loc.y;
		sprintf(result, "%sfinger:%d[%d](%04d, %04d), ", result, finger, w_index, x0, y0);
	}
	
	return result;
}


static char* m_touch_show_all_state(void)
{	
	static char result[256] = "";
	uint8_t touch_num = (job_state_t)get_touch_num();
	sprintf(result, "job:%d>%d, gstr:%d>%d, lvl:%03d, tchnum:%d, tapnum:%d , MTCHTAB:%d, UPDMVD:%d, ADJST:%d",
	m_state.jobState_old,
	m_state.jobState,
	m_state.gestureState_old,
	m_state.gestureState,	
	m_state.gestureLevel,	
	touch_num,
	m_state.tab_number,
	m_timer_get(M_TIMER_ID_MULTI_TOUCH_TAB),
	m_timer_get(M_TIMER_ID_UPDATE_MOVED),
	m_timer_get(M_TIMER_ID_MULTI_TOUCH_ADJUST)
	);


	return result;
}




//================================================================================
//			m_touch_show_touch_db()
//================================================================================
static char* m_touch_show_touch_db(uint8_t finger)
{	//void show_m_touch_db_EACH(uint8_t id)
	//static char msg[100];
	
	/*
	
			typedef struct 
			{// 터치 데이터
				uint8_t			w_index; 	// point 의 몇번째 배열에 써야하는지 가리킴.
				touch_vector_t  vector;		// speed, angle, delta
				touch_point_t 	point[M_POINT_BUF_NUM];
			} touch_touch_t; //m_touch_db[3]

		typedef struct 
		{// 벡터
			uint16_t 	speed; //
			uint16_t 	angle; // 
			uint16_t 	delta; //
		} touch_vector_t;

		typedef struct 
		{// 1점 터치 정보
			uint16_t			timestamp; 	// timestamp
			touch_attr_t 		attr; 		// attribute
			touch_coordinate_t  loc;		// location
		} touch_point_t; 

	typedef struct 
	{// 좌표
		uint16_t 	x;
		uint16_t 	y;
	} touch_coordinate_t; 
	 */

	uint16_t x0 = m_touch_db[finger].point[0].loc.x;
	uint16_t y0 = m_touch_db[finger].point[0].loc.y;
	uint16_t x1 = m_touch_db[finger].point[1].loc.x;
	uint16_t y1 = m_touch_db[finger].point[1].loc.y;
	uint16_t x2 = m_touch_db[finger].point[2].loc.x;
	uint16_t y2 = m_touch_db[finger].point[2].loc.y;
	char result[256] = "";
	/*
	uint16_t dT,dX,dY;
	uint16_t ddT;

	// dT
	if(m_touch_db[finger].point[2].loc.x !=0 || m_touch_db[finger].point[2].loc.y !=0 )
	{
		dX = (m_touch_db[finger].point[2].loc.x - m_touch_point_begin[finger].loc.x);
		dY = (m_touch_db[finger].point[2].loc.y - m_touch_point_begin[finger].loc.y);
		dT = (m_touch_db[finger].point[2].timestamp - m_touch_point_begin[finger].timestamp);
		ddT = (m_touch_db[finger].point[2].timestamp - m_touch_db[finger].point[1].timestamp);
	}
	else 
	{
		if(m_touch_db[finger].point[1].loc.x !=0 || m_touch_db[finger].point[1].loc.y !=0 )
		{
			dX = (m_touch_db[finger].point[1].loc.x - m_touch_point_begin[finger].loc.x);
			dY = (m_touch_db[finger].point[1].loc.y - m_touch_point_begin[finger].loc.y);
			dT = (m_touch_db[finger].point[1].timestamp - m_touch_point_begin[finger].timestamp);
			ddT = (m_touch_db[finger].point[1].timestamp - m_touch_db[finger].point[0].timestamp);
		} 
		else 
		{
			dX = (m_touch_db[finger].point[0].loc.x - m_touch_point_begin[finger].loc.x);
			dY = (m_touch_db[finger].point[0].loc.y - m_touch_point_begin[finger].loc.y);
			dT = (m_touch_db[finger].point[0].timestamp - m_touch_point_begin[finger].timestamp);
			ddT = 0;
		}
	}
*/
	
	
	sprintf(result,"fngr:[%d] buff:[%d] (%04d,%04d) (%04d,%04d) (%04d,%04d)",finger,m_touch_db[finger].w_index, x0, y0, x1, y1, x2, y2);	
	return result;	
}




//================================================================================
//			GetXYFromPoint
//================================================================================
static void GetXYFromPoint(m_comm_evt_arg_point_t *point, int16_t *SignedX, int16_t *SignedY)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(m_comm_evt_arg_point_t *point, int16_t *SignedX, int16_t *SignedY)");
	*SignedX  = (point->buf[0] & 0xff);				// Lower 8-bits
	*SignedX |= (point->buf[1] & 0x0f) << 8;		// Upper 4-bits
	if(point->buf[1] & 0x08) *SignedX |= 0xf000;  	// Sign extension
	//
	*SignedY  = (point->buf[1] & 0xf0) >> 4;		// Lower 4-bits
	*SignedY |= (point->buf[2] & 0xff) << 4;		// Upper 8-bits
	if(point->buf[2] & 0x80) *SignedY |= 0xf000;  	// Sign extension
	//
	return;
}




//================================================================================
//			SetXYToThePoint
//================================================================================
static void SetXYToThePoint(m_comm_evt_arg_point_t *point, int16_t SignedX, int16_t SignedY)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(m_comm_evt_arg_point_t *point, int16_t SignedX, int16_t SignedY)");
	point->buf[0]  = (SignedX & 0x0ff)     ;	// Lower 8-bits of X
	point->buf[1]  = (SignedX & 0xf00) >> 8; 	// Upper 4-bits of X
	point->buf[1] |= (SignedY & 0x00f) << 4;	// Lower 4-bits of Y
	point->buf[2]  = (SignedY & 0xff0) >> 4; 	// Upper 8-bits of Y
	//
	return;
}









/*\   $$\ $$\   $$\ $$\   $$\  $$$$$$\  $$$$$$$$\ $$$$$$$\                             
$$ |  $$ |$$$\  $$ |$$ |  $$ |$$  __$$\ $$  _____|$$  __$$\                            
$$ |  $$ |$$$$\ $$ |$$ |  $$ |$$ /  \__|$$ |      $$ |  $$ |                           
$$ |  $$ |$$ $$\$$ |$$ |  $$ |\$$$$$$\  $$$$$\    $$ |  $$ |                           
$$ |  $$ |$$ \$$$$ |$$ |  $$ | \____$$\ $$  __|   $$ |  $$ |                           
$$ |  $$ |$$ |\$$$ |$$ |  $$ |$$\   $$ |$$ |      $$ |  $$ |                           
\$$$$$$  |$$ | \$$ |\$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$  |                           
 \______/ \__|  \__| \______/  \______/ \________|\_______/                            
//============================================================================//
//                                 UNUSED FUNCTION HEADS                      //
//============================================================================*/



#if 0
static void change(job_state_t new)
{
	if(m_state.jobState == new)
	{return;}

	

	#if	defined(M_TOUCH_STATE_TRACE)
	DEB("TryChange from(%s)--> to(%s)...\n", m_jobString[m_state.jobState], m_jobString[new]);
	#endif
	
	// touch state가 idle이 아니고, 'gesture인식' 중에 터치 개수가 변하면 무시
	if( !is_jobstate_transition_allowed(new) )	//
	{return;}

	#if	defined(M_TOUCH_STATE_TRACE)	
	DEB("\n##### change(): [%s]==>[%s] ######\n", m_jobString[m_state.jobState], m_jobString[new]);
	#endif
	

	if(job_state_idle != new)// touch가 처음 닿고 갱신 될때
	{
		
		if(job_state_idle == m_state.jobState)// 이전 상태가 idle이면 즉시 타이머 스타트
		{//N -> 0
			m_state.tab_number = (uint8_t)new;	// tab 정보 저장
			
			m_timer_set(M_TIMER_ID_MULTI_TOUCH_TAB, TAB_ALIVE_TIME); // tab_timer on	//#define TAB_ALIVE_TIME 	M_TIMER_TIME_100MS(3)
			
			#if defined(M_TOUCH_STATE_TRACE)
				DEB("==>Start Tap(s):%d  for %dms \n", m_state.tab_number, TAB_ALIVE_TIME);
			#endif
		}
		else // touch가 on 상태에서 터치 개숫가 바뀌었으면// 여러개 tab일때 동시에 인식되지 않는 경우임
		{//N -> M
			
			if(!m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_TAB))// 타이머가 on이고 개수가 늘어났으면 증가
			{
				
				if((uint8_t)new> m_state.tab_number)// increased tapping numbers: multi-tapping !
				{
					#if	defined(M_TOUCH_STATE_TRACE)
						DEB(" ==> One more Tap added: From(%d) --> To(%d) Within(%dms) <==\n",
							m_state.tab_number, (uint8_t)new, TAB_ALIVE_TIME
						);
					#endif
					
					m_state.tab_number = (uint8_t)new;// job_state_idle/one/two/three/four
				}
			}
			else 
			{
				#if defined(M_TOUCH_STATE_TRACE)
					DEB("==> %dms has elapsed .... no more Taps Allowed.... curr=%d Taps \n", TAB_ALIVE_TIME, m_state.tab_number);
				#endif
			}
		}
	}//new!=job_state_idle	
	
	m_state.jobState = new;	
	
	change_gesture(gstr_state_none);// Reset to gstr_state_none
}
#endif


/*
static void update_scroll_left_right_four(void)
{
	ret_code_t rv;
	uint16_t local;
	uint32_t delay;
	uint8_t right_cnt, left_cnt;
	dir_type_t dir[M_TOUCH_COUNT_MAX];

	m_comm_evt_arg_t val;	
	if (NRF_SUCCESS == check_scroll_four()) 
	{				
		// clear local variable
		memset(&val, 0, sizeof(val));

		for(local = right_cnt = left_cnt = 0; local<M_TOUCH_COUNT_MAX; local++)
		{
			dir[local] = get_direction(m_touch_db[local].vector.angle);
			
			if(dir_right == dir[local]) 
			{
				right_cnt++;
			}
			if(dir_left == dir[local]) 
			{
				left_cnt++;
			}
		}

		#if	defined(M_TOUCH_FOUR_TRACE)
		DEBUG("[%s][%s][%s][%s]", m_dirString[dir[0]], m_dirString[dir[1]]
				, m_dirString[dir[2]], m_dirString[dir[3]]);
		#endif

		// First Detection condition
		if(0 == m_state.gestureLevel)
		{
			if((right_cnt >= 4) && (!left_cnt)) // Optimized count
			{
				val.touch.four.scroll_right = 1;
			}

			if((left_cnt >= 4) && (!right_cnt)) // Optimized count
			{
				val.touch.four.scroll_left = 1;
			}
		}
		else // On condition
		{
			if((right_cnt >= 2) && (!left_cnt)) // Optimized count
			{
				val.touch.four.scroll_right = 1;
			}

			if((left_cnt >= 2) && (!right_cnt)) // Optimized count
			{
				val.touch.four.scroll_left = 1;
			}
		}
	
		if(val.touch.four.scroll_right || val.touch.four.scroll_left)
		{
			#if	defined(M_TOUCH_FOUR_TRACE)
			if(val.touch.four.scroll_right) { DEBUG("4 SCROLL RIGHT"); }
			if(val.touch.four.scroll_left)  { DEBUG("4 SCROLL LEFT"); }
			#endif

			change_gesture(gstr_four_scroll_left_right);
			
			if(m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_ADJUST))
			{
				// 1) 현재 scroll 보내고
				rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);
				PRINT_IF_ERROR("m_comm_send_event()", rv);

				// Some delay
				nrf_delay_us(BACK_DELAY);

				// 2) 복귀
				// clear local variable
				memset(&val, 0, sizeof(val));
				val.touch.four.back = 1;
				
				rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);
				PRINT_IF_ERROR("m_comm_send_event()", rv);

				local = MAX(m_touch_db[0].vector.speed, m_touch_db[1].vector.speed);
				local = MAX(m_touch_db[2].vector.speed, local);
				local = MAX(m_touch_db[3].vector.speed, local);

				// 천천히 움직이면 띄엄띄엄 보내도록
				// Optimized value, be careful when changing this value 
				delay = (local < 30U) ? M_TIMER_TIME_100MS(6) : M_TIMER_TIME_100MS(5);
			
				m_timer_set(M_TIMER_ID_MULTI_TOUCH_ADJUST, delay);

				INCREASE(m_state.gestureLevel, 0xFF);
			}
			else
			{
				#if	defined(M_TOUCH_FOUR_TRACE)
				DEBUG("DISCARD[L:%d][R:%d]", val.touch.four.scroll_left, val.touch.four.scroll_right);
				#endif
			}
		}
	}
}
*/



#if 0 //////////////////////////////// delete get_vector() ////////////////////
/*
================================================================================
   Function name : get_vector()
================================================================================
*/
static touch_vector_t get_vector(touch_point_t prev, touch_point_t now)
{
	int16_t deltaX, deltaY, deltaT;
	uint16_t u_deltaX, u_deltaY;
	//
	touch_vector_t vector = {0, INVALID_ANGLE, 0}; // (speed, angle, delta)
		
	
	//
	deltaX = now.loc.x - prev.loc.x;
	deltaY = now.loc.y - prev.loc.y;

	
	
	//
	if((0 != deltaX) || (0 != deltaY))
	{
		deltaT = delta_time(prev.timestamp, now.timestamp);

		if(deltaT)
		{
			u_deltaX = abs(deltaX);
			u_deltaY = abs(deltaY);
			vector.delta = u_deltaX + u_deltaY;

			// Optimized value, be careful when changing this value
			//#define MOVE_MINIMUM 				1U
			//#define SPEED_ADJUST 				40U 						
			//#define ENOUGH_SPEED_FOR_ANGLE	10U
			if((u_deltaX>MOVE_MINIMUM) || (u_deltaY>MOVE_MINIMUM))
			{
				vector.speed = (vector.delta)*SPEED_ADJUST; 	// Make it bigger to check
				vector.speed /= deltaT; 						// Not real speed, simple speed
			}
		}

		//#define TOO_MUCH_DELTA 3000 --> see: mokibo_config.h

		if((vector.speed > ENOUGH_SPEED_FOR_ANGLE) && (vector.delta < TOO_MUCH_DELTA))
		{
			vector.angle = m_util_atan2deg(deltaX, deltaY);
		}
		//
		#ifdef M_TOUCH_VECTOR_TRACE
		
			DEB("__VECTOR__(dx=%3d, dy=%3d, dT=%d)__[speed=%4d, angle=%4d, delta=%3d]__\n",
				u_deltaX,u_deltaY, deltaT,
				vector.speed, vector.angle,vector.delta
			);
		#endif
	}
	
	return vector;
}
#endif





#if 0 ///////////////////////////// delete get_new() ////////////////////////
/*
================================================================================
   Function name : get_new()
================================================================================
*/
static touch_coordinate_t get_new(uint16_t x, uint16_t y)
{
	uint32_t i;
	uint32_t sum[2] = {0, };
	uint32_t non_zero[2] = {0, };
	touch_coordinate_t val = {0, 0};

	#if	defined(SLIDING_WINDOW_FILTER)
	filter(x, y);
	#endif

	// Update window queue
	i = m_move_window.write_index;
	m_move_window.queue[i].x = x;
	m_move_window.queue[i].y = y;

	// Update window index
	if( ++m_move_window.write_index >= SLIDING_WINDOW_SIZE)
	{
		m_move_window.write_index = 0;
	}
	
	INCREASE(m_move_window.is_used, SLIDING_WINDOW_SIZE);

	if(m_move_window.is_used>=SLIDING_WINDOW_SIZE)
	{
		// Get new value
		for(i=0; i<SLIDING_WINDOW_SIZE; i++)
		{
			if(m_move_window.queue[i].x)
			{
				sum[0] += m_move_window.queue[i].x;
				non_zero[0]++;
			}

			if(m_move_window.queue[i].y)
			{
				sum[1] += m_move_window.queue[i].y;
				non_zero[1]++;
			}
		}

		if(non_zero[0])
		{
			val.x = (sum[0]/non_zero[0]);
		}

		if(non_zero[1])
		{
			val.y = (sum[1]/non_zero[1]);
		}
	}

	#if	defined(M_TOUCH_WINDOW_TRACE)
		//DEBUGS("m_move_windows:%d [%d][%d]", m_move_window.write_index, non_zero[0], non_zero[1]);
	#endif

	return val;
}
#endif /////////////////////////// delete get_new() ///////////////////////////






#if 0

static void one_finger_handler()
{			
	uint8_t index;

	ret_code_t rv = get_touch_index(&index, 1); // touch-1: index[0]
	
	if(NRF_SUCCESS != rv)
	{		
		DEBUG("OOPS[%d]!!", rv);
		return;
	}
	
	//DEB("one_finger_handler: BEGIN\n");
	switch(m_state.gestureState)
	{
		case gstr_state_none: SYSTEM_MSG("-gstr_state_none-\n");break;
		case gstr_one_move: SYSTEM_MSG("-gstr_one_move-\n");break;
		case gstr_one_drag: SYSTEM_MSG("-gstr_one_drag-\n");break;
		case gstr_one_volume_ctrl: SYSTEM_MSG("-gstr_one_volume_ctrl-\n");break;
		case gstr_one_zoom_ctrl: SYSTEM_MSG("-gstr_one_zoom_ctrl-\n");break;
		case gstr_one_stroll: SYSTEM_MSG("-gstr_one_stroll-\n");break;
		case gstr_two_scroll: SYSTEM_MSG("-gstr_two_scroll-\n");break;
		case gstr_two_zoom: SYSTEM_MSG("-gstr_two_zoom-\n");break;
		case gstr_three_scroll_up_down: SYSTEM_MSG("-gstr_three_scroll_up_down-\n");break;
		case gstr_three_scroll_left_right: SYSTEM_MSG("-gstr_three_scroll_left_right-\n");break;
		case gstr_four_scroll_up_down: SYSTEM_MSG("-gstr_four_scroll_up_down-\n");break;
		case gstr_four_scroll_left_right: SYSTEM_MSG("-gstr_four_scroll_left_right-\n");break;
	}

	//
	switch(m_state.gestureState)
	{		
		case gstr_state_none :
			update_move(index);		// m_touch_db[index]
			break;
		
		case gstr_one_move :
			update_move(index);		// m_touch_db[index]
			break;
		
		case gstr_one_drag :
			update_drag(index);
			break;

		case gstr_one_volume_ctrl :
			update_volume_ctrl(index);
			break;
		
		case gstr_one_zoom_ctrl :
			update_zoom_ctrl(index);
			break;
	
		case gstr_one_stroll :
			update_scroll_one(index);
			break;

		default :
			DEBUG("OOPS[%d]!!", m_state.gestureState);
			break;
	}
	//DEB("one_finger_handler: END\n");
}
#endif

/*
static void check_tab(void)
{	
    if( !m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_TAB) && (m_state.jobState_old != m_state.jobState))
	{//타이머 도는중				
		uint16_t max = 0;	
		uint16_t ThresholdArr[M_TOUCH_COUNT_MAX+1] = {100,8,8,500,900};				
		
		for(uint8_t i = 0; i<M_TOUCH_COUNT_MAX; i++)
		{
			//if(m_touch_db[i].w_index < M_POINT_ENOUGH_NUM)
			if(max< m_touch_db[i].w_index <= 1)
			{
				//m_timer_set(M_TIMER_ID_MULTI_TOUCH_TAB, 0);
				return;
			}

			if(max < m_touch_db[i].vector.speed)
			{
				max = m_touch_db[i].vector.speed;
			}
		}
			
		if(max > ThresholdArr[m_state.jobState])////MOVING_SPEED_THRESHOLD)//if(max > 200)
		{				
			m_timer_set(M_TIMER_ID_MULTI_TOUCH_TAB, 0);						
		}
		else
		{
			
		}
		
	}	
}*/
#if 0
static void check_tab(void)
{
	uint8_t i;
	uint16_t max;

	// M_TIMER_ID_MULTI_TOUCH_TAB timer가 아직 유효하면
	if(!m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_TAB))
	{
		
		// Find the fastest speed!!
		for(i = 0, max = 0; i<M_TOUCH_COUNT_MAX; i++)
		{
			max = MAX(max, m_touch_db[i].vector.speed);
		}
		
		DEB("\n###MaxSpeed=%d###\n",max);

#if 1 ////////////////////////////////////////////////////////////////////////////////////////////////		
		// BUGFIX:	M_TIMER_ID_MULTI_TOUCH_TAB	
		// Stanley':		
		//	통상 touch-tapping할 때에는, 동일한 포인트를 빠르게 두드리는 것이므로,  터치지점이 크게 변하지 않고
		//	따라서, speed vector값이 작을 것이므로, 이를 바탕으로 두 포인트간의 움직임이 크게 존재한다면,
		//	moving (single-point, double-point, triple-point, 등)일 것이라 생각해서
		//	muti-tapping을 판단하기 위한 시간 기점을 다시 잡도록 하는 것이다.
		//
		//  'TOUCH_ALIVE_DURATION'(50ms)인 동안에는 연속된 문직임으로 판단하고 있기 때문에
		//	실제 multitapping이 인지가 되려면, 오히려, 너무 빠르게 tapping하지 않아야,
		//	즉, tapping간격이 TOUCH_ALIVE_DURATION(50ms) 이상 300ms이하가 되어야 multitapping이 가능하다. 
		
		// [MJ] : Optimized value, be careful when changing this value:
		// [Stanley ]:
		// 		The touch-screen resoution is changed FROM 3700x2100 TO 2508x985
		// 		---> so, Vector, Speed, ..., are changed significantly....
		//		---> !! 320 dpi to 260 dpi !!!
		//#define MOVING_SPEED_THRESHOLD	60U	 <--- If a point is moving, then it's not a 'tapping'...
		//#define MOVING_SPEED_THRESHOLD	5U	 <--- If a point is moving, then it's not a 'tapping'...
		
		if(max > MOVING_SPEED_THRESHOLD)	// 60U by MJ
		{						
			// 'MOVING_SPEED_THRESHOLD'보다 빠르게 움직인다면, 그것은 tapping이 아니라 moving이다.
			// 그러므로, tapping을 위한 시간 기점을 다시 잡는다.
			
			// Cancel the Tap:  
			// Turn-off timer immediately
			
			m_timer_set(M_TIMER_ID_MULTI_TOUCH_TAB, 0);

			DEB("###-?-### %d-tapping is cleared:  actual_speed(%d) > threshold(%d) \n", 
						m_state.tab_number, max, MOVING_SPEED_THRESHOLD
				);
		}
		else
		{
			
		}
	#endif//////////////////////////////////////////////////////////////////////////////////////////////////		
	}
	else
	{
		#ifdef 	M_TOUCH_STATE_TRACE
			//DEB("m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_TAB) !!! \n");
		#endif
	}
}
#endif



#if 0
static ret_code_t check_move(uint8_t index)
{
	uint16_t threshold;
	if(index>=M_TOUCH_COUNT_MAX)
	{		
		return NRF_ERROR_INVALID_PARAM;
	}
	else
	{
		if(m_touch_db[index].w_index >= M_POINT_ENOUGH_NUM)
		{
			return NRF_SUCCESS;
		}
	}

/*
	if(index>=M_TOUCH_COUNT_MAX)
	{		
		return NRF_ERROR_INVALID_PARAM;
	}
	else
	{
		if(m_touch_db[index].w_index >= M_POINT_ENOUGH_NUM)
		{			
			threshold = (m_state.gestureLevel < CHECK_MOVE_LEVEL_THRSHOLD) ? FIRST_MOVE_DELTA_THRESHOLD : MOVE_DELTA_THRESHOLD;
			
			if(
				(m_touch_db[index].vector.speed > MOVE_SPEED_THRESHOLD	) &&
				(m_touch_db[index].vector.delta >= threshold			) && 
				(m_touch_db[index].vector.delta < MOVE_TOO_BIG_DELTA)	)
			{
				return NRF_SUCCESS;
			}
			else
			{				
				return NRF_ERROR_INVALID_DATA;
			}
		}
	}
*/
	DEBUG("OOPS");
	return NRF_ERROR_INVALID_DATA;
}
#endif


/*
static void update_scroll_two(uint8_t first, uint8_t second)
{
	uint16_t speed, repeat;
	uint32_t delay;
	ret_code_t rv = NRF_SUCCESS;
	dir_type_t dir[2];
	m_comm_evt_arg_t val;

	if (NRF_SUCCESS == check_scroll_two(first, second)) 
	{		
		memset(&val, 0, sizeof(val));

		dir[0] = get_direction(m_touch_db[first].vector.angle);
		dir[1] = get_direction(m_touch_db[second].vector.angle);

		#if	defined(M_TOUCH_SLIDE_TRACE)
		DEBUG("[%s][%s]", m_dirString[dir[0]], m_dirString[dir[1]]);
		#endif

		if((dir_up == dir[0]) && (dir_up == dir[1]))
		{
			#if	defined(M_TOUCH_SLIDE_TRACE)
			DEBUG("SLIDE UP");
			#endif
			// OS별로 처리 방시기 다름, Windows10과 동일하게 동작하도록
			val.touch.two.scroll_up = 1; 
		}

		if((dir_down == dir[0]) && (dir_down == dir[1]))
		{
			#if	defined(M_TOUCH_SLIDE_TRACE)
			DEBUG("SLIDE DOWN");
			#endif
			val.touch.two.scroll_down = 1;
		}

		if((dir_left == dir[0]) && (dir_left == dir[1]))
		{
			#if	defined(M_TOUCH_SLIDE_TRACE)
			DEBUG("SLIDE LEFT");
			#endif
			// OS별로 처리 방시기 다름, Windows10과 동일하게 동작하도록
			val.touch.two.scroll_left = 1;
		}

		if((dir_right == dir[0]) && (dir_right == dir[1]))
		{
			#if	defined(M_TOUCH_SLIDE_TRACE)
			DEBUG("SLIDE RIGHT");
			#endif
			val.touch.two.scroll_right = 1;
		}

#ifdef FLIMBS_20181210_TWO_POINT_SCROLL_ADJUST_BY_SPEED_CONTEXTUAL
		if (val.touch.two.scroll_up || val.touch.two.scroll_down|| val.touch.two.scroll_left || val.touch.two.scroll_right)
		{
			change_gesture(gstr_two_scroll);
			speed = (m_touch_db[first].vector.speed + m_touch_db[second].vector.speed)/2;

			repeat = m_flimbs_adjust_scroll_two(speed);

			m_timer_set(M_TIMER_ID_MULTI_TOUCH_ADJUST, 0);
			if (m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_ADJUST))
			{
				while (repeat--)
				{										
					if (M_COMM_CENTRAL_MAC == m_comm_current_central_type())
					{
						rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);
					}
					else
					{
						rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);
					}
				}
			// Give speed weight
				nrf_delay_us(BACK_DELAY);
				{
					memset(&val, 0, sizeof(val));
					val.touch.two.back = 1;
					rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);
					speed = (m_touch_db[first].vector.speed + m_touch_db[second].vector.speed) / 2;
										
					delay = (speed < 30U) ? M_TIMER_TIME_10MS(8) : M_TIMER_TIME_10MS(6);
					m_timer_set(M_TIMER_ID_MULTI_TOUCH_ADJUST, delay);
					INCREASE(m_state.gestureLevel, 0xFF);
				}
			}
				
			if (NRF_SUCCESS != rv)
			{
				DEBUG("OOPS[%d]", rv);
			}
		}
#else				
		if(val.touch.two.scroll_up || val.touch.two.scroll_down || val.touch.two.scroll_left || val.touch.two.scroll_right)
		{
			change_gesture(gstr_two_scroll);
			speed = MAX(m_touch_db[first].vector.speed, m_touch_db[second].vector.speed);
			repeat = 1 + (speed / 50);

			if (repeat > 1)
			{	m_timer_set(M_TIMER_ID_MULTI_TOUCH_ADJUST, 0);	}

			DEBUG("repeat:%d", repeat);

			if(m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_ADJUST))
			{
				while(repeat--)
				{
					// 1) 현재 scroll 보내고
					rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);
				}

				// Some delay
				nrf_delay_us(BACK_DELAY);

				// 2) 복귀
				// clear local variable
				memset(&val, 0, sizeof(val));
				val.touch.two.back = 1;
				rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);
				PRINT_IF_ERROR("m_comm_send_event()", rv);

				speed = MAX(m_touch_db[first].vector.speed, m_touch_db[second].vector.speed);

				// 천천히 움직이면 띄엄띄엄 보내도록
				if(M_COMM_CENTRAL_MAC == m_comm_current_central_type())
				{
					// Mac은 반응이 느림, 많이 보내야 함
					delay = SLIDE_2_FAST_MAC; 
				}
				else
				{
					// Optimized value, be careful when changing this value
					delay = (speed < 30U ) ? M_TIMER_TIME_10MS(8) : M_TIMER_TIME_10MS(6);
				}
			
				m_timer_set(M_TIMER_ID_MULTI_TOUCH_ADJUST, delay);
				INCREASE(m_state.gestureLevel, 0xFF);
			}

			if(NRF_SUCCESS != rv)
			{
				DEBUG("OOPS[%d]", rv);
			}
		}
#endif
	}
}
*/




/*
static void update_move(uint8_t index)
{
	ret_code_t rv;
	m_comm_evt_arg_t val;
	
	//DEB("update_move: BEGIN\n");
	
	// M_TIMER_ID_MULTI_TOUCH_TAB_ALIVE timer가 아직 유효하면
	// single tab + on이면
	if(!m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_TAB_ALIVE))
	{		
		change_gesture(gstr_one_drag);
		// Turn-on timer immediately for next use
		
		m_timer_set(M_TIMER_ID_MULTI_TOUCH_TAB_ALIVE, DRAG_ALIVE_TIME);
//BUGFIX:M_TIMER_ID_MULTI_TOUCH_TAB
//		// Turn-off timer immediately for next use
//		m_timer_set(M_TIMER_ID_MULTI_TOUCH_TAB, 0);
	}
	else if(drv_keyboard_is_pressed(S2L(V)))
	{
		change_gesture(gstr_one_volume_ctrl);
	}
	else if(drv_keyboard_is_pressed(S2L(Z)))
	{
		change_gesture(gstr_one_zoom_ctrl);
	}
	else if(drv_keyboard_is_pressed(S2L(S)))
	{
		change_gesture(gstr_one_stroll);
	}
	else if (NRF_SUCCESS == check_move(index)) 
	{
		change_gesture(gstr_one_move);

		// clear local variable
		memset(&val, 0, sizeof(val));
		get_point(&val.point, index);	// <--- m_touch_db[index]

		// 시간이 매번 달라야만 제대로 전송된다. 시간이 동일한 포인트들은 중복전송으로 이해되어 전송이 제한됨에 주의한다.
		rv = m_comm_send_event(M_COMM_EVENT_POINT_UPDATE, val);	PRINT_IF_ERROR("m_comm_send_event()", rv);
	
		INCREASE(m_state.gestureLevel, 0xFF);
	}

	//DEB("update_move: END\n");	
}
*/










/*
static void update_zoom(uint8_t first, uint8_t second)
{
	dir_type_t dir[2];			
	static uint16_t prevWidth = 0;
	static uint16_t prevHeight = 0;
	static uint8_t isFirst=0;
	uint8_t is_failed;	//0 일대만 패스
	uint32_t delay;
	uint16_t speed, prv, now, gap;
	uint8_t index;
	ret_code_t rv = NRF_SUCCESS;
	m_comm_evt_arg_t val;
	
	uint16_t width = (abs(m_touch_db[first].point[0].loc.x - m_touch_db[second].point[0].loc.x))/10;
	uint16_t height = (abs(m_touch_db[first].point[0].loc.y - m_touch_db[second].point[0].loc.y))/10;
	
	if (NRF_SUCCESS == check_zoom(first,second)) 
	{
		
		memset(&val, 0, sizeof(val));// local varialbe clear
		dir[0] = get_direction(m_touch_db[first].vector.angle);
		dir[1] = get_direction(m_touch_db[second].vector.angle);
		
		if(0 == m_state.gestureLevel)// First Detection condition
		{			
			
			
			// 첫번째 조건은 정확한 대각/상하/좌우 조건으로 움직였는지 확인
			// 대각/상하/좌우으로 움직이고 있을때, 오른손잡이, 왼손잡이 모두 check
			if(	((dir_up_right == dir[0]) && (dir_down_left == dir[1])) 
			|| ((dir_down_left == dir[0]) && (dir_up_right == dir[1]))
			|| ((dir_up_left == dir[0]) && (dir_down_right == dir[1])) 
			|| ((dir_down_right == dir[0]) && (dir_up_left == dir[1]))
			|| ((dir_up == dir[0]) && (dir_down == dir[1]))
			|| ((dir_down == dir[0]) && (dir_up == dir[1]))
			|| ((dir_left == dir[0]) && (dir_right == dir[1]))
			|| ((dir_right == dir[0]) && (dir_left == dir[1]))
			)
			{				
				gap = MAX(m_touch_db[first].vector.delta, m_touch_db[second].vector.delta);
				is_failed = gap > 20U ? 0 : 1; // PASS	// 20U: Optimized value, be careful when changing this value
			}
			else
			{
				is_failed = 1; // Fail
			}
		} // On condition
		else
		{			
			is_failed = 0; // PASS						
			if ((dir[0] == dir[1]) && (dir_none != dir[0])) // Zoom 모드로 들어오면 // 두 방향이 같지 않으면서 둘 사이의 거리가 변경되고 있는지 체크
			{
				 is_failed = 2; 
			}
		}
		
		if(is_failed)
		{									
			return;
		}

		//두 점을 꼭지점으로 하는 사각형의 넓이를 구한다.
		
		
		if(prevWidth == 0 && prevHeight == 0)
		{
			prevWidth = width;
			prevHeight = height;
			return;
		}

		if(prevWidth * prevHeight + 100 < width * height)
		{
			
			val.zoom.in = 1;
			prevWidth = width;
			prevHeight= height;
		}
		else if(prevWidth * prevHeight - 100 > width * height)
		{
			
			val.zoom.out = 1;
			prevWidth = width;
			prevHeight= height;
		}							
		else
		{
			prevWidth = 0;
			prevHeight = 0;
			return;
		}
		

		if(val.zoom.out || val.zoom.in)// We got it!!
		{						
			change_gesture(gstr_two_zoom);

			if(m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_ADJUST))
			{				
				rv = m_comm_send_event(M_COMM_EVENT_ZOOM_CTRL, val);// 1) 현재 zoom data보내고
				PRINT_IF_ERROR("m_comm_send_event()", rv);				
				nrf_delay_us(BACK_DELAY);// Some delay
				
				memset(&val, 0, sizeof(val));
				rv = m_comm_send_event(M_COMM_EVENT_ZOOM_CTRL, val);// 2) 복귀
				PRINT_IF_ERROR("m_comm_send_event()", rv);

				speed = MAX(m_touch_db[first].vector.speed, m_touch_db[second].vector.speed);				
				delay = M_TIMER_TIME_10MS(40);// 천천히 움직이면 띄엄띄엄 보내도록	//delay = (speed < 30U) ? M_TIMER_TIME_100MS(1) : M_TIMER_TIME_10MS(8);
				m_timer_set(M_TIMER_ID_MULTI_TOUCH_ADJUST, delay);

				INCREASE(m_state.gestureLevel, 0xFF);
			}					
		}		
	}
}*/









/*
static void update_zoom(uint8_t first, uint8_t second)
{
	static uint8_t isFirst=0;
	uint8_t is_failed;	//0 일대만 패스
	uint32_t delay;
	uint16_t speed, prv, now, gap;
	uint8_t index;
	ret_code_t rv = NRF_SUCCESS;
	dir_type_t dir[2];	
	m_comm_evt_arg_t val;
	
	if (NRF_SUCCESS == check_zoom(first, second)) 
	{						
		memset(&val, 0, sizeof(val));// local varialbe clear
		dir[0] = get_direction(m_touch_db[first].vector.angle);
		dir[1] = get_direction(m_touch_db[second].vector.angle);				
		
		if(0 == m_state.gestureLevel)// First Detection condition
		{
			// 첫번째 조건은 정확한 대각/상하/좌우 조건으로 움직였는지 확인
			// 대각/상하/좌우으로 움직이고 있을때, 오른손잡이, 왼손잡이 모두 check
			if(	((dir_up_right == dir[0]) && (dir_down_left == dir[1])) 
			|| ((dir_down_left == dir[0]) && (dir_up_right == dir[1]))
			|| ((dir_up_left == dir[0]) && (dir_down_right == dir[1])) 
			|| ((dir_down_right == dir[0]) && (dir_up_left == dir[1]))
			|| ((dir_up == dir[0]) && (dir_down == dir[1]))
			|| ((dir_down == dir[0]) && (dir_up == dir[1]))
			|| ((dir_left == dir[0]) && (dir_right == dir[1]))
			|| ((dir_right == dir[0]) && (dir_left == dir[1]))
			)
			{				
				gap = MAX(m_touch_db[first].vector.delta, m_touch_db[second].vector.delta);
				is_failed = gap > 20U ? 0 : 1; // PASS	// 20U: Optimized value, be careful when changing this value
			}
			else
			{
				is_failed = 1; // Fail
			}
		} // On condition
		else
		{			
			is_failed = 0; // PASS						
			if ((dir[0] == dir[1]) && (dir_none != dir[0])) // Zoom 모드로 들어오면 // 두 방향이 같지 않으면서 둘 사이의 거리가 변경되고 있는지 체크
			{
				 is_failed = 2; 				 
			}
		}
		
		if(is_failed)
		{									
			return;
		}

		gap = 0; // Set to zero
		index = m_touch_db[first].w_index;

		if(index>=2)
		{
			prv = abs(m_touch_db[first].point[0].loc.x-m_touch_db[second].point[0].loc.x);
			prv =+ abs(m_touch_db[first].point[0].loc.y-m_touch_db[second].point[0].loc.y);	// 가장 오래된 포인트간의 거리	
			now = abs(m_touch_db[first].point[index-1].loc.x - m_touch_db[second].point[index-1].loc.x);// 현재 포인트간의 거리
			now += abs(m_touch_db[first].point[index-1].loc.y - m_touch_db[second].point[index-1].loc.y);
			if(now > prv)
			{ gap = now - prv;}
			else 
			{ gap = prv - now;}
		}
		
		if(gap>ZOOM_DELTA2_THRESHOLD)//#define ZOOM_DELTA2_THRESHOLD 	10U
		{
			if(now > prv)// 멀어지고 있다면 Zoom in
			{
				val.zoom.in = 1;
			}
			else if(now < prv)// 가까워지고 있다면 Zoom out
			{				
				val.zoom.out = 1;
			}			
			else
			{
				// ignore case now == prv
			}
		}
		
		if(val.zoom.out || val.zoom.in)// We got it!!
		{						

			change_gesture(gstr_two_zoom);


			if(m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_ADJUST))
			{				
				rv = m_comm_send_event(M_COMM_EVENT_ZOOM_CTRL, val);// 1) 현재 zoom data보내고
				PRINT_IF_ERROR("m_comm_send_event()", rv);				
				nrf_delay_us(BACK_DELAY);// Some delay
				
				memset(&val, 0, sizeof(val));
				rv = m_comm_send_event(M_COMM_EVENT_ZOOM_CTRL, val);// 2) 복귀
				PRINT_IF_ERROR("m_comm_send_event()", rv);

				speed = MAX(m_touch_db[first].vector.speed, m_touch_db[second].vector.speed);				
				delay = M_TIMER_TIME_10MS(40);// 천천히 움직이면 띄엄띄엄 보내도록	//delay = (speed < 30U) ? M_TIMER_TIME_100MS(1) : M_TIMER_TIME_10MS(8);
				m_timer_set(M_TIMER_ID_MULTI_TOUCH_ADJUST, delay);

				INCREASE(m_state.gestureLevel, 0xFF);
			}

			if(NRF_SUCCESS != rv)
			{
				DEBUG("OOPS[%d]", rv);
			}			
		}		
	}
}*/





/*
static void update_zoom(uint8_t first, uint8_t second)
{
	static uint8_t isFirst=0;
	uint8_t is_failed;	//0 일대만 패스
	uint32_t delay;
	uint16_t speed, prv, now, gap;
	uint8_t index;
	ret_code_t rv = NRF_SUCCESS;
	dir_type_t dir[2];	
	m_comm_evt_arg_t val;
	
	if (NRF_SUCCESS == check_zoom(first, second)) 
	{						
		memset(&val, 0, sizeof(val));// local varialbe clear
		dir[0] = get_direction(m_touch_db[first].vector.angle);
		dir[1] = get_direction(m_touch_db[second].vector.angle);				
		#if	defined(M_TOUCH_ZOOM_TRACE)
		DEBUG("angle[%d][%d] [%s][%s]",m_touch_db[first].vector.angle
				, m_touch_db[second].vector.angle
				, m_dirString[dir[0]], m_dirString[dir[1]] );
		#endif
		
		if(0 == m_state.gestureLevel)// First Detection condition
		{
			// 첫번째 조건은 정확한 대각/상하/좌우 조건으로 움직였는지 확인
			// 대각/상하/좌우으로 움직이고 있을때, 오른손잡이, 왼손잡이 모두 check
			if(	((dir_up_right == dir[0]) && (dir_down_left == dir[1])) 
			|| ((dir_down_left == dir[0]) && (dir_up_right == dir[1]))
			|| ((dir_up_left == dir[0]) && (dir_down_right == dir[1])) 
			|| ((dir_down_right == dir[0]) && (dir_up_left == dir[1]))
			|| ((dir_up == dir[0]) && (dir_down == dir[1]))
			|| ((dir_down == dir[0]) && (dir_up == dir[1]))
			|| ((dir_left == dir[0]) && (dir_right == dir[1]))
			|| ((dir_right == dir[0]) && (dir_left == dir[1]))
			)
			{				
				gap = MAX(m_touch_db[first].vector.delta, m_touch_db[second].vector.delta);
				is_failed = gap > 20U ? 0 : 1; // PASS	// 20U: Optimized value, be careful when changing this value
			}
			else
			{
				is_failed = 1; // Fail
			}
		} // On condition
		else
		{			
			is_failed = 0; // PASS						
			if ((dir[0] == dir[1]) && (dir_none != dir[0])) // Zoom 모드로 들어오면 // 두 방향이 같지 않으면서 둘 사이의 거리가 변경되고 있는지 체크
			{
				 is_failed = 2; 				 
			}
		}
		
		if(is_failed)
		{			
			#if	defined(M_TOUCH_ZOOM_TRACE)
			DEBUG("[%s][%s]is_failed[%d]", m_dirString[dir[0]], m_dirString[dir[1]], is_failed); 
			#endif			
			return;
		}

		gap = 0; // Set to zero
		index = m_touch_db[first].w_index;

		if(index>=2)
		{			
			prv = abs(m_touch_db[first].point[0].loc.x-m_touch_db[second].point[0].loc.x);
			prv =+ abs(m_touch_db[first].point[0].loc.y-m_touch_db[second].point[0].loc.y);	// 가장 오래된 포인트간의 거리	
			now = abs(m_touch_db[first].point[index-1].loc.x - m_touch_db[second].point[index-1].loc.x);// 현재 포인트간의 거리
			now += abs(m_touch_db[first].point[index-1].loc.y - m_touch_db[second].point[index-1].loc.y);
			if(now > prv)
			{ gap = now - prv;}
			else 
			{ gap = prv - now;}
		}

		#if	defined(M_TOUCH_ZOOM_TRACE)
		DEBUG("ZOOM GAP[%d]", gap);
		#endif
		
		if(gap>ZOOM_DELTA2_THRESHOLD)//#define ZOOM_DELTA2_THRESHOLD 	10U
		{
			if(now > prv)// 멀어지고 있다면 Zoom in
			{
				val.zoom.in = 1;
			}
			else if(now < prv)// 가까워지고 있다면 Zoom out
			{				
				val.zoom.out = 1;
			}			
			else
			{
				// ignore case now == prv
			}
		}
		
		if(val.zoom.out || val.zoom.in)// We got it!!
		{					
			#if	defined(M_TOUCH_ZOOM_TRACE)
			if(val.zoom.in)  { DEBUG("ZOOM IN"); }
			if(val.zoom.out) { DEBUG("ZOOM OUT"); }
			#endif

			change_gesture(gstr_two_zoom);


			if(m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_ADJUST))
			{				
				rv = m_comm_send_event(M_COMM_EVENT_ZOOM_CTRL, val);// 1) 현재 zoom data보내고
				PRINT_IF_ERROR("m_comm_send_event()", rv);				
				nrf_delay_us(BACK_DELAY);// Some delay
				
				memset(&val, 0, sizeof(val));
				rv = m_comm_send_event(M_COMM_EVENT_ZOOM_CTRL, val);// 2) 복귀
				PRINT_IF_ERROR("m_comm_send_event()", rv);

				speed = MAX(m_touch_db[first].vector.speed, m_touch_db[second].vector.speed);				
				delay = M_TIMER_TIME_10MS(40);// 천천히 움직이면 띄엄띄엄 보내도록	//delay = (speed < 30U) ? M_TIMER_TIME_100MS(1) : M_TIMER_TIME_10MS(8);
				m_timer_set(M_TIMER_ID_MULTI_TOUCH_ADJUST, delay);

				INCREASE(m_state.gestureLevel, 0xFF);
			}

			if(NRF_SUCCESS != rv)
			{
				DEBUG("OOPS[%d]", rv);
			}			
		}		
	}
}
*/



/*
static ret_code_t check_zoom(uint8_t first, uint8_t second)
{			
	uint16_t delta[M_TOUCH_COUNT_MAX];
	delta[index] = (m_touch_db[0].point[0].loc.y) + (m_touch_db[0].point[0].loc.x/8);// + m_touch_db[index].point[0].loc.y;
	static uint16_t delta_old[M_TOUCH_COUNT_MAX];		
	
	if(8 < abs(delta[index] - delta_old[index]))
	{		
		delta_old[index] = delta[index];
		return NRF_SUCCESS;
	}
	else
	{		
		return NRF_ERROR_INVALID_DATA;
	}

}
*/



/*
ret_code_t m_touch_scan_handler(void)
{	
	ret_code_t rv;		
	job_state_t touch_num;

#ifdef SKIP_SCANNING_IF_NOT_TOUCHMODE_OR_TT_INT_HIGH
	// Stanley: for power saving...
	//		touch mode가 아니면, 혹시모를 터치 데이터만 읽어 없앤다.
	if(m_mokibo_get_status() != M_MOKIBO_TOUCH_STATUS)
	{
		//	if( cypress_read_TT_INT() )
		//	{
		//		// P.30/TT_INT가 low일 때에만, Cypress(parade) touch를 scan한다.
		//		// TT_INT is not LOW ...
		//		return NRF_ERROR_INVALID_STATE;
		//	};
		while(!cypress_read_TT_INT())
		{
			scan_data_dummy();
			Update_WDT();
		}
		return NRF_ERROR_INVALID_STATE;
	}
	
#endif


//	if(m_mokibo_get_status() == M_MOKIBO_TOUCH_STATUS)
//	{
//		if(m_timer_is_expired(M_TIMER_ID_TOUCH_RESUME))
//		{
//			clear_m_touch_db();
//			// lengthen very long time
//			m_timer_set(M_TIMER_ID_TOUCH_RESUME, 9999);
//		}
//	}
	
	rv = scan_data();	// Scan data 
	
	//
	if((NRF_SUCCESS != rv) && (NRF_ERROR_NOT_FOUND != rv) && (NRF_ERROR_NOT_SUPPORTED != rv))
	{
		#ifdef M_TOUCH_SHOW_DETAILED_ERRORS
			DEB("#Error: rv= scan_data() --> rv=%d \n",rv);
		#endif
		return rv;
	}

	// INFO: currently, if Mokibo is not in touch mode
	// do not handle touch data 
	if(M_MOKIBO_TOUCH_STATUS != m_mokibo_get_status()) //TODO 터치 아니면 신호안받아도 되지않나
	{
		return NRF_ERROR_INVALID_STATE;
	}
	
	touch_num = (job_state_t)get_touch_num();// cypress Info->touchCnt말고  m_touch_db[] 쓰기


	if(touch_num>M_TOUCH_COUNT_MAX)
	{
		DEB("### mutitouch(>%d) is not supported--> ForceTo IDLE.\n",M_TOUCH_COUNT_MAX);
		touch_num = job_state_idle;
	}
	
	change(touch_num);// caution:--> 'job_state' is same as 'touch_num'

	// check tab 
	check_tab(); // TODO 필요없는 코드가 될 확률이 높음
	
	// Run state machine	
	
	m_touch_handler[m_state.jobState]();
		//goto: idle_handler() TODO: idle 이 아닌 one_finger_handler() 에서 처리해야 할듯하다.
		//goto: one_finger_handler()
		//goto: two_finger_handler()
		//goto: three_finger_handler()
		//goto: four_finger_handler()

	// m_timer_is_expired(ID);
	// touch is busy ... so, refresh the idle timer: M_TIMER_ID_SLEEP_IF_IDLE
	m_timer_set(M_TIMER_ID_SLEEP_IF_IDLE, SLEEP_ON_IDLE_INTERVAL);			// 20분 timer
	//
	
	return NRF_SUCCESS;
}
*/





#if 0
static ret_code_t check_scroll_two(uint8_t index)
{	
	________________________________LOG_mTouch_twoFingerTrace(0,"");
	//uint32_t delta =( m_touch_db[0].point[0].loc.y) + (m_touch_db[0].point[0].loc.x/8);// + m_touch_db[index].point[0].loc.y;
	static uint16_t delta_old[M_TOUCH_COUNT_MAX]= {0,};
	uint16_t delta[M_TOUCH_COUNT_MAX];
	
	if( (index >= M_TOUCH_COUNT_MAX))
	{
		return NRF_ERROR_INVALID_PARAM;
	}
	else
	{				
		if((m_touch_db[index].w_index >= M_POINT_ENOUGH_NUM))
		{			
			delta[index] = (m_touch_db[index].point[0].loc.y) + (m_touch_db[index].point[0].loc.x/2);// + m_touch_db[index].point[0].loc.y;
			
			________________________________LOG_mTouch_twoFingerTrace(5,"24 < (abs(delta[index] - delta_old[index]):%d",abs(delta[index] - delta_old[index]));
			if(24 < abs(delta[index] - delta_old[index]))
			{		
				delta_old[index] = delta[index];
				return NRF_SUCCESS;
			}
		}
	}
	return NRF_ERROR_INVALID_PARAM;
	
}
#endif


#if 0
static ret_code_t check_scroll_four(void)
{
	uint8_t pass = 0;
	uint8_t ok_cnt;
	uint8_t index;	

	for(index=0; index<M_TOUCH_COUNT_MAX; index++)
	{
		if(NRF_SUCCESS == check_scroll(index))
		{			
			pass++;
		}
	}	
	ok_cnt = (0 == m_state.gestureLevel) ? 2 : 1;

	if(pass>=ok_cnt)
	{		
		return NRF_SUCCESS;
	}
	else
	{
		return NRF_ERROR_INVALID_DATA;
	}
}
#endif





//================================================================================
//	Function name : check_drag_move()
//================================================================================
static ret_code_t check_drag_move(uint8_t index)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(uint8_t index:%d)",index);
	
	uint16_t threshold;

	if(index>=M_TOUCH_COUNT_MAX)
	{
		DEBUG("OOPS[%d]", index);
		return NRF_ERROR_INVALID_PARAM;
	}
	else
	{
		if(m_touch_db[index].w_index >= 1)
		{
			// Optimized value, be careful when changing this value
			//#define CHECK_DRAG_MOVE_LEVEL_THRSHOLD		1U
			//#define FIRST_DRAG_MOVE_DELTA_THRESHOLD 	1U
			//#define DRAG_MOVE_DELTA_THRESHOLD 			2U
			threshold = (m_state.gestureLevel < CHECK_DRAG_MOVE_LEVEL_THRSHOLD) 
				? FIRST_DRAG_MOVE_DELTA_THRESHOLD : DRAG_MOVE_DELTA_THRESHOLD;

			//#define DRAG_MOVE_SPEED_THRESHOLD 1	// Optimized value, be careful when changing this value

		#if 0
			if((m_touch_db[index].vector.speed > DRAG_MOVE_SPEED_THRESHOLD)
					&& (m_touch_db[index].vector.delta >= threshold) && 
					(m_touch_db[index].vector.delta < MOVE_TOO_BIG_DELTA))
		#else
			if((m_touch_db[index].vector.delta >= threshold) && 
					(m_touch_db[index].vector.delta < MOVE_TOO_BIG_DELTA))
		#endif
			{
				return NRF_SUCCESS;
			}
			else
			{
				//DEBUG("OOPS[%d]", m_touch_db[index].vector.speed);
				return NRF_ERROR_INVALID_DATA;
			}
		}
	}

	DEBUG("OOPS");
	return NRF_ERROR_INVALID_DATA;
}


//================================================================================
//	Function name : check_volume_ctrl()
//================================================================================
static ret_code_t check_volume_ctrl(uint8_t index)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(uint8_t index:%d)",index);
	
	if(index>=M_TOUCH_COUNT_MAX)
	{
		DEBUG("OOPS[%d]", index);
		return NRF_ERROR_INVALID_PARAM;
	}
	else
	{
		if(m_touch_db[index].w_index >= M_POINT_ENOUGH_NUM)
		{
			// Optimized value, be careful when changing this value
			//#define VOL_CTRL_SPEED_THRESHOLD 	10U		
			//#define VOL_CTRL_DELTA_THRESHOLD 	10U

			if((m_touch_db[index].vector.speed > VOL_CTRL_SPEED_THRESHOLD)
				&& (m_touch_db[index].vector.delta > VOL_CTRL_DELTA_THRESHOLD) 
				&& (m_touch_db[index].vector.delta < MOVE_TOO_BIG_DELTA))
			{
				return NRF_SUCCESS;
			}
			else
			{
				//DEBUG("OOPS[%d]", m_touch_db[index].vector.speed);
				return NRF_ERROR_INVALID_DATA;
			}
		}
	}

	DEBUG("OOPS");
	return NRF_ERROR_INVALID_DATA;
}





//================================================================================
//	Function name : update_drag()
//================================================================================
static void update_drag(uint8_t index)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(uint8_t index:%d)",index);
	
	ret_code_t rv;
	
	m_comm_evt_arg_t val;
	
	if (NRF_SUCCESS == check_drag_move(index)) 
	{
		//APP_ERROR_CHECK_BOOL(MOUSE_LEN == 3);
		
		// clear local variable
		memset(&val, 0, sizeof(val));

		// Ignore starting for smooth movement
		if(m_state.gestureLevel>=2)
		{
			// clear local variable
			memset(&val, 0, sizeof(val));
			get_point(&val.point, index);

			// Send move
			// 시간이 매번 달라야만 제대로 전송된다. 시간이 동일한 포인트들은 중복전송으로 이해되어 전송이 제한됨에 주의한다.
			rv = m_comm_send_event(M_COMM_EVENT_POINT_UPDATE, val);
		}

		PRINT_IF_ERROR("m_comm_send_event()", rv);
		INCREASE(m_state.gestureLevel, 0xFF);
	}
}





//================================================================================
//	Function name : update_volume_ctrl()
//================================================================================
static void update_volume_ctrl(uint8_t index)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(uint8_t index:%d)",index);
	
	ret_code_t rv;
	dir_type_t dir;
	m_comm_evt_arg_t val;
	
	if(!drv_keyboard_is_pressed(S2L(V)))
	{
		change_gesture(gstr_state_none);
		return;
	}

	if (NRF_SUCCESS == check_volume_ctrl(index)) 
	{
		 dir = get_direction(m_touch_db[index].vector.angle);

		#if	defined(M_TOUCH_VOLUME_TRACE)
		DEBUG("angle[%d] [%s]", m_touch_db[index].vector.angle, m_dirString[dir]);
		#endif
	
		if((dir_left == dir) || (dir_right == dir) || (dir_up == dir) || (dir_down == dir))
		{
			// clear local variable
			memset(&val, 0, sizeof(val));

			val.volume.down = ((dir_left == dir) || (dir_down == dir)) ? 1 : 0;
			val.volume.up = ((dir_right == dir) || (dir_up == dir)) ? 1 : 0;
			
			if(m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_ADJUST))
			{
				#if	defined(M_TOUCH_VOLUME_TRACE)
				DEBUG("volume up[%d] down[%d]", val.volume.up, val.volume.down);
				#endif

				rv = m_comm_send_event(M_COMM_EVENT_VOLUME_CTRL, val);
				PRINT_IF_ERROR("m_comm_send_event()", rv);

				// Some delay
				nrf_delay_us(BACK_DELAY);

				val.volume.down = 0;
				val.volume.up = 0;

				rv = m_comm_send_event(M_COMM_EVENT_VOLUME_CTRL, val);
				PRINT_IF_ERROR("m_comm_send_event()", rv);

				m_timer_set(M_TIMER_ID_MULTI_TOUCH_ADJUST, VOLUME_CTRL_TIME);
			}
		}
	}
}





//================================================================================
//	Function name : update_zoom_ctrl()
//================================================================================
static void update_zoom_ctrl(uint8_t index)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(uint8_t index:%d)",index);
	
	ret_code_t rv;
	dir_type_t dir;
	m_comm_evt_arg_t val;

	if(!drv_keyboard_is_pressed(S2L(Z)))
	{
		change_gesture(gstr_state_none);
		return;
	}

	if (NRF_SUCCESS == check_volume_ctrl(index)) 
	{
		dir = get_direction(m_touch_db[index].vector.angle);

		#if	defined(M_TOUCH_VOLUME_TRACE)
		DEBUG("angle[%d] [%s]", m_touch_db[index].vector.angle, m_dirString[dir]);
		#endif
	
		if((dir_left == dir) || (dir_right == dir) || 
				(dir_up == dir) || (dir_down == dir))
		{
			// clear local variable
			memset(&val, 0, sizeof(val));

			val.zoom.out = ((dir_left == dir) || (dir_down == dir)) ? 1 : 0;
			val.zoom.in = ((dir_right == dir) || (dir_up == dir)) ? 1 : 0;
			
			if(m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_ADJUST))
			{
				#if	defined(M_TOUCH_VOLUME_TRACE)
				DEBUG("volume up[%d] down[%d]", val.volume.up, val.volume.down);
				#endif

				rv = m_comm_send_event(M_COMM_EVENT_ZOOM_CTRL, val);
				PRINT_IF_ERROR("m_comm_send_event()", rv);

				// Some delay
				nrf_delay_us(BACK_DELAY);

				val.zoom.out = 0;
				val.zoom.in = 0;

				rv = m_comm_send_event(M_COMM_EVENT_ZOOM_CTRL, val);
				PRINT_IF_ERROR("m_comm_send_event()", rv);

				m_timer_set(M_TIMER_ID_MULTI_TOUCH_ADJUST, VOLUME_CTRL_TIME);
			}
		}
	}
}





/*================================================================================
   Function name : update_scroll_one()
================================================================================*/
static void update_scroll_one(uint8_t index)
{
	________________________________LOG_mTouch_funcHeads(FUNCHEAD_FLAG,"(uint8_t index)");
	
	ret_code_t rv;
	dir_type_t dir;
	m_comm_evt_arg_t val;
	
	if(!drv_keyboard_is_pressed(S2L(S)))
	{
		change_gesture(gstr_state_none);
		return;
	}

	if (NRF_SUCCESS == check_scroll(index)) 
	{
		 dir = get_direction(m_touch_db[index].vector.angle);

		#if	defined(M_TOUCH_SCROLL_ONE_TRACE)
		DEBUG("angle[%d] [%s]", m_touch_db[index].vector.angle, m_dirString[dir]);
		#endif
	
		if((dir_left == dir) || (dir_right == dir) || 
				(dir_up == dir) || (dir_down == dir))
		{
			// clear local variable
			memset(&val, 0, sizeof(val));

			if(dir_up == dir)
			{
				val.touch.two.scroll_up = 1; 
			}

			if(dir_down == dir)
			{
				val.touch.two.scroll_down = 1; 				
			}

			if(dir_left == dir)
			{
				val.touch.two.scroll_left = 1; 
			}

			if(dir_right == dir)
			{
				val.touch.two.scroll_right = 1; 							
			}

			if(m_timer_is_expired(M_TIMER_ID_MULTI_TOUCH_ADJUST))
			{
				#if	defined(M_TOUCH_SCROLL_ONE_TRACE)
				DEBUG("scroll up[%d] down[%d] left[%d] right[%d]"
						, val.touch.two.scroll_up
						, val.touch.two.scroll_down
						, val.touch.two.scroll_left
						, val.touch.two.scroll_right);
				#endif

				// 1) 현재 scroll 보내고
				rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);
		
				// Some delay
				nrf_delay_us(BACK_DELAY);

				// 2) 복귀
				// clear local variable
				memset(&val, 0, sizeof(val));
				val.touch.two.back = 1;
				rv = m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);
				PRINT_IF_ERROR("m_comm_send_event()", rv);

				// 천천히 움직이면 띄엄띄엄 보내도록
				if(M_COMM_CENTRAL_MAC == m_comm_current_central_type())
				{
					// Mac은 반응이 느림, 많이 보내야 함
					m_timer_set(M_TIMER_ID_MULTI_TOUCH_ADJUST, SLIDE_2_FAST_MAC);
				}
				else
				{
					// Optimized value, be careful when changing this value
					//#define SCROLL_ONE_TIME		M_TIMER_TIME_100MS(1)		
					m_timer_set(M_TIMER_ID_MULTI_TOUCH_ADJUST, SCROLL_ONE_TIME);
				}
			}
		}
	}
}


/*\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ 
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\_*/




// End of file 



