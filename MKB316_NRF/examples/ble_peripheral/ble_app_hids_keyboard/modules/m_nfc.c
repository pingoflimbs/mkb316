/// \file m_nfc.c
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* XX-Apr-2018  JH.Seo
* + Created initial version
*
* 8-May-2018  JH.Seo
* + Added NRF_MODULE_ENABLED(MOKIBO_NFC)
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/

/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/
#include "sdk_config.h" // mokibo_config.h && sdk_config.h
//

#include "sdk_common.h"
#if NRF_MODULE_ENABLED(MOKIBO_NFC)
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "app_error.h"
#include "nrf_log.h"
#include "hal_nfc_t2t.h"
#include "nfc_t2t_lib.h"
#include "nfc_ble_pair_lib.h"
#include "nrf_drv_rng.h"
#include "ecc.h"
#include "peer_manager.h"
#include "ble_advertising.h"

#include "m_comm.h"
#include "m_comm_adv.h"
#include "m_nfc.h"     

#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
	// should be placed after "sdk_config.h"
	#include "SEGGER_RTT.h"
	#include "m_timer.h"
#endif

/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/             

/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/

/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/


/*============================================================================*/
/*                       PROTOTYPES OF LOCAL FUNCTIONS                        */
/*============================================================================*/

/*============================================================================*/
/*                              CLASS VARIABLES                               */
/*============================================================================*/
static m_nfc_state_t 			m_nfc_state 		= M_NFC_STATE_IDLE;
static ble_advertising_t *      m_p_advertising 	= NULL;

/*============================================================================*/
/*                              LOCAL FUNCTIONS                               */
/*============================================================================*/
static void m_nfc_callback(	void            * p_context,
							nfc_t2t_event_t   event,
							uint8_t const   * p_data,
							size_t            data_length);
static void m_nfc_ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context);
					
NRF_SDH_BLE_OBSERVER(m_nfc_ble_evt_observer, APP_NFC_BLE_OBSERVER_PRIO, m_nfc_ble_evt_handler, NULL);

/*============================================================================*/
/*                              GLOBAL FUNCTIONS                              */
/*============================================================================*/
ret_code_t nfc_ble_pair_mode_init(nfc_pairing_mode_t mode);
ret_code_t nfc_ble_pair_oob_auth_key_gen(void);
ret_code_t nfc_ble_pair_generate_lesc_keys(void);
ret_code_t nfc_ble_pair_lesc_key_gen(void);
ret_code_t nfc_ble_pair_setup(nfc_pairing_mode_t mode);
ret_code_t pm_secure_mode_set(nfc_pairing_mode_t mode);
ret_code_t nfc_ble_pair_data_set(nfc_pairing_mode_t mode);
ret_code_t nfc_ble_pair_adv_set(ble_advertising_t * const p_advertising);
ret_code_t nfc_ble_pair_lesc_dhkey_reply(ble_evt_t const * p_ble_evt);
ret_code_t nfc_ble_pair_auth_key_reply(ble_evt_t const * p_ble_evt);
							
/*
================================================================================
   Function name : m_nfc_init()
Function for initializing NFC BLE pairing module.
================================================================================
*/
void m_nfc_init(void)
{
	ret_code_t err_code = NRF_SUCCESS;

	m_nfc_state = M_NFC_STATE_INIT;
	
	// get pointer to the advertising module instance
    m_p_advertising = m_comm_adv_instance_ptr_get();
	nfc_ble_pair_adv_set(m_p_advertising);
	
	// Set ble pairinf mode
	err_code = nfc_ble_pair_mode_init((nfc_pairing_mode_t)NFC_PAIRING_MODE);
	APP_ERROR_CHECK(err_code);

	// Initialize RNG peripheral for authentication OOB data generation
    err_code = nrf_drv_rng_init(NULL);
    if (err_code != NRF_ERROR_INVALID_STATE)
    {
        APP_ERROR_CHECK(err_code);
    }
	
    // Initialize encryption module with random number generator use
    ecc_init(true);

    // Start NFC
    err_code = nfc_t2t_setup(m_nfc_callback, NULL);
    APP_ERROR_CHECK(err_code);

    // Set Peer Manager pairing mode
    err_code = pm_secure_mode_set((nfc_pairing_mode_t)NFC_PAIRING_MODE);
    APP_ERROR_CHECK(err_code);
	
	// Generate LESC key, depends on mode
	err_code = nfc_ble_pair_lesc_key_gen();
	APP_ERROR_CHECK(err_code);
}

/*
================================================================================
   Function name : m_nfc_start()
Function for start NFC BLE pairing module.
================================================================================
*/
void m_nfc_start(void)
{	
	ret_code_t err_code;

	NRF_LOG_INFO("NFC Start!");

	m_nfc_state = M_NFC_STATE_START;

	// get pointer to the advertising module instance
    m_p_advertising = m_comm_adv_instance_ptr_get();
	nfc_ble_pair_adv_set(m_p_advertising);
	
    // Set proper NFC data according to the pairing mode
    err_code = nfc_ble_pair_data_set((nfc_pairing_mode_t)NFC_PAIRING_MODE);
    APP_ERROR_CHECK(err_code);
	
    // Turn on tag emulation
    err_code = nfc_t2t_emulation_start();
    APP_ERROR_CHECK(err_code);
}

/*
================================================================================
   Function name : m_nfc_stop()
Function for stop NFC BLE pairing module.
================================================================================
*/
void m_nfc_stop(void)
{
	ret_code_t err_code;

	NRF_LOG_INFO("NFC Stop!");
	
    // Turn off tag emulation
    err_code = nfc_t2t_emulation_stop();
    APP_ERROR_CHECK(err_code);
	
	m_nfc_state = M_NFC_STATE_STOP;
}

/*
================================================================================
   Function name : m_nfc_state_is()
Function for get NFC BLE pairing module
================================================================================
*/
m_nfc_state_t m_nfc_state_is(void)
{
	return m_nfc_state;
}

bool m_nfc_state_start(void)
{
	return (m_nfc_state >= M_NFC_STATE_START);
}

bool m_nfc_state_stop(void)
{
	return (m_nfc_state <= M_NFC_STATE_STOP);
}

bool m_nfc_state_run(void)
{
	return (m_nfc_state == M_NFC_STATE_RUN);
}

ret_code_t m_nfc_pm_params_req(pm_evt_t const * p_evt)
{
	ret_code_t err_code;
	
	NRF_LOG_INFO("Send event to the NFC BLE pairing library");
	
	// Send event to the NFC BLE pairing library as it may dynamically alternate
	// security parameters to achieve highest possible security level.
    err_code = nfc_ble_pair_on_pm_params_req(p_evt);
    APP_ERROR_CHECK(err_code);
	
	return err_code;
}

/**
 * @brief Function for handling NFC events.
 *
 * @details Starts advertising and generates new OOB keys on the NFC_T2T_EVENT_FIELD_ON event.
 *
 * @param[in] p_context    Context for callback execution, not used in this callback implementation.
 * @param[in] event        Event generated by hal NFC lib.
 * @param[in] p_data       Received/transmitted data or NULL, not used in this callback implementation.
 * @param[in] data_length  Size of the received/transmitted packet, not used in this callback implementation.
 */
static void m_nfc_callback(void            * p_context,
                         nfc_t2t_event_t   event,
                         uint8_t const   * p_data,
                         size_t            data_length)
{
    UNUSED_PARAMETER(p_context);
    UNUSED_PARAMETER(p_data);
    UNUSED_PARAMETER(data_length);

    switch (event)
    {
        case NFC_T2T_EVENT_FIELD_ON:
            NRF_LOG_INFO("NFC_EVENT_FIELD_ON, NFC Detected!");
		
			m_nfc_state = M_NFC_STATE_RUN;

			// Generate Authentication OOB Key and update NDEF message content.
			nfc_ble_pair_oob_auth_key_gen();	// nfc_ble_pair_lib.c

			// get pointer to the advertising module instance
			m_p_advertising = m_comm_adv_instance_ptr_get();
			nfc_ble_pair_adv_set(m_p_advertising);

            break;

        case NFC_T2T_EVENT_FIELD_OFF:
            NRF_LOG_INFO("NFC_EVENT_FIELD_OFF");
            break;

        default:
            break;
    }
}

/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void m_nfc_ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    ret_code_t      err_code = NRF_SUCCESS;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            NRF_LOG_DEBUG("BLE_GAP_EVT_CONNECTED");
						
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            NRF_LOG_DEBUG("BLE_GAP_EVT_DISCONNECTED");

			if(m_nfc_state_run())
			{
				// Generate new LESC key pair and OOB data only when connection is not terminated because of reading a new tag
				err_code = nfc_ble_pair_generate_lesc_keys();
				APP_ERROR_CHECK(err_code);
			}
			
            break;

        case BLE_GAP_EVT_AUTH_KEY_REQUEST:
            NRF_LOG_DEBUG("BLE_GAP_EVT_AUTH_KEY_REQUEST");
		
			if(m_nfc_state_run())
			{
				NRF_LOG_INFO("NFC: auth key reply");
				
				err_code = nfc_ble_pair_auth_key_reply(p_ble_evt);
				APP_ERROR_CHECK(err_code);
			}
				
            break;

        // Upon LESC Diffie_Hellman key request, reply with key computed from device secret key and peer public key
        case BLE_GAP_EVT_LESC_DHKEY_REQUEST:
            NRF_LOG_DEBUG("BLE_GAP_EVT_LESC_DHKEY_REQUEST");

			if(m_nfc_state_run())
			{
				err_code = nfc_ble_pair_lesc_dhkey_reply(p_ble_evt);
				APP_ERROR_CHECK(err_code);
			}
			
            break;

        case BLE_GAP_EVT_AUTH_STATUS:
            NRF_LOG_DEBUG("BLE_GAP_EVT_AUTH_STATUS");

			if(m_nfc_state_run())
			{
				// Generate new LESC key pair and OOB data
				err_code = nfc_ble_pair_generate_lesc_keys();
				APP_ERROR_CHECK(err_code);
			}
            break;
			
		default:
			NRF_LOG_DEBUG("<<!!!>> OOPS[0x%02x]", p_ble_evt->header.evt_id);
            // No implementation needed.
        break;
	}
}

#endif // NRF_MODULE_ENABLED(MOKIBO_NFC)

// End of file 
