/// \file m_input.h
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 27-Oct-2017  MJ.Kim
* + Created initial version
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/

/*============================================================================*/
/*                            COMPILER DIRECTIVES                             */
/*============================================================================*/
#ifndef __M_INPUT_H
#define __M_INPUT_H


/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/    


/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/


/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/


/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/
typedef enum 
{
    M_INPUT_ID_LEFT_BUTTON = 0,
    M_INPUT_ID_RIGHT_BUTTON,
    M_INPUT_ID_LEFT_TOUCH,
    M_INPUT_ID_CENTER_TOUCH,
    M_INPUT_ID_RIGHT_TOUCH,
    M_INPUT_ID_MAX,
} M_INPUT_ID_TYPE;

typedef enum 
{
    M_INPUT_STATUS_OFF = 0,
    M_INPUT_STATUS_ON,

} M_INPUT_STATUS_TYPE;

/*============================================================================*/
/*                               GLOBAL FUNCTIONS                             */
/*============================================================================*/
ret_code_t m_input_init(void);

M_INPUT_STATUS_TYPE m_input_get(M_INPUT_ID_TYPE );

void m_input_update(void);

#endif // __M_INPUT_H 
