/// \file m_nfc.h
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* XX-Apr-2018  JH.Seo
* + Created initial version
*
* 8-May-2018  JH.Seo
* + Added NRF_MODULE_ENABLED(MOKIBO_NFC)
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/

/*============================================================================*/
/*                            COMPILER DIRECTIVES                             */
/*============================================================================*/
#ifndef __M_NFC_H
#define __M_NFC_H

#if NRF_MODULE_ENABLED(MOKIBO_NFC)

/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/    


/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/
typedef enum {
	M_NFC_STATE_IDLE,
	M_NFC_STATE_INIT,
	M_NFC_STATE_STOP,
	M_NFC_STATE_START,
	M_NFC_STATE_RUN
} m_nfc_state_t;

/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/


/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/

/*============================================================================*/
/*                               GLOBAL FUNCTIONS                             */
/*============================================================================*/
void m_nfc_init(void);
m_nfc_state_t m_nfc_state_is(void);
bool m_nfc_state_start(void);
bool m_nfc_state_stop(void);
bool m_nfc_state_run(void);
void m_nfc_stop(void);
void m_nfc_start(void);
ret_code_t m_nfc_pm_params_req(pm_evt_t const * p_evt);

#endif // NRF_MODULE_ENABLED(MOKIBO_NFC)

#endif // __M_NFC_H 
