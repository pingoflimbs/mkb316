/// \file m_touch.h
/// This file contains all required configurations for mokibo
/*==============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
==============================================================================*/







/*\   $$\ $$$$$$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$$$$$$\ $$\     $$\          
$$ |  $$ |\_$$  _|$$  __$$\\__$$  __|$$  __$$\ $$  __$$\\$$\   $$  |         
$$ |  $$ |  $$ |  $$ /  \__|  $$ |   $$ /  $$ |$$ |  $$ |\$$\ $$  /          
$$$$$$$$ |  $$ |  \$$$$$$\    $$ |   $$ |  $$ |$$$$$$$  | \$$$$  /           
$$  __$$ |  $$ |   \____$$\   $$ |   $$ |  $$ |$$  __$$<   \$$  /            
$$ |  $$ |  $$ |  $$\   $$ |  $$ |   $$ |  $$ |$$ |  $$ |   $$ |             
$$ |  $$ |$$$$$$\ \$$$$$$  |  $$ |    $$$$$$  |$$ |  $$ |   $$ |             
\__|  \__|\______| \______/   \__|    \______/ \__|  \__|   \__|             
//============================================================================//
//                            REVISION HISTORY                                //
//============================================================================//
//----------------------------------------------------------------------------//
20170818_MJ
    Created initial version based on JH.Seo work

20171207_MJ
    Changed global function to local for encasulation

20180202_MJ
    Changed m_touch_click_button_send() into m_comm_click_button_send()

20180207_MJ
    Changed m_comm_send() into m_comm_send_event()

20180209_MJ
    Created touch_touch_t & update() for multi-touch gesture

20180213_MJ
    Implemented get_vector()
    Increased M_POINT_BUF_NUM from 3 to 4

20180222_MJ
    Changed switch() to state machine
    Implemented update_tab()

20180403_MJ
    Improved drag
    Added check_drag_move

20180413_MJ
    Added no delta condition in delta_time()

20180502_MJ
    Added tunning code to remove touch noise

20180505_MJ
    Added SLIDE_2_FAST_MAC for adjusting Mac scroll

20180520_MJ
    Added gesture_one_volume_ctrl, gesture_one_zoom_ctrl, gesture_one_stroll
    Changed name slide to scroll for unifing description
    Optimized two scroll and zoom in/out

20180521_MJ
    Optimized point movement & tab

20180704_MJ
    Added TOO_MUCH_DELTA condition when getting m_util_atan2deg()
    Added up/down and left/right condition in update_zoom()

20180710_MJ
    https://app.asana.com/0/650649611668999/648897092499797/f
    Added speed weight in two scroll

20180808_Stanley
    The followings are added for MOKIBO CERTIFICATION TEST;
        DRV_KEYBOARD_HOTKEY_CERT_MODE..... in drv_keyboard.h
        m_KeyboardCode().................. in m_touch.c
        m_MouseMove()..................... in m_touch.c
        m_cert_mode_enabled............... in m_event.c
        m_do_cert_mode_repetition()....... in m_event.c
        The Caller ....................... in keyboard_scan_timeout_handler() in mokibo.c
        S2L(CERT_MODE).................... in drv_keyboard.c
        "HOTKEY_CERT_MODE"................ in drv_keyboard.c
        You can activate the CERT_MODE by typing [Fn+0].
        To deactivate the mode, press the same key once more.

20190524_flimbs
    템플릿 개편함. ascii_art_header.txt 참조
//----------------------------------------------------------------------------*/





/*$$$$$\  $$\    $$\ $$$$$$$$\ $$$$$$$\  $$\    $$\ $$$$$$\ $$$$$$$$\ $$\      $$\ 
$$  __$$\ $$ |   $$ |$$  _____|$$  __$$\ $$ |   $$ |\_$$  _|$$  _____|$$ | $\  $$ |
$$ /  $$ |$$ |   $$ |$$ |      $$ |  $$ |$$ |   $$ |  $$ |  $$ |      $$ |$$$\ $$ |
$$ |  $$ |\$$\  $$  |$$$$$\    $$$$$$$  |\$$\  $$  |  $$ |  $$$$$\    $$ $$ $$\$$ |
$$ |  $$ | \$$\$$  / $$  __|   $$  __$$<  \$$\$$  /   $$ |  $$  __|   $$$$  _$$$$ |
$$ |  $$ |  \$$$  /  $$ |      $$ |  $$ |  \$$$  /    $$ |  $$ |      $$$  / \$$$ |
 $$$$$$  |   \$  /   $$$$$$$$\ $$ |  $$ |   \$  /   $$$$$$\ $$$$$$$$\ $$  /   \$$ |
 \______/     \_/    \________|\__|  \__|    \_/    \______|\________|\__/     \__|
//============================================================================//
//                        FUNCTION CALL STACK OVERVIEW                        //
//============================================================================//
//----------------------------------------------------------------------------//
//----------------------------------------------------//TIMERS
M_TIMER_ID_TOUCH_SCAN
M_TIMER_ID_MOUSE_TAB_SCAN
M_TIMER_ID_TOUCH_LOCK
M_TIMER_ID_MULTI_TOUCH_ADJUST
M_TIMER_ID_MULTI_TOUCH_TAB
M_TIMER_ID_MULTI_TOUCH_TAB_ALIVE
M_TIMER_ID_ALL_TOUCHES_RELEASED

//----------------------------------------------------//VARS
static job_state_db_t m_state
static touch_touch_t m_touch_db[M_TOUCH_COUNT_MAX];

//----------------------------------------------------//FUNCS
m_touch_init()
extern m_touch_scan_handler()
static scan_data()
drv_cyprss::drv_cypress_read()
static update()
static change()
static get_touch_num()
static is_jobstate_transition_allowed()
static change_gesture()

//----------------------------------------------------//INIT
m_touch_init()
{   
    m_mode = 0
    m_state.jobState = 0
    m_touch_db = 0
    drv_twi_init(true)
    drv_cypress_init()
}

//----------------------------------------------------//LOOP
m_touch_scan_handler()
{
	scan_data()
	{
		rv = drv_cypress_read(&touchInfo)
		if(rv)
		{
			update(drv_cypress_info_t &touchInfo)
			{
				get_vector_stanley()
			}
		}
		else
		{
            if(++m_state.no_data>DB_CLEEAR_NO_DATA_COUNT)
            {
                m_touch_db = 0
                m_move_window = 0
                change(job_state_idle)
                m_state.no_data = 0
            }
		}
	}
	change(get_touch_num())
	{
		if(!is_jobstate_transition_allowed(new))
		{
			return;
		}		
		m_state.jobstate = new
		change_gesture(gstr_state_none)
	}	
	check_tab()

	m_touch_handler[m_state.jobState]()
	{
		idle_handler()
		{
			update_tab()
		}
		one_finger_handler()
		{
			get_touch_index()
			switch()
			{
				update_move()
				{					 
					if(drv_keyboard_is_pressed(S2L(V)))
					{
						change_gesture(gstr_one_volume_ctrl);
					}
					else if(drv_keyboard_is_pressed(S2L(Z)))
					{
						change_gesture(gstr_one_zoom_ctrl);
					}
					else if(drv_keyboard_is_pressed(S2L(S)))
					{
						change_gesture(gstr_one_stroll);
					}
					else if (NRF_SUCCESS == check_move(index)) 
					{
						change_gesture(gstr_one_move);								
						get_point(&val.point, index);						
						rv = m_comm_send_event(M_COMM_EVENT_POINT_UPDATE, val);	PRINT_IF_ERROR("m_comm_send_event()", rv);
						INCREASE(m_state.gestureLevel, 0xFF);
					}
				}
				update_drag()
				update_volume_ctrl()
				update_zoom_ctrl()
				update_scroll_one()
			}	
		}
		two_finger_handler()
		{
			get_touch_index()
			switch()
			{
				update_scroll_two()
				update_zoom()
			}			
		}
		three_finger_handler()
		{
			get_touch_index()
			switch()
			{
				update_scroll_up_down_three()
				update_scroll_left_right_three()				
			}
		}
		four_finger_handler()
		{
			get_touch_index()
			switch()
			{
				update_scroll_up_down_four()
				update_scroll_left_right_four()
			}
		}
    }
}


//----------------------------------------------------//LOGS
//실제 로그
{
	m_touch_scan_handler(void)
	keyboard_scan_timeout_handler(void)
	keyboard_scan_timeout_handler(void)
	m_touch_mode_set(void)
	ChangeState(new:3)
	clear_m_touch_db
	clearAll_cypressTouchData
	scan_data_dummy(void)
	m_touch_mode_get(void)
	SetTouchColor(void)
	
	반복
	TouchHandler(evt:1,argv:8)
	{
		m_touch_scan_handler(void)
		scan_data(void)
		get_touch_num(void)
		change(job_state_t new :0)
		chek_tab
		idle_handler
		update_tab
	}	
}

//----------------------------------------------------------------------------*/






























/*\      $$\  $$$$$$\  $$$$$$$\  $$\   $$\ $$\       $$$$$$$$\  $$$$$$\  
$$$\    $$$ |$$  __$$\ $$  __$$\ $$ |  $$ |$$ |      $$  _____|$$  __$$\ 
$$$$\  $$$$ |$$ /  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$ /  \__|
$$\$$\$$ $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$$$$\    \$$$$$$\  
$$ \$$$  $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$  __|    \____$$\ 
$$ |\$  /$$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$\   $$ |
$$ | \_/ $$ | $$$$$$  |$$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$$\ \$$$$$$  |
\__|     \__| \______/ \_______/  \______/ \________|\________| \______/ 
//============================================================================//
//                                 MODULES USED                               //
//============================================================================*/
#ifndef __M_TOUCH_H
#define __M_TOUCH_H
#include "ble_hids.h"
#include "drv_cypress.h"





 /*$$$$\  $$\       $$$$$$\  $$$$$$$\   $$$$$$\  $$\                           
$$  __$$\ $$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                          
$$ /  \__|$$ |     $$ /  $$ |$$ |  $$ |$$ /  $$ |$$ |                          
$$ |$$$$\ $$ |     $$ |  $$ |$$$$$$$\ |$$$$$$$$ |$$ |                          
$$ |\_$$ |$$ |     $$ |  $$ |$$  __$$\ $$  __$$ |$$ |                          
$$ |  $$ |$$ |     $$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |                          
\$$$$$$  |$$$$$$$$\ $$$$$$  |$$$$$$$  |$$ |  $$ |$$$$$$$$\                     
 \______/ \________|\______/ \_______/ \__|  \__|\________|                    
 $$$$$$\   $$$$$$\  $$\   $$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$\   $$\ $$$$$$$$\ 
$$  __$$\ $$  __$$\ $$$\  $$ |$$  __$$\\__$$  __|$$  __$$\ $$$\  $$ |\__$$  __|
$$ /  \__|$$ /  $$ |$$$$\ $$ |$$ /  \__|  $$ |   $$ /  $$ |$$$$\ $$ |   $$ |   
$$ |      $$ |  $$ |$$ $$\$$ |\$$$$$$\    $$ |   $$$$$$$$ |$$ $$\$$ |   $$ |   
$$ |      $$ |  $$ |$$ \$$$$ | \____$$\   $$ |   $$  __$$ |$$ \$$$$ |   $$ |   
$$ |  $$\ $$ |  $$ |$$ |\$$$ |$$\   $$ |  $$ |   $$ |  $$ |$$ |\$$$ |   $$ |   
\$$$$$$  | $$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$ |  $$ |$$ | \$$ |   $$ |   
 \______/  \______/ \__|  \__| \______/   \__|   \__|  \__|\__|  \__|   \__|   
 //============================================================================//
//                                 GLOBAL CONSTANTS                            //
//============================================================================*/




/*$$$$\  $$\       $$$$$$\  $$$$$$$\   $$$$$$\  $$\                                     
$$  __$$\ $$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                             
$$ /  \__|$$ |     $$ /  $$ |$$ |  $$ |$$ /  $$ |$$ |                             
$$ |$$$$\ $$ |     $$ |  $$ |$$$$$$$\ |$$$$$$$$ |$$ |                             
$$ |\_$$ |$$ |     $$ |  $$ |$$  __$$\ $$  __$$ |$$ |                             
$$ |  $$ |$$ |     $$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |                             
\$$$$$$  |$$$$$$$$\ $$$$$$  |$$$$$$$  |$$ |  $$ |$$$$$$$$\                        
 \______/ \________|\______/ \_______/ \__|  \__|\________|                       
$$$$$$$$\ $$\     $$\ $$$$$$$\  $$$$$$$$\ $$$$$$$\  $$$$$$$$\ $$$$$$$$\  $$$$$$\  
\__$$  __|\$$\   $$  |$$  __$$\ $$  _____|$$  __$$\ $$  _____|$$  _____|$$  __$$\ 
   $$ |    \$$\ $$  / $$ |  $$ |$$ |      $$ |  $$ |$$ |      $$ |      $$ /  \__|
   $$ |     \$$$$  /  $$$$$$$  |$$$$$\    $$ |  $$ |$$$$$\    $$$$$\    \$$$$$$\  
   $$ |      \$$  /   $$  ____/ $$  __|   $$ |  $$ |$$  __|   $$  __|    \____$$\ 
   $$ |       $$ |    $$ |      $$ |      $$ |  $$ |$$ |      $$ |      $$\   $$ |
   $$ |       $$ |    $$ |      $$$$$$$$\ $$$$$$$  |$$$$$$$$\ $$ |      \$$$$$$  |
   \__|       \__|    \__|      \________|\_______/ \________|\__|       \______/ 
//============================================================================//
//                                 GLOBAL TYPEDEFS                            //
//============================================================================*/
typedef enum
{
    M_TOUCH_CYPRESS_ACTIVE_AREA_WHOLE,  // All area is active
    M_TOUCH_CYPRESS_ACTIVE_AREA_LEFT,   // Left area is active
    M_TOUCH_CYPRESS_ACTIVE_AREA_RIGHT,  // Right area is active, DEFAULT
} M_TOUCH_ACTIVE_AREA_T;










































/*\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ 
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\_*/




/*$$$$\  $$\       $$$$$$\  $$$$$$$\   $$$$$$\  $$\                           
$$  __$$\ $$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                          
$$ /  \__|$$ |     $$ /  $$ |$$ |  $$ |$$ /  $$ |$$ |                          
$$ |$$$$\ $$ |     $$ |  $$ |$$$$$$$\ |$$$$$$$$ |$$ |                          
$$ |\_$$ |$$ |     $$ |  $$ |$$  __$$\ $$  __$$ |$$ |                          
$$ |  $$ |$$ |     $$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |                          
\$$$$$$  |$$$$$$$$\ $$$$$$  |$$$$$$$  |$$ |  $$ |$$$$$$$$\                     
 \______/ \________|\______/ \_______/ \__|  \__|\________|                    
$$\    $$\  $$$$$$\  $$$$$$$\  $$$$$$\  $$$$$$\  $$$$$$$\  $$\       $$$$$$$$\ 
$$ |   $$ |$$  __$$\ $$  __$$\ \_$$  _|$$  __$$\ $$  __$$\ $$ |      $$  _____|
$$ |   $$ |$$ /  $$ |$$ |  $$ |  $$ |  $$ /  $$ |$$ |  $$ |$$ |      $$ |      
\$$\  $$  |$$$$$$$$ |$$$$$$$  |  $$ |  $$$$$$$$ |$$$$$$$\ |$$ |      $$$$$\    
 \$$\$$  / $$  __$$ |$$  __$$<   $$ |  $$  __$$ |$$  __$$\ $$ |      $$  __|   
  \$$$  /  $$ |  $$ |$$ |  $$ |  $$ |  $$ |  $$ |$$ |  $$ |$$ |      $$ |      
   \$  /   $$ |  $$ |$$ |  $$ |$$$$$$\ $$ |  $$ |$$$$$$$  |$$$$$$$$\ $$$$$$$$\ 
    \_/    \__|  \__|\__|  \__|\______|\__|  \__|\_______/ \________|\________|
//============================================================================//
//                                    VARIABLES                               //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                                GLOBAL VARIABLES                            //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                                 DEBUG VARIABLES                            //
//----------------------------------------------------------------------------*/


/*$$$$\  $$\       $$$$$$\  $$$$$$$\   $$$$$$\  $$\                                     
$$  __$$\ $$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                             
$$ /  \__|$$ |     $$ /  $$ |$$ |  $$ |$$ /  $$ |$$ |                             
$$ |$$$$\ $$ |     $$ |  $$ |$$$$$$$\ |$$$$$$$$ |$$ |                             
$$ |\_$$ |$$ |     $$ |  $$ |$$  __$$\ $$  __$$ |$$ |                             
$$ |  $$ |$$ |     $$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |                             
\$$$$$$  |$$$$$$$$\ $$$$$$  |$$$$$$$  |$$ |  $$ |$$$$$$$$\                        
 \______/ \________|\______/ \_______/ \__|  \__|\________|                       
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\  $$\   $$\ $$$$$$$$\  $$$$$$\  $$$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\ $$ |  $$ |$$  _____|$$  __$$\ $$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|$$ |  $$ |$$ |      $$ /  $$ |$$ |  $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |      $$$$$$$$ |$$$$$\    $$$$$$$$ |$$ |  $$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |      $$  __$$ |$$  __|   $$  __$$ |$$ |  $$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\ $$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |$$ |  $$ |$$$$$$$$\ $$ |  $$ |$$$$$$$  |
\__|       \______/ \__|  \__| \______/ \__|  \__|\________|\__|  \__|\_______/ 
//============================================================================//
//                                FUNCTION HEADS                              //
//============================================================================*/

/*----------------------------------------------------------------------------//
//                            GLOBAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
extern ret_code_t m_touch_init(void);
extern void m_touch_set_init(void);
extern ret_code_t m_touch_resume(void);
extern ret_code_t m_touch_scan_handler(void);

extern M_TOUCH_ACTIVE_AREA_T m_touch_mode_get(void);
extern void m_touch_mode_set(M_TOUCH_ACTIVE_AREA_T new_mode);

extern void clear_m_touch_db(void);
extern void clearAll_CypressTouchData(void);// cf. drv_cypress_req_suspend_scanning()	// 	touch 잔재가 있으면, suspend_scanning()이 듣지 않는다...

extern void m_MouseMove(int16_t signed_dx, int16_t signed_dy);
extern void m_KeyswitchDown(int8_t usb_defined_keycode, int8_t CtrlAltShift);
extern void m_KeyswitchUp(void);
extern void m_clear_inertiaScroll(void);
/*----------------------------------------------------------------------------//
//                         GLOBAL DEBUG FUNCTION HEADS                        //
//----------------------------------------------------------------------------*/





/*\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ 
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\_*/







#endif // __M_TOUCH_H 


