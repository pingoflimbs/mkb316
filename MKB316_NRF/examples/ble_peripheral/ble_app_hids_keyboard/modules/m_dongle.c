/// \file m_dongle.c
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 7-Mar-2017  MJ.Kim
* + Created initial version
*
* 9-Apr-2018  JH.Seo
* + Added Timeout module in while loop
*
* 9-Apr-2018  MJ.Kim
* + Removed m_dongle_get_config(), m_dongle_set_config_default()
*
* 13-Apr-2018  MJ.Kim
* + Created m_dongle_handle_long_key()
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/

/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/
#include "sdk_config.h" // mokibo_config.h && sdk_config.h
#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
	// should be placed after "sdk_config.h"
	#include "SEGGER_RTT.h"
#endif

#include "sdk_common.h"
#include "app_error.h"
#include "nrf_log.h"

#include "nrf_gzll.h"
#include "nrf_gzp.h"
#include "nrf_gzll_error.h"
#include "bsp.h"
#include "nrf_gzllde_params.h"
#include "nrf_gzp_config.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "drv_fds.h"
#include "drv_keyboard.h"

#include "m_mokibo.h"
#include "m_comm.h"
#include "m_timer.h"
#include "m_dongle.h"        

/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/             

/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/

/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/

/*============================================================================*/
/*                       PROTOTYPES OF LOCAL FUNCTIONS                        */
/*============================================================================*/
static gzp_id_req_res_t send_host_id_req(void);

/*============================================================================*/
/*                              CLASS VARIABLES                               */
/*============================================================================*/
static uint8_t 	m_dongle_packet[M_DONGLE_PAYLOAD_SIZE];

static bool host_id_received     = false;     ///< Host ID received.
static bool system_addr_received = false;     ///< System address receivedfrom Host.

uint8_t m_dongle_system_address[GZP_SYSTEM_ADDRESS_WIDTH];
uint8_t m_dongle_host_id[GZP_HOST_ID_LENGTH];

void gzp_get_system_addr(uint8_t * dst_addr);

/*============================================================================*/
/*                              LOCAL FUNCTIONS                               */
/*============================================================================*/
/*
================================================================================
   Function name : send_host_id_req()
================================================================================
*/
static gzp_id_req_res_t send_host_id_req(void)
{
    gzp_id_req_res_t id_resp;

    // Try sending "Host ID" request
    id_resp = gzp_id_req_send();

    switch (id_resp)
    {
        case GZP_ID_RESP_REJECTED:
        case GZP_ID_RESP_FAILED:
            host_id_received     = false;
            //system_addr_received = false;
            break;

        case GZP_ID_RESP_GRANTED:
            host_id_received     = true;
            //system_addr_received = true;
            break;

        case GZP_ID_RESP_PENDING:
        default:
            break;
    }

    return id_resp;
}


/*============================================================================*/
/*                              GLOBAL FUNCTIONS                              */
/*============================================================================*/
/*
================================================================================
   Function name : m_dongle_init()
================================================================================
*/
ret_code_t m_dongle_init(void)
{
	ret_code_t rv = NRF_SUCCESS;
	//
	memset(&m_dongle_packet, 0, sizeof(m_dongle_packet));

    return rv;
}

/*
================================================================================
   Function name : m_dongle_start()
================================================================================
*/
ret_code_t m_dongle_start(void)
{
	bool     result_value = false;

	#if defined(M_DONGLE_TRACE)	
		DEB("m_dongle_start: BEGIN \n");
	#endif
	
	// Stop all application functionality before disabling the SoftDevice.
//  m_comm_ble_stop();
//	m_mokibo_mode = M_MOKIBO_GAZELL;
	
	#if defined(M_DONGLE_TRACE)	
		DEB("m_dongle_start: m_timer_nrf_start() \n");
	#endif
	m_timer_nrf_start();

	
	#if defined(M_DONGLE_TRACE)	
		DEB("m_dongle_start: nrf_gzll_init() \n");
	#endif
    // Initialize the Gazell Link Layer
    result_value = nrf_gzll_init(NRF_GZLL_MODE_DEVICE);  GAZELLE_ERROR_CODE_CHECK(result_value);

	#if defined(M_DONGLE_TRACE)	
		DEB("m_dongle_start: nrf_gzll_set_max_tx_attempts() \n");
	#endif
    // Attempt sending every packet up to MAX_TX_ATTEMPTS times
    result_value = nrf_gzll_set_max_tx_attempts(M_DONGLE_MAX_TX_ATTEMPTS);  GAZELLE_ERROR_CODE_CHECK(result_value);

	#if defined(M_DONGLE_TRACE)	
		DEB("m_dongle_start: nrf_gzll_set_device_channel_selection_policy() \n");
	#endif
    result_value = nrf_gzll_set_device_channel_selection_policy( NRF_GZLLDE_DEVICE_CHANNEL_SELECTION_POLICY);   GAZELLE_ERROR_CODE_CHECK(result_value);

	#if defined(M_DONGLE_TRACE)	
		DEB("m_dongle_start: nrf_gzll_set_timeslot_period() \n");
	#endif
    result_value = nrf_gzll_set_timeslot_period(NRF_GZLLDE_RXPERIOD_DIV_2); GAZELLE_ERROR_CODE_CHECK(result_value);


	#if defined(M_DONGLE_TRACE)	
		DEB("m_dongle_start: nrf_gzll_set_sync_lifetime() \n");
	#endif
	// Asynchronous mode, more efficient for pairing.
    result_value = nrf_gzll_set_sync_lifetime(0);   GAZELLE_ERROR_CODE_CHECK(result_value);

	#if defined(M_DONGLE_TRACE)	
		DEB("m_dongle_start: gzp_init() \n");
	#endif
    gzp_init(); // restore DB and init

    switch (gzp_get_pairing_status())
    {
        case -2:
			#if defined(M_DONGLE_TRACE)	
				DEB("m_dongle_start: pairing= -2 \n");
			#endif
            host_id_received     = false;
            system_addr_received = false;
			gzp_erase_pairing_data();
            break;

        case -1:
			#if defined(M_DONGLE_TRACE)	
				DEB("m_dongle_start: pairing= -1 \n");
			#endif
            host_id_received     = false;
            system_addr_received = false;
			gzp_erase_pairing_data();
            break;

        default:
			#if defined(M_DONGLE_TRACE)	
				DEB("m_dongle_start: paired.\n");
			#endif
            host_id_received     = true;
            system_addr_received = true;
    }
	
	#if defined(M_DONGLE_TRACE)	
		DEB("m_dongle_start: gzp_get_system_addr().\n");
	#endif
	gzp_get_system_addr(m_dongle_system_address);

	#if defined(M_DONGLE_TRACE)	
		DEB("m_dongle_start: gzp_get_host_id().\n");
	#endif
	gzp_get_host_id(m_dongle_host_id);
	

	#if defined(M_DONGLE_TRACE)	
		DEB("m_dongle_start: Mokibo Dongle pairing: system address(%d), host id(%d)", system_addr_received, host_id_received);
	#endif
	
	
	#if defined(M_DONGLE_TRACE)	
		DEB("m_dongle_start: nrf_gzll_enable()\n");
	#endif
    result_value = nrf_gzll_enable();GAZELLE_ERROR_CODE_CHECK(result_value);
	if(!m_dongle_is_link_ok())
	{		
		#if defined(M_DONGLE_TRACE)	
			DEB("m_dongle_start: m_dongle_pairing_req(true)\n");
		#endif
		m_dongle_pairing_req(true);
	}
	
	#if defined(M_DONGLE_TRACE)	
		DEB("m_dongle_start: END \n");
	#endif
	
    return NRF_SUCCESS;
}

/*
================================================================================
   Function name : m_dongle_stop()
================================================================================
*/
ret_code_t m_dongle_stop(void)
{

	#if defined(M_DONGLE_TRACE)	
		DEB("m_dongle_stop: BEGIN\n");
		DEB("m_dongle_stop: m_timer_nrf_stop()\n");
	#endif
	m_timer_nrf_stop();

	
	#if defined(M_DONGLE_TRACE)	
		DEB("m_dongle_stop: nrf_gzll_disable()\n");
	#endif
    // Disable gazell.
    nrf_gzll_disable();

	#if defined(M_DONGLE_TRACE)	
		DEB("m_dongle_stop: m_timer_set(M_TIMER_ID_DONGLE_TIMEOUT)\n");
		DEB("m_dongle_stop: m_timer_set(M_TIMER_ID_DONGLE_LONG_KEY)\n");
	#endif
	m_timer_set(M_TIMER_ID_DONGLE_TIMEOUT, DONGLE_STOP_TIME);
	m_timer_set(M_TIMER_ID_DONGLE_LONG_KEY, DONGLE_LONG_KEY_TIME);
	
	TIME_TRACE_START();



	#if defined(M_DONGLE_TRACE)	
		DEB("m_dongle_stop: Wait for Gazell to shut down...\n");
	#endif
    // Wait for Gazell to shut down.
    while (nrf_gzll_is_enabled())
    {
		if(m_timer_is_expired(M_TIMER_ID_DONGLE_TIMEOUT))
		{
			#if defined(M_DONGLE_TRACE)	
				DEB("m_dongle_stop: M_TIMER_ID_DONGLE_TIMEOUT....gazell is not stopped yet !!\n");
			#endif
			break;
		}
		//m_mokibo_WDT_reload();
    }

	TIME_TRACE_END(0);
 
    // Clean up after Gazell.
    NVIC_DisableIRQ(RADIO_IRQn);
    NVIC_DisableIRQ(TIMER2_IRQn);
    NVIC_DisableIRQ(SWI0_IRQn);
    NVIC_ClearPendingIRQ(RADIO_IRQn);
    NVIC_ClearPendingIRQ(TIMER2_IRQn);
    NVIC_ClearPendingIRQ(SWI0_IRQn);

//  m_comm_ble_start();
//	m_mokibo_mode = M_MOKIBO_BLE;

	#if defined(M_DONGLE_TRACE)	
		DEB("m_dongle_stop: END.\n");
	#endif

    return NRF_SUCCESS;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			from_hid_to_xy
//-----------------------------------------------------------------------------------------------------------------------------------
#ifdef HID_INPUT_REPORT_TRACE
void from_hid_to_xy(uint8_t *p_data, int16_t *x_ret, int16_t *y_ret)
{
	int16_t x,y;
	//
	x= ((p_data[1]&0x0f)<<8) | (p_data[0]&0xff);		// lower 12-bits
	y= ((p_data[2]&0xff)<<4) | ((p_data[1]&0xf0)>>4); 	// lower 12-bits
	if(x&0x0800){ x |= 0xFC00; } // sign-extension
	if(y&0x0800){ y |= 0xFC00; } // sign-extension
	//
	*x_ret= x;
	*y_ret= y;
}
#endif


/*
================================================================================
   Function name : m_dongle_send()
================================================================================
*/
ret_code_t m_dongle_send(uint8_t rep_idx, uint8_t *p_data, uint8_t len)
{
	ret_code_t rv;

	// Stanley added.
	if(m_comm_mode != M_COMM_MODE_GAZELL) return NRF_ERROR_INTERNAL;
	if(!host_id_received) return NRF_ERROR_INTERNAL;
	if(!system_addr_received) return NRF_ERROR_INTERNAL;
	// Stanley added.
	
	
	#if defined(M_DONGLE_TRACE)	
		DEB("m_dongle_send: BEGIN \n");
	#endif

	if(NULL == p_data)
	{
		DEB("m_dongle_send: error\n");
		rv = NRF_ERROR_NULL;
	}
	else
	{
		#ifdef HID_INPUT_REPORT_TRACE
		DEB("m_dongle_send: AddToTxFIFO: Len=%d, ", len);
		switch(rep_idx)
		{
			case M_DONGLE_KEY: DEB("M_DONGLE_KEY"); break;
			case M_DONGLE_TOUCH_BUTTON: DEB("M_DONGLE_TOUCH_BUTTON"); break;
			case M_DONGLE_TOUCH_MOVE: DEB("M_DONGLE_TOUCH_MOVE"); break;
			case M_DONGLE_CONSUMER: DEB("M_DONGLE_CONSUMER"); break;
		}
		DEB(":");
		for(int i=0; i < len; i++)
		{
			DEB(" %02x", *(uint8_t *)(p_data+i) );
		}
		if(rep_idx == M_DONGLE_TOUCH_MOVE)
		{
			int16_t x,y;
			from_hid_to_xy(p_data, &x,&y);
			DEB(" (%d,%d)",x,y);
		}
		DEB("\n");
		#endif
		
		
		// Add a packet to the TX FIFO to start the data transfer.
		// Next packets to send will be added.
		switch(rep_idx)
		{
			case M_DONGLE_KEY: 
				{
					TIME_TRACE_START();
					//rv = gzp_crypt_data_send(p_data, NRFR_KEYBOARD_PACKET_LENGTH);
					rv = gzp_crypt_data_send(p_data, len);
					TIME_TRACE_END(0);
				}
				break;

			case M_DONGLE_TOUCH_BUTTON:
				{ 
					m_dongle_packet[0] = NRFR_MOUSE_BUTTONS_REPORT_ID;
					memcpy((uint8_t *)&m_dongle_packet[1], p_data, len);

					m_timer_set(M_TIMER_ID_DONGLE_TIMEOUT, DONGLE_TX_WAIT_TIME);

					TIME_TRACE_START();
					// Wait in case the FIFOs are full
					while (!nrf_gzll_ok_to_add_packet_to_tx_fifo(NRFR_MOUSE_EP))
					{
						//m_mokibo_WDT_reload();
						if(m_timer_is_expired(M_TIMER_ID_DONGLE_TIMEOUT))
						{
							rv = NRF_ERROR_INTERNAL;
							#if defined(M_DONGLE_TRACE)	
								DEB("m_dongle_send: error\n");
							#endif
							return rv;
						}
					}
					TIME_TRACE_END(0);

					// Add mouse packet to the mouse pipe's TX FIFO.
					rv = nrf_gzll_add_packet_to_tx_fifo(NRFR_MOUSE_EP,
							m_dongle_packet,
							NRFR_MOUSE_BUTTONS_PACKET_LENGTH); // 4Bytes
				}
				break;

			case M_DONGLE_TOUCH_MOVE:
				{
					m_dongle_packet[0] = NRFR_MOUSE_MOV_REPORT_ID;
					memcpy((uint8_t *)&m_dongle_packet[1], p_data, len);

					m_timer_set(M_TIMER_ID_DONGLE_TIMEOUT, DONGLE_TX_WAIT_TIME);
					
					TIME_TRACE_START();
					// Wait in case the FIFOs are full.
					while (!nrf_gzll_ok_to_add_packet_to_tx_fifo(NRFR_MOUSE_EP))
					{
						//m_mokibo_WDT_reload();
						if(m_timer_is_expired(M_TIMER_ID_DONGLE_TIMEOUT))
						{
							rv = NRF_ERROR_INTERNAL;
							#if defined(M_DONGLE_TRACE)	
								DEB("m_dongle_send: error\n");
							#endif
							return rv;
						}
					}
					TIME_TRACE_END(0);

					// Add mouse packet to the mouse pipe's TX FIFO.
					rv = nrf_gzll_add_packet_to_tx_fifo(NRFR_MOUSE_EP,
							m_dongle_packet,
							NRFR_MOUSE_MOV_PACKET_LENGTH); // 4Bytes
				}
				break;

			case M_DONGLE_CONSUMER:
				{
					m_dongle_packet[0] = NRFR_ADVANCED_REPORT_ID;
					memcpy((uint8_t *)&m_dongle_packet[1], p_data, len);

					m_timer_set(M_TIMER_ID_DONGLE_TIMEOUT, DONGLE_TX_WAIT_TIME);
					TIME_TRACE_START();
					// Wait in case the FIFOs are full.
					while (!nrf_gzll_ok_to_add_packet_to_tx_fifo(NRFR_MOUSE_EP))
					{
						//m_mokibo_WDT_reload();
						if(m_timer_is_expired(M_TIMER_ID_DONGLE_TIMEOUT))
						{
							rv = NRF_ERROR_INTERNAL;
							#if defined(M_DONGLE_TRACE)	
								DEB("m_dongle_send: error\n");
							#endif
							return rv;
						}
					}
					TIME_TRACE_END(0);

					// Add mouse packet to the mouse pipe's TX FIFO.
					rv = nrf_gzll_add_packet_to_tx_fifo(NRFR_MOUSE_EP,
							m_dongle_packet,
							NRFR_ADVANCED_PACKET_LENGTH); // 2Bytes
				}
				break;

			default :
				#if defined(M_DONGLE_TRACE)	
					DEB("m_dongle_send: error\n");
				#endif
				break;
		}

		if (!rv)
		{
			host_id_received = false;

			#if defined(M_DONGLE_TRACE)	
				DEB("m_dongle_send: TX error \n");
			#endif

			if(system_addr_received == false)
			{
				rv = gzp_address_req_send();
				if(rv)
				{
					system_addr_received = true;
			
					#if defined(M_DONGLE_TRACE)	
						DEB("m_dongle_send: System Address recieved\n");
					#endif
				}
			}
			
			if( (system_addr_received == true) && (host_id_received == false) )
			{
				m_timer_set(M_TIMER_ID_DONGLE_TIMEOUT, DONGLE_PAIRING_TIME);
				
				TIME_TRACE_START();
				// Send "Host ID request". Needed for sending encrypted user data to the host.
				while (send_host_id_req() == GZP_ID_RESP_PENDING)
				{
					//m_mokibo_WDT_reload();
					if(m_timer_is_expired(M_TIMER_ID_DONGLE_TIMEOUT))
					{
						rv = NRF_ERROR_INTERNAL;
						#if defined(M_DONGLE_TRACE)	
							DEB("m_dongle_send: send_host_id_req() failed\n");
						#endif
						return rv;
					}
				}
				TIME_TRACE_END(0);
			
				#if defined(M_DONGLE_TRACE)	
					DEB("m_dongle_send: Host ID state:%d", host_id_received);
				#endif
			}

			rv = NRF_ERROR_INTERNAL;
		}
		else
		{
			rv = NRF_SUCCESS;
		}

		#if defined(M_DONGLE_TRACE)	
		if(NRF_SUCCESS != rv ) { DEB("m_dongle_send: TX len[%d] RepID[%d] Error[%d]", len, rep_idx, rv); }
		#endif
	}

	#if defined(M_DONGLE_TRACE)	
		DEB("m_dongle_send: END \n");	
	#endif

	return rv;
}

/*
================================================================================
   Function name : m_dongle_is_link_ok()
================================================================================
*/
bool m_dongle_is_link_ok(void)
{
	bool rv = false;

	if((true == host_id_received) && (true == system_addr_received))
	{
		rv = true;
	}
	
	return rv;
}

/*
================================================================================
   Function name : m_dongle_pairing_req()
================================================================================
*/
ret_code_t m_dongle_pairing_req(bool flag)
{
	ret_code_t rv = NRF_SUCCESS;

	#if defined(M_DONGLE_TRACE)	
	DEB("m_dongle_pairing_req: BEGIN\n");
	#endif
	
	if(flag)
	{		
		// Erase pairing data. This example is intended to demonstrate pairing after every reset.
		// See the gzp_desktop_emulator example for a demonstration on how to maintain pairing data between resets.
		gzp_erase_pairing_data();

		system_addr_received = false;
		host_id_received     = false;
	}
	else
		host_id_received     = false;

	if(system_addr_received == false)
	{
		// Send "system address request". 
		// to find closed dongle
		rv = gzp_address_req_send();
		if(rv)
		{
			system_addr_received = true;
		}
		else
		{
			rv = NRF_ERROR_INTERNAL;
			
			#if defined(M_DONGLE_TRACE)	
			DEB("m_dongle_pairing_req: gzp_address_req_send failed \n");
			#endif
		}
	}
			
	if( (system_addr_received == true) && (host_id_received == false) )
	{
		// Send "host id request to known dongle
		// try pairing with dongle.
		m_timer_set(M_TIMER_ID_DONGLE_TIMEOUT, DONGLE_PAIRING_TIME);
		
		TIME_TRACE_START();
		// Send "Host ID request". Needed for sending encrypted user data to the host.
		while (send_host_id_req() == GZP_ID_RESP_PENDING)
		{
			//m_mokibo_WDT_reload();
			if(m_timer_is_expired(M_TIMER_ID_DONGLE_TIMEOUT))
			{
				rv = NRF_ERROR_INTERNAL;
				#if defined(M_DONGLE_TRACE)	
				DEB("m_dongle_pairing_req: send_host_id_req failed \n");
				#endif

				return rv;
			}
		}
		TIME_TRACE_END(0);

		if(!host_id_received)
			rv = NRF_ERROR_INTERNAL;
	}

	if(system_addr_received)
	{
		gzp_get_system_addr(m_dongle_system_address);
			
		#if defined(M_DONGLE_TRACE)	
			DEB("m_dongle_pairing_req: System Address [0x%x] \n", m_dongle_system_address);
		#endif
	}
		
	if(host_id_received)
	{
		gzp_get_host_id(m_dongle_host_id);
		
		#if defined(M_DONGLE_TRACE)
			DEB("m_dongle_pairing_req: Host ID [0x%x] \n", m_dongle_host_id);
		#endif
	}

	#if defined(M_DONGLE_TRACE)
		DEB("m_dongle_pairing_req: system address(%d), host id(%d) \n", system_addr_received, host_id_received);
	#endif

	#if defined(M_DONGLE_TRACE)	
	DEB("m_dongle_pairing_req: END\n");
	#endif

	return rv;
}


/*
================================================================================
   Function name : m_dongle_handle_long_key()
================================================================================
*/
void m_dongle_handle_long_key(void)
{
	ret_code_t rv;
	
	uint8_t i, len, changed, any_key;
	uint8_t now[DRV_KEYBOARD_MAX_NUM_OF_ALL_KEYS] = {0, };

	static uint8_t saved[DRV_KEYBOARD_MAX_NUM_OF_ALL_KEYS] = {0, };

	drv_keyboard_get_packet(now, &len);

	for(i=0, changed = 0, any_key = 0; i<len; i++)
	{
		if(saved[i] != now[i])
		{
			// Store
			saved[i] = now[i];
		
			// Detect number of changed keys
			changed++;
		}

		// Is any key pressed?
		if(saved[i])
		{
			// Get number of pressed key
			any_key++;
		}
	}
	
	// detect long press with no change
	if((0==changed) && (0 != any_key))
	{
		// Long key?
		if(m_timer_is_expired(M_TIMER_ID_DONGLE_LONG_KEY))
		{
			// Set timer
			m_timer_set(M_TIMER_ID_DONGLE_LONG_KEY, DONGLE_LONG_KEY_TIME);

			// Send long key repeatedly!!!
			rv = m_dongle_send(M_DONGLE_KEY, saved, len); PRINT_IF_ERROR("long-key m_dongle_send()", rv);
			
			#if defined(M_DONGLE_TRACE)	
				DEB("m_dongle_handle_long_key: long-key m_dongle_send() ret=%d", rv);
			#endif
		}
	}
	else
	{
		// Refresh timer
		m_timer_set(M_TIMER_ID_DONGLE_LONG_KEY, DONGLE_LONG_KEY_TIME);
	}
}

#ifndef __GZP_CONFIG_H
/**@brief Callback function for Gazell Transmit Success. Adds new packet to tx fifo.
 */
void nrf_gzll_device_tx_success(uint32_t pipe, nrf_gzll_device_tx_info_t tx_info)
{
    uint8_t    dummy[NRF_GZLL_CONST_MAX_PAYLOAD_LENGTH];
    uint32_t   dummy_length = NRF_GZLL_CONST_MAX_PAYLOAD_LENGTH;
	
    if (tx_info.payload_received_in_ack)
    {
        // if ack was sent with payload, pop them from rx fifo.
        GAZELLE_ERROR_CODE_CHECK(nrf_gzll_fetch_packet_from_rx_fifo(pipe, dummy, &dummy_length));
    }
}

/**@brief Callback function for Gazell Transmit fail. Resends the current packet.
 */
void nrf_gzll_device_tx_failed(uint32_t pipe, nrf_gzll_device_tx_info_t tx_info)
{
    uint8_t  dummy[NRF_GZLL_CONST_MAX_PAYLOAD_LENGTH];
    uint32_t dummy_length = NRF_GZLL_CONST_MAX_PAYLOAD_LENGTH;

	#if defined(M_DONGLE_TRACE)	
		DEB("nrf_gzll_device_tx_failed: Gazell transmission failed");
	#endif
	
    if (tx_info.payload_received_in_ack)
    {
        // if ack was sent with payload, pop them from rx fifo.
        GAZELLE_ERROR_CODE_CHECK(nrf_gzll_fetch_packet_from_rx_fifo(pipe, dummy, &dummy_length));
    }
}


/**@brief Callback function for Gazell Receive Data Ready. Flushes the receive's FIFO.
 */
void nrf_gzll_host_rx_data_ready(uint32_t pipe, nrf_gzll_host_rx_info_t rx_info)
{
    // We dont expect to receive any data in return, but if it happens we flush the RX fifo.
    GAZELLE_ERROR_CODE_CHECK(nrf_gzll_flush_rx_fifo(pipe));
}


/**@brief Callback function for Gazell Disabled - Not needed in this example.
 */
void nrf_gzll_disabled()
{
}
#endif
// End of file 
