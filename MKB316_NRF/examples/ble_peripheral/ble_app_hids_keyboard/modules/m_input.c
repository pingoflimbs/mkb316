/// \file m_input.c
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 27-Oct-2017  MJ.Kim
* + Created initial version
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/

/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/
#include "sdk_config.h" // mokibo_config.h && sdk_config.h
#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
	// should be placed after "sdk_config.h"
	#include "SEGGER_RTT.h"
	#include "m_timer.h"
#endif
#include "sdk_common.h"
#include "app_error.h"
#include "nrf_log.h"
#include "m_input.h"


/*============================================================================*/
/*                              CLASS VARIABLES                               */
/*============================================================================*/
/// Timer pool to management time
static M_INPUT_STATUS_TYPE m_inputs[M_INPUT_ID_MAX] = {M_INPUT_STATUS_OFF, };


/*============================================================================*/
/*                              LOCAL FUNCTIONS                               */
/*============================================================================*/

/*============================================================================*/
/*                              GLOBAL FUNCTIONS                              */
/*============================================================================*/
/*
================================================================================
   Function name : m_input_init()
================================================================================
*/
ret_code_t m_input_init(void)
{
	#ifdef	M_INPUT_TRACE
	DEBUG("Called");
	#endif

	// TODO: Add led driver initialize here
    return NRF_SUCCESS;
}

/*
================================================================================
   Function name : m_input_get()
================================================================================
*/
M_INPUT_STATUS_TYPE m_input_get(M_INPUT_ID_TYPE id)
{
	#ifdef	M_INPUT_TRACE
	DEBUG("[%d:%d]", id, m_inputs[id]);
	#endif
	
	return m_inputs[id];
}

/*
================================================================================
   Function name : m_input_update()
================================================================================
*/
void m_input_update(void)
{
	// TODO: read on/off input status by driver here 
	;
}


// End of file 
