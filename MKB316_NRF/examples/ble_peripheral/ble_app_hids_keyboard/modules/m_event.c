/// \file m_event.c
/// This file contains all required configurations for mokibo
/*==============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
==============================================================================*/




/*\      $$\  $$$$$$\  $$$$$$$\  $$\   $$\ $$\       $$$$$$$$\  $$$$$$\  
$$$\    $$$ |$$  __$$\ $$  __$$\ $$ |  $$ |$$ |      $$  _____|$$  __$$\ 
$$$$\  $$$$ |$$ /  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$ /  \__|
$$\$$\$$ $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$$$$\    \$$$$$$\  
$$ \$$$  $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$  __|    \____$$\ 
$$ |\$  /$$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$\   $$ |
$$ | \_/ $$ | $$$$$$  |$$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$$\ \$$$$$$  |
\__|     \__| \______/ \_______/  \______/ \________|\________| \______/ 
$$\   $$\  $$$$$$\  $$$$$$$$\ $$$$$$$\                                   
$$ |  $$ |$$  __$$\ $$  _____|$$  __$$\                                  
$$ |  $$ |$$ /  \__|$$ |      $$ |  $$ |                                 
$$ |  $$ |\$$$$$$\  $$$$$\    $$ |  $$ |                                 
$$ |  $$ | \____$$\ $$  __|   $$ |  $$ |                                 
$$ |  $$ |$$\   $$ |$$ |      $$ |  $$ |                                 
\$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$  |                                 
 \______/  \______/ \________|\_______/                                  
//============================================================================//
//                                 MODULSE USED                               //
//============================================================================*/
#include "sdk_config.h" // mokibo_config.h && sdk_config.h
#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
	// should be placed after "sdk_config.h"
	#include "SEGGER_RTT.h"
#endif

#include "sdk_common.h"
#include "app_error.h"
#include "nrf_log.h"
#include "nrf_delay.h"

#include "drv_stm8.h"
#include "drv_fds.h"
#include "drv_keyboard.h"        

#include "m_mokibo.h"
#include "m_timer.h"
#include "m_comm.h"
#include "m_led.h"        
#include "m_util.h"
#include "m_touch.h"  
#include "m_event.h"





/*\       $$$$$$\   $$$$$$\   $$$$$$\  $$\                                               
$$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                                              
$$ |     $$ /  $$ |$$ /  \__|$$ /  $$ |$$ |                                              
$$ |     $$ |  $$ |$$ |      $$$$$$$$ |$$ |                                              
$$ |     $$ |  $$ |$$ |      $$  __$$ |$$ |                                              
$$ |     $$ |  $$ |$$ |  $$\ $$ |  $$ |$$ |                                              
$$$$$$$$\ $$$$$$  |\$$$$$$  |$$ |  $$ |$$$$$$$$\                                         
\________|\______/  \______/ \__|  \__|\________|                                        
 $$$$$$\   $$$$$$\  $$\   $$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$\   $$\ $$$$$$$$\  $$$$$$\  
$$  __$$\ $$  __$$\ $$$\  $$ |$$  __$$\\__$$  __|$$  __$$\ $$$\  $$ |\__$$  __|$$  __$$\ 
$$ /  \__|$$ /  $$ |$$$$\ $$ |$$ /  \__|  $$ |   $$ /  $$ |$$$$\ $$ |   $$ |   $$ /  \__|
$$ |      $$ |  $$ |$$ $$\$$ |\$$$$$$\    $$ |   $$$$$$$$ |$$ $$\$$ |   $$ |   \$$$$$$\  
$$ |      $$ |  $$ |$$ \$$$$ | \____$$\   $$ |   $$  __$$ |$$ \$$$$ |   $$ |    \____$$\ 
$$ |  $$\ $$ |  $$ |$$ |\$$$ |$$\   $$ |  $$ |   $$ |  $$ |$$ |\$$$ |   $$ |   $$\   $$ |
\$$$$$$  | $$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$ |  $$ |$$ | \$$ |   $$ |   \$$$$$$  |
 \______/  \______/ \__|  \__| \______/   \__|   \__|  \__|\__|  \__|   \__|    \______/ 
//============================================================================//
//                                 LOCAL CONSTANTS                            //
//============================================================================*/





/*\       $$$$$$\   $$$$$$\   $$$$$$\  $$\                                        
$$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                                       
$$ |     $$ /  $$ |$$ /  \__|$$ /  $$ |$$ |                                       
$$ |     $$ |  $$ |$$ |      $$$$$$$$ |$$ |                                       
$$ |     $$ |  $$ |$$ |      $$  __$$ |$$ |                                       
$$ |     $$ |  $$ |$$ |  $$\ $$ |  $$ |$$ |                                       
$$$$$$$$\ $$$$$$  |\$$$$$$  |$$ |  $$ |$$$$$$$$\                                  
\________|\______/  \______/ \__|  \__|\________|                                 
$$$$$$$$\ $$\     $$\ $$$$$$$\  $$$$$$$$\ $$$$$$$\  $$$$$$$$\ $$$$$$$$\  $$$$$$\  
\__$$  __|\$$\   $$  |$$  __$$\ $$  _____|$$  __$$\ $$  _____|$$  _____|$$  __$$\ 
   $$ |    \$$\ $$  / $$ |  $$ |$$ |      $$ |  $$ |$$ |      $$ |      $$ /  \__|
   $$ |     \$$$$  /  $$$$$$$  |$$$$$\    $$ |  $$ |$$$$$\    $$$$$\    \$$$$$$\  
   $$ |      \$$  /   $$  ____/ $$  __|   $$ |  $$ |$$  __|   $$  __|    \____$$\ 
   $$ |       $$ |    $$ |      $$ |      $$ |  $$ |$$ |      $$ |      $$\   $$ |
   $$ |       $$ |    $$ |      $$$$$$$$\ $$$$$$$  |$$$$$$$$\ $$ |      \$$$$$$  |
   \__|       \__|    \__|      \________|\_______/ \________|\__|       \______/ 
//============================================================================//
//                                 LOCAL TYPEDEFS                             //
//============================================================================*/
typedef enum
{
    SUB_STATUS_READY,
    SUB_STATUS_RUN,
} sub_status_type_t;

typedef struct
{
    uint8_t  prv 		 		: 1;
    uint8_t  now 		 		: 1;
    uint8_t  rsvd 		 		: 6;
} input_bf_t; // input bitfield

typedef union 
{
	input_bf_t 	bit;
	uint8_t     data;
} input_t;

typedef enum
{
    input_left_button,
    input_right_button,
    input_logical_touch,
	//
    input_type_max,
} input_type_t;







/*\       $$$$$$\   $$$$$$\   $$$$$$\  $$\                                               
$$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                                              
$$ |     $$ /  $$ |$$ /  \__|$$ /  $$ |$$ |                                              
$$ |     $$ |  $$ |$$ |      $$$$$$$$ |$$ |                                              
$$ |     $$ |  $$ |$$ |      $$  __$$ |$$ |                                              
$$ |     $$ |  $$ |$$ |  $$\ $$ |  $$ |$$ |                                              
$$$$$$$$\ $$$$$$  |\$$$$$$  |$$ |  $$ |$$$$$$$$\                                         
\________|\______/  \______/ \__|  \__|\________|                                        
$$\    $$\  $$$$$$\  $$$$$$$\  $$$$$$\  $$$$$$\  $$$$$$$\  $$\       $$$$$$$$\  $$$$$$\  
$$ |   $$ |$$  __$$\ $$  __$$\ \_$$  _|$$  __$$\ $$  __$$\ $$ |      $$  _____|$$  __$$\ 
$$ |   $$ |$$ /  $$ |$$ |  $$ |  $$ |  $$ /  $$ |$$ |  $$ |$$ |      $$ |      $$ /  \__|
\$$\  $$  |$$$$$$$$ |$$$$$$$  |  $$ |  $$$$$$$$ |$$$$$$$\ |$$ |      $$$$$\    \$$$$$$\  
 \$$\$$  / $$  __$$ |$$  __$$<   $$ |  $$  __$$ |$$  __$$\ $$ |      $$  __|    \____$$\ 
  \$$$  /  $$ |  $$ |$$ |  $$ |  $$ |  $$ |  $$ |$$ |  $$ |$$ |      $$ |      $$\   $$ |
   \$  /   $$ |  $$ |$$ |  $$ |$$$$$$\ $$ |  $$ |$$$$$$$  |$$$$$$$$\ $$$$$$$$\ \$$$$$$  |
    \_/    \__|  \__|\__|  \__|\______|\__|  \__|\_______/ \________|\________| \______/ 
//============================================================================//
//                                 LOCAL VARIABLES                            //
//============================================================================*/

m_event_arg_t m_arg;
m_event_type_t m_event;
char m_version_message[256]= "";

static sub_status_type_t m_sub_status;
static input_t m_input[input_type_max];
static m_mokibo_status_type_t m_my_status;
static uint8_t 	m_touch_lock_detect;










/*\       $$$$$$\   $$$$$$\   $$$$$$\  $$\                                      
$$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                                     
$$ |     $$ /  $$ |$$ /  \__|$$ /  $$ |$$ |                                     
$$ |     $$ |  $$ |$$ |      $$$$$$$$ |$$ |                                     
$$ |     $$ |  $$ |$$ |      $$  __$$ |$$ |                                     
$$ |     $$ |  $$ |$$ |  $$\ $$ |  $$ |$$ |                                     
$$$$$$$$\ $$$$$$  |\$$$$$$  |$$ |  $$ |$$$$$$$$\                                
\________|\______/  \______/ \__|  \__|\________|                               
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\  $$\   $$\ $$$$$$$$\  $$$$$$\  $$$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\ $$ |  $$ |$$  _____|$$  __$$\ $$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|$$ |  $$ |$$ |      $$ /  $$ |$$ |  $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |      $$$$$$$$ |$$$$$\    $$$$$$$$ |$$ |  $$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |      $$  __$$ |$$  __|   $$  __$$ |$$ |  $$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\ $$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |$$ |  $$ |$$$$$$$$\ $$ |  $$ |$$$$$$$  |
\__|       \______/ \__|  \__| \______/ \__|  \__|\________|\__|  \__|\_______/ */
/*============================================================================*/
/*                       PROTOTYPES OF LOCAL FUNCTIONS                        */
/*============================================================================*/
static void scan_clickbar_touch(void);
static void scan_clickbar_button(void);
static void update_status(void);
static void reset_mokibo(void);



/*$$$$$\   $$$$$$\  $$$$$$\ $$\   $$\ $$$$$$$$\ $$$$$$$$\ $$$$$$$\                     
$$  __$$\ $$  __$$\ \_$$  _|$$$\  $$ |\__$$  __|$$  _____|$$  __$$\                    
$$ |  $$ |$$ /  $$ |  $$ |  $$$$\ $$ |   $$ |   $$ |      $$ |  $$ |                   
$$$$$$$  |$$ |  $$ |  $$ |  $$ $$\$$ |   $$ |   $$$$$\    $$$$$$$  |                   
$$  ____/ $$ |  $$ |  $$ |  $$ \$$$$ |   $$ |   $$  __|   $$  __$$<                    
$$ |      $$ |  $$ |  $$ |  $$ |\$$$ |   $$ |   $$ |      $$ |  $$ |                   
$$ |       $$$$$$  |$$$$$$\ $$ | \$$ |   $$ |   $$$$$$$$\ $$ |  $$ |                   
\__|       \______/ \______|\__|  \__|   \__|   \________|\__|  \__|                   
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\  $$\   $$\ $$$$$$$$\  $$$$$$\  $$$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\ $$ |  $$ |$$  _____|$$  __$$\ $$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|$$ |  $$ |$$ |      $$ /  $$ |$$ |  $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |      $$$$$$$$ |$$$$$\    $$$$$$$$ |$$ |  $$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |      $$  __$$ |$$  __|   $$  __$$ |$$ |  $$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\ $$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |$$ |  $$ |$$$$$$$$\ $$ |  $$ |$$$$$$$  |
\__|       \______/ \__|  \__| \______/ \__|  \__|\________|\__|  \__|\_______/ 
//============================================================================//
//                                POINTER FUNCTIONS                           //
//============================================================================*/
typedef void (*STATE_MACHINE)(void);
static void init_event_detect(void);
static void manufacture_event_detect(void);
static void keyboard_event_detect(void);
static void touch_event_detect(void);
static void upgrade_event_detect(void);
static void sleep_event_detect(void);
static void test_event_detect(void);
static void battery_check_event_detect(void);
static void end_event_detect(void);
static const STATE_MACHINE 		m_event_handler[M_MOKIBO_MAX_STATUS] = \
{ \
    init_event_detect, \
    manufacture_event_detect, \
    keyboard_event_detect, \
    touch_event_detect, \
    upgrade_event_detect, \
    sleep_event_detect, \
    test_event_detect, \
	battery_check_event_detect, \
    end_event_detect, \
};




/*$$$$$\  $$$$$$$$\ $$$$$$$\  $$\   $$\  $$$$$$\                                       
$$  __$$\ $$  _____|$$  __$$\ $$ |  $$ |$$  __$$\                                      
$$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |$$ /  \__|                                     
$$ |  $$ |$$$$$\    $$$$$$$\ |$$ |  $$ |$$ |$$$$\                                      
$$ |  $$ |$$  __|   $$  __$$\ $$ |  $$ |$$ |\_$$ |                                     
$$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |$$ |  $$ |                                     
$$$$$$$  |$$$$$$$$\ $$$$$$$  |\$$$$$$  |\$$$$$$  |                                     
\_______/ \________|\_______/  \______/  \______/          
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\  $$\   $$\ $$$$$$$$\  $$$$$$\  $$$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\ $$ |  $$ |$$  _____|$$  __$$\ $$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|$$ |  $$ |$$ |      $$ /  $$ |$$ |  $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |      $$$$$$$$ |$$$$$\    $$$$$$$$ |$$ |  $$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |      $$  __$$ |$$  __|   $$  __$$ |$$ |  $$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\ $$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |$$ |  $$ |$$$$$$$$\ $$ |  $$ |$$$$$$$  |
\__|       \______/ \__|  \__| \______/ \__|  \__|\________|\__|  \__|\_______/ 
//============================================================================//
//                               DEBUG FUNCTIONS                              //
//============================================================================*/
uint8_t m_cert_mode_enabled=0;
static void DoDebugByHotkey(uint32_t hoykey);

#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
	static void debug(void);
	static void debug2(void);
	static void debug3(void);
#endif




/*\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ 
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\_*/

















/*
 $$$$$$\  $$\       $$$$$$\  $$$$$$$\   $$$$$$\  $$\                                   
$$  __$$\ $$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                                  
$$ /  \__|$$ |     $$ /  $$ |$$ |  $$ |$$ /  $$ |$$ |                                  
$$ |$$$$\ $$ |     $$ |  $$ |$$$$$$$\ |$$$$$$$$ |$$ |                                  
$$ |\_$$ |$$ |     $$ |  $$ |$$  __$$\ $$  __$$ |$$ |                                  
$$ |  $$ |$$ |     $$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |                                  
\$$$$$$  |$$$$$$$$\ $$$$$$  |$$$$$$$  |$$ |  $$ |$$$$$$$$\                             
 \______/ \________|\______/ \_______/ \__|  \__|\________|                            
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\  $$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |$$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |$$ /  \__|
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |\$$$$$$\  
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ | \____$$\ 
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |$$\   $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |\$$$$$$  |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__| \______/ 
//============================================================================//
//                                 GLOBAL FUNCTIONS                           //
//============================================================================*/

//================================================================================
//			m_event_init
//================================================================================
ret_code_t m_event_init(void)
{	
	________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(void)");
	m_event = M_EVENT_NONE;

	m_touch_lock_detect = 0;

	memset(&m_arg, 0, sizeof(m_arg));
	memset(m_input, 0, sizeof(m_input));

    return NRF_SUCCESS;
}



//================================================================================
//			m_event_get
//================================================================================
void m_event_get(m_event_type_t *p_evt, m_event_arg_t *p_argv)
{
	________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(m_event_type_t *p_evt, m_event_arg_t *p_argv)");
	*p_evt 	= m_event;
	*p_argv = m_arg;

	#if defined(M_EVENT_TRACE)
	// TODO: Use stringizing(#) of preprocessor macros
	static const char *String[M_EVENT_MAX] = \
	{ \
		"NONE", \
		"TOUCH_MODE_SCAN_TIME", \
		"KEYBOAD_MODE_SCAN_TIME", \
		"STARTUP", \
		"MODE_CHANGED", \
		"TOUCH_MODE_CHANGED", \
		"CLICKBAR_BUTTON_CHANGED", \
		"CHANNEL_CHANGED", \
		"VOLUME_CHANGED", \
		"NEW_BOND", \
		"SPECIAL_KEY", \
	};

	for(uint32_t i=0; i<M_EVENT_MAX; i++)
	{
		if(*p_evt & NBIT(i))
		{
			// Too much to show
			if(!(NBIT(i) & (M_EVENT_KEYBOAD_MODE_SCAN_TIME 
							| M_EVENT_TOUCH_MODE_SCAN_TIME)))
			{
				DEB("m_event_get(): %s is received[%d]", String[i+1], i+1);
			}
		}
	}
	#endif
	
	// Cleared after reading!!
	// TODO??: buffer for multiple event ==> currently bit feild, up to 32 event
	m_event = M_EVENT_NONE;
	memset(&m_arg, 0, sizeof(m_arg));
}




//================================================================================
//			m_event_update
//================================================================================
bool m_event_update(void)
{   
	________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(void)");
	m_event = M_EVENT_NONE;

	update_status();

	m_event_handler[m_my_status]();

	return (M_EVENT_NONE != m_event) ? 1 : 0;
}



//================================================================================
//			m_event_get_button
//================================================================================
void m_event_get_button(m_event_clickbar_buttons_t *pBtn)
{
	________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(m_event_clickbar_buttonns_t)");
	pBtn->left 	= m_input[input_left_button].bit.now ? 1 : 0;
	pBtn->right	= m_input[input_right_button].bit.now ? 1 : 0;
}













/*\       $$$$$$\   $$$$$$\   $$$$$$\  $$\                                             
$$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                                            
$$ |     $$ /  $$ |$$ /  \__|$$ /  $$ |$$ |                                            
$$ |     $$ |  $$ |$$ |      $$$$$$$$ |$$ |                                            
$$ |     $$ |  $$ |$$ |      $$  __$$ |$$ |                                            
$$ |     $$ |  $$ |$$ |  $$\ $$ |  $$ |$$ |                                            
$$$$$$$$\ $$$$$$  |\$$$$$$  |$$ |  $$ |$$$$$$$$\                                       
\________|\______/  \______/ \__|  \__|\________|                                      
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\  $$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |$$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |$$ /  \__|
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |\$$$$$$\  
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ | \____$$\ 
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |$$\   $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |\$$$$$$  |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__| \______/ 
//============================================================================//
//                                 LOCAL FUNCTIONS                            //
//============================================================================*/

//================================================================================
//	Function name : scan_clickbar_touch()
//================================================================================
static void scan_clickbar_touch(void)
{	
	________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(void)");
	STM8_TOUCH_STATUS_t center_left, center_right, touch_left, touch_right;				
	
	center_left=center_right=touch_left=touch_right=0;
	

	
	#if 1
//충전기 체크
		if(m_timer_is_expired(M_TIMER_ID_CHARGER_CHECK))
		{
			
			static uint8_t oldBtrState=0;
			uint8_t curBtrState = 0;
			m_util_get_battery_status(&curBtrState);
			

			/*
			drv_stm8_read(STM8_READ_BATTERY_STATUS, &getMsg);
			
			drv_stm8_read(STM8_READ_LED_STATUS, &ledStatus);
			drv_stm8_read(STM8_READ_LED_COLOR, &ledColor);
						

			
			getMsg = getMsg & (1<<4);


			
			*/

			if(curBtrState != oldBtrState)
			{
				oldBtrState = curBtrState;
				m_event |= M_EVENT_MODE_CHANGED;
				m_arg.new_mode.battery_chk = 1;
			}
			m_timer_set(M_TIMER_ID_CHARGER_CHECK,M_TIMER_TIME_100MS(5));
		}
	
	#endif

	
	#if 0
//충전기 체크
		if(m_timer_is_expired(M_TIMER_ID_CHARGER_CHECK))
		{
			static uint8_t oldBtrState=0;
			uint8_t getMsg=0;
			uint8_t ledStatus=0;
			uint8_t ledColor=0;
			drv_stm8_read(STM8_READ_BATTERY_STATUS, &getMsg);
			
			drv_stm8_read(STM8_READ_LED_STATUS, &ledStatus);
			drv_stm8_read(STM8_READ_LED_COLOR, &ledColor);
			
			
			getMsg = getMsg & (1<<4);


			if(getMsg != oldBtrState)
			{
				uint8_t oldtype; //= m_led_get_status();
				uint8_t oldcolor =0;// m_led_get_color();
				
				drv_stm8_read(STM8_READ_LED_STATUS, &oldtype);
				nrf_delay_ms(20);
				drv_stm8_read(STM8_READ_LED_COLOR, &oldcolor);
				nrf_delay_ms(20);

				drv_stm8_write(STM8_WRITE_LED_STATUS, M_LED_OFF);
				nrf_delay_ms(20);
				drv_stm8_write(STM8_WRITE_LED_COLOR, M_LED_COLOR_WHITE);								
				nrf_delay_ms(20);
				drv_stm8_write(STM8_WRITE_LED_STATUS, M_LED_DIMMING_OUT);

				//drv_stm8_write(STM8_WRITE_LED_STATUS, M_LED_OFF);
				nrf_delay_ms(20);
				drv_stm8_write(STM8_WRITE_LED_COLOR, (M_LED_COLOR_TYPE_t)oldcolor);
				


				//m_led_set_color(oldcolor);
				//m_led_set_status(oldtype);

				oldBtrState = getMsg;
			}

			m_timer_set(M_TIMER_ID_CHARGER_CHECK,M_TIMER_TIME_100MS(5));
		}
	
	#endif

	
	#if 1
	//에러터치 입력 필터
		STM8_TOUCH_STATUS_t center_left_tmp=0;
		STM8_TOUCH_STATUS_t center_right_tmp=0;
		static uint8_t counter=0;

		drv_stm8_read(STM8_READ_TOUCH_CENTER_LEFT, &center_left_tmp);
		drv_stm8_read(STM8_READ_TOUCH_CENTER_RIGHT, &center_right_tmp);

		if(center_left_tmp || center_right_tmp)
		{
			________________________________LOG_mEvent_RecvClickBar(6,"ErrorTouch[%d|%d],Cnt++:%d",center_left_tmp,center_right_tmp,counter);
			counter = 128 <= counter ? 128 : counter+2;
		}
		else
		{
			________________________________LOG_mEvent_RecvClickBar(6,"ErrorTouch[%d|%d],Cnt--:%d",center_left_tmp,center_right_tmp,counter);
			counter = 8 <= counter ? counter-8 : 0;
		}

		
		if(100 <= counter && (center_left_tmp || center_right_tmp))
		{
			center_left = center_left_tmp;
			center_right = center_right_tmp;
		}

	#else

		drv_stm8_read(STM8_READ_TOUCH_CENTER_LEFT, &center_left);
		drv_stm8_read(STM8_READ_TOUCH_CENTER_RIGHT, &center_right);

	#endif

	if(center_left || center_right)
	{		
		________________________________LOG_mEvent_RecvClickBar(6,"ErrorTouch[%d|%d]",center_left,center_right);
		m_input[input_logical_touch].bit.now = 0;		
	}
	else
	{
		drv_stm8_read(STM8_READ_TOUCH_LEFT, &touch_right); // <-- left/right 바꿈
		drv_stm8_read(STM8_READ_TOUCH_RIGHT,&touch_left); // <-- left/right 바꿈		
				
		if(touch_left || touch_right)
		{
			m_input[input_logical_touch].bit.now = 1;
		}
		else
		{
			m_input[input_logical_touch].bit.now = 0;
		}
	}
		
	//if(m_input[input_logical_touch].bit.prv != m_input[input_logical_touch].bit.now)	
	if(m_input[input_logical_touch].bit.prv != (touch_left|touch_right) )
	{					
		________________________________LOG_mEvent_RecvClickBar(6,"ClickBarTouch:[%d|%d]",touch_left, touch_right);
		m_event |= M_EVENT_TOUCH_MODE_CHANGED;

		//if(m_input[input_logical_touch].bit.now)
		if( touch_left|touch_right )
		{
			m_arg.touch_mode.enter = 1;
		}
		else
		{
			m_arg.touch_mode.exit = 1;
		}
		// remember the change
		//m_input[input_logical_touch].bit.prv = m_input[input_logical_touch].bit.now;		
		m_input[input_logical_touch].bit.prv = (touch_left|touch_right);				
	}
}






//================================================================================
//	Function name : scan_clickbar_button()
//================================================================================
static void scan_clickbar_button(void)
{
	________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(void)");
	STM8_BUTTON_STATUS_t button_right;
	STM8_BUTTON_STATUS_t button_left;
	
	drv_stm8_read(STM8_READ_BUTTON_LEFT, &button_left);
	drv_stm8_read(STM8_READ_BUTTON_RIGHT, &button_right);
	
	m_input[input_left_button].bit.now = (STM8_BUTTON_ON == button_left) ? 1 : 0;
	m_input[input_right_button].bit.now = (STM8_BUTTON_ON == button_right) ? 1 : 0;	
	
	if(m_input[input_left_button].bit.prv != m_input[input_left_button].bit.now || m_input[input_right_button].bit.prv != m_input[input_right_button].bit.now)
	{
		________________________________LOG_mEvent_RecvClickBar(6,"ClickBarClick:[%d|%d] Recognized",button_left,button_right);

		if(m_input[input_left_button].bit.prv != m_input[input_left_button].bit.now)
		{					
			if(m_input[input_left_button].bit.now)
			{			
				m_event |= M_EVENT_CLICKBAR_BUTTON_CHANGED;
				m_arg.button.left= 1;
			} 
			else 
			{		
				m_event |= M_EVENT_CLICKBAR_BUTTON_CHANGED;
				m_arg.button.left= 0;
			}		
			m_input[input_left_button].bit.prv = m_input[input_left_button].bit.now;
		}

		if(m_input[input_right_button].bit.prv != m_input[input_right_button].bit.now)
		{		
			if(m_input[input_right_button].bit.now)
			{			
				m_event |= M_EVENT_CLICKBAR_BUTTON_CHANGED;
				m_arg.button.right= 1; // to indicate which button was released...
			} 
			else 
			{						
				m_event |= M_EVENT_CLICKBAR_BUTTON_CHANGED;
				m_arg.button.right= 0; //
			}				
			m_input[input_right_button].bit.prv = m_input[input_right_button].bit.now;		
		}
	}


		
#if 1 // 터치락 버튼 추가로 클릭으로 유도되는 슬립
	//-----------------------------------------------------------------------------------------------------------------------------------
	// Press two buttons for touch locking mode 
	//TOUCH_LOCK_PRESS_TIME -> SLEEP_PRESS_TIME
	if((0 == m_input[input_left_button].bit.now) || (0 == m_input[input_right_button].bit.now))
	{
		m_timer_set(M_TIMER_ID_TOUCH_LOCK, TOUCH_LOCK_PRESS_TIME);
	}
	//-----------------------------------------------------------------------------------------------------------------------------------
	// Hold press left & right key during TOUCH_LOCK_PRESS_TIME
	if(m_timer_is_expired(M_TIMER_ID_TOUCH_LOCK))
	{
		m_arg.new_mode.sleep = 1;
		m_event |= M_EVENT_MODE_CHANGED;
		
		// Refresh timer
		m_timer_set(M_TIMER_ID_TOUCH_LOCK, TOUCH_LOCK_PRESS_TIME);
	
	}

#endif
}



//================================================================================
//	Function name : update_status()
//================================================================================
static void update_status(void)
{
	________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(void)");
	m_mokibo_status_type_t new = m_mokibo_get_status();
	
	
	if(new != m_my_status)
	{
		________________________________LOG_mEvent_eventStatus(0,"m_my_status:[%d -> %d] (0:init,1:manu,2:kbd,3:tch,4:dfu,5:slp,6:tst,7:btr,8:end)",m_my_status, new);
		m_my_status 	= new;
		m_sub_status 	= SUB_STATUS_READY;
	}
}


//================================================================================
//	Function name : reset_mokibo()
//================================================================================
static void reset_mokibo(void)
{
	________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(void)");
	DEB("RESET DRV_STM8!!");
	drv_stm8_write(STM8_WRITE_FW_CMD, 3);
	DEBUG("RESET DRV_STM8!!");

	#if (MOKIBO_WDT_ENABLED)
	m_mokibo_WDT_reset(1);						// Reset by Watchdog
	#else
	m_mokibo_soft_reset(0xA3A3);				// Soft Reset
	//drv_stm8_write(STM8_WRITE_RESET_ME, 2); 	// Power-on Reset by STM8 
	#endif
}

















/*$$$$$\   $$$$$$\  $$$$$$\ $$\   $$\ $$$$$$$$\ $$$$$$$$\ $$$$$$$\                     
$$  __$$\ $$  __$$\ \_$$  _|$$$\  $$ |\__$$  __|$$  _____|$$  __$$\                    
$$ |  $$ |$$ /  $$ |  $$ |  $$$$\ $$ |   $$ |   $$ |      $$ |  $$ |                   
$$$$$$$  |$$ |  $$ |  $$ |  $$ $$\$$ |   $$ |   $$$$$\    $$$$$$$  |                   
$$  ____/ $$ |  $$ |  $$ |  $$ \$$$$ |   $$ |   $$  __|   $$  __$$<                    
$$ |      $$ |  $$ |  $$ |  $$ |\$$$ |   $$ |   $$ |      $$ |  $$ |                   
$$ |       $$$$$$  |$$$$$$\ $$ | \$$ |   $$ |   $$$$$$$$\ $$ |  $$ |                   
\__|       \______/ \______|\__|  \__|   \__|   \________|\__|  \__|                   
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\  $$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |$$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |$$ /  \__|
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |\$$$$$$\  
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ | \____$$\ 
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |$$\   $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |\$$$$$$  |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__| \______/ 
//============================================================================//
//                       POINTER FUCTIONS				                      //
//============================================================================*/

//================================================================================
//	Function name : init_event_detect() 	//RelatedTo: InitHandler
//================================================================================
static void init_event_detect(void)
{
	________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(void)");
	uint32_t hoykey;
	
	switch(m_sub_status)
	{
		case SUB_STATUS_READY :
			m_timer_set(M_TIMER_ID_EVENT_MODULE, INIT_WAIT_TIME);
			//
			m_timer_set(M_TIMER_ID_KEYBOARD_SCAN, KEYBOARD_SCAN_TIME);	// KEYBOARD_SCAN: 30ms
			// KEYBOARD_SCAN_TIME_SHIFT: for load balancing
			m_timer_set(M_TIMER_ID_KEYBOARD_SCAN, KEYBOARD_SCAN_TIME+KEYBOARD_SCAN_TIME_SHIFT); // 30ms + 5ms
		
			//
			m_event |= M_EVENT_STARTUP; //<---
			m_sub_status = SUB_STATUS_RUN;
			break;

		case SUB_STATUS_RUN :
			//
			if(m_timer_is_expired(M_TIMER_ID_TOUCH_SCAN))
			{
				m_timer_set(M_TIMER_ID_TOUCH_SCAN, TOUCH_SCAN_TIME);
				m_event |= M_EVENT_TOUCH_MODE_SCAN_TIME;
			}
			//
			if(m_timer_is_expired(M_TIMER_ID_KEYBOARD_SCAN))
			{
				m_timer_set(M_TIMER_ID_KEYBOARD_SCAN, KEYBOARD_SCAN_TIME);
				m_event |= M_EVENT_KEYBOAD_MODE_SCAN_TIME;
			}

			// TODO: define how to enter manufacture mode here
			// If timer is expired, goto keyboard mode
			if(m_timer_is_expired(M_TIMER_ID_EVENT_MODULE))
			{
				m_event |= M_EVENT_MODE_CHANGED;
				m_arg.new_mode.keyboard = 1;
			}
			
			hoykey = drv_keyboard_get_hotkey();
			
			if(hoykey & DRV_KEYBOARD_HOTKEY_TEST_ENTER)
			{
				m_event |= M_EVENT_MODE_CHANGED;
				m_arg.new_mode.test = 1;
			}
	
			if(hoykey & DRV_KEYBOARD_HOTKEY_ERASE_BOND)
			{
				m_event |= M_EVENT_NEW_BOND;
				m_arg.bond.all = 1;
			}
		break;

		default :
			DEBUG("OOPS!-init_event_detect");
		break;
	}

	#if defined(M_EVENT_STATUS_TRACE)
	DEBUG("Called");
	#endif
}





//================================================================================
//	Function name : manufacture_event_detect()
//================================================================================
static void manufacture_event_detect(void)
{
	________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(void)");
	#if defined(M_EVENT_STATUS_TRACE)
	DEBUG("Called");
	#endif
}




//================================================================================
//	Function name : keyboard_event_detect()
//================================================================================
static void keyboard_event_detect(void)
{
	________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(void)");
	uint32_t hoykey;
	
	switch(m_sub_status)
	{
		case SUB_STATUS_READY :
			m_timer_set(M_TIMER_ID_TOUCH_SCAN, TOUCH_SCAN_TIME_IN_KEYSTATE);					// TOUCH_SCAN: 100ms
			// KEYBOARD_SCAN_TIME_SHIFT: for load balancing
			m_timer_set(M_TIMER_ID_KEYBOARD_SCAN, KEYBOARD_SCAN_TIME+KEYBOARD_SCAN_TIME_SHIFT);	// KEYBOARD_SCAN: 30ms + 5ms
			m_sub_status = SUB_STATUS_RUN;
			break;

		case SUB_STATUS_RUN :
			if(m_timer_is_expired(M_TIMER_ID_TOUCH_SCAN))
			{
				//m_timer_set(M_TIMER_ID_TOUCH_SCAN, TOUCH_SCAN_TIME_IN_KEYSTATE); // 100ms //20190701_flimbs
				//m_event |= M_EVENT_TOUCH_MODE_SCAN_TIME;
			}

			if(m_timer_is_expired(M_TIMER_ID_KEYBOARD_SCAN))
			{
				scan_clickbar_button();
				scan_clickbar_touch();
				m_timer_set(M_TIMER_ID_KEYBOARD_SCAN, KEYBOARD_SCAN_TIME);	// 30ms
				// SENSOR_SCAN_TIME_SHIFT: for load balancing
				m_timer_set(M_TIMER_ID_SENSOR_SCAN, SENSOR_SCAN_TIME_SHIFT); // 2ms
				m_event |= M_EVENT_KEYBOAD_MODE_SCAN_TIME;

				m_clear_inertiaScroll();
			}

			if(m_timer_is_expired(M_TIMER_ID_SENSOR_SCAN))
			{
				//scan_clickbar_button();
				//scan_clickbar_touch();
				//m_timer_set(M_TIMER_ID_SENSOR_SCAN, SENSOR_SCAN_TIME);
			}

			// HOT KEY EVENT DETECTION HEAR AFTER
			hoykey = drv_keyboard_get_hotkey();
			
			if(hoykey & (DRV_KEYBOARD_HOTKEY_BT1 
						| DRV_KEYBOARD_HOTKEY_BT2 
						| DRV_KEYBOARD_HOTKEY_BT3 
						| DRV_KEYBOARD_HOTKEY_DONGLE))
			{
				m_event |= M_EVENT_CHANNEL_CHANGED;
				// Update argument
				if(hoykey & DRV_KEYBOARD_HOTKEY_BT1) 	{ m_arg.channel.bt1 = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_BT2) 	{ m_arg.channel.bt2 = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_BT3) 	{ m_arg.channel.bt3 = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_DONGLE) { m_arg.channel.dongle = 1;}
			}

		
			if(hoykey & (DRV_KEYBOARD_HOTKEY_V_DOWN | DRV_KEYBOARD_HOTKEY_V_UP | DRV_KEYBOARD_HOTKEY_V_MUTE))
			{
				m_event |= M_EVENT_VOLUME_CHANGED;
				// Update argument
				if(hoykey & DRV_KEYBOARD_HOTKEY_V_DOWN) { m_arg.volume.down = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_V_UP) 	{ m_arg.volume.up   = 1;}					
				if(hoykey & DRV_KEYBOARD_HOTKEY_V_MUTE) { m_arg.volume.mute = 1;}
			}
		

			if(hoykey & (DRV_KEYBOARD_HOTKEY_HOUSE 
							| DRV_KEYBOARD_HOTKEY_SWTAPP 
							| DRV_KEYBOARD_HOTKEY_VKBD 
							| DRV_KEYBOARD_HOTKEY_FIND 
							| DRV_KEYBOARD_HOTKEY_CAPTURE
							| DRV_KEYBOARD_HOTKEY_BACKWARD
							| DRV_KEYBOARD_HOTKEY_LOCKSCREEN
							| DRV_KEYBOARD_HOTKEY_LANGUAGE_Z
							| DRV_KEYBOARD_HOTKEY_LANGUAGE_X
							| DRV_KEYBOARD_HOTKEY_LANGUAGE_C))
			{
				m_event |= M_EVENT_SPECIAL_KEY;
				// Update argument        
				________________________________LOG_mComm_SendingFnHotKey(2," hotkey:%x , Z:%x, X:%x, C:%x",hoykey,DRV_KEYBOARD_HOTKEY_LANGUAGE_Z,DRV_KEYBOARD_HOTKEY_LANGUAGE_X,DRV_KEYBOARD_HOTKEY_LANGUAGE_C);
				if(hoykey & DRV_KEYBOARD_HOTKEY_HOUSE) 	{ m_arg.sp_key.home = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_SWTAPP) { m_arg.sp_key.swtapp = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_VKBD) 	{ m_arg.sp_key.vkbd = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_FIND) 	{ m_arg.sp_key.search = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_CAPTURE) { m_arg.sp_key.capture = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_BACKWARD) { m_arg.sp_key.backward= 1; }//
				if(hoykey & DRV_KEYBOARD_HOTKEY_LOCKSCREEN) { m_arg.sp_key.lockscreen= 1; }//
				if(hoykey & DRV_KEYBOARD_HOTKEY_LANGUAGE_Z)	{m_arg.sp_key.language_z= 1; ________________________________LOG_mComm_SendingFnHotKey(2,"lang_z hotkey:%d",hoykey);}//?��보드?��?���? ?��?��
				if(hoykey & DRV_KEYBOARD_HOTKEY_LANGUAGE_X)	{m_arg.sp_key.language_x= 1; ________________________________LOG_mComm_SendingFnHotKey(2,"lang_x hotkey:%d",hoykey);}//?��보드?��?���? ?��?��
				if(hoykey & DRV_KEYBOARD_HOTKEY_LANGUAGE_C)	{m_arg.sp_key.language_c= 1; ________________________________LOG_mComm_SendingFnHotKey(2,"lang_c hotkey:%d",hoykey);}//?��보드?��?���? ?��?��
			}
			

			if(hoykey & ( DRV_KEYBOARD_HOTKEY_BT1_NEW
							| DRV_KEYBOARD_HOTKEY_BT2_NEW
							| DRV_KEYBOARD_HOTKEY_BT3_NEW
							| DRV_KEYBOARD_HOTKEY_ERASE_BOND))
			{
				m_event |= M_EVENT_NEW_BOND;
				// Update argument
				if(hoykey & DRV_KEYBOARD_HOTKEY_BT1_NEW) 	{ m_arg.bond.bt1 = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_BT2_NEW) 	{ m_arg.bond.bt2 = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_BT3_NEW) 	{ m_arg.bond.bt3 = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_ERASE_BOND) { m_arg.bond.all = 1;}
			}

			
			if(hoykey & DRV_KEYBOARD_HOTKEY_TOUCHLOCK)
			{	
				if(M_TOUCH_CYPRESS_ACTIVE_AREA_WHOLE == m_touch_mode_get())
				{
					m_event |= M_EVENT_TOUCH_MODE_CHANGED;
					m_arg.touch_mode.lock_exit = 1;					
				}				
				else
				{
					if((M_COMM_ADV_STATUS_CONNECTED <= m_comm_adv_get_status())) // 채널기억이 없으면 터치락 금지
					{
						m_event |= M_EVENT_TOUCH_MODE_CHANGED;
						m_arg.touch_mode.lock_enter = 1;				
					}
				}
			}


			if(hoykey & DRV_KEYBOARD_HOTKEY_BTRCHK)
			{
				m_event |= M_EVENT_MODE_CHANGED;
				m_arg.new_mode.battery_chk = 1;
			}
			
			if (hoykey & (DRV_KEYBOARD_HOTKEY_CENTRAL_WINDOWS
				| DRV_KEYBOARD_HOTKEY_CENTRAL_ANDROID
				| DRV_KEYBOARD_HOTKEY_CENTRAL_MAC
				| DRV_KEYBOARD_HOTKEY_CENTRAL_IOS))
			{				
				m_event |= M_EVENT_CENTRAL_MODE;
				if (hoykey & DRV_KEYBOARD_HOTKEY_CENTRAL_WINDOWS) 
				{					
					m_arg.central.windows = 1;
				}
				if (hoykey & DRV_KEYBOARD_HOTKEY_CENTRAL_ANDROID) 
				{					
					m_arg.central.android = 1;
				}
				if (hoykey & DRV_KEYBOARD_HOTKEY_CENTRAL_MAC) 
				{					
					m_arg.central.mac = 1;
				}
				if (hoykey & DRV_KEYBOARD_HOTKEY_CENTRAL_IOS) 
				{					
					m_arg.central.ios = 1;
				}
				
			}

			

      		#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
				DoDebugByHotkey(hoykey);
			#endif

			break;

		default :			
			break;
	}
}



//================================================================================
//	Function name : touch_event_detect()
//================================================================================
static void touch_event_detect(void)
{
	________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(void)");
	uint32_t hoykey;
	
	switch(m_sub_status)
	{
		case SUB_STATUS_READY :
			m_timer_set(M_TIMER_ID_TOUCH_SCAN, TOUCH_SCAN_TIME);								// TOUCH_SCAN: 10ms
			// KEYBOARD_SCAN_TIME_SHIFT: for load balancing
			m_timer_set(M_TIMER_ID_KEYBOARD_SCAN, KEYBOARD_SCAN_TIME+KEYBOARD_SCAN_TIME_SHIFT); // KEYBOARD_SCAN: 30ms + 5ms
			m_sub_status = SUB_STATUS_RUN;
			break;

		case SUB_STATUS_RUN :
			if(m_timer_is_expired(M_TIMER_ID_TOUCH_SCAN))
			{
				scan_clickbar_button();
				scan_clickbar_touch();
				// Start timer again
				m_timer_set(M_TIMER_ID_TOUCH_SCAN, TOUCH_SCAN_TIME); // every 10ms
				m_event |= M_EVENT_TOUCH_MODE_SCAN_TIME;
			}

			if(m_timer_is_expired(M_TIMER_ID_KEYBOARD_SCAN))
			{
				m_timer_set(M_TIMER_ID_KEYBOARD_SCAN, KEYBOARD_SCAN_TIME); // every 30ms
				// SENSOR_SCAN_TIME_SHIFT: for load balancing
				m_timer_set(M_TIMER_ID_SENSOR_SCAN, SENSOR_SCAN_TIME_SHIFT); // 2ms
				m_event |= M_EVENT_KEYBOAD_MODE_SCAN_TIME;
			}

			if(m_timer_is_expired(M_TIMER_ID_SENSOR_SCAN))
			{
				//scan_clickbar_button();
				//scan_clickbar_touch();
				//m_timer_set(M_TIMER_ID_SENSOR_SCAN, SENSOR_SCAN_TIME);
			}

			// HOT KEY EVENT DETECTION HEAR AFTER
			hoykey = drv_keyboard_get_hotkey();

			if(hoykey & (DRV_KEYBOARD_HOTKEY_BT1 
						| DRV_KEYBOARD_HOTKEY_BT2 
						| DRV_KEYBOARD_HOTKEY_BT3 
						| DRV_KEYBOARD_HOTKEY_DONGLE))
			{
				m_event |= M_EVENT_CHANNEL_CHANGED;				
				if(hoykey & DRV_KEYBOARD_HOTKEY_BT1) 	{ m_arg.channel.bt1 = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_BT2) 	{ m_arg.channel.bt2 = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_BT3) 	{ m_arg.channel.bt3 = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_DONGLE) { m_arg.channel.dongle = 1;}
			}
		
			if(hoykey & (DRV_KEYBOARD_HOTKEY_V_DOWN | DRV_KEYBOARD_HOTKEY_V_UP | DRV_KEYBOARD_HOTKEY_V_MUTE))
			{
				m_event |= M_EVENT_VOLUME_CHANGED;				
				if(hoykey & DRV_KEYBOARD_HOTKEY_V_DOWN) { m_arg.volume.down = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_V_UP) 	{ m_arg.volume.up   = 1;}				
				if(hoykey & DRV_KEYBOARD_HOTKEY_V_MUTE) {m_arg.volume.mute = 1;}				
			}
		
			if(hoykey & (DRV_KEYBOARD_HOTKEY_HOUSE 
							| DRV_KEYBOARD_HOTKEY_SWTAPP 
							| DRV_KEYBOARD_HOTKEY_VKBD 
							| DRV_KEYBOARD_HOTKEY_FIND 
							| DRV_KEYBOARD_HOTKEY_CAPTURE
							| DRV_KEYBOARD_HOTKEY_LOCKSCREEN))
			{
				m_event |= M_EVENT_SPECIAL_KEY;
				if(hoykey & DRV_KEYBOARD_HOTKEY_HOUSE) 	{ m_arg.sp_key.home = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_SWTAPP) { m_arg.sp_key.swtapp = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_VKBD) 	{ m_arg.sp_key.vkbd = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_FIND) 	{ m_arg.sp_key.search = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_CAPTURE) { m_arg.sp_key.capture = 1;}					
				if(hoykey & DRV_KEYBOARD_HOTKEY_LOCKSCREEN) { m_arg.sp_key.lockscreen = 1;}					
			}
		

			if(hoykey & ( DRV_KEYBOARD_HOTKEY_BT1_NEW
							| DRV_KEYBOARD_HOTKEY_BT2_NEW
							| DRV_KEYBOARD_HOTKEY_BT3_NEW
							| DRV_KEYBOARD_HOTKEY_ERASE_BOND))
			{
				m_event |= M_EVENT_NEW_BOND;				
				if(hoykey & DRV_KEYBOARD_HOTKEY_BT1_NEW) 	{ m_arg.bond.bt1 = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_BT2_NEW) 	{ m_arg.bond.bt2 = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_BT3_NEW) 	{ m_arg.bond.bt3 = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_ERASE_BOND) { m_arg.bond.all = 1;}
			}



			if(hoykey & DRV_KEYBOARD_HOTKEY_TOUCHLOCK)
			{								
				if(M_TOUCH_CYPRESS_ACTIVE_AREA_WHOLE == m_touch_mode_get())
				{
					m_event |= M_EVENT_TOUCH_MODE_CHANGED;
					m_arg.touch_mode.lock_exit = 1;					
				}				
				else
				{
					if((M_COMM_ADV_STATUS_CONNECTED <= m_comm_adv_get_status())) // 채널기억이 없으면 터치락 금지
					{
						m_event |= M_EVENT_TOUCH_MODE_CHANGED;
						m_arg.touch_mode.lock_enter = 1;				
					}
				}
			}


			if(hoykey & DRV_KEYBOARD_HOTKEY_BTRCHK)
			{
				m_event |= M_EVENT_MODE_CHANGED;
				m_arg.new_mode.battery_chk = 1;
			}


			
			if (hoykey & (DRV_KEYBOARD_HOTKEY_CENTRAL_WINDOWS
				| DRV_KEYBOARD_HOTKEY_CENTRAL_ANDROID
				| DRV_KEYBOARD_HOTKEY_CENTRAL_MAC
				| DRV_KEYBOARD_HOTKEY_CENTRAL_IOS))
			{				
				m_event |= M_EVENT_CENTRAL_MODE;
				if (hoykey & DRV_KEYBOARD_HOTKEY_CENTRAL_WINDOWS) 
				{					
					m_arg.central.windows = 1;
				}
				if (hoykey & DRV_KEYBOARD_HOTKEY_CENTRAL_ANDROID) 
				{					
					m_arg.central.android = 1;
				}
				if (hoykey & DRV_KEYBOARD_HOTKEY_CENTRAL_MAC) 
				{					
					m_arg.central.mac = 1;
				}
				if (hoykey & DRV_KEYBOARD_HOTKEY_CENTRAL_IOS) 
				{					
					m_arg.central.ios = 1;
				}
				
			}
			
			
			//#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
			DoDebugByHotkey(hoykey);
			//#endif
						
			break;

		default :
			
			break;
	}
	
}

/*
================================================================================
   Function name : upgrade_event_detect()
================================================================================
*/
static void upgrade_event_detect(void)
{
	________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(void)");
	#if defined(M_EVENT_STATUS_TRACE)
	DEBUG("Called");
	#endif
}

/*
================================================================================
   Function name : sleep_event_detect()
================================================================================
*/
static void sleep_event_detect(void)
{
	________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(void)");
	// Give a time event to run sleep handler
	if(m_timer_is_expired(M_TIMER_ID_KEYBOARD_SCAN))
	{
		m_timer_set(M_TIMER_ID_KEYBOARD_SCAN, KEYBOARD_SCAN_TIME);
		m_event |= M_EVENT_KEYBOAD_MODE_SCAN_TIME;
	}
}

/*
================================================================================
   Function name : test_event_detect()
================================================================================
*/
static void test_event_detect(void)
{
	________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(void)");
	uint32_t hoykey;
	
	switch(m_sub_status)
	{
		case SUB_STATUS_READY :
			// Add start code for test mode
			m_led_set_color(M_LED_COLOR_TEST);
			m_timer_set(M_TIMER_ID_TOUCH_SCAN, M_TIMER_TIME_SECOND(5));
			m_timer_set(M_TIMER_ID_KEYBOARD_SCAN, M_TIMER_TIME_SECOND(5)+M_TIMER_TIME_100MS(1));
			m_sub_status = SUB_STATUS_RUN;
		break;

		case SUB_STATUS_RUN :
			if(m_timer_is_expired(M_TIMER_ID_TOUCH_SCAN))
			{
				m_timer_set(M_TIMER_ID_TOUCH_SCAN, TEST_SENDING_TIME);
				m_event |= M_EVENT_TOUCH_MODE_SCAN_TIME;
			}
			
			if(m_timer_is_expired(M_TIMER_ID_KEYBOARD_SCAN))
			{
				m_timer_set(M_TIMER_ID_KEYBOARD_SCAN, TEST_LED_UPDATE_TIME);
				m_event |= M_EVENT_KEYBOAD_MODE_SCAN_TIME;
			}

			hoykey = drv_keyboard_get_hotkey();

			if(hoykey & DRV_KEYBOARD_HOTKEY_RESET)
			{
				//reset_mokibo();
			}

			if(hoykey & DRV_KEYBOARD_HOTKEY_TEST_ENTER)
			{
				m_event |= M_EVENT_MODE_CHANGED;
				m_arg.new_mode.test = 1;
			}
			
			//#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
			DoDebugByHotkey(hoykey);
			//#endif

			
			if(hoykey & (DRV_KEYBOARD_HOTKEY_BT1 
						| DRV_KEYBOARD_HOTKEY_BT2 
						| DRV_KEYBOARD_HOTKEY_BT3 
						| DRV_KEYBOARD_HOTKEY_DONGLE))
			{
				m_event |= M_EVENT_CHANNEL_CHANGED;
				// Update argument
				if(hoykey & DRV_KEYBOARD_HOTKEY_BT1) 	{ m_arg.channel.bt1 = 1;};
				if(hoykey & DRV_KEYBOARD_HOTKEY_BT2) 	{ m_arg.channel.bt2 = 1;};
				if(hoykey & DRV_KEYBOARD_HOTKEY_BT3) 	{ m_arg.channel.bt3 = 1;};
				if(hoykey & DRV_KEYBOARD_HOTKEY_DONGLE) { m_arg.channel.dongle = 1;};
			}
		
			if(hoykey & ( DRV_KEYBOARD_HOTKEY_BT1_NEW
							| DRV_KEYBOARD_HOTKEY_BT2_NEW
							| DRV_KEYBOARD_HOTKEY_BT3_NEW
							| DRV_KEYBOARD_HOTKEY_ERASE_BOND))
			{
				m_event |= M_EVENT_NEW_BOND;
				// Update argument
				if(hoykey & DRV_KEYBOARD_HOTKEY_BT1_NEW) 	{ m_arg.bond.bt1 = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_BT2_NEW) 	{ m_arg.bond.bt2 = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_BT3_NEW) 	{ m_arg.bond.bt3 = 1;}
				if(hoykey & DRV_KEYBOARD_HOTKEY_ERASE_BOND) { m_arg.bond.all = 1;}
			}
		break;
		
		default :
			DEBUG("OOPS[%d]!-test_event_detect", m_sub_status);
		break;
	}
}

/*
================================================================================
   Function name : battery_check_event_detect()
================================================================================
*/
static void battery_check_event_detect(void)
{
	________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(void)");
	// Give a time event to run battery_check handler
	if(m_timer_is_expired(M_TIMER_ID_KEYBOARD_SCAN))
	{
		m_timer_set(M_TIMER_ID_KEYBOARD_SCAN, KEYBOARD_SCAN_TIME);
		m_event |= M_EVENT_KEYBOAD_MODE_SCAN_TIME;
	}
}

/*
================================================================================
   Function name : end_event_detect()
================================================================================
*/
static void end_event_detect(void)
{
	________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(void)");
	#if defined(M_EVENT_STATUS_TRACE)
	DEBUG("Called");
	#endif
}
























/*$$$$$\  $$$$$$$$\ $$$$$$$\  $$\   $$\  $$$$$$\                                       
$$  __$$\ $$  _____|$$  __$$\ $$ |  $$ |$$  __$$\                                      
$$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |$$ /  \__|                                     
$$ |  $$ |$$$$$\    $$$$$$$\ |$$ |  $$ |$$ |$$$$\                                      
$$ |  $$ |$$  __|   $$  __$$\ $$ |  $$ |$$ |\_$$ |                                     
$$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |$$ |  $$ |                                     
$$$$$$$  |$$$$$$$$\ $$$$$$$  |\$$$$$$  |\$$$$$$  |                                     
\_______/ \________|\_______/  \______/  \______/          
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\  $$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |$$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |$$ /  \__|
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |\$$$$$$\  
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ | \____$$\ 
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |$$\   $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |\$$$$$$  |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__| \______/ 
//============================================================================//
//                                 DEBUG FUNCTION                             //
//============================================================================*/


uint8_t CharToKey(uint8_t input)
{
	 if(48 <= input && input <= 57)
	{
		if(input == 48)
		{
			return APP_USBD_HID_KBD_0;
		}				
		return input - 19;
	}
	else if(65 <= input && input <= 90)
	{
		return input - 61;
	}
	else if(97<= input && input <= 122)
	{				
		return input - 93;
	}
	else if(input == ']')
	{	
		return APP_USBD_HID_KBD_C_BRCKT;
	}
	else if(input == '[')
	{	
		return APP_USBD_HID_KBD_O_BRCKT;
	}
	else if(input == '.')
	{
		return APP_USBD_HID_KBD_DOT;
	}
	else if(input == '\n')
	{
		return APP_USBD_HID_KBD_ENTER;
	}
	else if(input == ' ')
	{
		return APP_USBD_HID_KBD_SB;
	}
	else if(input == '=')
	{
		return APP_USBD_HID_KBD_PLUS;
	}
	else if(input == '_')
	{
		return APP_USBD_HID_KBD_U_SCORE;
	}
	else if(input=='/')
	{
		return APP_USBD_HID_KBD_SLASH;
	}
	else if(input==':')
	{
		return APP_USBD_HID_KBD_COLON;
	}
	else
	{
		return APP_USBD_HID_KBD_SB;
	}
	
}

//================================================================================
//			DoDebugByHotkey
//================================================================================
static void DoDebugByHotkey(uint32_t hoykey)
{
	static uint16_t TypeCounter = 0;
	static uint8_t startFlag = 0;
	static char version_message[512]="";
	
	________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(uint32_t hoykey:%d)",hoykey);
	#ifdef SIMULATE_KBD_AND_MOUSE_FOR_CERTIFICATION_TEST
	// Fn + 0  ... toggle certification mode (enable/disable)
	if(hoykey & DRV_KEYBOARD_HOTKEY_CERT_MODE)
	{
		// certification용 반복 작업을 시작하거나 끝내게 하기 위하여 사용하는 toggle 변수이다.
		m_cert_mode_enabled = (m_cert_mode_enabled) ? 0:1;
		DEBUGS("m_cert_mode_enabled=%d",m_cert_mode_enabled);
	}
	#endif


	if(hoykey & DRV_KEYBOARD_HOTKEY_RESET)
	{
	

		#if 0
		if(m_timer_is_expired(M_TIMER_ID_CHARGER_CHECK))
		{
			static uint8_t oldBtrState=0;
			uint8_t getMsg=0;
			uint8_t ledStatus=0;
			uint8_t ledColor=0;

			drv_stm8_read(STM8_READ_BATTERY_STATUS, &getMsg);
			
			drv_stm8_read(STM8_READ_LED_STATUS, &ledStatus);
			drv_stm8_read(STM8_READ_LED_COLOR, &ledColor);
			
			
			getMsg = getMsg & (1<<4);
			

			if(getMsg != oldBtrState)
			{
				uint8_t oldtype; //= m_led_get_status();
				uint8_t oldcolor =0;// m_led_get_color();

				drv_stm8_read(STM8_READ_LED_STATUS, &oldtype);
				nrf_delay_ms(20);
				drv_stm8_read(STM8_READ_LED_COLOR, &oldcolor);
				nrf_delay_ms(20);

				drv_stm8_write(STM8_WRITE_LED_STATUS, M_LED_OFF);
				nrf_delay_ms(20);
				drv_stm8_write(STM8_WRITE_LED_COLOR, M_LED_COLOR_WHITE);								
				nrf_delay_ms(20);
				drv_stm8_write(STM8_WRITE_LED_STATUS, M_LED_DIMMING_OUT);

				//drv_stm8_write(STM8_WRITE_LED_STATUS, M_LED_OFF);
				nrf_delay_ms(20);
				drv_stm8_write(STM8_WRITE_LED_COLOR, (M_LED_COLOR_TYPE_t)oldcolor);
				
				//m_led_set_color(oldcolor);
				//m_led_set_status(oldtype);

				oldBtrState = getMsg;
			}

			m_timer_set(M_TIMER_ID_CHARGER_CHECK,M_TIMER_TIME_100MS(5));
		}	
		#endif



		if(m_timer_is_expired(M_TIMER_ID_DEBUG))
		{
			TypeCounter = 0;
			startFlag = 1;
			m_timer_set(M_TIMER_ID_DEBUG, M_TIMER_TIME_MS(500));
		}
		else
		{			
			
			if(startFlag==1)
			{
				startFlag = 2;
				
			}
			else if(startFlag==2)
			{
				uint8_t battery_level, battery_status, high, low, fwStatus;
				battery_level = battery_status = high = low = fwStatus =0;
				m_util_get_battery_level_test(&fwStatus ,&battery_level,&battery_status,&high,&low);
				sprintf(version_message,"%s [FW_status:%d] [BTR_state:%d] [BTR_level:%d] [BTR_hilo:%d,%d]",m_version_message, fwStatus,battery_status, battery_level,high,low);
				startFlag = 3;
				m_timer_set(M_TIMER_ID_DEBUG, M_TIMER_TIME_MS(0));
			}
			else
			{
				startFlag=0;
			}
		}
	}


	if(startFlag == 3)
	{		//	reset_mokibo();
		if(m_timer_is_expired(M_TIMER_ID_DEBUG))
		{	
			if(m_comm_ble_get_status() != BLE_CONN_HANDLE_INVALID)
			{	
				
				if(TypeCounter > strlen(version_message))
				{
					TypeCounter = 0;
					startFlag=0;
					return;
				}
				else
				{
					if((65 <= version_message[TypeCounter] && version_message[TypeCounter] <= 90) || version_message[TypeCounter] == '_' ||  version_message[TypeCounter] == ':')
					{
						m_KeyswitchDown(CharToKey(version_message[TypeCounter]), APP_USBD_HID_KBD_MODIFIER_LEFT_SHIFT); // o	
					}
					else
					{
						m_KeyswitchDown(CharToKey(version_message[TypeCounter]), 0x00); // o
					}
					m_KeyswitchUp();
					TypeCounter++;					
					m_timer_set(M_TIMER_ID_DEBUG, M_TIMER_TIME_MS(24));
				}
			}
			else
			{
				TypeCounter = 0;
				startFlag=0;
			}
		}
	}

	if(hoykey & DRV_KEYBOARD_HOTKEY_TEST_ENTER)
	{
		
		
	}
			
}





//================================================================================
//			debug() ..... Fn+4  Fn + 4
//================================================================================
#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
static void debug(void)
{
	________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(void)");
	//static uint8_t wakeup_or_sleep= 0;
	
	DEB("\n\n\n\n");

	//DEB("################ CALIBRATION TEST ####################\n");
	//drv_cypress_req_calibration(0);

	//DEB("################ WakeUp or DeepSleep ####################\n");
	// wakeup= 0, deep_sleep= 1
	//drv_cypress_req_deep_sleep(0); // wake-up
	//drv_cypress_req_deep_sleep(1);	// deep-sleep

	DEB("################ SUSPEND SCANNING ####################\n");
	drv_cypress_req_suspend_scanning(); // suspend_scanning
	DEB("-----------------------------------------------------\n");
}
#endif



//================================================================================
//			debug2 - Fn + 5/8
//================================================================================
#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
static void debug2(void)
{
	________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(void)");
	DEB("################ RESUME SCANNING ####################\n");
	drv_cypress_req_resume_scanning(); // resume_scanning
	DEB("-----------------------------------------------------\n");
	
}
#endif //MOKIBO_DEBUG_ON




//================================================================================
//			debug3: Fn + 6
//================================================================================
#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
static void debug3(void)
{
	________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(void)");
	#if 1 ///
		
	#else
			
			static uint8_t sleepToggle=0;

			if(sleepToggle==1){
				DEB("################ DEEP SLEEP ####################\n");
			}else{
				DEB("################ ACT_LFT (<-- from Deep_sleep) ####################\n");
			}

			// wakeup= 0,  deep_sleep= 1
			drv_cypress_req_deep_sleep(sleepToggle); // deep_sleep
			//
			sleepToggle++;
			if(sleepToggle>1) sleepToggle=0;
			
			DEB("-----------------------------------------------------\n");			
	#endif	
}
#endif // MOKIBO_DEBUG_ON





#ifdef SIMULATE_KBD_AND_MOUSE_FOR_CERTIFICATION_TEST
	//		1. Fn + 0 키를 누르면 시작
	//				1.1 마우스를 순차적으로 반복 회전 (직사각형 이동: right --> down --> left --> up)
	//				1.2 임의의 키값(예를 들어, Hello Mokibo)을 반복해서 (Notepad등에) 입력
	//		2. Fn + 0 키를 다시 누르면 끝.
	//

	//================================================================================
	//			m_do_cert_mode_repetition
	//================================================================================
	void mouse_move_for_cert(void);
	void keystrokes_for_cert(void);
	//
	void m_do_cert_mode_repetition(void)
	{
		________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(void)");
		mouse_move_for_cert();
		keystrokes_for_cert();
	}


	//================================================================================
	//			mouse_move_for_cert
	//================================================================================
	void mouse_move_for_cert(void)
	{
		________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(void)");
		#define MOVE_RIGHT 	1
		#define MOVE_DOWN 	2
		#define MOVE_LEFT 	3
		#define MOVE_UP 		4
		static int8_t step_x= 1;			// 
		static int8_t step_y= 1;			//
		static uint8_t direction = 0;	// MOVE_RIGHT/MOVE_DOWN/MOVE_LEFT/MOVE_UP
		static int8_t dx= 1;			// +/-dx step
		static int8_t dy= 1;			// +/-dy step
		//
		static int8_t change_direction= 10;
		static int8_t max_step_x= 50;
		static int8_t max_step_y= 50;

		//
		max_step_x--;	if(max_step_x < 10) max_step_x = 50; 
		max_step_y--;	if(max_step_y < 10) max_step_y = 50; 
		//
		change_direction--;
		if(change_direction < 0)
		{
			change_direction= 10;
			//
			direction++;
			if(direction > MOVE_UP)
			{
				direction= MOVE_RIGHT;
				step_x++;
				step_y++;
				if(step_x > max_step_x) step_x= 1;
				if(step_y > max_step_y) step_y= 1;
			}
		}
		//
		switch(direction)
		{
			case MOVE_RIGHT:
				dx= +step_x; dy=0;
				break;

			case MOVE_DOWN:
				dx= 0; dy= +step_y;
				break;

			case MOVE_LEFT:
				dx= -step_x; dy= 0;
				break;

			case MOVE_UP:
				dx= 0; dy= -step_y;
				break;

			default:
				dx= step_x; dy= step_y;
				break;
		}
		
		// actually move
		m_MouseMove(dx,dy);
	}



	//================================================================================
	//			keystrokes_for_cert
	//================================================================================
	void keystrokes_for_cert(void)
	{
		________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(void)");
		// 	
		static uint32_t key_sequence= 0;
		
		// to read once
		static uint8_t bt_channel_ready=0;
		static m_comm_config_t	 m_config;

		static uint8_t	battery_level=0;
		if(!bt_channel_ready){
			//ret_code_t err_code = drv_fds_read(DRV_FDS_PAGE_M_COMM_CONFIG, (uint8_t *)&m_config, sizeof(m_config));
			bt_channel_ready=1;
		}
		
		// 매 timer tick 마다 Keyswitch Up/Down
		switch(key_sequence)
		{
			case 1: // 
				
				
				m_KeyswitchDown(APP_USBD_HID_KBD_F5, 0x00); // m (+Shift)
				key_sequence++;
				break;

			case 3: // 
				m_KeyswitchDown(APP_USBD_HID_KBD_COLON, 0x00); // o
				key_sequence++;
				break;

			case 5: // 
				m_KeyswitchDown(APP_USBD_HID_KBD_SB, 0x00); // k
				key_sequence++;
				break;

			case 7: // 
				//m_KeyswitchDown(APP_USBD_HID_KBD_I, 0x00); // i
				m_util_get_battery_level(&battery_level);			
				key_sequence++;
				break;

			case 9: // 
				//m_KeyswitchDown(APP_USBD_HID_KBD_B, 0x00); // b
				
				

				uint8_t tenth = battery_level/10;
				

				tenth = tenth==0?39:tenth+29;
				
				
				m_KeyswitchDown(tenth, 0x00); // k
				
				key_sequence++;
				break;

			case 11: // 
				//m_KeyswitchDown(APP_USBD_HID_KBD_O, 0x00); // o			

				
				uint8_t oneth = battery_level%10;

				
				oneth = oneth==0?39:oneth+29;
				
				m_KeyswitchDown(oneth, 0x00); // k
				key_sequence++;
				break;

			case 13: // 1 2 3 4 5 6 7 8 9 0 
				//m_KeyswitchDown(APP_USBD_HID_KBD_1+ m_config.config.selected_ch, 0x00); // 1+n
				key_sequence++;
				break;

			case 15:
				m_KeyswitchDown(APP_USBD_HID_KBD_ENTER, 0x00); // <ENTER>
				key_sequence++;
				break;

			case 990: // 
				// Repeat the sequence
				key_sequence=1;
				break;
				
			default:
				// Release the Keyswitch at every even-numbered sequence
				m_KeyswitchUp();
				key_sequence++;
				break;
		}
	}
#endif //SIMULATE_KBD_AND_MOUSE_FOR_CERTIFICATION_TEST






#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
	//================================================================================
	//			GetStr_fds_page_usage
	//================================================================================
	char *GetStr_fds_page_usage(uint8_t page)
	{
		________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(uint8_t page:%d)",page);
		switch(page)
		{
			case drv_fds_small_page_0: return "DRV_FDS_PAGE_CONFIG_VALID";
			case drv_fds_small_page_1: return "DRV_FDS_PAGE_TOUCH_VER";
			case drv_fds_small_page_2: return "DRV_FDS_PAGE_TEST";
			case drv_fds_small_page_3: return "DRV_FDS_PAGE_MANUFACTURE";
			case drv_fds_small_page_4: return "DRV_FDS_PAGE_M_DONGLE_CONFIG";
			case drv_fds_small_page_5: return "--\n";
			case drv_fds_medium_page_0:return "DRV_FDS_PAGE_M_COMM_CONFIG";
			case drv_fds_large_page_0: return "DRV_FDS_PAGE_M_COMM_ADV_CONFIG";
			default: 				   return "UNKNOWN PAGE";
		}
	}

	//================================================================================
	//			GetStr_comm_channel
	//================================================================================
	char *GetStr_comm_channel(uint8_t ch)
	{
		________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(uint8_t ch:%d)",ch);
		switch(ch)
		{
			case M_COMM_CHANNEL_BT1: return "0(BT1)";
			case M_COMM_CHANNEL_BT2: return "1(BT2)";
			case M_COMM_CHANNEL_BT3: return "2(BT3)";
	//		case M_COMM_CHANNEL_DONGLE: return "3(DONGLE)";
			default: return "UNKNOWN CH";
		}
	}


	//================================================================================
	//			GetStr_comm_central
	//================================================================================
	char *GetStr_comm_central(uint8_t cent)
	{
		________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(uint8_t cent:%d)",cent);
		switch(cent)
		{
			case M_COMM_CENTRAL_MICROSOFT: return "MICROSOFT(0)";
			case M_COMM_CENTRAL_MAC: return "MAC(1)";
			case M_COMM_CENTRAL_IOS: return "iOS(2)";
			case M_COMM_CENTRAL_ANDROID: return "ANDROID(3)";
			case M_COMM_CENTRAL_OTHER: return "OTHER(4)";
			default: return "Other(5~)";
		}
	}

	//================================================================================
	//			GetStr_comm_adv_channel
	//================================================================================
	char *GetStr_comm_adv_channel(uint8_t ch)
	{
		________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(uint8_t ch:%d)",ch);
		switch(ch)
		{
			case M_COMM_ADV_CHANNEL_BT1: return "0(BT1)";
			case M_COMM_ADV_CHANNEL_BT2: return "1(BT2)";
			case M_COMM_ADV_CHANNEL_BT3: return "2(BT3)";
			default: return "?(BT?)";
		}
	}
#endif











/*\   $$\ $$\   $$\ $$\   $$\  $$$$$$\  $$$$$$$$\ $$$$$$$\                             
$$ |  $$ |$$$\  $$ |$$ |  $$ |$$  __$$\ $$  _____|$$  __$$\                            
$$ |  $$ |$$$$\ $$ |$$ |  $$ |$$ /  \__|$$ |      $$ |  $$ |                           
$$ |  $$ |$$ $$\$$ |$$ |  $$ |\$$$$$$\  $$$$$\    $$ |  $$ |                           
$$ |  $$ |$$ \$$$$ |$$ |  $$ | \____$$\ $$  __|   $$ |  $$ |                           
$$ |  $$ |$$ |\$$$ |$$ |  $$ |$$\   $$ |$$ |      $$ |  $$ |                           
\$$$$$$  |$$ | \$$ |\$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$  |                           
 \______/ \__|  \__| \______/  \______/ \________|\_______/                            
//============================================================================//
//                                 UNUSED FUNCTION                            //
//============================================================================*/

/*
static void scan_clickbar_button(void)
{
	________________________________LOG_mEvent_FuncHeads(FUNCHEAD_FLAG,"(void)");
	STM8_BUTTON_STATUS_t button_right;
	STM8_BUTTON_STATUS_t button_left;
	
	drv_stm8_read(STM8_READ_BUTTON_LEFT, &button_left);
	drv_stm8_read(STM8_READ_BUTTON_RIGHT, &button_right);
	
	m_input[input_left_button].bit.now = (STM8_BUTTON_ON == button_left) ? 1 : 0;
	m_input[input_right_button].bit.now = (STM8_BUTTON_ON == button_right) ? 1 : 0;	
	
	if(m_input[input_left_button].bit.prv != m_input[input_left_button].bit.now || m_input[input_right_button].bit.prv != m_input[input_right_button].bit.now)
	{
		________________________________LOG_mEvent_RecvClickBar(6,"ClickBarClick:[%d|%d] Recognized",button_left,button_right);

		if(m_input[input_left_button].bit.prv != m_input[input_left_button].bit.now)
		{					
			if(m_input[input_left_button].bit.now)
			{			
				m_event |= M_EVENT_CLICKBAR_BUTTON_CHANGED;
				m_arg.button.left= 1;
			} 
			else 
			{		
				m_event |= M_EVENT_CLICKBAR_BUTTON_CHANGED;
				m_arg.button.left= 0;
			}		
			m_input[input_left_button].bit.prv = m_input[input_left_button].bit.now;
		}

		if(m_input[input_right_button].bit.prv != m_input[input_right_button].bit.now)
		{		
			if(m_input[input_right_button].bit.now)
			{			
				m_event |= M_EVENT_CLICKBAR_BUTTON_CHANGED;
				m_arg.button.right= 1; // to indicate which button was released...
			} 
			else 
			{						
				m_event |= M_EVENT_CLICKBAR_BUTTON_CHANGED;
				m_arg.button.right= 0; //
			}				
			m_input[input_right_button].bit.prv = m_input[input_right_button].bit.now;		
		}
	}
	
#if 0 // 터치락 버튼 추가로 클릭으로 유도되는 터치락 삭제
	//-----------------------------------------------------------------------------------------------------------------------------------
	// Press two buttons for touch locking mode 
	if((0 == m_input[input_left_button].bit.now) || (0 == m_input[input_right_button].bit.now))
	{
		m_timer_set(M_TIMER_ID_TOUCH_LOCK, TOUCH_LOCK_PRESS_TIME);
		
		if(m_touch_lock_detect)
		{
			m_touch_lock_detect = 0;
		}
	}
	//-----------------------------------------------------------------------------------------------------------------------------------
	// Hold press left & right key during TOUCH_LOCK_PRESS_TIME
	if(m_timer_is_expired(M_TIMER_ID_TOUCH_LOCK) && (0 == m_touch_lock_detect))
	{
		m_touch_lock_detect = 1;

		// Refresh timer
		m_timer_set(M_TIMER_ID_TOUCH_LOCK, TOUCH_LOCK_PRESS_TIME);

		m_event |= M_EVENT_TOUCH_MODE_CHANGED;

		if(M_TOUCH_CYPRESS_ACTIVE_AREA_WHOLE == m_touch_mode_get())
		{
			m_arg.touch_mode.lock_exit = 1;
			//m_arg.touch_mode.lock_enter = 0; // inserted by Stanley
		}
		else
		{
			m_arg.touch_mode.lock_enter = 1;
			//m_arg.touch_mode.lock_exit = 0; // inserted by Stanley
		}
		/// // Force to lock_enter !!  by Stanley
		/// m_arg.touch_mode.lock_enter = 1;   <---> m_arg.touch_mode.enter와 다르다 !!
		//
	
#ifdef TRACE_CLICKBAR_TOUCH
		if(m_arg.touch_mode.lock_enter) SYSTEM_MSG("=== ClickBar Touch LOCKED   ===\n");
		if(m_arg.touch_mode.lock_exit)  SYSTEM_MSG("=== ClickBar Touch UNLOCKED ===\n");
		//DEB("m_touch_lock_detect         = %d \n",m_touch_lock_detect);
		//DEB("m_arg.touch_mode.lock_enter = %d \n",m_arg.touch_mode.lock_enter);
		//DEB("m_arg.touch_mode.lock_exit  = %d \n",m_arg.touch_mode.lock_exit);
#endif
	}

#endif
}
*/





