/// \file m_mokibo.h
/// This file contains all required configurations for mokibo
/*==============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
==============================================================================*/



/*\   $$\ $$$$$$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$$$$$$\ $$\     $$\          
$$ |  $$ |\_$$  _|$$  __$$\\__$$  __|$$  __$$\ $$  __$$\\$$\   $$  |         
$$ |  $$ |  $$ |  $$ /  \__|  $$ |   $$ /  $$ |$$ |  $$ |\$$\ $$  /          
$$$$$$$$ |  $$ |  \$$$$$$\    $$ |   $$ |  $$ |$$$$$$$  | \$$$$  /           
$$  __$$ |  $$ |   \____$$\   $$ |   $$ |  $$ |$$  __$$<   \$$  /            
$$ |  $$ |  $$ |  $$\   $$ |  $$ |   $$ |  $$ |$$ |  $$ |   $$ |             
$$ |  $$ |$$$$$$\ \$$$$$$  |  $$ |    $$$$$$  |$$ |  $$ |   $$ |             
\__|  \__|\______| \______/   \__|    \______/ \__|  \__|   \__|             
//============================================================================//
//                                 REVISION HISTORY                           //
//============================================================================//
20170826_MJ
    Created initial version

20171118_MJ
    Added WTD

20171128_MJ
    Added m_mokibo_get_stm8_fw_version()

20180102_MJ
    Added m_mokibo_soft_reset(), m_mokibo_handle_reset()
    Added m_mokibo_get_reset_reason()

20180207_MJ
    Changed all m_comm_send() into m_comm_send_event()

20180406_JH
    Add m_mokibo_mode == M_MOKIBO_GAZELL (to send to dongle)

20180212_MJ
    Added M_MOKIBO_TEST_STATUS for aging test
    Issue#: http://lab.innopresso.com/issues/118#change-556

20180412_MJ
    Moved m_mokibo_get_random_value() to m_timer_get_random_value()

20180413_JH
    Added m_dongle_handle_long_key() in keyboard_scan_timeout_handler

20180502_MJ
    Created m_mokibo_set_config_default()

20180505_MJ
    Added delay in m_mokibo_WDT_reset()

20180508_MJ
    Moved MOKIBO_WDT to MOKIBO_WDT_ENABLED in mokibo_config.h

20180514_MJ
    Changed code for new event: M_EVENT_CHANNEL_CHANGED, M_EVENT_NEW_BOND 

20180515_MJ
    15-Mar-2018  MJ.Kim
    Added M_MOKIBO_BATTERY_CHECK_STATUS
20180515_MJ
    Changed code for new event: M_EVENT_TOUCH_MODE_CHANGED 
    Added M_MOKIBO_BATTERY_CHECK_STATUS
    Created SUB_STATES_MACHINE, sub_state_type_t

20180518_MJ
    Increased WTD_RELOAD from 1000ms to 2000ms for peer update time

20180519_MJ
    Changed color, time interval of battery check
    Refer to "180319_Mokibo 동작시나리오.docx"

20180520_JH
    Added MOKIBO protocol mode (BLE,GAZELL)
    Add m_mokibo_mode (initial mode = M_MOKIBO_BLE)

20180521_MJ
    Issue: https://app.asana.com/0/0/679909534836841/f
    ...2018.5.21...Failure in to go to the sleep in dongle communication....
    Not working sd_power_system_off() in dongle mode
    Correction: changed normal mode then run sd_power_system_off()

20180718_MJ
    Issue: https://app.asana.com/0/650649611668999/735638795663606
    Added FilterKeys() to send required keys in touch mode
    Issue: https://app.asana.com/0/650649611668999/735739225241414
    Gave the priority to advertising for LED control
    Issue: https://app.asana.com/0/650649611668999/747845262960817
    Added condition check when changing into touch mode in ChangeState()

20180719_MJ
    Issue: https://app.asana.com/0/650649611668999/735638795663606
    Added ESC, F1~F12 Keys in FilterKeys()

20180719_MJ
    2018
    Issue: https://app.asana.com/0/650649611668999/735638795663606
    Added Insert key in FilterKeys()
    Added all event handlers for function lines keys

20180723_MJ
    Issue: https://app.asana.com/0/650649611668999/735638795663606
    Added m_prv_mokibo_status to go back after battery testing
    
20180808_stanley
cert_mode: 마우스 포인터를 사각형으로 우회전하게 하고, keyboard 문자를 입력하는 과정을 반복함
The followings are added for MOKIBO CERTIFICATION TEST;
		DRV_KEYBOARD_HOTKEY_CERT_MODE..... in drv_keyboard.h
	m_KeyswitchDown()................. in m_touch.c
	m_KeyswitchUp()................... in m_touch.c
	m_MouseMove()..................... in m_touch.c
		m_cert_mode_enabled............... in m_event.c
		m_do_cert_mode_repetition()....... in m_event.c
	The Caller ....................... in keyboard_scan_timeout_handler() in mokibo.c
  S2L(CERT_MODE)...................... in drv_keyboard.c
		"HOTKEY_CERT_MODE"................ in drv_keyboard.c
You can activate the CERT_MODE by typing [Fn+0].
To deactivate the mode, press the same key once more.

==============================================================================*/






/*$$$$$\  $$\    $$\ $$$$$$$$\ $$$$$$$\  $$\    $$\ $$$$$$\ $$$$$$$$\ $$\      $$\ 
$$  __$$\ $$ |   $$ |$$  _____|$$  __$$\ $$ |   $$ |\_$$  _|$$  _____|$$ | $\  $$ |
$$ /  $$ |$$ |   $$ |$$ |      $$ |  $$ |$$ |   $$ |  $$ |  $$ |      $$ |$$$\ $$ |
$$ |  $$ |\$$\  $$  |$$$$$\    $$$$$$$  |\$$\  $$  |  $$ |  $$$$$\    $$ $$ $$\$$ |
$$ |  $$ | \$$\$$  / $$  __|   $$  __$$<  \$$\$$  /   $$ |  $$  __|   $$$$  _$$$$ |
$$ |  $$ |  \$$$  /  $$ |      $$ |  $$ |  \$$$  /    $$ |  $$ |      $$$  / \$$$ |
 $$$$$$  |   \$  /   $$$$$$$$\ $$ |  $$ |   \$  /   $$$$$$\ $$$$$$$$\ $$  /   \$$ |
 \______/     \_/    \________|\__|  \__|    \_/    \______|\________|\__/     \__|
//============================================================================//
//                        FUNCTION CALLSTACK OVERVIEW                         //
//============================================================================//
//----------------------------------------------------------------------------//

//----------------------------------------------------//TIMERS
//----------------------------------------------------//VARS
//----------------------------------------------------//FUNCS
void m_mokibo_handle_reset(void);

ret_code_t m_mokibo_init(void);
void m_mokibo_process(void);
void m_mokibo_pause(void);
void m_mokibo_resume(void);
void m_mokibo_soft_reset(uint32_t param);

m_mokibo_status_type_t m_mokibo_get_status(void);
uint32_t m_mokibo_get_reset_reason(void);
ret_code_t m_mokibo_get_manufacture_info(m_mokibo_manufacture_info_t *pInfo);
uint8_t m_mokibo_get_stm8_fw_version(void);

void m_mokibo_set_keyboard_color(void);
void m_mokibo_set_touch_color(void);
ret_code_t m_mokibo_set_config_default(void);  

void Init_WDT(void); 
void Update_WDT(void);
void DBG_Update_WDT(uint32_t ms);
void m_mokibo_WDT_reload(void);
void m_mokibo_WDT_reset(uint32_t delay);


//----------------------------------------------------//INIT
m_mokibo_handle_reset()	
{
    log_resetreason();		
    nrf_power_resetreas_clear(nrf_power_resetreas_get());
}

m_mokibo_WDT_reset()

m_mokibo_set_config_default()

m_mokibo_init()
{
    m_mokibo_status = M_MOKIBO_INIT_STATUS;
    m_prv_mokibo_status = m_mokibo_status;
    m_sub_states 	= PREPARE_STATE;

    m_sub_sate_argv = 0;	
    drv_stm8_init();	
    m_led_init();
    m_timer_set(M_TIMER_ID_SLEEP_IF_IDLE, SLEEP_ON_IDLE_INTERVAL);	
    power_on_reset_parade_touch();	
}

m_mokibo_get_manufacture_info()
m_mokibo_get_stm8_fw_version()


//----------------------------------------------------//LOOP
for(;;)
{
    m_mokibo_process()//폴링
    {			
        m_event_arg_t 	argv;
        m_event_type_t 	event;

        ret_code_t isUpdate = m_event_update()
        {
            m_event = M_EVENT_NONE;
            update_status();
            {
                m_mokibo_status_type_t new = m_mokibo_get_status();//폴
                {
                    if(new != m_my_status)
                    {
                        m_my_status 	= new;
                        m_sub_status 	= SUB_STATUS_READY;
                    }
                }
            }
            m_event_handler[m_my_status]();
            {
                M_MOKIBO_INIT_STATUS=0,	
                M_MOKIBO_MANUFACTURE_STATUS,
                M_MOKIBO_KEYBOARD_STATUS,	//폴링
                M_MOKIBO_TOUCH_STATUS,			
                M_MOKIBO_UPGRADE_STATUS,	
                M_MOKIBO_SLEEP_STATUS,
                M_MOKIBO_TEST_STATUS,
                M_MOKIBO_BATTERY_CHECK_STATUS,
                M_MOKIBO_END_STATUS,
            }
            return (M_EVENT_NONE != m_event) ? 1 : 0;
        }

        if(isUpdate)
        {				
            m_event_get(&event, &argv);
            m_mokibo_handler[m_mokibo_status](event, argv);
            {
                M_MOKIBO_INIT_STATUS=0,			// 0
                M_MOKIBO_MANUFACTURE_STATUS,	// 1
                M_MOKIBO_KEYBOARD_STATUS,		// 2
                M_MOKIBO_TOUCH_STATUS,			// 3
                M_MOKIBO_UPGRADE_STATUS,		// 4
                M_MOKIBO_SLEEP_STATUS,			// 5
                M_MOKIBO_TEST_STATUS,			// 6
                M_MOKIBO_BATTERY_CHECK_STATUS,	// 7
                M_MOKIBO_END_STATUS,			// 8
            }
        }			
        m_timer_update_handler();						
        Update_WDT();
    }
}
//----------------------------------------------------//LOGS

//----------------------------------------------------------------------------*/





/*\      $$\  $$$$$$\  $$$$$$$\  $$\   $$\ $$\       $$$$$$$$\  $$$$$$\  
$$$\    $$$ |$$  __$$\ $$  __$$\ $$ |  $$ |$$ |      $$  _____|$$  __$$\ 
$$$$\  $$$$ |$$ /  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$ /  \__|
$$\$$\$$ $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$$$$\    \$$$$$$\  
$$ \$$$  $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$  __|    \____$$\ 
$$ |\$  /$$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$\   $$ |
$$ | \_/ $$ | $$$$$$  |$$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$$\ \$$$$$$  |
\__|     \__| \______/ \_______/  \______/ \________|\________| \______/ 
//============================================================================//
//                                 MODULES USED                               //
//============================================================================*/
#ifndef __M_MOKIBO_H
#define __M_MOKIBO_H




 /*$$$$\  $$\       $$$$$$\  $$$$$$$\   $$$$$$\  $$\                           
$$  __$$\ $$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                          
$$ /  \__|$$ |     $$ /  $$ |$$ |  $$ |$$ /  $$ |$$ |                          
$$ |$$$$\ $$ |     $$ |  $$ |$$$$$$$\ |$$$$$$$$ |$$ |                          
$$ |\_$$ |$$ |     $$ |  $$ |$$  __$$\ $$  __$$ |$$ |                          
$$ |  $$ |$$ |     $$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |                          
\$$$$$$  |$$$$$$$$\ $$$$$$  |$$$$$$$  |$$ |  $$ |$$$$$$$$\                     
 \______/ \________|\______/ \_______/ \__|  \__|\________|                    
 $$$$$$\   $$$$$$\  $$\   $$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$\   $$\ $$$$$$$$\ 
$$  __$$\ $$  __$$\ $$$\  $$ |$$  __$$\\__$$  __|$$  __$$\ $$$\  $$ |\__$$  __|
$$ /  \__|$$ /  $$ |$$$$\ $$ |$$ /  \__|  $$ |   $$ /  $$ |$$$$\ $$ |   $$ |   
$$ |      $$ |  $$ |$$ $$\$$ |\$$$$$$\    $$ |   $$$$$$$$ |$$ $$\$$ |   $$ |   
$$ |      $$ |  $$ |$$ \$$$$ | \____$$\   $$ |   $$  __$$ |$$ \$$$$ |   $$ |   
$$ |  $$\ $$ |  $$ |$$ |\$$$ |$$\   $$ |  $$ |   $$ |  $$ |$$ |\$$$ |   $$ |   
\$$$$$$  | $$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$ |  $$ |$$ | \$$ |   $$ |   
 \______/  \______/ \__|  \__| \______/   \__|   \__|  \__|\__|  \__|   \__|   
 //============================================================================//
//                                 GLOBAL CONSTANTS                            //
//============================================================================*/





/*$$$$\  $$\       $$$$$$\  $$$$$$$\   $$$$$$\  $$\                                     
$$  __$$\ $$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                             
$$ /  \__|$$ |     $$ /  $$ |$$ |  $$ |$$ /  $$ |$$ |                             
$$ |$$$$\ $$ |     $$ |  $$ |$$$$$$$\ |$$$$$$$$ |$$ |                             
$$ |\_$$ |$$ |     $$ |  $$ |$$  __$$\ $$  __$$ |$$ |                             
$$ |  $$ |$$ |     $$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |                             
\$$$$$$  |$$$$$$$$\ $$$$$$  |$$$$$$$  |$$ |  $$ |$$$$$$$$\                        
 \______/ \________|\______/ \_______/ \__|  \__|\________|                       
$$$$$$$$\ $$\     $$\ $$$$$$$\  $$$$$$$$\ $$$$$$$\  $$$$$$$$\ $$$$$$$$\  $$$$$$\  
\__$$  __|\$$\   $$  |$$  __$$\ $$  _____|$$  __$$\ $$  _____|$$  _____|$$  __$$\ 
   $$ |    \$$\ $$  / $$ |  $$ |$$ |      $$ |  $$ |$$ |      $$ |      $$ /  \__|
   $$ |     \$$$$  /  $$$$$$$  |$$$$$\    $$ |  $$ |$$$$$\    $$$$$\    \$$$$$$\  
   $$ |      \$$  /   $$  ____/ $$  __|   $$ |  $$ |$$  __|   $$  __|    \____$$\ 
   $$ |       $$ |    $$ |      $$ |      $$ |  $$ |$$ |      $$ |      $$\   $$ |
   $$ |       $$ |    $$ |      $$$$$$$$\ $$$$$$$  |$$$$$$$$\ $$ |      \$$$$$$  |
   \__|       \__|    \__|      \________|\_______/ \________|\__|       \______/ 
//============================================================================//
//                                 GLOBAL TYPEDEFS                            //
//============================================================================*/


//----------------------------------------------------//mokibo_status
// If you add new m_event_type, Add 
// 1) handler in m_mokibo_handler[] 
// 2) descriptions in ChangeState() function
typedef enum
{
    M_MOKIBO_INIT_STATUS=0,			// 0
    M_MOKIBO_MANUFACTURE_STATUS,	// 1
    M_MOKIBO_KEYBOARD_STATUS,		// 2
    M_MOKIBO_TOUCH_STATUS,			// 3
    M_MOKIBO_UPGRADE_STATUS,		// 4
    M_MOKIBO_SLEEP_STATUS,			// 5
    M_MOKIBO_TEST_STATUS,			// 6
    M_MOKIBO_BATTERY_CHECK_STATUS,	// 7
    M_MOKIBO_END_STATUS,			// 8
	//
    M_MOKIBO_MAX_STATUS,	// 9
} m_mokibo_status_type_t;



//----------------------------------------------------//manufacture
typedef struct
{
	uint16_t	serial;
	uint16_t	date;
} m_mokibo_manufacture_info_t;

typedef struct
{
	uint8_t	flag;
	uint8_t	argv[3];

} m_mokibo_test_info_t;

















/*\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ 
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\_*/



 /*$$$$\  $$\       $$$$$$\  $$$$$$$\   $$$$$$\  $$\                           
$$  __$$\ $$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                          
$$ /  \__|$$ |     $$ /  $$ |$$ |  $$ |$$ /  $$ |$$ |                          
$$ |$$$$\ $$ |     $$ |  $$ |$$$$$$$\ |$$$$$$$$ |$$ |                          
$$ |\_$$ |$$ |     $$ |  $$ |$$  __$$\ $$  __$$ |$$ |                          
$$ |  $$ |$$ |     $$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |                          
\$$$$$$  |$$$$$$$$\ $$$$$$  |$$$$$$$  |$$ |  $$ |$$$$$$$$\                     
 \______/ \________|\______/ \_______/ \__|  \__|\________|                    
$$\    $$\  $$$$$$\  $$$$$$$\  $$$$$$\  $$$$$$\  $$$$$$$\  $$\       $$$$$$$$\ 
$$ |   $$ |$$  __$$\ $$  __$$\ \_$$  _|$$  __$$\ $$  __$$\ $$ |      $$  _____|
$$ |   $$ |$$ /  $$ |$$ |  $$ |  $$ |  $$ /  $$ |$$ |  $$ |$$ |      $$ |      
\$$\  $$  |$$$$$$$$ |$$$$$$$  |  $$ |  $$$$$$$$ |$$$$$$$\ |$$ |      $$$$$\    
 \$$\$$  / $$  __$$ |$$  __$$<   $$ |  $$  __$$ |$$  __$$\ $$ |      $$  __|   
  \$$$  /  $$ |  $$ |$$ |  $$ |  $$ |  $$ |  $$ |$$ |  $$ |$$ |      $$ |      
   \$  /   $$ |  $$ |$$ |  $$ |$$$$$$\ $$ |  $$ |$$$$$$$  |$$$$$$$$\ $$$$$$$$\ 
    \_/    \__|  \__|\__|  \__|\______|\__|  \__|\_______/ \________|\________|
/*============================================================================//
//                                    VARIABLES                               //
//============================================================================*/

/*----------------------------------------------------------------------------//
//                                GLOBAL VARIABLES                            //
//----------------------------------------------------------------------------*/
extern m_mokibo_manufacture_info_t m_manufacture_info;
extern m_mokibo_test_info_t	m_test_info;
extern bool m_touchlock_automode;

/*----------------------------------------------------------------------------//
//                             GLOBAL DEBUG VARIABLES                         //
//----------------------------------------------------------------------------*/




/*$$$$\  $$\       $$$$$$\  $$$$$$$\   $$$$$$\  $$\                                     
$$  __$$\ $$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                             
$$ /  \__|$$ |     $$ /  $$ |$$ |  $$ |$$ /  $$ |$$ |                             
$$ |$$$$\ $$ |     $$ |  $$ |$$$$$$$\ |$$$$$$$$ |$$ |                             
$$ |\_$$ |$$ |     $$ |  $$ |$$  __$$\ $$  __$$ |$$ |                             
$$ |  $$ |$$ |     $$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |                             
\$$$$$$  |$$$$$$$$\ $$$$$$  |$$$$$$$  |$$ |  $$ |$$$$$$$$\                        
 \______/ \________|\______/ \_______/ \__|  \__|\________|                       
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\  $$\   $$\ $$$$$$$$\  $$$$$$\  $$$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\ $$ |  $$ |$$  _____|$$  __$$\ $$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|$$ |  $$ |$$ |      $$ /  $$ |$$ |  $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |      $$$$$$$$ |$$$$$\    $$$$$$$$ |$$ |  $$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |      $$  __$$ |$$  __|   $$  __$$ |$$ |  $$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\ $$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |$$ |  $$ |$$$$$$$$\ $$ |  $$ |$$$$$$$  |
\__|       \______/ \__|  \__| \______/ \__|  \__|\________|\__|  \__|\_______/ 
/*============================================================================//
//                                FUNCTION HEADS                              //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                            GLOBAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
extern void m_mokibo_handle_reset(void);
extern ret_code_t m_mokibo_init(void);
extern void m_mokibo_process(void);
extern void m_mokibo_pause(void);
extern void m_mokibo_resume(void);
extern void m_mokibo_soft_reset(uint32_t param);

extern m_mokibo_status_type_t m_mokibo_get_status(void);
extern uint32_t m_mokibo_get_reset_reason(void);
extern ret_code_t m_mokibo_get_manufacture_info(m_mokibo_manufacture_info_t *pInfo);
extern uint8_t m_mokibo_get_stm8_fw_version(void);

extern void m_mokibo_set_keyboard_color(void);
extern void m_mokibo_set_touch_color(void);
extern ret_code_t m_mokibo_set_config_default(void);  

extern void Init_WDT(void);
extern void Update_WDT(void);
extern void m_mokibo_WDT_reload(void);
#if (MOKIBO_WDT_ENABLED)
    extern void m_mokibo_WDT_reset(uint32_t delay);
#endif


/*----------------------------------------------------------------------------//
//                         GLOBAL DEBUG FUNCTION HEADS                        //
//----------------------------------------------------------------------------*/
void DBG_Update_WDT(uint32_t ms);




/*\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ 
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\_*/





#endif // __M_MOKIBO_H 

