/// \file m_mokibo.c
/// This file contains all required configurations for mokibo
/*================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================*/






















/*\      $$\  $$$$$$\  $$$$$$$\  $$\   $$\ $$\       $$$$$$$$\  $$$$$$\  
$$$\    $$$ |$$  __$$\ $$  __$$\ $$ |  $$ |$$ |      $$  _____|$$  __$$\ 
$$$$\  $$$$ |$$ /  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$ /  \__|
$$\$$\$$ $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$$$$\    \$$$$$$\  
$$ \$$$  $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$  __|    \____$$\ 
$$ |\$  /$$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$\   $$ |
$$ | \_/ $$ | $$$$$$  |$$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$$\ \$$$$$$  |
\__|     \__| \______/ \_______/  \______/ \________|\________| \______/ 
//============================================================================//
//                              LOCAL MODULEL USED                            //
//============================================================================*/
#include <nrf_pwr_mgmt.h>

#include "sdk_config.h" // mokibo_config.h && sdk_config.h
#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
	// should be placed after "sdk_config.h"
	#include "SEGGER_RTT.h"
#endif

#include "sdk_common.h"
#include "app_error.h"
#include "nrf_power.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_drv_wdt.h"
#include "nrf_soc.h"
#include "nrf_delay.h"

#include "drv_twi.h"
#include "drv_stm8.h"
#include "drv_keyboard.h"
#include "drv_cypress.h"
#include "drv_fds.h"

#include "m_event.h"        
#include "m_led.h"        
#include "m_comm.h"        
#include "m_comm_adv.h"        
#include "m_touch.h"        
#include "m_keyboard.h"        
#include "m_timer.h"        
#include "m_util.h"        
#include "m_dongle.h"        
#include "m_mokibo.h"        
#include "m_flimbs.h"






/*$$$$\   $$$$$$\  $$\   $$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$\   $$\ $$$$$$$$\ 
$$  __$$\ $$  __$$\ $$$\  $$ |$$  __$$\\__$$  __|$$  __$$\ $$$\  $$ |\__$$  __|
$$ /  \__|$$ /  $$ |$$$$\ $$ |$$ /  \__|  $$ |   $$ /  $$ |$$$$\ $$ |   $$ |   
$$ |      $$ |  $$ |$$ $$\$$ |\$$$$$$\    $$ |   $$$$$$$$ |$$ $$\$$ |   $$ |   
$$ |      $$ |  $$ |$$ \$$$$ | \____$$\   $$ |   $$  __$$ |$$ \$$$$ |   $$ |   
$$ |  $$\ $$ |  $$ |$$ |\$$$ |$$\   $$ |  $$ |   $$ |  $$ |$$ |\$$$ |   $$ |   
\$$$$$$  | $$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$ |  $$ |$$ | \$$ |   $$ |   
 \______/  \______/ \__|  \__| \______/   \__|   \__|  \__|\__|  \__|   \__|   
//============================================================================//
//                                LOCAL CONSTANTS                             //
//============================================================================*/




/*$$$$$$\ $$\     $$\ $$$$$$$\  $$$$$$$$\ $$$$$$$\  $$$$$$$$\ $$$$$$$$\  $$$$$$\  
\__$$  __|\$$\   $$  |$$  __$$\ $$  _____|$$  __$$\ $$  _____|$$  _____|$$  __$$\ 
   $$ |    \$$\ $$  / $$ |  $$ |$$ |      $$ |  $$ |$$ |      $$ |      $$ /  \__|
   $$ |     \$$$$  /  $$$$$$$  |$$$$$\    $$ |  $$ |$$$$$\    $$$$$\    \$$$$$$\  
   $$ |      \$$  /   $$  ____/ $$  __|   $$ |  $$ |$$  __|   $$  __|    \____$$\ 
   $$ |       $$ |    $$ |      $$ |      $$ |  $$ |$$ |      $$ |      $$\   $$ |
   $$ |       $$ |    $$ |      $$$$$$$$\ $$$$$$$  |$$$$$$$$\ $$ |      \$$$$$$  |
   \__|       \__|    \__|      \________|\_______/ \________|\__|       \______/ 
//============================================================================//
//                                LOCAL TYPEDEFS                              //
//============================================================================*/
typedef enum
{
    PREPARE_STATE,
    PROCESS_STATE,
    FINISH_STATE,
    SUB_STATE_MAX,
} sub_state_type_t;
















/*\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ 
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\_*/


/*\    $$\  $$$$$$\  $$$$$$$\  $$$$$$\  $$$$$$\  $$$$$$$\  $$\       $$$$$$$$\ 
$$ |   $$ |$$  __$$\ $$  __$$\ \_$$  _|$$  __$$\ $$  __$$\ $$ |      $$  _____|
$$ |   $$ |$$ /  $$ |$$ |  $$ |  $$ |  $$ /  $$ |$$ |  $$ |$$ |      $$ |      
\$$\  $$  |$$$$$$$$ |$$$$$$$  |  $$ |  $$$$$$$$ |$$$$$$$\ |$$ |      $$$$$\    
 \$$\$$  / $$  __$$ |$$  __$$<   $$ |  $$  __$$ |$$  __$$\ $$ |      $$  __|   
  \$$$  /  $$ |  $$ |$$ |  $$ |  $$ |  $$ |  $$ |$$ |  $$ |$$ |      $$ |      
   \$  /   $$ |  $$ |$$ |  $$ |$$$$$$\ $$ |  $$ |$$$$$$$  |$$$$$$$$\ $$$$$$$$\ 
    \_/    \__|  \__|\__|  \__|\______|\__|  \__|\_______/ \________|\_______/
//============================================================================//
//                                   VARIABLES                                //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                                GLOBAL VARIABLES                            //
//----------------------------------------------------------------------------*/
m_mokibo_manufacture_info_t 	m_manufacture_info;
m_mokibo_test_info_t			m_test_info;
bool	m_touchlock_automode = false;

/*----------------------------------------------------------------------------//
//                                 LOCAL VARIABLES                            //
//----------------------------------------------------------------------------*/
static m_mokibo_status_type_t 	m_mokibo_status;
static m_mokibo_status_type_t 	m_prv_mokibo_status;
static sub_state_type_t 		m_sub_states;
static uint8_t 					m_sub_sate_argv;
static uint32_t 				m_reset_reason;
static uint32_t 				m_reset_param;
static uint8_t m_parade_touch_power_was_off=1;


/*----------------------------------------------------------------------------//
//                                 DEBUG VARIABLES                            //
//----------------------------------------------------------------------------*/
#ifdef M_RESET_PARADE_CHIP // 키 입력횟수가 어느정도 차면 리셋한다.
	static uint8_t 					m_key_pressed_cnt;
#endif

#if (MOKIBO_WDT_ENABLED)
	static bool 					m_mokibo_WTD_run_reset = true;
	static nrf_drv_wdt_channel_id 	m_mokibo_WTD_ch;
#endif


/*----------------------------------------------------------------------------//
//                                UNUSED VARIABLES                            //
//----------------------------------------------------------------------------*/
//volatile m_mokibo_mode_t 		m_mokibo_mode = M_MOKIBO_BLE;




/*$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\  $$\   $$\ $$$$$$$$\  $$$$$$\  $$$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\ $$ |  $$ |$$  _____|$$  __$$\ $$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|$$ |  $$ |$$ |      $$ /  $$ |$$ |  $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |      $$$$$$$$ |$$$$$\    $$$$$$$$ |$$ |  $$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |      $$  __$$ |$$  __|   $$  __$$ |$$ |  $$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\ $$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |$$ |  $$ |$$$$$$$$\ $$ |  $$ |$$$$$$$  |
\__|       \______/ \__|  \__| \______/ \__|  \__|\________|\__|  \__|\_______/ 
//============================================================================//
//                                   FUNCHEAD                                 //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                            GLOBAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
void m_mokibo_handle_reset(void);
ret_code_t m_mokibo_init(void);
void m_mokibo_process(void);
void m_mokibo_pause(void);
void m_mokibo_resume(void);
void m_mokibo_soft_reset(uint32_t param);

m_mokibo_status_type_t m_mokibo_get_status(void);
uint32_t m_mokibo_get_reset_reason(void);
ret_code_t m_mokibo_get_manufacture_info(m_mokibo_manufacture_info_t *pInfo);
uint8_t m_mokibo_get_stm8_fw_version(void);

void m_mokibo_set_keyboard_color(void);
void m_mokibo_set_touch_color(void);
ret_code_t m_mokibo_set_config_default(void);  

void Init_WDT(void);
void Update_WDT(void);
void m_mokibo_WDT_reload(void);
#if (MOKIBO_WDT_ENABLED)
    void m_mokibo_WDT_reset(uint32_t delay);
#endif


/*----------------------------------------------------------------------------//
//                             LOCAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
static void ChangeState(m_mokibo_status_type_t new);
static void keyboard_scan_timeout_handler(void);
static void PowerOnDisplay(void);
static void PowerOffnDisplay(void);
static void SetKeyboardColor(void);
static void SetTouchColor(void);
static uint8_t FilterKeys(uint8_t *dst, const uint8_t *src, uint8_t size);
static void power_on_reset_parade_touch(void);


/*----------------------------------------------------------------------------//
//                            POINTER FUNCTION HEADS                          //
//----------------------------------------------------------------------------*/
typedef void (*STATE_MACHINE)(m_event_type_t evt, m_event_arg_t argv);
static void InitHandler(m_event_type_t evt, m_event_arg_t argv);
static void ManufactureHandler(m_event_type_t evt, m_event_arg_t argv);
static void KeyboardHandler(m_event_type_t evt, m_event_arg_t argv);
static void TouchHandler(m_event_type_t evt, m_event_arg_t argv);
static void UpgradeHandler(m_event_type_t evt, m_event_arg_t argv);
static void SleepHandler(m_event_type_t evt, m_event_arg_t argv);
static void TestHandler(m_event_type_t evt, m_event_arg_t argv);
static void BatteryCheckHandler(m_event_type_t evt, m_event_arg_t argv);
static void EndHandler(m_event_type_t evt, m_event_arg_t argv);
static const STATE_MACHINE m_mokibo_handler[M_MOKIBO_MAX_STATUS] = \
{ \
		InitHandler, \
		ManufactureHandler, \
		KeyboardHandler, \
		TouchHandler, \
		UpgradeHandler, \
		SleepHandler, \
		TestHandler, \
		BatteryCheckHandler, \
		EndHandler, \
};

typedef void (*SUB_STATES_MACHINE)(void);
static void PrepareSleep(void);
static void ProcessSleep(void);
static void FinishSleep(void);
static const SUB_STATES_MACHINE m_sleep_handler[] = \
{ \
		PrepareSleep, \
		ProcessSleep, \
		FinishSleep, \
};

static void PrepareBattery(void);
static void ProcessBattery(void);
static void FinishBattery(void);
static const SUB_STATES_MACHINE m_battery_handler[] = \
{ \
		PrepareBattery, \
		ProcessBattery, \
		FinishBattery, \
};

/*----------------------------------------------------------------------------//
//                             DEBUG FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
static void log_resetreason(void);

/*----------------------------------------------------------------------------//
//                             UNUSED FUNCTION HEADS                          //
//----------------------------------------------------------------------------*/



/*\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ 
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\_*/























/*$$$$\  $$\       $$$$$$\  $$$$$$$\   $$$$$$\  $$\                                     
$$  __$$\ $$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                                  
$$ /  \__|$$ |     $$ /  $$ |$$ |  $$ |$$ /  $$ |$$ |                                  
$$ |$$$$\ $$ |     $$ |  $$ |$$$$$$$\ |$$$$$$$$ |$$ |                                  
$$ |\_$$ |$$ |     $$ |  $$ |$$  __$$\ $$  __$$ |$$ |                                  
$$ |  $$ |$$ |     $$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |                                  
\$$$$$$  |$$$$$$$$\ $$$$$$  |$$$$$$$  |$$ |  $$ |$$$$$$$$\                             
 \______/ \________|\______/ \_______/ \__|  \__|\________|                            
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\  $$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |$$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |$$ /  \__|
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |\$$$$$$\  
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ | \____$$\ 
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |$$\   $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |\$$$$$$  |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__| \______/ 
//============================================================================//
//                                 GLOBAL FUNCTIONS                           //
//============================================================================*/

/*==============================================================================//
	Function name : m_mokibo_handle_reset()
//==============================================================================*/
void m_mokibo_handle_reset(void)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
	log_resetreason();
	// TODO: why?? not working
	//sd_power_gpregret_get(1, &m_reset_param);
	
	________________________________LOG_mMokibo_showInitInfo(1,"* Reset param: 0x%04x \n", m_reset_param);
	
	nrf_power_resetreas_clear(nrf_power_resetreas_get());	
}




/*==============================================================================//
	Function name : m_mokibo_init()
//==============================================================================*/
ret_code_t m_mokibo_init(void)
{	
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
	ret_code_t err_code;

	m_mokibo_status = M_MOKIBO_INIT_STATUS;
	m_prv_mokibo_status = m_mokibo_status;
	m_sub_states 	= PREPARE_STATE;
	m_sub_sate_argv = 0;
	
	drv_twi_init(true);
	
	drv_stm8_init();
	
	//err_code = m_led_init();
	//APP_ERROR_CHECK(err_code);

	err_code = m_keyboard_init(); //APP_ERROR_CHECK(err_code); // 濡쒓렇媛 硫덉떠?덈떎媛 ?ш린???ㅼ떆 ?쒖옉?쒕떎.
 	err_code = m_touch_init(); //APP_ERROR_CHECK(err_code);	
	//power_on_reset_parade_touch();//resume_parade_touch();
	//nrf_delay_ms(100); // STM8과  Cypress Touch chip의 초기화가 완료되어, i2c 통신이 가능한 상태에 있어야 한다.

	m_timer_set(M_TIMER_ID_SLEEP_IF_IDLE, SLEEP_ON_IDLE_INTERVAL);// ASUME: kbd & Touch are busy ...
	m_timer_set(M_TIMER_ID_CHARGER_CHECK,M_TIMER_TIME_SECOND(5));
	err_code = m_led_init();

	return NRF_SUCCESS;
}






//================================================================================
//	Function name : m_mokibo_process()
//================================================================================
void m_mokibo_process(void)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");//POLLING

	m_event_arg_t 	argv;
	m_event_type_t 	event;

	TIME_TRACE_START();
		
	if(true == m_event_update())
	{				
		m_event_get(&event, &argv);// m_event, m_arg를 복사해 오고, (m_event,m_arg)는 clear한다.
		
		m_mokibo_handler[m_mokibo_status](event, argv); // <--- 'm_mokibo_status'는 ChangeState()에 의하여 갱신된다.	
	}

	// Call time-out handler
	m_timer_update_handler();

	TIME_TRACE_END(10);

	// Update watch dog
	Update_WDT();

}

//================================================================================
//	Function name : m_mokibo_pause()
//================================================================================
void m_mokibo_pause(void)
{
	// TODO: Add code if neccessary
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
	DEBUG("TODO");
}

//================================================================================
//	Function name : m_mokibo_resume()
//================================================================================
void m_mokibo_resume(void)
{
	// TODO: Add code if neccessary
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
	DEBUG("TODO");
}

//================================================================================
//	Function name : m_mokibo_soft_reset()
//================================================================================
void m_mokibo_soft_reset(uint32_t param)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(parm:%d)",param);
	// TODO: why?? not working
	//sd_power_gpregret_clr(1, 0xFFFFFFFF);
	//sd_power_gpregret_set(1, param);
	#if defined(M_MOKIBO_RESET_TRACE)
		m_reset_param = param;
	#endif
	NVIC_SystemReset();
}









//================================================================================
//	Function name : m_mokibo_get_status()
//================================================================================
m_mokibo_status_type_t m_mokibo_get_status(void)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
	return m_mokibo_status;
}


//================================================================================
//	Function name : m_mokibo_get_reset_reason()
//================================================================================
uint32_t m_mokibo_get_reset_reason(void)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
	return m_reset_reason;
}


//================================================================================
//	Function name : m_mokibo_get_manufacture_info()
//================================================================================
ret_code_t m_mokibo_get_manufacture_info(m_mokibo_manufacture_info_t *pInfo)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
	if (pInfo == NULL)
	{
			return NRF_ERROR_NULL;
	}

	memcpy(pInfo, &m_manufacture_info, sizeof(m_mokibo_manufacture_info_t));
	return NRF_SUCCESS;
}


//================================================================================
//	Function name : m_mokibo_get_stm8_fw_version()
//================================================================================
uint8_t m_mokibo_get_stm8_fw_version()
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
	ret_code_t err_code;
	uint8_t	stm8_fw_ver = 0xFF;

	err_code = drv_stm8_read(STM8_READ_VERSION, &stm8_fw_ver);
	PRINT_IF_ERROR("drv_stm8_read(STM8_READ_VERSION)", err_code);
 	return stm8_fw_ver;
}


//================================================================================
//	Function name : m_mokibo_set_keyboard_color()
//================================================================================
void m_mokibo_set_keyboard_color(void)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
	SetKeyboardColor();
}


//================================================================================
//	Function name : m_mokibo_set_touch_color()
//================================================================================
void m_mokibo_set_touch_color(void)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
	SetTouchColor();
}


//================================================================================
//	Function name : m_mokibo_set_config_default()
//================================================================================
ret_code_t m_mokibo_set_config_default(void)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
	ret_code_t rv;
	//uint32_t  val;
	//m_mokibo_test_info_t test_data;		// <-- m_test_info
	//m_mokibo_manufacture_info_t man_data; // <-- m_manufacture_info

	//-----------------------------------------------------------------------------------------------------------------------------------
//	val = DRV_FDS_CONFIG_VALID;
//	//rv = drv_fds_write(DRV_FDS_PAGE_CONFIG_VALID, (uint8_t *)&val,  sizeof(val));
//	//PRINT_IF_ERROR("drv_fds_write()", rv);
//	DEB("......Writing DRV_FDS_CONFIG_VALID(0x%x) into Flash........\n",DRV_FDS_CONFIG_VALID);
//	rv= fds_write_stanley(FDS_FILE_ID_STANLEY, FDS_RECORD_KEY_M_CONFIG_VALID, (uint8_t *)&val, sizeof(val) );

	//-----------------------------------------------------------------------------------------------------------------------------------
	memset(&m_manufacture_info, 0, sizeof(m_manufacture_info));
	m_manufacture_info.serial = 0xAAFF;
	m_manufacture_info.date = 0xAAFF;

	//rv = drv_fds_write(DRV_FDS_PAGE_MANUFACTURE, (uint8_t *)&man_data,  sizeof(man_data));
	//PRINT_IF_ERROR("drv_fds_write()", rv);
	//rv= fds_write_stanley(FDS_FILE_ID_STANLEY, FDS_RECORD_KEY_M_MANUFACTURE_INFO, (uint8_t *)&m_manufacture_info, sizeof(m_mokibo_manufacture_info_t) );

//	//-----------------------------------------------------------------------------------------------------------------------------------
//	memset(&m_test_info, 0, sizeof(m_mokibo_test_info_t));
//	//rv = drv_fds_write(DRV_FDS_PAGE_TEST, (uint8_t *)&test_data,  sizeof(test_data));
//	//PRINT_IF_ERROR("drv_fds_write()", rv);
//	rv= fds_write_stanley(FDS_FILE_ID_STANLEY, FDS_RECORD_KEY_M_TEST_INFO, (uint8_t *)&m_test_info, sizeof(m_mokibo_test_info_t) );

	//-----------------------------------------------------------------------------------------------------------------------------------
	return rv;
}


//================================================================================
//	Function name : Init_WDT()
//================================================================================
void Init_WDT(void)
{	
	#if (MOKIBO_WDT_ENABLED)	
		________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
		ret_code_t err_code;		
		m_mokibo_WTD_run_reset = false;

		// Main WDT is configured by WDT_ENABLED in sdk_config.h
			nrf_drv_wdt_config_t config = NRF_DRV_WDT_DEAFULT_CONFIG;

		/**< WDT reload value in ms. */
		config.reload_value = WTD_RELOAD;
			err_code = nrf_drv_wdt_init(&config, NULL);
			APP_ERROR_CHECK(err_code);

			err_code = nrf_drv_wdt_channel_alloc(&m_mokibo_WTD_ch);
			APP_ERROR_CHECK(err_code);
			
		nrf_drv_wdt_enable();

		#if defined(M_MOKIBO_WDT_TRACE)
			DEB("WatchDog(%d ms)is initialized\n", config.reload_value);
		#endif
	#endif
}

//================================================================================
//	Function name : Update_WDT()   
//================================================================================
void Update_WDT(void)//Polling Thread
{			
	#if (MOKIBO_WDT_ENABLED)
	//________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
	if(!m_mokibo_WTD_run_reset)
	{		
		nrf_drv_wdt_channel_feed(m_mokibo_WTD_ch);
	}
	#endif
}


//================================================================================
//	Function name : m_mokibo_WDT_reload()
//================================================================================
void m_mokibo_WDT_reload(void)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
	Update_WDT();
}

//================================================================================
//	Function name : m_mokibo_WDT_reset()
//================================================================================
#if (MOKIBO_WDT_ENABLED)
void m_mokibo_WDT_reset(uint32_t delay)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(delay:%d)",delay);
	uint32_t remain;
	
	m_timer_stop();

	remain = delay;

	while(1)
	{
		if(remain)
		{
			remain--;
		}
		else
		{
			m_mokibo_WTD_run_reset = 1;
		}

		nrf_delay_ms(1);
		
		Update_WDT();

		NRF_LOG_PROCESS();
	}
}
#endif










/*\       $$$$$$\   $$$$$$\   $$$$$$\  $$\                                             
$$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                                            
$$ |     $$ /  $$ |$$ /  \__|$$ /  $$ |$$ |                                            
$$ |     $$ |  $$ |$$ |      $$$$$$$$ |$$ |                                            
$$ |     $$ |  $$ |$$ |      $$  __$$ |$$ |                                            
$$ |     $$ |  $$ |$$ |  $$\ $$ |  $$ |$$ |                                            
$$$$$$$$\ $$$$$$  |\$$$$$$  |$$ |  $$ |$$$$$$$$\                                       
\________|\______/  \______/ \__|  \__|\________|                                      
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\  $$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |$$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |$$ /  \__|
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |\$$$$$$\  
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ | \____$$\ 
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |$$\   $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |\$$$$$$  |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__| \______/ 
//============================================================================//
//                                 LOCAL FUNCTIONS                            //
//============================================================================*/






/*============================================================================//
//                            ChangeState()
//============================================================================*/
/*----------------------------------------------------------------------------//
00000000_flimbs
	State 가 바뀔때마다 호출되어 특정 스테이트에 어던 동작을 수행하도록 하고나서 state를 바꾼다.
	기존 m_mokibo_state 를 new 로 덮어쓴다. substatus를 초기화한다. substate_argv 를 초기화한다.
//----------------------------------------------------------------------------*/
static void ChangeState(m_mokibo_status_type_t new)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(m_mokibo_status_type_t new:%d)",new);
	m_comm_evt_arg_t val;
	
	if(m_mokibo_status != new)
	{	
		________________________________LOG_mMokibo_mokiboStatus(0,"m_mokibo_state:[%d -> %d] , m_sub_state:[%d] (INIT:0,MANU:1,KBD:2,TOUCH:3,UPGRADE:4,SLEEP:5,TEST:6,BATT:7,END:8)",m_mokibo_status,new,m_sub_states);		

				//[키보드 스테이트 -> 어떤 스테이트] 키 눌림 방지(터치모드에선 그대로 유지)
		if(m_mokibo_status == M_MOKIBO_KEYBOARD_STATUS && new != M_MOKIBO_TOUCH_STATUS)
		{		
			if ( (m_comm_ble_get_status() != BLE_CONN_HANDLE_INVALID) || (m_comm_get_mode() == M_COMM_MODE_GAZELL) )
			{					
				m_comm_clear_keyboard();
			}			
		}
		
		//[터치 스테이트를 -> 어떤 스테이트] 사이프레스 베이스라인 리셋.
		if(M_MOKIBO_TOUCH_STATUS == m_mokibo_status)
		{
			if ( (m_comm_ble_get_status() != BLE_CONN_HANDLE_INVALID) || (m_comm_get_mode() == M_COMM_MODE_GAZELL) )
			{				
				memset(&val, 0, sizeof(val)); // clear --> all button release
				m_comm_send_event(M_COMM_EVENT_CLICK_CTRL, val);
			}			
		}

		
		// [어떤 스테이트 -> 키보드 스테이트]	사이프레스 고스트 방지
		if(M_MOKIBO_KEYBOARD_STATUS == new)
		{
			#ifdef M_RESET_PARADE_CHIP 
				m_key_pressed_cnt = 0;
			#endif
			SetKeyboardColor();
			if(M_COMM_ADV_STATUS_IN_ADV != m_comm_adv_get_status())
			{
				m_led_set_status(M_LED_OFF);
			}

			SYSTEM_MSG("\n\n\n\n========= Keyboard Mode ==========\n");

			SYSTEM_MSG("### drv_cypress_init_Baseline() ### \n");
			#ifdef BASELINEINIT_USE
				drv_cypress_init_Baseline();
			#endif
			
			// touch 잔재가 있으면, suspend_scanning()이 듣지 않는다...
			SYSTEM_MSG("### clearAll_CypressTouchData() ### \n");
						clearAll_CypressTouchData(); //in ChangeState(키보드 모드 들어갈때 )
			
			SYSTEM_MSG("### drv_cypress_init_Baseline() ### \n");
			#ifdef BASELINEINIT_USE
				drv_cypress_init_Baseline();
			#endif

			// cf. drv_cypress_req_resume_scanning
			SYSTEM_MSG("### drv_cypress_req_suspend_scanning() ### \n");
			drv_cypress_req_suspend_scanning();
			drv_cypress_req_suspend_scanning();
			
		}

		// [어떤 스테이트 -> 터치 스테이트] : 
		if(M_MOKIBO_TOUCH_STATUS == new)
		{	
			SYSTEM_MSG("\n\n\n\n========= Touch Mode ==========\n");

			//SYSTEM_MSG("### drv_cypress_init_Baseline() ### \n");
			#ifdef BASELINEINIT_USE
				drv_cypress_init_Baseline();
			#endif

			//-------------------------------------------------------------------------------------------------------------------------------
			clear_m_touch_db();	// by Stanley, 2018.11.26
			//-------------------------------------------------------------------------------------------------------------------------------

			//SYSTEM_MSG("### drv_cypress_init_Baseline() ### \n");
			#ifdef BASELINEINIT_USE
				drv_cypress_init_Baseline();
			#endif

			//-------------------------------------------------------------------------------------------------------------------------------
			// cf. drv_cypress_req_suspend_scanning
			SYSTEM_MSG("### drv_cypress_req_resume_scanning() ### \n");
			drv_cypress_req_resume_scanning();
			//-------------------------------------------------------------------------------------------------------------------------------

			//SYSTEM_MSG("### drv_cypress_init_Baseline() ### \n");
			#ifdef BASELINEINIT_USE
				drv_cypress_init_Baseline();
				drv_cypress_init_Baseline();
			#endif
			
			if(M_TOUCH_CYPRESS_ACTIVE_AREA_WHOLE == m_touch_mode_get())
			{
				#ifdef FLIMBS_20190102_TOUCH_LOCK_LED_DEPENDS_ON_BTCH
					SetTouchColor();
				#else
					m_led_set_color(TOUCH_LOCK_MODE_COLOR);
				#endif
			}
			else
			{
				SetTouchColor();
			}

			if(M_COMM_ADV_STATUS_IN_ADV != m_comm_adv_get_status())
			{
				switch(m_comm_get_mode())
				{
					case  M_COMM_MODE_BLE :
						if(m_comm_ble_get_status() != BLE_CONN_HANDLE_INVALID)
						{
							m_led_set_status(M_LED_ON);
						}
						else
						{
							m_led_init();
							nrf_delay_ms(10);
							SetTouchColor();
							nrf_delay_ms(10);
							m_led_set_status(M_LED_DIMMING_OUT);
						}
					break;

					case  M_COMM_MODE_GAZELL :
						m_led_set_status(M_LED_ON);
					break;
				}
			}
		}
		//-------------------------------------------------------------------------------------------------------------------------------
		// sleep 모드 들어갈때 
		if(M_MOKIBO_SLEEP_STATUS == new)
		{
			SYSTEM_MSG("### Going to sleep mode ###\n");

			// Stop communication module
			m_comm_cmd(M_COMM_CMD_STOP, 0);

//			// Start BLE for entering sleep mode
//			if(is_DONGLE_channel(m_config.config.selected_ch))
//			{
//				rv = m_dongle_stop();
//				PRINT_IF_ERROR("m_dongle_stop()", rv);
//				
//				rv = m_comm_ble_start();
//				PRINT_IF_ERROR("m_comm_ble_start()", rv);
//			}			
		}

		m_prv_mokibo_status = m_mokibo_status;	// Remeber the current as previous
		m_mokibo_status		= new;				// Remeber the new as current
		m_sub_states 		= PREPARE_STATE;	// Reset m_sub_states
		m_sub_sate_argv		= 0;				// Reset m_sub_sate_argv		
	}
	
	if(new == M_MOKIBO_TOUCH_STATUS)
	{
			if(BLE_CONN_HANDLE_INVALID == m_conn_handle)
			{
				// Bonding ?•ë³´ê°€ ?€?¥ë˜???ˆìœ¼ë©´ì„œ, ì¦??˜ì–´ë§ì´ ?˜ì–´ ?ˆëŠ” ?íƒœ?¸ë°
				if((M_COMM_ADV_BOND_INVALID != m_comm_adv_get_addr(m_config.config.selected_ch)) 
						&& (M_COMM_ADV_BOND_READY != m_comm_adv_get_addr(m_config.config.selected_ch)))
				{
					// Advertising????챗쨀혻 ??챙? ?힋챘?뮤ㅓヂ??째챗짼째 ?흹챙탑??
					switch(m_comm_adv_get_status())
					{
					case M_COMM_ADV_STATUS_NONE:
					case M_COMM_ADV_STATUS_READY:
					case M_COMM_ADV_STATUS_CONNECTED:
					case M_COMM_ADV_STATUS_CONNECTRUN:
						update_device_name(m_config.config.selected_ch); // --> sd_ble_gap_device_name_set('MOKIBO BT2') 

						DEB("%s() start advertising", __func__);
						m_comm_adv_start(m_config.config.selected_ch);
						
						#if defined(M_COMM_CH_TRACE)
						DEBUG("connecting to my_ch:%d", m_config.config.selected_ch);
						#endif
						
						break;
					}
				}
			}
	}
}










/*$$$$$\   $$$$$$\  $$$$$$\ $$\   $$\ $$$$$$$$\ $$$$$$$$\ $$$$$$$\                     
$$  __$$\ $$  __$$\ \_$$  _|$$$\  $$ |\__$$  __|$$  _____|$$  __$$\                    
$$ |  $$ |$$ /  $$ |  $$ |  $$$$\ $$ |   $$ |   $$ |      $$ |  $$ |                   
$$$$$$$  |$$ |  $$ |  $$ |  $$ $$\$$ |   $$ |   $$$$$\    $$$$$$$  |                   
$$  ____/ $$ |  $$ |  $$ |  $$ \$$$$ |   $$ |   $$  __|   $$  __$$<                    
$$ |      $$ |  $$ |  $$ |  $$ |\$$$ |   $$ |   $$ |      $$ |  $$ |                   
$$ |       $$$$$$  |$$$$$$\ $$ | \$$ |   $$ |   $$$$$$$$\ $$ |  $$ |                   
\__|       \______/ \______|\__|  \__|   \__|   \________|\__|  \__|                   
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\  $$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |$$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |$$ /  \__|
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |\$$$$$$\  
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ | \____$$\ 
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |$$\   $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |\$$$$$$  |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__| \______/ 
//============================================================================//
//                       POINTER FUCTIONS				                      				//
//============================================================================*/



//================================================================================
//	Function name : InitHandler()	: 	by: init_event_detect --> M_EVENT_STARTUP -->
//================================================================================
static void InitHandler(m_event_type_t evt, m_event_arg_t argv)
{	
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(m_event_type_t evt:%d, m_event_arg_t argv:%d)",evt, argv);
	m_mokibo_test_info_t test_data;
	static uint8_t is_started = false;
			
	if(is_started)
	{
		PowerOnDisplay();
	}

#if	0	// Blocked by jaehong, 2019/06/24
	// TT_INT 핀이 로우 상태일때(즉, 터치 데이터가 있는 경우)
	// TouchHandler에서 데이터를 읽도록 한다.
	if(evt & M_EVENT_TOUCH_MODE_SCAN_TIME 
		&& M_COMM_ADV_STATUS_CONNECTED == m_comm_adv_get_status()
		&& m_conn_handle != BLE_CONN_HANDLE_INVALID
		&& M_COMM_ADV_BOND_INVALID != m_comm_adv_get_addr(m_config.config.selected_ch)
		&& M_COMM_ADV_BOND_READY != m_comm_adv_get_addr(m_config.config.selected_ch)
	)
	{
			// == 현재는, keyboard모드일 경우에도 터치를 읽음 ===
		// 목적은 터치칩의 터치 데이터가 누적되어,
		// 이 데이터가 터치 모드로 진입할때 전송되어 오동작 방지.
		// 추후 performance, watchdog, 터치 모드 진입시누적데이터 처림(??) 
		// 또는 interrupt 방식등을 고려하여 변경 가능함
		//if(!cypress_read_TT_INT())
		//m_touch_scan_handler();
	}
#endif
	
	if(evt & M_EVENT_KEYBOAD_MODE_SCAN_TIME)
	{
		keyboard_scan_timeout_handler();
	}
	
	if(evt & M_EVENT_STARTUP)
	{		
		is_started = true;
		m_led_set_status(M_LED_OFF);
		m_timer_set(M_TIMER_ID_TEMP, POWER_ON_COLOR_INTERVAL);
	}
	
	// (1) on M_TIMER_ID_EVENT_MODULE
	// (2) on DRV_KEYBOARD_HOTKEY_TEST_ENTER)
	if(evt & M_EVENT_MODE_CHANGED)
	{
		if(argv.new_mode.manufacure) 
		{
			//DEB("INVOKE: if(argv.new_mode.manufacure): ChangeState(M_MOKIBO_MANUFACTURE_STATUS) \n");
			ChangeState(M_MOKIBO_MANUFACTURE_STATUS);
		}
	
		if(argv.new_mode.keyboard) 
		{
				// 정상 키보드 모드 진입
				ChangeState(M_MOKIBO_KEYBOARD_STATUS); 
		}
				
		// [Fn + 7] : DRV_KEYBOARD_HOTKEY_TEST_ENTER <-- (see: m_event.c line 506)
		if(argv.new_mode.test)
		{			
			memset(&test_data, 0, sizeof(test_data));
			test_data.flag = 1;
			DEBUG("Saving... TEST MODE FLAG");
			//rv = drv_fds_write(DRV_FDS_PAGE_TEST, (uint8_t *)&test_data,  sizeof(test_data));
			//PRINT_IF_ERROR("drv_fds_write()", rv);
			memcpy(&m_test_info, &test_data, sizeof(m_mokibo_test_info_t));
			fds_RequestToSaveAfterThisTime(m_config.config.selected_ch,0);
			
			DEB("INVOKE: on argv.new_mode.test --> M_COMM_CMD_START\n");
			m_comm_cmd(M_COMM_CMD_START, 0);
			ChangeState(M_MOKIBO_TEST_STATUS);
		}		
	}
	
	if(evt & M_EVENT_NEW_BOND)
	{
		DEBUG("### evt: M_EVENT_NEW_BOND ###");
		if(argv.bond.all) { m_comm_cmd(M_COMM_CMD_ERASE_BONDS, 0); }
	}
}


//================================================================================
//	Function name : ManufactureHandler()
//================================================================================
static void ManufactureHandler(m_event_type_t evt, m_event_arg_t argv)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(m_event_type_t evt:%d, m_event_arg_t argv:%d)",evt, argv);
	if(evt)
	{
		DEBUG("OOPS!!![%d]", evt);
	}
}


//================================================================================
//	Function name : KeyboardHandler()
//================================================================================
static void KeyboardHandler(m_event_type_t evt, m_event_arg_t argv)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(m_event_type_t evt:%d, m_event_arg_t argv:%d)",evt, argv);

	m_comm_evt_arg_t val;	

	// TT_INT가 Low인 동안은, cypress chip 내에 축적되어 있는 point 정보들을 쓸어 없애야 한다.
	//while(!cypress_read_TT_INT()) // <-- TT_INT단자에 pull-up이 없는 경우에는 infinite loop가 될 수 있다.
	//	clearAll_CypressTouchData();
	// (1) 30ms마다 한번씩 호출되지만, 충분해보인다.
	// (2) clear_m_touch_db()안에서도 호출된다.

	// TT_INT 핀이 로우 상태일때(즉, 터치 데이터가 있는 경우)
	// TouchHandler에서 데이터를 읽도록 한다.
	if(evt & M_EVENT_TOUCH_MODE_SCAN_TIME 
		//&& M_COMM_ADV_STATUS_CONNECTRUN == m_comm_adv_get_status()
		&& M_COMM_ADV_STATUS_CONNECTED <= m_comm_adv_get_status()
		&& m_conn_handle != BLE_CONN_HANDLE_INVALID
		&& M_COMM_ADV_BOND_INVALID != m_comm_adv_get_addr(m_config.config.selected_ch)
		&& M_COMM_ADV_BOND_READY != m_comm_adv_get_addr(m_config.config.selected_ch)
	)
	{
		// INFO: 현재는 keyboard모드일 경우에도 터치를 읽음
		// 목적은 터치칩의 터치 데이터가 누적되어 이 데이터가 터치 모드로 
		// 진입할때 전송되어 오동작 방지
		// 추후 performance, watchdog, 터치 모드 진입시누적데이터 처림(??) 
		// 또는 interrupt 방식등을 고려하여 변경 가능함
		m_touch_scan_handler();
	}
	
	if(evt & M_EVENT_KEYBOAD_MODE_SCAN_TIME)
	{
		keyboard_scan_timeout_handler();
	}
	
	// (1) on DRV_KEYBOARD_HOTKEY_ONOFF
	// (2) on DRV_KEYBOARD_HOTKEY_BTRCHK
	if(evt & M_EVENT_MODE_CHANGED)
	{
		if(argv.new_mode.sleep)
		{ 	
			ChangeState(M_MOKIBO_SLEEP_STATUS);
		}

		if(argv.new_mode.battery_chk) 
		{ 	
			if(M_COMM_ADV_STATUS_IN_ADV != m_comm_adv_get_status())
			{
				ChangeState(M_MOKIBO_BATTERY_CHECK_STATUS);
			}
		}
	}
	//----------------------------------------------------------------------------
	if(evt & M_EVENT_TOUCH_MODE_CHANGED)
	{
		if(argv.touch_mode.enter) 
		{ 	
			m_touch_mode_set(M_TOUCH_CYPRESS_ACTIVE_AREA_RIGHT); 

#ifdef M_TOUCH_CLICKBAR_TRACE
		DEB("-->touch_mode.enter: call ChangeState(M_MOKIBO_TOUCH_STATUS)<--");
#endif
			ChangeState(M_MOKIBO_TOUCH_STATUS); 
		}

		if(argv.touch_mode.lock_enter) 
		{ 
#ifdef M_TOUCH_CLICKBAR_TRACE
			DEB("-->touch_mode.lock_enter: call ChangeState(M_MOKIBO_TOUCH_STATUS)<--");
#endif
			m_touch_mode_set(M_TOUCH_CYPRESS_ACTIVE_AREA_WHOLE); 
			ChangeState(M_MOKIBO_TOUCH_STATUS); 
		}
	}
	//----------------------------------------------------------------------------
	if(evt & M_EVENT_CHANNEL_CHANGED)
	{
		m_touch_mode_set(M_TOUCH_CYPRESS_ACTIVE_AREA_RIGHT);
		ChangeState(M_MOKIBO_KEYBOARD_STATUS);
		if(argv.channel.bt1) 			{ m_comm_cmd(M_COMM_CMD_CH_CHANGE, M_COMM_CHANNEL_BT1); }
		if(argv.channel.bt2) 			{ m_comm_cmd(M_COMM_CMD_CH_CHANGE, M_COMM_CHANNEL_BT2); }
		if(argv.channel.bt3) 			{ m_comm_cmd(M_COMM_CMD_CH_CHANGE, M_COMM_CHANNEL_BT3); }
//		if(argv.channel.dongle)			{ m_comm_cmd(M_COMM_CMD_CH_CHANGE, M_COMM_CHANNEL_DONGLE); }
	}
	//----------------------------------------------------------------------------
	if(evt & M_EVENT_VOLUME_CHANGED)
	{
		#ifdef M_TOUCH_VOLUME_TRACE
			DEB(" VolumeTrace...");
		#endif

		// update argument value
		memset(&val, 0, sizeof(val)); // clear
		if(argv.volume.down)	{ val.volume.down = drv_keyboard_get_volume(S2L(V_DOWN)) ? 1 : 0; }		
		if(argv.volume.up) 		{ val.volume.up = drv_keyboard_get_volume(S2L(V_UP)) ? 1 : 0; }		
		if(argv.volume.mute)	{val.volume.mute = drv_keyboard_get_volume(S2L(V_MUTE)) ? 1 : 0; }
		
		m_comm_send_event(M_COMM_EVENT_VOLUME_CTRL, val);
	}
	//----------------------------------------------------------------------------
	if(evt & M_EVENT_NEW_BOND)
	{
		m_touch_mode_set(M_TOUCH_CYPRESS_ACTIVE_AREA_RIGHT);
		ChangeState(M_MOKIBO_KEYBOARD_STATUS);

		if(argv.bond.bt1) { m_comm_cmd(M_COMM_CMD_NEW_BOND, M_COMM_CHANNEL_BT1); }
		if(argv.bond.bt2) { m_comm_cmd(M_COMM_CMD_NEW_BOND, M_COMM_CHANNEL_BT2); }
		if(argv.bond.bt3) { m_comm_cmd(M_COMM_CMD_NEW_BOND, M_COMM_CHANNEL_BT3); }
		if(argv.bond.all) { m_comm_cmd(M_COMM_CMD_ERASE_BONDS, 0); }
	}
	//----------------------------------------------------------------------------
	if(evt & M_EVENT_SPECIAL_KEY)
	{
		#ifdef M_TOUCH_SPECIAL_KEY_TRACE
			DEB(" SpecialKey(1)...");
		#endif

		// update argument value
		memset(&val, 0, sizeof(val)); // clear
		if(argv.sp_key.home) 	{ val.sp_key.home = 1; }
		if(argv.sp_key.vkbd) 	{ val.sp_key.vkbd = 1; }
		if(argv.sp_key.search) 	{ val.sp_key.search = 1; }
		if(argv.sp_key.capture) { val.sp_key.capture = 1; }
		if(argv.sp_key.swtapp) 	{ val.sp_key.swtapp	= 1; val.sp_key.search_opt 	= 1; }
		if(argv.sp_key.backward) { val.sp_key.backward = 1; } //20181109_flimbs: add backward				
		if(argv.sp_key.lockscreen) {val.sp_key.lockscreen = 1;}
		//if(argv.sp_key.touchlock){val.sp_key.touchlock =1;}
		if(argv.sp_key.language_z){
			________________________________LOG_mComm_SendingFnHotKey(2,"send to val.sp_key language_z");
			val.sp_key.language_z = 1;}
		if(argv.sp_key.language_x){
			________________________________LOG_mComm_SendingFnHotKey(2,"send to val.sp_key language_x");
			val.sp_key.language_x = 1;}
		if(argv.sp_key.language_c){
			________________________________LOG_mComm_SendingFnHotKey(2,"send to val.sp_key language_c");
			val.sp_key.language_c = 1;}
	
	
		// Send data
		m_comm_send_event(M_COMM_EVENT_SPECIAL_KEY, val);

		// Go back
		if(argv.sp_key.swtapp) 	
		{
			#ifdef M_TOUCH_SPECIAL_KEY_TRACE
				DEB(" SpecialKey(2)...");
			#endif
			val.sp_key.swtapp 		= 1;
			val.sp_key.search_opt 	= 0;
			m_comm_send_event(M_COMM_EVENT_SPECIAL_KEY, val);
		}
		else
		{
			#ifdef M_TOUCH_SPECIAL_KEY_TRACE
				DEB(" SpecialKey(3)...");
			#endif
			// release all keys
			memset(&val, 0, sizeof(val)); // clear
			val.sp_key.release = 1;  //20181109_flimbs:rename back -> release
			m_comm_send_event(M_COMM_EVENT_SPECIAL_KEY, val);
		}
	}
	//----------------------------------------------------------------------------
  //20181104_flimbs: added for centralmode
	if (evt & M_EVENT_CENTRAL_MODE)
	{		
		if(M_COMM_ADV_STATUS_IN_ADV != m_comm_adv_get_status())
		{
			if (argv.central.windows)
			{
				m_comm_set_central_type(m_comm_get_selected_channel(), M_COMM_CENTRAL_MICROSOFT);
			}
			if (argv.central.android)
			{			
				m_comm_set_central_type(m_comm_get_selected_channel(), M_COMM_CENTRAL_ANDROID);
			}
			if (argv.central.mac)
			{
				m_comm_set_central_type(m_comm_get_selected_channel(), M_COMM_CENTRAL_MAC);
			}
			if (argv.central.ios)
			{
				m_comm_set_central_type(m_comm_get_selected_channel(), M_COMM_CENTRAL_IOS);
			}
			SetTouchColor();
			m_led_set_status(M_LED_DIMMING_OUT);		
			
			#if ________________________________DBG_flimbs_20190310_mEvent_OsLedIndicateNotWork_setDelay100 == 1
				nrf_delay_ms(100);	
			#endif
			
			m_led_set_status(M_LED_OFF);
			m_timer_set(M_TIMER_ID_TEMP, POWER_ON_COLOR_INTERVAL);
		}
	}
}


/*================================================================================
   Function name : TouchHandler()
================================================================================*/
static void TouchHandler(m_event_type_t evt, m_event_arg_t argv)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(m_event_type_t evt:%d, m_event_arg_t argv:%d)",evt, argv);
	m_comm_evt_arg_t val;
	//m_event_clickbar_buttons_t btn;
	
	#if defined(M_MOKIBO_STATE_MACHINE_TRACE)
	DEBUG("Called[%d]", evt);
	#endif

	if(evt & M_EVENT_TOUCH_MODE_SCAN_TIME)
	{
		//if( M_COMM_ADV_STATUS_CONNECTRUN == m_comm_adv_get_status()
		if( M_COMM_ADV_STATUS_CONNECTED <= m_comm_adv_get_status()
		&& m_conn_handle != BLE_CONN_HANDLE_INVALID
		&& M_COMM_ADV_BOND_INVALID != m_comm_adv_get_addr(m_config.config.selected_ch)
		&& M_COMM_ADV_BOND_READY != m_comm_adv_get_addr(m_config.config.selected_ch) )
		{
		m_touch_scan_handler();
	}
	}

	if(evt & M_EVENT_KEYBOAD_MODE_SCAN_TIME)
	{
		keyboard_scan_timeout_handler();
	}

	// (1) on DRV_KEYBOARD_HOTKEY_ONOFF
	// (2) on DRV_KEYBOARD_HOTKEY_BTRCHK
	if(evt & M_EVENT_MODE_CHANGED)
	{
		if(argv.new_mode.sleep) 
		{ 	
			ChangeState(M_MOKIBO_SLEEP_STATUS);
		}

		if(argv.new_mode.battery_chk) 
		{ 	
			if(M_COMM_ADV_STATUS_IN_ADV != m_comm_adv_get_status())
			{
				ChangeState(M_MOKIBO_BATTERY_CHECK_STATUS);
			}
		}
	}

	if(evt & M_EVENT_CHANNEL_CHANGED)
	{
		m_touch_mode_set(M_TOUCH_CYPRESS_ACTIVE_AREA_RIGHT);
		ChangeState(M_MOKIBO_KEYBOARD_STATUS);
		if(argv.channel.bt1) 	{ m_comm_cmd(M_COMM_CMD_CH_CHANGE, M_COMM_CHANNEL_BT1); }
		if(argv.channel.bt2) 	{ m_comm_cmd(M_COMM_CMD_CH_CHANGE, M_COMM_CHANNEL_BT2); }
		if(argv.channel.bt3) 	{ m_comm_cmd(M_COMM_CMD_CH_CHANGE, M_COMM_CHANNEL_BT3); }
//		if(argv.channel.dongle)	{ m_comm_cmd(M_COMM_CMD_CH_CHANGE, M_COMM_CHANNEL_DONGLE); }
	}
	
	if(evt & M_EVENT_VOLUME_CHANGED)
	{
		#ifdef M_TOUCH_SPECIAL_KEY_TRACE
			DEB(" VolumeCOntrol...");
		#endif
		// update argument value
		memset(&val, 0, sizeof(val)); // clear
		if(argv.volume.down)	{ val.volume.down = drv_keyboard_get_volume(S2L(V_DOWN)) ? 1 : 0; }
		if(argv.volume.up) 		{ val.volume.up = drv_keyboard_get_volume(S2L(V_UP)) ? 1 : 0; }		
		if(argv.volume.mute)	{val.volume.mute = drv_keyboard_get_volume(S2L(V_MUTE)) ? 1 : 0; }
		m_comm_send_event(M_COMM_EVENT_VOLUME_CTRL, val);
	}

	if(evt & M_EVENT_NEW_BOND)
	{
		m_touch_mode_set(M_TOUCH_CYPRESS_ACTIVE_AREA_RIGHT);
		ChangeState(M_MOKIBO_KEYBOARD_STATUS);
		if(argv.bond.bt1) { m_comm_cmd(M_COMM_CMD_NEW_BOND, M_COMM_CHANNEL_BT1); }
		if(argv.bond.bt2) { m_comm_cmd(M_COMM_CMD_NEW_BOND, M_COMM_CHANNEL_BT2); }
		if(argv.bond.bt3) { m_comm_cmd(M_COMM_CMD_NEW_BOND, M_COMM_CHANNEL_BT3); }
		if(argv.bond.all) { m_comm_cmd(M_COMM_CMD_ERASE_BONDS, 0); }
	}

	if(evt & M_EVENT_SPECIAL_KEY)
	{
		#ifdef M_TOUCH_SPECIAL_KEY_TRACE
			DEB(" SpecialKey(20)...");
		#endif
		// update argument value
		memset(&val, 0, sizeof(val)); // clear
		if(argv.sp_key.home) 	{ val.sp_key.home = 1; }
		if(argv.sp_key.vkbd) 	{ val.sp_key.vkbd = 1; }
		if(argv.sp_key.search) 	{ val.sp_key.search = 1; }
		if(argv.sp_key.capture) { val.sp_key.capture = 1; }
		if(argv.sp_key.swtapp) 	{ val.sp_key.swtapp	= 1; val.sp_key.search_opt 	= 1; }
		if (argv.sp_key.backward) { val.sp_key.backward = 1; }//20181109_flimbs: add backward		
		if(argv.sp_key.lockscreen) {val.sp_key.lockscreen = 1;}
	
		// Send data
		m_comm_send_event(M_COMM_EVENT_SPECIAL_KEY, val);

		// Go back
		if(argv.sp_key.swtapp) 	
		{
			#ifdef M_TOUCH_SPECIAL_KEY_TRACE
				DEB(" SpecialKey(21)...");
			#endif
			val.sp_key.swtapp 		= 1; 
			val.sp_key.search_opt 	= 0; 
			m_comm_send_event(M_COMM_EVENT_SPECIAL_KEY, val);
		}
		else
		{
			#ifdef M_TOUCH_SPECIAL_KEY_TRACE
				DEB(" SpecialKey(22)...");
			#endif
			// release all keys
			memset(&val, 0, sizeof(val)); // clear
			val.sp_key.release = 1; // 20181109_flimbs: rename back -> release
			m_comm_send_event(M_COMM_EVENT_SPECIAL_KEY, val);
		}
	}

	if(evt & M_EVENT_TOUCH_MODE_CHANGED)
	{
		if(argv.touch_mode.exit) 
		{
			// If touch is not locking mode
			if(M_TOUCH_CYPRESS_ACTIVE_AREA_WHOLE != m_touch_mode_get())
			{
				ChangeState(M_MOKIBO_KEYBOARD_STATUS);
			}
		}
		
		if(argv.touch_mode.lock_enter) 
		{
			// If touch is not locking mode
			if(M_TOUCH_CYPRESS_ACTIVE_AREA_WHOLE != m_touch_mode_get())
			{
				m_touch_mode_set(M_TOUCH_CYPRESS_ACTIVE_AREA_WHOLE); 
				#ifdef FLIMBS_20190102_TOUCH_LOCK_LED_DEPENDS_ON_BTCH
					SetTouchColor();
				#else
					m_led_set_color(TOUCH_LOCK_MODE_COLOR);
					
				#endif
			}
		}

		if(argv.touch_mode.lock_exit) 
		{ 
			m_touch_mode_set(M_TOUCH_CYPRESS_ACTIVE_AREA_RIGHT);
			ChangeState(M_MOKIBO_KEYBOARD_STATUS);
		}
	}

	//----------------------------------------------------------------------------
	if(evt & M_EVENT_CLICKBAR_BUTTON_CHANGED)
	{
		DEB("TouchHandler()-M_EVENT_CLICKBAR_BUTTON_CHANGED-");
		
		memset(&val, 0, sizeof(val)); // clear
		if(argv.button.left) { val.click.left = 1; }
		if(argv.button.right) { val.click.right = 1; }
		//
		m_comm_send_event(M_COMM_EVENT_CLICK_CTRL, val);		
		________________________________LOG_mComm_Recv_mCommSendEvent(5,"if(evt & M_EVENT_CLICKBAR_BUTTON_CHANGED)");
	}
	//----------------------------------------------------------------------------
	
  //20181104_flimbs: added for centralmode
	if (evt & M_EVENT_CENTRAL_MODE)
	{
		if(M_COMM_ADV_STATUS_IN_ADV != m_comm_adv_get_status())
		{
			if (argv.central.windows)
			{
				m_comm_set_central_type(m_comm_get_selected_channel(), M_COMM_CENTRAL_MICROSOFT);
			}
			if (argv.central.android)
			{
				m_comm_set_central_type(m_comm_get_selected_channel(), M_COMM_CENTRAL_ANDROID);
			}
			if (argv.central.mac)
			{
				m_comm_set_central_type(m_comm_get_selected_channel(), M_COMM_CENTRAL_MAC);
			}
			if (argv.central.ios)
			{
				m_comm_set_central_type(m_comm_get_selected_channel(), M_COMM_CENTRAL_IOS);
			}
			//SetTouchColor();
			
			//m_led_set_status(M_LED_DIMMING_OUT);		
			m_led_set_status(M_LED_OFF);		
			#if ________________________________DBG_flimbs_20190310_mEvent_OsLedIndicateNotWork_setDelay100==1
				nrf_delay_ms(100);					
			#endif
			m_led_set_status(M_LED_ON);		
			//m_led_set_status(M_LED_OFF);		
			//m_timer_set(M_TIMER_ID_TEMP, POWER_ON_COLOR_INTERVAL);
		}
	}
}


/*================================================================================
   Function name : UpgradeHandler()
================================================================================*/
static void UpgradeHandler(m_event_type_t evt, m_event_arg_t argv)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(m_event_type_t evt:%d, m_event_arg_t argv:%d)",evt, argv);
	#if defined(M_MOKIBO_STATE_MACHINE_TRACE)
	DEBUG("Called[%d]", evt);
	#endif

	if(evt)
	{
		DEBUG("OOPS!!![%d]", evt);
	}
}


/*================================================================================
   Function name : SleepHandler()
================================================================================*/
static void SleepHandler(m_event_type_t evt, m_event_arg_t argv)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(m_event_type_t evt:%d, m_event_arg_t argv:%d)",evt, argv);
	#if defined(M_MOKIBO_STATE_MACHINE_TRACE)
	DEBUG("Called[%d]", evt);
	#endif

	// Run state machine
	m_sleep_handler[m_sub_states]();
}


/*================================================================================
   Function name : EndHandler()
================================================================================*/
static void EndHandler(m_event_type_t evt, m_event_arg_t argv)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(m_event_type_t evt:%d, m_event_arg_t argv:%d)",evt, argv);
	#if defined(M_MOKIBO_STATE_MACHINE_TRACE)
	DEBUG("Called[%d]", evt);
	#endif
	
	if(evt)
	{
		DEBUG("OOPS!!![%d]", evt);
	}
}

/*==============================================================================
		Function name : TestHandler() //DRV_KEYBOARD_HOTKEY_TEST_ENTER .... see: drv_keyboard.h (Fn + 7)		
==============================================================================*/
static void TestHandler(m_event_type_t evt, m_event_arg_t argv)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(m_event_type_t evt:%d, m_event_arg_t argv:%d)",evt, argv);
	//ret_code_t rv;
	m_comm_evt_arg_t val;
	//m_mokibo_test_info_t test_data;
	uint32_t num;
	
	#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
	uint8_t touch_alive, stm8_alive;
	#endif

	static uint32_t sequence = 0;
	static uint8_t toggle = 0;
	static uint8_t reset = 0;

	#if defined(M_MOKIBO_STATE_MACHINE_TRACE)
	DEBUG("Called[%d]", evt);
	#endif
	
	//-----------------------------------------------------------------------------------------------------------------------
		// M_EVENT_KEYBOAD_MODE_SCAN_TIME ==> LED UPDATE용도로 사용
	if(evt & M_EVENT_KEYBOAD_MODE_SCAN_TIME)
	{
#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
		ret_code_t rv = m_led_set_status( sequence%2 ? M_LED_SLOW_BLINK : M_LED_FAST_BLINK);

		stm8_alive = (NRF_SUCCESS == rv) ? 1 : 0;

		m_touch_scan_handler();
	
		// NRF_ERROR_NOT_FOUND: not touch data & I2C communication is OK
		touch_alive = (NRF_ERROR_INTERNAL == rv) ? 0 : 1;

		DEBUGS("MOKIBO LONG-TERM TEST: SEQ(%d) TOUCH:%s[%d] STM8L:%s", sequence
				, touch_alive ? "Alive" : "Error", rv
				, stm8_alive ? "Alive" : "Error");
#endif
	}
	//-----------------------------------------------------------------------------------------------------------------------
	// M_EVENT_KEYBOAD_MODE_SCAN_TIME ==> TEXT sending용으로 ���용
	if(evt & M_EVENT_TOUCH_MODE_SCAN_TIME)
	{
		//-----------------------------------------------------------------------------------------------------------------------
		keyboard_scan_timeout_handler();
		
		//-----------------------------------------------------------------------------------------------------------------------
		if( (m_comm_ble_get_status() != BLE_CONN_HANDLE_INVALID) ||	(m_comm_get_mode() == M_COMM_MODE_GAZELL) )
		{

			//-----------------------------------------------------------------------------------------------------------------------
			memset(&val, 0, sizeof(val)); // clear

			//-----------------------------------------------------------------------------------------------------------------------
			if(toggle)
			{
				val.keys.buf[DRV_KEYBOARD_PACKET_KEY_INDEX]	  = APP_USBD_HID_KBD_M;
				val.keys.buf[DRV_KEYBOARD_PACKET_KEY_INDEX+1] = APP_USBD_HID_KBD_O;
				val.keys.buf[DRV_KEYBOARD_PACKET_KEY_INDEX+2] = APP_USBD_HID_KBD_K;
				
				// Sequence: "0->1->2->....->99->0->1->..."
				num = sequence%100;
				num /= 10;
				//
				if(num) { val.keys.buf[DRV_KEYBOARD_PACKET_KEY_INDEX+3]	= APP_USBD_HID_KBD_1+(num)-1; }
				else 		{ val.keys.buf[DRV_KEYBOARD_PACKET_KEY_INDEX+3] = APP_USBD_HID_KBD_0; 				}
				//
				if(sequence%10) { val.keys.buf[DRV_KEYBOARD_PACKET_KEY_INDEX+4]	= APP_USBD_HID_KBD_1+(sequence%10)-1; }
				else 						{ val.keys.buf[DRV_KEYBOARD_PACKET_KEY_INDEX+4] = APP_USBD_HID_KBD_0; 								}

				if(99 == sequence%100) { val.keys.buf[DRV_KEYBOARD_PACKET_KEY_INDEX+5] = APP_USBD_HID_KBD_KEYPAD_ENTER; }
				
				sequence++;
			}
			else
			{
				if(0 != sequence%100)
				{
					val.keys.buf[DRV_KEYBOARD_PACKET_KEY_INDEX] 	= APP_USBD_HID_KBD_BS;
					val.keys.buf[DRV_KEYBOARD_PACKET_KEY_INDEX+1] = APP_USBD_HID_KBD_BS;
					val.keys.buf[DRV_KEYBOARD_PACKET_KEY_INDEX+2] = APP_USBD_HID_KBD_BS;
					val.keys.buf[DRV_KEYBOARD_PACKET_KEY_INDEX+3] = APP_USBD_HID_KBD_BS;
					val.keys.buf[DRV_KEYBOARD_PACKET_KEY_INDEX+4] = APP_USBD_HID_KBD_BS;
				}
			}
			//-----------------------------------------------------------------------------------------------------------------------
			#ifdef M_TOUCH_CLICKBAR_TRACE
				DEB(" TestHandler()....");
			#endif
			m_comm_send_event(M_COMM_EVENT_KEYBOARD_UPDATE, val);
			//-----------------------------------------------------------------------------------------------------------------------
			// clear --> release all keys
			m_comm_clear_keyboard();
		}
		
		//-----------------------------------------------------------------------------------------------------------------------
		toggle = toggle ? 0 : 1;

		//-----------------------------------------------------------------------------------------------------------------------
		if(reset)
		{
			if(reset++>10)
			{
				#if (MOKIBO_WDT_ENABLED)
				m_mokibo_WDT_reset(1000);
				#else
				m_mokibo_soft_reset(0xA3A3);
				#endif
			}
		}

		//-----------------------------------------------------------------------------------------------------------------------
	}

	//-----------------------------------------------------------------------------------------------------------------------
	// on DRV_KEYBOARD_HOTKEY_TEST_ENTER,
	if(evt & M_EVENT_MODE_CHANGED)
	{
		//
		if(argv.new_mode.test)
		{
			m_mokibo_test_info_t test_data;
			
			// Save test mode enter flag
			memset(&test_data, 0, sizeof(test_data));
			//rv = drv_fds_read(DRV_FDS_PAGE_TEST, (uint8_t *)&test_data,  sizeof(test_data));
			//PRINT_IF_ERROR("drv_fds_read()", rv);
			fds_read_stanley( FDS_FILE_ID_STANLEY,FDS_RECORD_KEY_M_TEST_INFO, sizeof(test_data), (uint8_t *)&test_data);

			test_data.flag = 0; 
			DEBUG("Saving... TEST MODE FLAG[%d]", test_data.flag);

			//rv = drv_fds_write(DRV_FDS_PAGE_TEST, (uint8_t *)&test_data,  sizeof(test_data));
			//PRINT_IF_ERROR("drv_fds_write()", rv);
			memcpy(&m_test_info, &test_data, sizeof(m_mokibo_test_info_t));
			fds_RequestToSaveAfterThisTime(m_config.config.selected_ch, 0);
			
			#if 0 // BLOCK_MJ_FDS by Stanley
			drv_fds_save_immediately();
			#endif

			reset = 1;
		}
		//
		
	}

	//-----------------------------------------------------------------------------------------------------------------------
	if(evt & M_EVENT_CHANNEL_CHANGED)
	{
		if(argv.channel.bt1) { m_comm_cmd(M_COMM_CMD_CH_CHANGE, M_COMM_CHANNEL_BT1); }
		if(argv.channel.bt2) { m_comm_cmd(M_COMM_CMD_CH_CHANGE, M_COMM_CHANNEL_BT2); }
		if(argv.channel.bt3) { m_comm_cmd(M_COMM_CMD_CH_CHANGE, M_COMM_CHANNEL_BT3); }
	}

	//-----------------------------------------------------------------------------------------------------------------------
	if(evt & M_EVENT_NEW_BOND)
	{
		if(argv.bond.bt1) { m_comm_cmd(M_COMM_CMD_NEW_BOND, M_COMM_CHANNEL_BT1); }
		if(argv.bond.bt2) { m_comm_cmd(M_COMM_CMD_NEW_BOND, M_COMM_CHANNEL_BT2); }
		if(argv.bond.bt3) { m_comm_cmd(M_COMM_CMD_NEW_BOND, M_COMM_CHANNEL_BT3); }
	}
}



/*==============================================================================
   Function name : BatteryCheckHandler()
==============================================================================*/
static void BatteryCheckHandler(m_event_type_t evt, m_event_arg_t argv)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(m_event_type_t evt, m_event_arg_t argv)");
	#if defined(M_MOKIBO_STATE_MACHINE_TRACE)
	DEBUG("Called[%d]", evt);
	#endif
	
	// Run state machine
	m_battery_handler[m_sub_states]();
}




/*==============================================================================
   Function name : keyboard_scan_timeout_handler()
==============================================================================*/
static void keyboard_scan_timeout_handler(void)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
	const uint8_t 		*key_packet;
	uint8_t        		key_packet_size, new_size;
	m_comm_evt_arg_t 	val;
	//-----------------------------------------------------------------------------
	static uint8_t sieved_calling = 1; // for m_do_cert_mode_repetition()

	//-----------------------------------------------------------------------------
	// see: m_event.c
	CallMeRepeatedlyToSaveInfoToFlash(30);
//	CallMeRepeatedlyToCallDongleFunc(30);
//	CallMeRepeatedlyToCallBTFunc(30);

	//
	//clearAll_CypressTouchData();
	//  <--- timing에 영향을 주지 안도록 main.c의 main loop로 옮겼음.

//	//-----------------------------------------------------------------------------
//	// Turn off the Touch Power if 2-minutes idle
//	if(	m_timer_is_expired(M_TIMER_ID_TURN_OFF_PARADE_TOUCH) )
//	{
//		// Turn off Touch-Power
//		turn_off_parade_touch(); // m_parade_touch_power_was_off = 1
//		
//		// new beginning of the 2-minutes timer
//		m_timer_set(M_TIMER_ID_TURN_OFF_PARADE_TOUCH,TURN_OFF_PARADETOUCH_ON_IDLE);
//	}

	//-----------------------------------------------------------------------------
	// goto SLEEP if idle (kbd & touch)
	if(	m_timer_is_expired(M_TIMER_ID_SLEEP_IF_IDLE) )
	{
		// Now, goto SLEEP !
		SYSTEM_MSG("############ going to SLEEP ############\n");
		ChangeState( M_MOKIBO_SLEEP_STATUS );
	}

	
	//-----------------------------------------------------------------------------
		// 인증을 위한 반복 동작이 활성화되어 있으면, 30ms마다 반복되는 호출에 대하여 10번 중 한 번 꼴로 실행한다.
	if(m_cert_mode_enabled){
			sieved_calling--;
			if(sieved_calling <= 0)
			{
				sieved_calling=1;
				m_do_cert_mode_repetition();
			}
	}
	//-----------------------------------------------------------------------------
	if(m_keyboard_new_packet(&key_packet, &key_packet_size))
	{
		//-----------------------------------------------------------------------------
		// kbd is busy ... so, refresh the idle timers: M_TIMER_ID_SLEEP_IF_IDLE & M_TIMER_ID_TURN_OFF_PARADE_TOUCH
//		m_timer_set(M_TIMER_ID_TURN_OFF_PARADE_TOUCH, TURN_OFF_PARADETOUCH_ON_IDLE);
		m_timer_set(M_TIMER_ID_SLEEP_IF_IDLE, SLEEP_ON_IDLE_INTERVAL);
		m_timer_set(M_TIMER_ID_KEYBOARD_INPUT, 300);
		
// <--- clickbar touch에 의하여 resume하므로 keyboard input시 resume은 사용하지 않는다.
		//if( is_parade_touch_turned_off() )
		//{
		//	resume_parade_touch();	
		//}
		
		//-----------------------------------------------------------------------------
		if(M_MOKIBO_KEYBOARD_STATUS == m_mokibo_status)
		{	

#ifdef M_RESET_PARADE_CHIP //=======================================================================
			// 자판의 dn,up 각각마다 하나씩으로 카운트 한다.
			if(++m_key_pressed_cnt == 3) // original by MJ Kim
			//if(++m_key_pressed_cnt == 4) // modified by Stanley: 2018.8.3
			{
				DEB("m_key_pressed_cnt:%d", m_key_pressed_cnt);
				DEB("drv_cypress_touch_reset_fast()+drv_cypress_re_init()+m_touch_set_init()\n");
								// 일단 한번 이상 ClickBar 터치가 이루어진 이후,
				// 키보드 자판이 한번 눌렸다 떨어지고, 두번 째 자판이 눌리는 순간....(떨어지는 순간으로 일단 바꾸었다)
				// <--- 그러나, 잘못되면 Error! touch data read failed! 라는 메시지가 계속 뜰 수 있다 ??
				//       see: drv_cypress_read()
				drv_cypress_touch_reset_fast(__func__, __LINE__);
				drv_cypress_re_init();
				m_touch_set_init();
			}
#endif // M_RESET_PARADE_CHIP //=======================================================================

		}

		//if((m_comm_ble_get_status() != BLE_CONN_HANDLE_INVALID) || (m_comm_get_mode() == M_COMM_MODE_GAZELL))
		{
			if(M_MOKIBO_KEYBOARD_STATUS == m_mokibo_status || M_MOKIBO_TOUCH_STATUS == m_mokibo_status)
			{
				#ifdef M_TOUCH_CLICKBAR_TRACE
					DEB(" in M_MOKIBO_KEYBOARD_STATUS...\n");
				#endif
				memset(&val, 0, sizeof(val)); // clear
				memcpy(&val, (uint8_t *)&key_packet[0], key_packet_size);
				m_comm_send_event(M_COMM_EVENT_KEYBOARD_UPDATE, val);
			}
			/*
			else if(M_MOKIBO_TOUCH_STATUS == m_mokibo_status)
			{
				#ifdef M_TOUCH_CLICKBAR_TRACE
					DEB(" in M_MOKIBO_TOUCH_STATUS...\n");
				#endif
				memset(&val, 0, sizeof(val)); // clear
				new_size = FilterKeys(&val.keys.buf[0], (uint8_t *)&key_packet[0], key_packet_size);
				if(new_size)
				{
					//DEBUG("new_size:%d", new_size);
					m_comm_send_event(M_COMM_EVENT_KEYBOARD_UPDATE, val);
				}
				else
				{
					m_comm_send_event(M_COMM_EVENT_KEYBOARD_UPDATE, val);
				}
			}
			*/
			m_touchlock_automode = false;
		}
	}
	else
	{
		if(M_COMM_MODE_GAZELL == m_comm_get_mode())
		{
			#if 0 // 2018.10.16 - Blocked by Stanley ( ... unknown usage ... )
			m_dongle_handle_long_key();
			#endif
		}
	}
}




//================================================================================
//	Function name : PowerOnDisplay()
//================================================================================
static void PowerOnDisplay(void)
{	
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
	if(m_timer_is_expired(M_TIMER_ID_TEMP))
	{
		m_timer_set(M_TIMER_ID_TEMP, POWER_ON_COLOR_INTERVAL);

		switch(m_sub_sate_argv)
		{
			case 0 : 				
				m_led_set_status(M_LED_ON);
				break;

			case 1 : 
				m_led_set_status(M_LED_OFF);
				break;
			
			case 2 : 
				m_led_set_status(M_LED_ON);
				break;
			
			case 3 : 
				m_led_set_status(M_LED_OFF);
				break;

			case 6:
				m_led_set_status(M_LED_ON);
				break;

			case 8:
				m_led_set_status(M_LED_DIMMING_OUT);
				break;
		}
		m_sub_sate_argv++;
	}
}

//================================================================================
//	Function name : PowerOffnDisplay()
//================================================================================
static void PowerOffnDisplay(void)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
	if(m_timer_is_expired(M_TIMER_ID_TEMP))
	{
		m_timer_set(M_TIMER_ID_TEMP, POWER_ON_COLOR_INTERVAL);

		switch(m_sub_sate_argv)
		{
			case 0 : 
				//m_led_set_color(M_LED_COLOR_WHITE);
				m_led_set_status(M_LED_ON);
				break;

			case 10:
				m_led_set_status(M_LED_OFF);
				break;
			
			case 13:
				m_led_set_status(M_LED_ON);
				break;			
			case 14:
				m_led_set_status(M_LED_OFF);
				break;

			case 15:
				m_led_set_status(M_LED_ON);
				//m_led_set_status(M_LED_DIMMING_OUT);				
				break;
			case 16:
				m_led_set_status(M_LED_OFF);
				//m_led_set_status(M_LED_DIMMING_OUT);				
				break;
		}
		m_sub_sate_argv++;
	}

	if(m_sub_sate_argv >= 17)
	{
		// Goto Next step
		m_sub_states = FINISH_STATE;
	}
}

//================================================================================
//	Function name : SetKeyboardColor()
//================================================================================
static void SetKeyboardColor(void)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
	// Do nothing
}


//================================================================================
//	Function name : SetTouchColor()
//================================================================================
static void SetTouchColor(void)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
	m_comm_channel_type_t ch = m_comm_get_selected_channel();
	
	switch(ch)
	{
		case M_COMM_CHANNEL_BT1 :
			m_led_set_color(BT1_MODE_COLOR);
			break;

		case M_COMM_CHANNEL_BT2 :
			m_led_set_color(BT2_MODE_COLOR);
			break;

		case M_COMM_CHANNEL_BT3 :
			m_led_set_color(BT3_MODE_COLOR);
			break;

		case M_COMM_CHANNEL_MAX :
			m_led_set_color(NO_CHANNEL_MODE_COLOR);
			break;

		default :
			m_led_set_color(NO_CHANNEL_MODE_COLOR);
			DEBUG("OOPS[%d]!!", ch);
		break;
	}	
}


//================================================================================
//	Function name : FilterKeys()
//	Requirment: refer to "https://app.asana.com/0/650649611668999/735638795663606"
//================================================================================
static uint8_t FilterKeys(uint8_t *dst, const uint8_t *src, uint8_t size)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(uint8_t dst, const uint8_t src, uint8_t size)");
	uint8_t i, new_keys = 0;
	
	if(size)
	{		
		for(i=0; i<size; i++)
		{			
			if(DRV_KEYBOARD_PACKET_MODIFIER_INDEX == i)
			{
				// Check only All modifier bit, refer to app_usbd_hid_kbd_modifier_t																		
				dst[i] = src[i];
				new_keys++;						
			}
			else
			{
				if(	((src[i] >= APP_USBD_HID_KBD_F1) && (src[i] <= APP_USBD_HID_KBD_F12)) 				
				||	(APP_USBD_HID_KBD_ESC    == src[i])
				||	(APP_USBD_HID_KBD_TILDE    == src[i])
				||	(APP_USBD_HID_KBD_TAB    == src[i])
				||	(APP_USBD_HID_KBD_CAPS    == src[i])    //leftend key				
				||	(APP_USBD_HID_KBD_DELETE == src[i]) 
				||	(APP_USBD_HID_KBD_BS == src[i]) 
				||	(APP_USBD_HID_KBD_BSLH == src[i]) 
				||	(APP_USBD_HID_KBD_ENTER == src[i])  //rightend key              
				||	(APP_USBD_HID_KBD_UP == src[i]) 
				||	(APP_USBD_HID_KBD_DOWN == src[i]) 
				||	(APP_USBD_HID_KBD_LFT == src[i]) 
				||	(APP_USBD_HID_KBD_RGHT == src[i]) //right arrow
				||	(APP_USBD_HID_KBD_SB == src[i]) //spacebar
				)    
				{
					//FLIMBS_DBG("		find filterKey");
					dst[i] = src[i];
					new_keys++;					
				}
			}			
		}
	}

	return new_keys;
}




//-----------------------------------------------------------------------------------------------------------------------------------
//			power_on_reset_parade_touch
//-----------------------------------------------------------------------------------------------------------------------------------
static void power_on_reset_parade_touch(void)
{	
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
	m_parade_touch_power_was_off = 0;	
	m_timer_set(M_TIMER_ID_SLEEP_IF_IDLE, SLEEP_ON_IDLE_INTERVAL);		// 20분 timer

	drv_cypress_power_on();
	drv_cypress_ResetPin_Low_Wait_High();
	
	nrf_delay_ms(100);// parade touch가 정상상태에 이르기를 기다린다....	

	m_touch_init();	// good but need long time - 12 secs

	DEB("### Power On Reset: parade touch ###\n");
}







/*================================================================================
   Function name : PrepareSleep()
================================================================================*/
static void PrepareSleep(void)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
	//ret_code_t	err_code;
	//uint8_t 	val = 0;
	

	// TODO, prepare everythings before sleep
	// Turn-off touch chipset...etc
	// 1) Set the keyboard ready for read by STM8L
	drv_keyboard_prepare_sleep();
	m_timer_set(M_TIMER_ID_TEMP, POWER_ON_COLOR_INTERVAL);
	
#if MOKIBO_FDS_ENABLED==1 //===========================================================================
#if 0 // BLOCK_MJ_FDS by Stanley
	// 2) Save Configuration
    err_code = drv_fds_save_immediately();
	PRINT_IF_ERROR("drv_fds_save_immediately()", err_code);
#endif
#endif// MOKIBO_FDS_ENABLED//==========================================================================================
	
	// Goto Next step
	m_sub_states = PROCESS_STATE;
}



/*================================================================================
   Function name : ProcessSleep()
================================================================================*/
static void ProcessSleep(void)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
	PowerOffnDisplay();
}

/*================================================================================
   Function name : FinishSleep()
================================================================================*/
// PrepareSleep() --> ProcessSleep() --> FInishSleep()
static void FinishSleep(void)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
	uint8_t 	val = 0;
	ret_code_t	err_code;
	
	//DEB("FinsishSleep()...");

	// 3) Make STM8L Sleep handler
	err_code = drv_stm8_write(STM8_WRITE_FW_CMD, STM8_FW_STATUS_SLEEP);
	PRINT_IF_ERROR("drv_stm8_write()", err_code);

	//DEB("drv_stm8_write(STM8_WRITE_FW_CMD, STM8_FW_STATUS_SLEEP)");
	
	// 4) Check it
	err_code = drv_stm8_read(STM8_READ_FW_STATUS, &val);
	PRINT_IF_ERROR("drv_stm8_read()", err_code);
	
	//DEB("STM8_READ_FW_STATUS[%d]!!!",val);

	if(STM8_FW_STATUS_SLEEP != val)
	{
		//DEB("Request SLEEP to STM8 but different Respose=%d\n",val);
	}
	//
	sd_power_gpregret_set(1, 0x1234);

	//===========================================================================================
	// Go to system-off mode (this function will not return; wakeup will cause a reset).
	//===========================================================================================
#if 0
	// MJ's:
	sd_power_system_off();
#else
	// Stanley's:	
	//	typedef enum
	//	{
	//		NRF_PWR_MGMT_EVT_PREPARE_WAKEUP = NRF_PWR_MGMT_SHUTDOWN_GOTO_SYSOFF,
	//		//!< Application will prepare the wakeup mechanism.
	//
	//		NRF_PWR_MGMT_EVT_PREPARE_SYSOFF = NRF_PWR_MGMT_SHUTDOWN_STAY_IN_SYSOFF,
	//		//!< Application will prepare to stay in System OFF state.
	//
	//		NRF_PWR_MGMT_EVT_PREPARE_DFU    = NRF_PWR_MGMT_SHUTDOWN_GOTO_DFU,
	//		//!< Application will prepare to enter DFU mode.
	//
	//		NRF_PWR_MGMT_EVT_PREPARE_RESET  = NRF_PWR_MGMT_SHUTDOWN_RESET,
	//		//!< Application will prepare to chip reset.
	//	} nrf_pwr_mgmt_evt_t;
	//
	//	void nrf_pwr_mgmt_shutdown(nrf_pwr_mgmt_shutdown_t shutdown_type);
	//
	nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_GOTO_SYSOFF);
	//nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_STAY_IN_SYSOFF);
#endif

}



/*============================================================================//
//                            PrepareBattery()
//============================================================================*/
/*----------------------------------------------------------------------------//
00000000_flimbs
	BatteryCheckHandler() --> m_battery_handler[]: 
		PrepareBattery --> ProcessBattery --> FinishBattery

	Vbat------R174K---+---R124K---+---GND
										|
										|
										ADC
	Vadc= Vbat * 124/(174+124)= Vbat * 124/298 = 0.42 * Vbat

	(1) Vadc = 0.42 * Vbat(4.1) = 1.72 [volt]
	(2) Vadc = 0.42 * Vbat(3.9) = 1.64 [volt]
	(3) Vadc = 0.42 * Vbat(3.3) = 1.39 [volt]
	2.6v ~ 4.3v

	99 ~ 40	~3.9		AM06:20 ~ AM10:20 4시간
	40 ~ 24	~3.6		AM10:20 ~ PM08:20 10시간
	24 ~ 14	~3.4		PM08:20 ~ AM06:20 10시간
	14 ~ 0				AM06:20 ~ AM09:42 3시간20분
//----------------------------------------------------------------------------*/
static void PrepareBattery(void)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
	ret_code_t rv;
	uint8_t	battery_level;
	
	rv = m_util_get_battery_level(&battery_level);
	PRINT_IF_ERROR("m_util_get_battery_level", rv);
	

	if(NRF_SUCCESS == rv)
	{
		// Backup current led status
		m_sub_sate_argv = m_led_get_status();

		// 8시간초록 + 14시간노랑 + 5시간빨강
		// AM06~PM02 + PM02~AM04 
		// 52+ ~32   + 32 ~ 16   +  16 ~10-
		// refer to specification for details
		if(battery_level<=16U)//3.8v
		{
			m_led_set_color(M_LED_COLOR_RED);
			m_led_set_status(M_LED_SLOW_BLINK);
		}				
		else if(battery_level<=32U)//3.9v
		{			
			m_led_set_color(M_LED_COLOR_RSVD2);//YELLOW
			m_led_set_status(M_LED_SLOW_BLINK);
		}
		else  // 100U
		{
			m_led_set_color(M_LED_COLOR_GREEN);
			m_led_set_status(M_LED_SLOW_BLINK);
		}

		m_timer_set(M_TIMER_ID_TEMP, BATTERY_CHECK_INTERVAL);

		// Goto Next step
		m_sub_states = PROCESS_STATE;
	}
	else
	{
		#if defined(M_MOKIBO_BATTERY_TRACE)
		SYSTEM_MSG("BATTERY: -Try again! - \n");
		#endif

		if(++m_sub_sate_argv >= 2)
		{
			#if defined(M_MOKIBO_BATTERY_TRACE)
			SYSTEM_MSG("BATTERY: OOPS!!!:%d, go to with 0%!!!", m_sub_sate_argv);
			#endif

			m_sub_sate_argv = m_led_get_status();
			m_led_set_color(M_LED_COLOR_RED);
			m_led_set_status(M_LED_SLOW_BLINK);
		
			m_timer_set(M_TIMER_ID_TEMP, BATTERY_CHECK_INTERVAL);

			// Goto Next step
			m_sub_states = PROCESS_STATE;
		}
	}
}




/*============================================================================//
//                            funcname()
//============================================================================*/
/*----------------------------------------------------------------------------//
//	BatteryCheckHandler() --> m_battery_handler[]: PrepareBattery --> ProcessBattery --> FinishBattery
//----------------------------------------------------------------------------*/

static void ProcessBattery(void)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
	if(m_timer_is_expired(M_TIMER_ID_TEMP))
	{		
		// Goto Next step
		m_sub_states = FINISH_STATE;
		m_led_set_status(M_LED_DIMMING_OUT);
		m_timer_set(M_TIMER_ID_TEMP, BATTERY_CHECK_INTERVAL_2);
	}
}


/*============================================================================//
//                            FinishBattery()
//============================================================================*/
/*----------------------------------------------------------------------------//
00000000_flimbs
	BatteryCheckHandler() --> m_battery_handler[]: PrepareBattery --> ProcessBattery --> FinishBattery
//----------------------------------------------------------------------------*/
static void FinishBattery(void)
{
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
	if(m_timer_is_expired(M_TIMER_ID_TEMP))
	{	
		m_led_set_status(m_sub_sate_argv);// Store previous led status		
		ChangeState(m_prv_mokibo_status);//ChangeState(M_MOKIBO_KEYBOARD_STATUS);
	}
}

























/*$$$$$\  $$$$$$$$\ $$$$$$$\  $$\   $$\  $$$$$$\                                       
$$  __$$\ $$  _____|$$  __$$\ $$ |  $$ |$$  __$$\                                      
$$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |$$ /  \__|                                     
$$ |  $$ |$$$$$\    $$$$$$$\ |$$ |  $$ |$$ |$$$$\                                      
$$ |  $$ |$$  __|   $$  __$$\ $$ |  $$ |$$ |\_$$ |                                     
$$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |$$ |  $$ |                                     
$$$$$$$  |$$$$$$$$\ $$$$$$$  |\$$$$$$  |\$$$$$$  |                                     
\_______/ \________|\_______/  \______/  \______/          
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\  $$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |$$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |$$ /  \__|
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |\$$$$$$\  
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ | \____$$\ 
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |$$\   $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |\$$$$$$  |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__| \______/ 
//============================================================================//
//                                 DEBUG FUNCTION                             //
//============================================================================*/


//================================================================================
//	Function name : DBG_Update_WDT()   
//================================================================================
void DBG_Update_WDT(uint32_t ms)//Polling Thread
{				
	#if (MOKIBO_WDT_ENABLED)
	________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(ms:%d)",ms);
	if(!m_mokibo_WTD_run_reset)
	{		
		nrf_drv_wdt_channel_feed(m_mokibo_WTD_ch);
	}
	#endif		
	nrf_delay_ms(ms);	
	
}


//================================================================================
//	Function name : log_resetreason()
//================================================================================
static void log_resetreason(void)
{	
		________________________________LOG_mMokibo_FuncHeads(FUNCHEAD_FLAG,"(void)");
    uint32_t m_reset_reason = nrf_power_resetreas_get();
	
		if (0 == m_reset_reason)
    {
		________________________________LOG_mMokibo_showInitInfo(1,"resetReason: %d:none",m_reset_reason);
    }
    if (0 != (m_reset_reason & NRF_POWER_RESETREAS_RESETPIN_MASK))
    {		
		________________________________LOG_mMokibo_showInitInfo(1,"resetReason: %d:resetpin",m_reset_reason);
    }
    if (0 != (m_reset_reason & NRF_POWER_RESETREAS_DOG_MASK     ))
    {		
		________________________________LOG_mMokibo_showInitInfo(1,"resetReason: %d:dog",m_reset_reason);
    }
    if (0 != (m_reset_reason & NRF_POWER_RESETREAS_SREQ_MASK    ))
    {		
		________________________________LOG_mMokibo_showInitInfo(1,"resetReason: %d:sreq",m_reset_reason);
    }
    if (0 != (m_reset_reason & NRF_POWER_RESETREAS_LOCKUP_MASK  ))
    {
		________________________________LOG_mMokibo_showInitInfo(1,"resetReason: %d:lookup",m_reset_reason);		
    }
    if (0 != (m_reset_reason & NRF_POWER_RESETREAS_OFF_MASK     ))
    {		
		________________________________LOG_mMokibo_showInitInfo(1,"resetReason: %d:off",m_reset_reason);
    }
    if (0 != (m_reset_reason & NRF_POWER_RESETREAS_LPCOMP_MASK  ))
    {		
		________________________________LOG_mMokibo_showInitInfo(1,"resetReason: %d:lpcomp",m_reset_reason);
    }
    if (0 != (m_reset_reason & NRF_POWER_RESETREAS_DIF_MASK     ))
    {		
		________________________________LOG_mMokibo_showInitInfo(1,"resetReason: %d:dif",m_reset_reason);
    }
    if (0 != (m_reset_reason & NRF_POWER_RESETREAS_NFC_MASK     ))
    {		
		________________________________LOG_mMokibo_showInitInfo(1,"resetReason: %d:nfc",m_reset_reason);
    }
	#if 0
	if (0 != (m_reset_reason & NRF_POWER_RESETREAS_VBUS_MASK    ))
    {
        ________________________________LOG_mMokibo_showInitInfo(1,"resetReason: %d:vbus",m_reset_reason);
    }
	#endif
	
}



/*\   $$\ $$\   $$\ $$\   $$\  $$$$$$\  $$$$$$$$\ $$$$$$$\                             
$$ |  $$ |$$$\  $$ |$$ |  $$ |$$  __$$\ $$  _____|$$  __$$\                            
$$ |  $$ |$$$$\ $$ |$$ |  $$ |$$ /  \__|$$ |      $$ |  $$ |                           
$$ |  $$ |$$ $$\$$ |$$ |  $$ |\$$$$$$\  $$$$$\    $$ |  $$ |                           
$$ |  $$ |$$ \$$$$ |$$ |  $$ | \____$$\ $$  __|   $$ |  $$ |                           
$$ |  $$ |$$ |\$$$ |$$ |  $$ |$$\   $$ |$$ |      $$ |  $$ |                           
\$$$$$$  |$$ | \$$ |\$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$  |                           
 \______/ \__|  \__| \______/  \______/ \________|\_______/                            
//============================================================================//
//                                 UNUSED FUNCTION                            //
//============================================================================*/



#if 0 /////////////////////////////////////////////////////////////////////////////////
        //-----------------------------------------------------------------------------------------------------------------------------------
        //            is_parade_touch_turned_off
        //-----------------------------------------------------------------------------------------------------------------------------------
        uint8_t is_parade_touch_turned_off(void)
        {
            return m_parade_touch_power_was_off;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        //            turn_off_parade_touch
        //-----------------------------------------------------------------------------------------------------------------------------------
        void turn_off_parade_touch(void)
        {
            // set as it is 'OFF' state
            m_parade_touch_power_was_off = 1;
            //

            //m_timer_set(M_TIMER_ID_SLEEP_IF_IDLE, SLEEP_ON_IDLE_INTERVAL);			// 20분 timer
            m_timer_set(M_TIMER_ID_TURN_OFF_PARADE_TOUCH, TURN_OFF_PARADETOUCH_ON_IDLE);	// 2분 timer

        #if 0
            // turn off the power: @ P.25
            //**어느 경우에도, 전원은 끄지 않도록 한다.**
            cypress_pwr(0); // power off @ P.25
        #else
            //**어느 경우에도, 전원은 끄지 않도록 한다.**
            //drv_cypress_power_off();
            drv_cypress_ResetPin_Low();
        #endif
            
            //
            DEB("\n");
            DEB("### Turn Off: parade touch ### \n");
            DEB("\n");
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        //            resume_parade_touch
        //-----------------------------------------------------------------------------------------------------------------------------------
        void resume_parade_touch(void)
        {
            // set as it is 'ON' state
            m_parade_touch_power_was_off = 0;
            //
						m_timer_set(M_TIMER_ID_SLEEP_IF_IDLE, SLEEP_ON_IDLE_INTERVAL);			// 20분 timer
						m_timer_set(M_TIMER_ID_TURN_OFF_PARADE_TOUCH, TURN_OFF_PARADETOUCH_ON_IDLE);	// 2분 timer

        #if 0    
            // turn on the power : @ P.25
            cypress_pwr(1); // power on @ P.25
        #else
            drv_cypress_power_on();
            drv_cypress_ResetPin_Low_Wait_High();
        #endif
            
            // parade touch가 정상상태에 이르기를 기다린다....
			// <--- Battery전원이 부족한 경우, parade touch 초기화가 잘 이루어 지지 않는 것 같다...
            nrf_delay_ms(100);
            
            // and initialize it 
            //m_touch_init();    // good but need long time - 12 secs
            m_touch_resume();	// resume이므로 drv_twi_init()을 하지 않는다.
            
            //
            DEB("\n");
            DEB("\n");
            DEB("### Turn On: parade touch ###\n");
            DEB("\n");
            DEB("\n");
            
        }
#endif ////////////////////////////////////////


// End of file 
