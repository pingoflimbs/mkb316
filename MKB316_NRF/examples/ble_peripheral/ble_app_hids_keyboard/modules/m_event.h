/// \file m_event.h
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/


/*\   $$\ $$$$$$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$$$$$$\ $$\     $$\          
$$ |  $$ |\_$$  _|$$  __$$\\__$$  __|$$  __$$\ $$  __$$\\$$\   $$  |         
$$ |  $$ |  $$ |  $$ /  \__|  $$ |   $$ /  $$ |$$ |  $$ |\$$\ $$  /          
$$$$$$$$ |  $$ |  \$$$$$$\    $$ |   $$ |  $$ |$$$$$$$  | \$$$$  /           
$$  __$$ |  $$ |   \____$$\   $$ |   $$ |  $$ |$$  __$$<   \$$  /            
$$ |  $$ |  $$ |  $$\   $$ |  $$ |   $$ |  $$ |$$ |  $$ |   $$ |             
$$ |  $$ |$$$$$$\ \$$$$$$  |  $$ |    $$$$$$  |$$ |  $$ |   $$ |             
\__|  \__|\______| \______/   \__|    \______/ \__|  \__|   \__|             
//============================================================================//
//                            REVISION HISTORY                                //
//============================================================================//
//----------------------------------------------------------------------------//
20171026_MJ
	Created initial version

20171211_MJ
	Added M_EVENT_CLICKBAR_BUTTON_CHANGED

20171222_MJ
	Added M_EVENT_GOTO_SLEEP

20171230_MJ
	Created M_EVENT_BT1_NEW_BOND

20171121_MJ
	Added DRV_KEYBOARD_HOTKEY_V_UP/DRV_KEYBOARD_HOTKEY_V_DOWN handle code

20180130_MJ
	Created M_EVENT_ERASE_BONDS

20180212_MJ
	Added M_EVENT_TEST_MODE_ENTER
	Issue#: http://lab.innopresso.com/issues/118#change-556 

20180309_MJ
	Removed DRV_KEYBOARD_HOTKEY_MODECHANGE

20180309_MJ
	Added m_event_arg_t in m_event_get()
	Merged m_event_is_new() into m_event_update()

20180314_MJ
	Created M_EVENT_CHANNEL_CHANGED, M_EVENT_NEW_BOND, M_EVENT_VOLUME_CHANGED

20180315_MJ
	Created M_EVENT_TOUCH_MODE_CHANGED
	Created battery_check_event_detect

20180319_MJ
	Added center clickbar logic 

20180320_MJ
	Added F and D key mapping to click button

20180323_MJ
	Correctd left click conditon in button and Key map

20180521_MJ
	Created reset_mokibo

20180620_MJ
	Added m_touch_lock_detect to prevent toggle when long press

20180719_MJ
	Issue: https://app.asana.com/0/650649611668999/735739225241413
	Created input_logical_touch in input_type_t type
	Added new condition in scan_clickbar_touch() 

20180720_MJ
	Issue: https://app.asana.com/0/650649611668999/735638795663606
	Added event detect in touch_event_detect handler

20180808_stanley
	cert_mode: 마우스 포인터를 사각형으로 우회전하게 하고, keyboard 문자를 입력하는 과정을 반복함
	The followings are added for MOKIBO CERTIFICATION TEST;
	DRV_KEYBOARD_HOTKEY_CERT_MODE..... in drv_keyboard.h
	m_KeyswitchDown()................. in m_touch.c
	m_KeyswitchUp()................... in m_touch.c
	m_MouseMove()..................... in m_touch.c
	m_cert_mode_enabled............... in m_event.c
	m_do_cert_mode_repetition()....... in m_event.c
	The Caller ....................... in keyboard_scan_timeout_handler() in mokibo.c
	S2L(CERT_MODE).................... in drv_keyboard.c
	"HOTKEY_CERT_MODE"................ in drv_keyboard.c
	You can activate the CERT_MODE by typing [Fn+0].
	To deactivate the mode, press the same key once more.
//----------------------------------------------------------------------------*/




/*$$$$$\  $$\    $$\ $$$$$$$$\ $$$$$$$\  $$\    $$\ $$$$$$\ $$$$$$$$\ $$\      $$\ 
$$  __$$\ $$ |   $$ |$$  _____|$$  __$$\ $$ |   $$ |\_$$  _|$$  _____|$$ | $\  $$ |
$$ /  $$ |$$ |   $$ |$$ |      $$ |  $$ |$$ |   $$ |  $$ |  $$ |      $$ |$$$\ $$ |
$$ |  $$ |\$$\  $$  |$$$$$\    $$$$$$$  |\$$\  $$  |  $$ |  $$$$$\    $$ $$ $$\$$ |
$$ |  $$ | \$$\$$  / $$  __|   $$  __$$<  \$$\$$  /   $$ |  $$  __|   $$$$  _$$$$ |
$$ |  $$ |  \$$$  /  $$ |      $$ |  $$ |  \$$$  /    $$ |  $$ |      $$$  / \$$$ |
 $$$$$$  |   \$  /   $$$$$$$$\ $$ |  $$ |   \$  /   $$$$$$\ $$$$$$$$\ $$  /   \$$ |
 \______/     \_/    \________|\__|  \__|    \_/    \______|\________|\__/     \__|
//============================================================================//
//                        FUNCTION CALLSTACK OVERVIEW                         //
//============================================================================//
//----------------------------------------------------------------------------//

//----------------------------------------------------//TIMERS
//----------------------------------------------------//VARS
//----------------------------------------------------//FUNCS
//----------------------------------------------------//INIT
m_event_init();		
	{	
		m_event = M_EVENT_NONE;
		m_touch_lock_detect = 0;		
		memset(&m_arg, 0, sizeof(m_arg));
		memset(m_input, 0, sizeof(m_input));
	}

//----------------------------------------------------//LOOP
for(;;)
{
	m_mokibo_process()
	{
		m_event_arg_t	argv;
		m_event_type_t	event;
		var isUpdate = m_event_update();
		{
			m_event = M_EVNET_NONE;
		}
	}
}
//----------------------------------------------------//LOGS

//----------------------------------------------------------------------------*/




/*\      $$\  $$$$$$\  $$$$$$$\  $$\   $$\ $$\       $$$$$$$$\  $$$$$$\  
$$$\    $$$ |$$  __$$\ $$  __$$\ $$ |  $$ |$$ |      $$  _____|$$  __$$\ 
$$$$\  $$$$ |$$ /  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$ /  \__|
$$\$$\$$ $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$$$$\    \$$$$$$\  
$$ \$$$  $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$  __|    \____$$\ 
$$ |\$  /$$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$\   $$ |
$$ | \_/ $$ | $$$$$$  |$$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$$\ \$$$$$$  |
\__|     \__| \______/ \_______/  \______/ \________|\________| \______/ 
//============================================================================//
//                                 MODULES USED                               //
//============================================================================*/
#ifndef __M_EVENT_H
#define __M_EVENT_H



/*$$$$\  $$\       $$$$$$\  $$$$$$$\   $$$$$$\  $$\                           
$$  __$$\ $$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                          
$$ /  \__|$$ |     $$ /  $$ |$$ |  $$ |$$ /  $$ |$$ |                          
$$ |$$$$\ $$ |     $$ |  $$ |$$$$$$$\ |$$$$$$$$ |$$ |                          
$$ |\_$$ |$$ |     $$ |  $$ |$$  __$$\ $$  __$$ |$$ |                          
$$ |  $$ |$$ |     $$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |                          
\$$$$$$  |$$$$$$$$\ $$$$$$  |$$$$$$$  |$$ |  $$ |$$$$$$$$\                     
 \______/ \________|\______/ \_______/ \__|  \__|\________|                    
 $$$$$$\   $$$$$$\  $$\   $$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$\   $$\ $$$$$$$$\ 
$$  __$$\ $$  __$$\ $$$\  $$ |$$  __$$\\__$$  __|$$  __$$\ $$$\  $$ |\__$$  __|
$$ /  \__|$$ /  $$ |$$$$\ $$ |$$ /  \__|  $$ |   $$ /  $$ |$$$$\ $$ |   $$ |   
$$ |      $$ |  $$ |$$ $$\$$ |\$$$$$$\    $$ |   $$$$$$$$ |$$ $$\$$ |   $$ |   
$$ |      $$ |  $$ |$$ \$$$$ | \____$$\   $$ |   $$  __$$ |$$ \$$$$ |   $$ |   
$$ |  $$\ $$ |  $$ |$$ |\$$$ |$$\   $$ |  $$ |   $$ |  $$ |$$ |\$$$ |   $$ |   
\$$$$$$  | $$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$ |  $$ |$$ | \$$ |   $$ |   
 \______/  \______/ \__|  \__| \______/   \__|   \__|  \__|\__|  \__|   \__|   
 //============================================================================//
//                                 GLOBAL CONSTANTS                            //
//============================================================================*/
#define NBIT(N)							(1<<N)




/*$$$$\  $$\       $$$$$$\  $$$$$$$\   $$$$$$\  $$\                                     
$$  __$$\ $$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                             
$$ /  \__|$$ |     $$ /  $$ |$$ |  $$ |$$ /  $$ |$$ |                             
$$ |$$$$\ $$ |     $$ |  $$ |$$$$$$$\ |$$$$$$$$ |$$ |                             
$$ |\_$$ |$$ |     $$ |  $$ |$$  __$$\ $$  __$$ |$$ |                             
$$ |  $$ |$$ |     $$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |                             
\$$$$$$  |$$$$$$$$\ $$$$$$  |$$$$$$$  |$$ |  $$ |$$$$$$$$\                        
 \______/ \________|\______/ \_______/ \__|  \__|\________|                       
$$$$$$$$\ $$\     $$\ $$$$$$$\  $$$$$$$$\ $$$$$$$\  $$$$$$$$\ $$$$$$$$\  $$$$$$\  
\__$$  __|\$$\   $$  |$$  __$$\ $$  _____|$$  __$$\ $$  _____|$$  _____|$$  __$$\ 
   $$ |    \$$\ $$  / $$ |  $$ |$$ |      $$ |  $$ |$$ |      $$ |      $$ /  \__|
   $$ |     \$$$$  /  $$$$$$$  |$$$$$\    $$ |  $$ |$$$$$\    $$$$$\    \$$$$$$\  
   $$ |      \$$  /   $$  ____/ $$  __|   $$ |  $$ |$$  __|   $$  __|    \____$$\ 
   $$ |       $$ |    $$ |      $$ |      $$ |  $$ |$$ |      $$ |      $$\   $$ |
   $$ |       $$ |    $$ |      $$$$$$$$\ $$$$$$$  |$$$$$$$$\ $$ |      \$$$$$$  |
   \__|       \__|    \__|      \________|\_______/ \________|\__|       \______/ 
//============================================================================//
//                                 GLOBAL TYPEDEFS                            //
//============================================================================*/

//----------------------------------------------------//m_event_type_t
// If you add new m_event_type, please add descriptions in m_event_get() function
// Currently this event is bitfied of 32bit variable.
//20181104_flimbs: CENTRAL_MODE 추가
typedef enum
{
    M_EVENT_NONE										= 0x00000000,
    M_EVENT_TOUCH_MODE_SCAN_TIME	= NBIT(0),
    M_EVENT_KEYBOAD_MODE_SCAN_TIME	= NBIT(1),
    M_EVENT_STARTUP					= NBIT(2),
    M_EVENT_MODE_CHANGED			= NBIT(3),
	M_EVENT_TOUCH_MODE_CHANGED		= NBIT(4),
    M_EVENT_CLICKBAR_BUTTON_CHANGED	= NBIT(5),
    M_EVENT_CHANNEL_CHANGED			= NBIT(6),
    M_EVENT_VOLUME_CHANGED			= NBIT(7),
    M_EVENT_NEW_BOND				= NBIT(8),
    M_EVENT_SPECIAL_KEY				= NBIT(9),
	M_EVENT_CENTRAL_MODE			= NBIT(10),

	// M_EVENT_MAX shulde be Last number of NBIT() + 2
    //M_EVENT_MAX		= 11,    
	M_EVENT_MAX			= 12,
} m_event_type_t;





//----------------------------------------------------//m_event_arg_t
typedef struct 
{
	uint8_t left		:1;
	uint8_t center		:1;
	uint8_t right 		:1;
	uint8_t rsvd 		:5;
} m_event_clickbar_buttons_t;

typedef struct 
{
	uint32_t home		:1; // home
	uint32_t swtapp	:1;	// switching App
	uint32_t vkbd 		:1;	// virtual keyboard
	uint32_t search 	:1;	// search
	uint32_t capture :1;	// capture
	uint32_t backward : 1; // backward 20181106_flimbs	
	uint32_t lockscreen	:1;
	uint32_t touchlock	:1;		
	uint32_t language_z :1;
	uint32_t language_c :1;
	uint32_t language_x :1;
	uint32_t rsvd		:21;	// reserved
} m_event_special_key_t;

typedef struct 
{
	uint8_t bt1				:1;
	uint8_t bt2				:1;
	uint8_t bt3				:1;
	uint8_t dongle			:1;
	uint8_t dongle_pseudo	:1; // Stanley added: dongle로 전환할 때 FDS가 정상동작하지 않게 되므로, dongle모드로 넘어가기 전에 먼저 flash에 저장한 후, 저장이 끊나면, 그제야 dongle로 전환하기 위하여 추가함.
	uint8_t rsvd 			:3;
	
} m_event_channel_t;

typedef struct 
{
	uint8_t bt1			:1;
	uint8_t bt2			:1;
	uint8_t bt3			:1;
	uint8_t all 		:1;
	uint8_t rsvd 		:4;
} m_event_new_bond_t;

typedef struct 
{
	uint8_t down		:1;
	uint8_t up			:1;	
	uint8_t mute		:1;
	uint8_t rsvd 		:5;	
} m_event_volume_t;

typedef struct 
{
	uint8_t enter			:1;
	uint8_t exit			:1;
	uint8_t lock_enter		:1;
	uint8_t lock_exit		:1;
	uint8_t rsvd 			:4;
} m_event_touch_mode_t;

typedef struct 
{
	uint8_t manufacure	:1;
	uint8_t keyboard		:1;
	uint8_t upgrade 		:1;
	uint8_t sleep				:1;
	uint8_t test 				:1; 
	uint8_t battery_chk	:1;
	uint8_t end					:1;
	uint8_t rsvd 				:1;	
} m_event_new_mode_t;

typedef struct
{
	uint8_t windows : 1;
	uint8_t android : 1;
	uint8_t mac : 1;
	uint8_t ios : 1;
	uint8_t rsvd : 4;
} m_event_central_mode_t;//20181106_flimbs: central mode 추가


typedef struct
{
	m_event_clickbar_buttons_t 	button;
	m_event_special_key_t		sp_key;
	m_event_channel_t			channel;
	m_event_new_bond_t			bond;
	m_event_volume_t			volume;
	m_event_touch_mode_t		touch_mode;
	m_event_new_mode_t			new_mode;	
	m_event_central_mode_t		central;//20181106_flimbs: central mode 추가
} m_event_arg_t;











/*\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ 
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\_*/





/*$$$$\  $$\       $$$$$$\  $$$$$$$\   $$$$$$\  $$\                                     
$$  __$$\ $$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                                    
$$ /  \__|$$ |     $$ /  $$ |$$ |  $$ |$$ /  $$ |$$ |                                    
$$ |$$$$\ $$ |     $$ |  $$ |$$$$$$$\ |$$$$$$$$ |$$ |                                    
$$ |\_$$ |$$ |     $$ |  $$ |$$  __$$\ $$  __$$ |$$ |                                    
$$ |  $$ |$$ |     $$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |                                    
\$$$$$$  |$$$$$$$$\ $$$$$$  |$$$$$$$  |$$ |  $$ |$$$$$$$$\                               
 \______/ \________|\______/ \_______/ \__|  \__|\________|                              
$$\    $$\  $$$$$$\  $$$$$$$\  $$$$$$\  $$$$$$\  $$$$$$$\  $$\       $$$$$$$$\  $$$$$$\  
$$ |   $$ |$$  __$$\ $$  __$$\ \_$$  _|$$  __$$\ $$  __$$\ $$ |      $$  _____|$$  __$$\ 
$$ |   $$ |$$ /  $$ |$$ |  $$ |  $$ |  $$ /  $$ |$$ |  $$ |$$ |      $$ |      $$ /  \__|
\$$\  $$  |$$$$$$$$ |$$$$$$$  |  $$ |  $$$$$$$$ |$$$$$$$\ |$$ |      $$$$$\    \$$$$$$\  
 \$$\$$  / $$  __$$ |$$  __$$<   $$ |  $$  __$$ |$$  __$$\ $$ |      $$  __|    \____$$\ 
  \$$$  /  $$ |  $$ |$$ |  $$ |  $$ |  $$ |  $$ |$$ |  $$ |$$ |      $$ |      $$\   $$ |
   \$  /   $$ |  $$ |$$ |  $$ |$$$$$$\ $$ |  $$ |$$$$$$$  |$$$$$$$$\ $$$$$$$$\ \$$$$$$  |
    \_/    \__|  \__|\__|  \__|\______|\__|  \__|\_______/ \________|\________| \______/ 
//============================================================================//
//                                 GLOBAL VARIABLES                           //
//============================================================================*/
extern uint8_t m_cert_mode_enabled;
extern m_event_arg_t	m_arg;
extern m_event_type_t	m_event;
extern char m_version_message[256];



/*$$$$\  $$\       $$$$$$\  $$$$$$$\   $$$$$$\  $$\                                     
$$  __$$\ $$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                           
$$ /  \__|$$ |     $$ /  $$ |$$ |  $$ |$$ /  $$ |$$ |                           
$$ |$$$$\ $$ |     $$ |  $$ |$$$$$$$\ |$$$$$$$$ |$$ |                           
$$ |\_$$ |$$ |     $$ |  $$ |$$  __$$\ $$  __$$ |$$ |                           
$$ |  $$ |$$ |     $$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |                           
\$$$$$$  |$$$$$$$$\ $$$$$$  |$$$$$$$  |$$ |  $$ |$$$$$$$$\                      
 \______/ \________|\______/ \_______/ \__|  \__|\________|                     
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\  $$\   $$\ $$$$$$$$\  $$$$$$\  $$$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\ $$ |  $$ |$$  _____|$$  __$$\ $$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|$$ |  $$ |$$ |      $$ /  $$ |$$ |  $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |      $$$$$$$$ |$$$$$\    $$$$$$$$ |$$ |  $$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |      $$  __$$ |$$  __|   $$  __$$ |$$ |  $$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\ $$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |$$ |  $$ |$$$$$$$$\ $$ |  $$ |$$$$$$$  |
\__|       \______/ \__|  \__| \______/ \__|  \__|\________|\__|  \__|\_______/ 
//============================================================================//
//                                 GLOBAL FUNCTION HEADS                      //
//============================================================================*/
ret_code_t m_event_init(void);
void m_event_get(m_event_type_t *p_evt, m_event_arg_t *p_argv);
bool m_event_update(void);
void m_event_get_button(m_event_clickbar_buttons_t *pBtn);




/*$$$$$\  $$$$$$$$\ $$$$$$$\  $$\   $$\  $$$$$$\                                       
$$  __$$\ $$  _____|$$  __$$\ $$ |  $$ |$$  __$$\                                      
$$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |$$ /  \__|                                     
$$ |  $$ |$$$$$\    $$$$$$$\ |$$ |  $$ |$$ |$$$$\                                      
$$ |  $$ |$$  __|   $$  __$$\ $$ |  $$ |$$ |\_$$ |                                     
$$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |$$ |  $$ |                                     
$$$$$$$  |$$$$$$$$\ $$$$$$$  |\$$$$$$  |\$$$$$$  |                                     
\_______/ \________|\_______/  \______/  \______/          
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\  $$\   $$\ $$$$$$$$\  $$$$$$\  $$$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\ $$ |  $$ |$$  _____|$$  __$$\ $$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|$$ |  $$ |$$ |      $$ /  $$ |$$ |  $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |      $$$$$$$$ |$$$$$\    $$$$$$$$ |$$ |  $$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |      $$  __$$ |$$  __|   $$  __$$ |$$ |  $$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\ $$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |$$ |  $$ |$$$$$$$$\ $$ |  $$ |$$$$$$$  |
\__|       \______/ \__|  \__| \______/ \__|  \__|\________|\__|  \__|\_______/ 
//============================================================================//
//                               DEBUG FUNCTIONS                              //
//============================================================================*/

#ifdef SIMULATE_KBD_AND_MOUSE_FOR_CERTIFICATION_TEST
	void m_do_cert_mode_repetition(void);
#endif

#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
	char *GetStr_fds_page_usage(uint8_t page);
	char *GetStr_comm_channel(uint8_t ch);
	char *GetStr_comm_central(uint8_t cent);
	char *GetStr_comm_adv_channel(uint8_t ch);
#endif





#endif // __M_COMM_H 

