#include <stdio.h>
#include <stdlib.h>

#define DEBUG(ARGV) 

typedef unsigned char uint8_t;
typedef signed char int8_t;

typedef unsigned short int uint16_t;
typedef signed short int int16_t;

typedef unsigned  int uint32_t;
typedef signed  int int32_t;

#define M_UTIL_ATAN2DEG_MAX 	(1456)
#define M_UTIL_ATAN2DEG_MIN 	(-1456)

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

uint16_t m_util_atan2deg(int16_t x, int16_t y) 
{
	uint8_t negflag, tempdegree, comp;
	uint32_t degree, uy,  ux;

	if (y == 0 && x == 0) 
	{
		return 0;
	}

	// Added by Mj.kim
	if(y > M_UTIL_ATAN2DEG_MAX)
	{
		DEBUG("");
		y = M_UTIL_ATAN2DEG_MAX; 
	}
	
	if(y < M_UTIL_ATAN2DEG_MIN)
	{
		DEBUG("");
		y = M_UTIL_ATAN2DEG_MIN; 
	}
	
	if(x > M_UTIL_ATAN2DEG_MAX)
	{
		DEBUG("");
		x = M_UTIL_ATAN2DEG_MAX; 
	}
	
	if(x < M_UTIL_ATAN2DEG_MIN)
	{
		DEBUG("");
		x = M_UTIL_ATAN2DEG_MIN; 
	}
	// end of Added by Mj.kim

	// Save the sign flags then remove signs and get XY as unsigned ints
	negflag = 0;
	if (y < 0) {
		negflag += 0x01;    // y flag bit
		y = (0 - y);        // is now +
	}
	uy = y;                // copy to unsigned var before multiply
	if (x < 0) {
		negflag += 0x02;    // x flag bit
		x = (0 - x);        // is now +
	}
	ux = x;                // copy to unsigned var before multiply

	// 1. Calc the scaled "degrees"
	if (uy > ux) {
		degree = (ux * 45) / uy;   // degree result will be 0-45 range
		negflag += 0x10;    // octant flag bit
	} else {
		degree = (uy * 45) / ux;   // degree result will be 0-45 range
	}

	// 2. Compensate for the 4 degree error curve
	comp = 0;
	tempdegree = degree;    // use an unsigned char for speed!
	if (tempdegree > 22) {   // if top half of range
		if (tempdegree <= 44) {
			comp++;
		}
		if (tempdegree <= 41) {
			comp++;
		}
		if (tempdegree <= 37) {
			comp++;
		}
		if (tempdegree <= 32) {
			comp++;  // max is 4 degrees compensated
		}
	} else { // else is lower half of range
		if (tempdegree >= 2) {
			comp++;
		}
		if (tempdegree >= 6) {
			comp++;
		}
		if (tempdegree >= 10) {
			comp++;
		}
		if (tempdegree >= 15) {
			comp++;  // max is 4 degrees compensated
		}
	}
	degree += comp;   // degree is now accurate to +/- 1 degree!

	// Invert degree if it was X>Y octant, makes 0-45 into 90-45
	if (negflag & 0x10) {
		degree = (90 - degree);
	}

	// 3. Degree is now 0-90 range for this quadrant,
	// need to invert it for whichever quadrant it was in
	if (negflag & 0x02) { // if -Y
		if (negflag & 0x01) { // if -Y -X
			degree = (180 + degree);
		} else       { // else is -Y +X
			degree = (180 - degree);
		}
	} else { // else is +Y
		if (negflag & 0x01) { // if +Y -X
			degree = (360 - degree);
		}
	}

	return degree;
}

int main(int argc, char *argv[]) {
	
	unsigned int i;
	int j;
	uint16_t a, b, c, d;
	
	printf("sizeof(unsigned int):%d\r\n", sizeof(unsigned int));
	printf("sizeof(int):%d\r\n", sizeof(int));
	printf("sizeof(uint16_t):%d\r\n", sizeof(uint16_t));
	printf("sizeof(int16_t):%d\r\n", sizeof(int16_t));
	printf("sizeof(uint8_t):%d\r\n", sizeof(uint8_t));
	printf("sizeof(int8_t):%d\r\n", sizeof(int8_t));
#if 0
	a = m_util_atan2deg(1456, 1456);
	b = m_util_atan2deg(-1456, 1456);
	c = m_util_atan2deg(-1456, -1456);
	d = m_util_atan2deg(1456, -1456);
#else
	a = m_util_atan2deg(1456, 0);
	b = m_util_atan2deg(0, 1456);
	c = m_util_atan2deg(-1456, 0);
	d = m_util_atan2deg(0, -1456);
#endif
	printf("a:%d\r\n", a);
	printf("b:%d\r\n", b);
	printf("c:%d\r\n", c);
	printf("d:%d\r\n", d);	


	system("pause");

	return 0;
}

