/// \file m_timer.h
/// This file contains all required configurations for mokibo
/*==============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
==============================================================================*/


/*\   $$\ $$$$$$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$$$$$$\ $$\     $$\          
$$ |  $$ |\_$$  _|$$  __$$\\__$$  __|$$  __$$\ $$  __$$\\$$\   $$  |         
$$ |  $$ |  $$ |  $$ /  \__|  $$ |   $$ /  $$ |$$ |  $$ |\$$\ $$  /          
$$$$$$$$ |  $$ |  \$$$$$$\    $$ |   $$ |  $$ |$$$$$$$  | \$$$$  /           
$$  __$$ |  $$ |   \____$$\   $$ |   $$ |  $$ |$$  __$$<   \$$  /            
$$ |  $$ |  $$ |  $$\   $$ |  $$ |   $$ |  $$ |$$ |  $$ |   $$ |             
$$ |  $$ |$$$$$$\ \$$$$$$  |  $$ |    $$$$$$  |$$ |  $$ |   $$ |             
\__|  \__|\______| \______/   \__|    \______/ \__|  \__|   \__|             
//============================================================================//
//                            REVISION HISTORY                                //
//============================================================================//
//----------------------------------------------------------------------------//

20171026_MJ
    Created initial version

20171211_MJ
    Added M_TIMER_TIME_SECOND()

20171218_MJ
    Added M_TIMER_ID_BLUETOOTH_NEW

20171226_MJ
    Added M_TIMER_ID_INVALID/M_TIMER_ID_ONOFF_LONG_PRESS

20180212_MJ
    Added M_TIMER_ID_TEST_ENTER_LONG_PRESS
    Issue#: http://lab.innopresso.com/issues/118#change-556 

20180214_MJ
    Added M_TIMER_ID_MULTI_TOUCH_SLIDE_ADJUST
    Added M_TIMER_ID_MULTI_TOUCH_ZOOM_ADJUST

20180219_MJ
Added M_TIMER_ID_MULTI_TOUCH_FIRST_ON

20180222_MJ
    Added M_TIMER_ID_MULTI_TOUCH_TAB
    Removed M_TIMER_ID_MULTI_TOUCH_FIRST_ON
    Removed M_TIMER_ID_MULTI_TOUCH_ZOOM_ADJUST
    Renamed M_TIMER_ID_MULTI_TOUCH_SLIDE_ADJUST to M_TIMER_ID_MULTI_TOUCH_ADJUST

20180305_MJ
    Created m_timer_start()/m_timer_stop()
    Changed m_timer_update() to local function
    incryeased APPLICATION_TIMER_INTERVAL to 2ms 

20180313_MJ
    Created m_timer_add_handler(), m_timer_update_handler()

20180322_Seo
    Add nrf_drv_timer

20180409_Seo
    Added M_TIMER_ID_DONGLE

20180412_MJ
    Added APPLICATION_NRF_TIMER_INTERVAL 
    Moved m_mokibo_get_random_value() to m_timer_get_counter()
    Created m_timer_get_delta()

20180413_MJ
    Added M_TIMER_ID_TWI_TIMEOUT     
    Added no delta condition in m_timer_get_delta()

//----------------------------------------------------------------------------*/




/*$$$$$\  $$\    $$\ $$$$$$$$\ $$$$$$$\  $$\    $$\ $$$$$$\ $$$$$$$$\ $$\      $$\ 
$$  __$$\ $$ |   $$ |$$  _____|$$  __$$\ $$ |   $$ |\_$$  _|$$  _____|$$ | $\  $$ |
$$ /  $$ |$$ |   $$ |$$ |      $$ |  $$ |$$ |   $$ |  $$ |  $$ |      $$ |$$$\ $$ |
$$ |  $$ |\$$\  $$  |$$$$$\    $$$$$$$  |\$$\  $$  |  $$ |  $$$$$\    $$ $$ $$\$$ |
$$ |  $$ | \$$\$$  / $$  __|   $$  __$$<  \$$\$$  /   $$ |  $$  __|   $$$$  _$$$$ |
$$ |  $$ |  \$$$  /  $$ |      $$ |  $$ |  \$$$  /    $$ |  $$ |      $$$  / \$$$ |
 $$$$$$  |   \$  /   $$$$$$$$\ $$ |  $$ |   \$  /   $$$$$$\ $$$$$$$$\ $$  /   \$$ |
 \______/     \_/    \________|\__|  \__|    \_/    \______|\________|\__/     \__|
//============================================================================//
//                        FUNCTION CALLSTACK OVERVIEW                         //
//============================================================================//
//----------------------------------------------------------------------------//

//----------------------------------------------------//TIMERS
//----------------------------------------------------//VARS
//----------------------------------------------------//FUNCS
//----------------------------------------------------//INIT
//----------------------------------------------------//LOOP
//----------------------------------------------------//LOGS

//----------------------------------------------------------------------------*/







/*\      $$\  $$$$$$\  $$$$$$$\  $$\   $$\ $$\       $$$$$$$$\  $$$$$$\  
$$$\    $$$ |$$  __$$\ $$  __$$\ $$ |  $$ |$$ |      $$  _____|$$  __$$\ 
$$$$\  $$$$ |$$ /  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$ /  \__|
$$\$$\$$ $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$$$$\    \$$$$$$\  
$$ \$$$  $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$  __|    \____$$\ 
$$ |\$  /$$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$\   $$ |
$$ | \_/ $$ | $$$$$$  |$$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$$\ \$$$$$$  |
\__|     \__| \______/ \_______/  \______/ \________|\________| \______/ 
//============================================================================//
//                                 MODULES USED                               //
//============================================================================*/
#ifndef __M_TIMER_H
#define __M_TIMER_H    
#include "app_timer.h"









 /*$$$$\   $$$$$$\  $$\   $$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$\   $$\ $$$$$$$$\ 
$$  __$$\ $$  __$$\ $$$\  $$ |$$  __$$\\__$$  __|$$  __$$\ $$$\  $$ |\__$$  __|
$$ /  \__|$$ /  $$ |$$$$\ $$ |$$ /  \__|  $$ |   $$ /  $$ |$$$$\ $$ |   $$ |   
$$ |      $$ |  $$ |$$ $$\$$ |\$$$$$$\    $$ |   $$$$$$$$ |$$ $$\$$ |   $$ |   
$$ |      $$ |  $$ |$$ \$$$$ | \____$$\   $$ |   $$  __$$ |$$ \$$$$ |   $$ |   
$$ |  $$\ $$ |  $$ |$$ |\$$$ |$$\   $$ |  $$ |   $$ |  $$ |$$ |\$$$ |   $$ |   
\$$$$$$  | $$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$ |  $$ |$$ | \$$ |   $$ |   
 \______/  \______/ \__|  \__| \______/   \__|   \__|  \__|\__|  \__|   \__|   
 //============================================================================//
//                                 GLOBAL CONSTANTS                            //
//============================================================================*/
#define M_TIMER_DELTA_TRACE

#define APPLICATION_TIMER_INTERVAL  		APP_TIMER_TICKS(1) 	// Resolution is ms, used in BT mode
#define APPLICATION_NRF_TIMER_INTERVAL  	1					// Resolution is ms, used in dongle mode

#define M_TIMER_TIME_MS(VAL)		(VAL)				// currently resolution is 1ms
#define M_TIMER_TIME_10MS(VAL)		(VAL*10)			// currently resolution is 1ms
#define M_TIMER_TIME_100MS(VAL)		(VAL*100)			// currently resolution is 1ms
#define M_TIMER_TIME_SECOND(VAL)	(VAL*1000)			// currently resolution is 1ms
#define M_TMR(NAME)					(M_TIMER_ID_##NAME)	// for short name

#if defined(M_TIMER_DELTA_TRACE)
#define M_TIMER_TRACE_START()  uint32_t m_timer_delta = m_timer_get_counter() 
#define M_TIMER_TRACE_END(LIMIT) \
{ \
	m_timer_delta = m_timer_get_delta(m_timer_delta, m_timer_get_counter()); \
	if(m_timer_delta>LIMIT) { DEBUG("Time delta:%dms", m_timer_delta); } \
}
#else
#define M_TIMER_TRACE_START()
#define M_TIMER_TRACE_END(LIMIT)
#endif







/*$$$$$$\ $$\     $$\ $$$$$$$\  $$$$$$$$\ $$$$$$$\  $$$$$$$$\ $$$$$$$$\  $$$$$$\  
\__$$  __|\$$\   $$  |$$  __$$\ $$  _____|$$  __$$\ $$  _____|$$  _____|$$  __$$\ 
   $$ |    \$$\ $$  / $$ |  $$ |$$ |      $$ |  $$ |$$ |      $$ |      $$ /  \__|
   $$ |     \$$$$  /  $$$$$$$  |$$$$$\    $$ |  $$ |$$$$$\    $$$$$\    \$$$$$$\  
   $$ |      \$$  /   $$  ____/ $$  __|   $$ |  $$ |$$  __|   $$  __|    \____$$\ 
   $$ |       $$ |    $$ |      $$ |      $$ |  $$ |$$ |      $$ |      $$\   $$ |
   $$ |       $$ |    $$ |      $$$$$$$$\ $$$$$$$  |$$$$$$$$\ $$ |      \$$$$$$  |
   \__|       \__|    \__|      \________|\_______/ \________|\__|       \______/ 
//============================================================================//
//                                 GLOBAL TYPEDEFS                            //
//============================================================================*/
typedef void (*m_timer_handler_t)(void);

typedef enum 
{
    M_TIMER_ID_INVALID= 0,
    M_TIMER_ID_EVENT_MODULE,		//
	
    M_TIMER_ID_KEYBOARD_SCAN,		//
    M_TIMER_ID_TOUCH_SCAN,			//
    M_TIMER_ID_SENSOR_SCAN,	// clickbar-button & clickbar-touch
    M_TIMER_ID_HOTKEY_SCAN,			//
    M_TIMER_ID_MOUSE_TAB_SCAN,
    M_TIMER_ID_TEMP,
    M_TIMER_ID_TOUCH_LOCK,          //8
    M_TIMER_ID_BT1_LONG_PRESS,
    M_TIMER_ID_BT2_LONG_PRESS,
    M_TIMER_ID_BT3_LONG_PRESS,
    M_TIMER_ID_DONGLE_LONG_PRESS, //12
    M_TIMER_ID_ONOFF_LONG_PRESS,
    M_TIMER_ID_TEST_ENTER_LONG_PRESS,
    M_TIMER_ID_MULTI_TOUCH_ADJUST,
    M_TIMER_ID_MULTI_TOUCH_TAB, //
    M_TIMER_ID_MULTI_TOUCH_TAB_ALIVE,   //
    M_TIMER_ID_SPECIAL_KEY_TIMEOUT,
    M_TIMER_ID_TOUCH_RESET_TIMEOUT,
	M_TIMER_ID_DONGLE_TIMEOUT,
	M_TIMER_ID_DONGLE_LONG_KEY,
	M_TIMER_ID_COMM_TIMEOUT,
	M_TIMER_ID_TWI_TIMEOUT, // 23
    M_TIMER_ID_DEBUG,	// For test only
    M_TiMER_ID_INERTIA_BLOCK,    
    M_TIMER_ID_KEYBOARD_INPUT,

	// Stanley added:
	//		parade touch chip으로부터의 point들이 마치 아무것도 터치되지 않은 듯한 데이터가 있어 
	// 		잠깐 사이에 일어나는 no touch는 무시하기 위하여 추가 하였음.
	M_TIMER_ID_ALL_TOUCHES_RELEASED,
	
	// Stanley added:
	//		keyboard나 parade touch에 아무조작이 없은 지 오래되면, Sleep-Mode로 전환하기 위하여 추가 하였음.
	//		sleep mode에서 깨어난 직후에도, 이 값은 갱신되어야 한다.
	M_TIMER_ID_SLEEP_IF_IDLE,	// SLEEP_ON_IDLE_INTERVAL

	// Stanley added:
	// (1) 터치나 키보드 입력 없이 2분이 경과하면, touch의 전원을 off한다.
	// (2) 키보드 입력이 들어오면, touch 전원을 On 한다.(Touch Init을 포함한다)
//	M_TIMER_ID_TURN_OFF_PARADE_TOUCH,	// TURN_OFF_PARADETOUCH_ON_IDLE

	// Stanley added:
	// (1) 터치나 키보드 입력 없이 2분이 경과하면, touch의 전원을 off한다.
	// (2) 키보드 입력이 들어오면, touch 전원을 On 한다.(Touch Init을 포함한다)
//	M_TIMER_ID_TOUCH_RESUME,	// TOUCH_RESUME_DELAY

	//
    M_TIMER_ID_INERTIA_SCROLL,
    M_TIMER_ID_BATTERY_LOW,
    M_TIMER_ID_CHARGER_CHECK,    
    M_TIMER_ID_UPDATE_MOVED,
    M_TIMER_ID_MAX,	// THE NUMBER OF TIMER_ID_'S 
} M_TIMER_ID_TYPE;






/*\    $$\  $$$$$$\  $$$$$$$\  $$$$$$\  $$$$$$\  $$$$$$$\  $$\       $$$$$$$$\ 
$$ |   $$ |$$  __$$\ $$  __$$\ \_$$  _|$$  __$$\ $$  __$$\ $$ |      $$  _____|
$$ |   $$ |$$ /  $$ |$$ |  $$ |  $$ |  $$ /  $$ |$$ |  $$ |$$ |      $$ |      
\$$\  $$  |$$$$$$$$ |$$$$$$$  |  $$ |  $$$$$$$$ |$$$$$$$\ |$$ |      $$$$$\    
 \$$\$$  / $$  __$$ |$$  __$$<   $$ |  $$  __$$ |$$  __$$\ $$ |      $$  __|   
  \$$$  /  $$ |  $$ |$$ |  $$ |  $$ |  $$ |  $$ |$$ |  $$ |$$ |      $$ |      
   \$  /   $$ |  $$ |$$ |  $$ |$$$$$$\ $$ |  $$ |$$$$$$$  |$$$$$$$$\ $$$$$$$$\ 
    \_/    \__|  \__|\__|  \__|\______|\__|  \__|\_______/ \________|\________|
/*============================================================================//
//                            GLOBAL  VARIABLES                               //
//============================================================================*/

/*----------------------------------------------------------------------------//
//                                GLOBAL VARIABLES                            //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                             GLOBAL DEBUG VARIABLES                         //
//----------------------------------------------------------------------------*/









 /*$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\  $$\   $$\ $$$$$$$$\  $$$$$$\  $$$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\ $$ |  $$ |$$  _____|$$  __$$\ $$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|$$ |  $$ |$$ |      $$ /  $$ |$$ |  $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |      $$$$$$$$ |$$$$$\    $$$$$$$$ |$$ |  $$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |      $$  __$$ |$$  __|   $$  __$$ |$$ |  $$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\ $$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |$$ |  $$ |$$$$$$$$\ $$ |  $$ |$$$$$$$  |
\__|       \______/ \__|  \__| \______/ \__|  \__|\________|\__|  \__|\_______/ 
/*============================================================================//
//                                 GLOBAL FUNCTIONS                           //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                            GLOBAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
extern ret_code_t m_timer_init(void);
extern ret_code_t m_timer_start(void);
extern ret_code_t m_timer_stop(void);
extern bool m_timer_is_expired(M_TIMER_ID_TYPE id);
extern uint32_t m_timer_get(M_TIMER_ID_TYPE id);
extern void m_timer_set(M_TIMER_ID_TYPE id, uint32_t tick);
extern void m_timer_add_handler(M_TIMER_ID_TYPE id, uint32_t tick, m_timer_handler_t handler);
extern void m_timer_update_handler(void);
extern void m_timer_nrf_start(void);
extern void m_timer_nrf_stop(void);
extern uint32_t m_timer_get_counter(void);
extern uint32_t m_timer_get_counter_delta(void);
extern uint32_t m_timer_get_delta(uint32_t prv, uint32_t now);


/*----------------------------------------------------------------------------//
//                         GLOBAL DEBUG FUNCTION HEADS                        //
//----------------------------------------------------------------------------*/




#endif // __M_TIMER_H 
