/// \file m_heatmap.h
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 16-Dec-2018  Flimbs
* + Created initial version
================================================================================
*/
/*============================================================================*/
/*                            COMPILER DIRECTIVES                             */
/*============================================================================*/


/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/
#include "sdk_config.h" // mokibo_config.h && sdk_config.h
#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
	// should be placed after "sdk_config.h"
	#include "SEGGER_RTT.h"
#endif

#include "m_flimbs.h"


/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/

#ifdef FLIMBS_20181210_ONE_POINT_TOUCH_ADJUST_BY_HEATMAP
#define TOUCH_MAP_MAX_X 157
#define TOUCH_MAP_MAX_Y 62
#endif
/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/

/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/

/*============================================================================*/
/*                               GLOBAL FUNCTIONS                             */
/*============================================================================*/

#ifdef FLIMBS_20181210_ONE_POINT_TOUCH_ADJUST_BY_HEATMAP
uint8_t m_heatmap_get_touch_sensitive_x(uint16_t x, uint16_t y);
uint8_t m_heatmap_get_touch_sensitive_y(uint16_t x, uint16_t y);
#endif

