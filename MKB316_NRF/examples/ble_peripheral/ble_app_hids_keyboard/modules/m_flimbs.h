#include "sdk_config.h" // mokibo_config.h && sdk_config.h
#include "sdk_common.h"


//============================================================================*/
//      FLIMBS_20181210_POWERONOFF_LIGHTING_CHANGE_TO_WHITE       
//============================================================================*/
#ifdef FLIMBS_20181210_POWERONOFF_LIGHTING_CHANGE_TO_WHITE
//: 전원 ONOFF 시 흰색으로 , 새로운 패턴으로 점멸
#endif

//============================================================================*/
//      FLIMBS_20181210_ONE_POINT_TOUCH_ADJUST_BY_HEATMAP       
//============================================================================*/
#ifdef FLIMBS_20181210_ONE_POINT_TOUCH_ADJUST_BY_HEATMAP
//: 포인팅시 센서 빈공간 도약현상 제거위해 heatmap.c 파일 가져와서 매핑하는 코드
void m_flimbs_adjust_onepointXY_flimbs(uint16_t posX, uint16_t posY, uint8_t *p_heatmap_counter, int16_t *p_diffX, int16_t *p_diffY);
#endif

//============================================================================*/
//      FLIMBS_20181210_TWO_POINT_SCROLL_ADJUST_BY_SPEED_CONTEXTUAL
//============================================================================*/
#ifdef FLIMBS_20181210_TWO_POINT_SCROLL_ADJUST_BY_SPEED_CONTEXTUAL
//: 스크롤시 스크롤 속도에 따라 느릴수록 감속시킴
#define SCROLL_MAP_MAX 8
uint16_t m_flimbs_adjust_scroll_two(uint16_t speed);
#endif


//============================================================================*/
//      FLIMBS_20190110_CHANGE_LUI_LALT_ON_CENTRAL_MAC
//============================================================================*/
#ifdef FLIMBS_20190110_CHANGE_LUI_LALT_ON_CENTRAL_MAC
//:central mac 변경시 CMD, ALT 키위치 변경
void m_flimbs_change_lookup_table_when_read(uint8_t *buf);
#endif


//============================================================================
//     FLIMBS_20190111_CHANGE_LED_COLOR_GBR 
//============================================================================
#ifdef FLIMBS_20190111_CHANGE_LED_COLOR_GBR
//동글이 빠짐에 따라 컬러를 BT1 BT2 BT3 각 GBR로 변경
#endif

//============================================================================
//      FLIMBS_20190102_TOUCH_LOCK_LED_DEPENDS_ON_BTCH
//============================================================================
#ifdef FLIMBS_20190102_TOUCH_LOCK_LED_DEPENDS_ON_BTCH
//터치락 모드 들어가면 LED가 현재 채널색으로 토글된다
#endif



//============================================================================
//      FLIMBS_20190110_THREE_POINT_GESTURE_LEFTRIGHT_WITHOUT_OPTION
//============================================================================
#ifdef FLIMBS_20190110_THREE_POINT_GESTURE_LEFTRIGHT_WITHOUT_OPTION
//3점 터치 Left Right 제스처 사용시 three.option 으로 제스처고정하는걸 해제하고 
//1번만 제스처보내도록 변경
#endif





//============================================================================
//      FLIMBS_20190118_ADD_FILTERKEY
//============================================================================
#ifdef FLIMBS_20190118_ADD_FILTERKEY
//터치 모드에서 입력되는 키 추가 (Esc ~ Tab CapsLock Shift Ctrl Fn Win Alt, )
void m_flimbs_swapfilterKey(uint8_t i, uint8_t *new_keys, uint8_t *dst, const uint8_t *src, uint8_t size);

#endif


//============================================================================
//     AREA_PROPERTIE_FLiMBS_20190121_MOD_TOUCH_WHOLE_AREA_WITH_FILTER_KEYS
//============================================================================
#if (AREA_PROPERTIE == AREA_PROPERTIE_FLiMBS_20190121_MOD_TOUCH_WHOLE_AREA_WITH_FILTER_KEYS)
//터치 영역을 ALWAYS_FORCE_TO_WHOLE_AREA 로 할때 새로 추가한 필터키들에 맞춰서 영역조절
// AREA_PROPERTIE 에서 스위
ret_code_t m_flimbs_touch_clipping_filterkey(uint16_t x, uint16_t y);
#endif

