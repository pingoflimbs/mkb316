/// \file m_comm.c
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/










/*\      $$\  $$$$$$\  $$$$$$$\  $$\   $$\ $$\       $$$$$$$$\  $$$$$$\  
$$$\    $$$ |$$  __$$\ $$  __$$\ $$ |  $$ |$$ |      $$  _____|$$  __$$\ 
$$$$\  $$$$ |$$ /  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$ /  \__|
$$\$$\$$ $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$$$$\    \$$$$$$\  
$$ \$$$  $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$  __|    \____$$\ 
$$ |\$  /$$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$\   $$ |
$$ | \_/ $$ | $$$$$$  |$$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$$\ \$$$$$$  |
\__|     \__| \______/ \_______/  \______/ \________|\________| \______/ 
//============================================================================//
//                              LOCAL MODULEL USED                            //
//============================================================================*/
#include "sdk_config.h" // mokibo_config.h && sdk_config.h
#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
	// should be placed after "sdk_config.h"
	#include "SEGGER_RTT.h"
#endif

#include "sdk_common.h"
#include "ble_dis.h"
#include "ble_conn_params.h"

#if MOKIBO_NFC_ENABLED
	#include "ble_advertising.h"
#endif

#include "nrf_sdh.h"
#include "peer_manager.h"
#include "nrf_ble_gatt.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#if	(MOKIBO_DFU_ENABLED)
	#include "nrf_pwr_mgmt.h"
	#include "nrf_dfu_svci.h"
	#include "nrf_svci_async_function.h"
	#include "nrf_svci_async_handler.h"
	#include "ble_dfu.h"
#endif

#include "nrf_fstorage.h"
#include "drv_fds.h"
#include "drv_keyboard.h"

#include "m_keyboard.h"        
#include "m_util.h"        
#include "m_comm_adv.h"        
#include "m_led.h"        
#include "m_mokibo.h"        
#include "m_timer.h"        
#include "m_dongle.h"        
#include "m_touch.h"     
#include "m_comm.h"     

#if MOKIBO_NFC_ENABLED
	#include "m_nfc.h"
#endif

#include "nrf_delay.h"
#include "m_event.h"  // for RestoreInfoFromFlash()









 /*$$$$\   $$$$$$\  $$\   $$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$\   $$\ $$$$$$$$\ 
$$  __$$\ $$  __$$\ $$$\  $$ |$$  __$$\\__$$  __|$$  __$$\ $$$\  $$ |\__$$  __|
$$ /  \__|$$ /  $$ |$$$$\ $$ |$$ /  \__|  $$ |   $$ /  $$ |$$$$\ $$ |   $$ |   
$$ |      $$ |  $$ |$$ $$\$$ |\$$$$$$\    $$ |   $$$$$$$$ |$$ $$\$$ |   $$ |   
$$ |      $$ |  $$ |$$ \$$$$ | \____$$\   $$ |   $$  __$$ |$$ \$$$$ |   $$ |   
$$ |  $$\ $$ |  $$ |$$ |\$$$ |$$\   $$ |  $$ |   $$ |  $$ |$$ |\$$$ |   $$ |   
\$$$$$$  | $$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$ |  $$ |$$ | \$$ |   $$ |   
 \______/  \______/ \__|  \__| \______/   \__|   \__|  \__|\__|  \__|   \__|   
//============================================================================//
//                                LOCAL CONSTANTS                             //
//============================================================================*/








/*$$$$$$\ $$\     $$\ $$$$$$$\  $$$$$$$$\ $$$$$$$\  $$$$$$$$\ $$$$$$$$\  $$$$$$\  
\__$$  __|\$$\   $$  |$$  __$$\ $$  _____|$$  __$$\ $$  _____|$$  _____|$$  __$$\ 
   $$ |    \$$\ $$  / $$ |  $$ |$$ |      $$ |  $$ |$$ |      $$ |      $$ /  \__|
   $$ |     \$$$$  /  $$$$$$$  |$$$$$\    $$ |  $$ |$$$$$\    $$$$$\    \$$$$$$\  
   $$ |      \$$  /   $$  ____/ $$  __|   $$ |  $$ |$$  __|   $$  __|    \____$$\ 
   $$ |       $$ |    $$ |      $$ |      $$ |  $$ |$$ |      $$ |      $$\   $$ |
   $$ |       $$ |    $$ |      $$$$$$$$\ $$$$$$$  |$$$$$$$$\ $$ |      \$$$$$$  |
   \__|       \__|    \__|      \________|\_______/ \________|\__|       \______/ 
//============================================================================//
//                                LOCAL TYPEDEFS                              //
//============================================================================*/
 
#define MAX_BUFFER_ENTRIES                  20                                          /**< Number of elements that can be enqueued */

/**Buffer queue access macros
 *
 * @{ */
/** Initialization of buffer list */
#define BUFFER_LIST_INIT()     \
    do                         \
    {                          \
        buffer_list.rp    = 0; \
        buffer_list.wp    = 0; \
        buffer_list.count = 0; \
    } while (0)

/** Provide status of data list is full or not */
#define BUFFER_LIST_FULL() \
    ((MAX_BUFFER_ENTRIES == buffer_list.count - 1) ? true : false)

/** Provides status of buffer list is empty or not */
#define BUFFER_LIST_EMPTY() \
    ((0 == buffer_list.count) ? true : false)

#define BUFFER_ELEMENT_INIT(i)                 \
    do                                         \
    {                                          \
        buffer_list.buffer[(i)].p_data = NULL; \
		memset(buffer_list.buffer[(i)].data, 0, INPUT_REP_KEYS_MAX_LEN);\
    } while (0)

/** @} */

/** Abstracts buffer element */
typedef struct hid_key_buffer
{
    uint8_t      data_offset; /**< Max Data that can be buffered for all entries */
	uint8_t  	 rep_index;
    uint8_t      data_len;    /**< Total length of data */
    uint8_t      data[INPUT_REP_KEYS_MAX_LEN];        /**< Scanned key */
    uint8_t    * p_data;      /**< Scanned key pattern */
    ble_hids_t * p_instance;  /**< Identifies peer and service instance */
} buffer_entry_t;

STATIC_ASSERT(sizeof(buffer_entry_t) % 4 == 0);

/** Circular buffer list */
typedef struct
{
    buffer_entry_t buffer[MAX_BUFFER_ENTRIES]; /**< Maximum number of entries that can enqueued in the list */
    uint8_t        rp;                         /**< Index to the read location */
    uint8_t        wp;                         /**< Index to write location */
    uint8_t        count;                      /**< Number of elements in the list */
} buffer_list_t;

STATIC_ASSERT(sizeof(buffer_list_t) % 4 == 0);
 








/*\    $$\  $$$$$$\  $$$$$$$\  $$$$$$\  $$$$$$\  $$$$$$$\  $$\       $$$$$$$$\ 
$$ |   $$ |$$  __$$\ $$  __$$\ \_$$  _|$$  __$$\ $$  __$$\ $$ |      $$  _____|
$$ |   $$ |$$ /  $$ |$$ |  $$ |  $$ |  $$ /  $$ |$$ |  $$ |$$ |      $$ |      
\$$\  $$  |$$$$$$$$ |$$$$$$$  |  $$ |  $$$$$$$$ |$$$$$$$\ |$$ |      $$$$$\    
 \$$\$$  / $$  __$$ |$$  __$$<   $$ |  $$  __$$ |$$  __$$\ $$ |      $$  __|   
  \$$$  /  $$ |  $$ |$$ |  $$ |  $$ |  $$ |  $$ |$$ |  $$ |$$ |      $$ |      
   \$  /   $$ |  $$ |$$ |  $$ |$$$$$$\ $$ |  $$ |$$$$$$$  |$$$$$$$$\ $$$$$$$$\ 
    \_/    \__|  \__|\__|  \__|\______|\__|  \__|\_______/ \________|\_______/
//============================================================================//
//                                   VARIABLES                                //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                                GLOBAL VARIABLES                            //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                                 LOCAL VARIABLES                            //
//----------------------------------------------------------------------------*/
static buffer_list_t     buffer_list;                               /**< List to enqueue not just data to be sent, but also related information like the handle, connection handle etc */

BLE_HIDS_DEF(m_hids);     /**< Structure used to identify the HID service. */
		// static ble_hids_t m_hids;
		// NRF_SDH_BLE_OBSERVER(m_hids_obs, BLE_HIDS_BLE_OBSERVER_PRIO, ble_hids_on_ble_evt, &m_hids);

// see: nrf_ble_gatt.h
// change: Stanley has changed this macro.
NRF_BLE_GATT_DEF(m_gatt); /**< GATT module instance. */
		// static nrf_ble_gatt_t  m_gatt;
		// NRF_SDH_BLE_OBSERVER(m_gatt_obs, NRF_BLE_GATT_BLE_OBSERVER_PRIO, nrf_ble_gatt_on_ble_evt, &m_gatt);

static const uint8_t m_vol_mute_cmd[M_COMM_CONSUMER_BUF_LEN]	= {0xE2, 0x00, 0x00};//226
static const uint8_t m_vol_down_cmd[M_COMM_CONSUMER_BUF_LEN] 	= {0xEA, 0x00, 0x00};
static const uint8_t m_vol_up_cmd[M_COMM_CONSUMER_BUF_LEN]   	= {0xE9, 0x00, 0x00};

static const uint8_t m_android_home[M_COMM_CONSUMER_BUF_LEN]	= {35, 2, 0};
static const uint8_t m_android_srch[M_COMM_CONSUMER_BUF_LEN]	= {207,0,0};
static const uint8_t m_android_menu[M_COMM_CONSUMER_BUF_LEN]	= {162,1,0};
static const uint8_t m_android_lock[M_COMM_CONSUMER_BUF_LEN]	= {48,0,0};
static const uint8_t m_android_back[M_COMM_CONSUMER_BUF_LEN]	= {70,0,0};

static const uint8_t m_iphone_home[M_COMM_CONSUMER_BUF_LEN]	= {64, 0x00, 0x00};
static const uint8_t m_iphone_lock[M_COMM_CONSUMER_BUF_LEN]	= {48, 0x00, 0x00};
static const uint8_t m_iphone_vkbd[M_COMM_CONSUMER_BUF_LEN]	= {184, 0x00, 0x00};

static const uint8_t m_consumer_no_cmd[M_COMM_CONSUMER_BUF_LEN] = {0x00, 0x00, 0x00};
static const uint8_t m_virtual_keyboard_cmd[M_COMM_CONSUMER_BUF_LEN] = {0xAE, 0x01, 0x00};

static const char *m_mokibo_name[M_COMM_CHANNEL_MAX] = \
{ \
    	MOKIBO_CH1_STR, \
    	MOKIBO_CH2_STR, \
    	MOKIBO_CH3_STR, \
};

m_comm_mode_t		m_comm_mode = M_COMM_MODE_BLE;
m_comm_config_t		m_config;
passkey_t			m_passkey;
bool				m_caps_on = false;                         /**< Variable to indicate if Caps Lock is turned on. */
uint16_t			m_conn_handle  = BLE_CONN_HANDLE_INVALID;  /**< Handle of the current connection. */

uint8_t				m_bt_ch= M_COMM_CHANNEL_MAX;
uint16_t			m_ble_evt_count = 0;

static const hid_to_ascii_t m_passkey_table[] = 
{
	{APP_USBD_HID_KBD_0, 	0x30},
	{APP_USBD_HID_KBD_1, 	0x31},
	{APP_USBD_HID_KBD_2, 	0x32},
	{APP_USBD_HID_KBD_3, 	0x33},
	{APP_USBD_HID_KBD_4, 	0x34},
	{APP_USBD_HID_KBD_5, 	0x35},
	{APP_USBD_HID_KBD_6, 	0x36},
	{APP_USBD_HID_KBD_7, 	0x37},
	{APP_USBD_HID_KBD_8, 	0x38},
	{APP_USBD_HID_KBD_9, 	0x39},
};

#if LAYOUT_OPTION == LAYOUT_OPTION_UNIQ
static uint8_t m_report_map_data[] =
{
/////////////////////////////////////////////////////////////////////////
	// Keyboard 엔화 백슬래시 동작하는버전.
	/////////////////////////////////////////////////////////////////////////
	0x05, 0x01,       // Usage Page (Generic Desktop)
	0x09, 0x06,       // Usage (Keyboard)
	0xA1, 0x01,       // Collection (Application)
		// Report ID 1: Keyboard
		0x85, 0x01,  	// Report Id (1)
		
		0x05, 0x07,       // Usage Page (Key Codes)
			0x19, 0xe0,       // Usage Minimum (224)
			0x29, 0xe7,       // Usage Maximum (231)
			0x15, 0x00,       // Logical Minimum (0)
			0x25, 0x01,       // Logical Maximum (1)
			0x75, 0x01,       // Report Size (1)
			0x95, 0x08,       // Report Count (8)
			0x81, 0x02,       // Input (Data, Variable, Absolute)

			0x95, 0x01,       // Report Count (1)
			0x75, 0x08,       // Report Size (8)
			0x81, 0x03,       // Input (Constant) reserved byte(1)

			0x95, 0x06,       // Report Count (6)
			0x75, 0x08,       // Report Size (8)
			0x15, 0x00,       // Logical Minimum (0)
			0x25, 0x65,       // Logical Maximum (101)

		0x05, 0x07,       // Usage Page (Key codes)
			0x19, 0x00,       // Usage Minimum (0)
			0x29, 0x65,       // Usage Maximum (101)
			0x81, 0x00,       // Input (Data, Array) Key array(6 bytes)
	0xC0,              // End Collection (Application

	



	/////////////////////////////////////////////////////////////////////////
	// Mouse: Click/wheel/Move
	/////////////////////////////////////////////////////////////////////////
	0x05, 0x01, // Usage Page (Generic Desktop)
	0x09, 0x02, // Usage (Mouse)

	0xA1, 0x01, // Collection (Application)

	// Report ID 2: Mouse buttons + scroll/pan
	0x85, INPUT_REP_REF_BUTTONS_ID, 	// Report Id 2
	0x09, 0x01,       // Usage (Pointer)
	0xA1, 0x00,       // Collection (Physical)
	0x95, 0x05,       // Report Count (3)
	0x75, 0x01,       // Report Size (1)
	0x05, 0x09,       // Usage Page (Buttons)
	0x19, 0x01,       // Usage Minimum (01)
	0x29, 0x05,       // Usage Maximum (05)
	0x15, 0x00,       // Logical Minimum (0)
	0x25, 0x01,       // Logical Maximum (1)
	0x81, 0x02,       // Input (Data, Variable, Absolute)
	0x95, 0x01,       // Report Count (1)
	0x75, 0x03,       // Report Size (3)
	0x81, 0x01,       // Input (Constant) for padding
	0x75, 0x08,       // Report Size (8)
	0x95, 0x01,       // Report Count (1)
	0x05, 0x01,       // Usage Page (Generic Desktop)
	0x09, 0x38,       // Usage (Wheel)
	0x15, 0x81,       // Logical Minimum (-127)
	0x25, 0x7F,       // Logical Maximum (127)
	0x81, 0x06,       // Input (Data, Variable, Relative)
	0x05, 0x0C,       // Usage Page (Consumer)
	0x0A, 0x38, 0x02, // Usage (Undefined)
	0x95, 0x01,       // Report Count (1)
	0x81, 0x06,       // Input (Data,Value,Relative,Bit Field), 0xFF for left, or 0x00 for no scroll, or 0x01 for right
	0xC0,             // End Collection (Physical)
	
	// Report ID 3: Mouse motion
	0x85, INPUT_REP_REF_MOVEMENT_ID, 	// Report Id 3
	0x09, 0x01,       // Usage (Pointer)
	0xA1, 0x00,       // Collection (Physical)
	0x75, 0x0C,       // Report Size (12)
	0x95, 0x02,       // Report Count (2)
	0x05, 0x01,       // Usage Page (Generic Desktop)
	0x09, 0x30,       // Usage (X)
	0x09, 0x31,       // Usage (Y)
	0x16, 0x01, 0xF8, // Logical maximum (2047)
	0x26, 0xFF, 0x07, // Logical minimum (-2047)
	0x81, 0x06,       // Input (Data, Variable, Relative)
	0xC0,             // End Collection (Physical)
	0xC0,             // End Collection (Application)
	
	/////////////////////////////////////////////////////////////////////////
	// Consumer control: volume up/down
	/////////////////////////////////////////////////////////////////////////
    0x05, 0x0C,    		// Usage Page (Consumer Devices)
    0x09, 0x01,       // Usage (Consumer Control)
    0xA1, 0x01,       // Collection (Application)
    0x85, INPUT_REP_REF_CONSUMER_ID,//      Report ID (CONSUMER_CTRL_IN_REP_ID)
    0x75, 12,					//      Report Size (CONSUMER_CTRL_IN_REP_SIZE)
    0x95, 2,					//      Report Count (CONSUMER_CTRL_IN_REP_COUNT)
    0x15, 0x00,       //      Logical Minimum (0)
    0x26, 0xFF, 0x07, //      Logical Maximum (2047)
    0x19, 0x00,       //      Usage Minimum (0)
    0x2A, 0xFF, 0x07, //      Usage Maximum (2047)
    0x81, 0x00,       //      Input (Data, Ary, Abs)
    0xC0,          		// End Collection
};

/*
static uint8_t m_report_map_data[] =
{
	/////////////////////////////////////////////////////////////////////////
	// Keyboard
	/////////////////////////////////////////////////////////////////////////
	0x05, 0x01,       // Usage Page (Generic Desktop)
	0x09, 0x06,       // Usage (Keyboard)
	0xA1, 0x01,       // Collection (Application)

	// Report ID 1: Keyboard
	0x85, INPUT_REP_REF_ID,  	// Report Id (1)
	0x05, 0x07,       // Usage Page (Key Codes)
	0x19, 0xe0,       // Usage Minimum (224)
	0x29, 0xe7,       // Usage Maximum (231)
	0x15, 0x00,       // Logical Minimum (0)
	0x25, 0x01,       // Logical Maximum (1)
	0x75, 0x01,       // Report Size (1)
	0x95, 0x08,       // Report Count (8)
	0x81, 0x02,       // Input (Data, Variable, Absolute)
	0x95, 0x01,       // Report Count (1)
	0x75, 0x08,       // Report Size (8)
	0x81, 0x01,       // Input (Constant) reserved byte(1)
	0x95, 0x05,       // Report Count (5)
	0x75, 0x01,       // Report Size (1)
	0x05, 0x08,       // Usage Page (Page# for LEDs)
	0x19, 0x01,       // Usage Minimum (1)
	0x29, 0x05,       // Usage Maximum (5)
	0x91, 0x02,       // Output (Data, Variable, Absolute), Led report
	0x95, 0x01,       // Report Count (1)
	0x75, 0x03,       // Report Size (3)
	0x91, 0x01,       // Output (Data, Variable, Absolute), Led report padding
	0x95, 0x06,       // Report Count (6)
	0x75, 0x08,       // Report Size (8)
	0x15, 0x00,       // Logical Minimum (0)
	0x25, 0xFF,//0x65,       // Logical Maximum (101)
	0x05, 0x07,       // Usage Page (Key codes)
	0x19, 0x00,       // Usage Minimum (0)
	0x29, 0xFF,//0x29, 0x65,       // Usage Maximum (101)
	0x81, 0x00,       // Input (Data, Array) Key array(6 bytes)

	0x09, 0x05,       // Usage (Vendor Defined)
	0x15, 0x00,       // Logical Minimum (0)
	0x26, 0xFF, 0x00, // Logical Maximum (255)
	0x75, 0x08,       // Report Count (2)
	0x95, 0x02,       // Report Size (8 bit)
	0xB1, 0x02,       // Feature (Data, Variable, Absolute)

	0xC0,              // End Collection (Application)

	/////////////////////////////////////////////////////////////////////////
	// Mouse: Click/wheel/Move
	/////////////////////////////////////////////////////////////////////////
	0x05, 0x01, // Usage Page (Generic Desktop)
	0x09, 0x02, // Usage (Mouse)

	0xA1, 0x01, // Collection (Application)

	// Report ID 2: Mouse buttons + scroll/pan
	0x85, INPUT_REP_REF_BUTTONS_ID, 	// Report Id 2
	0x09, 0x01,       // Usage (Pointer)
	0xA1, 0x00,       // Collection (Physical)
	0x95, 0x05,       // Report Count (3)
	0x75, 0x01,       // Report Size (1)
	0x05, 0x09,       // Usage Page (Buttons)
	0x19, 0x01,       // Usage Minimum (01)
	0x29, 0x05,       // Usage Maximum (05)
	0x15, 0x00,       // Logical Minimum (0)
	0x25, 0x01,       // Logical Maximum (1)
	0x81, 0x02,       // Input (Data, Variable, Absolute)
	0x95, 0x01,       // Report Count (1)
	0x75, 0x03,       // Report Size (3)
	0x81, 0x01,       // Input (Constant) for padding
	0x75, 0x08,       // Report Size (8)
	0x95, 0x01,       // Report Count (1)
	0x05, 0x01,       // Usage Page (Generic Desktop)
	0x09, 0x38,       // Usage (Wheel)
	0x15, 0x81,       // Logical Minimum (-127)
	0x25, 0x7F,       // Logical Maximum (127)
	0x81, 0x06,       // Input (Data, Variable, Relative)
	0x05, 0x0C,       // Usage Page (Consumer)
	0x0A, 0x38, 0x02, // Usage (Undefined)
	0x95, 0x01,       // Report Count (1)
	0x81, 0x06,       // Input (Data,Value,Relative,Bit Field), 0xFF for left, or 0x00 for no scroll, or 0x01 for right
	0xC0,             // End Collection (Physical)
	
	// Report ID 3: Mouse motion
	0x85, INPUT_REP_REF_MOVEMENT_ID, 	// Report Id 3
	0x09, 0x01,       // Usage (Pointer)
	0xA1, 0x00,       // Collection (Physical)
	0x75, 0x0C,       // Report Size (12)
	0x95, 0x02,       // Report Count (2)
	0x05, 0x01,       // Usage Page (Generic Desktop)
	0x09, 0x30,       // Usage (X)
	0x09, 0x31,       // Usage (Y)
	0x16, 0x01, 0xF8, // Logical maximum (2047)
	0x26, 0xFF, 0x07, // Logical minimum (-2047)
	0x81, 0x06,       // Input (Data, Variable, Relative)
	0xC0,             // End Collection (Physical)
	0xC0,             // End Collection (Application)
	
	/////////////////////////////////////////////////////////////////////////
	// Consumer control: volume up/down
	/////////////////////////////////////////////////////////////////////////
    0x05, 0x0C,    		// Usage Page (Consumer Devices)
    0x09, 0x01,       // Usage (Consumer Control)
    0xA1, 0x01,       // Collection (Application)
    0x85, INPUT_REP_REF_CONSUMER_ID,//      Report ID (CONSUMER_CTRL_IN_REP_ID)
    0x75, 12,					//      Report Size (CONSUMER_CTRL_IN_REP_SIZE)
    0x95, 2,					//      Report Count (CONSUMER_CTRL_IN_REP_COUNT)
    0x15, 0x00,       //      Logical Minimum (0)
    0x26, 0xFF, 0x07, //      Logical Maximum (2047)
    0x19, 0x00,       //      Usage Minimum (0)
    0x2A, 0xFF, 0x07, //      Usage Maximum (2047)
    0x81, 0x00,       //      Input (Data, Ary, Abs)
    0xC0,          		// End Collection
};

*/
#else

static uint8_t m_report_map_data[] =
{
	/////////////////////////////////////////////////////////////////////////
	// Keyboard
	/////////////////////////////////////////////////////////////////////////
	0x05, 0x01,       // Usage Page (Generic Desktop)
	0x09, 0x06,       // Usage (Keyboard)
	0xA1, 0x01,       // Collection (Application)

	// Report ID 1: Keyboard
	0x85, INPUT_REP_REF_ID,  	// Report Id (1)
	0x05, 0x07,       // Usage Page (Key Codes)
	0x19, 0xe0,       // Usage Minimum (224)
	0x29, 0xe7,       // Usage Maximum (231)
	0x15, 0x00,       // Logical Minimum (0)
	0x25, 0x01,       // Logical Maximum (1)
	0x75, 0x01,       // Report Size (1)
	0x95, 0x08,       // Report Count (8)
	0x81, 0x02,       // Input (Data, Variable, Absolute)

	0x95, 0x01,       // Report Count (1)
	0x75, 0x08,       // Report Size (8)
	0x81, 0x01,       // Input (Constant) reserved byte(1)

	0x95, 0x05,       // Report Count (5)
	0x75, 0x01,       // Report Size (1)
	0x05, 0x08,       // Usage Page (Page# for LEDs)
	0x19, 0x01,       // Usage Minimum (1)
	0x29, 0x05,       // Usage Maximum (5)
	0x91, 0x02,       // Output (Data, Variable, Absolute), Led report

	0x95, 0x01,       // Report Count (1)
	0x75, 0x03,       // Report Size (3)
	0x91, 0x01,       // Output (Data, Variable, Absolute), Led report padding

	0x95, 0x06,       // Report Count (6)
	0x75, 0x08,       // Report Size (8)
	0x15, 0x00,       // Logical Minimum (0)
	0x25, 0x65,       // Logical Maximum (101)
	0x05, 0x07,       // Usage Page (Key codes)
	0x19, 0x00,       // Usage Minimum (0)
	0x29, 0x65,       // Usage Maximum (101)
	0x81, 0x00,       // Input (Data, Array) Key array(6 bytes)

	0x09, 0x05,       // Usage (Vendor Defined)
	0x15, 0x00,       // Logical Minimum (0)
	0x26, 0xFF, 0x00, // Logical Maximum (255)
	0x75, 0x08,       // Report Count (2)
	0x95, 0x02,       // Report Size (8 bit)
	0xB1, 0x02,       // Feature (Data, Variable, Absolute)

	0xC0,              // End Collection (Application)

	/////////////////////////////////////////////////////////////////////////
	// Mouse: Click/wheel/Move
	/////////////////////////////////////////////////////////////////////////
	0x05, 0x01, // Usage Page (Generic Desktop)
	0x09, 0x02, // Usage (Mouse)

	0xA1, 0x01, // Collection (Application)

	// Report ID 2: Mouse buttons + scroll/pan
	0x85, INPUT_REP_REF_BUTTONS_ID, 	// Report Id 2
	0x09, 0x01,       // Usage (Pointer)
	0xA1, 0x00,       // Collection (Physical)
	0x95, 0x05,       // Report Count (3)
	0x75, 0x01,       // Report Size (1)
	0x05, 0x09,       // Usage Page (Buttons)
	0x19, 0x01,       // Usage Minimum (01)
	0x29, 0x05,       // Usage Maximum (05)
	0x15, 0x00,       // Logical Minimum (0)
	0x25, 0x01,       // Logical Maximum (1)
	0x81, 0x02,       // Input (Data, Variable, Absolute)
	0x95, 0x01,       // Report Count (1)
	0x75, 0x03,       // Report Size (3)
	0x81, 0x01,       // Input (Constant) for padding
	0x75, 0x08,       // Report Size (8)
	0x95, 0x01,       // Report Count (1)
	0x05, 0x01,       // Usage Page (Generic Desktop)
	0x09, 0x38,       // Usage (Wheel)
	0x15, 0x81,       // Logical Minimum (-127)
	0x25, 0x7F,       // Logical Maximum (127)
	0x81, 0x06,       // Input (Data, Variable, Relative)
	0x05, 0x0C,       // Usage Page (Consumer)
	0x0A, 0x38, 0x02, // Usage (Undefined)
	0x95, 0x01,       // Report Count (1)
	0x81, 0x06,       // Input (Data,Value,Relative,Bit Field), 0xFF for left, or 0x00 for no scroll, or 0x01 for right
	0xC0,             // End Collection (Physical)
	
	// Report ID 3: Mouse motion
	0x85, INPUT_REP_REF_MOVEMENT_ID, 	// Report Id 3
	0x09, 0x01,       // Usage (Pointer)
	0xA1, 0x00,       // Collection (Physical)
	0x75, 0x0C,       // Report Size (12)
	0x95, 0x02,       // Report Count (2)
	0x05, 0x01,       // Usage Page (Generic Desktop)
	0x09, 0x30,       // Usage (X)
	0x09, 0x31,       // Usage (Y)
	0x16, 0x01, 0xF8, // Logical maximum (2047)
	0x26, 0xFF, 0x07, // Logical minimum (-2047)
	0x81, 0x06,       // Input (Data, Variable, Relative)
	0xC0,             // End Collection (Physical)
	0xC0,             // End Collection (Application)
	
	/////////////////////////////////////////////////////////////////////////
	// Consumer control: volume up/down
	/////////////////////////////////////////////////////////////////////////
    0x05, 0x0C,    		// Usage Page (Consumer Devices)
    0x09, 0x01,       // Usage (Consumer Control)
    0xA1, 0x01,       // Collection (Application)
    0x85, INPUT_REP_REF_CONSUMER_ID,//      Report ID (CONSUMER_CTRL_IN_REP_ID)
    0x75, 12,					//      Report Size (CONSUMER_CTRL_IN_REP_SIZE)
    0x95, 2,					//      Report Count (CONSUMER_CTRL_IN_REP_COUNT)
    0x15, 0x00,       //      Logical Minimum (0)
    0x26, 0xFF, 0x07, //      Logical Maximum (2047)
    0x19, 0x00,       //      Usage Minimum (0)
    0x2A, 0xFF, 0x07, //      Usage Maximum (2047)
    0x81, 0x00,       //      Input (Data, Ary, Abs)
    0xC0,          		// End Collection
};

#endif
/*----------------------------------------------------------------------------//
//                                 DEBUG VARIABLES                            //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                                UNUSED VARIABLES                            //
//----------------------------------------------------------------------------*/





/*$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\  $$\   $$\ $$$$$$$$\  $$$$$$\  $$$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\ $$ |  $$ |$$  _____|$$  __$$\ $$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|$$ |  $$ |$$ |      $$ /  $$ |$$ |  $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |      $$$$$$$$ |$$$$$\    $$$$$$$$ |$$ |  $$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |      $$  __$$ |$$  __|   $$  __$$ |$$ |  $$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\ $$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |$$ |  $$ |$$$$$$$$\ $$ |  $$ |$$$$$$$  |
\__|       \______/ \__|  \__| \______/ \__|  \__|\________|\__|  \__|\_______/ 
//============================================================================//
//                                   FUNCHEAD                                 //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                            GLOBAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                             LOCAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                            POINTER FUNCTION HEADS                          //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                             DEBUG FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                             UNUSED FUNCTION HEADS                          //
//----------------------------------------------------------------------------*/


#if 1
void debshow_comm_config(const char *msg, m_comm_config_t comm_config);
void debshow_gap_addr(ble_gap_addr_t *p_g_addr);
void debshow_comm_adv_config(const char *msg, m_comm_adv_config_t comm_adv_config);
void debshow_mokibo_manufacture_info(const char *msg, m_mokibo_manufacture_info_t factory_data);
void debshow_mokibo_test_info(const char *msg, m_mokibo_test_info_t test_data);
#endif

static void dis_init(void);
static void ble_stack_init(void);

//static void gap_params_init(void);
static void gap_params_init(m_comm_channel_type_t selected);

void update_device_name(m_comm_channel_type_t selected);
static void gatt_init(void);
static void services_init(void);
static void conn_params_init(void);
static void conn_params_error_handler(uint32_t nrf_error);
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context);

static void hids_init(void);
static void on_hids_evt(ble_hids_t * p_hids, ble_hids_evt_t * p_evt);
static void on_hid_rep_char_write(ble_hids_evt_t * p_evt);

//static void sleep_mode_enter(void);
static void update_central(ble_gap_addr_t * p_peer_addr);
static void service_error_handler(uint32_t nrf_error);
static ret_code_t click_button_send(uint8_t left, uint8_t right, uint8_t center);
static ret_code_t wheel_send(int8_t up_down, int8_t left_right, uint8_t trv_dist);
static ret_code_t zoom_send(int8_t zoom);
static ret_code_t multi_touch_send(m_comm_evt_arg_multi_touch_t	touch);
static ret_code_t special_key_send(m_comm_evt_arg_special_key_t	key);
static ret_code_t clear_keyboard(void);
static ret_code_t send(send_type_t type, uint8_t *p_data, uint8_t len);
static ret_code_t passkey_handle(uint8_t *p_data, uint8_t len);

// by Stanley
void GetXYFromPoint(m_comm_evt_arg_point_t *point, int16_t *SignedX, int16_t *SignedY);
void SetXYToThePoint(m_comm_evt_arg_point_t *point, int16_t SignedX, int16_t SignedY);














/*\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ 
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\_*/

 /*$$$$\  $$\       $$$$$$\  $$$$$$$\   $$$$$$\  $$\                         
$$  __$$\ $$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                        
$$ /  \__|$$ |     $$ /  $$ |$$ |  $$ |$$ /  $$ |$$ |                        
$$ |$$$$\ $$ |     $$ |  $$ |$$$$$$$\ |$$$$$$$$ |$$ |                        
$$ |\_$$ |$$ |     $$ |  $$ |$$  __$$\ $$  __$$ |$$ |                        
$$ |  $$ |$$ |     $$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |                        
\$$$$$$  |$$$$$$$$\ $$$$$$  |$$$$$$$  |$$ |  $$ |$$$$$$$$\                   
 \______/ \________|\______/ \_______/ \__|  \__|\________|                  
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\ 
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__|
//============================================================================//
//                                 GLOBAL FUNCTIONS                           //
//============================================================================*/

/**@brief   Function for initializing the buffer queue used to key events that could not be
 *          transmitted
 *
 * @warning This handler is an example only. You need to analyze how you wish to buffer or buffer at
 *          all.
 *
 * @note    In case of HID keyboard, a temporary buffering could be employed to handle scenarios
 *          where encryption is not yet enabled or there was a momentary link loss or there were no
 *          Transmit buffers.
 */
void buffer_init(void)
{
    uint32_t buffer_count;

    BUFFER_LIST_INIT();

    for (buffer_count = 0; buffer_count < MAX_BUFFER_ENTRIES; buffer_count++)
    {
        BUFFER_ELEMENT_INIT(buffer_count);
    }
}


/**@brief Function for enqueuing key scan patterns that could not be transmitted either completely
 *        or partially.
 *
 * @warning This handler is an example only. You need to analyze how you wish to send the key
 *          release.
 *
 * @param[in]  p_hids         Identifies the service for which Key Notifications are buffered.
 * @param[in]  p_key_pattern  Pointer to key pattern.
 * @param[in]  pattern_len    Length of key pattern.
 * @param[in]  offset         Offset applied to Key-- Pattern when requesting a transmission on
 *                            dequeue, @ref buffer_dequeue.
 * @return     NRF_SUCCESS on success, else an error code indicating reason for failure.
 */
uint32_t buffer_enqueue(ble_hids_t * p_hids,
						uint8_t 	 rep_index,
                        uint8_t    * p_key_pattern,
                        uint16_t     pattern_len)
{
    buffer_entry_t * element;
    uint32_t         err_code = NRF_SUCCESS;

    if (BUFFER_LIST_FULL())
    {
        // Element cannot be buffered.
        err_code = NRF_ERROR_NO_MEM;
    }
    else
    {
        // Make entry of buffer element and copy data.
        element              = &buffer_list.buffer[(buffer_list.wp)];
        element->p_instance  = p_hids;
		element->rep_index	 = rep_index;
        element->p_data      = element->data;
		memcpy(element->p_data, p_key_pattern, pattern_len);
        element->data_len    = pattern_len;
		
		DEB("Enque: 0x[%x,%x,%x,%x,%x,%x,%x,%x]\n",
				element->data[0], element->data[1], element->data[2], element->data[3],
				element->data[4], element->data[5], element->data[6], element->data[7]);

        buffer_list.count++;
        buffer_list.wp++;

        if (buffer_list.wp == MAX_BUFFER_ENTRIES)
        {
            buffer_list.wp = 0;
        }
    }

    return err_code;
}


/**@brief   Function to dequeue key scan patterns that could not be transmitted either completely of
 *          partially.
 *
 * @warning This handler is an example only. You need to analyze how you wish to send the key
 *          release.
 *
 * @param[in]  tx_flag   Indicative of whether the dequeue should result in transmission or not.
 * @note       A typical example when all keys are dequeued with transmission is when link is
 *             disconnected.
 *
 * @return     NRF_SUCCESS on success, else an error code indicating reason for failure.
 */
uint32_t buffer_dequeue(bool tx_flag)
{
    buffer_entry_t * p_element;
    uint32_t         err_code = NRF_SUCCESS;

    if (BUFFER_LIST_EMPTY())
    {
        err_code = NRF_ERROR_NOT_FOUND;
    }
    else
    {
        bool remove_element = true;

		//while(!BUFFER_LIST_EMPTY())
		//{
        p_element = &buffer_list.buffer[(buffer_list.rp)];

        if (tx_flag)
        {
			err_code = ble_hids_inp_rep_send(&m_hids, p_element->rep_index, p_element->data_len, p_element->p_data);
            if (err_code != NRF_SUCCESS)
            {
                // Transmission could not be completed, do not remove the entry, adjust next data to
                // be transmitted
				if(p_element->rep_index != SEND_TOUCH_BUTTON && p_element->rep_index !=SEND_TOUCH_MOVE)
                remove_element         = false;
				DEB1("[%s] send error, 0x%x\n", __func__, p_element->p_data[2]);
            }
			else
			{
			DEB("[%s] %d counts, 0x%x\n", __func__, buffer_list.count, p_element->p_data[2]);
			DEB("Deque: 0x[%x,%x,%x,%x,%x,%x,%x,%x]\n",
				p_element->data[0], p_element->data[1], p_element->data[2], p_element->data[3],
				p_element->data[4], p_element->data[5], p_element->data[6], p_element->data[7]);
            }
        }

        if (remove_element)
        {
            BUFFER_ELEMENT_INIT(buffer_list.rp);

            buffer_list.rp++;
            buffer_list.count--;

            if (buffer_list.rp == MAX_BUFFER_ENTRIES)
            {
                buffer_list.rp = 0;
            }
        }
		//}
    }

    return err_code;
}



/*
$$\       $$$$$$\   $$$$$$\   $$$$$$\  $$\                                             
$$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                                            
$$ |     $$ /  $$ |$$ /  \__|$$ /  $$ |$$ |                                            
$$ |     $$ |  $$ |$$ |      $$$$$$$$ |$$ |                                            
$$ |     $$ |  $$ |$$ |      $$  __$$ |$$ |                                            
$$ |     $$ |  $$ |$$ |  $$\ $$ |  $$ |$$ |                                            
$$$$$$$$\ $$$$$$  |\$$$$$$  |$$ |  $$ |$$$$$$$$\                                       
\________|\______/  \______/ \__|  \__|\________|                                      
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\  $$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |$$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |$$ /  \__|
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |\$$$$$$\  
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ | \____$$\ 
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |$$\   $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |\$$$$$$  |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__| \______/ 
//============================================================================//
//                                 LOCAL FUNCTIONS                            //
//============================================================================*/
//-----------------------------------------------------------------------------------------------------------------------------------
//			update_central
//-----------------------------------------------------------------------------------------------------------------------------------
static void update_central(ble_gap_addr_t * p_peer_addr)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
    uint32_t err_code;
	m_comm_central_type_t central = M_COMM_CENTRAL_MICROSOFT;

	//DEB("update_central: BEGIN \n");
	
	// Only update when central is not set
	if(M_COMM_CENTRAL_OTHER	== m_config.config.central[m_config.config.selected_ch])
	{
		if(is_BT_channel(m_config.config.selected_ch))
		{
    /*
    //20181104_flimbs: central 버튼을 추가하면서 자동으로 mac 감지할 필요가 없어졌으므로 삭제함.
			if(m_util_oui_APPLE == m_util_get_manufacturer(p_peer_addr))
			{
				DEB("update_central: central = M_COMM_CENTRAL_MAC \n");
				central = M_COMM_CENTRAL_MAC;
			}
      */
		}
//		else if(is_DONGLE_channel(m_config.config.selected_ch))
//		{
//			DEB("update_central: TODO: --dongle-- \n");
//			; // TODO
//		}
		
		//-----------------------------------------------------------------------------------------------------------------------------------
		//DEB("update_central: m_comm_set_central_type(%d,central)\n",m_config.config.selected_ch);
		err_code = m_comm_set_central_type(m_config.config.selected_ch, central); PRINT_IF_ERROR("m_comm_set_central_type()", err_code);

#if 0		
		//-----------------------------------------------------------------------------------------------------------------------------------
		// Save any change in BLE states
		DEB("update_central: SaveInfoToFlash() .... maybe saved far later...\n");
		if( !is_DONGLE_channel(m_config.config.selected_ch) )
		{	// Dongle채널은 기억하지 않는다
			SaveInfoToFlash(m_config.config.selected_ch);
		}
#endif	
		//-----------------------------------------------------------------------------------------------------------------------------------
		//debshow_comm_config("update_central",m_config);
		
	} else {
		//DEB("update_central: no update - Only update when central is not set \n");
	}
	//DEB("update_central: END \n");
}

//-----------------------------------------------------------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------------------------------------------------------
/**@brief Function for handling Service errors.
 *
 * @details A pointer to this function will be passed to each service which may need to inform the
 *          application about an error.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void service_error_handler(uint32_t nrf_error)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
    //deleted by jaehong, 2019/06/28 APP_ERROR_HANDLER(nrf_error);
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			gap_params_init
//-----------------------------------------------------------------------------------------------------------------------------------
//	Function for the GAP initialization.
//	This function sets up all the necessary GAP (Generic Access Profile) parameters of the device
//	including the device name, appearance, and the preferred connection parameters.
//-
//static void gap_params_init(void)
static void gap_params_init(m_comm_channel_type_t selected)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
    ret_code_t              rv;
    ble_gap_conn_params_t   gap_conn_params;

	update_device_name(selected);
	//
    rv = sd_ble_gap_appearance_set(BLE_APPEARANCE_HID_KEYBOARD); 
	//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(rv);
	//
    memset(&gap_conn_params, 0, sizeof(gap_conn_params));
    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;
    rv = sd_ble_gap_ppcp_set(&gap_conn_params);  
	//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(rv);

	sd_ble_gap_tx_power_set(BLE_TX_POWER);
	
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			update_device_name
//-----------------------------------------------------------------------------------------------------------------------------------
//static void update_device_name(void)
void update_device_name(m_comm_channel_type_t selected)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
    ret_code_t              rv;
    ble_gap_conn_sec_mode_t sec_mode;
	//m_comm_channel_type_t   selected;

	//
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);
	//
	//selected = m_config.config.selected_ch;

	if(    (M_COMM_CHANNEL_BT1 != selected) 
		&& (M_COMM_CHANNEL_BT2 != selected) 
		&& (M_COMM_CHANNEL_BT3 != selected))
	{
		//
		selected = M_COMM_CHANNEL_BT1;
	}
	//
	//	for(int i=0; i <strlen(m_mokibo_name[M_COMM_CHANNEL_BT1]); i++){
	//		DEB("%c", *( (uint8_t *)m_mokibo_name[selected] + i) );
	//	}
	//	DEB("\n");
	//
    rv = sd_ble_gap_device_name_set(
		&sec_mode,
        (const uint8_t *)m_mokibo_name[selected],
         strlen(m_mokibo_name[M_COMM_CHANNEL_BT1])
	);
	//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(rv);
		
	#if defined(M_COMM_EVT_TRACE) || defined(M_COMM_CH_TRACE)
		DEB("sd_ble_gap_device_name_set('%s') \n", m_mokibo_name[selected]);
	#endif
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			gatt_init - Initialize GATT module
//-----------------------------------------------------------------------------------------------------------------------------------
// Function for initializing the GATT module.
static void gatt_init(void)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
    ret_code_t rv = nrf_ble_gatt_init(&m_gatt, NULL); 
	//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(rv);
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			dis_init - Init Device Information Service
//-----------------------------------------------------------------------------------------------------------------------------------
//	Function for initializing Device Information Service.
static void dis_init(void)
{	
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
    ret_code_t       rv;
    ble_dis_init_t   dis_init_obj;
    ble_dis_pnp_id_t pnp_id;

    pnp_id.vendor_id_source = PNP_ID_VENDOR_ID_SOURCE;
    pnp_id.vendor_id        = PNP_ID_VENDOR_ID;
    pnp_id.product_id       = PNP_ID_PRODUCT_ID;
    pnp_id.product_version  = PNP_ID_PRODUCT_VERSION;

    memset(&dis_init_obj, 0, sizeof(dis_init_obj));

    ble_srv_ascii_to_utf8(&dis_init_obj.manufact_name_str, MANUFACTURER_NAME);
    dis_init_obj.p_pnp_id = &pnp_id;

    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&dis_init_obj.dis_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&dis_init_obj.dis_attr_md.write_perm);
	//
    rv = ble_dis_init(&dis_init_obj); 
	//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(rv);
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			hids_init - Init HID Service
//-----------------------------------------------------------------------------------------------------------------------------------
// Function for initializing HID Service.
/* 
- Human Interface Device
    - Report Map (read) : 05 01 09 06 A1 01 85 01 05 07 19 E0 29 E7 15 00 25 01 75 01 95 08.....


    - Report (read, write, notify) : 00 00 00 00 00 00 00 00 [모디키1 키코드배열6]
        - Client Characteristic Configuration : 01 00
        - Report Reference : 01 01
    - Report (read, write, notify) : 00 00 00 [클릭 상하스크롤 좌우스크롤]
        - Client Characteristic Configuration
            - 01 00
        - Report Reference
            - 02 01
    - Report (read, write, notify) : 09 00 00 [마우스좌표]
        - Client Characteristic Configuration
            - 01 00
        - Report Reference
            - 03 01
    - Report (read, write, notify) : 00 00 00 [미디어키]
        - Client Characteristic Configuration : 01 01
        - Report Reference : 04 01
    - Report (read writeWoResp write) : 00 [캡스락 불들어오고 그런거]
        - Report Reference
            - 01 02
    

    
    - HID Information(read) : 01 01 00 03

    - Boot Keyboard Input Report (read , notify) : 0D A5 09 23 FD 73 6C A5
        - Client Characteristic Configuration : 01 00
    - Boot Keyboard Output Report (read, writeWoResp, write) : EE
   
    - HID Control Point (write,WoResp) : 없음
    - Protocol Mode (read, writeWoResp) : 01


*/
static void hids_init(void)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
    ret_code_t                 rv;
    ble_hids_init_t            hids_init_obj;
	//
    ble_hids_inp_rep_init_t    input_report_array[INPUT_REPORT_COUNT];
    ble_hids_inp_rep_init_t  * p_input_report;
	//
    ble_hids_outp_rep_init_t   output_report_array[OUTPUT_REPORT_COUNT];
    ble_hids_outp_rep_init_t * p_output_report;
	//
    uint8_t                    hid_info_flags;

    memset((void *)input_report_array, 0, sizeof(ble_hids_inp_rep_init_t)*INPUT_REPORT_COUNT);
    memset((void *)output_report_array, 0, sizeof(ble_hids_outp_rep_init_t)*OUTPUT_REPORT_COUNT);

	//-----------------------------------------------------------------------------------------------------------------------------------
    // Initialize HID Service - 'Keyboard' Input Report
    p_input_report                      = &input_report_array[INPUT_REP_KEYBOARD_INDEX];
    p_input_report->max_len             = INPUT_REP_KEYS_MAX_LEN;
    p_input_report->rep_ref.report_id   = INPUT_REP_REF_ID;
    p_input_report->rep_ref.report_type = BLE_HIDS_REP_TYPE_INPUT;

    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&p_input_report->security_mode.cccd_write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&p_input_report->security_mode.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&p_input_report->security_mode.write_perm);

	//-----------------------------------------------------------------------------------------------------------------------------------
    // Initialize HID Service - 'Buttons' Input Report
    p_input_report                      = &input_report_array[INPUT_REP_BUTTONS_INDEX];
    p_input_report->max_len             = INPUT_REP_BUTTONS_LEN;
    p_input_report->rep_ref.report_id   = INPUT_REP_REF_BUTTONS_ID;
    p_input_report->rep_ref.report_type = BLE_HIDS_REP_TYPE_INPUT;

    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&p_input_report->security_mode.cccd_write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&p_input_report->security_mode.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&p_input_report->security_mode.write_perm);

	//-----------------------------------------------------------------------------------------------------------------------------------
	#if (!MOKIBO_KEYBOARD_ONLY)

		//-----------------------------------------------------------------------------------------------------------------------------------
		// Initialize HID Service - 'Movement' Input Report
		p_input_report                      = &input_report_array[INPUT_REP_MOVEMENT_INDEX];
		p_input_report->max_len             = INPUT_REP_MOVEMENT_LEN;
		p_input_report->rep_ref.report_id   = INPUT_REP_REF_MOVEMENT_ID;
		p_input_report->rep_ref.report_type = BLE_HIDS_REP_TYPE_INPUT;

		BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&p_input_report->security_mode.cccd_write_perm);
		BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&p_input_report->security_mode.read_perm);
		BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&p_input_report->security_mode.write_perm);

		//-----------------------------------------------------------------------------------------------------------------------------------
		// Initialize HID Service - 'Consumer' Input Report
		p_input_report                      = &input_report_array[INPUT_REP_CONSUMER_INDEX];
		p_input_report->max_len             = INPUT_REP_CONSUMER_LEN;
		p_input_report->rep_ref.report_id   = INPUT_REP_REF_CONSUMER_ID;
		p_input_report->rep_ref.report_type = BLE_HIDS_REP_TYPE_INPUT;

		BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&p_input_report->security_mode.cccd_write_perm);
		BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&p_input_report->security_mode.read_perm);
		BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&p_input_report->security_mode.write_perm);
	#endif 

	//-----------------------------------------------------------------------------------------------------------------------------------
	// Initialize HID Service - '' Output Report
	p_output_report                      = &output_report_array[OUTPUT_REPORT_INDEX];
    p_output_report->max_len             = OUTPUT_REPORT_MAX_LEN;
    p_output_report->rep_ref.report_id   = OUTPUT_REP_REF_ID;
    p_output_report->rep_ref.report_type = BLE_HIDS_REP_TYPE_OUTPUT;

    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&p_output_report->security_mode.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&p_output_report->security_mode.write_perm);

	//-----------------------------------------------------------------------------------------------------------------------------------
    hid_info_flags = HID_INFO_FLAG_REMOTE_WAKE_MSK | HID_INFO_FLAG_NORMALLY_CONNECTABLE_MSK;

	//-----------------------------------------------------------------------------------------------------------------------------------
	//
    memset(&hids_init_obj, 0, sizeof(hids_init_obj));
	//
    hids_init_obj.evt_handler                    = on_hids_evt;
    hids_init_obj.error_handler                  = service_error_handler;
    hids_init_obj.is_kb                          = true;
    hids_init_obj.is_mouse                       = false;
    hids_init_obj.inp_rep_count                  = INPUT_REPORT_COUNT;
    hids_init_obj.p_inp_rep_array                = input_report_array;
    hids_init_obj.outp_rep_count                 = OUTPUT_REPORT_COUNT;
    hids_init_obj.p_outp_rep_array               = output_report_array;
    hids_init_obj.feature_rep_count              = 0;
    hids_init_obj.p_feature_rep_array            = NULL;
    hids_init_obj.rep_map.data_len               = sizeof(m_report_map_data);
    hids_init_obj.rep_map.p_data                 = m_report_map_data;
    hids_init_obj.hid_information.bcd_hid        = BASE_USB_HID_SPEC_VERSION;
    hids_init_obj.hid_information.b_country_code = 0;
    hids_init_obj.hid_information.flags          = hid_info_flags;
    hids_init_obj.included_services_count        = 0;
    hids_init_obj.p_included_services_array      = NULL;

    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&hids_init_obj.rep_map.security_mode.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&hids_init_obj.rep_map.security_mode.write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&hids_init_obj.hid_information.security_mode.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&hids_init_obj.hid_information.security_mode.write_perm);

    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&hids_init_obj.security_mode_boot_kb_inp_rep.cccd_write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&hids_init_obj.security_mode_boot_kb_inp_rep.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&hids_init_obj.security_mode_boot_kb_inp_rep.write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&hids_init_obj.security_mode_boot_kb_outp_rep.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&hids_init_obj.security_mode_boot_kb_outp_rep.write_perm);

    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&hids_init_obj.security_mode_protocol.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&hids_init_obj.security_mode_protocol.write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&hids_init_obj.security_mode_ctrl_point.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&hids_init_obj.security_mode_ctrl_point.write_perm);

	//-----------------------------------------------------------------------------------------------------------------------------------
    rv = ble_hids_init(&m_hids, &hids_init_obj);
    //deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(rv);
}

//===================================================================================================================================
//			MOKIBO_DFU_ENABLED
//===================================================================================================================================

#if	(MOKIBO_DFU_ENABLED)

/**@brief Handler for shutdown preparation.
 *
 * @details During shutdown procedures, this function will be called at a 1 second interval
 *          untill the function returns true. When the function returns true, it means that the
 *          app is ready to reset to DFU mode.
 *
 * @param[in]   event   Power manager event.
 *
 * @retval  True if shutdown is allowed by this power manager handler, otherwise false.
 */

//-----------------------------------------------------------------------------------------------------------------------------------
//			app_shutdown_handler
//-----------------------------------------------------------------------------------------------------------------------------------
static bool app_shutdown_handler(nrf_pwr_mgmt_evt_t event)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
    switch (event)
    {
		case NRF_PWR_MGMT_EVT_PREPARE_SYSOFF:
			// added by jaehong, 2019/07/02
            DEB("app_shutdown_handler: NRF_PWR_MGMT_EVT_PREPARE_SYSOFF: Power management wants to poweroff.\n");
			SaveInfoToFlash(m_config.config.selected_ch);
			//configure the pin like this to wake up from sleep:
			//nrf_gpio_cfg_sense_input(PIN NUMBER, NRF_GPIO_PIN_PULLUP, NRF_GPIO_PIN_SENSE_LOW);
			break;
	
        case NRF_PWR_MGMT_EVT_PREPARE_DFU:
            DEB("app_shutdown_handler: NRF_PWR_MGMT_EVT_PREPARE_DFU: Power management wants to reset to DFU mode.\n");
            // YOUR_JOB: Get ready to reset into DFU mode
            //
            // If you aren't finished with any ongoing tasks, return "false" to
            // signal to the system that reset is impossible at this stage.
            //
            // Here is an example using a variable to delay resetting the device.
            //
            // if (!m_ready_for_reset)
            // {
            //      return false;
            // }
            // else
            //{
            //
            //    // Device ready to enter
            //    uint32_t err_code;
            //    err_code = sd_softdevice_disable();
            //    APP_ERROR_CHECK(err_code);
            //    err_code = app_timer_stop_all();
            //    APP_ERROR_CHECK(err_code);
            //}
            break;

        default:
            DEB("app_shutdown_handler: default - \n");
            // YOUR_JOB: Implement any of the other events available from the power management module:
            //      -NRF_PWR_MGMT_EVT_PREPARE_SYSOFF
            //      -NRF_PWR_MGMT_EVT_PREPARE_WAKEUP
            //      -NRF_PWR_MGMT_EVT_PREPARE_RESET
            return true;
    }

    NRF_LOG_INFO("Power management allowed to reset to DFU mode.");
    return true;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			NRF_PWR_MGMT_HANDLER_REGISTER
//-----------------------------------------------------------------------------------------------------------------------------------
//lint -esym(528, m_app_shutdown_handler)
/**@brief Register application shutdown handler with priority 0.
 */
NRF_PWR_MGMT_HANDLER_REGISTER(app_shutdown_handler, 0);

//-----------------------------------------------------------------------------------------------------------------------------------
//			ble_dfu_evt_handler
//-----------------------------------------------------------------------------------------------------------------------------------
/**@brief Function for handling dfu events from the Buttonless Secure DFU service
 *
 * @param[in]   event   Event from the Buttonless Secure DFU service.
 */
static void ble_dfu_evt_handler(ble_dfu_buttonless_evt_type_t event)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
    switch (event)
    {
        case BLE_DFU_EVT_BOOTLOADER_ENTER_PREPARE:
            DEB("ble_dfu_evt_handler: Device is preparing to enter bootloader mode.\n");
            // YOUR_JOB: Disconnect all bonded devices that currently are connected.
            //           This is required to receive a service changed indication
            //           on bootup after a successful (or aborted) Device Firmware Update.
            break;

        case BLE_DFU_EVT_BOOTLOADER_ENTER:
            // YOUR_JOB: Write app-specific unwritten data to FLASH, control finalization of this
            //           by delaying reset by reporting false in app_shutdown_handler
            DEB("ble_dfu_evt_handler: Device will enter bootloader mode.\n");
            break;

        case BLE_DFU_EVT_BOOTLOADER_ENTER_FAILED:
            DEB("ble_dfu_evt_handler: Request to enter bootloader mode failed asynchroneously.\n");
            // YOUR_JOB: Take corrective measures to resolve the issue
            //           like calling APP_ERROR_CHECK to reset the device.
            break;

        case BLE_DFU_EVT_RESPONSE_SEND_ERROR:
            DEB("ble_dfu_evt_handler: Request to send a response to client failed.\n");
            // YOUR_JOB: Take corrective measures to resolve the issue
            //           like calling APP_ERROR_CHECK to reset the device.
            //APP_ERROR_CHECK(false);
            break;

        default:
            DEB("ble_dfu_evt_handler: Unknown event from ble_dfu_buttonless.\n");
            break;
    }
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			dfu_init
//-----------------------------------------------------------------------------------------------------------------------------------
void dfu_init(void)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
    uint32_t err_code;

	ble_dfu_buttonless_init_t dfus_init =
    {
        .evt_handler = ble_dfu_evt_handler
    };

    // Initialize the async SVCI interface to bootloader.
    err_code = ble_dfu_buttonless_async_svci_init();
	if(err_code != NRF_SUCCESS)
	{
		//DEB("dfu_init: ble_dfu_buttonless_async_svci_init: err_code=%d \n", err_code);
	}
	
    //APP_ERROR_CHECK(err_code);
       
    err_code = ble_dfu_buttonless_init(&dfus_init);
	if(err_code != NRF_SUCCESS)
	{
		//DEB("ble_dfu_buttonless_init: err_code=%d \n", err_code);
	}
    //APP_ERROR_CHECK(err_code);

}

#endif //MOKIBO_DFU_ENABLED =========================================================================





//-----------------------------------------------------------------------------------------------------------------------------------
//			services_init
//-----------------------------------------------------------------------------------------------------------------------------------
// Function for initializing services that will be used by the application.
static void services_init(void)
{	
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
    dis_init(); 	// Device Information Service.
    hids_init();	// HID Service
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			conn_params_error_handler
//-----------------------------------------------------------------------------------------------------------------------------------
// Function for handling a Connection Parameters error.
// @param[in]   nrf_error   Error code containing information about what went wrong.
static void conn_params_error_handler(uint32_t nrf_error)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
    //deleted by jaehong, 2019/06/28 APP_ERROR_HANDLER(nrf_error);
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			conn_params_init
//-----------------------------------------------------------------------------------------------------------------------------------
// Function for initializing the Connection Parameters module.
static void conn_params_init(void)
{
	________________________________LOG_mComm_FuncHeads(0,"");
    ret_code_t             rv;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));
	//
    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = NULL;
    cp_init.error_handler                  = conn_params_error_handler;
	//
    rv = ble_conn_params_init(&cp_init); 
	//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(rv);
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			on_hid_rep_char_write
//-----------------------------------------------------------------------------------------------------------------------------------
static void on_hid_rep_char_write(ble_hids_evt_t * p_evt)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");

	//	switch(p_evt->evt_type)
	//	{
	//		case BLE_HIDS_EVT_HOST_SUSP:			DEB("BLE_HIDS_EVT_HOST_SUSP"); break;
	//		case BLE_HIDS_EVT_HOST_EXIT_SUSP:		DEB("BLE_HIDS_EVT_HOST_EXIT_SUSP"); break;
	//		case BLE_HIDS_EVT_NOTIF_ENABLED:		DEB("BLE_HIDS_EVT_NOTIF_ENABLED"); break;
	//		case BLE_HIDS_EVT_NOTIF_DISABLED:		DEB("BLE_HIDS_EVT_NOTIF_DISABLED"); break;
	//		case BLE_HIDS_EVT_REP_CHAR_WRITE:		DEB("BLE_HIDS_EVT_REP_CHAR_WRITE"); break;
	//		case BLE_HIDS_EVT_BOOT_MODE_ENTERED:	DEB("BLE_HIDS_EVT_BOOT_MODE_ENTERED"); break;
	//		case BLE_HIDS_EVT_REPORT_MODE_ENTERED:	DEB("BLE_HIDS_EVT_REPORT_MODE_ENTERED"); break;
	//		case BLE_HIDS_EVT_REPORT_READ:			DEB("BLE_HIDS_EVT_REPORT_READ"); break;
	//		default:								DEB("UNKNOWN"); break;
	//	}
	//	DEB("\n");
	
    if (p_evt->params.char_write.char_id.rep_type == BLE_HIDS_REP_TYPE_OUTPUT)
    {
        ret_code_t rv;
        uint8_t  report_val;
        uint8_t  report_index = p_evt->params.char_write.char_id.rep_index;

        if (report_index == OUTPUT_REPORT_INDEX)
        {
            // This code assumes that the outptu report is one byte long. Hence the following
            // static assert is made.
            STATIC_ASSERT(OUTPUT_REPORT_MAX_LEN == 1);

            rv = ble_hids_outp_rep_get(
				&m_hids, report_index, OUTPUT_REPORT_MAX_LEN, 0, &report_val
			);
			//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(rv);

            //if (!m_caps_on && ((report_val & OUTPUT_REPORT_BIT_MASK_CAPS_LOCK) != 0))
            if( (report_val & OUTPUT_REPORT_BIT_MASK_CAPS_LOCK) != 0 )
            {
                // Caps Lock is turned On.
                SYSTEM_MSG("Caps Lock - ON \n");
                m_caps_on = true;
            }
            //else if (m_caps_on && ((report_val & OUTPUT_REPORT_BIT_MASK_CAPS_LOCK) == 0))
            else
            {
                // Caps Lock is turned Off .
                SYSTEM_MSG("Caps Lock - OFF \n");
                m_caps_on = false;
            }
			
        }
    }
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			sleep_mode_enter
//-----------------------------------------------------------------------------------------------------------------------------------
#if 0
/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
static void sleep_mode_enter(void)
{
    ret_code_t err_code;

    //err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    //APP_ERROR_CHECK(err_code);

    // Prepare wakeup buttons.
    //err_code = bsp_btn_ble_sleep_mode_prepare();
    //APP_ERROR_CHECK(err_code);

    // Go to system-off mode (this function will not return; wakeup will cause a reset).
    err_code = sd_power_system_off();

    //deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(err_code);
}
#endif

//-----------------------------------------------------------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------------------------------------------------------
// Function for handling HID events.
// This function will be called for all HID events which are passed to the application.
static void on_hids_evt(ble_hids_t * p_hids, ble_hids_evt_t * p_evt)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	
	________________________________LOG_mComm_HidTmp(2,"p_evt->evt_type:%d",p_evt->evt_type);
    switch (p_evt->evt_type)
    {
        case BLE_HIDS_EVT_BOOT_MODE_ENTERED:
			DEB("on_hids_evt: BLE_HIDS_EVT_BOOT_MODE_ENTERED\n");
            break;

        case BLE_HIDS_EVT_REPORT_MODE_ENTERED:
			DEB("on_hids_evt: BLE_HIDS_EVT_REPORT_MODE_ENTERED\n");
            break;

        case BLE_HIDS_EVT_REP_CHAR_WRITE:
			DEB("on_hids_evt: BLE_HIDS_EVT_REP_CHAR_WRITE\n");
            on_hid_rep_char_write(p_evt);
            break;

        case BLE_HIDS_EVT_NOTIF_ENABLED:
			DEB("on_hids_evt: BLE_HIDS_EVT_NOTIF_ENABLED\n");
            break;

        default:
			
			//DEB("on_hids_evt: UNKNOWN \n");
            // No implementation needed.
            break;
    }
}


#if defined(M_COMM_EVT_TRACE) //===================================================================================================
//-----------------------------------------------------------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------------------------------------------------------
// see: BLE_SVC_BASE
static const char *GetStr_BLE_SVC(uint16_t id){return "One of BLE_SVC";}
static const char *GetStr_BLE_GAP_SVC(uint16_t id){return "One of BLE_GAP_SVC";}
static const char *GetStr_BLE_GATTC_SVC(uint16_t id){return "One of BLE_GATTC_SVC";}
static const char *GetStr_BLE_GATTS_SVC(uint16_t id){return "One of BLE_GATTS_SVC";}
static const char *GetStr_BLE_L2CAP_SVC(uint16_t id){return "One of BLE_L2CAP_SVC";}
//-----------------------------------------------------------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------------------------------------------------------
// see: BLE_OPT_BASE
static const char *GetStr_BLE_OPT(uint16_t id){return "One of BLE_OPT";}
static const char *GetStr_BLE_GAP_OPT(uint16_t id){return "One of BLE_GAP_OPT";}
static const char *GetStr_BLE_GATT_OPT(uint16_t id){return "One of BLE_GATT_OPT";}
static const char *GetStr_BLE_GATTC_OPT(uint16_t id){return "One of BLE_GATTC_OPT";}
static const char *GetStr_BLE_GATTS_OPT(uint16_t id){return "One of BLE_GATTS_OPT";}
static const char *GetStr_BLE_L2CAP_OPT(uint16_t id){return "One of BLE_L2CAP_OPT";}
//-----------------------------------------------------------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------------------------------------------------------
// see: BLE_CFG_BASE
static const char *GetStr_BLE_CFG(uint16_t id){return "One of BLE_CFG";}
static const char *GetStr_BLE_CONN_CFG(uint16_t id){return "One of BLE_CONN_CFG";}
static const char *GetStr_BLE_GAP_CFG(uint16_t id){return "One of BLE_GAP_CFG";}
static const char *GetStr_BLE_GATT_CFG(uint16_t id){return "One of BLE_GATT_CFG";}
static const char *GetStr_BLE_GATTC_CFG(uint16_t id){return "One of BLE_GATTC_CFG";}
static const char *GetStr_BLE_GATTS_CFG(uint16_t id){return "One of BLE_GATTS_CFG";}
static const char *GetStr_BLE_L2CAP_CFG(uint16_t id){return "One of BLE_L2CAP_CFG";}
//-----------------------------------------------------------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------------------------------------------------------
// see: BLE_EVT_BASE
static const char *GetStr_BLE_EVT(uint16_t id);
static const char *GetStr_BLE_GAP_EVT(uint16_t id);
static const char *GetStr_BLE_GATTC_EVT(uint16_t id);
static const char *GetStr_BLE_GATTS_EVT(uint16_t id);
static const char *GetStr_BLE_L2CAP_EVT(uint16_t id);
//-----------------------------------------------------------------------------------------------------------------------------------
static const char *GetStr_BLE_EVT(uint16_t id)
{
	switch(id)
	{
		case BLE_EVT_USER_MEM_REQUEST: return "BLE_EVT_USER_MEM_REQUEST - User Memory request.";
		case BLE_EVT_USER_MEM_RELEASE: return "BLE_EVT_USER_MEM_RELEASE - User Memory release.";
		
		case BLE_GAP_EVT_CONNECTED: return "BLE_GAP_EVT_CONNECTED - Connection established.";
		case BLE_GAP_EVT_DISCONNECTED: return "BLE_GAP_EVT_DISCONNECTED - Disconnected from peer.";
		case BLE_GAP_EVT_CONN_PARAM_UPDATE: return "BLE_GAP_EVT_CONN_PARAM_UPDATE - Connection Parameters updated.";
		case BLE_GAP_EVT_SEC_PARAMS_REQUEST: return "BLE_GAP_EVT_SEC_PARAMS_REQUEST - Request to provide security parameters.";
		case BLE_GAP_EVT_SEC_INFO_REQUEST: return "BLE_GAP_EVT_SEC_INFO_REQUEST - Request to provide security information.";
		case BLE_GAP_EVT_PASSKEY_DISPLAY: return "BLE_GAP_EVT_PASSKEY_DISPLAY - Request to display a passkey to the user.";
		case BLE_GAP_EVT_KEY_PRESSED: return "BLE_GAP_EVT_KEY_PRESSED - Notification of a keypress on the remote device.";
		case BLE_GAP_EVT_AUTH_KEY_REQUEST: return "BLE_GAP_EVT_AUTH_KEY_REQUEST - Request to provide an authentication key.";
		case BLE_GAP_EVT_LESC_DHKEY_REQUEST: return "BLE_GAP_EVT_LESC_DHKEY_REQUEST - Request to calculate an LE Secure Connections DHKey.";
		case BLE_GAP_EVT_AUTH_STATUS: return "BLE_GAP_EVT_AUTH_STATUS - Authentication procedure completed with status.";
		case BLE_GAP_EVT_CONN_SEC_UPDATE: return "BLE_GAP_EVT_CONN_SEC_UPDATE - Connection security updated.";
		case BLE_GAP_EVT_TIMEOUT: return "BLE_GAP_EVT_TIMEOUT - Timeout expired.";
		case BLE_GAP_EVT_RSSI_CHANGED: return "BLE_GAP_EVT_RSSI_CHANGED - RSSI report. ";
		case BLE_GAP_EVT_ADV_REPORT: return "BLE_GAP_EVT_ADV_REPORT - Advertising report.";
		case BLE_GAP_EVT_SEC_REQUEST: return "BLE_GAP_EVT_SEC_REQUEST - Security Request.";
		case BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST: return "BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST - Connection Parameter Update Request.";
		case BLE_GAP_EVT_SCAN_REQ_REPORT: return "BLE_GAP_EVT_SCAN_REQ_REPORT - Scan request report.";
		case BLE_GAP_EVT_PHY_UPDATE_REQUEST: return "BLE_GAP_EVT_PHY_UPDATE_REQUEST - PHY Update Request.";
		case BLE_GAP_EVT_PHY_UPDATE: return "BLE_GAP_EVT_PHY_UPDATE - PHY Update Procedure is complete.";
		case BLE_GAP_EVT_DATA_LENGTH_UPDATE_REQUEST: return "BLE_GAP_EVT_DATA_LENGTH_UPDATE_REQUEST - ata Length Update Request.";
		case BLE_GAP_EVT_DATA_LENGTH_UPDATE: return "BLE_GAP_EVT_DATA_LENGTH_UPDATE - LL Data Channel PDU payload length updated.";
		default: return "UNKNOWN_BLE_EVT";
	}
}
//-----------------------------------------------------------------------------------------------------------------------------------
static const char *GetStr_BLE_GAP_EVT(uint16_t id)
{
	switch(id)
	{
		case BLE_GAP_EVT_CONNECTED: return "BLE_GAP_EVT_CONNECTED - Connection established.";
		case BLE_GAP_EVT_DISCONNECTED: return "BLE_GAP_EVT_DISCONNECTED - Disconnected from peer.";
		case BLE_GAP_EVT_CONN_PARAM_UPDATE: return "BLE_GAP_EVT_CONN_PARAM_UPDATE - Connection Parameters updated.";
		case BLE_GAP_EVT_SEC_PARAMS_REQUEST: return "BLE_GAP_EVT_SEC_PARAMS_REQUEST - Request to provide security parameters.";
		case BLE_GAP_EVT_SEC_INFO_REQUEST: return "BLE_GAP_EVT_SEC_INFO_REQUEST - Request to provide security information.";
		case BLE_GAP_EVT_PASSKEY_DISPLAY: return "BLE_GAP_EVT_PASSKEY_DISPLAY - Request to display a passkey to the user.";
		case BLE_GAP_EVT_KEY_PRESSED: return "BLE_GAP_EVT_KEY_PRESSED - Notification of a keypress on the remote device.";
		case BLE_GAP_EVT_AUTH_KEY_REQUEST: return "BLE_GAP_EVT_AUTH_KEY_REQUEST - Request to provide an authentication key.";
		case BLE_GAP_EVT_LESC_DHKEY_REQUEST: return "BLE_GAP_EVT_LESC_DHKEY_REQUEST - Request to calculate an LE Secure Connections DHKey.";
		case BLE_GAP_EVT_AUTH_STATUS: return "BLE_GAP_EVT_AUTH_STATUS - Authentication procedure completed with status.";
		case BLE_GAP_EVT_CONN_SEC_UPDATE: return "BLE_GAP_EVT_CONN_SEC_UPDATE - Connection security updated.";
		case BLE_GAP_EVT_TIMEOUT: return "BLE_GAP_EVT_TIMEOUT - Timeout expired.";
		case BLE_GAP_EVT_RSSI_CHANGED: return "BLE_GAP_EVT_RSSI_CHANGED - RSSI report.";
		case BLE_GAP_EVT_ADV_REPORT: return "BLE_GAP_EVT_ADV_REPORT - Advertising report.";
		case BLE_GAP_EVT_SEC_REQUEST: return "BLE_GAP_EVT_SEC_REQUEST - Security Request.";
		case BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST: return "BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST - Connection Parameter Update Request.";
		case BLE_GAP_EVT_SCAN_REQ_REPORT: return "BLE_GAP_EVT_SCAN_REQ_REPORT - Scan request report.";
		case BLE_GAP_EVT_PHY_UPDATE_REQUEST: return "BLE_GAP_EVT_PHY_UPDATE_REQUEST - PHY Update Request.";
		case BLE_GAP_EVT_PHY_UPDATE: return "BLE_GAP_EVT_PHY_UPDATE - PHY Update Procedure is complete.";
		case BLE_GAP_EVT_DATA_LENGTH_UPDATE_REQUEST: return "BLE_GAP_EVT_DATA_LENGTH_UPDATE_REQUEST - Data Length Update Request.";
		case BLE_GAP_EVT_DATA_LENGTH_UPDATE: return "BLE_GAP_EVT_DATA_LENGTH_UPDATE - LL Data Channel PDU payload length updated.";
		default: return "UNKNOWN_BLE_GAP_EVT";
	}
}
//-----------------------------------------------------------------------------------------------------------------------------------
static const char *GetStr_BLE_GATTC_EVT(uint16_t id)
{
	switch(id)
	{
		case BLE_GATTC_EVT_PRIM_SRVC_DISC_RSP: return "BLE_GATTC_EVT_PRIM_SRVC_DISC_RSP - Primary Service Discovery Response event.";
		case BLE_GATTC_EVT_REL_DISC_RSP: return "BLE_GATTC_EVT_REL_DISC_RSP - Relationship Discovery Response event.";
		case BLE_GATTC_EVT_CHAR_DISC_RSP: return "BLE_GATTC_EVT_CHAR_DISC_RSP - Characteristic Discovery Response event.";
		case BLE_GATTC_EVT_DESC_DISC_RSP: return "BLE_GATTC_EVT_DESC_DISC_RSP - Descriptor Discovery Response event.";
		case BLE_GATTC_EVT_ATTR_INFO_DISC_RSP: return "BLE_GATTC_EVT_ATTR_INFO_DISC_RSP - Attribute Information Response event.";
		case BLE_GATTC_EVT_CHAR_VAL_BY_UUID_READ_RSP: return "BLE_GATTC_EVT_CHAR_VAL_BY_UUID_READ_RSP - Read By UUID Response event.";
		case BLE_GATTC_EVT_READ_RSP: return "BLE_GATTC_EVT_READ_RSP - Read Response event.";
		case BLE_GATTC_EVT_CHAR_VALS_READ_RSP: return "BLE_GATTC_EVT_CHAR_VALS_READ_RSP - Read multiple Response event.";
		case BLE_GATTC_EVT_WRITE_RSP: return "BLE_GATTC_EVT_WRITE_RSP - Write Response event.";
		case BLE_GATTC_EVT_HVX: return "BLE_GATTC_EVT_HVX - Handle Value Notification or Indication event.";
		case BLE_GATTC_EVT_EXCHANGE_MTU_RSP: return "BLE_GATTC_EVT_EXCHANGE_MTU_RSP - Exchange MTU Response event.";
		case BLE_GATTC_EVT_TIMEOUT: return "BLE_GATTC_EVT_TIMEOUT - Timeout event.";
		case BLE_GATTC_EVT_WRITE_CMD_TX_COMPLETE: return "BLE_GATTC_EVT_WRITE_CMD_TX_COMPLETE - Write without Response transmission complete.";
		default: return "UNKNOWN_BLE_GATTC_EVT";
	}
}
//-----------------------------------------------------------------------------------------------------------------------------------
static const char *GetStr_BLE_GATTS_EVT(uint16_t id)
{
	switch(id)
	{
		case BLE_GATTS_EVT_WRITE: return "BLE_GATTS_EVT_WRITE - Write operation performed.";
		case BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST: return "BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST - Read/Write Authorization request.";
		case BLE_GATTS_EVT_SYS_ATTR_MISSING: return "BLE_GATTS_EVT_SYS_ATTR_MISSING - A persistent system attribute access is pending.";
		case BLE_GATTS_EVT_HVC: return "BLE_GATTS_EVT_HVC - Handle Value Confirmation.";
		case BLE_GATTS_EVT_SC_CONFIRM: return "BLE_GATTS_EVT_SC_CONFIRM - Service Changed Confirmation.";
		case BLE_GATTS_EVT_EXCHANGE_MTU_REQUEST: return "BLE_GATTS_EVT_EXCHANGE_MTU_REQUEST - Exchange MTU Request.";
		case BLE_GATTS_EVT_TIMEOUT: return "BLE_GATTS_EVT_TIMEOUT - Peer failed to respond to an ATT request in time.";
		case BLE_GATTS_EVT_HVN_TX_COMPLETE: return "BLE_GATTS_EVT_HVN_TX_COMPLETE - Handle Value Notification transmission complete.";
		default: return "UNKNOWN_BLE_GATTS_EVT";
	}
}
//-----------------------------------------------------------------------------------------------------------------------------------
static const char *GetStr_BLE_L2CAP_EVT(uint16_t id)
{
	switch(id)
	{
		case BLE_L2CAP_EVT_CH_SETUP_REQUEST: return "BLE_L2CAP_EVT_CH_SETUP_REQUEST - L2CAP Channel Setup Request event.";
		case BLE_L2CAP_EVT_CH_SETUP_REFUSED: return "BLE_L2CAP_EVT_CH_SETUP_REFUSED - L2CAP Channel Setup Refused event.";
		case BLE_L2CAP_EVT_CH_SETUP: return "BLE_L2CAP_EVT_CH_SETUP - L2CAP Channel Setup Completed event.";
		case BLE_L2CAP_EVT_CH_RELEASED: return "BLE_L2CAP_EVT_CH_RELEASED - L2CAP Channel Released event.";
		case BLE_L2CAP_EVT_CH_SDU_BUF_RELEASED: return "BLE_L2CAP_EVT_CH_SDU_BUF_RELEASED - L2CAP Channel SDU data buffer released event.";
		case BLE_L2CAP_EVT_CH_CREDIT: return "BLE_L2CAP_EVT_CH_CREDIT - L2CAP Channel Credit received.";
		case BLE_L2CAP_EVT_CH_RX: return "BLE_L2CAP_EVT_CH_RX - L2CAP Channel SDU received.";
		case BLE_L2CAP_EVT_CH_TX: return "BLE_L2CAP_EVT_CH_TX - L2CAP Channel SDU transmitted.";
		default: return "UNKNOWN_BLE_L2CAP_EVT";
	}
}


//-----------------------------------------------------------------------------------------------------------------------------------
//			GetStrBLE_........
//-----------------------------------------------------------------------------------------------------------------------------------
static const char *GetStrBLE_SVC(uint16_t id)
{
	// SVC
	if(BLE_SVC_BASE       <= id && id <= BLE_SVC_LAST		) return GetStr_BLE_SVC(id);
	if(BLE_GAP_SVC_BASE   <= id && id <= BLE_GAP_SVC_LAST	) return GetStr_BLE_GAP_SVC(id);
	if(BLE_GATTC_SVC_BASE <= id && id <= BLE_GATTC_SVC_LAST	) return GetStr_BLE_GATTC_SVC(id);
	if(BLE_GATTS_SVC_BASE <= id && id <= BLE_GATTS_SVC_LAST	) return GetStr_BLE_GATTS_SVC(id);
	if(BLE_L2CAP_SVC_BASE <= id && id <= BLE_L2CAP_SVC_LAST	) return GetStr_BLE_L2CAP_SVC(id);
	return "UNKNOWN_BLE_SVC";
}

static const char *GetStrBLE_EVT(uint16_t id)
{
	// EVT
	if(BLE_EVT_INVALID == id ) return "BLE_EVT_INVALID";
	if(BLE_EVT_BASE		  <= id && id <= BLE_EVT_LAST		) return GetStr_BLE_EVT(id);
	if(BLE_GAP_EVT_BASE   <= id && id <= BLE_GAP_EVT_LAST	) return GetStr_BLE_GAP_EVT(id);
	if(BLE_GATTC_EVT_BASE <= id && id <= BLE_GATTC_EVT_LAST	) return GetStr_BLE_GATTC_EVT(id);
	if(BLE_GATTS_EVT_BASE <= id && id <= BLE_GATTS_EVT_LAST	) return GetStr_BLE_GATTS_EVT(id);
	if(BLE_L2CAP_EVT_BASE <= id && id <= BLE_L2CAP_EVT_LAST	) return GetStr_BLE_L2CAP_EVT(id);
	return "UNKNOWN_BLE_EVT";
}

static const char *GetStrBLE_OPT(uint16_t id)
{
	// OPT
	if(BLE_OPT_INVALID == id) return "BLE_OPT_INVALID";
	if(BLE_OPT_BASE 	  <= id && id <= BLE_OPT_LAST		) return GetStr_BLE_OPT(id);
	if(BLE_GAP_OPT_BASE   <= id && id <= BLE_GAP_OPT_LAST	) return GetStr_BLE_GAP_OPT(id);
	if(BLE_GATT_OPT_BASE  <= id && id <= BLE_GATT_OPT_LAST	) return GetStr_BLE_GATT_OPT(id);
	if(BLE_GATTC_OPT_BASE <= id && id <= BLE_GATTC_OPT_LAST	) return GetStr_BLE_GATTC_OPT(id);
	if(BLE_GATTS_OPT_BASE <= id && id <= BLE_GATTS_OPT_LAST	) return GetStr_BLE_GATTS_OPT(id);
	if(BLE_L2CAP_OPT_BASE <= id && id <= BLE_L2CAP_OPT_LAST	) return GetStr_BLE_L2CAP_OPT(id);
	return "UNKNOWN_BLE_OPT";
}

static const char *GetStrBLE_CFG(uint16_t id)
{
	// CFG
	if(BLE_CFG_INVALID == id ) return "BLE_CFG_INVALID";
	if(BLE_CFG_BASE 	  <= id && id <= BLE_CFG_LAST		) return GetStr_BLE_CFG(id);
	if(BLE_CONN_CFG_BASE  <= id && id <= BLE_CONN_CFG_LAST	) return GetStr_BLE_CONN_CFG(id);
	if(BLE_GAP_CFG_BASE   <= id && id <= BLE_GAP_CFG_LAST	) return GetStr_BLE_GAP_CFG(id);
	if(BLE_GATT_CFG_BASE  <= id && id <= BLE_GATT_CFG_LAST	) return GetStr_BLE_GATT_CFG(id);
	if(BLE_GATTC_CFG_BASE <= id && id <= BLE_GATTC_CFG_LAST	) return GetStr_BLE_GATTC_CFG(id);
	if(BLE_GATTS_CFG_BASE <= id && id <= BLE_GATTS_CFG_LAST	) return GetStr_BLE_GATTS_CFG(id);
	if(BLE_L2CAP_CFG_BASE <= id && id <= BLE_L2CAP_CFG_LAST	) return GetStr_BLE_L2CAP_CFG(id);
	return "UNKNOWN_BLE_CFG";
}
#endif//==========================================================================================================================

//-----------------------------------------------uu------------------------------------------------------------------------------------
//			ble_evt_handler
//----------------------------------------------------------------------------------------------------------------------------------
//	Function for handling BLE events.
static void ble_evt_handler(	
	ble_evt_t const * p_ble_evt,	// Bluetooth stack event.
	void * p_context				// Unused.
)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
    ret_code_t rv;
	ble_gap_addr_t peer_addr;
	pm_peer_id_t   peer_id;
	//-----------------------------------------------------------------------------------------------------------------------------------
	#if defined(M_COMM_EVT_TRACE)
			DEB("___BLE___:ID=0x%x, Len=0x%x, %s\n",
			p_ble_evt->header.evt_id,
			p_ble_evt->header.evt_len,
			GetStrBLE_EVT(p_ble_evt->header.evt_id)
		);
			// ...no meaning...
			GetStrBLE_SVC(p_ble_evt->header.evt_id);
			GetStrBLE_OPT(p_ble_evt->header.evt_id);
			GetStrBLE_CFG(p_ble_evt->header.evt_id);
	#endif

	//-----------------------------------------------------------------------------------------------------------------------------------
	// BT가 연결된 상태에서, power on reset했을 때, 바로 연결되지 못하는 경우, 재차 reset함.
	// --> BLE_GATTS_EVT_SYS_ATTR_MISSING - A persistent system attribute access is pending. <---
#if 0
	m_ble_evt_count++;
	if(p_ble_evt->header.evt_id == BLE_GATTS_EVT_SYS_ATTR_MISSING)
	{
		m_mokibo_WDT_reset(1);			// Reset by Watchdog
	}
	if(m_ble_evt_count > 200)
	{
		// too much
		m_ble_evt_count=0;
		m_mokibo_soft_reset(0xA3A3);
	}
#endif
	
	//-----------------------------------------------------------------------------------------------------------------------------------
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
    switch (p_ble_evt->header.evt_id)
    {

		//-----------------------------------------------------------------------------------------------------------------------------------
        case BLE_GAP_EVT_CONNECTED : // Connection established.
			DEB("ble_evt_handler: BLE_GAP_EVT_CONNECTED \n");

			m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
			peer_addr = p_ble_evt->evt.gap_evt.params.connected.peer_addr;
			pm_peer_id_get(m_conn_handle, &peer_id);

			DEB("ble_evt_handler: m_conn_handle= 0x%x\n",m_conn_handle);
			DEB("ble_evt_handler: peer_id= 0x%x\n",peer_id);
			DEB("ble_evt_handler: peer_addr= 0x%x\n",peer_addr);

			DEB("ble_evt_handler: update_central(peer_addr=0x%x)\n",peer_addr);
			update_central(&peer_addr);
#if 0	
			switch(m_mokibo_get_status())
			{
				case M_MOKIBO_KEYBOARD_STATUS :
					// 연결이 되는 순간 키보드 모드이면
					// 현재 채널 Color + Dimming out으로 표시한다
					//DEB("ble_evt_handler: m_mokibo_set_touch_color() \n");
					m_mokibo_set_touch_color();

					//DEB("ble_evt_handler: m_led_set_status(M_LED_DIMMING_OUT) \n");
					m_led_set_status(M_LED_DIMMING_OUT);
					break;
				
				case M_MOKIBO_TOUCH_STATUS: 
					//DEB("ble_evt_handler: m_mokibo_set_touch_color()\n");
					m_mokibo_set_touch_color();

					//DEB("ble_evt_handler: m_led_set_status(M_LED_ON) \n");
					m_led_set_status(M_LED_ON);
					break;
			}
#endif
			#if MOKIBO_NFC_ENABLED
			if(m_nfc_state_start() && !m_nfc_state_run())
			{
				#if defined(M_COMM_CH_TRACE)
				DEB("ble_evt_handler: m_nfc_stop() \n");
				#endif
				m_nfc_stop();
			}
			#endif

			DEB("ble_evt_handler: m_comm_adv_set_status(M_COMM_ADV_STATUS_CONNECTING)\n");
			m_comm_adv_set_status(M_COMM_ADV_STATUS_CONNECTING);
			//
			
//			//-----------------------------------------------------------------------------------------------------------------------------------
//			// Save any change in BLE states
//			DEB("update_central: SaveInfoToFlash() .... maybe saved far later...\n");
///			if( !is_DONGLE_channel(m_config.config.selected_ch) )
//			{	// Dongle채널은 기억하지 않는다
//				SaveInfoToFlash(m_config.config.selected_ch);
//			}
			
			//DEB("update_central: SaveInfoToFlash() .... maybe saved far later...\n");
			if( is_BT_channel(m_config.config.selected_ch) )
			{	// BT channel 만 기억한다
				SaveInfoToFlash(m_config.config.selected_ch);
			}
            break;

		//-----------------------------------------------------------------------------------------------------------------------------------
        case BLE_GAP_EVT_DISCONNECTED :
			DEB("ble_evt_handler: BLE_GAP_EVT_DISCONNECTED \n");

			// invalidate the handle
			DEB("ble_evt_handler: m_conn_handle=BLE_CONN_HANDLE_INVALID \n");
			m_conn_handle = BLE_CONN_HANDLE_INVALID;

            // Reset m_caps_on variable. Upon reconnect, the HID host will re-send the Output
            // report containing the Caps lock state.
			//DEB("ble_evt_handler: m_caps_on=false \n");
            m_caps_on = false;

			// Clear Passkey
			//DEB("ble_evt_handler: m_passkey <-- 0 (clear)  ??? \n");
			memset(&m_passkey, 0, sizeof(passkey_t));
			//
		
			if(M_COMM_ADV_WAITE_GAP_DISCONNECT == m_comm_adv_get_status() )
			//	|| M_COMM_ADV_STATUS_CONNECTING == m_comm_adv_get_status())
			{
				if(is_BT_channel(m_config.config.selected_ch))
				{
					// TODO: be careful conversion: m_comm_channel_type_t --> m_comm_adv_channel_type_t
					m_comm_adv_channel_type_t adv_ch_typ = (m_comm_adv_channel_type_t)m_comm_get_selected_channel();

					DEB("update_device_name(%d)....'%s'\n",adv_ch_typ, m_mokibo_name[adv_ch_typ] );
					update_device_name(adv_ch_typ); // --> sd_ble_gap_device_name_set('MOKIBO BT2') 
					
					#if defined(M_COMM_EVT_TRACE)
						DEB("ble_evt_handler: m_comm_adv_start(ch=%d)...\n",adv_ch_typ);
					#endif
					m_comm_adv_start(adv_ch_typ);
				}
			}
            break; // BLE_GAP_EVT_DISCONNECTED

		//-----------------------------------------------------------------------------------------------------------------------------------
		#ifndef S140
        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
			{
				//DEB("ble_evt_handler: BLE_GAP_EVT_PHY_UPDATE_REQUEST \n");
				//DEB("ble_evt_handler: sd_ble_gap_phy_update() \n");
				ble_gap_phys_t const phys =
				{
					.rx_phys = BLE_GAP_PHY_AUTO,
					.tx_phys = BLE_GAP_PHY_AUTO,
				};
				rv = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys); APP_ERROR_CHECK(rv);
			}
			break;
		#endif
        
		//-----------------------------------------------------------------------------------------------------------------------------------
		case BLE_GATTS_EVT_HVN_TX_COMPLETE:
			if(!BUFFER_LIST_EMPTY())
			{
				buffer_dequeue(true);
			}
            break;

		//-----------------------------------------------------------------------------------------------------------------------------------
        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
			#if defined(M_COMM_EVT_TRACE)
				DEB("ble_evt_handler: BLE_GATTC_EVT_TIMEOUT - sd_ble_gap_disconnect(BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION) \n");
			#endif
            rv = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            //deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(rv);
            break;

		//-----------------------------------------------------------------------------------------------------------------------------------
        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
			#if defined(M_COMM_EVT_TRACE)
				DEB("ble_evt_handler: BLE_GATTS_EVT_TIMEOUT - sd_ble_gap_disconnect(BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION)\n");
			#endif
            rv = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            //deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(rv);
            break;

        case BLE_GAP_EVT_TIMEOUT:
			DEB1("ble_evt_handler: BLE_GAP_EVT_TIMEOUT \n");

            //rv = sd_ble_gap_disconnect(m_conn_handle,BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            //deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(rv);
            break;

		//-----------------------------------------------------------------------------------------------------------------------------------
        case BLE_EVT_USER_MEM_REQUEST:
			#if defined(M_COMM_EVT_TRACE)
				DEB("ble_evt_handler: sd_ble_user_mem_reply(m_conn_handle=0x%x,NULL) \n",m_conn_handle);
			#endif
            rv = sd_ble_user_mem_reply(m_conn_handle, NULL); 
			//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(rv);
            break;

		//-----------------------------------------------------------------------------------------------------------------------------------
        case BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST:
			{
				DEB("ble_evt_handler: BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST \n");
				
				#if	(defined MOKIBO_DFU_ENABLED) && (MOKIBO_DFU_ENABLED)
				if(!ble_dfu_state_run()) {
				#endif
				ble_gatts_evt_rw_authorize_request_t  req;
				ble_gatts_rw_authorize_reply_params_t auth_reply;
				req = p_ble_evt->evt.gatts_evt.params.authorize_request;

				if (req.type != BLE_GATTS_AUTHORIZE_TYPE_INVALID)
				{
					if ((req.request.write.op == BLE_GATTS_OP_PREP_WRITE_REQ)     ||
						(req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_NOW) ||
						(req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_CANCEL))
					{
						if (req.type == BLE_GATTS_AUTHORIZE_TYPE_WRITE)
						{
							auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_WRITE;
						}
						else
						{
							auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
						}
						auth_reply.params.write.gatt_status = APP_FEATURE_NOT_SUPPORTED;

						#if defined(M_COMM_EVT_TRACE)
							DEB("ble_evt_handler: sd_ble_gatts_rw_authorize_reply()\n");
						#endif
						rv = sd_ble_gatts_rw_authorize_reply(p_ble_evt->evt.gatts_evt.conn_handle,&auth_reply);
						//APP_ERROR_CHECK(rv);	// deleted by JAEHONG(2019/05/22)
					}
				}
				#if	(defined MOKIBO_DFU_ENABLED) && (MOKIBO_DFU_ENABLED)
				}
				#endif
			}
			break; // BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST

		//-----------------------------------------------------------------------------------------------------------------------------------
		case BLE_GAP_EVT_AUTH_KEY_REQUEST:
			{
				// Reset m_caps_on variable. Upon reconnect, the HID host will re-send the Output
				// report containing the Caps lock state.
				//DEB("ble_evt_handler: m_caps_on=false \n");
				m_caps_on = false;

				// Clear Passkey
				//DEB("ble_evt_handler: m_passkey <-- 0 (clear)  ??? \n");
				memset(&m_passkey, 0, sizeof(passkey_t));

				#if MOKIBO_NFC_ENABLED
				if(!m_nfc_state_run())
					m_passkey.is_ready = true;
				#else
					m_passkey.is_ready = true;
				#endif
				
				//DEB("ble_evt_handler: m_passkey.is_ready <--true \n");

			} 
			break; // BLE_GAP_EVT_AUTH_KEY_REQUEST

		//-----------------------------------------------------------------------------------------------------------------------------------
        case BLE_GAP_EVT_AUTH_STATUS:
			//security_dispatcher.c::smd_ble_evt_handler() process
			{
				if (p_ble_evt->evt.gap_evt.params.auth_status.auth_status == BLE_GAP_SEC_STATUS_SUCCESS)
				{
					#if defined(M_COMM_EVT_TRACE)
						DEB("ble_evt_handler: Authorization succeeded! \n");
					#endif

					#if MOKIBO_NFC_ENABLED	
					if(m_nfc_state_start())
					{
						#if defined(M_COMM_CH_TRACE)
							DEB(">>>>>>>> BT channel NFC stop! \n");
						#endif
						m_nfc_stop();
					}
					#endif
				}
				else
				{
					#if defined(M_COMM_EVT_TRACE)
						DEB("ble_evt_handler: Authorization failed with code: %u!", p_ble_evt->evt.gap_evt.params.auth_status.auth_status);
						DEB("ble_evt_handler: m_config.config.selected_ch = M_COMM_CENTRAL_OTHER \n");
					#endif
					rv = m_comm_set_central_type(m_config.config.selected_ch, M_COMM_CENTRAL_OTHER); PRINT_IF_ERROR("m_comm_set_central_type()", rv);

					sd_ble_gap_disconnect(p_ble_evt->evt.gap_evt.conn_handle,BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
				}
			} 
			break;

		//-----------------------------------------------------------------------------------------------------------------------------------
        case BLE_GAP_EVT_CONN_PARAM_UPDATE:
			DEB("ble_evt_handler: BLE_GAP_EVT_CONN_PARAM_UPDATE \n");
			DEB("ble_evt_handler: m_comm_adv_set_status(M_COMM_ADV_STATUS_CONNECTRUN)\n");
			m_comm_adv_set_status(M_COMM_ADV_STATUS_CONNECTRUN);
			break; // BLE_GAP_EVT_CONN_PARAM_UPDATE

		//-----------------------------------------------------------------------------------------------------------------------------------
        case BLE_GAP_EVT_CONN_SEC_UPDATE:
			//security_dispatcher.c::smd_ble_evt_handler() process
			DEB("ble_evt_handler: BLE_GAP_EVT_CONN_SEC_UPDATE \n");
			DEB("ble_evt_handler: m_comm_adv_set_status(M_COMM_ADV_STATUS_CONNECTRUN)\n");
			m_comm_adv_set_status(M_COMM_ADV_STATUS_CONNECTRUN);
			break; // BLE_GAP_EVT_CONN_SEC_UPDATE

		//-----------------------------------------------------------------------------------------------------------------------------------
        case BLE_GAP_EVT_SEC_INFO_REQUEST:
			//security_dispatcher.c::smd_ble_evt_handler() process
			DEB("ble_evt_handler: BLE_GAP_EVT_SEC_INFO_REQUEST \n");
			break; // BLE_GAP_EVT_SEC_INFO_REQUEST

		//-----------------------------------------------------------------------------------------------------------------------------------
        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
			//security_dispatcher.c::smd_ble_evt_handler() process
			DEB("ble_evt_handler: BLE_GAP_EVT_SEC_PARAMS_REQUEST \n");
			break; // BLE_GAP_EVT_SEC_PARAMS_REQUEST
			
        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
			DEB("ble_evt_handler: BLE_GATTS_EVT_SYS_ATTR_MISSING \n");
            // No system attributes have been stored.
            rv = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0, 0);
			DEB("sd_ble_gatts_sys_attr_set() rsp err_code:%d\r\n",rv);
            break;

		//-----------------------------------------------------------------------------------------------------------------------------------
        default:
            // No implementation needed.
			#if defined(M_COMM_EVT_TRACE)
				//DEB("ble_evt_handler(): NOT PROCESSED EVT_ID: 0x%02x \n", p_ble_evt->header.evt_id);
			#endif
			break;

    }//switch (p_ble_evt->header.evt_id)

	
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			ble_stack_init
//-----------------------------------------------------------------------------------------------------------------------------------
// Function for initializing the BLE stack.
// Initializes the SoftDevice and the BLE event interrupt.
static void ble_stack_init(void)
{
	________________________________LOG_mComm_FuncHeads(0,"");
    ret_code_t rv;

    rv = nrf_sdh_enable_request(); 
	//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(rv);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    rv = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start); 
	//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(rv);

    // Enable BLE stack.
    rv = nrf_sdh_ble_enable(&ram_start); 
	//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(rv);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			click_button_send
//-----------------------------------------------------------------------------------------------------------------------------------
static ret_code_t click_button_send(uint8_t left, uint8_t right, uint8_t center)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
    ret_code_t err_code;

	uint8_t buffer[INPUT_REP_BUTTONS_LEN];

	memset(buffer, 0, INPUT_REP_BUTTONS_LEN);

	if(left) 	{ buffer[BUTTONS_INDEX] |= LEFT_PRESS; }
	if(right) 	{ buffer[BUTTONS_INDEX] |= RIGHT_PRESS; }
	if(center) 	{ buffer[BUTTONS_INDEX] |= CENTER_PRESS; }
	
	//
	________________________________LOG_mComm_SendingGstr(6,"send Click[%d|%d|%d]",left,center,right);	
	SYSTEM_MSG("click_button_send: SEND_TOUCH_BUTTON - ");
	//for(int i=0; i<INPUT_REP_BUTTONS_LEN;i++) SYSTEM_MSG(" %02X",buffer[i]);
	//SYSTEM_MSG("\n");
	//
	err_code = send(SEND_TOUCH_BUTTON, buffer, INPUT_REP_BUTTONS_LEN);
	if (NRF_SUCCESS != err_code)
    {
		SYSTEM_MSG("click_button_send: error: %d \n", err_code);
    }

	return err_code;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			wheel_send
//-----------------------------------------------------------------------------------------------------------------------------------
static ret_code_t wheel_send(int8_t up_down, int8_t left_right, uint8_t trvl_dist)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
    ret_code_t err_code;
	uint8_t buffer[INPUT_REP_BUTTONS_LEN];

	memset(buffer, 0, INPUT_REP_BUTTONS_LEN);

	// Vertical
	if(up_down != 0) 
	{
		if(up_down < 0)
		{
			buffer[WHEEL_INDEX] = -1*trvl_dist;//WHEEL_UP;	// 아래
			
			________________________________LOG_mComm_SendingGstr(6,"send Wheel[UP]");	
		}
		else
		{
			buffer[WHEEL_INDEX] = 1 * trvl_dist;//WHEEL_DOWN;	// 위			
			________________________________LOG_mComm_SendingGstr(6,"send Wheel[DOWN]");	
		}
	}		
	
	// horizontal
	if(left_right != 0) 
	{
		if(left_right < 0)
		{
			buffer[AC_INDEX] = WHEEL_RIGHT;	// 오른쪽
			________________________________LOG_mComm_SendingGstr(6,"send Wheel[RIGHT]");	
		}
		else
		{
			buffer[AC_INDEX] = WHEEL_LEFT;	// 왼쪽
			________________________________LOG_mComm_SendingGstr(6,"send Wheel[LEFT]");	
		}
	}		
	
	err_code = send(SEND_TOUCH_BUTTON, buffer, INPUT_REP_BUTTONS_LEN);
    
	if (NRF_SUCCESS != err_code)
    {
		________________________________LOG_mComm_SendingGstr(6,"!send Wheel[ERROR]");	
    }

	return err_code;
}

/*===========================================================================//
	name    : multi_touch_send
	author  : MJ
	caller  : 		
		m_comm_send_event(M_COMM_EVENT_MUTI_TOUCH_UPDATE, val);		
	params  : 
		m_comm_evt_arg_multi_touch_t	touch
	returns : 
		ret_code_t
	object  : 
		멀티제스처 들어온 값을 OS 별로 분류하여 동작
		back 을 사용하여 키입력 리셋		
	history : 
		20180000_MJ : 파일생성		
		20190222_flimbs: 3점 4점 터치 구분하여 정확하게 적용.
//===========================================================================*/
static ret_code_t multi_touch_send(m_comm_evt_arg_multi_touch_t	touch)
{	
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t err_code = NRF_SUCCESS;	
	uint8_t buffer[INPUT_REP_KEYS_MAX_LEN] = {0, }; // Clear all
	
	if(M_COMM_CENTRAL_MAC == m_config.config.central[m_config.config.selected_ch])
	{
		//MAC 2점
		if(touch.two.scroll_up)
		{			
			________________________________LOG_mComm_SendingGstr(6,"MAC touch.two.scroll_up");	
			err_code = wheel_send(1, 0, touch.two.scroll_trvldist); 
		}
		
		if(touch.two.scroll_down)
		{			
			________________________________LOG_mComm_SendingGstr(6,"MAC touch.two.scroll_down");	
			err_code = wheel_send(-1, 0, touch.two.scroll_trvldist); 
		}
				
		if(touch.two.scroll_left)
		{			
			________________________________LOG_mComm_SendingGstr(6,"MAC touch.two.scroll_left");	
			/*
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_SHIFT;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			err_code = wheel_send(1, 0, WHEEL_DEFAULT_TRVDIST); 
			*/
			err_code = wheel_send(0, 1, WHEEL_DEFAULT_TRVDIST); 
		}
		
		if(touch.two.scroll_right)
		{
			________________________________LOG_mComm_SendingGstr(6,"MAC touch.two.scroll_right");	
			/*
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_SHIFT;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			err_code = wheel_send(-1, 0, WHEEL_DEFAULT_TRVDIST); 
			*/
			err_code = wheel_send(0, -1, WHEEL_DEFAULT_TRVDIST); 
		}
				
		if(touch.two.back) 
		{//스크롤 좌우가 Shift Modifier 사용하기 때문에 키리셋 해줘야함		
			________________________________LOG_mComm_SendingGstr(6,"MAC touch.tow.back");	
			/*
			memset(buffer, 0, sizeof(buffer));
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);	*/
		}

        // 3, tab 터치 처리
        if(touch.three.tab)
        {					
			________________________________LOG_mComm_SendingGstr(6,"MAC touch.three.tab ");			
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = MAC_CMD_KEY | APP_USBD_HID_KBD_MODIFIER_LEFT_CTRL;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_D;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);						
        }
		if(touch.three.scroll_up)
		{
			
			________________________________LOG_mComm_SendingGstr(6,"MAC touch.three.scroll_up ");			
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_CTRL;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_UP;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			
		}
		if(touch.three.scroll_down)
		{
			
			________________________________LOG_mComm_SendingGstr(6,"MAC touch.three.scroll_down ");						
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_CTRL;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_DOWN;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			
		}
		if(touch.three.scroll_left)
		{
			
			________________________________LOG_mComm_SendingGstr(6,"MAC touch.three.scroll_left ");			
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_CTRL;
			//buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_RGHT;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = touch.three.option ? APP_USBD_HID_KBD_RGHT : 0;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			
		}
		if(touch.three.scroll_right)
		{
			
			________________________________LOG_mComm_SendingGstr(6,"MAC touch.three.scroll_right ");			
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_CTRL;
			//buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_LFT;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = touch.three.option ? APP_USBD_HID_KBD_LFT : 0;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);			

		}
		if(touch.three.back)
		{			
			________________________________LOG_mComm_SendingGstr(6,"MAC touch.three.back ");			
			memset(buffer, 0, sizeof(buffer));
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		}

		//MAC 4 TOUCH
		if(touch.four.tab)
		{			
			________________________________LOG_mComm_SendingGstr(6,"MAC touch.four.tab ");            
            buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = MAC_CMD_KEY | APP_USBD_HID_KBD_MODIFIER_LEFT_CTRL;
            buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_D;
            err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		}
				
		if(touch.four.scroll_up)
		{
			________________________________LOG_mComm_SendingGstr(6,"MAC touch.four.scrll_up ");
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_CTRL;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_UP;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		}
		
		// 3, 4점 scroll down 터치 처리
		if(touch.four.scroll_down)
		{
			________________________________LOG_mComm_SendingGstr(6,"MAC touch.four.scroll_down ");
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_CTRL;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_DOWN;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		}
		
		// 3, 4점 scroll left 터치 처리
		if(touch.four.scroll_left)
		{
			________________________________LOG_mComm_SendingGstr(6,"MAC touch.four.scroll_left ");			
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_CTRL;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_RGHT;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		}
				
		if(touch.four.scroll_right)
		{
			________________________________LOG_mComm_SendingGstr(6,"MAC touch.four.scroll_right ");	
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_CTRL;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_LFT;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		}
		
		if(touch.four.back)
		{			
			________________________________LOG_mComm_SendingGstr(6,"MAC touch.four.back ");
			memset(buffer, 0, sizeof(buffer));
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		}
	}
	else if((M_COMM_CENTRAL_MICROSOFT == m_config.config.central[m_config.config.selected_ch]) || (M_COMM_CENTRAL_OTHER == m_config.config.central[m_config.config.selected_ch]))
	{				
		//Windows 2점
		if(touch.two.scroll_up)
		{			
			________________________________LOG_mComm_SendingGstr(6,"WIN touch.two.scroll_up");
			err_code = wheel_send(-1, 0, touch.two.scroll_trvldist); 			
		}
		
		if(touch.two.scroll_down)
		{	
			________________________________LOG_mComm_SendingGstr(6,"WIN touch.two.scroll_down");
			err_code = wheel_send(1, 0, touch.two.scroll_trvldist); 
		}
				
		if(touch.two.scroll_left)
		{			
			________________________________LOG_mComm_SendingGstr(6,"WIN touch.two.scroll_left");
			err_code = wheel_send(0, -1, WHEEL_DEFAULT_TRVDIST); 		
		}
		
		if(touch.two.scroll_right)
		{			
			________________________________LOG_mComm_SendingGstr(6,"WIN touch.two.scroll_right");
			err_code = wheel_send(0, 1, WHEEL_DEFAULT_TRVDIST); 
		}
				
		if(touch.two.back)
		{
			________________________________LOG_mComm_SendingGstr(6,"WIN touch.two.scroll_back");
		}

		//Windows 3점
		if(touch.three.tab)
		{			
			static bool flag = false;
			if(flag == true)
			{
				________________________________LOG_mComm_SendingGstr(6,"WIN touch.three.tab SendFlag:%d",flag);
				//buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = WINDOW_KEY;
				buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_ESC;
				err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);	
				flag = false;
			}
			else
			{
				________________________________LOG_mComm_SendingGstr(6,"WIN touch.three.tab SendFlag:%d",flag);
				buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = WINDOW_KEY;
				buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_S;
				err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);	
				flag = true;
			}						
		}

		if(touch.three.scroll_up)
		{
			
			________________________________LOG_mComm_SendingGstr(6,"WIN touch.three.scroll_up");
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = WINDOW_KEY;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_D;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);			
			
		}
		if(touch.three.scroll_down)
		{
			________________________________LOG_mComm_SendingGstr(6,"WIN touch.three.scroll_down");
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = WINDOW_KEY;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_D;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		}
						
		if(touch.three.scroll_left)
		{//쓰지말까.....
			
			________________________________LOG_mComm_SendingGstr(6,"WIN touch.three.scroll_left");
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_ALT | APP_USBD_HID_KBD_MODIFIER_LEFT_SHIFT;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = touch.three.option ? APP_USBD_HID_KBD_TAB : 0;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			
		}
		
		if(touch.three.scroll_right)
		{						
			
			________________________________LOG_mComm_SendingGstr(6,"WIN touch.three.scroll.right");
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_ALT;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = touch.three.option ? APP_USBD_HID_KBD_TAB : 0;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);			
			
		}

		if(touch.three.back)
		{
			________________________________LOG_mComm_SendingGstr(6,"WIN touch.three.back");
			memset(buffer, 0, sizeof(buffer));
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);			
		}

		//Windows 4점
		if(touch.four.tab)
		{			
			________________________________LOG_mComm_SendingGstr(6,"WIN touch.four.tab");
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = WINDOW_KEY;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_A;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		}
		
		if(touch.four.scroll_up)
		{			
			________________________________LOG_mComm_SendingGstr(6,"WIN touch.four.scroll_up");			
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = WINDOW_KEY;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_TAB;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		}
		
		if(touch.four.scroll_down)
		{
			________________________________LOG_mComm_SendingGstr(6,"WIN touch.four.scroll.down");
			//buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = WINDOW_KEY;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_ESC;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		}
		
		if(touch.four.scroll_left)
		{
			________________________________LOG_mComm_SendingGstr(6,"WIN touch.four.scroll.left");
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_CTRL | WINDOW_KEY;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_RGHT;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);			
		}
			
		if(touch.four.scroll_right)
		{
			________________________________LOG_mComm_SendingGstr(6,"touch.four.scroll.right");
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_CTRL | WINDOW_KEY;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_LFT;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);			
		}

		if(touch.four.back)
		{			
			________________________________LOG_mComm_SendingGstr(6,"WIN touch.four.back");
			memset(buffer, 0, sizeof(buffer));
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);			
		}
	}
	else if(M_COMM_CENTRAL_ANDROID == m_config.config.central[m_config.config.selected_ch])
	{
		//Windows 2점
		if(touch.two.scroll_up)
		{			
			________________________________LOG_mComm_SendingGstr(6,"AND touch.two.scroll_up");
			err_code = wheel_send(-1, 0, touch.two.scroll_trvldist); 	
		}
		
		if(touch.two.scroll_down)
		{	
			________________________________LOG_mComm_SendingGstr(6,"AND touch.two.scroll_down");
			err_code = wheel_send(1, 0, touch.two.scroll_trvldist); 
		}
				
		if(touch.two.scroll_left)
		{			
			________________________________LOG_mComm_SendingGstr(6,"AND touch.two.scroll_left");
			err_code = wheel_send(0, -1, WHEEL_DEFAULT_TRVDIST); 		
		}
		
		if(touch.two.scroll_right)
		{			
			________________________________LOG_mComm_SendingGstr(6,"AND touch.two.scroll_right");
			err_code = wheel_send(0, 1, WHEEL_DEFAULT_TRVDIST); 
		}
				
		if(touch.two.back)
		{
			________________________________LOG_mComm_SendingGstr(6,"AND touch.two.scroll_back");
		}

		//ANDdows 3점
		if(touch.three.tab)
		{	
			________________________________LOG_mComm_SendingFnHotKey(1,"AND backward");
		
			uint8_t buf[M_COMM_CONSUMER_BUF_LEN] = {0,0,0};

			memcpy(buf, m_android_back, sizeof(m_android_back));										
			send(SEND_CONSUMER, buf, sizeof(m_android_back));

			//
			
			/*
			uint8_t buf[M_COMM_CONSUMER_BUF_LEN] = {0,0,0};
			
			memcpy(buf, m_iphone_home, sizeof(m_iphone_home));										
			send(SEND_CONSUMER, buf, sizeof(m_iphone_home));*/
		}

		if(touch.three.scroll_up)
		{
			________________________________LOG_mComm_SendingGstr(6,"AND touch.three.scroll_up");	
			//
			/*
			
			DEB("AND:mouseCenter: home\n");			
			err_code = click_button_send(0, 0, CENTER_PRESS);
			err_code = click_button_send(0, 0, 0);*/
			
		}
		if(touch.three.scroll_down)
		{
			________________________________LOG_mComm_SendingGstr(6,"AND touch.three.scroll_down");
			/*		
			DEB("AND:mouseCenter: home\n");			
			err_code = click_button_send(0, 0, CENTER_PRESS);
			err_code = click_button_send(0, 0, 0);*/
		}
						
		if(touch.three.scroll_left)
		{//쓰지말까.....
			uint8_t buf[M_COMM_CONSUMER_BUF_LEN] = {0,0,0};

			memcpy(buf, m_android_back, sizeof(m_android_back));										
			send(SEND_CONSUMER, buf, sizeof(m_android_back));
			
			________________________________LOG_mComm_SendingGstr(6,"AND touch.three.scroll_left");
			/*
			DEB("AND:mouseCenter: home\n");	
			err_code = click_button_send(0, RIGHT_PRESS, 0);
			err_code = click_button_send(0, 0, 0);*/
			
		}
		
		if(touch.three.scroll_right)
		{						

			________________________________LOG_mComm_SendingGstr(6,"ANDtouch.three.scroll.right");			
			/*
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_ALT;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = touch.three.option ? APP_USBD_HID_KBD_TAB : 0;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);			*/
			
		}

		if(touch.three.back)
		{			
			________________________________LOG_mComm_SendingGstr(6,"AND touch.three.back");
			memset(buffer, 0, sizeof(buffer));
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);			
		}

		//Windows 4점
		if(touch.four.tab)
		{			

			

			/*
			________________________________LOG_mComm_SendingGstr(6,"AND touch.four.tab");
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = WINDOW_KEY;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_A;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);*/
		}
		
		if(touch.four.scroll_up)
		{			

			________________________________LOG_mComm_SendingGstr(6,"AND fourtab");	


						

			/*
			
			________________________________LOG_mComm_SendingGstr(6,"AND touch.four.scroll_up");			
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = WINDOW_KEY;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_TAB;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);*/
		}
		
		if(touch.four.scroll_down)
		{
			________________________________LOG_mComm_SendingGstr(6,"AND fourtab");	


		
			/*
			________________________________LOG_mComm_SendingGstr(6,"AND touch.four.scroll.down");
			//buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = WINDOW_KEY;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_ESC;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);*/
		}
		
		if(touch.four.scroll_left)
		{
			/*
			________________________________LOG_mComm_SendingGstr(6,"AND touch.four.scroll.left");
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_CTRL | WINDOW_KEY;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_RGHT;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);*/			
		}
			
		if(touch.four.scroll_right)
		{
			/*________________________________LOG_mComm_SendingGstr(6,"touch.four.scroll.right");
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_CTRL | WINDOW_KEY;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_LFT;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);			*/
		}

		if(touch.four.back)
		{			
			________________________________LOG_mComm_SendingGstr(6,"AND touch.four.back");
			memset(buffer, 0, sizeof(buffer));
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);			
		}
	}
	else if(M_COMM_CENTRAL_IOS == m_config.config.central[m_config.config.selected_ch])
	{
		//MAC 2점
		if(touch.two.scroll_up)
		{			
			________________________________LOG_mComm_SendingGstr(6,"IOS touch.two.scroll_up");	
			err_code = wheel_send(1, 0, touch.two.scroll_trvldist); 
		}
		
		if(touch.two.scroll_down)
		{			
			________________________________LOG_mComm_SendingGstr(6,"IOS touch.two.scroll_down");	
			err_code = wheel_send(-1, 0, touch.two.scroll_trvldist); 
		}
				
		if(touch.two.scroll_left)
		{			
			________________________________LOG_mComm_SendingGstr(6,"IOS touch.two.scroll_left");	
			/*
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_SHIFT;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			err_code = wheel_send(1, 0, WHEEL_DEFAULT_TRVDIST); 
			*/
			err_code = wheel_send(0, 1, WHEEL_DEFAULT_TRVDIST); 
		}
		
		if(touch.two.scroll_right)
		{
			________________________________LOG_mComm_SendingGstr(6,"IOS touch.two.scroll_right");	
			/*
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_SHIFT;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			err_code = wheel_send(-1, 0, WHEEL_DEFAULT_TRVDIST); 
			*/
			err_code = wheel_send(0, -1, WHEEL_DEFAULT_TRVDIST); 
		}
				
		if(touch.two.back) 
		{//스크롤 좌우가 Shift Modifier 사용하기 때문에 키리셋 해줘야함		
			________________________________LOG_mComm_SendingGstr(6,"IOS touch.tow.back");	
			
			memset(buffer, 0, sizeof(buffer));
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		}

        // 3, tab 터치 처리
        if(touch.three.tab)
        {					
			________________________________LOG_mComm_SendingGstr(6,"IOS touch.three.tab ");						
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = MAC_CMD_KEY | APP_USBD_HID_KBD_MODIFIER_LEFT_ALT;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_D;
            err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);

        }
		if(touch.three.scroll_up)
		{
			________________________________LOG_mComm_SendingGstr(6,"IOS touch.three.scroll_up ");		
			
			uint8_t buf[M_COMM_CONSUMER_BUF_LEN] = {0,0,0};
			
			memcpy(buf, m_iphone_home, sizeof(m_iphone_home));										
			send(SEND_CONSUMER, buf, sizeof(m_iphone_home));			
			nrf_delay_ms(50);
			memset(buf, 0, sizeof(m_iphone_home));
			send(SEND_CONSUMER, buf, sizeof(m_iphone_home));

			nrf_delay_ms(180);
			memcpy(buf, m_iphone_home, sizeof(m_iphone_home));										
			send(SEND_CONSUMER, buf, sizeof(m_iphone_home));			
			nrf_delay_ms(50);
			memset(buf, 0, sizeof(m_iphone_home));
			send(SEND_CONSUMER, buf, sizeof(m_iphone_home));
			
			
		}
		if(touch.three.scroll_down)
		{
			
			________________________________LOG_mComm_SendingGstr(6,"IOS touch.three.scroll_down ");	
			
			uint8_t buf[M_COMM_CONSUMER_BUF_LEN] = {0,0,0};
			
			memcpy(buf, m_iphone_home, sizeof(m_iphone_home));										
			send(SEND_CONSUMER, buf, sizeof(m_iphone_home));
			nrf_delay_ms(50);
			memset(buf, 0, sizeof(m_iphone_home));
			send(SEND_CONSUMER, buf, sizeof(m_iphone_home));
		}
		if(touch.three.scroll_left)
		{
			
			________________________________LOG_mComm_SendingGstr(6,"IOS touch.three.scroll_left ");		
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = MAC_CMD_KEY | APP_USBD_HID_KBD_MODIFIER_LEFT_SHIFT;
			//buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_LFT;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = touch.three.option ? APP_USBD_HID_KBD_TAB : 0;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);		
			
		}
		if(touch.three.scroll_right)
		{
			
			________________________________LOG_mComm_SendingGstr(6,"IOS touch.three.scroll_right ");		
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = MAC_CMD_KEY;
			//buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_LFT;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = touch.three.option ? APP_USBD_HID_KBD_TAB : 0;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);			

		}
		if(touch.three.back)
		{			
			________________________________LOG_mComm_SendingGstr(6,"IOS touch.three.back ");			
			memset(buffer, 0, sizeof(buffer));
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		}
	}
	return err_code;
}

//=========================================================================================================
//			Function name : special_key_send()
//=========================================================================================================
static ret_code_t special_key_send(m_comm_evt_arg_special_key_t	key)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t err_code = NRF_SUCCESS;
	uint8_t buffer[INPUT_REP_KEYS_MAX_LEN] = {0, }; // Clear all	

	#if 1
	if(key.language_z)
	{
		________________________________LOG_mComm_SendingFnHotKey(3,"key.language_z");
		buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = 100;
		err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		PRINT_IF_ERROR("send()", err_code);
	}
	if(key.language_x)
	{
		buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_SHIFT;
		________________________________LOG_mComm_SendingFnHotKey(3,"key.language_c");
		buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = 100;
		err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		PRINT_IF_ERROR("send()", err_code);
		
	}
	if(key.language_c)
	{
		________________________________LOG_mComm_SendingFnHotKey(3,"key.language_x");
		buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_RIGHT_ALT;
		buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = 100;
		err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		PRINT_IF_ERROR("send()", err_code);
	}
	#else
	static int16_t testKey = 0;
	if(key.language_z)
	{
		________________________________LOG_mComm_SendingFnHotKey(3,"send:%x",testKey);		
		buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = --testKey;
		err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		PRINT_IF_ERROR("send()", err_code);
	}
	if(key.language_x)
	{
		________________________________LOG_mComm_SendingFnHotKey(3,"send:%x",testKey);
		buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = testKey;
		err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		PRINT_IF_ERROR("send()", err_code);
		
	}
	if(key.language_c)
	{
		________________________________LOG_mComm_SendingFnHotKey(3,"send:%x",testKey);
		buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = ++testKey;
		err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		PRINT_IF_ERROR("send()", err_code);
	}
	#endif

	
	if(M_COMM_CENTRAL_MAC == m_config.config.central[m_config.config.selected_ch])
	{
		if(key.home)
		{
			________________________________LOG_mComm_SendingFnHotKey(1,"MAC home");
			
			DEB("MAC:key_home:\n");
			// home key: F11
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_F11;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			PRINT_IF_ERROR("send()", err_code);
		}		

		if(key.swtapp)
		{
			________________________________LOG_mComm_SendingFnHotKey(1,"swtapp home");
			
			DEB("MAC:cmd_tab:\n");
			// switching App: CMD + Tab
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = MAC_CMD_KEY;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = key.search_opt ? APP_USBD_HID_KBD_TAB : 0;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			PRINT_IF_ERROR("send()", err_code);
			/*
			nrf_delay_ms(100);
			
			uint8_t buf[M_COMM_CONSUMER_BUF_LEN] = {0,0,0};
			
			memcpy(buf, m_iphone_home, sizeof(m_iphone_home));										
			send(SEND_CONSUMER, buf, sizeof(m_iphone_home));
			
			nrf_delay_ms(100);

			memset(buf, 0, sizeof(m_iphone_home));
			send(SEND_CONSUMER, buf, sizeof(m_iphone_home));

			nrf_delay_ms(100);

			memcpy(buf, m_iphone_home, sizeof(m_iphone_home));										
			send(SEND_CONSUMER, buf, sizeof(m_iphone_home));
			
			nrf_delay_ms(100);

			memset(buf, 0, sizeof(m_iphone_home));
			send(SEND_CONSUMER, buf, sizeof(m_iphone_home));
			*/
		}

		if(key.vkbd)
		{
			
			________________________________LOG_mComm_SendingFnHotKey(1,"MAC vkbd");
			// toggle virtual keyboard
			memcpy(buffer, m_virtual_keyboard_cmd, sizeof(m_virtual_keyboard_cmd));
			err_code = send(SEND_CONSUMER, buffer, sizeof(m_vol_up_cmd));
			PRINT_IF_ERROR("send()", err_code);
			memcpy(buffer, m_consumer_no_cmd, sizeof(m_consumer_no_cmd));
			err_code = send(SEND_CONSUMER, buffer, sizeof(m_vol_up_cmd));
			PRINT_IF_ERROR("send()", err_code);
			


			nrf_delay_ms(100);
			uint8_t buf[M_COMM_CONSUMER_BUF_LEN] = {0,0,0};

			memcpy(buf, m_iphone_vkbd, sizeof(m_iphone_vkbd));										
			//send(SEND_CONSUMER, buf, sizeof(m_iphone_vkbd));
			
			//nrf_delay_ms(50);

			//memset(buf, 0, sizeof(m_iphone_vkbd));
			//send(SEND_CONSUMER, buf, sizeof(m_iphone_vkbd));

		}

		if(key.search)
		{
			
			________________________________LOG_mComm_SendingFnHotKey(1,"MAC search");
			// search: CMD + Space
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = MAC_CMD_KEY;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_SB;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			PRINT_IF_ERROR("send()", err_code);	
			
		}
		
		if(key.capture)
		{
			________________________________LOG_mComm_SendingFnHotKey(1,"MAC capture");
			// capture: Shift + CMD + ctrl + "3"
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = 
			APP_USBD_HID_KBD_MODIFIER_LEFT_SHIFT | MAC_CMD_KEY;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_3;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			PRINT_IF_ERROR("send()", err_code);
		}
		
		if(key.lockscreen)
		{			
			
			________________________________LOG_mComm_SendingFnHotKey(1,"MAC lockscreen");
			// lockscreen: Shift + CMD + ctrl + "Q"
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = 
			APP_USBD_HID_KBD_MODIFIER_LEFT_CTRL | MAC_CMD_KEY;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_Q;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			PRINT_IF_ERROR("send()", err_code);
			
			
			//uint8_t buf[M_COMM_CONSUMER_BUF_LEN] = {0,0,0};
			
			//memcpy(buf, m_iphone_lock, sizeof(m_iphone_lock));										
			//send(SEND_CONSUMER, buf, sizeof(m_iphone_lock));
			/*
			nrf_delay_ms(100);

			memset(buf, 0, sizeof(m_iphone_lock));
			send(SEND_CONSUMER, buf, sizeof(m_iphone_lock));
			*/
			
		}
		if (key.backward)
		{//Alt 백스페이스
			________________________________LOG_mComm_SendingFnHotKey(1,"WIN backward");
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = MAC_CMD_KEY;
			
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_LFT;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		}
		// 복구 처리
		if(key.release)
		{
			________________________________LOG_mComm_SendingFnHotKey(1,"MAC release");
			// step 1) release all keys
			memset(buffer, 0, sizeof(buffer));
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			PRINT_IF_ERROR("send()", err_code);
		}

		
	}  
	else if((M_COMM_CENTRAL_MICROSOFT == m_config.config.central[m_config.config.selected_ch]) || (M_COMM_CENTRAL_OTHER == m_config.config.central[m_config.config.selected_ch]))
	{
		if(key.home)
		{
			________________________________LOG_mComm_SendingFnHotKey(1,"WIN home");
			// home key: Window + "d"
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = WINDOW_KEY;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_D;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			PRINT_IF_ERROR("send()", err_code);
		}

		if(key.swtapp)
		{
			
			________________________________LOG_mComm_SendingFnHotKey(1,"WIN swtap");
			// switching App: alt + tab
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_ALT;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = key.search_opt ? APP_USBD_HID_KBD_TAB : 0;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			PRINT_IF_ERROR("send()", err_code);
		}

		if(key.vkbd)
		{
			________________________________LOG_mComm_SendingFnHotKey(1,"WIN vkbd");
			// toggle virtual keyboard
			memcpy(buffer, m_virtual_keyboard_cmd, sizeof(m_virtual_keyboard_cmd));
			err_code = send(SEND_CONSUMER, buffer, sizeof(m_vol_up_cmd));
			PRINT_IF_ERROR("send()", err_code);
			memcpy(buffer, m_consumer_no_cmd, sizeof(m_consumer_no_cmd));
			err_code = send(SEND_CONSUMER, buffer, sizeof(m_vol_up_cmd));
			PRINT_IF_ERROR("send()", err_code);

			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_CTRL|APP_USBD_HID_KBD_MODIFIER_LEFT_ALT;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_F10;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			PRINT_IF_ERROR("send()", err_code);
		}

		if(key.search)
		{
			________________________________LOG_mComm_SendingFnHotKey(1,"WIN srch");
			// search: Window + "s"
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = WINDOW_KEY;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_S;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			PRINT_IF_ERROR("send()", err_code);
		}

		if(key.capture)
		{
			________________________________LOG_mComm_SendingFnHotKey(1,"WIN scrcpt");
			// capture: print screen
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_PRNTSC;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			PRINT_IF_ERROR("send()", err_code);
		}

		// 복구 처리
		if(key.release)
		{
			________________________________LOG_mComm_SendingFnHotKey(1,"WIN release");
			// step 1) release all keys
			memset(buffer, 0, sizeof(buffer));
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			PRINT_IF_ERROR("send()", err_code);
		}
		if (key.backward)
		{//Alt 백스페이스
			________________________________LOG_mComm_SendingFnHotKey(1,"WIN backward");
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_ALT;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_LFT;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		}	
		if(key.lockscreen)
		{
			________________________________LOG_mComm_SendingFnHotKey(1,"WIN lockscreen");
			// capture: Shift + CMD + ctrl + "3"
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = WINDOW_KEY;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_L;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			PRINT_IF_ERROR("send()", err_code);
		}
	
	}
  //20181102_flimbs: central 버튼 추가되면서 분기가 각 os 별로 나눠지도록 변경
	else if (M_COMM_CENTRAL_ANDROID == m_config.config.central[m_config.config.selected_ch])
	{//20181109_flimbs		
		if (key.home)
		{
			________________________________LOG_mComm_SendingFnHotKey(1,"AND home");
			/*
			DEB("AND:mouseCenter: home\n");			
			err_code = click_button_send(0, 0, CENTER_PRESS);
			err_code = click_button_send(0, 0, 0);
			*/
			uint8_t buf[M_COMM_CONSUMER_BUF_LEN] = {0,0,0};
			
			memcpy(buf, m_android_home, sizeof(m_android_home));										
			send(SEND_CONSUMER, buf, sizeof(m_android_home));
			
			nrf_delay_ms(50);

			memset(buf, 0, sizeof(m_android_home));
			send(SEND_CONSUMER, buf, sizeof(m_android_home));
		}
		if (key.capture)
		{
			________________________________LOG_mComm_SendingFnHotKey(1,"AND cptrue");
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_PRNTSC;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		}
		if (key.search)
		{
			________________________________LOG_mComm_SendingFnHotKey(1,"AND srch");
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = WINDOW_KEY;			
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			PRINT_IF_ERROR("send()", err_code);
			/*
			uint8_t buf[M_COMM_CONSUMER_BUF_LEN] = {0,0,0};
			
			memcpy(buf, m_android_srch, sizeof(m_android_srch));										
			send(SEND_CONSUMER, buf, sizeof(m_android_srch));
			
			

			memset(buf, 0, sizeof(m_android_srch));
			send(SEND_CONSUMER, buf, sizeof(m_android_srch));*/
		}
		if (key.swtapp)
		{//윈도키 탭
		________________________________LOG_mComm_SendingFnHotKey(1,"AND swtapp");
			uint8_t buf[M_COMM_CONSUMER_BUF_LEN] = {0,0,0};

			memcpy(buf, m_android_menu, sizeof(m_android_menu));										
			send(SEND_CONSUMER, buf, sizeof(m_android_menu));
			
			nrf_delay_ms(50);

			memset(buf, 0, sizeof(m_android_menu));
			send(SEND_CONSUMER, buf, sizeof(m_android_menu));

		}
		if (key.backward)
		{//윈도키 백스페이스
		/*
			DEB("AND:backward: win_backspc\n");
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = WINDOW_KEY;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_BS;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);*/
			________________________________LOG_mComm_SendingFnHotKey(1,"AND backward");
			uint8_t buf[M_COMM_CONSUMER_BUF_LEN] = {0,0,0};

			memcpy(buf, m_android_back, sizeof(m_android_back));										
			send(SEND_CONSUMER, buf, sizeof(m_android_back));
			
			nrf_delay_ms(50);

			memset(buf, 0, sizeof(m_android_back));
			send(SEND_CONSUMER, buf, sizeof(m_android_back));
		}
		if (key.vkbd)
		{
			________________________________LOG_mComm_SendingFnHotKey(1,"AND virtualKbd(none)");
			DEB("VKBD");
		}
		
		if(key.lockscreen)
		{
			________________________________LOG_mComm_SendingFnHotKey(1,"AND Lockscreen");
			uint8_t buf[M_COMM_CONSUMER_BUF_LEN] = {0,0,0};

			memcpy(buf, m_android_lock, sizeof(m_android_lock));										
			send(SEND_CONSUMER, buf, sizeof(m_android_lock));
			
			nrf_delay_ms(50);

			memset(buf, 0, sizeof(m_android_lock));
			send(SEND_CONSUMER, buf, sizeof(m_android_lock));
		}
		
	}
	else if (M_COMM_CENTRAL_IOS == m_config.config.central[m_config.config.selected_ch])
	{//20181109_flimbs		
		if(key.home)
		{
			________________________________LOG_mComm_SendingFnHotKey(1,"MAC home");
						
			uint8_t buf[M_COMM_CONSUMER_BUF_LEN] = {0,0,0};
			
			memcpy(buf, m_iphone_home, sizeof(m_iphone_home));										
			send(SEND_CONSUMER, buf, sizeof(m_iphone_home));
			nrf_delay_ms(50);
			memset(buf, 0, sizeof(m_iphone_home));
			send(SEND_CONSUMER, buf, sizeof(m_iphone_home));
			
			
		}		
		if(key.swtapp)
		{
			________________________________LOG_mComm_SendingFnHotKey(1,"swtapp home");
			
			
			// switching App: CMD + Tab
			
			uint8_t buf[M_COMM_CONSUMER_BUF_LEN] = {0,0,0};
			
			memcpy(buf, m_iphone_home, sizeof(m_iphone_home));										
			send(SEND_CONSUMER, buf, sizeof(m_iphone_home));
			nrf_delay_ms(100);
			memset(buf, 0, sizeof(m_iphone_home));
			send(SEND_CONSUMER, buf, sizeof(m_iphone_home));


/*
			nrf_delay_ms(150);



			memcpy(buf, m_iphone_home, sizeof(m_iphone_home));										
			send(SEND_CONSUMER, buf, sizeof(m_iphone_home));
			nrf_delay_ms(100);
			memset(buf, 0, sizeof(m_iphone_home));
			send(SEND_CONSUMER, buf, sizeof(m_iphone_home));*/

			
			
		}
		if(key.vkbd)
		{
			
			________________________________LOG_mComm_SendingFnHotKey(1,"MAC vkbd");
			// toggle virtual keyboard
			memcpy(buffer, m_virtual_keyboard_cmd, sizeof(m_virtual_keyboard_cmd));
			err_code = send(SEND_CONSUMER, buffer, sizeof(m_vol_up_cmd));
			PRINT_IF_ERROR("send()", err_code);
			memcpy(buffer, m_consumer_no_cmd, sizeof(m_consumer_no_cmd));
			err_code = send(SEND_CONSUMER, buffer, sizeof(m_vol_up_cmd));
			PRINT_IF_ERROR("send()", err_code);

			nrf_delay_ms(100);
			uint8_t buf[M_COMM_CONSUMER_BUF_LEN] = {0,0,0};

			memcpy(buf, m_iphone_vkbd, sizeof(m_iphone_vkbd));									
		}
		if(key.search)
		{
			
			________________________________LOG_mComm_SendingFnHotKey(1,"MAC search");
			// search: CMD + Space
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = MAC_CMD_KEY;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_SB;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			PRINT_IF_ERROR("send()", err_code);	
			
		}
		
		if(key.capture)
		{
			________________________________LOG_mComm_SendingFnHotKey(1,"MAC capture");
			// capture: Shift + CMD + ctrl + "3"
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = 
			APP_USBD_HID_KBD_MODIFIER_LEFT_SHIFT | MAC_CMD_KEY;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_3;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			PRINT_IF_ERROR("send()", err_code);
		}
		if(key.lockscreen)
		{			
			
			memcpy(buffer, m_iphone_lock, sizeof(m_iphone_lock));										
			send(SEND_CONSUMER, buffer, sizeof(m_iphone_lock));
			nrf_delay_ms(50);
			memset(buffer, 0, sizeof(m_iphone_lock));
			send(SEND_CONSUMER, buffer, sizeof(m_iphone_lock));
		}
		if (key.backward)
		{//Alt 백스페이스
			________________________________LOG_mComm_SendingFnHotKey(1,"WIN backward");
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = MAC_CMD_KEY;
			
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_LFT;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		}
		// 복구 처리
		if(key.release)
		{
			________________________________LOG_mComm_SendingFnHotKey(1,"MAC release");
			// step 1) release all keys
			memset(buffer, 0, sizeof(buffer));
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			PRINT_IF_ERROR("send()", err_code);
		}

	}	

	return err_code;
}


/*===========================================================================//
	name    : zoom_send
	author  : MJ
	caller  : 
		two_finger_handler
		update_zoom
	params  : 
		zoom
	returns : 
	object  : 		
	history : 
		20180000_MJ : 생성
		20190222_flimbs : 정리
//===========================================================================*/
static ret_code_t zoom_send(int8_t zoom)
{	
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t err_code = NRF_SUCCESS;

	uint8_t buffer[INPUT_REP_KEYS_MAX_LEN] = {0, }; // Clear all	
	if(M_COMM_CENTRAL_MAC == m_config.config.central[m_config.config.selected_ch])
	{						
		if(zoom > 0)  // Zoom in
		{						
			________________________________LOG_mComm_SendingGstr(6,"MAC zoom_in:%d",zoom);
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = MAC_CMD_KEY;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_PLUS;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			nrf_delay_ms(10);
			memset(buffer, 0, sizeof(buffer));
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		}
		else if(zoom < 0)
		{
			________________________________LOG_mComm_SendingGstr(6,"MAC zoom_out:%d",zoom);
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = MAC_CMD_KEY;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_U_SCORE;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			nrf_delay_ms(10);
			memset(buffer, 0, sizeof(buffer));
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		}				
		else // Zero
		{			
			________________________________LOG_mComm_SendingGstr(6,"MAC zoom_none:%d",zoom);
			memset(buffer, 0, sizeof(buffer));
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);			
		}		

		/*
		static bool sendFlag= false;
		if(sendFlag == false)
		{
			if(zoom > 0)  // Zoom in
			{						
				________________________________LOG_mComm_SendingGstr(6,"MAC zoom_in:%d",zoom);
				buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = MAC_CMD_KEY;
				buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_PLUS;
				err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
				nrf_delay_us(100);
				memset(buffer, 0, sizeof(buffer));
				err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			}
			else if(zoom < 0)
			{
				________________________________LOG_mComm_SendingGstr(6,"MAC zoom_out:%d",zoom);
				buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = MAC_CMD_KEY;
				buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_U_SCORE;
				err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
				nrf_delay_us(100);
				memset(buffer, 0, sizeof(buffer));
				err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			}
			sendFlag = true;
		}
		else // Zero
		{			
			//________________________________LOG_mComm_SendingGstr(6,"MAC zoom_none:%d",zoom);
			memset(buffer, 0, sizeof(buffer));
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			sendFlag = false;
		}		
		*/
	}
	else if((M_COMM_CENTRAL_MICROSOFT == m_config.config.central[m_config.config.selected_ch]) || (M_COMM_CENTRAL_OTHER == m_config.config.central[m_config.config.selected_ch]))
	{		
		if(zoom > 0) // Zoom in
		{
			________________________________LOG_mComm_SendingGstr(6,"WIN zoom_in:%d  + + + + + + + + + + + + + + + +",zoom);
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_CTRL;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			err_code = wheel_send(1, 0, WHEEL_DEFAULT_TRVDIST);

			//buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_KEYPAD_PLUS;
			//err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			//buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = 0;
			//err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);				
		} 
		else if(zoom < 0) // Zoom out
		{
			________________________________LOG_mComm_SendingGstr(6,"WIN zoom_out:%d --------------------------",zoom);
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_CTRL;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);			
			err_code = wheel_send(-1, 0, WHEEL_DEFAULT_TRVDIST);		
			//buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_KEYPAD_MINUS;
			//err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			//buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = 0;
			//err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);								
		}
		else // Zero
		{
			________________________________LOG_mComm_SendingGstr(6,"WIN zoom_none:%d |||||||||||||||||||||||||",zoom);			
			memset(buffer, 0, sizeof(buffer));
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			err_code = wheel_send(0, 0, WHEEL_DEFAULT_TRVDIST);
			//buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_SB;
			//err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			//buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = 0;
			//err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);			
			
		}
	}
	else if(M_COMM_CENTRAL_ANDROID == m_config.config.central[m_config.config.selected_ch])
	{
		if(zoom > 0) // Zoom in
		{
			________________________________LOG_mComm_SendingGstr(6,"WIN zoom_in:%d  + + + + + + + + + + + + + + + +",zoom);
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_CTRL;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			err_code = wheel_send(1, 0, WHEEL_DEFAULT_TRVDIST);

			//buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_KEYPAD_PLUS;
			//err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			//buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = 0;
			//err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);				
		} 
		else if(zoom < 0) // Zoom out
		{
			________________________________LOG_mComm_SendingGstr(6,"WIN zoom_out:%d --------------------------",zoom);
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_CTRL;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);			
			err_code = wheel_send(-1, 0, WHEEL_DEFAULT_TRVDIST);		
			//buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_KEYPAD_MINUS;
			//err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			//buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = 0;
			//err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);								
		}
		else // Zero
		{
			________________________________LOG_mComm_SendingGstr(6,"WIN zoom_none:%d |||||||||||||||||||||||||",zoom);			
			memset(buffer, 0, sizeof(buffer));
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			err_code = wheel_send(0, 0, WHEEL_DEFAULT_TRVDIST);
			//buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_SB;
			//err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			//buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = 0;
			//err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);			
		}
	}
	else if(M_COMM_CENTRAL_IOS == m_config.config.central[m_config.config.selected_ch])
	{
		if(zoom > 0)  // Zoom in
		{						
			________________________________LOG_mComm_SendingGstr(6,"MAC zoom_in:%d",zoom);
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = MAC_CMD_KEY;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_PLUS;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			nrf_delay_ms(10);
			memset(buffer, 0, sizeof(buffer));
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		}
		else if(zoom < 0)
		{
			________________________________LOG_mComm_SendingGstr(6,"MAC zoom_out:%d",zoom);
			buffer[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = MAC_CMD_KEY;
			buffer[DRV_KEYBOARD_PACKET_KEY_INDEX] = APP_USBD_HID_KBD_U_SCORE;
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
			nrf_delay_ms(10);
			memset(buffer, 0, sizeof(buffer));
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
		}				
		else // Zero
		{			
			________________________________LOG_mComm_SendingGstr(6,"MAC zoom_none:%d",zoom);
			memset(buffer, 0, sizeof(buffer));
			err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);			
		}	
	}

	return err_code;
}

//=========================================================================================================
//			Function name : clear_keyboard()
//=========================================================================================================
static ret_code_t clear_keyboard(void)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t err_code;

	DEB("clear_keyboard: BEGIN\n");

	//  release all keys
	uint8_t buffer[INPUT_REP_KEYS_MAX_LEN] = {0, }; // Clear all
	
	err_code = send(SEND_KEY, buffer, INPUT_REP_KEYS_MAX_LEN);
	PRINT_IF_ERROR("send()", err_code);

	DEB("clear_keyboard: END\n");
	return err_code;
}

//=========================================================================================================
//			Function name : send()
//=========================================================================================================
ret_code_t send(send_type_t type, uint8_t *p_data, uint8_t len)
{
	________________________________LOG_mComm_FuncHeads(0,"");
    uint8_t  	rep_index;
    ret_code_t 	rv = NRF_ERROR_INVALID_STATE;
	uint8_t		enque_flag = false;

	if(is_BT_channel(m_config.config.selected_ch))
	{
		switch(type)
		{
			case SEND_KEY : 
				#ifdef HID_INPUT_REPORT_TRACE
				SYSTEM_MSG("send:KEY:");
				#endif
				rep_index = INPUT_REP_KEYBOARD_INDEX;
				enque_flag = true;
				break;

			case SEND_TOUCH_BUTTON : 
				#ifdef HID_INPUT_REPORT_TRACE
				SYSTEM_MSG("send:TOUCH_BUTTON:");
				#endif
				rep_index = INPUT_REP_BUTTONS_INDEX;
				enque_flag = false;
				break;

#if (!MOKIBO_KEYBOARD_ONLY)
			case SEND_TOUCH_MOVE : 
				#ifdef HID_INPUT_REPORT_TRACE
				SYSTEM_MSG("send:TOUCH_MOVE:");
				#endif
				rep_index = INPUT_REP_MOVEMENT_INDEX;
				enque_flag = false;
				break;

			case SEND_CONSUMER : 
				#ifdef HID_INPUT_REPORT_TRACE
				SYSTEM_MSG("send:CONSUMER:");
				#endif
				rep_index = INPUT_REP_CONSUMER_INDEX;
				enque_flag = true;
				break;
#endif
			default:
				#ifdef HID_INPUT_REPORT_TRACE
				SYSTEM_MSG("send: invalid:(%d):",type);
				#endif
				return NRF_ERROR_INVALID_PARAM;
				//break; unreachable
		}

		if( (m_conn_handle != BLE_CONN_HANDLE_INVALID) &&
			(M_COMM_ADV_STATUS_CONNECTED <= m_comm_adv_get_status()) &&
			(M_COMM_ADV_BOND_INVALID != m_comm_adv_get_addr(m_config.config.selected_ch)) &&
			(M_COMM_ADV_BOND_READY != m_comm_adv_get_addr(m_config.config.selected_ch)) )
		{

		if(BUFFER_LIST_EMPTY())
		{
			rv = ble_hids_inp_rep_send(&m_hids, rep_index, len, p_data);
			if(NRF_SUCCESS != rv)
			{
				if(enque_flag == true)
			{
				buffer_enqueue(&m_hids, rep_index, p_data, len);
				DEB("#UnknownError: send(),buffer_enqueue type=%d, len=%d\n", type, len);
				DEB1("#UnknownError[L:%d]: send(),buffer_enqueue type=%d, len=%d\n", __LINE__, type, len);
				}
			}
			else
			{
				DEB1("[%s] send data=0x%x\n", __func__, p_data[2]);
			}
		}
		else
		{
			buffer_enqueue(&m_hids, rep_index, p_data, len);
				DEB("#UnknownError: send(),buffer_enqueue type=%d, len=%d\n", type, len);
				DEB1("#UnknownError[L:%d]: send(),buffer_enqueue type=%d, len=%d, data=0x%x\n", __LINE__, type, len, p_data[2]);
			buffer_dequeue(true);
		}
	}
		else
		{
			if( 
				(	(m_conn_handle == BLE_CONN_HANDLE_INVALID) &&
					(M_COMM_ADV_STATUS_CONNECTED <= m_comm_adv_get_status()) &&
					(M_COMM_ADV_BOND_INVALID != m_comm_adv_get_addr(m_config.config.selected_ch)) &&
					(M_COMM_ADV_BOND_READY != m_comm_adv_get_addr(m_config.config.selected_ch))
				) 
				|| (M_COMM_ADV_STATUS_IN_ADV == m_comm_adv_get_status())	
			)
			{
				if(enque_flag == true)
				{
					buffer_enqueue(&m_hids, rep_index, p_data, len);
					DEB("#UnknownError[L:%d]: send(),buffer_enqueue type=%d, len=%d, data=0x%x\n", __LINE__, type, len, p_data[2]);
					DEB("DATA: %x, %x, %x, %x, %x, %x\n", p_data[0], p_data[1], p_data[2], p_data[3], p_data[4], p_data[5]);
				}
				return rv;
			}
		}
	}
#if 0 ////////////////////////// delete dongle //////////////////////////////
	else if(is_DONGLE_channel(m_config.config.selected_ch))
	{
		//
		rv = m_dongle_send(type, p_data, len);

		if(NRF_ERROR_INTERNAL == rv)
		{
			DEB("#Error: send() via Dongle: m_dongle_send(type=%d, p_data, len=%d) \n", type, len);
			//m_led_set_status(M_LED_SLOW_BLINK);
		}
		else
		{
			//m_led_set_status(M_LED_ON);
		}
	}
#endif ///////////////////////////// delete dongle /////////////////////////
	else
	{
		DEB("#Error: send() SelectedCh=%d ?\n", m_config.config.selected_ch);
		DEB1("#Error[L:%d]: send() SelectedCh=%d ?\n", __LINE__, m_config.config.selected_ch);
	}

	if(NRF_SUCCESS != rv)
	{
		DEB("#UnknownError: send()... rv=%d, type=%d, len=%d\n", rv, type, len);
	}

	return rv;
}

//=========================================================================================================
//			Function name : passkey_handle()
//=========================================================================================================
extern void debshow_passkey(const char *msg, passkey_t passkey);

static ret_code_t passkey_handle(uint8_t *p_data, uint8_t len)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	uint8_t i, j;
	ret_code_t rv = NRF_SUCCESS;

	#if defined(M_COMM_PASSKEY_TRACE)
		DEB("passkey_handle: BEGIN\n");
	#endif

	for( i=0; i<len; i++)
	{
		for(j=0; j<sizeof(m_passkey_table)/sizeof(hid_to_ascii_t); j++)
		{
			if(m_passkey_table[j].hid == p_data[i])
			{
				if(m_passkey.index < BLE_GAP_PASSKEY_LEN)
				{
					m_passkey.keys[m_passkey.index] = m_passkey_table[j].ascii;
					m_passkey.index++;
				}
			}
		}
		if(APP_USBD_HID_KBD_ENTER == p_data[i])
		{
			m_passkey.is_enter = true;
		}
	}

	#if defined(M_COMM_PASSKEY_TRACE)
		DEB("passkey_handle: ");
		debshow_passkey("",m_passkey);
	#endif

	if(m_passkey.is_enter)
	{
		#if defined(M_COMM_PASSKEY_TRACE)
			DEB("passkey_handle: sd_ble_gap_auth_key_reply()\n");
		#endif
		//
		// see: ble_gap.h  (BLE_GAP_PASSKEY_LEN, BLE_GAP_AUTH_KEY_TYPE_PASSKEY, BLE_GAP_AUTH_KEY_TYPE_NONE, etc)
		//
		rv = sd_ble_gap_auth_key_reply(m_conn_handle, BLE_GAP_AUTH_KEY_TYPE_PASSKEY, m_passkey.keys); // 6-key authentication
		//rv = sd_ble_gap_auth_key_reply(m_conn_handle, BLE_GAP_AUTH_KEY_TYPE_NONE, m_passkey.keys);	// none
		//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(rv);
		
		// Clear
		#if defined(M_COMM_PASSKEY_TRACE)
			DEB("passkey_handle: clear m_passkey \n");
		#endif
		memset(&m_passkey, 0, sizeof(passkey_t));

	}

	#if defined(M_COMM_PASSKEY_TRACE)
		DEB("passkey_handle: END\n");
	#endif
	return rv;
}

/**@brief Function for initializing the Advertising functionality.
 */

//=========================================================================================================
//			Function name : m_comm_init()
//=========================================================================================================
ret_code_t m_comm_init(void)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	//<-- clear하면 않된다 !!
	//memset(&m_passkey, 0, sizeof(passkey_t));				//

	//<-- clear하면 않된다 !!
	// memset(&m_config, 0, sizeof(m_comm_config_t));			// use RestoreInfoFromFlash()<--
	
	//<-- clear하면 않된다 !!
	//memset(&m_adv_config, 0, sizeof(m_comm_adv_config_t));	// use RestoreInfoFromFlash()<--Stanley added ( <--- m_comm_adv_init()안에서 초기화하는 것을 막고 )

	// Get configuration
//	if(sizeof(m_comm_config_t)>DRV_FDS_M_COMM_CONFIG_SIZE)
//	{
//		DEB("ERROR: m_comm_config_t(%d) > DRV_FDS_M_COMM_CONFIG_SIZE(%d)", sizeof(m_comm_config_t), DRV_FDS_M_COMM_CONFIG_SIZE);
//	}
//	//ret_code_t err_code = drv_fds_read(DRV_FDS_PAGE_M_COMM_CONFIG, (uint8_t *)&m_config, sizeof(m_config));
//	//PRINT_IF_ERROR("drv_fds_read()", err_code);
//	rv = fds_read_stanley( FDS_FILE_ID_STANLEY,FDS_RECORD_KEY_M_COMM_CONFIG, sizeof(m_config), (uint8_t *)&m_config);
//--> main.c/config_init()으로 이동 <--

	// Initializing the buffer queue used to key events
	buffer_init();

	// Initialize SoftDevice
	ble_stack_init();

//	DEB("m_comm_init: m_dongle_init()\n");
//	m_dongle_init();
	
	//gap_params_init();
	gap_params_init(m_config.config.selected_ch);
	
	gatt_init();
	
	// m_comm_adv_init() 안에 있는 다음 함수호출을 막고, 'RestoreInfoFromFlash()'로 대체함.
	//	<--- rv = fds_read_stanley(FDS_FILE_ID_STANLEY, FDS_RECORD_KEY_M_COMM_ADV_CONFIG, sizeof(m_adv_config), (uint8_t *)&m_adv_config);
	m_comm_adv_init();


	#if	(MOKIBO_DFU_ENABLED)
		DEB("m_comm_init: dfu_init()\n");
		dfu_init();
	#endif	

	services_init();
	
	conn_params_init();
	
	#if MOKIBO_NFC_ENABLED	
		DEB("m_comm_init: m_nfc_init()\n");
		m_nfc_init();
	#endif	

	//
	//debshow_comm_config("m_comm_init: m_config:", m_config);
	
	//
	switch(m_config.config.selected_ch)
	{
		case M_COMM_CHANNEL_BT1 :
			m_led_set_color(BT1_MODE_COLOR);
			break;

		case M_COMM_CHANNEL_BT2 :
			m_led_set_color(BT2_MODE_COLOR);
			break;

		case M_COMM_CHANNEL_BT3 :
			m_led_set_color(BT3_MODE_COLOR);
			break;

//		case M_COMM_CHANNEL_DONGLE :
//			m_led_set_color(DONGLE_MODE_COLOR);
//			break;

		case M_COMM_CHANNEL_MAX :
			m_led_set_color(NO_CHANNEL_MODE_COLOR);
			break;

		default : 
			m_led_set_color(NO_CHANNEL_MODE_COLOR);
			break;
	}
    return NRF_SUCCESS;
}


//=========================================================================================================
//			Function name : m_comm_get_mode()
//=========================================================================================================
m_comm_mode_t m_comm_get_mode(void)
{
	//________________________________LOG_mComm_FuncHeads(0,""); main_powermanage() 에서 polling 한다
	return m_comm_mode;
}
//---------------------------------------------------------------------------------------------------------
//			GetStr_m_comm_cmd
//---------------------------------------------------------------------------------------------------------
char *GetStr_m_comm_cmd(m_comm_cmd_type_t cmd)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	switch(cmd)
	{
		case M_COMM_CMD_START: return "M_COMM_CMD_START";
		case M_COMM_CMD_ERASE_BONDS: return "M_COMM_CMD_ERASE_BONDS";
		case M_COMM_CMD_CH_CHANGE: return "M_COMM_CMD_CH_CHANGE";
		case M_COMM_CMD_NEW_BOND: return "M_COMM_CMD_NEW_BOND";
		case M_COMM_CMD_STOP: return "M_COMM_CMD_STOP";
		default: return "UNKNOWN CMD";
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			soft_comm_init
//-----------------------------------------------------------------------------------------------------------------------------------
void soft_comm_init(void)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t ret;
	
//	//-----------------------------------------------------------------------------------------------------------------------------------
//	// Init timer module
//	DEB("soft_comm_init: m_timer_init()\n");
// 	ret = m_timer_init(); APP_ERROR_CHECK(ret);
	
//	//-----------------------------------------------------------------------------------------------------------------------------------
//	// Init touch module
//	DEB("soft_comm_init: m_timer_init()\n");
// 	ret = m_touch_init();	APP_ERROR_CHECK(ret);
	
//	//-----------------------------------------------------------------------------------------------------------------------------------
//	// Init Keyboard module
//	DEB("soft_comm_init: m_keyboard_init()\n");
//	ret = m_keyboard_init(); //APP_ERROR_CHECK(ret);

	//-----------------------------------------------------------------------------------------------------------------------------------
	// Init Event module
//	DEB("soft_comm_init: m_event_init()\n");
//	ret = m_event_init();	APP_ERROR_CHECK(ret);

	//-----------------------------------------------------------------------------------------------------------------------------------
	// Init main module
	// <--- Watchdog 초기화가 들어 있으나 이를 막고 main loop 직전으로 옮김.
//	DEB("soft_comm_init: m_mokibo_init()\n");
//	ret = m_mokibo_init();	APP_ERROR_CHECK(ret);
	
	//-----------------------------------------------------------------------------------------------------------------------------------
	// Init Comm module
	DEB("soft_comm_init: m_comm_init()\n");
	ret = m_comm_init();	
	//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(ret);
	
}

//---------------------------------------------------------------------------------------------------------
//		Dongle킬ㄹ 누른 후 프로그램까지의 전달 과정
//---------------------------------------------------------------------------------------------------------
#if 0
m_event.c: keyboard_event_detect()
{
	hoykey = drv_keyboard_get_hotkey();
	//
	if(hotkey & DRV_KEYBOARD_HOTKEY_BT1)
		... m_event |= M_EVENT_CHANNEL_CHANGED;
		... m_arg.channel.bt1 = 1

	if(hotkey & DRV_KEYBOARD_HOTKEY_BT2)
		... m_event |= M_EVENT_CHANNEL_CHANGED;
		... m_arg.channel.bt2 = 1

	if(hotkey & DRV_KEYBOARD_HOTKEY_BT3)
		... m_event |= M_EVENT_CHANNEL_CHANGED;
		... m_arg.channel.bt3 = 1

	if(hotkey & DRV_KEYBOARD_HOTKEY_DONGLE)
		... m_event |= M_EVENT_CHANNEL_CHANGED;
		... m_arg.channel.dongle = 1

	//if(hotkey & DRV_KEYBOARD_HOTKEY_DONGLE_PSEUDO)
	//>> Flash에 저장 후 다음과 같이 설정하면, mokibo.c:/KeyboardHandler()에서
	//   m_comm_cmd(M_COMM_CMD_CH_CHANGE, M_COMM_CHANNEL_DONGLE_PSEUDO)를 호출하게 된다.
		... m_event |= M_EVENT_CHANNEL_CHANGED;
		... m_arg.channel.dongle_pseudo = 1

	if(hotkey & DRV_KEYBOARD_HOTKEY_RESET)
		reset_mokibo();

	if(hoykey & DRV_KEYBOARD_HOTKEY_BTRCHK)
}
//---------------------------------------------------------------------------------------------------------
mokibo.c: KeyboardHandler(evt, argv)
{
	if(evt & M_EVENT_CHANNEL_CHANGED)
	{
		if(argv.channel.bt1) 			{ m_comm_cmd(M_COMM_CMD_CH_CHANGE, M_COMM_CHANNEL_BT1); }
		if(argv.channel.bt2) 			{ m_comm_cmd(M_COMM_CMD_CH_CHANGE, M_COMM_CHANNEL_BT2); }
		if(argv.channel.bt3) 			{ m_comm_cmd(M_COMM_CMD_CH_CHANGE, M_COMM_CHANNEL_BT3); }
//		if(argv.channel.dongle)			{ m_comm_cmd(M_COMM_CMD_CH_CHANGE, M_COMM_CHANNEL_DONGLE); }
		if(argv.channel.dongle_pseudo)	{ m_comm_cmd(M_COMM_CMD_CH_CHANGE, M_COMM_CHANNEL_DONGLE_PSEUDO); }
	}
	...
}
//---------------------------------------------------------------------------------------------------------
#endif // Dongle킬ㄹ 누른 후 프로그램까지의 전달 과정
//---------------------------------------------------------------------------------------------------------

	

//=========================================================================================================
//			m_comm_cmd
//=========================================================================================================
ret_code_t m_comm_cmd(m_comm_cmd_type_t cmd, uint32_t argv)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t rv = NRF_SUCCESS;


	#ifdef M_COMM_CH_TRACE
		DEB("\n\n\n");
		DEB("############################## M_COMM_CMD: %d=(%s) ###############################\n",
			cmd, GetStr_m_comm_cmd(cmd) 
		);
	#endif
	
	switch(cmd)
	{
		// BT mode에서만, keyboard누름으로써 발생함
		case M_COMM_CMD_NEW_BOND : 
			rv= onCommCmdNewBond(argv);
			//
			break;
		
		// channel to be changed
		case M_COMM_CMD_CH_CHANGE :
			rv= onCommCmdChChange(m_config.config.selected_ch, argv);
			//
			break;

		case M_COMM_CMD_ERASE_BONDS :
			// TODO: Add configuration setting to default
			// 예를 들면, 	마지막 선택 채널 ===> M_COMM_CHANNEL_MAX
			// 			adv config 값 초기화 등등
			rv= onCommCmdEraseBonds();
			break;

		// maybe for testing !
		case M_COMM_CMD_START :	 // advertisement
			rv= onCommCmdStart(m_config.config.selected_ch);
			break;

		case M_COMM_CMD_STOP : 
			// mokibo가 sleep mode등으로 들어갈 때, 현재의 연결을 해제한다.
			rv= onCommCmdStop(m_config.config.selected_ch);
			break;

		default :
			
			break;
	}

	//-----------------------------------------------------------------------------------------------------------------------------------
    return rv;
}
//---------------------------------------------------------------------------------------------------------
//			onCommCmdStart
//---------------------------------------------------------------------------------------------------------
ret_code_t onCommCmdStart(m_comm_channel_type_t selected)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t rv=NRF_SUCCESS;

	//DEB("onCommCmdStart: BEGIN - sel_ch(%d)\n", selected);
	
	//switch(m_config.config.selected_ch)
	switch(selected)
	{
		case M_COMM_CHANNEL_BT1 :
			m_led_set_color(BT1_MODE_COLOR);
			//DEB("onCommCmdStart: m_comm_adv_start(BT1)\n");
			rv = m_comm_adv_start(M_COMM_ADV_CHANNEL_BT1);	PRINT_IF_ERROR("m_comm_adv_start()", rv);
			break;
		
		case M_COMM_CHANNEL_BT2 :
			m_led_set_color(BT2_MODE_COLOR);
			//DEB("onCommCmdStart: m_comm_adv_start(BT2)\n");
			rv = m_comm_adv_start(M_COMM_ADV_CHANNEL_BT2);	PRINT_IF_ERROR("m_comm_adv_start()", rv);
			break;
		
		case M_COMM_CHANNEL_BT3 :
			m_led_set_color(BT3_MODE_COLOR);
			//DEB("onCommCmdStart: m_comm_adv_start(BT3)\n");
			rv = m_comm_adv_start(M_COMM_ADV_CHANNEL_BT3);	PRINT_IF_ERROR("m_comm_adv_start()", rv);
			break;
		
//		case M_COMM_CHANNEL_DONGLE :
//			m_led_set_color(DONGLE_MODE_COLOR);
//			DEB("onCommCmdStart: m_comm_adv_start(DONGLE)\n");
//			rv = m_dongle_start();	PRINT_IF_ERROR("m_dongle_start()", rv);
//			break;
		
		case M_COMM_CHANNEL_MAX :
		default : 
			m_led_set_color(NO_CHANNEL_MODE_COLOR);
			//DEB("onCommCmdStart: INVALID\n");
			break;
	}

	//DEB("onCommCmdStart: END\n");
	return rv;
}
//---------------------------------------------------------------------------------------------------------
//			onCommCmdEraseBonds
//---------------------------------------------------------------------------------------------------------
// TODO: Add configuration setting to default
// 예를 들면  
// 마지막 선택 채널 ===> M_COMM_CHANNEL_MAX
// adv config 값 초기화 등등
ret_code_t onCommCmdEraseBonds(void)
{	
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t rv= NRF_SUCCESS;

	//-----------------------------------------------------------------------------------------------------------------------------------

	//DEB("onCommCmdEraseBonds: m_comm_adv_disconnect()\n");
	m_comm_adv_disconnect();

	//DEB("onCommCmdEraseBonds: m_comm_set_config_default()\n");
	rv = m_comm_set_config_default();	PRINT_IF_ERROR("m_comm_set_config_default()", rv);
	
	//DEB("onCommCmdEraseBonds: m_comm_adv_clear_bonds()\n");
	rv = m_comm_adv_clear_bonds();	PRINT_IF_ERROR("m_comm_adv_clear_bonds()", rv);
	
#if 0 // BLOCK_MJ_FDS by Stanley
	drv_fds_save_immediately();
#endif
	
	//
	//DEB("onCommCmdEraseBonds: END\n");
	return rv;
}


//---------------------------------------------------------------------------------------------------------
//			onCommCmdChChange
//---------------------------------------------------------------------------------------------------------
// 채널 변환 시나리오:
//	(1) 동일 host에서는 (BT던 Dongle이던) 오로지 하나의 채널만 허용된다.
//	(2) BT에서 Dongle로 전환하는 경우에는, Flash상의 selected_ch을 UNKNOWN(즉, M_COMM_CHANNEL_MAX)로 저장한다.
//	(3) 그 외의 경우에는, Flash상의 selected_ch을 제대로 저장한다.
//	(4) (Power On) Reset 시에는,
//		a. Flash에서 읽은 selected_ch이 UNKNOW이면(즉, BT1,BT2,BT3가 아니면), Dongle로 연결(시도)한다.
//		b. Flash에서 읽은 selected_ch이 BT이면(즉, BT1,BT2,BT3이면), BT로 연결(시도)한다.
//
ret_code_t onCommCmdChChange(uint8_t selected_ch_old, uint8_t selected_ch_new)
{
	//----------------------------------------------------------------------------------------------------------------------
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t ret= NRF_SUCCESS;

	//-----------------------------------------------------------------------------------------------------------------------------------
	//DEB("onCommCmdChChange: BEGIN: %d-->%d\n",selected_ch_old,selected_ch_new);
	
	//----------------------------------------------------------------------------------------------------------------------
	// 	BT1,BT2,BT3,DONGLE이 아니면 ... 바뀔 필요 없음...
	if( !is_VALID_channel(selected_ch_new) )
	{
		#if defined(M_COMM_CH_TRACE)
			DEB("\t CommChange: NO NEED (invalid channel= %d\n", selected_ch_new);
		#endif
		return NRF_ERROR_INTERNAL;
	}

	//----------------------------------------------------------------------------------------------------------------------
	// 	채널이 동일하면.... 바뀔 필요 없음...(refresh)
	//----------------------------------------------------------------------------------------------------------------------
	if(selected_ch_old == selected_ch_new)
	{
		DEB("onCommCmdChChange: no_change: %d \n", selected_ch_new);
		//----------------------------------------------------------------------------------------------------------------------
		if(is_BT_channel(selected_ch_new))
		{
			ret=Refresh_BT(selected_ch_new);
		}
		//----------------------------------------------------------------------------------------------------------------------
//		if(is_DONGLE_channel(selected_ch_new))
//		{
//			ret=Refresh_DONGLE(selected_ch_new);
//		}
	
		//----------------------------------------------------------------------------------------------------------------------
		// M_COMM_CMD_CH_CHANGE 이벤트는 키보드 모드에서만 발생함
		//DEB("onCommCmdChChange: m_mokibo_set_touch_color() \n");
		m_mokibo_set_touch_color();

		DEB("onCommCmdChChange: m_led_set_status(M_LED_DIMMING_OUT) \n");
		m_led_set_status(M_LED_DIMMING_OUT);

		//----------------------------------------------------------------------------------------------------------------------
		return ret;
	}

	//----------------------------------------------------------------------------------------------------------------------
	// 	채널이 다르면.... 바뀌어야 함
	//----------------------------------------------------------------------------------------------------------------------
	if( is_BT_channel(selected_ch_old) )
	{// ?댁쟾 梨꾨꼸: BT
		// Clear Buffered Data
		buffer_init();

		//----------------------------------------------------------------------------------------------------------------------
		// BT 연결? advertising? 이 완성되지 않은 상태에서 채널이 바뀌면 --> 해당 채널 주소 초기화
		if(selected_ch_old != selected_ch_new)
		{
			if(M_COMM_ADV_STATUS_IN_ADV == m_comm_adv_get_status())
			{
				sd_ble_gap_adv_stop();
			}
			
			if(M_COMM_ADV_BOND_READY == m_comm_adv_get_addr( (m_comm_adv_channel_type_t)selected_ch_old ) )
			{
				DEB("onCommCmdChChange: m_comm_adv_set_addr(ch=%d, INVALID) \n", selected_ch_old);
				m_comm_adv_set_addr( (m_comm_adv_channel_type_t)selected_ch_old, M_COMM_ADV_BOND_INVALID );

				DEB("onCommCmdChChange: m_led_set_status(M_LED_DIMMING_OUT) \n");
				m_led_set_status(M_LED_DIMMING_OUT);
			}
		}
		
		//----------------------------------------------------------------------------------------------------------------------
		if( is_BT_channel(selected_ch_new) )
		{// 이전 채널: BT, 이후 채널: BT
			ret=Change_BT_to_BT_before(selected_ch_old, selected_ch_new);

			// update the channel changed
			m_config.config.selected_ch= selected_ch_new;
			//
			ret=Change_BT_to_BT_after(selected_ch_old, selected_ch_new);
			//
			//DEB("m_comm_cmd: fds_RequestToSaveAfterThisTime(%d,%d) .... maybe saved far later...\n",selected_ch_new,SAVEDELAY_BT_TO_BT);
			fds_RequestToSaveAfterThisTime(selected_ch_new,SAVEDELAY_BT_TO_BT);
		} 
#if 0 ///////////////////////////////////// delete dongle ///////////////////////
		//----------------------------------------------------------------------------------------------------------------------
		else if(is_DONGLE_channel(selected_ch_new))
		{// 이전 채널: BT, 이후 채널: Dongle

			// update the channel changed
			m_config.config.selected_ch= selected_ch_new;
			//
			//Unknown 채널을 기억함. 차후 dongle 모드 중에 power down 되는 경우 --> dongle에서 시작할 수 있도록 하기 위함
			//	(1) Ddngle로 넘어가기 전에 '확실하게' 먼저 저장하기 위해서는, Flash 저장이 완료된 것을 확인하여야 한다.
			//	(2) 저장이 완료되면 FDS_EVT_WRITE/FDS_EVT_UPDATE로 이어짐,
			fds_RequestToSaveAfterThisTime(M_COMM_CHANNEL_MAX,SAVEDELAY_BT_TO_DONGLE);
			//	(2) FDS Event에서 추가적으로, 저장이 확인 되었으므로, 
			//		a. Dongle로 전환하는 경우에는--> ChangeToDongle_delayed()를 호출
			//		b. ChangeToDongle_delayed()에서 다시 호출: fds_RequestToCallDongle_AfterThisTime("OnFdsWrite:", 100);
			//		c. delay후 호출: Change_to_DONGLE(M_COMM_CHANNEL_DONGLE);
		}
#endif /////////////////////////////////// delete dongle /////////////////////////////
		
	}
	
	

#if 0 ///////////////////////////////// delete dongle //////////////////////////////////	
	else if( is_DONGLE_channel(selected_ch_old) )
	{// 이전 채널: Dongle

		//----------------------------------------------------------------------------------------------------------------------
		if( is_BT_channel(selected_ch_new) )
		{// 이전 채널: Dongle, 이후 채널: BT
			//
			ret=Change_DONGLE_to_BT_before(selected_ch_old, selected_ch_new);

			// update the channel changed
			m_config.config.selected_ch= selected_ch_new;
			//
			ret=Change_DONGLE_to_BT_after(selected_ch_old, selected_ch_new);

			// 새로운 채널을 기억함. 연결에 필요한 충분한 시간 이후에 저장해야 context가 제대로 저장될 수 있음.
			fds_RequestToSaveAfterThisTime(selected_ch_new,SAVEDELAY_DONGLE_TO_BT);
			
		}
		else if( is_DONGLE_channel(selected_ch_new) )
		{// 이전 채널: Dongle, 이후 채널: Dongle
			//
			ret= Refresh_DONGLE(selected_ch_new);
		}
	}
#endif //////////////////////////////////// delete dongle //////////////////////////////////	
	
	// Stanley added.
	else
	{// 이전 채널: Invalid
		//----------------------------------------------------------------------------------------------------------------------
		if( is_BT_channel(selected_ch_new) )
		{// 이전 채널: Invalid, 이후 채널: BT
			//
			ret=Change_INVALID_to_BT_before(selected_ch_new);
			// update the channel changed
			m_config.config.selected_ch= selected_ch_new;
			//
			ret=Change_INVALID_to_BT_after(selected_ch_new);

			// 새로운 채널을 기억함. 연결에 필요한 충분한 시간 이후에 저장해야 context가 제대로 저장될 수 있음.
			fds_RequestToSaveAfterThisTime(selected_ch_new,SAVEDELAY_INVALID_TO_BT);
			
		}
#if 0 /////////////////////////////////////  delete dongle //////////////////////////////		
		else if( is_DONGLE_channel(selected_ch_new) )
		{// 이전 채널: Invalid, 이후 채널: Dongle

			// update the channel changed
			m_config.config.selected_ch= selected_ch_new;

			// Unknown 채널을 기억함. 차후 dongle 모드 중에 power down 되는 경우 --> dongle에서 시작할 수 있도록 하기 위함
			// --> 이미 Unknow이기 때문에 액션 필요 없음.
			// --> 저장 과정 없이 바로 전환함.
			ret= Change_to_DONGLE(selected_ch_new);
		}
#endif ////////////////////////////// delete dongle /////////////////////////////////////////
		
	}
	
	//----------------------------------------------------------------------------------------------------------------------
	// M_COMM_CMD_CH_CHANGE 이벤트는 키보드 모드에서만 발생함
	//DEB("onCommCmdChChange: m_mokibo_set_touch_color() \n");
	m_mokibo_set_touch_color();

	DEB("onCommCmdChChange: m_led_set_status(M_LED_DIMMING_OUT) \n");
	m_led_set_status(M_LED_DIMMING_OUT);

	//----------------------------------------------------------------------------------------------------------------------
	//DEB("onCommCmdChChange: END: %d-->%d\n",selected_ch_old,selected_ch_new);
	return ret;
}
//----------------------------------------------------------------------------------------------------------------------
//			Change_BT_to_BT_before
//----------------------------------------------------------------------------------------------------------------------
ret_code_t Change_BT_to_BT_before(uint8_t ch_old, uint8_t ch_new)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t ret=NRF_SUCCESS;

	if( M_COMM_ADV_BOND_INVALID != m_comm_adv_get_addr(ch_old)
		&& M_COMM_ADV_BOND_READY != m_comm_adv_get_addr(ch_old)
		&& m_comm_ble_get_status() != BLE_CONN_HANDLE_INVALID )
	{
		sd_ble_gap_disconnect(m_comm_ble_get_status(), BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
	}
				
	m_comm_adv_set_status(M_COMM_ADV_STATUS_READY);
	
	//----------------------------------------------------------------------------------------------------------------------
	return ret;
}
//----------------------------------------------------------------------------------------------------------------------
//			Change_BT_to_BT_after
//----------------------------------------------------------------------------------------------------------------------
ret_code_t Change_BT_to_BT_after(uint8_t ch_old, uint8_t ch_new)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t ret=NRF_SUCCESS;

	//———————————————————————————————————————————————————————————
	if(ch_old == ch_new)
	{
		DEB("Change_BT_to_BT_after: do-nothing : channel(%d) not changed.\n",ch_old);
		return ret;
	}

	//———————————————————————————————————————————————————————————
	// BT ———————————————————————————————————————————————————————————> BT ?´ë?ë¡?m_dongle_stop()???„ìš” ?†ë‹¤.
	//ret = m_dongle_stop();	PRINT_IF_ERROR("m_dongle_stop()", ret);
	
	if( M_COMM_ADV_BOND_INVALID != m_comm_adv_get_addr(ch_new)
		&& M_COMM_ADV_BOND_READY != m_comm_adv_get_addr(ch_new) )
	{
		//———————————————————————————————————————————————————————————
		// m_mokibo_name[ch_new]??device name?¼ë¡œ ?¤ì •
		DEB("Change_BT_to_BT_after: update_device_name(%d)\n",ch_new);
		update_device_name(ch_new); // --> sd_ble_gap_device_name_set('MOKIBO BTn') 

		DEB("Change_BT_to_BT_after: (ch=%d, peer_id=%d) \n", ch_new, m_adv_config.config[ch_new].id);

		//———————————————————————————————————————————————————————————
		DEB("Change_BT_to_BT_after: m_comm_adv_start() \n");
		ret = m_comm_adv_start((m_comm_adv_channel_type_t)ch_new); PRINT_IF_ERROR("m_comm_adv_start", ret);
	}
	
	//———————————————————————————————————————————————————————————
	//DEB("Change_BT_to_BT_after: m_led_set_status() \n");
	m_led_set_status(M_LED_DIMMING_OUT);

	//———————————————————————————————————————————————————————————
	// Stanley added
	//DEB("Change_BT_to_BT_after: m_comm_mode=BLE\n");
	m_comm_mode = M_COMM_MODE_BLE;
	
	return ret;
}

//----------------------------------------------------------------------------------------------------------------------
//			Change_INVALID_to_BT_before
//----------------------------------------------------------------------------------------------------------------------
ret_code_t Change_INVALID_to_BT_before(uint8_t ch_new)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	return NRF_SUCCESS;
}
//----------------------------------------------------------------------------------------------------------------------
//			Change_INVALID_to_BT_after
//----------------------------------------------------------------------------------------------------------------------
ret_code_t Change_INVALID_to_BT_after(uint8_t ch_new)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	return Change_BT_to_BT_after(M_COMM_CHANNEL_MAX, ch_new);
}




#if 0 ////////////////////////////// delete dongle ///////////////////////////
//----------------------------------------------------------------------------------------------------------------------
//			Change_DONGLE_to_BT_before
//----------------------------------------------------------------------------------------------------------------------
ret_code_t Change_DONGLE_to_BT_before(uint8_t ch_old, uint8_t ch_new)
{
	ret_code_t ret=NRF_SUCCESS;
	
	if(!is_DONGLE_channel(ch_old) )
	{
		//DEB("Change_DONGLE_to_BT_before: (%d) is not dongle\n",ch_old);
		return ret;
	}

	// dongle과 ble에 어떤 영향이 있는지 확인 필요 - see: main.c loop
	// (1) m_dongle_send()에서 data를 보내지 못하도록 먼저 m_comm_mode를 BLE로 바꾸어 놓는다.
	// (2) 실제 dongle mode가 아닌데 data를 보내면, SOFTDEVICE: INVALID MEMORY ACCESS등과 같은 오류가 발생한 후 제어 불능상태가 된다.
	//DEB("Change_DONGLE_to_BT_before: m_comm_mode=M_COMM_MODE_BLE \n");
	m_comm_mode = M_COMM_MODE_BLE;

	//
	//DEB("Change_DONGLE_to_BT_before: m_dongle_stop() \n");
	ret = m_dongle_stop();	PRINT_IF_ERROR("m_dongle_stop()", ret);

	//
	//DEB("Change_DONGLE_to_BT_before: m_comm_ble_start() \n");
	ret = m_comm_ble_start();PRINT_IF_ERROR("m_comm_ble_start()", ret);
	//
	//
	return ret;
}
//----------------------------------------------------------------------------------------------------------------------
//			Change_DONGLE_to_BT_after
//----------------------------------------------------------------------------------------------------------------------
ret_code_t Change_DONGLE_to_BT_after(uint8_t ch_old, uint8_t ch_new)
{
	ret_code_t ret=NRF_SUCCESS;

	//----------------------------------------------------------------------------------------------------------------------
	//DEB("Change_DONGLE_to_BT_after(%d,%d)\n",ch_old,ch_new);
	//
	if(!is_DONGLE_channel(ch_old))
	{
		//DEB("Change_DONGLE_to_BT_after: %d is not a dongle\n",ch_old);
		return NRF_ERROR_INTERNAL;
	}
	if(!is_BT_channel(ch_new))
	{
		//DEB("Change_DONGLE_to_BT_after: %d is not a ble\n",ch_new);
		return NRF_ERROR_INTERNAL;
	}
	//----------------------------------------------------------------------------------------------------------------------
	//
	//DEB("Change_DONGLE_to_BT: drv_fds_init()\n");
	//drv_fds_init();  // <----

	//----------------------------------------------------------------------------------------------------------------------
	// Stanley added
	//DEB("\t Change_DONGLE_to_BT_after: m_comm_mode=BLE \n");
	m_comm_mode = M_COMM_MODE_BLE;	

	
	//----------------------------------------------------------------------------------------------------------------------
	// m_mokibo_name[ch_new]을 device name으로 설정
	//DEB("\t Change_DONGLE_to_BT_after: update_device_name() \n");
	update_device_name(ch_new); // --> sd_ble_gap_device_name_set('MOKIBO BTn') 

	//----------------------------------------------------------------------------------------------------------------------
	//DEB("\t Change_DONGLE_to_BT_after: m_comm_adv_start() \n");
	ret = m_comm_adv_start((m_comm_adv_channel_type_t)ch_new); PRINT_IF_ERROR("m_comm_adv_start", ret);
	
	//----------------------------------------------------------------------------------------------------------------------
	//DEB("\t Change_DONGLE_to_BT_after: m_led_set_status() \n");
	m_led_set_status(M_LED_DIMMING_OUT);
	

	//----------------------------------------------------------------------------------------------------------------------
	return ret;
}
#endif /////////////////////////////////// delete dongle /////////////////////






#if 0 /////////////////////////// delete dongle ////////////////////////////////
//----------------------------------------------------------------------------------------------------------------------
//			Change_to_DONGLE
//----------------------------------------------------------------------------------------------------------------------
ret_code_t Change_to_DONGLE(uint8_t ch_new)
{
	ret_code_t ret=NRF_SUCCESS;

	//----------------------------------------------------------------------------------------------------------------------
	//DEB("Change_to_DONGLE: BEGIN \n");
	//
	if(!is_DONGLE_channel(ch_new))
	{
		//DEB("Change_to_DONGLE: %d is not a DONGLE\n",ch_new);
		return NRF_ERROR_INTERNAL;
	}
	
	//----------------------------------------------------------------------------------------------------------------------
	// 현재의 BLE 연결을 끊는다.
	//DEB("Change_to_DONGLE: m_comm_adv_disconnect() \n");
	ret = m_comm_adv_disconnect();	PRINT_IF_ERROR("m_comm_adv_disconnect()", ret);

	//----------------------------------------------------------------------------------------------------------------------
	// Stop all application functionality before disabling the SoftDevice.
	//DEB("Change_to_DONGLE: m_comm_ble_stop() \n");
	ret = m_comm_ble_stop();	PRINT_IF_ERROR("m_comm_ble_stop()", ret);
	
//	//----------------------------------------------------------------------------------------------------------------------
//	// Dongle을 시작한다.
//	DEB("Change_to_DONGLE: m_dongle_start() \n");
//	ret = m_dongle_start();	PRINT_IF_ERROR("m_dongle_start()", ret);

	//----------------------------------------------------------------------------------------------------------------------
	// 이젠, dongle mode다.
	//DEB("Change_to_DONGLE: m_comm_mode=M_COMM_MODE_GAZELL\n");
	m_comm_mode = M_COMM_MODE_GAZELL;	

	//----------------------------------------------------------------------------------------------------------------------
	//DEB("Change_to_DONGLE: END \n");
	return ret;
}
#endif //////////////////////////////// delete dongle /////////////////////////



//----------------------------------------------------------------------------------------------------------------------
//			Refresh_BT
//----------------------------------------------------------------------------------------------------------------------
ret_code_t Refresh_BT(uint8_t ch)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t ret=NRF_SUCCESS;
	//----------------------------------------------------------------------------------------------------------------------
	#if defined(M_COMM_CH_TRACE)
		DEB("Refresh_BT(:%d)\n",ch);
	#endif
	//
	if(!is_BT_channel(ch)) return NRF_ERROR_INTERNAL;
	
	//m_dongle_stop();
	//m_comm_ble_stop();
	//m_comm_ble_start();
	
	return ret;
}

#if 0 ////////////////////////////// delete dongle ////////////////////////
//----------------------------------------------------------------------------------------------------------------------
//			Refresh_DONGLE
//----------------------------------------------------------------------------------------------------------------------
ret_code_t Refresh_DONGLE(uint8_t ch)
{
	ret_code_t ret=NRF_SUCCESS;
	//----------------------------------------------------------------------------------------------------------------------
	#if defined(M_COMM_CH_TRACE)
		DEB("Refresh_DONGLE:(%d)\n",ch);
	#endif
	if(!is_DONGLE_channel(ch)) return NRF_ERROR_INTERNAL;

	// 
	//m_comm_ble_stop();
	//m_dongle_stop();
	//m_dongle_start();
	
	//
	return ret;
}
#endif ////////////////////////////////////////////////////////////////////




//---------------------------------------------------------------------------------------------------------
//			onCommCmdNewBond
//---------------------------------------------------------------------------------------------------------
// (1) BT mode에서만, keyboard 버튼을 '길게' 누름으로써 발생함 (참조: 짧게 누르면 채널 전환)
ret_code_t onCommCmdNewBond(uint32_t argv)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t rv= NRF_SUCCESS;
	m_comm_channel_type_t 	selected_ch_old, selected_ch_new;
	pm_peer_id_t 			pm_peer_id;

	//-----------------------------------------------------------------------------------------------------------------------------------
	#if defined(M_COMM_CH_TRACE)
		DEB("========== onCommCmdNewBond() ============================================================\n");
	#endif

	//-----------------------------------------------------------------------------------------------------------------------------------
	selected_ch_old= m_config.config.selected_ch;
	selected_ch_new= (m_comm_channel_type_t)argv;
	
	if(!is_BT_channel(selected_ch_new))
		return rv;

	// Clear Buffered Data
	buffer_init();

	if(selected_ch_old != selected_ch_new)
	{
	//-----------------------------------------------------------------------------------------------------------------------------------
	// Remember the new channel config
	#if defined(M_COMM_CH_TRACE)
		DEB("\t NewBond: m_config.config.selected_ch <-- selected_ch_new(%d) \n",selected_ch_new);
	#endif
	m_config.config.selected_ch = selected_ch_new;
	}

	//-----------------------------------------------------------------------------------------------------------------------------------
	//		Display LED
	m_mokibo_set_touch_color();
	m_led_set_status(M_LED_DIMMING_OUT);

	DEB("\t NewBond: m_config.config.central <-- M_COMM_CENTRAL_OTHER \n");
	m_config.config.central[selected_ch_new] = M_COMM_CENTRAL_OTHER;
	
	//-----------------------------------------------------------------------------------------------------------------------------------
	// Invalidate The Old Bonding
	if(is_BT_channel(selected_ch_new))
	{
	pm_peer_id = m_comm_adv_get_addr( (m_comm_adv_channel_type_t)selected_ch_old );
	
	DEB("\t NewBond: m_comm_adv_get_addr(%d) --> pm_peer_id: 0x%x\n",selected_ch_old, pm_peer_id);
	if( pm_peer_id == M_COMM_ADV_BOND_READY )
	{
		DEB("\t NewBond: Invalidate The Old Bonding: m_comm_adv_set_addr(0x%x, M_COMM_ADV_BOND_INVALID) \n",pm_peer_id);
		m_comm_adv_set_addr( (m_comm_adv_channel_type_t)selected_ch_old, M_COMM_ADV_BOND_INVALID);
	}
	}

		//-----------------------------------------------------------------------------------------------------------------------------------
		// 새로운 bonding (advertising) 준비
	DEB("\t NewBond: m_comm_adv_ready_bond(%d)\n",selected_ch_new);
		rv = m_comm_adv_ready_bond((m_comm_adv_channel_type_t)selected_ch_new);	PRINT_IF_ERROR("m_comm_adv_ready_bond()", rv);

		//-----------------------------------------------------------------------------------------------------------------------------------
		// 새로운 bonding (advertising) 시작  (처음엔 Fast Advertising, 시간이 조금 더 지난 후에는 Slow Advertising)
		// (1) 이로 인하여, 기존에 connected되었던 채널은 disconnected된다.(참고-->ID=0x11, Len=0x9, BLE_GAP_EVT_DISCONNECTED - Disconnected from peer.)
		// (2) peer 측에서 선택하면, mokibo로부터 PIN 입력을 기다린다.
		// (2) peer 측에서 (취소)거절하면, disconnected된다.(참고--> ID=0x11, Len=0x9, BLE_GAP_EVT_DISCONNECTED - Disconnected from peer.)
	if(M_COMM_ADV_WAITE_GAP_DISCONNECT != m_comm_adv_get_status())
	{
		//-----------------------------------------------------------------------------------------------------------------------------------
		// m_mokibo_name[selected_ch_new]을 device name으로 설정
		DEB("\t NewBond: update_device_name(%d)....'%s'\n",selected_ch_new, m_mokibo_name[selected_ch_new] );
		update_device_name(selected_ch_new); // --> sd_ble_gap_device_name_set('MOKIBO BT2') 

		DEB("\t NewBond: m_comm_adv_start(%d)\n",selected_ch_new);
		rv = m_comm_adv_start((m_comm_adv_channel_type_t)selected_ch_new);	PRINT_IF_ERROR("m_comm_adv_start", rv);
		// (4) PIN입력이 맞으면 (peer 측에서 수락하면), 다음과 같은 이벤트들이 발생한다.
		//			ID=0x10, Len=0x18, BLE_GAP_EVT_CONNECTED - Connection established.
		//			ID=0x23, Len=0x10, BLE_GAP_EVT_DATA_LENGTH_UPDATE_REQUEST - Data Length Update Request.
		//			ID=0x24, Len=0x10, BLE_GAP_EVT_DATA_LENGTH_UPDATE - LL Data Channel PDU payload length updated.
		//			ID=0x55, Len=0x8, BLE_GATTS_EVT_EXCHANGE_MTU_REQUEST - Exchange MTU Request.
		//			ID=0x13, Len=0xD, BLE_GAP_EVT_SEC_PARAMS_REQUEST - Request to provide security parameters.
		//			ID=0x17, Len=0x9, BLE_GAP_EVT_AUTH_KEY_REQUEST - Request to provide an authentication key.
		//			ID=0x50, Len=0x14, BLE_GATTS_EVT_WRITE - Write operation performed.
		//			ID=0x12, Len=0x10, BLE_GAP_EVT_CONN_PARAM_UPDATE - Connection Parameters updated.
		//			ID=0x23, Len=0x10, BLE_GAP_EVT_DATA_LENGTH_UPDATE_REQUEST - Data Length Update Request.
		//			ID=0x24, Len=0x10, BLE_GAP_EVT_DATA_LENGTH_UPDATE - LL Data Channel PDU payload length updated.
		//			ID=0x1A, Len=0xA, BLE_GAP_EVT_CONN_SEC_UPDATE - Connection security updated.
		//			ID=0x19, Len=0xE, BLE_GAP_EVT_AUTH_STATUS - Authentication procedure completed with status.
		//			ID=0x57, Len=0x7, BLE_GATTS_EVT_HVN_TX_COMPLETE - Handle Value Notification transmission complete.
		//			ID=0x12, Len=0x10, BLE_GAP_EVT_CONN_PARAM_UPDATE - Connection Parameters updated.
		//			ID=0x12, Len=0x10, BLE_GAP_EVT_CONN_PARAM_UPDATE - Connection Parameters updated.
		//			ID=0x50, Len=0x13, BLE_GATTS_EVT_WRITE - Write operation performed.
		//-----------------------------------------------------------------------------------------------------------------------------------
		#if MOKIBO_NFC_ENABLED		
		if(m_nfc_state_stop())
		{
			//DEB("\t NewBond: m_nfc_start()....wait for PIN input...\n");
			m_nfc_start();
		}
		#endif
	}
	
		//-----------------------------------------------------------------------------------------------------------------------------------
	// M_COMM_CMD_NEW_BOND ?대㈃ ?몄젣??M_COMM_MODE_BLE??
	//DEB("\t NewBond: m_comm_mode = M_COMM_MODE_BLE \n");
	m_comm_mode = M_COMM_MODE_BLE;

	//-----------------------------------------------------------------------------------------------------------------------------------
	// Save any change in BLE states
	//DEB("m_comm_cmd: fds_RequestToSaveAfterThisTime(%d) .... maybe saved far later...\n",SAVEDELAY_NEW_BOND);
	fds_RequestToSaveAfterThisTime(selected_ch_new,SAVEDELAY_NEW_BOND);

	//
	return rv;
}

//---------------------------------------------------------------------------------------------------------
//			onCommCmdStop
//---------------------------------------------------------------------------------------------------------
// mokibo가 sleep mode 등으로 들어갈 때, 현재의 연결을 해제한다.
ret_code_t onCommCmdStop(uint8_t selected)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t rv= NRF_SUCCESS;

	//-----------------------------------------------------------------------------------------------------------------------------------
	// power down the (cypress/parade touch).
	drv_cypress_touch_power_down(); // drv_cypress.c

	//-----------------------------------------------------------------------------------------------------------------------------------
	#if defined(M_COMM_CH_TRACE)
	DEB("========== onCommCmdStop(%d) by sleeping =====================================================\n",selected);
	#endif


	//===HALT를 이용하는 경우기 아니므로, 다음은 사용하지 않는다====
	//	//-----------------------------------------------------------------------------------------------------------------------------------
	//	// [Fn+Del]에 의한 Interrupt가 가능하도록, i/o pin을 Relese한다.
	//	DEB(" RELEASE: Fn+Del Pins \n");
	//	drv_release_fn_del_colums();

	//-----------------------------------------------------------------------------------------------------------------------------------
	if( is_BT_channel(selected) ) {
		//-----------------------------------------------------------------------------------------------------------------------------------
		#if defined(M_COMM_CH_TRACE)
			DEB("\t CommSTOP: m_comm_adv_disconnect()\n");
		#endif
		rv = m_comm_adv_disconnect(); PRINT_IF_ERROR("m_comm_adv_disconnect()", rv);

//	} else if( is_DONGLE_channel(selected) ){
//		//-----------------------------------------------------------------------------------------------------------------------------------
//		#if defined(M_COMM_CH_TRACE)
//			DEB("\t CommSTOP: m_dongle_stop()\n");
//		#endif
//		rv = m_dongle_stop(); PRINT_IF_ERROR("m_dongle_stop()", rv);
//
	} else {
		//-----------------------------------------------------------------------------------------------------------------------------------
		#if defined(M_COMM_CH_TRACE)
			DEB("\t CommSTOP: nothing-related(ch=%d)\n",selected);
		#endif
	}
	//
	return rv;
}
//---------------------------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------------------------
//			m_comm_ble_get_status
//-----------------------------------------------------------------------------------------------------
uint16_t m_comm_ble_get_status(void)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	return m_conn_handle;
}

//-----------------------------------------------------------------------------------------------------
//			m_comm_get_selected_channel
//-----------------------------------------------------------------------------------------------------
m_comm_channel_type_t m_comm_get_selected_channel(void)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	return m_config.config.selected_ch;
}

//-----------------------------------------------------------------------------------------------------
//			is_BT_channel
//-----------------------------------------------------------------------------------------------------
bool is_BT_channel(m_comm_channel_type_t ch)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	bool is_bt = false;

	//m_comm_channel_type_t selected;
	//selected = m_config.config.selected_ch;

	if( (M_COMM_CHANNEL_BT1 == ch) 
		|| (M_COMM_CHANNEL_BT2 == ch) 
		|| (M_COMM_CHANNEL_BT3 == ch))
	{
		is_bt = true;
	}

	return is_bt;
}
	//-----------------------------------------------------------------------------------------------------
	//			is_DONGLE_channel
	//-----------------------------------------------------------------------------------------------------
//	bool is_DONGLE_channel(m_comm_channel_type_t ch)
//	{
//		return (ch == M_COMM_CHANNEL_DONGLE)? 1 : 0;
//	}

//-----------------------------------------------------------------------------------------------------
//			is_VALID_channel
//-----------------------------------------------------------------------------------------------------
bool is_VALID_channel(m_comm_channel_type_t ch)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	if(is_BT_channel(ch)) return 1;
//	if(is_DONGLE_channel(ch)) return 1;
	return 0;
}








/*
 $$$$$$\  $$\       $$$$$$\  $$$$$$$\   $$$$$$\  $$\                                     
$$  __$$\ $$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                                    
$$ /  \__|$$ |     $$ /  $$ |$$ |  $$ |$$ /  $$ |$$ |                                    
$$ |$$$$\ $$ |     $$ |  $$ |$$$$$$$\ |$$$$$$$$ |$$ |                                    
$$ |\_$$ |$$ |     $$ |  $$ |$$  __$$\ $$  __$$ |$$ |                                    
$$ |  $$ |$$ |     $$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |                                    
\$$$$$$  |$$$$$$$$\ $$$$$$  |$$$$$$$  |$$ |  $$ |$$$$$$$$\                               
 \______/ \________|\______/ \_______/ \__|  \__|\________|                              
 $$$$$$\   $$$$$$\  $$\   $$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$\   $$\ $$$$$$$$\  $$$$$$\  
$$  __$$\ $$  __$$\ $$$\  $$ |$$  __$$\\__$$  __|$$  __$$\ $$$\  $$ |\__$$  __|$$  __$$\ 
$$ /  \__|$$ /  $$ |$$$$\ $$ |$$ /  \__|  $$ |   $$ /  $$ |$$$$\ $$ |   $$ |   $$ /  \__|
$$ |      $$ |  $$ |$$ $$\$$ |\$$$$$$\    $$ |   $$$$$$$$ |$$ $$\$$ |   $$ |   \$$$$$$\  
$$ |      $$ |  $$ |$$ \$$$$ | \____$$\   $$ |   $$  __$$ |$$ \$$$$ |   $$ |    \____$$\ 
$$ |  $$\ $$ |  $$ |$$ |\$$$ |$$\   $$ |  $$ |   $$ |  $$ |$$ |\$$$ |   $$ |   $$\   $$ |
\$$$$$$  | $$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$ |  $$ |$$ | \$$ |   $$ |   \$$$$$$  |
 \______/  \______/ \__|  \__| \______/   \__|   \__|  \__|\__|  \__|   \__|    \______/ */
 //============================================================================//
//                                 GLOBAL CONSTANTS                            //
//============================================================================*/

//-----------------------------------------------------------------------------------------------------
//			m_comm_send_event
//-----------------------------------------------------------------------------------------------------
ret_code_t m_comm_send_event(m_comm_event_t evt, const m_comm_evt_arg_t arg)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	int8_t up_down, left_right, zoom;
	ret_code_t  rv = NRF_SUCCESS;
	uint8_t 	buf[M_COMM_KEYS_BUF_LEN] = {0, };
	

	#if defined(M_COMM_EVT_SEND_TRACE)
	// TODO: Use stringizing(#) of preprocessor macros
	static const char *String[M_COMM_EVENT_MAX] = \
	{ \
		"VOLUME_CTRL", \
		"CLICK_CTRL", \
		"WHEEL_CTRL", \
		"ZOOM_CTRL", \
		"MULTI_TOUCH_UPDATE", \
		"KEYBOARD_UPDATE", \
		"POINT_UPDATE", \
		"SPECIAL_KEY", \
	};
	SYSTEM_MSG("===(comm_send: %s)===:%6d[%4d] ", String[evt], arg.point.timestamp, arg.point.timestamp-timestamp_sent);
	#endif
	

	if(is_BT_channel(m_config.config.selected_ch))
	{
		m_comm_adv_channel_type_t my_ch = (m_comm_adv_channel_type_t)m_config.config.selected_ch;
		
		// Data를 보내야 하는데, 연결이 되어 있지 않고
		if(BLE_CONN_HANDLE_INVALID == m_conn_handle)
		{
			// Bonding 정보가 저장되어 있으면서, 즉 페어링이 되어 있는 상태인데
			if((M_COMM_ADV_BOND_INVALID != m_comm_adv_get_addr(my_ch)) 
					&& (M_COMM_ADV_BOND_READY != m_comm_adv_get_addr(my_ch)))
			{
				// Advertising을 하고 있지 않다면 연결 시작
				if(M_COMM_ADV_STATUS_IN_ADV != m_comm_adv_get_status())
				{
					DEB("update_device_name(%d)....'%s'\n",my_ch, m_mokibo_name[my_ch] );
					update_device_name(my_ch); // --> sd_ble_gap_device_name_set('MOKIBO BT2') 
					
					DEB("%s() start advertising", __func__);
					m_comm_adv_start(my_ch);
					
					#if defined(M_COMM_CH_TRACE)
					DEBUG("connecting to my_ch:%d", my_ch);
					#endif
				}
			}
			//return NRF_ERROR_INVALID_STATE;
		}
	}
	________________________________LOG_mComm_Recv_mCommSendEvent(5,"switch(evt:%d)",evt);


	if(	(m_touch_mode_get() == M_TOUCH_CYPRESS_ACTIVE_AREA_WHOLE) &&
		(m_touchlock_automode == false) && 
		(evt == M_COMM_EVENT_WHEEL_CTRL || evt == M_COMM_EVENT_ZOOM_CTRL || evt == M_COMM_EVENT_MUTI_TOUCH_UPDATE || evt == M_COMM_EVENT_POINT_UPDATE ))
	{
		return rv;
	}
	

	switch(evt)
	{
		//-------------------------------------------------------------------------------------------
	
		case M_COMM_EVENT_VOLUME_CTRL :

			if(arg.volume.up)
			{
				memcpy(buf, m_vol_up_cmd, sizeof(m_vol_up_cmd));			
			}
			else if(arg.volume.down)
			{
				memcpy(buf, m_vol_down_cmd, sizeof(m_vol_down_cmd));
			}
			else if(arg.volume.mute)
			{
				memcpy(buf, m_vol_mute_cmd, sizeof(m_vol_mute_cmd));
			}
			else
			{
				memcpy(buf, m_consumer_no_cmd, sizeof(m_consumer_no_cmd));
			}

			rv = send(SEND_CONSUMER, buf, sizeof(m_vol_up_cmd));
		break;				
	
		//-------------------------------------------------------------------------------------------
		case M_COMM_EVENT_CLICK_CTRL :			
		
			rv = click_button_send(arg.click.left, arg.click.right, 0);	
			break;

		//-------------------------------------------------------------------------------------------
		case M_COMM_EVENT_WHEEL_CTRL :
			if(arg.wheel.up) { up_down = -1; }
			else if(arg.wheel.down) { up_down = 1; }
			else { up_down = 0; }
			
			if(arg.wheel.right) { left_right = -1; }
			else if(arg.wheel.left) { left_right = 1; }
			else { left_right = 0; }

			rv = wheel_send(up_down, left_right, WHEEL_DEFAULT_TRVDIST);
			break;
		
		//-------------------------------------------------------------------------------------------
		case M_COMM_EVENT_ZOOM_CTRL :			
			if(arg.zoom.in) { zoom = 1; }
			else if(arg.zoom.out) { zoom = -1; }
			else { zoom = 0; }

			rv = zoom_send(zoom);
			break;
		
		//-------------------------------------------------------------------------------------------
		case M_COMM_EVENT_MUTI_TOUCH_UPDATE :			
			rv = multi_touch_send(arg.touch);
			break;
		
		//-------------------------------------------------------------------------------------------
		case M_COMM_EVENT_KEYBOARD_UPDATE :			
			memcpy(buf, arg.keys.buf, sizeof(buf));
		
			if(m_passkey.is_ready)
			{
				rv = passkey_handle(buf, sizeof(buf));
			}
			else
			{
				rv = send(SEND_KEY, buf, sizeof(buf));
			}
			break;

		//-------------------------------------------------------------------------------------------
		case M_COMM_EVENT_POINT_UPDATE :		
			memcpy(buf, arg.point.buf, M_COMM_MOVEMENT_BUF_LEN);
			rv = send(SEND_TOUCH_MOVE, buf, M_COMM_MOVEMENT_BUF_LEN);
			break;
			
		//-------------------------------------------------------------------------------------------
		case M_COMM_EVENT_SPECIAL_KEY :					
			
			rv = special_key_send(arg.sp_key);
			break;

		//-------------------------------------------------------------------------------------------
		case M_COMM_EVENT_MAX :
		default :
			DEBUG("OOPS[%d]!!", evt);
			rv = NRF_ERROR_INVALID_PARAM;
			break;

		//-------------------------------------------------------------------------------------------
	}

	return rv;
}
/*
================================================================================
   Function name : m_comm_set_central_type()
================================================================================
*/
ret_code_t m_comm_set_central_type(m_comm_channel_type_t ch, m_comm_central_type_t type)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
    ret_code_t err_code = NRF_SUCCESS;
		
	if((M_COMM_CHANNEL_MAX <= ch) || (M_COMM_CENTRAL_OTHER < type))
	{
		//DEB("#INVALID: ch=%d, central=%d\n", ch, type);
		err_code = NRF_ERROR_INVALID_PARAM;
	}
	else
	{
		//DEB("SET_CENTRAL_TYPE(): m_config.config.central: ch=%d, type=%d \n", ch, type);
		
		//if(m_config.config.central[ch] != type)  // <--- blocked by Stanley 2018.
		{
			m_config.config.central[ch] = type;
			if(is_BT_channel(ch))
			{
				fds_RequestToSaveAfterThisTime(ch,SAVEDELAY_CENTRAL);
			}
		}
	}
	//
	return err_code;
}

/*
================================================================================
   Function name : m_comm_get_central_type()
================================================================================
*/
ret_code_t m_comm_get_central_type(m_comm_channel_type_t ch, m_comm_central_type_t* p_type)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
    ret_code_t err_code = NRF_SUCCESS;
	
	if((M_COMM_CHANNEL_MAX <= ch))
	{
		DEBUG("OOPS[%d]", ch);
		err_code = NRF_ERROR_INVALID_PARAM;
	}
	else if(NULL == p_type)
	{
		err_code = NRF_ERROR_NULL;
	}
	else
	{
		*p_type = m_config.config.central[ch];
	}

	return err_code;
}

/*
================================================================================
   Function name : m_comm_current_central_type()
================================================================================
*/
m_comm_central_type_t m_comm_current_central_type(void)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	return m_config.config.central[m_config.config.selected_ch];
}

/*
================================================================================
   Function name : m_comm_set_config_default()
================================================================================
*/
ret_code_t m_comm_set_config_default(void)
{
	________________________________LOG_mComm_FuncHeads(0,"");
	ret_code_t rv;
	uint32_t  val;

	//m_comm_config_t	config;
	
	memset(&m_config, 0, sizeof(m_comm_config_t));
	
	m_config.config.selected_ch = M_COMM_CHANNEL_MAX;
	for(val=0; val<M_COMM_CHANNEL_MAX; val++)
	{
		m_config.config.central[val] = M_COMM_CENTRAL_OTHER;
	}

//	//rv = drv_fds_write(DRV_FDS_PAGE_M_COMM_CONFIG, (uint8_t *)&config,  sizeof(config));
//	//PRINT_IF_ERROR("drv_fds_write()", rv);
//	memcpy(&m_config, &config, sizeof(m_config) ); // copy it to the global m_config !
//	rv= fds_write_stanley(FDS_FILE_ID_STANLEY, FDS_RECORD_KEY_M_COMM_CONFIG, (uint8_t *)&config, sizeof(config) );

	//debshow_comm_config("default", m_config);
	rv= FDS_SUCCESS;
	
	return rv;
}

/*
================================================================================
   Function name : m_comm_clear_keyboard()
================================================================================
*/
void m_comm_clear_keyboard(void)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
	clear_keyboard();
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			m_comm_ble_start
//-----------------------------------------------------------------------------------------------------------------------------------
ret_code_t m_comm_ble_start(void)
{	
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
    uint32_t err_code;
    ret_code_t rv;

	#if defined(M_COMM_CH_TRACE)
	DEB("m_comm_ble_start: BEGIN \n");
	#endif

//	// newly added by Stanley
//	DEB("m_comm_ble_start: m_timer_init() ... newly added by Stanley\n");
//	m_timer_init();
	
	//DEB("m_comm_ble_start: m_timer_set(M_TIMER_ID_COMM_TIMEOUT)\n");
	m_timer_set(M_TIMER_ID_COMM_TIMEOUT, BLE_START_TIMEOUT_TIME);

	TIME_TRACE_START();
	// Initialize SoftDevice
	do {
		//m_mokibo_WDT_reload();
		Update_WDT();	// add by jaehong, 2019/06/28

		// SoftDevice enable request
		rv = nrf_sdh_enable_request(); 
		//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(rv);
		if(m_timer_is_expired(M_TIMER_ID_COMM_TIMEOUT))
		{
			//DEB("m_comm_ble_start: M_TIMER_ID_COMM_TIMEOUT... ble not started !! \n");
			break;
		}
	} while(!nrf_sdh_is_enabled());
	TIME_TRACE_END(BLE_START_TIMEOUT_TIME);


    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
	//DEB("m_comm_ble_start: nrf_sdh_ble_default_cfg_set()\n");
    uint32_t ram_start = 0;
    rv = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start); 
	//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(rv);

    // Enable BLE stack.
	//DEB("m_comm_ble_start: nrf_sdh_ble_enable()\n");
    rv = nrf_sdh_ble_enable(&ram_start); 
	//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(rv);

    // Register a handler for BLE events.
//   NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
// HERE HERE by Stanley
	
	//DEB("m_comm_ble_start: services_init()\n");
	
	//gap_params_init();
	//gatt_init();    
	//m_comm_adv_init();
	services_init();
    
	//DEB("m_comm_ble_start: conn_params_init()\n");
	conn_params_init();

	//DEB("m_comm_ble_start: m_timer_start()\n");
 	err_code = m_timer_start(); 
	//deleted by jaehong, 2019/06/28 APP_ERROR_CHECK(err_code);

	// <--- ????????? =============================================================
	// 초기화 하지 않으면 m_comm_adv_start() 처리시 죽음.
	// adv를 초기화 하여 handle을 다시 받을수 있도록 한다.
	//DEB("m_comm_ble_start: ==== m_conn_handle:= BLE_CONN_HANDLE_INVALID =============  \n");
	m_conn_handle= BLE_CONN_HANDLE_INVALID;

	//DEB("m_comm_ble_start: END\n");
	return err_code;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			m_comm_ble_stop
//-----------------------------------------------------------------------------------------------------------------------------------
ret_code_t m_comm_ble_stop(void)
{
	________________________________LOG_mComm_FuncHeads(FUNCHEAD_FLAG,"");
    uint32_t err_code;
	
	//DEB("m_comm_ble_stop: BEGIN\n");

	// Stop application and all timers.
	//DEB("m_comm_ble_stop: app_timer_stop_all()\n");
	err_code = app_timer_stop_all(); PRINT_IF_ERROR("app_timer_stop_all()", err_code);

    // Stop any pending connection parameters update.
	//DEB("m_comm_ble_stop: ble_conn_params_stop()\n");
    err_code = ble_conn_params_stop(); PRINT_IF_ERROR("ble_conn_params_stop()", err_code);

	//DEB("m_comm_ble_stop: m_timer_set(M_TIMER_ID_COMM_TIMEOUT)\n");
	m_timer_set(M_TIMER_ID_COMM_TIMEOUT, BLE_STOP_TIMEOUT_TIME);

	TIME_TRACE_START();
	// Disable the SoftDevice.
	do {
		//DEB("m_comm_ble_stop: nrf_sdh_disable_request()\n");

		//m_mokibo_WDT_reload();
		Update_WDT();	// add by jaehong, 2019/06/28

		// SoftDevice disable request
		err_code = nrf_sdh_disable_request(); PRINT_IF_ERROR("nrf_sdh_disable_request()", err_code);

		if(m_timer_is_expired(M_TIMER_ID_COMM_TIMEOUT))
		{
			//DEB("m_comm_ble_stop: nrf_sdh_disable_request()...failed !!!\n");
			break;
		}
    } while(nrf_sdh_is_enabled());	//true/false
	
	TIME_TRACE_END(BLE_STOP_TIMEOUT_TIME);

	//DEB("m_comm_ble_stop: END\n");
	return err_code;
}


// End of file 

