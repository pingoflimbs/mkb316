/// \file m_led.h
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 27-Oct-2017  MJ.Kim
* + Created initial version
*
* 27-Dec-2017  MJ.Kim
* + Rewrote code according to "171218_Mokibo 동작시나리오.docx"
*
* 12-Feb-2018  MJ.Kim
* + Added M_LED_COLOR_TEST
* + Issue#: http://lab.innopresso.com/issues/118#change-556 
*
* 20-Feb-2017  MJ.Kim
* + Updated modules based on Message version: 3.7
* + Added M_LED_COLOR_RSVD1/M_LED_COLOR_RSVD2 
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/

/*============================================================================*/
/*                            COMPILER DIRECTIVES                             */
/*============================================================================*/
#ifndef __M_LED_H
#define __M_LED_H

/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/    
#include "drv_stm8.h"

/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/
#if (MOKIBO_LED_TRACE_ENABLED==0)
	#define LED_TRACE_SET(x)	
	#define LED_TRACE_TOGGLE()	
	#define LED_TRACE_GET()		
#elif (MOKIBO_LED_TRACE_ENABLED==1)
	#define LED_TRACE_SET(x)	m_led_trace_set(x)
	#define LED_TRACE_TOGGLE()	m_led_trace_toggle()
	#define LED_TRACE_GET()		m_led_get_stm8_INT()
#else
	#error "What happen!!"
#endif

#define	M_LED_OFF 				STM8_LED_OFF
#define	M_LED_ON				STM8_LED_ON
#define	M_LED_SLOW_BLINK		STM8_LED_SLOW_BLINK
#define	M_LED_FAST_BLINK		STM8_LED_FAST_BLINK
#define	M_LED_DIMMING_OUT		STM8_LED_DIMMING_OUT
#define	M_LED_RESET				STM8_LED_RESET
#define	M_LED_STATUS_MAX		STM8_LED_STATUS_MAX

#define	M_LED_COLOR_ORANGE 		STM8_LED_COLOR_ORANGE
#define	M_LED_COLOR_RED			STM8_LED_COLOR_RED
#define	M_LED_COLOR_GREEN		STM8_LED_COLOR_GREEN
#define	M_LED_COLOR_BLUE		STM8_LED_COLOR_BLUE
#define	M_LED_COLOR_WHITE		STM8_LED_COLOR_WHITE
#define	M_LED_COLOR_RSVD1		STM8_LED_COLOR_RSVD1	// Purple
#define	M_LED_COLOR_RSVD2		STM8_LED_COLOR_RSVD2	// Yellow
#define	M_LED_COLOR_CUSTOM		STM8_LED_COLOR_CUSTOM 
#define	M_LED_COLOR_TEST		STM8_LED_COLOR_MAX		// Custom color 
#define	M_LED_COLOR_MAX			STM8_LED_COLOR_MAX	  

#define TOUCH_MODE_COLOR		M_LED_COLOR_WHITE
#define TOUCH_LOCK_MODE_COLOR	STM8_LED_COLOR_RSVD2





#ifdef FLIMBS_20190111_CHANGE_LED_COLOR_GBR
	#define BT1_MODE_COLOR 			M_LED_COLOR_GREEN	// BT1= GREEN
	#define BT2_MODE_COLOR 			M_LED_COLOR_RED		// BT2= RED
	#define BT3_MODE_COLOR	 		M_LED_COLOR_BLUE	// BT3= BLUE
	#define DONGLE_MODE_COLOR		M_LED_COLOR_RSVD2	// Dongle= YELLOW
	#define NO_CHANNEL_MODE_COLOR	M_LED_COLOR_GREEN	// BT2= GREEN
#else
	#define BT1_MODE_COLOR 			M_LED_COLOR_GREEN	// BT2= GREEN
	#define BT2_MODE_COLOR 			M_LED_COLOR_RED		// BT1= RED
	#define BT3_MODE_COLOR 			M_LED_COLOR_RSVD2	// BT3= YELLOW
	#define DONGLE_MODE_COLOR 		M_LED_COLOR_BLUE	// DONGLE= BLUE
	#define NO_CHANNEL_MODE_COLOR 	M_LED_COLOR_GREEN	// BT2= GREEN
#endif


/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/

/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/
typedef uint8_t  M_LED_STATUS_TYPE_t; 
typedef uint8_t  M_LED_COLOR_TYPE_t; 

/*============================================================================*/
/*                               GLOBAL FUNCTIONS                             */
/*============================================================================*/
ret_code_t m_led_init(void);

ret_code_t m_led_set_color(M_LED_COLOR_TYPE_t);
M_LED_COLOR_TYPE_t m_led_get_color(void);

ret_code_t m_led_set_status(M_LED_STATUS_TYPE_t);
M_LED_STATUS_TYPE_t m_led_get_status(void);

#if (MOKIBO_LED_TRACE_ENABLED==1)
void m_led_trace_set(uint8_t on_off);
void m_led_trace_toggle(void);
uint8_t m_led_get_stm8_INT(void);
#endif

//void m_led_update(void);

#endif // __M_LED_H 
