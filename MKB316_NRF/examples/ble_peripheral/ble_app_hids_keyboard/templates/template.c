/// \file template.c
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/
/*
================================================================================
* File Revision History (top to bottom: first revision to last revision)
================================================================================
*
* Date          user    (Description on following lines: changes, etc.)
* ----------- --------
* 16-Oct-2017  MJ.Kim
* + Created initial version.
*
* Note: Mention Issue number for every change here after.
*
================================================================================
*/

/*============================================================================*/
/*                                MODULES USED                                */
/*============================================================================*/
#include "sdk_config.h"
#include "sdk_errors.h"        
#include "template.h"        

/*============================================================================*/
/*                                   MACROS                                   */
/*============================================================================*/             

/*============================================================================*/
/*                                 CONSTANTS                                  */
/*============================================================================*/

/*============================================================================*/
/*                                  TYPEDEFS                                  */
/*============================================================================*/
/// For defining the clear and set states.
typedef enum 
{
    TEMPLATE_STATUS_INVALID = 0,    ///< Used in place of zero.
    TEMPLATE_STATUS_VALID           ///< Used in place of one .
} TEMPLATE_STATUS_TYPE;

/*============================================================================*/
/*                       PROTOTYPES OF LOCAL FUNCTIONS                        */
/*============================================================================*/


/*============================================================================*/
/*                              CLASS VARIABLES                               */
/*============================================================================*/


/*============================================================================*/
/*                              LOCAL FUNCTIONS                               */
/*============================================================================*/


/*============================================================================*/
/*                              GLOBAL FUNCTIONS                              */
/*============================================================================*/
/*
================================================================================
   Function name : m_template_init()
================================================================================
*/
ret_code_t m_template_init(void)
{
	return 0;
}

/*
================================================================================
   Function name : m_template_enable()
================================================================================
*/
ret_code_t m_template_enable(void)
{
	return 0;
}

/*
================================================================================
   Function name : m_template_disable()
================================================================================
*/
ret_code_t m_template_disable(void)
{
	return 0;
}





// End of file 
