#  DFU

참조) [Getting started with Nordic's Secure DFU bootloader, a step by step guide](https://devzone.nordicsemi.com/blogs/1085/getting-started-with-nordics-secure-dfu-bootloader/)를 참조하면 DFU에 대해서 이해할 수 있다.

## 사용하려는 펌웨어 복사

"nrf52832_xxaa.hex"를 "c:\Python27\Scripts"에 복사

## 부트로더 복사

"nrf52832_xxaa_s132.hex"를 "c:\Python27\Scripts"에 복사


## bootloader_settings.hex 생성

```
nrfutil settings generate --family NRF52 --application nrf52832_xxaa.hex --application-version 0 --bootloader-version 0 --bl-settings-version 1 bootloader_settings_1420.hex
nrfutil settings generate --family NRF52 --application nrf52832_xxaa.hex --application-version 0 --bootloader-version 0 --bl-settings-version 1 bootloader_settings.hex

Generated Bootloader DFU settings .hex file and stored it in: bootloader_settings.hex

Bootloader DFU Settings:
* File:                 bootloader_settings.hex
* Family:               nRF52
* CRC:                  0x4A5B64D0
* Settings Version:     0x00000001 (1)
* App Version:          0x00000001 (1)
* Bootloader Version:   0x00000001 (1)
* Bank Layout:          0x00000000
* Current Bank:         0x00000000
* Application Size:     0x0000C9FC (51708 bytes)
* Application CRC:      0x62365C4B
* Bank0 Bank Code:      0x00000001

```


## 1: 처음으로 퓨징할 패키지 생성

이것은 앱과 부트로더를 포함하는 패키지를 만드는 것으로 생성된 패키지를 nRFgoStudio 프로그램을 이용해서 타겟 보드에 퓨징한다.

```
mergehex -m bootloader_settings.hex nrf52832_xxaa.hex nrf52832_xxaa_s132.hex -o mokibo_bootloader_and_app.hex
mergehex -m bootloader_settings_1420.hex nrf52832_xxaa.hex nrf52832_xxaa_s132.hex -o mokibo_bootloader_and_app_1420.hex
```

위에서 생성한 mokibo_bootloader_and_app.hex를 go studio를 이용해서 퓨징.



## 2: 앱 펌웨어 패키지 Version 1 생성

"--sd-req 0x9D"는 SoftDevice의 ID 값

프로그램을 수정하고 두버째 버전을 만드는 경우
두번째 프로그램: nrf52832_xxaa.hex

```
nrfutil pkg generate --hw-version 52 --application-version 1 --application nrf52832_xxaa.hex --sd-req 0x9D --key-file priv.pem mokibo_dfu_app_v1.zip
nrfutil pkg generate --hw-version 52 --application-version 1 --application nrf52832_xxaa.hex --sd-req 0x9D --key-file priv.pem mokibo_dfu_app_1420.zip
```

mokibo_dfu_app_v1.zip 파일을 핸드폰의 "nrf connect" 앱에 전송하여 업데이트를 진행한다.

## 2: SoftDevice 패키지 생성

SoftDevice: s132_nrf52_5.0.0_softdevice.hex

```
nrfutil pkg generate --hw-version 52 --softdevice s132_nrf52_5.0.0_softdevice.hex --sd-req 0x9D --key-file priv.pem mokibo_dfu_sd_9D.zip
```

## 3: BootLoader 패키지 Version 1 생성

BootLoader: nrf52832_xxaa_s132.hex

```
nrfutil pkg generate --hw-version 52 --bootloader-version 1 --bootloader nrf52832_xxaa_s132.hex --sd-req 0x9D --key-file priv.pem mokibo_dfu_dl_v1.zip
```


## 확인 명령

```
nrfutil settings display nrf52832_xxaa.hex
```

