/// \file main.c
/// This file contains all required configurations for mokibo
/*
//-----------------------------------------------------------------------------------------------------------------------------------
* Copyright (C) 2017-2018
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
//-----------------------------------------------------------------------------------------------------------------------------------
*/


/*\   $$\ $$$$$$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$$$$$$\ $$\     $$\          
$$ |  $$ |\_$$  _|$$  __$$\\__$$  __|$$  __$$\ $$  __$$\\$$\   $$  |         
$$ |  $$ |  $$ |  $$ /  \__|  $$ |   $$ /  $$ |$$ |  $$ |\$$\ $$  /          
$$$$$$$$ |  $$ |  \$$$$$$\    $$ |   $$ |  $$ |$$$$$$$  | \$$$$  /           
$$  __$$ |  $$ |   \____$$\   $$ |   $$ |  $$ |$$  __$$<   \$$  /            
$$ |  $$ |  $$ |  $$\   $$ |  $$ |   $$ |  $$ |$$ |  $$ |   $$ |             
$$ |  $$ |$$$$$$\ \$$$$$$  |  $$ |    $$$$$$  |$$ |  $$ |   $$ |             
\__|  \__|\______| \______/   \__|    \______/ \__|  \__|   \__|             
//============================================================================//
//                                 REVISION HISTORY                           //
//============================================================================//
20170826_MJ
	Created initial version

20180102_MJ
	Added log_resetreason()

20180122_Mj
	Removed unneccessary headers

20180226_MJ
	Increased SCHED_QUEUE_SIZE from 10 to 20

20180310_JH
	Increased SCHED_QUEUE_SIZE from 20 to 30, after adding NFC

20180520_JH
	Change power_manage() for GAZELL protocol mode

20180508_JH
	Added SCHED_QUEUE_SIZE condition

20180808_stanley
	cert_mode 추가: 마우스 포인터를 사각형으로 우회전하게 하고, keyboard 문자를 입력하는 과정을 반복함
	Fn+0 를 한번 누르면, cert_mode에 진입하고, 또한번 누르면 cert_mode를 벗어난다.
	전파인증, 블루투스인증, 전원인증 등 기타 인증을 받는 중간에 Reset이 발생할 수 있음.
	이러한 비정상 상황 발생 후 reset에 의하여 다시 시작하는 경우, 조작자 없이 스스로 동작 중에 있음을
	표현하는 하나의 방법으로서 '마우스 와 키보드 문자입력' 이 재개되어 정상상태에 있음을 알리기 위한 수단을
	구비하려는 데 그 목적이 있다

20180820_stanley
	Bluetooth Channel 사이의 전환은 정상이나,
	일단 USB Dongle로 전환했다가 다시 Bluetooth로 전환하는 경우에 비정상이어서,
	USB Dongle에서 Bluetooth로의 전환 직후 Soft-Reset을 시행함.
	향후 원인을 찾아서 근본적인 수정이 필요함.
==============================================================================*/







/*$$$$$\  $$\    $$\ $$$$$$$$\ $$$$$$$\  $$\    $$\ $$$$$$\ $$$$$$$$\ $$\      $$\ 
$$  __$$\ $$ |   $$ |$$  _____|$$  __$$\ $$ |   $$ |\_$$  _|$$  _____|$$ | $\  $$ |
$$ /  $$ |$$ |   $$ |$$ |      $$ |  $$ |$$ |   $$ |  $$ |  $$ |      $$ |$$$\ $$ |
$$ |  $$ |\$$\  $$  |$$$$$\    $$$$$$$  |\$$\  $$  |  $$ |  $$$$$\    $$ $$ $$\$$ |
$$ |  $$ | \$$\$$  / $$  __|   $$  __$$<  \$$\$$  /   $$ |  $$  __|   $$$$  _$$$$ |
$$ |  $$ |  \$$$  /  $$ |      $$ |  $$ |  \$$$  /    $$ |  $$ |      $$$  / \$$$ |
 $$$$$$  |   \$  /   $$$$$$$$\ $$ |  $$ |   \$  /   $$$$$$\ $$$$$$$$\ $$  /   \$$ |
 \______/     \_/    \________|\__|  \__|    \_/    \______|\________|\__/     \__|
//============================================================================//
//                        FUNCTION CALLSTACK OVERVIEW                         //
//============================================================================//
//----------------------------------------------------------------------------//
//----------------------------------------------------//TIMERS
//----------------------------------------------------//VARS
main.c

m_mokibo.c
	m_mokibo_status;

m_event.c
	m_event;
	m_arg;
	m_my_status;
	m_sub_status;

m_module_process()
	m_event_type_t event;
	m_event_arg_t argv;
	m_mokibo_status_type_t new;


//----------------------------------------------------//FUNCS
main.c
	int main(void);
	static void log_init(void);
	static void config_init(void);
	static void scheduler_init(void);
	static ret_code_t set_default(void);
	static void restoreFromFlash_or_setDefaults(void);
	static void startup_print(void);
	static void INFO_PRINT(void);
	static void STM8_VER(uint8_t ver, uint8_t *Maj, uint8_t *Min, uint8_t *Rev);
	static void power_manage(void);
m_mokibo.c
m_event.c
m_module_process()



//----------------------------------------------------//INIT
log_init();
startup_print();
m_mokibo_handle_reset();
{
	log_resetreason();		
	nrf_power_resetreas_clear(nrf_power_resetreas_get());
}

config_init();
{		
	if(Error == drv_fds_init())
	{
		drv_fds_erase()
		m_mokibo_WDT_reset(1)
	}
}

restoreFromFlash_or_setDefaults();
{
	restoreFromFlash_or_set_Defaults()
	{
		if(Succ == isValudFlash())
		{
			RestoreInfoFromFlash()
		}
		else
		{
			set_default()
			{
				rv = m_comm_adv_set_config_default();	
				rv += m_comm_set_config_default();	
				rv += m_mokibo_set_config_default();
			}
		}
	}
}

m_timer_init(); 
{	
	app_timer_init();
	app_timer_create(&m_timer_id, APP_TIMER_MODE_REPEATED, update);
}

m_touch_init();	
{		
	m_mode 				= M_TOUCH_CYPRESS_ACTIVE_AREA_WHOLE;
	m_state.jobState 	= job_state_idle;
	m_state.jobState_old = job_state_idle;
	m_state.jobState_oldold = job_state_idle;
	
	drv_twi_init(true);
	drv_cypress_init();
	drv_cypress_mode_set(DRV_CYPRESS_ACTIVE_AREA_WHOLE);
	
	memset(m_touch_db, 0, sizeof(m_touch_db));
	memset(&m_state, 0, sizeof(job_state_db_t));
}

m_keyboard_init();	
{   
	drv_keyboard_init(event_handler);
	{
		input_scan_vector 		= 0;
		
		for (uint_fast8_t i = 0; i<KEYBOARD_NUM_OF_COLUMNS; i++)
		{
			//as GPIO_PIN_CNF_DRIVE_S0S1 (normal cases) - Standard '0', standard '1'.. 
			nrf_gpio_cfg_output((uint32_t)columns[i]);				
			// Set pin to be "Disconnected 0 and 1"
			NRF_GPIO->PIN_CNF[(uint32_t)columns[i]] |= 0x400;					
			// Set pin to low
			nrf_gpio_pin_clear((uint32_t)columns[i]);
		}
		
		// row_pin_array: .... INPUT + PULL-DOWN
		for (uint_fast8_t i = 0; i<KEYBOARD_NUM_OF_ROWS; i++)
		{
			nrf_gpio_cfg_input((uint32_t)rows[i], NRF_GPIO_PIN_PULLDOWN);
			
			// Prepare the magic number
			input_scan_vector |= (1U << rows[i]);	
		}

		// Detect if any input pin is high
		if (((NRF_GPIO->IN) & input_scan_vector) != 0) 
		{
			// If inputs are not all low while output are, there must be something wrong
			DEB("drv_keyboard_init: ===ABNORMAL KEY INPUTS=== \n");
			return NRF_ERROR_INTERNAL;
		}
		else
		{
			// Clear the arrays
			prv_keys_n 		= 0;	
			pressed_keys_n 	= 0;
			
			for (uint_fast8_t i = 0; i<MAX_NUM_OF_KEYS; i++)
			{
				keys_buf[i] 	= 0;
				prv_keys_buf[i] = 0;
			}
		}

	init_hot_key();

	return NRF_SUCCESS;
}

m_event_init();		
{	
	m_event = M_EVENT_NONE;
	m_touch_lock_detect = 0;		
	memset(&m_arg, 0, sizeof(m_arg));
	memset(m_input, 0, sizeof(m_input));
}

m_mokibo_init();
{
	m_mokibo_status = M_MOKIBO_INIT_STATUS;
	m_prv_mokibo_status = m_mokibo_status;
	m_sub_states 	= PREPARE_STATE;
	m_sub_sate_argv = 0;

	drv_stm8_init();
	m_led_init();		

	m_timer_set(M_TIMER_ID_SLEEP_IF_IDLE, SLEEP_ON_IDLE_INTERVAL);
	power_on_reset_parade_touch();

	return NRF_SUCCESS;
}

m_comm_init();
{			
	ret_code_t       rv;
	ble_dis_init_t   dis_init_obj;
	ble_dis_pnp_id_t pnp_id;

	pnp_id.vendor_id_source = PNP_ID_VENDOR_ID_SOURCE;
	pnp_id.vendor_id        = PNP_ID_VENDOR_ID;
	pnp_id.product_id       = PNP_ID_PRODUCT_ID;
	pnp_id.product_version  = PNP_ID_PRODUCT_VERSION;

	memset(&dis_init_obj, 0, sizeof(dis_init_obj));

	ble_srv_ascii_to_utf8(&dis_init_obj.manufact_name_str, MANUFACTURER_NAME);
	dis_init_obj.p_pnp_id = &pnp_id;

	BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&dis_init_obj.dis_attr_md.read_perm);
	BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&dis_init_obj.dis_attr_md.write_perm);		
	rv = ble_dis_init(&dis_init_obj); APP_ERROR_CHECK(rv);
}

scheduler_init();
{
	APP_SCHED_INIT()
}
m_timer_start();

INFO_PRINT();
{
	m_mokibo_get_manufacture_info(&info);
	st8ver= m_mokibo_get_stm8_fw_version();
	drv_cypress_get_fw_version()
}








//----------------------------------------------------//LOOP
for (;;)
{
	app_sched_execute();

	m_mokibo_process()
	{			
		func::m_event_arg_t 	argv;//새로만듬
		func::m_event_type_t 	event;//새로만듬/
		
		var isUpdate = m_event_update();
		{   
			m_event::m_event = M_EVENT_NONE;//이전 이벤트취급. 클리어
			m_event::update_status();
			{
				m_mokibo_status_type_t new; //새로만듬
				new = m_mokibo::m_mokibo_status; //m_mokibo 로부터 status가져옴. 왜 얘가 최신이지?

				if(new != m_my_status)	//바뀌었으면
				{
					m_my_status 	= new;				//교체
					m_sub_status 	= SUB_STATUS_READY;	//sub_status 초기화					
				}
			}

			m_event::m_event_handler[m_my_status]();
			{
				m_event::init_event_detect()
				{
					switch(m_sub_status)
					{
						case SUB_STATUS_READY:
							m_event |= M_EVENT_STARTUP;
							m_sub_status |= SUB_STATUS_RUN;
						case SUB_STATUS_RUN:
							m_event	|= M_EVENT_KEYBOARD_MOCE_SCAN_TIME;
							m_event |= M_EVENT_MODE_CHANGED;
							...
					}
				}
				m_event::manufacture_event_detect(void)
				m_event::keyboard_event_detect(void)
				m_event::touch_event_detect(void)
				m_event::upgrade_event_detect(void)
				m_event::sleep_event_detect(void)
				m_event::test_ebattery_check_event_detectvent_detect(void)
				m_event::(void)
				m_event::end_event_detect(void)					
			};
			return (M_EVENT_NONE != m_event::m_event) ? 1 : 0; // NONE 이면 false 얘가 isUpdate값임.
		}

		if(true == isUpdate) //m_event_update() 에서 m_event::m_mystatus==m_mokibo::m_mokibo_status 가 NONE이 아닐때
		{
			m_event::m_event_get(func::&event, func::&argv);
			{
				func::*event= m_event::m_event;
				func::*argv = m_event::m_arg;
				
				m_event::m_event = M_EVENT_NONE;	//m_event 초기화
				memset(&m_arg, 0, sizeof(m_arg)); 	//m_arg 초기화
			}

			m_mokibo::m_mokibo_handler[m_mokibo_status](func::event, func::argv);	 // 쥰나실행
			{
				m_mokibo::InitHandler(m_event_type_t evt, m_event_arg_t argv)
				{
					if(evt & M_EVENT_mODE_CHANGED)
					{
						if(argv.new_ode.manufacture)
						{
							ChangeState(new:M_MOKIBO_MANUFATURE_STATUS);
							{
								m_prv_mokibo_status = m_mokibo_status;	// Remeber the current as previous
								m_mokibo_status		= new;				// Remeber the new as current
								m_sub_states 		= PREPARE_STATE;	// Reset m_sub_states
								m_sub_sate_argv		= 0;				// Reset m_sub_sate_argv
							}
						}
						if(argv.new_mode.keyboard)
						{
							ChangeState(M_MOKIBO_KEYBOARD_STATUS);
						}
					}
				}
				m_mokibo::ManufactureHandler(m_event_type_t evt, m_event_arg_t argv)
				m_mokibo::KeyboardHandler(m_event_type_t evt, m_event_arg_t argv)
				{					
					if(evt & M_EVENT_TOUCH_MODE_SCAN_TIME)
					{				
						m_touch_scan_handler();
					}
					if(evt & M_EVENT_KEYBOAD_MODE_SCAN_TIME)
					{
						keyboard_scan_timeout_handler();
					}
					if(evt & M_EVENT_MODE_CHANGED)
					{
						if(argv.new_mode.sleep)
						{ 	
							ChangeState(M_MOKIBO_SLEEP_STATUS);
						}

						if(argv.new_mode.battery_chk) 
						{ 	
							ChangeState(M_MOKIBO_BATTERY_CHECK_STATUS);
						}
					}

					if(evt & M_EVENT_VOLUME_CHANGED)
					{
						memset(&val, 0, sizeof(val)); // clear
						if(argv.volume.down)	{ val.volume.down = drv_keyboard_get_volume(S2L(V_DOWN)) ? 1 : 0; }
						if(argv.volume.up) 		{ val.volume.up = drv_keyboard_get_volume(S2L(V_UP)) ? 1 : 0; }
						if(argv.volume.mute)	{val.volume.mute = drv_keyboard_get_volume(S2L(V_MUTE)) ? 1 : 0; }
						m_comm_send_event(M_COMM_EVENT_VOLUME_CTRL, val);
					}					
					if(evt & M_EVENT_CHANNEL_CHANGED)
					{
						if(argv.channel.bt1) 	{ m_comm_cmd(M_COMM_CMD_CH_CHANGE, M_COMM_CHANNEL_BT1); }
						if(argv.channel.bt2) 	{ m_comm_cmd(M_COMM_CMD_CH_CHANGE, M_COMM_CHANNEL_BT2); }
						if(argv.channel.bt3) 	{ m_comm_cmd(M_COMM_CMD_CH_CHANGE, M_COMM_CHANNEL_BT3); }				
					}
				}
				m_mokibo::TouchHandler(m_event_type_t evt, m_event_arg_t argv)
				m_mokibo::UpgradeHandler(m_event_type_t evt, m_event_arg_t argv)
				m_mokibo::SleepHandler(m_event_type_t evt, m_event_arg_t argv)
				m_mokibo::TestHandler(m_event_type_t evt, m_event_arg_t argv)
				m_mokibo::BatteryCheckHandler(m_event_type_t evt, m_event_arg_t argv)
				{
					m_battery_handler[m_sub_states]();
				}
				m_mokibo::EndHandler(m_event_type_t evt, m_event_arg_t argv)
			}				
		}		
		void m_timer_update_handler(void)
		{
			uint32_t  index;				
			for(index = 0U; index < M_TIMER_ID_MAX; index++)
			{					
				if(!m_pool[index])
				{
					if(NULL != m_handlers[index])
					{
						m_handlers[index]();
						m_handlers[index] = NULL;
					}
				}
			}
		}
		Update_WDT();
	}

	if (NRF_LOG_PROCESS() == false)
	{
		power_manage();
		{
			sd_app_evt_wait();
		}
	}
}


//----------------------------------------------------//LOGS
m_mokibo_status -> 
new ->
 m_my_status,m_sub_status -> 
 m_event_handler[m_my_status] -> 
 m_event, m_sub_status, m_arg.newmode=1 ->
 m_event, m_arg ->
 event, argv -> 

//----------------------------------------------------------------------------*/





/*\      $$\  $$$$$$\  $$$$$$$\  $$\   $$\ $$\       $$$$$$$$\  $$$$$$\  
$$$\    $$$ |$$  __$$\ $$  __$$\ $$ |  $$ |$$ |      $$  _____|$$  __$$\ 
$$$$\  $$$$ |$$ /  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$ /  \__|
$$\$$\$$ $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$$$$\    \$$$$$$\  
$$ \$$$  $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$  __|    \____$$\ 
$$ |\$  /$$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$\   $$ |
$$ | \_/ $$ | $$$$$$  |$$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$$\ \$$$$$$  |
\__|     \__| \______/ \_______/  \______/ \________|\________| \______/ 
//============================================================================//
//                                 MODULES USED                               //
//============================================================================*/
#include "sdk_config.h"	// +also included "mokibo_config.h"
#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)	
	#include "SEGGER_RTT.h"// should be placed after "sdk_config.h"
#endif
#include "app_scheduler.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "drv_fds.h"
#include "m_event.h"
#include "m_comm.h"
#include "m_keyboard.h"
#include "m_touch.h"
#include "m_timer.h"	
#include "m_mokibo.h"
#include "m_util.h"
#include "m_led.h"
#include "m_dongle.h"
#include "nrf_delay.h"



 /*$$$$\   $$$$$$\  $$\   $$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$\   $$\ $$$$$$$$\ 
$$  __$$\ $$  __$$\ $$$\  $$ |$$  __$$\\__$$  __|$$  __$$\ $$$\  $$ |\__$$  __|
$$ /  \__|$$ /  $$ |$$$$\ $$ |$$ /  \__|  $$ |   $$ /  $$ |$$$$\ $$ |   $$ |   
$$ |      $$ |  $$ |$$ $$\$$ |\$$$$$$\    $$ |   $$$$$$$$ |$$ $$\$$ |   $$ |   
$$ |      $$ |  $$ |$$ \$$$$ | \____$$\   $$ |   $$  __$$ |$$ \$$$$ |   $$ |   
$$ |  $$\ $$ |  $$ |$$ |\$$$ |$$\   $$ |  $$ |   $$ |  $$ |$$ |\$$$ |   $$ |   
\$$$$$$  | $$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$ |  $$ |$$ | \$$ |   $$ |   
 \______/  \______/ \__|  \__| \______/   \__|   \__|  \__|\__|  \__|   \__|   
//============================================================================//
//                                LOCAL CONSTANTS                             //
//============================================================================*/
#define SCHED_MAX_EVENT_DATA_SIZE           APP_TIMER_SCHED_EVENT_DATA_SIZE	/**< Maximum size of scheduler events. */
#ifdef SVCALL_AS_NORMAL_FUNCTION
	#define SCHED_QUEUE_SIZE                    20	/**< Maximum number of events in the scheduler queue. More is needed in case of Serialization. */
#else	
	#if (MOKIBO_NFC_ENABLED)
		#define SCHED_QUEUE_SIZE                    30
	#else
		#define SCHED_QUEUE_SIZE                    20
#endif
#endif




/*$$$$$$\ $$\     $$\ $$$$$$$\  $$$$$$$$\ $$$$$$$\  $$$$$$$$\ $$$$$$$$\  $$$$$$\  
\__$$  __|\$$\   $$  |$$  __$$\ $$  _____|$$  __$$\ $$  _____|$$  _____|$$  __$$\ 
   $$ |    \$$\ $$  / $$ |  $$ |$$ |      $$ |  $$ |$$ |      $$ |      $$ /  \__|
   $$ |     \$$$$  /  $$$$$$$  |$$$$$\    $$ |  $$ |$$$$$\    $$$$$\    \$$$$$$\  
   $$ |      \$$  /   $$  ____/ $$  __|   $$ |  $$ |$$  __|   $$  __|    \____$$\ 
   $$ |       $$ |    $$ |      $$ |      $$ |  $$ |$$ |      $$ |      $$\   $$ |
   $$ |       $$ |    $$ |      $$$$$$$$\ $$$$$$$  |$$$$$$$$\ $$ |      \$$$$$$  |
   \__|       \__|    \__|      \________|\_______/ \________|\__|       \______/ 
//============================================================================//
//                                LOCAL TYPEDEFS                              //
//============================================================================*/
























/*\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ 
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\_*/

/*\    $$\  $$$$$$\  $$$$$$$\  $$$$$$\  $$$$$$\  $$$$$$$\  $$\       $$$$$$$$\ 
$$ |   $$ |$$  __$$\ $$  __$$\ \_$$  _|$$  __$$\ $$  __$$\ $$ |      $$  _____|
$$ |   $$ |$$ /  $$ |$$ |  $$ |  $$ |  $$ /  $$ |$$ |  $$ |$$ |      $$ |      
\$$\  $$  |$$$$$$$$ |$$$$$$$  |  $$ |  $$$$$$$$ |$$$$$$$\ |$$ |      $$$$$\    
 \$$\$$  / $$  __$$ |$$  __$$<   $$ |  $$  __$$ |$$  __$$\ $$ |      $$  __|   
  \$$$  /  $$ |  $$ |$$ |  $$ |  $$ |  $$ |  $$ |$$ |  $$ |$$ |      $$ |      
   \$  /   $$ |  $$ |$$ |  $$ |$$$$$$\ $$ |  $$ |$$$$$$$  |$$$$$$$$\ $$$$$$$$\ 
    \_/    \__|  \__|\__|  \__|\______|\__|  \__|\_______/ \________|\_______/
//============================================================================//
//                                   VARIABLES                                //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                                GLOBAL VARIABLES                            //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                                 LOCAL VARIABLES                            //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                                 DEBUG VARIABLES                            //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                                UNUSED VARIABLES                            //
//----------------------------------------------------------------------------*/




/*$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\  $$\   $$\ $$$$$$$$\  $$$$$$\  $$$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\ $$ |  $$ |$$  _____|$$  __$$\ $$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|$$ |  $$ |$$ |      $$ /  $$ |$$ |  $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |      $$$$$$$$ |$$$$$\    $$$$$$$$ |$$ |  $$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |      $$  __$$ |$$  __|   $$  __$$ |$$ |  $$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\ $$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |$$ |  $$ |$$$$$$$$\ $$ |  $$ |$$$$$$$  |
\__|       \______/ \__|  \__| \______/ \__|  \__|\________|\__|  \__|\_______/ 
//============================================================================//
//                                   FUNCHEAD                                 //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                            GLOBAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
int main(void);
/*----------------------------------------------------------------------------//
//                             LOCAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
static void log_init(void);
static void config_init(void);
static void scheduler_init(void);
static ret_code_t set_default(void);
static void restoreFromFlash_or_setDefaults(void);
static void startup_print(void);
static void INFO_PRINT(void);
static void STM8_VER(uint8_t ver, uint8_t *Maj, uint8_t *Min, uint8_t *Rev);
static void power_manage(void);
/*----------------------------------------------------------------------------//
//                            POINTER FUNCTION HEADS                          //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                             DEBUG FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                             UNUSED FUNCTION HEADS                          //
//----------------------------------------------------------------------------*/


/*\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ 
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\_*/















 /*$$$$\  $$\       $$$$$$\  $$$$$$$\   $$$$$$\  $$\                         
$$  __$$\ $$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                        
$$ /  \__|$$ |     $$ /  $$ |$$ |  $$ |$$ /  $$ |$$ |                        
$$ |$$$$\ $$ |     $$ |  $$ |$$$$$$$\ |$$$$$$$$ |$$ |                        
$$ |\_$$ |$$ |     $$ |  $$ |$$  __$$\ $$  __$$ |$$ |                        
$$ |  $$ |$$ |     $$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |                        
\$$$$$$  |$$$$$$$$\ $$$$$$  |$$$$$$$  |$$ |  $$ |$$$$$$$$\                   
 \______/ \________|\______/ \_______/ \__|  \__|\________|                  
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\ 
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__|
//============================================================================//
//                                 GLOBAL FUNCTIONS                           //
//============================================================================*/


//================================================================================
//	Function name : main()
//================================================================================
int main(void)
{
	ret_code_t ret;
	
    log_init();	
	startup_print();
		
	m_mokibo_handle_reset();	// reset reason information

	config_init();

 	ret = m_timer_init(); APP_ERROR_CHECK(ret);
	ret = m_timer_start(); APP_ERROR_CHECK(ret);

	ret = m_event_init(); APP_ERROR_CHECK(ret);
	ret = m_mokibo_init(); APP_ERROR_CHECK(ret); //// <--- Watchdog 초기화가 들어 있으나 이를 막고 main loop 직전으로 옮김.
	ret = m_comm_init(); APP_ERROR_CHECK(ret);

	scheduler_init();


	#ifdef TRACE_FLASH_SAVED_INFO
		ShowTotInfo("main:");//FDS.c
	#endif //TRACE_FLASH_SAVED_INFO
	
	
	________________________________LOG_mMokibo_mokiboStatus(0, "1] BT Ch: %d", m_config.config.selected_ch);
	
	if( is_BT_channel( m_config.config.selected_ch ) )
	{
		m_bt_ch= m_config.config.selected_ch;
	} 
	else 
	{
		m_bt_ch= m_config.config.selected_ch = 0;
	}	
	m_comm_cmd(M_COMM_CMD_CH_CHANGE, m_bt_ch);
	
	________________________________LOG_mMokibo_mokiboStatus(0, "2] BT Ch: %d", m_config.config.selected_ch);

	INFO_PRINT();
	
	// Initialize WDT here
	// Be careful!!!, If WDT is started, there is no way to disable WDT
	// Even though PC is jumped into bootloader, WDT is still running
	// So WTD should be reloaded in bootoader
	Init_WDT();	// <--- Watchdog을 m_mokibo()에서 제거하고 이쪽으로 옮김 ---

	for ( ; ; )
    {
		________________________________LOG_MainLoop_FuncHeads(FUNCHEAD_FLAG,"################## MAIN LOOP ################");
		app_sched_execute();
				
		m_mokibo_process();

		if (NRF_LOG_PROCESS() == false)
		{
			power_manage();
		}
    }
}







/*\       $$$$$$\   $$$$$$\   $$$$$$\  $$\                                             
$$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                                            
$$ |     $$ /  $$ |$$ /  \__|$$ /  $$ |$$ |                                            
$$ |     $$ |  $$ |$$ |      $$$$$$$$ |$$ |                                            
$$ |     $$ |  $$ |$$ |      $$  __$$ |$$ |                                            
$$ |     $$ |  $$ |$$ |  $$\ $$ |  $$ |$$ |                                            
$$$$$$$$\ $$$$$$  |\$$$$$$  |$$ |  $$ |$$$$$$$$\                                       
\________|\______/  \______/ \__|  \__|\________|                                      
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\  $$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |$$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |$$ /  \__|
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |\$$$$$$\  
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ | \____$$\ 
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |$$\   $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |\$$$$$$  |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__| \______/ 
//============================================================================//
//                                 LOCAL FUNCTIONS                            //
//============================================================================*/


//================================================================================
//	Function name : log_init(void)
//================================================================================
static void log_init(void)
{	
	//________________________________LOG_Main_FuncHeads(FUNCHEAD_FLAG,"(void)");//
    ret_code_t err_code = NRF_LOG_INIT(NULL); APP_ERROR_CHECK(err_code);
    NRF_LOG_DEFAULT_BACKENDS_INIT();
}



//================================================================================
//	Function name : config_init(void)
//================================================================================
static void config_init(void)
{			
	________________________________LOG_Main_FuncHeads(FUNCHEAD_FLAG,"(void)");
	ret_code_t 	ret= 1;
	
	ret = drv_fds_init();

	if (ret != NRF_SUCCESS)
	{
		#if ________________________________DBG_flimbs_20190307_drvFds_BootFailAfterDrvFdsInit_DelFdsIfError == 1
			________________________________DBG_flimbs_20190307_drvFds_BootFailAfterDrvFdsInit(1,"if (ret:%d != NRF_SUCCESS:%d)",ret,NRF_SUCCESS);
		#endif
		
		drv_fds_erase();
		set_default();
		
		//#if (MOKIBO_WDT_ENABLED)
		//m_mokibo_WDT_reset(1);
		//#endif
		//NVIC_SystemReset();
	}
	else
	{
		restoreFromFlash_or_setDefaults();
	}
}



//================================================================================
//	Function name : scheduler_init(void)
//================================================================================
static void scheduler_init(void)
{
	________________________________LOG_Main_FuncHeads(FUNCHEAD_FLAG,"(void)");
    APP_SCHED_INIT(SCHED_MAX_EVENT_DATA_SIZE, SCHED_QUEUE_SIZE);
}



//================================================================================
//	Function name : set_default(void)
//================================================================================
static ret_code_t set_default(void)
{	
	________________________________LOG_Main_FuncHeads(FUNCHEAD_FLAG,"(void)");
	ret_code_t rv;
	rv = m_comm_adv_set_config_default();	
	rv += m_comm_set_config_default();	
	rv += m_mokibo_set_config_default();
	return rv;
}




//================================================================================
//	Function name : restoreFromFlash_or_setDefaults(void)
//================================================================================
static void restoreFromFlash_or_setDefaults(void)
{	
	________________________________LOG_Main_FuncHeads(FUNCHEAD_FLAG,"(void)");
	if(IsValidFlash())
	{		
		RestoreInfoFromFlash();
	} else 
	{		
		set_default();
	}	
}




//================================================================================
//	Function name : startup_print(void)
//================================================================================
static void startup_print(void)
{		
	________________________________LOG_Main_FuncHeads(FUNCHEAD_FLAG,"(void)");
	________________________________LOG_mMokibo_showInitInfo(1,"********************************************************************");
	________________________________LOG_mMokibo_showInitInfo(1,"*                          MOKIOBO                                 *");
	________________________________LOG_mMokibo_showInitInfo(1,"********************************************************************");
	________________________________LOG_mMokibo_showInitInfo(1,"* MainPBA Version : %s ", 		MOKIBO_HW_VERSION				 );
	________________________________LOG_mMokibo_showInitInfo(1,"* MainPBA ID      : 0x%08x ", 	m_util_get_unique_device_id()	 );
	________________________________LOG_mMokibo_showInitInfo(1,"* FW Version      : %s ", 		MOKIBO_FW_VERSION				 );
	________________________________LOG_mMokibo_showInitInfo(1,"* Build Date/Time : %s / %s ", 	__DATE__, __TIME__				 );
	________________________________LOG_mMokibo_showInitInfo(1,"********************************************************************");
	//[PBA_ver:%s, PBA_id:0x%08x] [FW_ver:%s, FW_date:%s/%s]
	char tmp_message[256]=0;
	sprintf(tmp_message,"[NRFFW_ver:%s] [NRFFW_date:%s/%s] [LAYOUT:%d] [PBA_ver:%s] [PBA_id:0x%08X]"
	,MOKIBO_FW_VERSION
	,__DATE__,__TIME__
	,LAYOUT_OPTION
	,MOKIBO_HW_VERSION
	,m_util_get_unique_device_id());
	strcat(m_version_message, tmp_message);
}



//================================================================================
//	Function name : INFO_PRINT(void)
//================================================================================
static void INFO_PRINT(void)
{
	________________________________LOG_Main_FuncHeads(FUNCHEAD_FLAG,"(void)");
	// 1. (battery level과 version 정보는, 1-byte 그대로 전달된다)
	// 2. STM8 Version ::=  Maj(3bits):Min(3bits):Rev(2bits) = MMM:mmm:rr
	
	uint8_t st8ver,maj=0, min=0, rev=0;
	m_mokibo_manufacture_info_t info;
	
	memset(&info, 0, sizeof(info));
	m_mokibo_get_manufacture_info(&info);
	
	st8ver= m_mokibo_get_stm8_fw_version();
	STM8_VER(st8ver, &maj, &min, &rev);
	
	________________________________LOG_mMokibo_showInitInfo(1,"********************************************************************");
	________________________________LOG_mMokibo_showInitInfo(1,"* Clickbar FW Ver : 0x%02x(V%d.%d.%d) ", st8ver, maj,min,rev		);
	________________________________LOG_mMokibo_showInitInfo(1,"* Touch FW Ver    : 0x%08x : FW1030 ", drv_cypress_get_fw_version()	);
	________________________________LOG_mMokibo_showInitInfo(1,"* Serial number   : 0x%04x ", 			info.serial					);
	________________________________LOG_mMokibo_showInitInfo(1,"* Assemble Date   : 0x%04x ", 			info.date					);
	________________________________LOG_mMokibo_showInitInfo(1,"********************************************************************");		

	char tmp_message[256]=0;
	sprintf(tmp_message,"[CLKBARFW_ver:0x%02x_V%d.%d.%d] [TCHFW_ver: 0x%08X] [SN:0x%04X] [ASMN:0x%04X]"
	, st8ver
	, maj,min,rev 
	, drv_cypress_get_fw_version
	, info.serial
	, info.date);
	strcat(m_version_message, tmp_message);

	//[PBA_ver:%s, PBA_id:0x%08x] [FW_ver:%s, FW_date:%s/%s]
	//+
	//", [CLKBARFW_ver:0x%02x(V%d.%d.%d)], [TCHFW_ver: 0x%08x], [SN:0x%04x], [ASMN:0x%04x]", st8ver, maj,min,rev , drv_cypress_get_fw_version,info.serial, info.date)
}



//================================================================================
//	Function name : STM8_VER(uint8_t ver, uint8_t *Maj, uint8_t *Min, uint8_t *Rev)
//================================================================================
static void STM8_VER(uint8_t ver, uint8_t *Maj, uint8_t *Min, uint8_t *Rev)
{	
	// 2. STM8 Version ::=  Maj(3bits):Min(3bits):Rev(2bits) = MMM:mmm:rr	
	*Maj= ((ver & 0xE0))>>5 ;	// MMM- ----
	*Min= ((ver & 0x1C))>>2 ;	// ---m mm--
	*Rev= ((ver & 0x03))>>0 ;	// ---- --rr
}





//================================================================================
//	Function name : power_manage(void)
//================================================================================
static void power_manage(void)
{// Function for the Power manager.
	// BLE mode? or Gazell mode?
	//________________________________LOG_Main_FuncHeads(FUNCHEAD_FLAG,"(void)");//POLLING
	m_comm_mode_t mode = m_comm_get_mode();

	if(M_COMM_MODE_BLE == mode)
	{
		ret_code_t err_code = sd_app_evt_wait();
			//see 'nrf_soc.c':
			//		uint32_t sd_app_evt_wait(void)
			//		{
			//			__WFE();
			//			return NRF_SUCCESS;
			//		}
		APP_ERROR_CHECK(err_code);
		
	} 
	else if(M_COMM_MODE_GAZELL == mode)
	{
		//DEB("power_manage: --in GAZELL--\n");		
        // Use directly __WFE and __SEV macros
		// since the SoftDevice is not available in proprietary mode.
        __WFE();	// Wait for event.
        __SEV();	// Clear Event Register.
	}
}





