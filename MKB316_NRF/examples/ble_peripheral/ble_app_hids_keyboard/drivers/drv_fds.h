/// \file drv_fds.h
/// This file contains all required configurations for mokibo
/*==============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
==============================================================================*/


/*\   $$\ $$$$$$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$$$$$$\ $$\     $$\          
$$ |  $$ |\_$$  _|$$  __$$\\__$$  __|$$  __$$\ $$  __$$\\$$\   $$  |         
$$ |  $$ |  $$ |  $$ /  \__|  $$ |   $$ /  $$ |$$ |  $$ |\$$\ $$  /          
$$$$$$$$ |  $$ |  \$$$$$$\    $$ |   $$ |  $$ |$$$$$$$  | \$$$$  /           
$$  __$$ |  $$ |   \____$$\   $$ |   $$ |  $$ |$$  __$$<   \$$  /            
$$ |  $$ |  $$ |  $$\   $$ |  $$ |   $$ |  $$ |$$ |  $$ |   $$ |             
$$ |  $$ |$$$$$$\ \$$$$$$  |  $$ |    $$$$$$  |$$ |  $$ |   $$ |             
\__|  \__|\______| \______/   \__|    \______/ \__|  \__|   \__|             
//============================================================================//
//                            REVISION HISTORY                                //
//============================================================================//
//----------------------------------------------------------------------------//
20171210_Seo
	Created initial version

20180126_MJ
	Created DRV_FDS_ACCESS_TYPE_T

20180129_MJ
	Created drv_fds_config_default()

20180131_MJ
	Changed FDS_FILE_ID, FDS_REC_KEY

20180220_MJ
	Created drv_fds_config_flush()

20180227_MJ
	Changed central from M_COMM_CENTRAL_MICROSOFT to M_COMM_CENTRAL_OTHER

20180302_MJ
	Made drv_fds simple to use by page access

20180307_MJ
	Decreased total size from 256 to 64
	Added DRV_FDS_PAGE_M_DONGLE_CONFIG
	Refer to "drv_fds_map_rev1.1.xlsx"

20180409_MJ
	Removed DRV_FDS_PAGE_M_DONGLE_CONFIG
//----------------------------------------------------------------------------*/






/*$$$$$\  $$\    $$\ $$$$$$$$\ $$$$$$$\  $$\    $$\ $$$$$$\ $$$$$$$$\ $$\      $$\ 
$$  __$$\ $$ |   $$ |$$  _____|$$  __$$\ $$ |   $$ |\_$$  _|$$  _____|$$ | $\  $$ |
$$ /  $$ |$$ |   $$ |$$ |      $$ |  $$ |$$ |   $$ |  $$ |  $$ |      $$ |$$$\ $$ |
$$ |  $$ |\$$\  $$  |$$$$$\    $$$$$$$  |\$$\  $$  |  $$ |  $$$$$\    $$ $$ $$\$$ |
$$ |  $$ | \$$\$$  / $$  __|   $$  __$$<  \$$\$$  /   $$ |  $$  __|   $$$$  _$$$$ |
$$ |  $$ |  \$$$  /  $$ |      $$ |  $$ |  \$$$  /    $$ |  $$ |      $$$  / \$$$ |
 $$$$$$  |   \$  /   $$$$$$$$\ $$ |  $$ |   \$  /   $$$$$$\ $$$$$$$$\ $$  /   \$$ |
 \______/     \_/    \________|\__|  \__|    \_/    \______|\________|\__/     \__|
//============================================================================//
//                        FUNCTION CALLSTACK OVERVIEW                         //
//============================================================================//
//----------------------------------------------------------------------------//

//----------------------------------------------------//TIMERS
//----------------------------------------------------//VARS
//----------------------------------------------------//FUNCS
//----------------------------------------------------//INIT
//----------------------------------------------------//LOOP

//----------------------------------------------------------------------------*/







/*\      $$\  $$$$$$\  $$$$$$$\  $$\   $$\ $$\       $$$$$$$$\  $$$$$$\  
$$$\    $$$ |$$  __$$\ $$  __$$\ $$ |  $$ |$$ |      $$  _____|$$  __$$\ 
$$$$\  $$$$ |$$ /  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$ /  \__|
$$\$$\$$ $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$$$$\    \$$$$$$\  
$$ \$$$  $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$  __|    \____$$\ 
$$ |\$  /$$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$\   $$ |
$$ | \_/ $$ | $$$$$$  |$$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$$\ \$$$$$$  |
\__|     \__| \______/ \_______/  \______/ \________|\________| \______/ 
//============================================================================//
//                                 MODULES USED                               //
//============================================================================*/
#ifndef __M_FDS_H
#define __M_FDS_H    
#include "fds.h"






 /*$$$$\   $$$$$$\  $$\   $$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$\   $$\ $$$$$$$$\ 
$$  __$$\ $$  __$$\ $$$\  $$ |$$  __$$\\__$$  __|$$  __$$\ $$$\  $$ |\__$$  __|
$$ /  \__|$$ /  $$ |$$$$\ $$ |$$ /  \__|  $$ |   $$ /  $$ |$$$$\ $$ |   $$ |   
$$ |      $$ |  $$ |$$ $$\$$ |\$$$$$$\    $$ |   $$$$$$$$ |$$ $$\$$ |   $$ |   
$$ |      $$ |  $$ |$$ \$$$$ | \____$$\   $$ |   $$  __$$ |$$ \$$$$ |   $$ |   
$$ |  $$\ $$ |  $$ |$$ |\$$$ |$$\   $$ |  $$ |   $$ |  $$ |$$ |\$$$ |   $$ |   
\$$$$$$  | $$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$ |  $$ |$$ | \$$ |   $$ |   
 \______/  \______/ \__|  \__| \______/   \__|   \__|  \__|\__|  \__|   \__|   
 //============================================================================//
//                                 GLOBAL CONSTANTS                            //
//=============================================================================*/
#define DRV_FDS_CONFIG_VALID			0x1234FE00

#define DRV_FDS_SMALL_PAGE_SIZE			4
#define DRV_FDS_MEDIUM_PAGE_SIZE		8
#define DRV_FDS_LARGE_PAGE_SIZE			32
#define DRV_FDS_TOTAL_SIZE				64

// User configuration
#define DRV_FDS_CONFIG_VALID_SIZE		DRV_FDS_SMALL_PAGE_SIZE
#define DRV_FDS_TOUCH_VER_SIZE			DRV_FDS_SMALL_PAGE_SIZE
#define DRV_FDS_USER_TEST_SIZE			DRV_FDS_SMALL_PAGE_SIZE
#define DRV_FDS_MANUFACTURE_SIZE		DRV_FDS_SMALL_PAGE_SIZE	
#define DRV_FDS_DONGLE_SIZE				DRV_FDS_SMALL_PAGE_SIZE	
#define DRV_FDS_M_COMM_CONFIG_SIZE		DRV_FDS_MEDIUM_PAGE_SIZE
#define DRV_FDS_M_COMM_ADV_SIZE			DRV_FDS_LARGE_PAGE_SIZE

#define DRV_FDS_PAGE_CONFIG_VALID		drv_fds_small_page_0
#define DRV_FDS_PAGE_TOUCH_VER			drv_fds_small_page_1
#define DRV_FDS_PAGE_TEST				drv_fds_small_page_2
#define DRV_FDS_PAGE_MANUFACTURE		drv_fds_small_page_3
#define DRV_FDS_PAGE_M_RESERVED_0		drv_fds_small_page_4
#define DRV_FDS_PAGE_M_RESERVED_1		drv_fds_small_page_5
#define DRV_FDS_PAGE_M_COMM_CONFIG		drv_fds_medium_page_0
#define DRV_FDS_PAGE_M_COMM_ADV_CONFIG 	drv_fds_large_page_0





/*$$$$$$\ $$\     $$\ $$$$$$$\  $$$$$$$$\ $$$$$$$\  $$$$$$$$\ $$$$$$$$\  $$$$$$\  
\__$$  __|\$$\   $$  |$$  __$$\ $$  _____|$$  __$$\ $$  _____|$$  _____|$$  __$$\ 
   $$ |    \$$\ $$  / $$ |  $$ |$$ |      $$ |  $$ |$$ |      $$ |      $$ /  \__|
   $$ |     \$$$$  /  $$$$$$$  |$$$$$\    $$ |  $$ |$$$$$\    $$$$$\    \$$$$$$\  
   $$ |      \$$  /   $$  ____/ $$  __|   $$ |  $$ |$$  __|   $$  __|    \____$$\ 
   $$ |       $$ |    $$ |      $$ |      $$ |  $$ |$$ |      $$ |      $$\   $$ |
   $$ |       $$ |    $$ |      $$$$$$$$\ $$$$$$$  |$$$$$$$$\ $$ |      \$$$$$$  |
   \__|       \__|    \__|      \________|\_______/ \________|\__|       \______/ 
//============================================================================//
//                                 GLOBAL TYPEDEFS                            //
//============================================================================*/
typedef enum
{
    drv_fds_small_page_0 = 0,
    drv_fds_small_page_1,
    drv_fds_small_page_2,
    drv_fds_small_page_3,
    drv_fds_small_page_4,
    drv_fds_small_page_5,
    drv_fds_medium_page_0,
    drv_fds_large_page_0,
	//
    drv_fds_max_page,
} drv_fds_page_t;





/*\    $$\  $$$$$$\  $$$$$$$\  $$$$$$\  $$$$$$\  $$$$$$$\  $$\       $$$$$$$$\ 
$$ |   $$ |$$  __$$\ $$  __$$\ \_$$  _|$$  __$$\ $$  __$$\ $$ |      $$  _____|
$$ |   $$ |$$ /  $$ |$$ |  $$ |  $$ |  $$ /  $$ |$$ |  $$ |$$ |      $$ |      
\$$\  $$  |$$$$$$$$ |$$$$$$$  |  $$ |  $$$$$$$$ |$$$$$$$\ |$$ |      $$$$$\    
 \$$\$$  / $$  __$$ |$$  __$$<   $$ |  $$  __$$ |$$  __$$\ $$ |      $$  __|   
  \$$$  /  $$ |  $$ |$$ |  $$ |  $$ |  $$ |  $$ |$$ |  $$ |$$ |      $$ |      
   \$  /   $$ |  $$ |$$ |  $$ |$$$$$$\ $$ |  $$ |$$$$$$$  |$$$$$$$$\ $$$$$$$$\ 
    \_/    \__|  \__|\__|  \__|\______|\__|  \__|\_______/ \________|\________|
/*============================================================================//
//                            GLOBAL  VARIABLES                               //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                                GLOBAL VARIABLES                            //
//----------------------------------------------------------------------------*/
extern bool m_fds_evt_refreshed;
extern fds_evt_t m_fds_evt_last;
/*----------------------------------------------------------------------------//
//                             GLOBAL DEBUG VARIABLES                         //
//----------------------------------------------------------------------------*/





/*$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\  $$\   $$\ $$$$$$$$\  $$$$$$\  $$$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\ $$ |  $$ |$$  _____|$$  __$$\ $$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|$$ |  $$ |$$ |      $$ /  $$ |$$ |  $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |      $$$$$$$$ |$$$$$\    $$$$$$$$ |$$ |  $$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |      $$  __$$ |$$  __|   $$  __$$ |$$ |  $$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\ $$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |$$ |  $$ |$$$$$$$$\ $$ |  $$ |$$$$$$$  |
\__|       \______/ \__|  \__| \______/ \__|  \__|\________|\__|  \__|\_______/ 
/*============================================================================//
//                                 GLOBAL FUNCTIONS                           //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                            GLOBAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
extern ret_code_t drv_fds_init (void);

extern void reset_fds_op_trace(void);
extern bool is_fds_op_finshed(fds_evt_id_t id);

extern ret_code_t fds_write_stanley(uint16_t file_id, uint16_t record_key, const uint8_t *pUserData, uint16_t size_in_bytes);
extern ret_code_t fds_read_stanley(uint16_t file_id,uint16_t record_key,uint16_t size_in_bytes, uint8_t *pUserBuff);
extern ret_code_t fds_delete_stanley(uint16_t file_id,	uint16_t record_key);

extern bool IsValidFlash(void);

// m_Info--> Flash
extern void SaveInfoToFlash(uint8_t ch_to_save);

// Flash--> mInfo
extern bool RestoreInfoFromFlash(void);

//extern void RequestToSave(uint32_t delay_ms);// (1) 매 1초마다// (2) user의 request를 살펴서 // (3) request가 있으면, flash memory에 정보를 저장한다.
extern void fds_RequestToSaveAfterThisTime(uint8_t ch_to_save, uint32_t delay_ms);
extern void CallMeRepeatedlyToSaveInfoToFlash(uint32_t every_call_ms);

extern void fds_RequestToCallDongle_AfterThisTime(char *msg, uint32_t delay_ms);
extern void fds_RequestToCallBT_AfterThisTime(char *msg, uint32_t delay_ms);

//extern void CallMeRepeatedlyToCallDongleFunc(uint32_t every_call_ms);
//extern void CallMeRepeatedlyToCallBTFunc(uint32_t every_call_ms);

#if 0 // BLOCK_MJ_FDS by Stanley
	extern ret_code_t drv_fds_read(drv_fds_page_t page, uint8_t *pBuf, uint8_t size);
	extern ret_code_t drv_fds_write(drv_fds_page_t page, const uint8_t *pBuf, uint8_t size);
	extern ret_code_t drv_fds_clear_all(void);
	extern ret_code_t drv_fds_save_immediately(void);
#endif


#if 0
typedef struct {
	bool                          m_module_initialized;              /**< Whether or not @ref pm_init has been called successfully. */
	bool                          m_peer_rank_initialized;           /**< Whether or not @ref rank_init has been called successfully. */
	bool                          m_deleting_all;                    /**< True from when @ref pm_peers_delete is called until all peers have been deleted. */
	pm_store_token_t              m_peer_rank_token;                 /**< The store token of an ongoing peer rank update via a call to @ref pm_peer_rank_highest. If @ref PM_STORE_TOKEN_INVALID, there is no ongoing update. */
	uint32_t                      m_current_highest_peer_rank;       /**< The current highest peer rank. Used by @ref pm_peer_rank_highest. */
	pm_peer_id_t                  m_highest_ranked_peer;             /**< The peer with the highest peer rank. Used by @ref pm_peer_rank_highest. */
	pm_evt_handler_t              m_evt_handlers[PM_MAX_REGISTRANTS];/**< The subscribers to Peer Manager events, as registered through @ref pm_register. */
	uint8_t                       m_n_registrants;                   /**< The number of event handlers registered through @ref pm_register. */
	ble_conn_state_user_flag_id_t m_bonded_flag_id;                  /**< The flag ID for which connections are with a peer with which we are bonded. */
} peer_manager_storage_t;

void PMstorage_to_buff_stanley(void *pBuff);
void buff_to_PMstorage_stanley(void *pBuff);
	
typedef struct align32
{
	//--------------------------------------------------------------------------------------------
	peer_manager_storage_t			pm_storage;			// see: peer_manage.c
	//--------------------------------------------------------------------------------------------
	m_comm_config_t					comm_config;		// see: m_comm.h
	uint16_t						conn_handle;		// see: m_comm.h
	m_comm_adv_config_t				comm_adv_config;	// see: m_comm_adv.h
	m_comm_adv_status_t				comm_adv_status;	// see: m_comm_adv.h
	m_comm_adv_channel_type_t		comm_adv_channel;	// see: m_comm_adv.h
	m_comm_mode_t					comm_mode; 			// M_COMM_MODE_BLE, M_COMM_MODE_GAZELL
	//
	m_mokibo_manufacture_info_t		manufacture_info;	// see: m_mokibo.h
	m_mokibo_test_info_t			test_info;			// see: m_mokibo.h
	//
	uint16_t 						validity;			// valid iff ts value is '0xCADE'
	uint16_t						saveCount;			// save할 때마다, count가 하나씩 증가한다.
	//--------------------------------------------------------------------------------------------
	uint32_t						dummy;				// for 4byte(32bit) align - 끝에서 잘릴 수 있다.
	//--------------------------------------------------------------------------------------------

} InfoFlash_t;
#endif




/*----------------------------------------------------------------------------//
//                         GLOBAL DEBUG FUNCTION HEADS                        //
//----------------------------------------------------------------------------*/
#ifdef TRACE_FLASH_SAVED_INFO
	void ShowTotInfo(char *msg);
	void show_flashInfo(char *msg);
	void show_mInfo(char *msg);
#endif
char *GetStr_fds_evt_id(fds_evt_id_t id);
char *GetStr_fds_record_key(uint32_t key);

uint8_t hid_to_ascii(uint8_t hid);




/*\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ 
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\_*/
#endif // __M_FDS_H 




