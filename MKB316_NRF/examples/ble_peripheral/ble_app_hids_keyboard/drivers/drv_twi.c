/// \file drv_twi.c
/// This file contains all required configurations for mokibo
/*==============================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
==============================================================================*/



/*\      $$\  $$$$$$\  $$$$$$$\  $$\   $$\ $$\       $$$$$$$$\  $$$$$$\  
$$$\    $$$ |$$  __$$\ $$  __$$\ $$ |  $$ |$$ |      $$  _____|$$  __$$\ 
$$$$\  $$$$ |$$ /  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$ /  \__|
$$\$$\$$ $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$$$$\    \$$$$$$\  
$$ \$$$  $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$  __|    \____$$\ 
$$ |\$  /$$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$\   $$ |
$$ | \_/ $$ | $$$$$$  |$$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$$\ \$$$$$$  |
\__|     \__| \______/ \_______/  \______/ \________|\________| \______/ 
//============================================================================//
//                              LOCAL MODULEL USED                            //
//============================================================================*/


#include "sdk_config.h" // mokibo_config.h && sdk_config.h
#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
	// should be placed after "sdk_config.h"
	#include "SEGGER_RTT.h"
#endif
//
#include "nrf_delay.h"
#include "nrf_log.h"
#include "nrf_drv_twi.h"
#include "app_util_platform.h"
#include "drv_twi.h"
#include "drv_cypress.h"

#include "m_timer.h"


 /*$$$$\   $$$$$$\  $$\   $$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$\   $$\ $$$$$$$$\ 
$$  __$$\ $$  __$$\ $$$\  $$ |$$  __$$\\__$$  __|$$  __$$\ $$$\  $$ |\__$$  __|
$$ /  \__|$$ /  $$ |$$$$\ $$ |$$ /  \__|  $$ |   $$ /  $$ |$$$$\ $$ |   $$ |   
$$ |      $$ |  $$ |$$ $$\$$ |\$$$$$$\    $$ |   $$$$$$$$ |$$ $$\$$ |   $$ |   
$$ |      $$ |  $$ |$$ \$$$$ | \____$$\   $$ |   $$  __$$ |$$ \$$$$ |   $$ |   
$$ |  $$\ $$ |  $$ |$$ |\$$$ |$$\   $$ |  $$ |   $$ |  $$ |$$ |\$$$ |   $$ |   
\$$$$$$  | $$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$ |  $$ |$$ | \$$ |   $$ |   
 \______/  \______/ \__|  \__| \______/   \__|   \__|  \__|\__|  \__|   \__|   
//============================================================================//
//                                LOCAL CONSTANTS                             //
//============================================================================*/
#define TWI_TX_WAIT_TIMEOUT		M_TIMER_TIME_MS(5)
#define TWI_RX_WAIT_TIMEOUT		M_TIMER_TIME_MS(5)
#define TWI_BURST_WAIT_TIMEOUT	M_TIMER_TIME_MS(5)
#define TWI_TX_WAIT_DELAY		TWI_DELAY * 256		// TWI_DELAY = 1US
#define TWI_RX_WAIT_DELAY		TWI_DELAY * 5000
#define TWI_BURST_WAIT_DELAY	TWI_DELAY * 5000


/*$$$$$$\ $$\     $$\ $$$$$$$\  $$$$$$$$\ $$$$$$$\  $$$$$$$$\ $$$$$$$$\  $$$$$$\  
\__$$  __|\$$\   $$  |$$  __$$\ $$  _____|$$  __$$\ $$  _____|$$  _____|$$  __$$\ 
   $$ |    \$$\ $$  / $$ |  $$ |$$ |      $$ |  $$ |$$ |      $$ |      $$ /  \__|
   $$ |     \$$$$  /  $$$$$$$  |$$$$$\    $$ |  $$ |$$$$$\    $$$$$\    \$$$$$$\  
   $$ |      \$$  /   $$  ____/ $$  __|   $$ |  $$ |$$  __|   $$  __|    \____$$\ 
   $$ |       $$ |    $$ |      $$ |      $$ |  $$ |$$ |      $$ |      $$\   $$ |
   $$ |       $$ |    $$ |      $$$$$$$$\ $$$$$$$  |$$$$$$$$\ $$ |      \$$$$$$  |
   \__|       \__|    \__|      \________|\_______/ \________|\__|       \______/ 
//============================================================================//
//                                LOCAL TYPEDEFS                              //
//============================================================================*/
 

/*\    $$\  $$$$$$\  $$$$$$$\  $$$$$$\  $$$$$$\  $$$$$$$\  $$\       $$$$$$$$\ 
$$ |   $$ |$$  __$$\ $$  __$$\ \_$$  _|$$  __$$\ $$  __$$\ $$ |      $$  _____|
$$ |   $$ |$$ /  $$ |$$ |  $$ |  $$ |  $$ /  $$ |$$ |  $$ |$$ |      $$ |      
\$$\  $$  |$$$$$$$$ |$$$$$$$  |  $$ |  $$$$$$$$ |$$$$$$$\ |$$ |      $$$$$\    
 \$$\$$  / $$  __$$ |$$  __$$<   $$ |  $$  __$$ |$$  __$$\ $$ |      $$  __|   
  \$$$  /  $$ |  $$ |$$ |  $$ |  $$ |  $$ |  $$ |$$ |  $$ |$$ |      $$ |      
   \$  /   $$ |  $$ |$$ |  $$ |$$$$$$\ $$ |  $$ |$$$$$$$  |$$$$$$$$\ $$$$$$$$\ 
    \_/    \__|  \__|\__|  \__|\______|\__|  \__|\_______/ \________|\_______/
//============================================================================//
//                                   VARIABLES                                //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                                GLOBAL VARIABLES                            //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                                 LOCAL VARIABLES                            //
//----------------------------------------------------------------------------*/
static nrf_drv_twi_t g_twi_instance = NRF_DRV_TWI_INSTANCE(0);
static bool g_device_used 			= false;
static bool g_device_probe 			= false;
/*----------------------------------------------------------------------------//
//                                 DEBUG VARIABLES                            //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                                UNUSED VARIABLES                            //
//----------------------------------------------------------------------------*/







/*$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\  $$\   $$\ $$$$$$$$\  $$$$$$\  $$$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\ $$ |  $$ |$$  _____|$$  __$$\ $$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|$$ |  $$ |$$ |      $$ /  $$ |$$ |  $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |      $$$$$$$$ |$$$$$\    $$$$$$$$ |$$ |  $$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |      $$  __$$ |$$  __|   $$  __$$ |$$ |  $$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\ $$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |$$ |  $$ |$$$$$$$$\ $$ |  $$ |$$$$$$$  |
\__|       \______/ \__|  \__| \______/ \__|  \__|\________|\__|  \__|\_______/ 
//============================================================================//
//                                   FUNCHEAD                                 //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                            GLOBAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
ret_code_t drv_twi_write_burst (uint8_t addr, uint8_t reg, uint8_t *data, uint32_t length);
ret_code_t drv_twi_write (uint8_t addr, uint8_t reg, uint8_t data);
ret_code_t drv_twi_read (uint8_t addr, uint8_t reg, uint8_t *data, uint32_t length);
void drv_twi_init (bool probe);


/*----------------------------------------------------------------------------//
//                             LOCAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
static void buffer_merger(uint8_t * new_buffer, uint8_t reg, uint8_t *data, uint32_t length);
static void twi_handler(nrf_drv_twi_evt_t const * p_event, void * p_context);
static int twi_tx(uint8_t addr, uint8_t *data, uint32_t length);
static int twi_rx(uint8_t addr, uint8_t *data, uint32_t length);


/*----------------------------------------------------------------------------//
//                            POINTER FUNCTION HEADS                          //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                             DEBUG FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                             UNUSED FUNCTION HEADS                          //
//----------------------------------------------------------------------------*/














/*\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ 
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\_*/












 /*$$$$\  $$\       $$$$$$\  $$$$$$$\   $$$$$$\  $$\                         
$$  __$$\ $$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                        
$$ /  \__|$$ |     $$ /  $$ |$$ |  $$ |$$ /  $$ |$$ |                        
$$ |$$$$\ $$ |     $$ |  $$ |$$$$$$$\ |$$$$$$$$ |$$ |                        
$$ |\_$$ |$$ |     $$ |  $$ |$$  __$$\ $$  __$$ |$$ |                        
$$ |  $$ |$$ |     $$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |                        
\$$$$$$  |$$$$$$$$\ $$$$$$  |$$$$$$$  |$$ |  $$ |$$$$$$$$\                   
 \______/ \________|\______/ \_______/ \__|  \__|\________|                  
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\ 
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__|
//============================================================================//
//                                 GLOBAL FUNCTIONS                           //
//============================================================================*/

/*
================================================================================
   Function name : drv_twi_init()
================================================================================
*/
void drv_twi_init(bool probe)
{
	________________________________LOG_drvTwi_FuncHeads(FUNCHEAD_FLAG,"");
	
	ret_code_t ret;

	nrf_drv_twi_config_t g_twi_config = {
		.scl                = CFG_MOKIBO_TOUCH_SCL_PIN,
		.sda                = CFG_MOKIBO_TOUCH_SDA_PIN,
		.frequency          = CFG_MOKIBO_TOUCH_FREQ,
		.interrupt_priority = CFG_MOKIBO_TOUCH_PRIORITY
	};

	g_device_probe = probe;

	ret = nrf_drv_twi_init(&g_twi_instance, &g_twi_config, twi_handler, NULL);
	
	if (NRF_SUCCESS != ret) {		
		________________________________DBG_oldDEBUG (0, "Error! nrf_drv_twi_init()");
		return;
	}
		
	nrf_drv_twi_enable(&g_twi_instance);
	
	if (true == g_device_probe) 
	{
		int i;
		uint8_t dummy_data = 0x55;
		for (i=0; i<=0x7F; i++) 
		{			
			ret = twi_tx(i, &dummy_data, 1);
		}
	
		g_device_probe = false;
	}	
}

/*
================================================================================
   Function name : drv_twi_uninit()
================================================================================
*/
void drv_twi_uninit(void)
{
	________________________________LOG_drvTwi_FuncHeads(FUNCHEAD_FLAG,"");
	nrf_drv_twi_disable (&g_twi_instance);
	nrf_drv_twi_uninit (&g_twi_instance);
}



/*
================================================================================
   Function name : drv_twi_write_burst()
================================================================================
*/
ret_code_t drv_twi_write_burst(uint8_t addr, uint8_t reg, uint8_t *data, uint32_t length)
{    
	________________________________LOG_drvTwi_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t err_code;
	uint8_t buffer[256];
	nrf_drv_twi_xfer_desc_t xfer_desc;

	//
	buffer_merger(buffer, reg, data, length);
	//

	xfer_desc.address = addr;
	xfer_desc.type = NRF_DRV_TWI_XFER_TX;
	xfer_desc.primary_length = length + 1;
	xfer_desc.p_primary_buf = buffer;

	err_code = nrf_drv_twi_xfer (&g_twi_instance, &xfer_desc, 0);
	if (NRF_SUCCESS != err_code) 
	{
		#if	defined(CONFIG_MOKIBO_TWI_TRACE)
		DEBUGS ("TWI write burst xfer error!");
		#endif
		return err_code;
	}

	int i = 0;
	m_timer_set(M_TIMER_ID_TWI_TIMEOUT, TWI_BURST_WAIT_TIMEOUT);
	TIME_TRACE_START();
	g_device_used = true;

	while (true == g_device_used ) 
	{ 
		i++;
		nrf_delay_us (TWI_DELAY); 
	
		if(m_timer_is_expired(M_TIMER_ID_TWI_TIMEOUT)) 
		{
			DEBUG("OOPS!!");
			g_device_used = false;
		}

		if(i>TWI_BURST_WAIT_DELAY)
		{
			________________________________DBG_flimbs_20190307_drvTwi_InfiniteLoop_ExitOver128_trace(3,"g_device_used = false; i>256");
			#if(________________________________DBG_flimbs_20190307_drvTwi_InfiniteLoop_ExitOver128 == 1)
				g_device_used = false;			
			#endif			
		}
	}
	TIME_TRACE_END(TWI_BURST_WAIT_TIMEOUT);

	return err_code;
}

/*
================================================================================
   Function name : drv_twi_write()
================================================================================
*/
ret_code_t drv_twi_write(uint8_t addr, uint8_t reg, uint8_t data)
{
	________________________________LOG_drvTwi_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t err_code;

	uint8_t packet[2] = {reg, data };

	err_code = twi_tx (addr, packet, 2);
	if (NRF_SUCCESS != err_code) 
	{
		#if	defined(CONFIG_MOKIBO_TWI_TRACE)
		DEBUGS ("TWI write tx error!");
		#endif
		return err_code;
	}

	return err_code;
}

/*================================================================================
   Function name : drv_twi_read()
================================================================================*/
ret_code_t drv_twi_read(uint8_t addr, uint8_t reg, uint8_t *data, uint32_t length)
{
	________________________________LOG_drvTwi_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t err_code;

	err_code = twi_tx (addr, &reg, 1);
	if (NRF_SUCCESS != err_code) 
	{
		#if	defined(CONFIG_MOKIBO_TWI_TRACE)
		DEBUGS ("TWI read tx error!");
		#endif
		return err_code;
	}

	err_code = twi_rx (addr, data, length);
	if (NRF_SUCCESS != err_code) 
	{
		#if	defined(CONFIG_MOKIBO_TWI_TRACE)
		DEBUGS ("TWI read rx error!");
		#endif
		return err_code;
	}	return err_code;


}












/*\       $$$$$$\   $$$$$$\   $$$$$$\  $$\                                   
$$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                                  
$$ |     $$ /  $$ |$$ /  \__|$$ /  $$ |$$ |                                  
$$ |     $$ |  $$ |$$ |      $$$$$$$$ |$$ |                                  
$$ |     $$ |  $$ |$$ |      $$  __$$ |$$ |                                  
$$ |     $$ |  $$ |$$ |  $$\ $$ |  $$ |$$ |                                  
$$$$$$$$\ $$$$$$  |\$$$$$$  |$$ |  $$ |$$$$$$$$\                             
\________|\______/  \______/ \__|  \__|\________|                            
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\ 
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__|
//============================================================================//
//                                 LOCAL FUNCTIONS                            //
//============================================================================*/
/*
================================================================================
   Function name : buffer_merger()
================================================================================
*/
static void buffer_merger(uint8_t * new_buffer, uint8_t reg, uint8_t *data, uint32_t length)
{
	________________________________LOG_drvTwi_FuncHeads(FUNCHEAD_FLAG,"");
	int i;
	uint8_t *ptr_new_buffer;
	uint8_t *ptr_data_place_holder;
	//
	ptr_data_place_holder = data;
	//
	ptr_new_buffer = new_buffer;
	*ptr_new_buffer = reg;
	ptr_new_buffer++;

	for(i=0; i<length; i++) 
	{
		*ptr_new_buffer = *ptr_data_place_holder;
		ptr_new_buffer++;
		ptr_data_place_holder++;
	}
}

/*
================================================================================
   Function name : twi_handler()
================================================================================
*/
static void twi_handler(nrf_drv_twi_evt_t const * p_event, void * p_context)
{
	________________________________LOG_drvTwi_FuncHeads(FUNCHEAD_FLAG,"");
	#if (TWI_WAIT_TYPE == TWI_WAIT_TYPE_TIMER)
	g_device_used = false;
	#endif

	if ((true == g_device_probe) && (NRF_DRV_TWI_EVT_DONE == p_event->type)) 
	{
		#if	defined(CONFIG_MOKIBO_TWI_PROBE_TRACE)
		________________________________DBG_oldDEBUG (0, "TWI device found at 7-bit address : 0x%02x", p_event->xfer_desc.address);
		//DEBUG("TWI device found at 7-bit address : 0x%02x", p_event->xfer_desc.address);
		#endif
	}
}

/*
================================================================================
   Function name : twi_tx()
================================================================================
*/
static int twi_tx(uint8_t addr, uint8_t *data, uint32_t length)
{
	________________________________LOG_drvTwi_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t result;

	result = nrf_drv_twi_tx (&g_twi_instance, addr, data, length, false);
	
	if (NRF_SUCCESS != result) 
	{				
		return -1;
	}
	
	int i = 0;
	m_timer_set(M_TIMER_ID_TWI_TIMEOUT, TWI_TX_WAIT_TIMEOUT);
	TIME_TRACE_START();
	g_device_used = true;
	
	while (true == g_device_used) 
	{ 
		i++;
		nrf_delay_us (TWI_DELAY); 
				
		if(m_timer_is_expired(M_TIMER_ID_TWI_TIMEOUT)) 
		{			
			g_device_used = false;
			//________________________________DBG_flimbs_20190307_drvTwi_InfiniteLoop_ExitOver128_trace(3,"m_timer_is_expired(M_TIMER_ID_TWI_TIMEOUT)");
		}

		if(i>TWI_TX_WAIT_DELAY)
		{
			________________________________DBG_flimbs_20190307_drvTwi_InfiniteLoop_ExitOver128_trace(3,"g_device_used = false; i>256");
			#if(________________________________DBG_flimbs_20190307_drvTwi_InfiniteLoop_ExitOver128 == 1)
				g_device_used = false;			
			#endif
			
		}
	}
	TIME_TRACE_END(TWI_TX_WAIT_TIMEOUT);

	return 0;
}

/*
================================================================================
   Function name : twi_rx()
================================================================================
*/
static int twi_rx(uint8_t addr, uint8_t *data, uint32_t length)
{
	________________________________LOG_drvTwi_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t result;

	result = nrf_drv_twi_rx (&g_twi_instance, addr, data, length);

	if (NRF_SUCCESS != result) 
	{
		#if	defined(CONFIG_MOKIBO_TWI_TRACE)
		DEBUGS ("Error! TWI RX RET=0x%x", (int)result);
		#endif
		return -1;
	}
					
	int i = 0;
	m_timer_set(M_TIMER_ID_TWI_TIMEOUT, TWI_RX_WAIT_TIMEOUT);
	TIME_TRACE_START();
	g_device_used = true;

	while (true == g_device_used)
	{ 
		i++;
		nrf_delay_us (TWI_DELAY); 
		
		if(m_timer_is_expired(M_TIMER_ID_TWI_TIMEOUT)) 
		{
			DEBUG("OOPS!!");
			g_device_used = false;
		}

		if(i>TWI_RX_WAIT_DELAY)
		{
			________________________________DBG_flimbs_20190307_drvTwi_InfiniteLoop_ExitOver128_trace(3,"g_device_used = false; i>256");
			#if(________________________________DBG_flimbs_20190307_drvTwi_InfiniteLoop_ExitOver128 == 1)
				g_device_used = false;			
			#endif	
		}	
	}
	TIME_TRACE_END(TWI_RX_WAIT_TIMEOUT);  
	
	return 0;
}










/*$$$$$\  $$$$$$$$\ $$$$$$$\  $$\   $$\  $$$$$$\                             
$$  __$$\ $$  _____|$$  __$$\ $$ |  $$ |$$  __$$\                            
$$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |$$ /  \__|                           
$$ |  $$ |$$$$$\    $$$$$$$\ |$$ |  $$ |$$ |$$$$\                            
$$ |  $$ |$$  __|   $$  __$$\ $$ |  $$ |$$ |\_$$ |                           
$$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |$$ |  $$ |                           
$$$$$$$  |$$$$$$$$\ $$$$$$$  |\$$$$$$  |\$$$$$$  |                           
\_______/ \________|\_______/  \______/  \______/                            
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\ 
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__|
//============================================================================//
//                                 DEBUG FUNCTIONS                            //
//============================================================================*/




/*\   $$\ $$\   $$\ $$\   $$\  $$$$$$\  $$$$$$$$\ $$$$$$$\                   
$$ |  $$ |$$$\  $$ |$$ |  $$ |$$  __$$\ $$  _____|$$  __$$\                  
$$ |  $$ |$$$$\ $$ |$$ |  $$ |$$ /  \__|$$ |      $$ |  $$ |                 
$$ |  $$ |$$ $$\$$ |$$ |  $$ |\$$$$$$\  $$$$$\    $$ |  $$ |                 
$$ |  $$ |$$ \$$$$ |$$ |  $$ | \____$$\ $$  __|   $$ |  $$ |                 
$$ |  $$ |$$ |\$$$ |$$ |  $$ |$$\   $$ |$$ |      $$ |  $$ |                 
\$$$$$$  |$$ | \$$ |\$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$  |                 
 \______/ \__|  \__| \______/  \______/ \________|\_______/                  
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\ 
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__|
//============================================================================//
//                                 UNUSED FUNCTIONS                           //
//============================================================================*/




