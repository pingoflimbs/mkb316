/// \file drv_cypress.h
/// This file contains all required configurations for mokibo
/*==============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
==============================================================================*/







/*\      $$\  $$$$$$\  $$$$$$$\  $$\   $$\ $$\       $$$$$$$$\  $$$$$$\  
$$$\    $$$ |$$  __$$\ $$  __$$\ $$ |  $$ |$$ |      $$  _____|$$  __$$\ 
$$$$\  $$$$ |$$ /  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$ /  \__|
$$\$$\$$ $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$$$$\    \$$$$$$\  
$$ \$$$  $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$  __|    \____$$\ 
$$ |\$  /$$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$\   $$ |
$$ | \_/ $$ | $$$$$$  |$$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$$\ \$$$$$$  |
\__|     \__| \______/ \_______/  \______/ \________|\________| \______/ 
//============================================================================//
//                                 MODULES USED                               //
//============================================================================*/
#ifndef CYPRESS_TT21XXX_H
#define CYPRESS_TT21XXX_H    
#include <stdio.h>
#include <stdint.h>




 /*$$$$\   $$$$$$\  $$\   $$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$\   $$\ $$$$$$$$\ 
$$  __$$\ $$  __$$\ $$$\  $$ |$$  __$$\\__$$  __|$$  __$$\ $$$\  $$ |\__$$  __|
$$ /  \__|$$ /  $$ |$$$$\ $$ |$$ /  \__|  $$ |   $$ /  $$ |$$$$\ $$ |   $$ |   
$$ |      $$ |  $$ |$$ $$\$$ |\$$$$$$\    $$ |   $$$$$$$$ |$$ $$\$$ |   $$ |   
$$ |      $$ |  $$ |$$ \$$$$ | \____$$\   $$ |   $$  __$$ |$$ \$$$$ |   $$ |   
$$ |  $$\ $$ |  $$ |$$ |\$$$ |$$\   $$ |  $$ |   $$ |  $$ |$$ |\$$$ |   $$ |   
\$$$$$$  | $$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$ |  $$ |$$ | \$$ |   $$ |   
 \______/  \______/ \__|  \__| \______/   \__|   \__|  \__|\__|  \__|   \__|   
 //============================================================================//
//                                 GLOBAL CONSTANTS                            //
//============================================================================*/

//#define I2C_SDA						27	//[i2c bus]	// p.27 SDA
//#define I2C_SCL						28	//[i2c bus]	// p.28 SCL
#define CFG_MOKIBO_TOUCH_RESET_PIN		29	//[output]	// p.29 TT_RST
#define CFG_MOKIBO_TOUCH_INT_PIN		30	//[input]	// p.30 TT_INT
//
#define CFG_MOKIBO_TOUCH_PWR_PIN		25	//[output]	// p.25 U6_EN (cypress touch power)
#define CFG_MOKIBO_TOUCH_DEBUG_PIN		31	//[output]	// p.31 n.c.

#define DEVICE_MODE_OPERATING			0
#define DEVICE_MODE_SYSTEMINFOR			1
#define DEVICE_MODE_CONFIGURATION		2

#define DRV_CYPRESS_MAX_COUNT			10
#define DRV_CYPRESS_RECORD_HEADER 		7
#define DRV_CYPRESS_RECORD_LENGTH 		10

#define DRV_CYPRESS_DATA_REQ_LEN		(DRV_CYPRESS_RECORD_HEADER + (DRV_CYPRESS_RECORD_LENGTH*DRV_CYPRESS_MAX_COUNT) )
#define DRV_CYPRESS_1_DATA_LEN			(DRV_CYPRESS_RECORD_HEADER + DRV_CYPRESS_RECORD_LENGTH)

#define DRV_CYPRESS_BUFFER_SIZE			256

#define DRV_CYPRESS_HEADER_SIZE			sizeof(drv_cypress_header_t)

#define CY_PIN_LOW						0
#define CY_PIN_HIGH						1
#define CY_DATA_MAX_ROW_SIZE    		(256)
#define CY_FLASH_BLOCK_SIZE    			(0x80)
#define DRV_CYPRESS_MODE_BOOTLOADER		(0x80)
#define DRV_CYPRESS_MODE_OPERATION		(0x81)



/*$$$$$$\ $$\     $$\ $$$$$$$\  $$$$$$$$\ $$$$$$$\  $$$$$$$$\ $$$$$$$$\  $$$$$$\  
\__$$  __|\$$\   $$  |$$  __$$\ $$  _____|$$  __$$\ $$  _____|$$  _____|$$  __$$\ 
   $$ |    \$$\ $$  / $$ |  $$ |$$ |      $$ |  $$ |$$ |      $$ |      $$ /  \__|
   $$ |     \$$$$  /  $$$$$$$  |$$$$$\    $$ |  $$ |$$$$$\    $$$$$\    \$$$$$$\  
   $$ |      \$$  /   $$  ____/ $$  __|   $$ |  $$ |$$  __|   $$  __|    \____$$\ 
   $$ |       $$ |    $$ |      $$ |      $$ |  $$ |$$ |      $$ |      $$\   $$ |
   $$ |       $$ |    $$ |      $$$$$$$$\ $$$$$$$  |$$$$$$$$\ $$ |      \$$$$$$  |
   \__|       \__|    \__|      \________|\_______/ \________|\__|       \______/ 
//============================================================================//
//                                 GLOBAL TYPEDEFS                            //
//============================================================================*/

//----------------------------------------------------//drv_cypress_packet_t
typedef struct drv_cypress_len 
{
	uint16_t	len;
} __attribute__((__packed__)) drv_cypress_len_t;

typedef struct 
{//	Refer to File: "001-88195_0e_TRM.pdf"//	"TRM(Technical Reference Manual) Document No. 001-88195 Rev.E"//	"Table 7-1. Touch Report Structure"
	drv_cypress_len_t	len;			/* 2bytes */
	uint8_t				report_id;		/* 1byte */
	uint16_t			timestamp;		/* 2bytes */
	uint8_t				touch_count;	/* 1byte */
	uint8_t				dummy;			/* 1byte */
} __attribute__((__packed__)) drv_cypress_header_t;					/* 7Bytes */

typedef struct 
{// Refer to File: "001-88195_0e_TRM.pdf"// "TRM(Technical Reference Manual) Document No. 001-88195 Rev.E"// "Table 7-2. Touch Record Structure"
	uint8_t			touchType:3	; // finger(0), proximity(1), reserved(4~15)
	uint8_t			res1:5		; // *
	uint8_t			touchID:5	; // arbitrary touch id
	uint8_t			eventID:2	; // no_event(0), touch_down(1), significant_displacement(2), lift_off(3)
	uint8_t			tip:1		; // touch is currently on the panel
	uint16_t		x;			  // 16bit x-coordinates in pixels
	uint16_t		y;			  // 16bit y-coordinates in pixels
	uint8_t			pressure;	  // touch intensity in counts
	//uint16_t		axis;
	uint8_t			major_axis;	  // the length of the major axis (in mm) of the ellipse of contact between the finger and the panel
	uint8_t			minor_axis;	  // the length of the minor axis (in mm) of the ellipse of contact between the finger and the panel	
	uint8_t			orientation;  //
}  __attribute__((__packed__)) drv_cypress_touch_info_t;

typedef struct {
	drv_cypress_header_t		header;
	drv_cypress_touch_info_t	info[DRV_CYPRESS_MAX_COUNT];
}  __attribute__((__packed__)) drv_cypress_packet_touch_t;

typedef union {
	uint8_t	buf[DRV_CYPRESS_BUFFER_SIZE];	
	drv_cypress_len_t			len;
	drv_cypress_header_t		header;
	drv_cypress_packet_touch_t	body;
} __attribute__((__packed__)) drv_cypress_packet_t;



//----------------------------------------------------//drv_cypress_info_t
typedef struct {
		uint8_t		touchType;	// finger(0), proximity(1), reserved(4~15)
		uint8_t 	touchID; 	// adjusted touch id
		uint8_t 	eventID;	// no_event(0), touch_down(1), significant_displacement(2), lift_off(3)
		uint8_t 	tip;		// touch is currently on the panel
		uint16_t 	x;			// 16bit x-coordinates in pixels
		uint16_t 	y;			// 16bit y-coordinates in pixels
		uint8_t 	presure;	// touch intensity in counts
		//uint16_t 	axis;
		uint8_t 	major_axis;	// the length of the major axis (in mm) of the ellipse of contact between the finger and the panel
		uint8_t 	minor_axis;	// the length of the minor axis (in mm) of the ellipse of contact between the finger and the panel
		uint8_t 	orient;		// orientation
} drv_touch_finger_info_t;

typedef struct {
	uint8_t		touchCnt;		// total number of current touches 
	uint16_t	timestamp;		// unit: 0.1ms  
	drv_touch_finger_info_t info[DRV_CYPRESS_MAX_COUNT]; // [10]
} drv_cypress_info_t;



//----------------------------------------------------//cyttsp5_hex_image
struct cyttsp5_hex_image{
	uint8_t array_id;
	uint16_t row_num;
	uint16_t row_size;
	uint8_t row_data[CY_FLASH_BLOCK_SIZE];
} __attribute__((__packed__));



//----------------------------------------------------//MULTIINFO
typedef enum _GROUP_OP
{
	TAPPING=0,	// tapping-mode(single-, double-, triple-tapping, etc)
	MOVE		// moving-mode (pointer move, scroll, drag, zoom-in/out, etc)
} GROUP_OP;

typedef struct _MULTIINFO
{
	uint16_t	numPoints;		// single-, double-, triple-, ...
	GROUP_OP	group_op;		// TAPPING, MOVE

	// MOVE: pointing, scroll, drag, zoom
	int16_t	dx;			// x-방향 변위(또는 x방향 zoom), signed(+/-)
	int16_t	dy;			// y-방향 변위(또는 y방향 zoom), signed(+/-)
} MULTIINFO;



//----------------------------------------------------//DRV_CYPRESS_ACTIVE_AREA_T
typedef enum
{
    DRV_CYPRESS_ACTIVE_AREA_WHOLE,       	// All area is active
    DRV_CYPRESS_ACTIVE_AREA_LEFT,       	// Left area is active
    DRV_CYPRESS_ACTIVE_AREA_RIGHT,       	// Right area is active
} DRV_CYPRESS_ACTIVE_AREA_T;





/*\    $$\  $$$$$$\  $$$$$$$\  $$$$$$\  $$$$$$\  $$$$$$$\  $$\       $$$$$$$$\ 
$$ |   $$ |$$  __$$\ $$  __$$\ \_$$  _|$$  __$$\ $$  __$$\ $$ |      $$  _____|
$$ |   $$ |$$ /  $$ |$$ |  $$ |  $$ |  $$ /  $$ |$$ |  $$ |$$ |      $$ |      
\$$\  $$  |$$$$$$$$ |$$$$$$$  |  $$ |  $$$$$$$$ |$$$$$$$\ |$$ |      $$$$$\    
 \$$\$$  / $$  __$$ |$$  __$$<   $$ |  $$  __$$ |$$  __$$\ $$ |      $$  __|   
  \$$$  /  $$ |  $$ |$$ |  $$ |  $$ |  $$ |  $$ |$$ |  $$ |$$ |      $$ |      
   \$  /   $$ |  $$ |$$ |  $$ |$$$$$$\ $$ |  $$ |$$$$$$$  |$$$$$$$$\ $$$$$$$$\ 
    \_/    \__|  \__|\__|  \__|\______|\__|  \__|\_______/ \________|\________|
/*============================================================================//
//                            GLOBAL  VARIABLES                               //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                                GLOBAL VARIABLES                            //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                             GLOBAL DEBUG VARIABLES                         //
//----------------------------------------------------------------------------*/



 /*$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\  $$\   $$\ $$$$$$$$\  $$$$$$\  $$$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\ $$ |  $$ |$$  _____|$$  __$$\ $$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|$$ |  $$ |$$ |      $$ /  $$ |$$ |  $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |      $$$$$$$$ |$$$$$\    $$$$$$$$ |$$ |  $$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |      $$  __$$ |$$  __|   $$  __$$ |$$ |  $$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\ $$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |$$ |  $$ |$$$$$$$$\ $$ |  $$ |$$$$$$$  |
\__|       \______/ \__|  \__| \______/ \__|  \__|\________|\__|  \__|\_______/ 
/*============================================================================//
//                                 GLOBAL FUNCTIONS                           //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                            GLOBAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
extern void drv_cypress_init(void);
extern void drv_cypress_re_init(void);
extern ret_code_t drv_cypress_read(drv_cypress_info_t *pInfo);
extern void drv_cypress_touch_reset(const char *caller, int caller_num);
extern void drv_cypress_touch_reset_fast(const char *caller, int caller_num);
extern void drv_cypress_mode_set(DRV_CYPRESS_ACTIVE_AREA_T new_area);
extern uint32_t drv_cypress_get_fw_version(void);
extern void drv_cypress_init_Baseline(void);
extern void drv_cypress_req_deep_sleep(uint8_t wake_of_sleep);
extern void drv_cypress_req_wake_up(void);
extern void drv_cypress_req_device_reset(void);
extern void drv_cypress_req_calibration(uint8_t senseMode);
extern void drv_cypress_req_suspend_scanning(void);
extern void drv_cypress_req_resume_scanning(void);
extern void drv_cypress_touch_power_down(void);
extern ret_code_t cypress_read_TT_INT(void);
extern void drv_cypress_power_on(void);
extern void drv_cypress_power_off(void);
extern void drv_cypress_ResetPin_Low(void);
extern void drv_cypress_ResetPin_High(void);
extern void drv_cypress_ResetPin_Low_Wait_High(void);
//extern void cypress_pwr(uint8_t OnOff);
/*----------------------------------------------------------------------------//
//                         GLOBAL DEBUG FUNCTION HEADS                        //
//----------------------------------------------------------------------------*/











/*\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ 
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\_*/


#endif

