/// \file drv_fds.c
/// This file contains all required configurations for mokibo
/*==============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
==============================================================================*/





/*\      $$\  $$$$$$\  $$$$$$$\  $$\   $$\ $$\       $$$$$$$$\  $$$$$$\  
$$$\    $$$ |$$  __$$\ $$  __$$\ $$ |  $$ |$$ |      $$  _____|$$  __$$\ 
$$$$\  $$$$ |$$ /  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$ /  \__|
$$\$$\$$ $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$$$$\    \$$$$$$\  
$$ \$$$  $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$  __|    \____$$\ 
$$ |\$  /$$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$\   $$ |
$$ | \_/ $$ | $$$$$$  |$$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$$\ \$$$$$$  |
\__|     \__| \______/ \_______/  \______/ \________|\________| \______/ 
//============================================================================//
//                                 MODULES USED                               //
//============================================================================*/
#include "sdk_config.h" // mokibo_config.h && sdk_config.h
//
#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
	// should be placed after "sdk_config.h"
	#include "SEGGER_RTT.h"
	
#endif
//
#include "fds.h"
#include "nrf_log.h"
#include "drv_fds.h"
#include "nrf_delay.h"
#include "m_timer.h"
#include "m_mokibo.h"
#include "stdlib.h"	// for malloc(), free()
#include "m_event.h"
#include "m_comm.h"
#include "m_comm_adv.h"
#include "ble_conn_state.h"





 /*$$$$\   $$$$$$\  $$\   $$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$\   $$\ $$$$$$$$\ 
$$  __$$\ $$  __$$\ $$$\  $$ |$$  __$$\\__$$  __|$$  __$$\ $$$\  $$ |\__$$  __|
$$ /  \__|$$ /  $$ |$$$$\ $$ |$$ /  \__|  $$ |   $$ /  $$ |$$$$\ $$ |   $$ |   
$$ |      $$ |  $$ |$$ $$\$$ |\$$$$$$\    $$ |   $$$$$$$$ |$$ $$\$$ |   $$ |   
$$ |      $$ |  $$ |$$ \$$$$ | \____$$\   $$ |   $$  __$$ |$$ \$$$$ |   $$ |   
$$ |  $$\ $$ |  $$ |$$ |\$$$ |$$\   $$ |  $$ |   $$ |  $$ |$$ |\$$$ |   $$ |   
\$$$$$$  | $$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$ |  $$ |$$ | \$$ |   $$ |   
 \______/  \______/ \__|  \__| \______/   \__|   \__|  \__|\__|  \__|   \__|   
//============================================================================//
//                                LOCAL CONSTANTS                             //
//============================================================================*/
#define	VALID_FLASH_INFO	0xBEEF




/*$$$$$$\ $$\     $$\ $$$$$$$\  $$$$$$$$\ $$$$$$$\  $$$$$$$$\ $$$$$$$$\  $$$$$$\  
\__$$  __|\$$\   $$  |$$  __$$\ $$  _____|$$  __$$\ $$  _____|$$  _____|$$  __$$\ 
   $$ |    \$$\ $$  / $$ |  $$ |$$ |      $$ |  $$ |$$ |      $$ |      $$ /  \__|
   $$ |     \$$$$  /  $$$$$$$  |$$$$$\    $$ |  $$ |$$$$$\    $$$$$\    \$$$$$$\  
   $$ |      \$$  /   $$  ____/ $$  __|   $$ |  $$ |$$  __|   $$  __|    \____$$\ 
   $$ |       $$ |    $$ |      $$ |      $$ |  $$ |$$ |      $$ |      $$\   $$ |
   $$ |       $$ |    $$ |      $$$$$$$$\ $$$$$$$  |$$$$$$$$\ $$ |      \$$$$$$  |
   \__|       \__|    \__|      \________|\_______/ \________|\__|       \______/ 
//============================================================================//
//                                LOCAL TYPEDEFS                              //
//============================================================================*/
//----------------------------------------------------//name
typedef struct {
	bool                          m_module_initialized;              /**< Whether or not @ref pm_init has been called successfully. */
	bool                          m_peer_rank_initialized;           /**< Whether or not @ref rank_init has been called successfully. */
	bool                          m_deleting_all;                    /**< True from when @ref pm_peers_delete is called until all peers have been deleted. */
	pm_store_token_t              m_peer_rank_token;                 /**< The store token of an ongoing peer rank update via a call to @ref pm_peer_rank_highest. If @ref PM_STORE_TOKEN_INVALID, there is no ongoing update. */
	uint32_t                      m_current_highest_peer_rank;       /**< The current highest peer rank. Used by @ref pm_peer_rank_highest. */
	pm_peer_id_t                  m_highest_ranked_peer;             /**< The peer with the highest peer rank. Used by @ref pm_peer_rank_highest. */
	pm_evt_handler_t              m_evt_handlers[PM_MAX_REGISTRANTS];/**< The subscribers to Peer Manager events, as registered through @ref pm_register. */
	uint8_t                       m_n_registrants;                   /**< The number of event handlers registered through @ref pm_register. */
	ble_conn_state_user_flag_id_t m_bonded_flag_id;                  /**< The flag ID for which connections are with a peer with which we are bonded. */
} peer_manager_storage_t;


//----------------------------------------------------//name
typedef struct align32
{
	//--------------------------------------------------------------------------------------------
	peer_manager_storage_t			pm_storage;			// see: peer_manage.c
	//--------------------------------------------------------------------------------------------
	m_comm_config_t					comm_config;		// see: m_comm.h
	uint16_t						conn_handle;		// see: m_comm.h
	m_comm_adv_config_t				comm_adv_config;	// see: m_comm_adv.h
	m_comm_adv_status_t				comm_adv_status;	// see: m_comm_adv.h
	m_comm_adv_channel_type_t		comm_adv_channel;	// see: m_comm_adv.h
	m_comm_mode_t					comm_mode; 			// M_COMM_MODE_BLE, M_COMM_MODE_GAZELL
	//
	nrf_ble_gatt_t					gatt;				// see: nrf_ble_gatt.h
	//
	m_mokibo_manufacture_info_t		manufacture_info;	// see: m_mokibo.h
	m_mokibo_test_info_t			test_info;			// see: m_mokibo.h
	//
	uint16_t 						validity;			// valid iff ts value is '0xCADE'
	uint16_t						saveCount;			// save할 때마다, count가 하나씩 증가한다.
	//--------------------------------------------------------------------------------------------
	uint32_t						dummy;				// for 4byte(32bit) align - 끝에서 잘릴 수 있다.
	//--------------------------------------------------------------------------------------------
} InfoFlash_t;






/*\    $$\  $$$$$$\  $$$$$$$\  $$$$$$\  $$$$$$\  $$$$$$$\  $$\       $$$$$$$$\ 
$$ |   $$ |$$  __$$\ $$  __$$\ \_$$  _|$$  __$$\ $$  __$$\ $$ |      $$  _____|
$$ |   $$ |$$ /  $$ |$$ |  $$ |  $$ |  $$ /  $$ |$$ |  $$ |$$ |      $$ |      
\$$\  $$  |$$$$$$$$ |$$$$$$$  |  $$ |  $$$$$$$$ |$$$$$$$\ |$$ |      $$$$$\    
 \$$\$$  / $$  __$$ |$$  __$$<   $$ |  $$  __$$ |$$  __$$\ $$ |      $$  __|   
  \$$$  /  $$ |  $$ |$$ |  $$ |  $$ |  $$ |  $$ |$$ |  $$ |$$ |      $$ |      
   \$  /   $$ |  $$ |$$ |  $$ |$$$$$$\ $$ |  $$ |$$$$$$$  |$$$$$$$$\ $$$$$$$$\ 
    \_/    \__|  \__|\__|  \__|\______|\__|  \__|\_______/ \________|\_______/
//============================================================================//
//                                   VARIABLES                                //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                                GLOBAL VARIABLES                            //
//----------------------------------------------------------------------------*/
bool m_fds_evt_refreshed = 0;
fds_evt_t m_fds_evt_last;
/*----------------------------------------------------------------------------//
//                                 LOCAL VARIABLES                            //
//----------------------------------------------------------------------------*/


 
// to save 'selected_ch'
int32_t m_save_after_this_ms = 0;
uint8_t m_ch_to_save= M_COMM_CHANNEL_MAX;
//
int32_t m_call_dongle_after_this_ms = 0; // to call Change_To_DONGLE()
int32_t m_call_bt_after_this_ms = 0; // to call Change_To_DONGLE()


/*----------------------------------------------------------------------------//
//                                 DEBUG VARIABLES                            //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                                UNUSED VARIABLES                            //
//----------------------------------------------------------------------------*/






/*$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\  $$\   $$\ $$$$$$$$\  $$$$$$\  $$$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\ $$ |  $$ |$$  _____|$$  __$$\ $$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|$$ |  $$ |$$ |      $$ /  $$ |$$ |  $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |      $$$$$$$$ |$$$$$\    $$$$$$$$ |$$ |  $$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |      $$  __$$ |$$  __|   $$  __$$ |$$ |  $$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\ $$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |$$ |  $$ |$$$$$$$$\ $$ |  $$ |$$$$$$$  |
\__|       \______/ \__|  \__| \______/ \__|  \__|\________|\__|  \__|\_______/ 
//============================================================================//
//                                   FUNCHEAD                                 //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                            GLOBAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
ret_code_t drv_fds_init (void);
void drv_fds_erase(void);

void reset_fds_op_trace(void);
bool is_fds_op_finshed(fds_evt_id_t id);

ret_code_t fds_write_stanley(uint16_t file_id, uint16_t record_key, const uint8_t *pUserData, uint16_t size_in_bytes);
ret_code_t fds_read_stanley(uint16_t file_id,uint16_t record_key,uint16_t size_in_bytes, uint8_t *pUserBuff);
ret_code_t fds_delete_stanley(uint16_t file_id,	uint16_t record_key);

bool IsValidFlash(void);

// m_Info--> Flash
void SaveInfoToFlash(uint8_t ch_to_save);

// Flash--> mInfo
bool RestoreInfoFromFlash(void);

//void RequestToSave(uint32_t delay_ms);// (1) 매 1초마다// (2) user의 request를 살펴서 // (3) request가 있으면, flash memory에 정보를 저장한다.
void fds_RequestToSaveAfterThisTime(uint8_t ch_to_save, uint32_t delay_ms);
void CallMeRepeatedlyToSaveInfoToFlash(uint32_t every_call_ms);

void fds_RequestToCallDongle_AfterThisTime(char *msg, uint32_t delay_ms);
void fds_RequestToCallBT_AfterThisTime(char *msg, uint32_t delay_ms);

//void CallMeRepeatedlyToCallDongleFunc(uint32_t every_call_ms);
//void CallMeRepeatedlyToCallBTFunc(uint32_t every_call_ms);

/*----------------------------------------------------------------------------//
//                             LOCAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
extern void fds_evt_handler(fds_evt_t const * const p_fds_evt);

static void drv_fds_evt_handler(fds_evt_t const * p_fds_evt);


static void copy_mInfo_to_buff(InfoFlash_t *pBuff);
static void copy_buff_to_mInfo(InfoFlash_t *pBuff);
static bool ReadInfoFromFlash(InfoFlash_t *p);

static void PMstorage_to_buff_stanley(void *pBuff);
static void buff_to_PMstorage_stanley(void *pBuff);
static void *FlashDataToBeWritten[FDS_RECORD_KEY_MAX - FDS_RECORD_KEY_SAMPLE1]={0, };

// see: peer_data_storage.c



#if 0 //BLOCK_MJ_FDS:  Intentianlly blocked by Stanley 2018.10.9

typedef struct
{
	uint16_t addr;
	uint16_t size;
} page_info_t;

// m_buffer should word aligned!!! to be used in fds
static uint32_t m_bufffer[DRV_FDS_TOTAL_SIZE/sizeof(uint32_t)];

// Refer to "drv_fds_map_rev1.1.xls"
static const page_info_t m_pages_info[drv_fds_max_page] = {\
	{  0,    4 }, 	// page 0
	{  4,    4 }, 	// page 1
	{  8,    4 }, 	// page 2
	{  12,   4 }, 	// page 3
	{  16,   4 }, 	// page 4
	{  20,   4 }, 	// page 5
	{  24,   8 }, 	// page 6
	{  32,  32 }, 	// page 7 ... totlal 8 pages
};

#endif

/*----------------------------------------------------------------------------//
//                            POINTER FUNCTION HEADS                          //
//----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------//
//                             DEBUG FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
#if 1
void debshow_conn_stat(const char *msg,conn_stat_t conn_stat);
void debshow_gap_addr(ble_gap_addr_t *p_g_addr);
void debshow_mokibo_manufacture_info(const char *msg,m_mokibo_manufacture_info_t factory_data);
void debshow_mokibo_test_info(const char *msg,m_mokibo_test_info_t test_data);

void debshow_comm_mode(const char *msg, m_comm_mode_t comm_mode);
void debshow_comm_config(const char *msg, m_comm_config_t comm_config);
void debshow_conn_handle(const char *msg, uint16_t conn_handle);
void debshow_comm_adv_config(const char *msg, m_comm_adv_config_t comm_adv_config);
void debshow_comm_adv_status(const char *msg, m_comm_adv_status_t comm_adv_status);
void debshow_comm_adv_channel(const char *msg, m_comm_adv_channel_type_t comm_adv_channel);
void debshow_gatt(const char *msg, nrf_ble_gatt_t gatt);
void debshow_passkey(const char *msg, passkey_t m_passkey);

#endif

#ifdef TRACE_FDS_READWRITE
char *GetStr_fds_evt_id(fds_evt_id_t id)
{
	switch(id)
	{
		case FDS_EVT_INIT: return "FDS_EVT_INIT";
		case FDS_EVT_WRITE: return "FDS_EVT_WRITE";
		case FDS_EVT_UPDATE: return "FDS_EVT_UPDATE";
		case FDS_EVT_DEL_RECORD: return "FDS_EVT_DEL_RECORD";
		case FDS_EVT_DEL_FILE: return "FDS_EVT_DEL_FILE";
		case FDS_EVT_GC: return "FDS_EVT_GC";
		default: return "UNKNOWN";
	}
}
char *GetStr_fds_record_key(uint32_t key)
{
	switch(key)
	{
		case FDS_RECORD_KEY_SAMPLE1: return "FDS_RECORD_KEY_SAMPLE1";
		case FDS_RECORD_KEY_TOT_INFO: return "FDS_RECORD_KEY_TOT_INFO";
		case FDS_RECORD_KEY_M_CONFIG_VALID: return "FDS_RECORD_KEY_M_CONFIG_VALID";
		case FDS_RECORD_KEY_M_COMM_CONFIG: return "FDS_RECORD_KEY_M_COMM_CONFIG";
		case FDS_RECORD_KEY_M_COMM_ADV_CONFIG: return "FDS_RECORD_KEY_M_COMM_ADV_CONFIG";
		case FDS_RECORD_KEY_M_COMM_MODE: return "FDS_RECORD_KEY_M_COMM_MODE";
		case FDS_RECORD_KEY_M_COMM_CAPS_ON: return "FDS_RECORD_KEY_M_COMM_CAPS_ON";
		case FDS_RECORD_KEY_M_COMM_PASS_KEY: return "FDS_RECORD_KEY_M_COMM_PASS_KEY";
		case FDS_RECORD_KEY_M_MANUFACTURE_INFO: return "FDS_RECORD_KEY_M_MANUFACTURE_INFO";
		case FDS_RECORD_KEY_M_TEST_INFO: return "FDS_RECORD_KEY_M_TEST_INFO";
		case FDS_RECORD_KEY_M_TOUCH_VER: return "FDS_RECORD_KEY_M_TOUCH_VER";
		default: return "UNKNOWN";
	}
}
#endif


/*----------------------------------------------------------------------------//
//                             UNUSED FUNCTION HEADS                          //
//----------------------------------------------------------------------------*/
#if 0 ///////////////////////// delete dongle ////////////
void ChangeToDongle_delayed(uint32_t delay_ms)
{
	// Only when the selected channel is for the Dongle
	if(m_config.config.selected_ch != M_COMM_CHANNEL_DONGLE)
	{
		return;
	}
	
	//Method1:
	//	Change_to_DONGLE(M_COMM_CHANNEL_DONGLE);
	// <-- 시차가 적어 문제 발생.
	
	//Method2:
	//	m_event |= M_EVENT_CHANNEL_CHANGED;
	//	m_arg.channel.dongle_pseudo = 1;
	// <-- 시차가 적어 문제 발생.
	
	//Method3:
	// <-- 100ms 이후에 처리 : ok
	fds_RequestToCallDongle_AfterThisTime("OnFdsWrite:", delay_ms);
		// later invoke --> Change_to_DONGLE(M_COMM_CHANNEL_DONGLE);

}
#endif 











/*\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ 
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\_*/




 /*$$$$\  $$\       $$$$$$\  $$$$$$$\   $$$$$$\  $$\                         
$$  __$$\ $$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                        
$$ /  \__|$$ |     $$ /  $$ |$$ |  $$ |$$ /  $$ |$$ |                        
$$ |$$$$\ $$ |     $$ |  $$ |$$$$$$$\ |$$$$$$$$ |$$ |                        
$$ |\_$$ |$$ |     $$ |  $$ |$$  __$$\ $$  __$$ |$$ |                        
$$ |  $$ |$$ |     $$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |                        
\$$$$$$  |$$$$$$$$\ $$$$$$  |$$$$$$$  |$$ |  $$ |$$$$$$$$\                   
 \______/ \________|\______/ \_______/ \__|  \__|\________|                  
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\ 
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__|
//============================================================================//
//                                 GLOBAL FUNCTIONS                           //
//============================================================================*/


//================================================================================
//	Function name : ret_code_t drv_fds_init()
//================================================================================
ret_code_t drv_fds_init(void)
{		
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	________________________________DBG_flimbs_20190307_drvFds_BootFailAfterDrvFdsInit(0,"void");		
	
	________________________________DBG_flimbs_20190307_drvFds_BootFailAfterDrvFdsInit(1, "for(int i=0; i < (FDS_RECORD_KEY_MAX:%d - FDS_RECORD_KEY_SAMPLE1:%d); i++)", FDS_RECORD_KEY_MAX, FDS_RECORD_KEY_SAMPLE1);
	for(int i=0; i < (FDS_RECORD_KEY_MAX - FDS_RECORD_KEY_SAMPLE1); i++)
	{		
		________________________________DBG_flimbs_20190307_drvFds_BootFailAfterDrvFdsInit(2,"FlashDataToBeWritten[i:%d] = (void *)0;",i);
		FlashDataToBeWritten[i] = (void *)0;
	}
	
	//==================================================================
	// peer_data_storage.c에서
	// 'fds_evt_handler'를 등록(fds_register)하지 않고, 대신 여기서 추가로 호출해준다.
	// 즉 등록된 'drv_fds_evt_handler'에서'fds_evt_handler'를 추가로 호출해준다.
	// <-- fds_init()을 Power On Reset시 한번 만 호출하기 위함이다.
	//==================================================================
	________________________________DBG_flimbs_20190307_drvFds_BootFailAfterDrvFdsInit(1,"ret_code_t ret = fds_register(drv_fds_evt_handler:%d);", drv_fds_evt_handler);
	ret_code_t ret = fds_register(drv_fds_evt_handler);
	________________________________DBG_flimbs_20190307_drvFds_BootFailAfterDrvFdsInit(1, "ret:%d != FDS_SUCCESS:%d", ret, FDS_SUCCESS);		
	if (ret != FDS_SUCCESS)
	{
		________________________________DBG_flimbs_20190307_drvFds_BootFailAfterDrvFdsInit(2,"return ret:%d",ret);
		return ret;
	}

	________________________________DBG_flimbs_20190307_drvFds_BootFailAfterDrvFdsInit(1,"reset_fds_op_trace();");
	reset_fds_op_trace();	// is_fds_op_finshed(FDS_EVT_INIT)를 위하여 reset한다.

	________________________________DBG_flimbs_20190307_drvFds_BootFailAfterDrvFdsInit(1,"ret = fds_init();");
	ret = fds_init();	// cf. see: peer_data_storage.c !!!!	
	
	________________________________DBG_flimbs_20190307_drvFds_BootFailAfterDrvFdsInit(1, "if (ret:%d != FDS_SUCCESS:%d)",ret, FDS_SUCCESS);
	if (ret != FDS_SUCCESS)
	{
		________________________________DBG_flimbs_20190307_drvFds_BootFailAfterDrvFdsInit(2,"return ret:%d",ret);
		return ret;
	}


	// fds 가 완료될 때까지 기다린다.
	________________________________DBG_flimbs_20190307_drvFds_BootFailAfterDrvFdsInit(2,"while(1)");
	uint8_t cnt=0;
	while(1)
	{
		cnt++;
		if(	is_fds_op_finshed(FDS_EVT_INIT) )
		{			
			// fds_init finished
			________________________________DBG_flimbs_20190307_drvFds_BootFailAfterDrvFdsInit(1,"if(	is_fds_op_finshed(FDS_EVT_INIT) )");
			________________________________DBG_flimbs_20190307_drvFds_BootFailAfterDrvFdsInit(2,"while(1){fds_init_finished(FDS_EVT_INIT:%d)}",FDS_EVT_INIT);
			break;
		}
		if(cnt >= UINT8_MAX)
		{
			________________________________DBG_flimbs_20190307_drvFds_BootFailAfterDrvFdsInit(1,"if(cnt>=UINT8_MAX)");
			________________________________DBG_flimbs_20190307_drvFds_BootFailAfterDrvFdsInit(2,"return NRF_ERROR_INVALID_STATE;");
			return NRF_ERROR_INVALID_STATE;
		}
	}
	
#if 0 // BLOCK_MJ_FDS
	memset(m_bufffer, 0, sizeof(m_bufffer));

	#if	defined(DRV_FDS_TRACE)
	DEB("===CLEARED: m_bufffer[] in drv_fds_init().\n");
	#endif
#endif	
				
	________________________________DBG_flimbs_20190307_drvFds_BootFailAfterDrvFdsInit(1, "return NRF_SUCCESS:%d",NRF_SUCCESS);
	return NRF_SUCCESS;			
}





//================================================================================
//	Function name : drv_fds_erase()
//================================================================================
void drv_fds_erase(void)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	m_fds_erase();
}







//================================================================================
//	Function name : void reset_fds_op_trace(void)
//================================================================================
void reset_fds_op_trace(void)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	________________________________DBG_flimbs_20190307_drvFds_BootFailAfterDrvFdsInit(0,"");
	m_fds_evt_refreshed= 0;
}







//================================================================================
//	Function name : bool is_fds_op_finshed(fds_evt_id_t id)
//================================================================================
bool is_fds_op_finshed(fds_evt_id_t id)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	________________________________DBG_flimbs_20190307_drvFds_BootFailAfterDrvFdsInit(0,"fds_evt_id_t:%d", id);	
	
	________________________________DBG_flimbs_20190307_drvFds_BootFailAfterDrvFdsInit(1,"if(m_fds_evt_refreshed:%d)",m_fds_evt_refreshed);
	if(m_fds_evt_refreshed)
	{
		________________________________DBG_flimbs_20190307_drvFds_BootFailAfterDrvFdsInit(2,"if(m_fds_evt_last.id:%d == id:%d)",m_fds_evt_last.id,id);
		if(m_fds_evt_last.id == id)
		{
			________________________________DBG_flimbs_20190307_drvFds_BootFailAfterDrvFdsInit(3,"return 1:finished");
			return 1; // yes, finished
		}
	}
	________________________________DBG_flimbs_20190307_drvFds_BootFailAfterDrvFdsInit(1,"return 0: not finished");
	return 0; // No, not yet
}







//================================================================================
//	Function name : fds_write_stanley()
//================================================================================
ret_code_t fds_write_stanley(uint16_t file_id, uint16_t record_key, const uint8_t *pUserData, uint16_t size_in_bytes)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	
	fds_record_t			record;
    fds_record_desc_t		record_desc;
    fds_find_token_t		ftok;
	ret_code_t				ret;
	
	uint16_t record_key_index;
	void *pRecordData;

	
	if(record_key < FDS_RECORD_KEY_SAMPLE1 || record_key >= FDS_RECORD_KEY_MAX) return FDS_ERR_INTERNAL;
	//
	//DEB("fds_write_stanley: BEGIN\n");	

	
	record_key_index= record_key - FDS_RECORD_KEY_SAMPLE1;
	//DEB("fds_write_stanley: record_key_index=%d \n",record_key_index);	
	
	if(FlashDataToBeWritten[record_key_index])
	{
		//free(FlashDataToBeWritten[record_key_index]);
	} else {
		pRecordData= (void *)malloc(size_in_bytes);
		FlashDataToBeWritten[record_key_index]= pRecordData;
	}
	
	pRecordData= FlashDataToBeWritten[record_key_index];
	memcpy(pRecordData, pUserData, size_in_bytes);
	
	
	memset(&ftok, 0x00, sizeof(fds_find_token_t));
	memset(&record, 0x00, sizeof(fds_record_t));
	memset(&record_desc, 0x00, sizeof(fds_record_desc_t));
	
    record.file_id           = file_id;
    record.key               = record_key;
    //record.data.p_data     = (void *)pUserData;
    record.data.p_data       = (void *)pRecordData;
    //record.data.length_words = (uint16_t) ( (size_in_bytes+BYTES_PER_WORD-1) / BYTES_PER_WORD );
    record.data.length_words = (uint16_t) ( size_in_bytes / BYTES_PER_WORD ); // 끝의 자투리(dummy)는 잘릴 수 있다.

	
	ret = fds_record_find(FDS_FILE_ID_STANLEY, record_key, &record_desc, &ftok);
    if(ret == FDS_SUCCESS)
    {
		
		//DEB("fds_write_stanley: FDS_RECORD_FOUND: key= 0x%x(%d) ",record_key, record_key);
		//DEB("fds_write_stanley: FDS_TRY_UPDATE: with=");for(int i=0; i < size_in_bytes; i++) DEB(" %02X", *(uint8_t *)((uint8_t *)pRecordData+i));
		//DEB("\n");
		
		//DEB("fds_write_stanley: fds_record_update(&record_desc, &record) ... pRecordData= 0x%x \n", pRecordData);
		ret = fds_record_update(&record_desc, &record);
		// 실제 update되기까지는 시간이 걸린다... pointer(pRecordData)는 record별로 모두 달라야 한다 !! 그래야 겹쳐쓰기가 없다.
		
	} else {
		
		ret = fds_record_write(&record_desc, &record);
		if(ret == FDS_SUCCESS)
		{
			//DEB("fds_write_stanley: FDS_TRY_WRITE: SUCCESS - (file_id=0x%x, record_key=0x%x, pRecordData=0x%x, sizeBytes=%d) :",
			//	file_id, record_key, pRecordData, size_in_bytes
			//);
			//for(int i=0; i < size_in_bytes; i++)
			//{
			//	DEB(" %02X", *(uint8_t *)((uint8_t *)pRecordData+i) );
			//}
			//DEB("\n");
		} else {
			//DEB("fds_write_stanley: FDS_TRY_WRITE: FAILED - fds_write_stanley() \n");
		}
	}
	
	//DEB("fds_write_stanley: THE ACTUAL WRITING will be done ... far LATER ...\n");
	//DEB("fds_write_stanley: END\n");
	
	return ret;
}



//================================================================================
//	Function name : ret_code_t drv_fds_init()
//================================================================================
ret_code_t fds_read_stanley(uint16_t file_id, uint16_t record_key, uint16_t size_in_bytes, uint8_t *pUserBuff)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");	
	fds_find_token_t    ftok;
	fds_record_desc_t   record_desc;
	fds_flash_record_t	flash_record;	// flash에 
	ret_code_t 			ret;		
	
	memset(&ftok, 0x00, sizeof(fds_find_token_t));
	memset(&flash_record, 0x00, sizeof(fds_flash_record_t));
	memset(&record_desc, 0x00, sizeof(fds_record_desc_t));
		
	ret = fds_record_find(file_id, record_key, &record_desc, &ftok);
	
	if(FDS_SUCCESS == ret)
	{
		
		ret = fds_record_open(&record_desc, &flash_record);

		if ( ret != FDS_SUCCESS)
		{
			
			return ret;
		}
		if(flash_record.p_data)
		{			
			//DEB("fds_read_stanley: memcpy(pUserBuff,record.p_data, %d)\n",size_in_bytes);
			// bytes계산 주의:
			//	(1) 저장길이: record.data.length_words = (uint16_t) ( size_in_bytes / BYTES_PER_WORD ); // 끝의 자투리(dummy)는 잘릴 수 있다.
			//	(2) 읽을 길이: (size_in_bytes / BYTES_PER_WORD) * BYTES_PER_WORD
			
			memcpy(pUserBuff, flash_record.p_data, (size_in_bytes / BYTES_PER_WORD) * BYTES_PER_WORD);			
		}
		else 
		{			
			return NRF_ERROR_INVALID_PARAM;
		}		
		
		ret = fds_record_close(&record_desc);

		
		if (ret != FDS_SUCCESS)
		{
			return ret;	
		}
	}
	else
	{
		//DEB("fds_read_stanley: fds_record_find()....FAILED.... \n");
	}

	return ret;
}


//================================================================================
//	Function name :  fds_delete_stanley()
//================================================================================
ret_code_t fds_delete_stanley(uint16_t file_id, uint16_t record_key)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t			ret;
    fds_find_token_t	ftok;
	fds_record_desc_t	record_desc;  
	//fds_flash_record_t	flash_record;

	
	memset(&ftok, 0x00, sizeof(fds_find_token_t));
	memset(&record_desc, 0x00, sizeof(fds_record_desc_t));

	
	ret = fds_record_find(file_id, record_key, &record_desc, &ftok);
	if(ret==FDS_SUCCESS)
	{
		// invalidate the record. (make it dirty)
		ret= fds_record_delete(&record_desc);
		//
		#ifdef TRACE_FDS_READWRITE
			DEB("===Try FDS_DELETE Result= ");
			switch(ret)
			{
				case FDS_ERR_NOT_INITIALIZED:	DEB("FDS_ERR_NOT_INITIALIZED"); break;
				case FDS_ERR_NULL_ARG: 			DEB("FDS_ERR_NULL_ARG"); break;
				case FDS_ERR_NO_SPACE_IN_QUEUES:DEB("FDS_ERR_NO_SPACE_IN_QUEUES"); break;
				default: DEB("UNKNOWN ERR"); break;
			}
			DEB("\n");
		#endif

	} else {
		#ifdef TRACE_FDS_READWRITE
			DEB("===FDS_RECORD_FIND To DELETE .... FAILED \n");
		#endif
	}
	
	return ret;
}





//================================================================================
//	Function name : IsValidFlash()
//================================================================================
bool IsValidFlash(void)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	InfoFlash_t buff;
	
	memset(&buff, 0, sizeof(InfoFlash_t));
	if( ReadInfoFromFlash(&buff) )
	{		
		return (buff.validity == VALID_FLASH_INFO) ? 1: 0;
	}
	
	return 0;
}




//================================================================================
//	Function name : SaveInfoToFlash()
//================================================================================
// m_Info-->infoInFlash-->Flash
void SaveInfoToFlash(uint8_t ch_to_save)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	InfoFlash_t buff;	
	memset(&buff, 0, sizeof(InfoFlash_t));
	copy_mInfo_to_buff(&buff);

	buff.comm_config.config.selected_ch= ch_to_save; 	// <---
	buff.validity = VALID_FLASH_INFO; 					//
	buff.saveCount++;									//

	fds_write_stanley(FDS_FILE_ID_STANLEY,FDS_RECORD_KEY_TOT_INFO, (uint8_t *)&buff, sizeof(InfoFlash_t));// // 실제 write되기 까지는 시간이 걸리므로...
}




//================================================================================
//	Function name : RestoreInfoFromFlash()
//================================================================================
// Flash-->infoInFlash --> Info
bool RestoreInfoFromFlash(void)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	InfoFlash_t buff, *p;
	bool ret;
	
	p= &buff;
	if( ReadInfoFromFlash(p) )
	{
		if(p->validity)
		{			
			copy_buff_to_mInfo(p); // infoInFlash --> m_Info
			ret= 1; // true
		}
		 else 
		{
			ret= 0; // false			
		}
	} 
	else 
	{
		ret= 0; // false
	}
	
	return ret;
}




//================================================================================
//	Function name : fds_RequestToSaveAfterThisTime()
//================================================================================
void fds_RequestToSaveAfterThisTime(uint8_t ch_to_save, uint32_t delay_ms)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	int32_t min_ms;
	
	// 0이면, save하지 않을 것이므로, 최소 1 이상이어야 한다.
	min_ms= (delay_ms <= 0) ? 1: delay_ms;
	//
	m_save_after_this_ms += min_ms;
	//
	m_ch_to_save= ch_to_save;
	
#ifdef TRACE_SAVING_TIMEDELAY
	DEB("+++++++ RequestToSaveAfterThisTime(%d ms) --> m_save_after_this_ms=%d  +++++++\n",
		delay_ms,m_save_after_this_ms
	);
#endif//TRACE_SAVING_TIMEDELAY
}




//================================================================================
//	Function name : CallMeRepeatedlyToSaveInfoToFlash()
//================================================================================
void CallMeRepeatedlyToSaveInfoToFlash(uint32_t every_call_ms)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	static uint32_t elapsed_ms=0;

	// do nothing if there is no request.
	if(m_save_after_this_ms <= 0) return;
	
	// if there was any request...
	elapsed_ms += every_call_ms;

	//
	if( elapsed_ms < m_save_after_this_ms ){
		// do not save iff the required time is not elapsed yet
		return;
	}
	// (1) 'm_save_after_this_ms'후에 저장해달라는 요청이 있었고,
	// (2) 시간이(elapsed_time)이 이(m_save_after_this_ms)를 경과하였으므로.... 저장한다.
	//DEB("\n ...SaveInfoToFlash()...\n");
	SaveInfoToFlash(m_ch_to_save);
	
	// 요청을 처리했으므로...
	elapsed_ms= 0;				// 다음 counting을 위하여 초기화
	m_save_after_this_ms=0;		// no request
}




//================================================================================
//	Function name : fds_RequestToCallDongle_AfterThisTime()
//================================================================================
void fds_RequestToCallDongle_AfterThisTime(char *msg, uint32_t delay_ms)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	int32_t min_ms;
	
	// 0이면, call하지 않을 것이므로, 최소 1 이상이어야 한다.
	min_ms= (delay_ms <= 0) ? 1: delay_ms;
	//
	m_call_dongle_after_this_ms += min_ms;

#ifdef TRACE_SAVING_TIMEDELAY
	DEB("+++++++%s+++++ fds_RequestToCallDongle_AfterThisTime(%d ms) --> m_call_dongle_after_this_ms=%d  +++++++\n",
		msg, delay_ms, m_call_dongle_after_this_ms
	);
#endif
}


//================================================================================
//	Function name : fds_RequestToCallBT_AfterThisTime()
//================================================================================
void fds_RequestToCallBT_AfterThisTime(char *msg, uint32_t delay_ms)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	int32_t min_ms;
	
	// 0이면, call하지 않을 것이므로, 최소 1 이상이어야 한다.
	min_ms= (delay_ms <= 0) ? 1: delay_ms;
	//
	m_call_bt_after_this_ms += min_ms;

#ifdef TRACE_SAVING_TIMEDELAY
	DEB("+++++++%s+++++ fds_RequestToCallBT_AfterThisTime(%d ms) --> m_call_dongle_after_this_ms=%d  +++++++\n",
		msg, delay_ms, m_call_bt_after_this_ms
	);
#endif	
}






/*\       $$$$$$\   $$$$$$\   $$$$$$\  $$\                                   
$$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                                  
$$ |     $$ /  $$ |$$ /  \__|$$ /  $$ |$$ |                                  
$$ |     $$ |  $$ |$$ |      $$$$$$$$ |$$ |                                  
$$ |     $$ |  $$ |$$ |      $$  __$$ |$$ |                                  
$$ |     $$ |  $$ |$$ |  $$\ $$ |  $$ |$$ |                                  
$$$$$$$$\ $$$$$$  |\$$$$$$  |$$ |  $$ |$$$$$$$$\                             
\________|\______/  \______/ \__|  \__|\________|                            
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\ 
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__|
//============================================================================//
//                                 LOCAL FUNCTIONS                            //
//============================================================================*/


//================================================================================
//	Function name : drv_fds_evt_handler()
//================================================================================
static void drv_fds_evt_handler(fds_evt_t const * p_fds_evt)
{	
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
    ________________________________LOG_drvFds_oldDebShow(1,"__FDS___: %s: file_id:=0x%x, record_key:=0x%x(%s), result=%s",	GetStr_fds_evt_id(p_fds_evt->id),	p_fds_evt->write.file_id,	p_fds_evt->write.record_key,	GetStr_fds_record_key(p_fds_evt->write.record_key),	(p_fds_evt->result==FDS_SUCCESS)?"SUCCESS":"FAILED");
	// Remember the last result
	memcpy(&m_fds_evt_last, p_fds_evt, sizeof(fds_evt_t));
	m_fds_evt_refreshed= 1;

		
#ifdef COMBINE_FDS_HANDLERS
	//-----------------------------------------------------------------------------------------------------------------------------------
	// see: peer_data_storage.c
	//==================================================================
	// peer_data_storage.c에서
	// 'fds_evt_handler'를 등록(fds_register)하지 않고, 대신 여기서 추가로 호출해준다.
	// 즉 등록된 'drv_fds_evt_handler'에서'fds_evt_handler'를 추가로 호출해준다.
	// <-- fds_init()을 Power On Reset시 한번 만 호출하기 위함이다.
	//==================================================================
	if(p_fds_evt->write.file_id != FDS_FILE_ID_STANLEY)
	{
		fds_evt_handler(p_fds_evt);
	}
	
#endif


	//-----------------------------------------------------------------------------------------------------------------------------------
	//
	//-----------------------------------------------------------------------------------------------------------------------------------
    switch (p_fds_evt->id)
    {
        case FDS_EVT_INIT:
			//p_evt->id = FDS_EVT_INIT;
			//p_evt->result;
			break;

        case FDS_EVT_WRITE:
			//p_evt->id
			//p_evt->result;
			//p_evt->write.file_id
			//p_evt->write.record_key
			//p_evt->write.record_id
			//p_evt->write.is_record_updated
			
			//DEB("===FDS_EVT_WRITE:\n");
			if( p_fds_evt->result==FDS_SUCCESS)
			{
				if(p_fds_evt->write.file_id == FDS_FILE_ID_STANLEY)
				{
					//DEB("===FDS_EVT_WRITE: by Stanley's\n");
					
				//	// ??? ???? ????
				//	if(m_config.config.selected_ch == M_COMM_CHANNEL_DONGLE)
				//	{
				//		ChangeToDongle_delayed(CALLDELAY_FDS_TO_DONGLE);
				//	}
				}
			}
			break;

        case FDS_EVT_UPDATE:
			//p_evt->id
			//p_evt->result;
			//p_evt->write.file_id
			//p_evt->write.record_key
			//p_evt->write.record_id
			//p_evt->write.is_record_updated
			if( p_fds_evt->result==FDS_SUCCESS)
			{
				if(p_fds_evt->write.file_id == FDS_FILE_ID_STANLEY)
				{
					// update?? ????? dirty record ?? ??.
					//DEB("===OnUpdate Call ...FDS_GC()...\n");
					//fds_gc();
					//
				//	if(m_config.config.selected_ch == M_COMM_CHANNEL_DONGLE)
				//	{
				//		ChangeToDongle_delayed(CALLDELAY_FDS_TO_DONGLE);
				//	}
				}
				//
				fds_gc();
				
			}
			break;

        case FDS_EVT_DEL_RECORD:
			//p_evt->id;
			//p_evt->result;
			//p_evt->del.file_id
			//p_evt->del.record_key
			//p_evt->del.record_id
			if( p_fds_evt->result==FDS_SUCCESS)
			{
				if(p_fds_evt->del.file_id == FDS_FILE_ID_STANLEY)
				{
					//DEB("===OnDelRecord Call ...FDS_GC()...\n");
					//fds_gc();
				}
				fds_gc();
			}
			break;

        case FDS_EVT_DEL_FILE:
			//p_evt->id
			//p_evt->result;
			//p_evt->del.file_id
			//p_evt->del.record_key
			//p_evt->del.record_id
			if( p_fds_evt->result==FDS_SUCCESS)
			{
				if(p_fds_evt->del.file_id == FDS_FILE_ID_STANLEY)
				{
					//DEB("===OnDelFile Call ...FDS_GC()...\n");
					//fds_gc();
				}
				fds_gc();
			}
			break;

        case FDS_EVT_GC:
			//p_evt->id = FDS_EVT_GC;
			//p_evt->result;
			//DEB("=== FDS_EVT_GC: \n");
			break;

		default:
			break;
    }

	// Stanley added.
	//fds_gc();
	
}








//================================================================================
//	Function name : copy_mInfo_to_buff()
//================================================================================
static void copy_mInfo_to_buff(InfoFlash_t *pBuff)
{	
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	//-----------------------------------------------------------------------------------------------------------------------------------
	//?? ???.
	//PMstorage_to_buff_stanley(&pBuff->pm_storage);

	//-----------------------------------------------------------------------------------------------------------------------------------
	memcpy(&pBuff->comm_mode, 			&m_comm_mode, 			sizeof(m_comm_mode_t)				);
	memcpy(&pBuff->comm_config, 		&m_config, 				sizeof(m_comm_config_t)				);
	memcpy(&pBuff->conn_handle, 		&m_conn_handle, 		sizeof(m_conn_handle)				);
	memcpy(&pBuff->comm_adv_config, 	&m_adv_config, 			sizeof(m_comm_adv_config_t)			);
	memcpy(&pBuff->comm_adv_status, 	&m_adv_status, 			sizeof(m_comm_adv_status_t)			);
	memcpy(&pBuff->comm_adv_channel, 	&m_adv_channel, 		sizeof(m_comm_adv_channel_type_t)	);
	//
	memcpy(&pBuff->gatt,				&m_gatt,				sizeof(nrf_ble_gatt_t)				);
	//
	memcpy(&pBuff->manufacture_info, 	&m_manufacture_info, 	sizeof(m_mokibo_manufacture_info_t)	);
	memcpy(&pBuff->test_info, 			&m_test_info, 			sizeof(m_mokibo_test_info_t)		);

	//DEB("copy_mInfo_to_buff: 뚱\n");
}





//================================================================================
//	Function name : copy_buff_to_mInfo()
//================================================================================
//	infoFlash --> m_buff
static void copy_buff_to_mInfo(InfoFlash_t *pBuff)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	//-----------------------------------------------------------------------------------------------------------------------------------
	//?? ???...
	//buff_to_PMstorage_stanley(pBuff);

	//-----------------------------------------------------------------------------------------------------------------------------------
	memcpy(&m_comm_mode, 		&pBuff->comm_mode, 				sizeof(m_comm_mode_t)				);
	memcpy(&m_config, 			&pBuff->comm_config, 			sizeof(m_comm_config_t)				);
	// <-- m_conn_handle???? ???.
	//memcpy(&m_conn_handle, 		&pBuff->conn_handle, 			sizeof(m_conn_handle)				);
	memcpy(&m_adv_config, 		&pBuff->comm_adv_config, 		sizeof(m_comm_adv_config_t)			);
	memcpy(&m_adv_status, 		&pBuff->comm_adv_status, 		sizeof(m_comm_adv_status_t)			);
	memcpy(&m_adv_channel, 		&pBuff->comm_adv_channel, 		sizeof(m_comm_adv_channel_type_t)	);
	//
//	memcpy(&m_gatt,				&pBuff->gatt,					sizeof(nrf_ble_gatt_t)				);
	//
	memcpy(&m_manufacture_info, &pBuff->manufacture_info, 		sizeof(m_mokibo_manufacture_info_t)	);
	memcpy(&m_test_info, 		&pBuff->test_info, 				sizeof(m_mokibo_test_info_t)		);
}







//================================================================================
//	Function name : ReadInfoFromFlash()
//================================================================================
static bool ReadInfoFromFlash(InfoFlash_t *p)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t ret;
	
	ret= fds_read_stanley(FDS_FILE_ID_STANLEY,FDS_RECORD_KEY_TOT_INFO, sizeof(InfoFlash_t), (uint8_t *)p );	
		
	
	return (ret==FDS_SUCCESS) ? 1 : 0;
}









































/*$$$$$\  $$$$$$$$\ $$$$$$$\  $$\   $$\  $$$$$$\                             
$$  __$$\ $$  _____|$$  __$$\ $$ |  $$ |$$  __$$\                            
$$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |$$ /  \__|                           
$$ |  $$ |$$$$$\    $$$$$$$\ |$$ |  $$ |$$ |$$$$\                            
$$ |  $$ |$$  __|   $$  __$$\ $$ |  $$ |$$ |\_$$ |                           
$$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |$$ |  $$ |                           
$$$$$$$  |$$$$$$$$\ $$$$$$$  |\$$$$$$  |\$$$$$$  |                           
\_______/ \________|\_______/  \______/  \______/                            
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\ 
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__|
//============================================================================//
//                                 DEBUG FUNCTIONS                            //
//============================================================================*/


#ifdef TRACE_FLASH_SAVED_INFO
//-----------------------------------------------------------------------------------------------------------------------------------
//			ShowTotInfo
//-----------------------------------------------------------------------------------------------------------------------------------
void ShowTotInfo(char *msg)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");

	show_flashInfo(msg);
	Update_WDT();

	show_mInfo(msg);	
	Update_WDT();
}
#endif //TRACE_FLASH_SAVED_INFO


#ifdef TRACE_FLASH_SAVED_INFO
//-----------------------------------------------------------------------------------------------------------------------------------
//			show_flashInfo
//-----------------------------------------------------------------------------------------------------------------------------------
void show_flashInfo(char *msg)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	
	InfoFlash_t buff, *p;
	
	//-----------------------------------------------------------------------------------------------------------------------------------
	p= &buff;
	ReadInfoFromFlash(p);
	
	//-----------------------------------------------------------------------------------------------------------------------------------
	________________________________LOG_drvFds_oldDebShow(1,"============show_flashInfo (%s)=================n",msg);
	________________________________LOG_drvFds_oldDebShow(1, "Validity :0x%x", p->validity);
	________________________________LOG_drvFds_oldDebShow(1,"%s", (p->validity==VALID_FLASH_INFO)? "***VALID***":"---INVALID---");
	________________________________LOG_drvFds_oldDebShow(1," SaveCount:%d", p->saveCount);
	//-----------------------------------------------------------------------------------------------------------------------------------
	debshow_comm_mode(msg,p->comm_mode);
	debshow_comm_config(msg,p->comm_config);
	debshow_conn_handle(msg,p->conn_handle);
	debshow_comm_adv_config(msg,p->comm_adv_config);
	debshow_comm_adv_status(msg,p->comm_adv_status);
	debshow_comm_adv_channel(msg,p->comm_adv_channel);
	debshow_gatt(msg,p->gatt);
	//
	debshow_mokibo_manufacture_info(msg,p->manufacture_info);
	debshow_mokibo_test_info(msg,p->test_info);
}
#endif //TRACE_FLASH_SAVED_INFO



#ifdef TRACE_FLASH_SAVED_INFO
//-----------------------------------------------------------------------------------------------------------------------------------
//			show_mInfo
//-----------------------------------------------------------------------------------------------------------------------------------
void show_mInfo(char *msg)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	________________________________LOG_drvFds_oldDebShow(1,"===============show_mInfo(%s)====================",msg);
	debshow_comm_mode("m_comm_mode",m_comm_mode);
	debshow_comm_config("m_config",m_config);
	debshow_conn_handle("m_conn_handle",m_conn_handle);
	debshow_comm_adv_config("m_adv_config",m_adv_config);
	debshow_comm_adv_status("m_adv_status",m_adv_status);
	debshow_comm_adv_channel("m_adv_channel",m_adv_channel);
	debshow_gatt("m_gatt",m_gatt);

	//
	debshow_mokibo_manufacture_info("m_manufacture_info",m_manufacture_info);
	debshow_mokibo_test_info("m_test_info",m_test_info);
	debshow_passkey("m_passkey", m_passkey);
}
#endif //TRACE_FLASH_SAVED_INFO



//-----------------------------------------------------------------------------------------------------------------------------------
//			debshow_gap_addr
//-----------------------------------------------------------------------------------------------------------------------------------
void debshow_conn_stat(const char *msg, conn_stat_t conn_stat)
{	
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	________________________________LOG_drvFds_oldDebShow(1,"CONN_STAT:(%s:) - try_ch=%d, prev_ch=%d --> conn_ch=%d ",msg,conn_stat.conn_try_ch,conn_stat.connected_ch_prev,conn_stat.connected_ch);
	________________________________LOG_drvFds_oldDebShow(1,"\t paired[]= "); 

	for(int i=0; i<M_COMM_CHANNEL_MAX; i++) 	
	{
		________________________________LOG_drvFds_oldDebShow(2," %d",conn_stat.paired[i]);
	}
	________________________________LOG_drvFds_oldDebShow(1,"\t conn_handle[]= "); 
	
	for(int i=0; i<M_COMM_CHANNEL_MAX; i++) 
	{
		________________________________LOG_drvFds_oldDebShow(2," %04x",conn_stat.conn_handle[i]);
	}
}



//-----------------------------------------------------------------------------------------------------------------------------------
//			debshow_gap_addr
//-----------------------------------------------------------------------------------------------------------------------------------
void debshow_gap_addr(ble_gap_addr_t *p_g_addr)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	________________________________LOG_drvFds_oldDebShow(1," gap_addr id_peer=%d, type=0x%x",
		p_g_addr->addr_id_peer,
		p_g_addr->addr_type
	);
	
	for(int j=0; j < BLE_GAP_ADDR_LEN; j++){
		________________________________LOG_drvFds_oldDebShow(2,"addr[]= %02x",p_g_addr->addr[j]);
	}
	
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			debshow_comm_mode
//-----------------------------------------------------------------------------------------------------------------------------------
void debshow_comm_mode(const char *msg, m_comm_mode_t comm_mode)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	________________________________LOG_drvFds_oldDebShow(1,"COMM_MODE:(%s:)- %s", msg, (m_comm_mode==M_COMM_MODE_BLE)? "BLE":"DONGLE" );
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			debshow_conn_handle
//-----------------------------------------------------------------------------------------------------------------------------------
void debshow_conn_handle(const char *msg, uint16_t conn_handle)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	________________________________LOG_drvFds_oldDebShow(1,"CONN_HANDLE:(%s:)- 0x%x", msg, conn_handle);
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			debshow_gatt_link
//-----------------------------------------------------------------------------------------------------------------------------------
void debshow_gatt_link(nrf_ble_gatt_link_t link)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	________________________________LOG_drvFds_oldDebShow(1,"desired=%d", link.att_mtu_desired);
	________________________________LOG_drvFds_oldDebShow(1,", att_mtu_effective=%d", link.att_mtu_effective);
	________________________________LOG_drvFds_oldDebShow(1,", att_mtu_exchange_pending= %d", link.att_mtu_exchange_pending);
	________________________________LOG_drvFds_oldDebShow(1,", att_mtu_exchange_requested= %d", link.att_mtu_exchange_requested);
	________________________________LOG_drvFds_oldDebShow(1,", data_length_desired=%d", link.data_length_desired);
	________________________________LOG_drvFds_oldDebShow(1,", data_length_effective=%d", link.data_length_effective);
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			debshow_gatt
//-----------------------------------------------------------------------------------------------------------------------------------
void debshow_gatt(const char *msg, nrf_ble_gatt_t gatt)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	________________________________LOG_drvFds_oldDebShow(1,"GATT:(%s) ",msg);
	________________________________LOG_drvFds_oldDebShow(1," att_mtu_desired_periph =%d ", gatt.att_mtu_desired_periph);
	________________________________LOG_drvFds_oldDebShow(1," att_mtu_desired_central=%d ", gatt.att_mtu_desired_central);
	________________________________LOG_drvFds_oldDebShow(1,"data_length=%d ", gatt.data_length);
	________________________________LOG_drvFds_oldDebShow(1,"links[]");
	for(int i=0; i < NRF_BLE_GATT_LINK_COUNT; i++)
	{		
		//debshow_gatt_link( gatt.links[i] );		
	}
}


//-----------------------------------------------------------------------------------------------------------------------------------
//			debshow_comm_config
//-----------------------------------------------------------------------------------------------------------------------------------
void debshow_comm_config(const char *msg, m_comm_config_t comm_config)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	#if MOKIBO_DEBUG==MOKIBO_DEBUG_ON
		uint8_t selected_ch, central;
		//
		selected_ch= comm_config.config.selected_ch;
		central = comm_config.config.central[selected_ch];
	#endif
	
	//----------------------------------------------------------------------
	________________________________LOG_drvFds_oldDebShow(1,"COMM_CONFIG:(%s:)", msg);
	________________________________LOG_drvFds_oldDebShow(1," selected_ch=%d (%s)", selected_ch, GetStr_comm_channel(selected_ch) );
	________________________________LOG_drvFds_oldDebShow(1," central=%s", GetStr_comm_central(central ) 	);
	//----------------------------------------------------------------------
	for(int i=0; i < M_COMM_CHANNEL_MAX; i++){
		________________________________LOG_drvFds_oldDebShow(1," central[%d]=%s",i, GetStr_comm_central( comm_config.config.central[i] ) );
	}
	
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			debshow_comm_adv_config
//-----------------------------------------------------------------------------------------------------------------------------------
void debshow_comm_adv_config(const char *msg, m_comm_adv_config_t comm_adv_config)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	//(1,"COMM_ADV_CONFIG:(%s:) ",msg);
	for(int i=0; i < M_COMM_ADV_CHANNEL_MAX; i++){
		________________________________LOG_drvFds_oldDebShow(1,"\t config[%d]: id=0x%04x", i, comm_adv_config.config[i].id);
		debshow_gap_addr(&comm_adv_config.config[i].g_addr);		
	}
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			debshow_comm_adv_status
//-----------------------------------------------------------------------------------------------------------------------------------
void debshow_comm_adv_status(const char *msg, m_comm_adv_status_t comm_adv_status)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	//(1,"COMM_ADV_STATUS:(%s:) ",msg);
	switch(comm_adv_status)
	{
		case M_COMM_ADV_STATUS_NONE: ________________________________LOG_drvFds_oldDebShow(1,"M_COMM_ADV_STATUS_NONE"); break;
		case M_COMM_ADV_STATUS_READY: ________________________________LOG_drvFds_oldDebShow(1,"M_COMM_ADV_STATUS_READY"); break;
		case M_COMM_ADV_WAITE_GAP_DISCONNECT: ________________________________LOG_drvFds_oldDebShow(1,"M_COMM_ADV_WAITE_GAP_DISCONNECT"); break;
		case M_COMM_ADV_STATUS_IN_ADV: ________________________________LOG_drvFds_oldDebShow(1,"M_COMM_ADV_STATUS_IN_ADV"); break;
		case M_COMM_ADV_STATUS_CONNECTED: ________________________________LOG_drvFds_oldDebShow(1,"M_COMM_ADV_STATUS_CONNECTED"); break;
		case M_COMM_ADV_STATUS_STOP: ________________________________LOG_drvFds_oldDebShow(1,"M_COMM_ADV_STATUS_STOP"); break;
		default: ________________________________LOG_drvFds_oldDebShow(1,"UNKNOWN");
	}	
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			debshow_comm_adv_channel
//-----------------------------------------------------------------------------------------------------------------------------------
void debshow_comm_adv_channel(const char *msg, m_comm_adv_channel_type_t comm_adv_channel)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	________________________________LOG_drvFds_oldDebShow(1,"COMM_ADV_CHANNEL:(%s:) ",msg);
	switch(comm_adv_channel)
	{
		case M_COMM_ADV_CHANNEL_BT1: ________________________________LOG_drvFds_oldDebShow(1,"M_COMM_ADV_CHANNEL_BT1"); break;
		case M_COMM_ADV_CHANNEL_BT2: ________________________________LOG_drvFds_oldDebShow(1,"M_COMM_ADV_CHANNEL_BT2"); break;
		case M_COMM_ADV_CHANNEL_BT3: ________________________________LOG_drvFds_oldDebShow(1,"M_COMM_ADV_CHANNEL_BT3"); break;
//jaehong		case M_COMM_ADV_CHANNEL_DONGLE: ________________________________LOG_drvFds_oldDebShow(1,"M_COMM_ADV_CHANNEL_DONGLE"); break;
		default: ________________________________LOG_drvFds_oldDebShow(1,"UNKNOWN");
	}	
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			debshow_passkey
//-----------------------------------------------------------------------------------------------------------------------------------
void debshow_passkey(const char *msg, passkey_t passkey)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	________________________________LOG_drvFds_oldDebShow(1,"PASSKEY:(%s:) ",msg);
	________________________________LOG_drvFds_oldDebShow(1," is_ready=%d, is_enter=%d, index=%d, keys[]=",
		passkey.is_ready,
		passkey.is_enter,
		passkey.index
	);
	for(int i=0; i < BLE_GAP_PASSKEY_LEN; i++)
	{
		________________________________LOG_drvFds_oldDebShow(1,"%c",passkey.keys[i]);
	}
	
}


//-----------------------------------------------------------------------------------------------------------------------------------
//			hid_to_ascii
//-----------------------------------------------------------------------------------------------------------------------------------
uint8_t hid_to_ascii(uint8_t hid)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	switch(hid)
	{
		case APP_USBD_HID_KBD_1: return 0x31;
		case APP_USBD_HID_KBD_2: return 0x32;
		case APP_USBD_HID_KBD_3: return 0x33;
		case APP_USBD_HID_KBD_4: return 0x34;
		case APP_USBD_HID_KBD_5: return 0x35;
		case APP_USBD_HID_KBD_6: return 0x36;
		case APP_USBD_HID_KBD_7: return 0x37;
		case APP_USBD_HID_KBD_8: return 0x38;
		case APP_USBD_HID_KBD_9: return 0x39;
		case APP_USBD_HID_KBD_0: return 0x30;
	}
	return 0;
}




//-----------------------------------------------------------------------------------------------------------------------------------
//			debshow_mokibo_manufacture_info
//-----------------------------------------------------------------------------------------------------------------------------------
void debshow_mokibo_manufacture_info(const char *msg,m_mokibo_manufacture_info_t factory_data)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	________________________________LOG_drvFds_oldDebShow(1,"MOKIBO MANUFACTURE INFO:(%s:) ",msg);
	________________________________LOG_drvFds_oldDebShow(1,"\t serial= 0x%x(%d)",factory_data.serial,factory_data.serial);
	________________________________LOG_drvFds_oldDebShow(1,"\t date= 0x%x(%d)",factory_data.date,factory_data.date);
	
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			debshow_mokibo_test_info
//-----------------------------------------------------------------------------------------------------------------------------------
void debshow_mokibo_test_info(const char *msg,m_mokibo_test_info_t test_data)
{
	________________________________LOG_drvFds_FuncHeads(FUNCHEAD_FLAG,"");
	________________________________LOG_drvFds_oldDebShow(1,"MOKIBO TEST INFO:(%s:) ",msg);
	________________________________LOG_drvFds_oldDebShow(1,"\t flag=%d",test_data.flag);
	for(int i=0; i<3; i++){
		________________________________LOG_drvFds_oldDebShow(1,"argv[%d]=0x%x", i, test_data.argv[i]);
	}
	
}







/*\   $$\ $$\   $$\ $$\   $$\  $$$$$$\  $$$$$$$$\ $$$$$$$\                   
$$ |  $$ |$$$\  $$ |$$ |  $$ |$$  __$$\ $$  _____|$$  __$$\                  
$$ |  $$ |$$$$\ $$ |$$ |  $$ |$$ /  \__|$$ |      $$ |  $$ |                 
$$ |  $$ |$$ $$\$$ |$$ |  $$ |\$$$$$$\  $$$$$\    $$ |  $$ |                 
$$ |  $$ |$$ \$$$$ |$$ |  $$ | \____$$\ $$  __|   $$ |  $$ |                 
$$ |  $$ |$$ |\$$$ |$$ |  $$ |$$\   $$ |$$ |      $$ |  $$ |                 
\$$$$$$  |$$ | \$$ |\$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$  |                 
 \______/ \__|  \__| \______/  \______/ \________|\_______/                  
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\ 
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__|
//============================================================================//
//                                 UNUSED FUNCTIONS                           //
//============================================================================*/






#if 0 // BLOCK_MJ_FDS by Stanley
/*
================================================================================
   Function name : drv_fds_config_read()
================================================================================
*/
ret_code_t drv_fds_read(drv_fds_page_t page, uint8_t *pBuf, uint8_t size)
{
	ret_code_t 			err_code;
	fds_find_token_t    ftok;
	fds_record_desc_t   record_desc;
	fds_flash_record_t  flash_record;	// flash에 
	uint8_t *           p_addr;
	//
	memset(&ftok, 0x00, sizeof(fds_find_token_t));

	// Try to find FDS record with node settings.
	// if record is found, use record_desc!!
	err_code = fds_record_find(FDS_FILE_ID, FDS_REC_KEY, &record_desc, &ftok);
	// Loop until all records with the given key and file ID have been found.
	if(FDS_SUCCESS == err_code)
	{

		// record_desc 정보를 가지고 flash 위치를 가져욤
		err_code = fds_record_open(&record_desc, &flash_record);
		if ( err_code != FDS_SUCCESS)
		{
			#ifdef DRV_FDS_READ_TRACE
				DEB("fds_record_open() failed\n");
			#endif
			return err_code;		
		}

		// flash 정보에 있는 버퍼 어드레스 위치
		if(flash_record.p_data)
		{
			// m_buffer로 복사
			memcpy(m_bufffer, flash_record.p_data, sizeof(m_bufffer));
		}

		p_addr = (uint8_t *)m_bufffer;

		if(page>=drv_fds_max_page)
		{
			#ifdef DRV_FDS_READ_TRACE
				DEB(" page out of range: %d >= max %d \n",page, drv_fds_max_page);
			#endif
			return NRF_ERROR_INVALID_PARAM;		
		}

		if(size>m_pages_info[page].size)
		{
			#ifdef DRV_FDS_READ_TRACE
				DEB(" size out of range: %d >= max %d \n",size, m_pages_info[page].size);
			#endif
			return NRF_ERROR_INVALID_PARAM;		
		}

		memcpy(pBuf, p_addr+m_pages_info[page].addr, size);

		// Access the record through the flash_record structure.
		// Close the record when done.
		err_code = fds_record_close(&record_desc);

		if (err_code != FDS_SUCCESS)
		{
			#ifdef DRV_FDS_READ_TRACE
				DEB(" fds_record_close() failed\n");
			#endif
			return err_code;	
		}
	}
	else
	{
		#ifdef DRV_FDS_READ_TRACE
			DEB(" fds_record_find() failed\n");
		#endif
	}

	return err_code;
}

/*
================================================================================
   Function name : drv_fds_config_write()
================================================================================
*/
ret_code_t drv_fds_write(drv_fds_page_t page, const uint8_t *pBuf, uint8_t size)
{
	//-------------------------------------------------------------------------------------------------
	ret_code_t				err_code;
    fds_find_token_t		ftok;
	fds_record_t			record;
    fds_record_desc_t		record_desc;
	uint8_t *           	p_addr;

	//-------------------------------------------------------------------------------------------------
	memset(&ftok, 0x00, sizeof(fds_find_token_t));
	memset(&record, 0x00, sizeof(fds_record_t));

    record.file_id           = FDS_FILE_ID;
    record.key               = FDS_REC_KEY;
    record.data.p_data       = m_bufffer;										 // =
    record.data.length_words = ALIGN_NUM(4, sizeof(m_bufffer))/sizeof(uint32_t); // = 0x10

	// FDS_FILE_ID, FDS_REC_KEY 값을 가지고 record_desc 가져온다
	err_code = fds_record_find(FDS_FILE_ID, FDS_REC_KEY, &record_desc, &ftok);
    if(FDS_SUCCESS == err_code)
    {
		p_addr = (uint8_t *)m_bufffer;

		if(page >= drv_fds_max_page)
		{
			return NRF_ERROR_INVALID_PARAM;		
		}

		if(size > m_pages_info[page].size)
		{
			return NRF_ERROR_INVALID_PARAM;		
		}
		//======================================================================
		memcpy(p_addr+m_pages_info[page].addr, pBuf, size);

		//=====================================================================

		//err_code = m_timer_stop();
		//PRINT_IF_ERROR("m_timer_stop()", err_code);

		// 전체 m_bufffer 데이터를 flash 업데트(write) 함
		err_code = fds_record_update(&record_desc, &record);

		//err_code = m_timer_start();
		//PRINT_IF_ERROR("m_timer_start()", err_code);
		if(FDS_SUCCESS == err_code)
		{
			#if	defined(DRV_FDS_TRACE)
        	DEBUG("fds_record_update[%d] is updated!!", record_desc.record_id);
			#endif
		}
		else
		{
        	DEBUG("fds_record_update() fail[%d]", err_code);
		}
    }
    else
    {
		//  새로운 데이터가 없음 초기값을 적음

        err_code = fds_record_write(&record_desc, &record);
		APP_ERROR_CHECK(err_code);
		if(FDS_SUCCESS == err_code)
		{
			//#if	defined(DRV_FDS_TRACE)
        	DEBUG("$$$$$$  fds_record_write --> SUCCESS   $$$$$$");
        	DEBUG("New Record ID(%d) created!!", record_desc.record_id);
			return FDS_ERR_NOT_FOUND; // for rewrite config value !!!
			//#endif
		}
		else
		{
        	DEBUG("@@@@@@@@ fds_record_write --> Failed @@@@@");
        	DEBUG("fds_record_write() fail[%d]", err_code);
		}
    }

    if (err_code == FDS_ERR_NO_SPACE_IN_FLASH)
    {
        // Run garbage collector to reclaim the flash space that is occupied by records that have been deleted,
        // or that failed to be completely written due to, for example, a power loss.
       	DEBUG("err_code == FDS_ERR_NO_SPACE_IN_FLASH ==> fds_gc()", err_code);
        err_code = fds_gc();
		if(FDS_SUCCESS != err_code)
		{
       		DEBUG("fds_gc() fail[%d]", err_code);
		}
    }
	//
	return err_code;
}
#endif // BLOCK_MJ_FDS by Stanley



#if 0 //////////////////////////////// delete dongle /////////////////////////
//-----------------------------------------------------------------------------------------------------------------------------------
//			CallMeRepeatedlyToCallDongleFunc
//-----------------------------------------------------------------------------------------------------------------------------------
void CallMeRepeatedlyToCallDongleFunc(uint32_t every_call_ms)
{
	static uint32_t elapsed_ms=0;

	// do nothing if there is no request.
	if(m_call_dongle_after_this_ms <= 0) return;
	
	// if there was any request...
	elapsed_ms += every_call_ms;

	//
	if( elapsed_ms < m_call_dongle_after_this_ms ){
		// do not call iff the required time is not elapsed yet
		return;
	}
	// (1) 'm_call_dongle_after_this_ms'후에 호출달라는 요청이 있었고,
	// (2) 시간이(elapsed_time)이 이(m_call_dongle_after_this_ms)를 경과하였으므로.... 호춯한다.
	//DEB("\n ...Change_to_DONGLE(M_COMM_CHANNEL_DONGLE)...\n");
	Change_to_DONGLE(M_COMM_CHANNEL_DONGLE);
	
	// 요청을 처리했으므로...
	elapsed_ms= 0;				// 다음 counting을 위하여 초기화
	m_call_dongle_after_this_ms=0;		// no request
}
#endif ///////////////////////////////////////////// delete dongle //////////////////////



#if 0 //////////////////////////////// delete dongle /////////////////////////
//-----------------------------------------------------------------------------------------------------------------------------------
//			CallMeRepeatedlyToCallBTFunc
//-----------------------------------------------------------------------------------------------------------------------------------
void CallMeRepeatedlyToCallBTFunc(uint32_t every_call_ms)
{
	static uint32_t elapsed_ms=0;

	// do nothing if there is no request.
	if(m_call_bt_after_this_ms <= 0) return;
	
	// if there was any request...
	elapsed_ms += every_call_ms;

	//
	if( elapsed_ms < m_call_bt_after_this_ms ){
		// do not call iff the required time is not elapsed yet
		return;
	}
	// (1) 'm_call_bt_after_this_ms'후에 호출달라는 요청이 있었고,
	// (2) 시간이(elapsed_time)이 이(m_call_bt_after_this_ms)를 경과하였으므로.... 호춯한다.
	//DEB("\n ...Change_to_DONGLE(M_COMM_CHANNEL_DONGLE)...\n");
	Change_DONGLE_to_BT_before(M_COMM_CHANNEL_DONGLE, m_bt_ch);
	Change_DONGLE_to_BT_after(M_COMM_CHANNEL_DONGLE, m_bt_ch);
	
	// 요청을 처리했으므로...
	elapsed_ms= 0;				// 다음 counting을 위하여 초기화
	m_call_bt_after_this_ms=0;		// no request
}
#endif //////////////////////////////////// delete dongle /////////////////////////










#if 0 // BLOCK_MJ_FDS by Stanley
/*
================================================================================
   Function name : drv_fds_clear_all()
================================================================================
*/
ret_code_t drv_fds_clear_all(void)
{
	ret_code_t			err_code;
    fds_find_token_t   ftok;
	fds_record_desc_t  record_desc;  

	memset(&ftok, 0x00, sizeof(fds_find_token_t));
    DEBUG(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

	// Try to find FDS record with node settings.
	// if record is found, use record_desc!!
	err_code = fds_record_find(FDS_FILE_ID, FDS_REC_KEY, &record_desc, &ftok);

	// Loop until all records with the given key and file ID have been found.
	if( FDS_SUCCESS == err_code)
	{
       	DEBUG("fds_record_delete");
		err_code = fds_record_delete(&record_desc);
	}
	
	return err_code;
}

/*
================================================================================
   Function name : drv_fds_save_immediately()
================================================================================
*/
ret_code_t drv_fds_save_immediately(void)
{	
	
//########################################################################	
			//Stanley's
			return FDS_SUCCESS;
//########################################################################	

    ret_code_t err_code = fds_gc();

  	if (err_code == FDS_ERR_BUSY || err_code == FDS_ERR_NO_SPACE_IN_QUEUES)
    {
        // Retry.
		DEBUG("OOPS[%d]", err_code);
    	err_code = fds_gc();
    }

	return err_code;
}
#endif // BLOCK_MJ_FDS by Stanley


















