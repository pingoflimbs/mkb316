/// \file drv_cypress.c
/// This file contains all required configurations for mokibo
/*==============================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
==============================================================================*/




/*\   $$\ $$$$$$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$$$$$$\ $$\     $$\          
$$ |  $$ |\_$$  _|$$  __$$\\__$$  __|$$  __$$\ $$  __$$\\$$\   $$  |         
$$ |  $$ |  $$ |  $$ /  \__|  $$ |   $$ /  $$ |$$ |  $$ |\$$\ $$  /          
$$$$$$$$ |  $$ |  \$$$$$$\    $$ |   $$ |  $$ |$$$$$$$  | \$$$$  /           
$$  __$$ |  $$ |   \____$$\   $$ |   $$ |  $$ |$$  __$$<   \$$  /            
$$ |  $$ |  $$ |  $$\   $$ |  $$ |   $$ |  $$ |$$ |  $$ |   $$ |             
$$ |  $$ |$$$$$$\ \$$$$$$  |  $$ |    $$$$$$  |$$ |  $$ |   $$ |             
\__|  \__|\______| \______/   \__|    \______/ \__|  \__|   \__|             
//============================================================================//
//                                 REVISION HISTORY                           //
//============================================================================//
20181018_MJ
	Created initial version based on JH.Seo work

20171228_MJ
	Added cypress_get_fw_version() 

20181109_MJ
	Moved cypress_get_system_infor into local function

20180402_MJ
	Created drv_cypress_init_Baseline()

20180625_MJ
	Created drv_cypress_req_deep_sleep()
	Created drv_cypress_req_wake_up()
	Created drv_cypress_req_device_reset()

20180605_MJ
	Added correct() for linearity
	Refer to "R.MOK.FW.18.Mutl-touch gesture design Rev1.7" correction sheet

20180828_Stanley
	Correction is modified: see 'OLD_VERSION_TOUCH_CORRECTION_BELOW_0_9_5'
	Active area x-coordinate thresholds are added: 
		ACTIVE_AREA_LEFT_THRESHOLD, 
		ACTIVE_AREA_RIGHT_THRESHOLD

==============================================================================*/




/*$$$$$\  $$\    $$\ $$$$$$$$\ $$$$$$$\  $$\    $$\ $$$$$$\ $$$$$$$$\ $$\      $$\ 
$$  __$$\ $$ |   $$ |$$  _____|$$  __$$\ $$ |   $$ |\_$$  _|$$  _____|$$ | $\  $$ |
$$ /  $$ |$$ |   $$ |$$ |      $$ |  $$ |$$ |   $$ |  $$ |  $$ |      $$ |$$$\ $$ |
$$ |  $$ |\$$\  $$  |$$$$$\    $$$$$$$  |\$$\  $$  |  $$ |  $$$$$\    $$ $$ $$\$$ |
$$ |  $$ | \$$\$$  / $$  __|   $$  __$$<  \$$\$$  /   $$ |  $$  __|   $$$$  _$$$$ |
$$ |  $$ |  \$$$  /  $$ |      $$ |  $$ |  \$$$  /    $$ |  $$ |      $$$  / \$$$ |
 $$$$$$  |   \$  /   $$$$$$$$\ $$ |  $$ |   \$  /   $$$$$$\ $$$$$$$$\ $$  /   \$$ |
 \______/     \_/    \________|\__|  \__|    \_/    \______|\________|\__/     \__|
//============================================================================//
//                        FUNCTION CALLSTACK OVERVIEW                         //
//============================================================================//

//----------------------------------------------------//TIMERS
//----------------------------------------------------//VARS
//----------------------------------------------------//FUNCS


//----------------------------------------------------//INIT
static ret_code_t cypress_init(void)
{
	ret_code_t rv;
	
	nrf_gpio_pin_dir_set(CFG_MOKIBO_TOUCH_PWR_PIN, NRF_GPIO_PIN_DIR_OUTPUT);
	nrf_gpio_pin_write(CFG_MOKIBO_TOUCH_PWR_PIN, 1);

	rv = cypress_Check_Reset_Sentinel();
	
    if (NRF_SUCCESS != rv)
    {
        DEBUG("Error! RESET Sentinel Failed[%d]", rv);
		INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30
		return NRF_ERROR_INTERNAL;
    }
	else 
	{
		#if	defined(DRV_CYPRESS_CMD_TRACE)
		DEBUG("RESET Sentinel read OK, buf[0: 0x%x, 1:0x%x]", rdbuff[0], rdbuff[1]);
		#endif
	}

    // if running Application, then start bootloader
    if (cypress_Get_HID_Descriptor() == DRV_CYPRESS_MODE_BOOTLOADER)
    {
		DEBUG("Running Bootloader! go to start Application");

		rv = cypress_LaunchApp();

        if (NRF_SUCCESS != rv)
        {
            DEBUG("TT Application Start  Failed!![%d]", rv);
			INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30
			return NRF_ERROR_INTERNAL;
        }
		
		#if	defined(DRV_CYPRESS_CMD_TRACE)
		DEBUG("TT Application Start OK !!");
		#endif
    }
	else
	{
		#if	defined(DRV_CYPRESS_CMD_TRACE)
		DEBUG("");
		#endif
	}
	
	
	
	//cypress_get_system_infor();

	return NRF_SUCCESS;
}
#endif






//----------------------------------------------------//LOOP
ret_code_t drv_cypress_read(drv_cypress_info_t *pInfo)
{
	uint16_t correction;
	uint32_t i, valid_touch;
	uint8_t buf[256] = {0, };

	drv_cypress_len_t		*pLen;
	drv_cypress_header_t	*pHeader;
	drv_cypress_packet_t	*pPacket;
	//
	drv_touch_finger_info_t	 *pInfo_finger;
	drv_cypress_touch_info_t *pPacketInfo;

	drv_twi_read(CYPRESS_ADDR, 0x0, buf, DRV_CYPRESS_DATA_REQ_LEN)	
		
	pLen = (drv_cypress_len_t *)buf;
	
	if(pLen->len < DRV_CYPRESS_1_DATA_LEN)
	{		
		trace_cypress_no_touch();
		return NRF_ERROR_NOT_FOUND;
	}
	
	if( ( pLen->len > DRV_CYPRESS_DATA_REQ_LEN) ) 
	{ 
		drv_cypress_touch_reset(__func__, __LINE__);
		return NRF_ERROR_INVALID_LENGTH;
	}
	
	pHeader = (drv_cypress_header_t *)buf;
		
	pHeader->touch_count &= 0x1F;

	if (pHeader->touch_count > DRV_CYPRESS_MAX_COUNT) 
	{
		drv_cypress_touch_reset(__func__, __LINE__);
		return NRF_ERROR_INTERNAL;
	}
	
	pPacket = (drv_cypress_packet_t *)buf;
		
	switch (pHeader->touch_count) 
	{
		case 1:
		case 2:
		case 3:
		case 4:		
		case 5:
		case 6:
		case 7:
		case 8:
			for(i = 0, valid_touch = 0; i < pHeader->touch_count; i++) 
			{								
				correction = correct(pPacket->body.info[i].y);

				if(pPacket->body.info[i].x > correction)
				{
					pPacket->body.info[i].x -= correction;
				}
								
				check_valid_range(pPacket->body.info[i].x, pPacket->body.info[i].y)) 
				{
					continue; 
				}	
				pInfo_finger= &(pInfo->info[valid_touch]);
				pPacketInfo  = &(pPacket->body.info[i]);

				if(pPacketInfo->eventID==2){
					pInfo_finger->touchType 	= pPacketInfo->touchType;
					pInfo_finger->touchID 		= valid_touch;	// <--> pPacketInfo->touchID;
					pInfo_finger->eventID 		= pPacketInfo->eventID;
					pInfo_finger->tip 			= pPacketInfo->tip;
					pInfo_finger->x 			= pPacketInfo->x; // = abs(4000 - pPacketInfo->x);
					pInfo_finger->y 			= pPacketInfo->y;
					pInfo_finger->presure 		= pPacketInfo->pressure;
					pInfo_finger->major_axis 	= pPacketInfo->major_axis;
					pInfo_finger->minor_axis 	= pPacketInfo->minor_axis;
					pInfo_finger->orient 		= pPacketInfo->orientation;								
					valid_touch++;
				}				
			}			
			break;
		default:
			
		return NRF_ERROR_NOT_SUPPORTED;
	}

	if(valid_touch)
	{
		//nrf_gpio_pin_write(CFG_MOKIBO_TOUCH_DEBUG_PIN, 1);
		//
		pInfo->touchCnt = valid_touch;
		pInfo->timestamp = pHeader->timestamp;
		return NRF_SUCCESS;
	}
	else
	{		
		return NRF_ERROR_NOT_FOUND;
	}
}


//----------------------------------------------------------------------------*/








/*\      $$\  $$$$$$\  $$$$$$$\  $$\   $$\ $$\       $$$$$$$$\  $$$$$$\  
$$$\    $$$ |$$  __$$\ $$  __$$\ $$ |  $$ |$$ |      $$  _____|$$  __$$\ 
$$$$\  $$$$ |$$ /  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$ /  \__|
$$\$$\$$ $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$$$$\    \$$$$$$\  
$$ \$$$  $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$  __|    \____$$\ 
$$ |\$  /$$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$\   $$ |
$$ | \_/ $$ | $$$$$$  |$$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$$\ \$$$$$$  |
\__|     \__| \______/ \_______/  \______/ \________|\________| \______/ 
//============================================================================//
//                                 MODULES USED                               //
//============================================================================*/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "sdk_config.h" // mokibo_config.h && sdk_config.h
#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
	// should be placed after "sdk_config.h"
	#include "SEGGER_RTT.h"
	#include "m_timer.h"
#endif
#include "nrf_error.h"
#include "nrf_delay.h"
#include "nrf_gpio.h"
#include "nrf_log.h"
#include "drv_twi.h"
#include "drv_cypress.h"

#if CYPRESS_FW_INCLUDED
	#include "drv_cypress_fw.h"
#endif

#include "drv_fds.h"

#include "m_mokibo.h"
#include "m_flimbs.h"





 /*$$$$\   $$$$$$\  $$\   $$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$\   $$\ $$$$$$$$\ 
$$  __$$\ $$  __$$\ $$$\  $$ |$$  __$$\\__$$  __|$$  __$$\ $$$\  $$ |\__$$  __|
$$ /  \__|$$ /  $$ |$$$$\ $$ |$$ /  \__|  $$ |   $$ /  $$ |$$$$\ $$ |   $$ |   
$$ |      $$ |  $$ |$$ $$\$$ |\$$$$$$\    $$ |   $$$$$$$$ |$$ $$\$$ |   $$ |   
$$ |      $$ |  $$ |$$ \$$$$ | \____$$\   $$ |   $$  __$$ |$$ \$$$$ |   $$ |   
$$ |  $$\ $$ |  $$ |$$ |\$$$ |$$\   $$ |  $$ |   $$ |  $$ |$$ |\$$$ |   $$ |   
\$$$$$$  | $$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$ |  $$ |$$ | \$$ |   $$ |   
 \______/  \______/ \__|  \__| \______/   \__|   \__|  \__|\__|  \__|   \__|   
//============================================================================//
//                                LOCAL CONSTANTS                             //
//============================================================================*/






/*$$$$$$\ $$\     $$\ $$$$$$$\  $$$$$$$$\ $$$$$$$\  $$$$$$$$\ $$$$$$$$\  $$$$$$\  
\__$$  __|\$$\   $$  |$$  __$$\ $$  _____|$$  __$$\ $$  _____|$$  _____|$$  __$$\ 
   $$ |    \$$\ $$  / $$ |  $$ |$$ |      $$ |  $$ |$$ |      $$ |      $$ /  \__|
   $$ |     \$$$$  /  $$$$$$$  |$$$$$\    $$ |  $$ |$$$$$\    $$$$$\    \$$$$$$\  
   $$ |      \$$  /   $$  ____/ $$  __|   $$ |  $$ |$$  __|   $$  __|    \____$$\ 
   $$ |       $$ |    $$ |      $$ |      $$ |  $$ |$$ |      $$ |      $$\   $$ |
   $$ |       $$ |    $$ |      $$$$$$$$\ $$$$$$$  |$$$$$$$$\ $$ |      \$$$$$$  |
   \__|       \__|    \__|      \________|\_______/ \________|\__|       \______/ 
//============================================================================//
//                                LOCAL TYPEDEFS                              //
//============================================================================*/
typedef struct {		
	uint16_t 	y;
	uint16_t  	correction;
} correction_t;




/*\    $$\  $$$$$$\  $$$$$$$\  $$$$$$\  $$$$$$\  $$$$$$$\  $$\       $$$$$$$$\ 
$$ |   $$ |$$  __$$\ $$  __$$\ \_$$  _|$$  __$$\ $$  __$$\ $$ |      $$  _____|
$$ |   $$ |$$ /  $$ |$$ |  $$ |  $$ |  $$ /  $$ |$$ |  $$ |$$ |      $$ |      
\$$\  $$  |$$$$$$$$ |$$$$$$$  |  $$ |  $$$$$$$$ |$$$$$$$\ |$$ |      $$$$$\    
 \$$\$$  / $$  __$$ |$$  __$$<   $$ |  $$  __$$ |$$  __$$\ $$ |      $$  __|   
  \$$$  /  $$ |  $$ |$$ |  $$ |  $$ |  $$ |  $$ |$$ |  $$ |$$ |      $$ |      
   \$  /   $$ |  $$ |$$ |  $$ |$$$$$$\ $$ |  $$ |$$$$$$$  |$$$$$$$$\ $$$$$$$$\ 
    \_/    \__|  \__|\__|  \__|\______|\__|  \__|\_______/ \________|\_______/
//============================================================================//
//                                   VARIABLES                                //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                                GLOBAL VARIABLES                            //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                                 LOCAL VARIABLES                            //
//----------------------------------------------------------------------------*/
static uint32_t m_img_fw_ver;

#if(AREA_PROPERTIE == AREA_PROPERTIE_ALWAYSFORCE_TO_WHOLE_AREA)
	//
#elif (AREA_PROPERTIE == AREA_PROPERTIE_ORIGINAL)
	static DRV_CYPRESS_ACTIVE_AREA_T m_active_area;
#elif (AREA_PROPERTIE == AREA_PROPERTIE_FLiMBS_20190121_MOD_TOUCH_WHOLE_AREA_WITH_FILTER_KEYS)

#endif

static uint8_t rdbuff[DRV_CYPRESS_BUFFER_SIZE];
static uint8_t wrbuff[DRV_CYPRESS_BUFFER_SIZE];

#if CYPRESS_FW_INCLUDED
static uint8_t Security_Key[8] = {
	0xA5, 0x01, 0x02, 0x03, 0xFF, 0xFE, 0xFD, 0x5A
};

static uint16_t crc_table[16] = {
	0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
	0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
};
#endif

#ifdef OLD_VERSION_TOUCH_CORRECTION_BELOW_0_9_5
#define	LOWER_LIMIT_Y	1051
#define	UPPER_LIMIT_Y	1226
const correction_t m_correction_table[] = { \
	{1226,	68},
	{1221,	66},
	{1216,	64},
	{1211,	62},
	{1206,	60},
	{1201,	58},
	{1196,	56},
	{1191,	54},
	{1186,	52},
	{1181,	50},
	{1176,	48},
	{1171,	46},
	{1166,	44},
	{1161,	42},
	{1156,	40},
	{1151,	38},
	{1146,	36},
	{1141,	34},
	{1136,	33},
	{1131,	31},
	{1126,	29},
	{1121,	27},
	{1116,	25},
	{1111,	23},
	{1106,	21},
	{1101,	19},
	{1096,	17},
	{1091,	15},
	{1086,	13},
	{1081,	11},
	{1076,	9},
	{1071,	7},
	{1066,	5},
	{1061,	3},
	{1056,	1},
	{1051,	0},
};
#else

#define	LOWER_LIMIT_Y	435//492
#define	UPPER_LIMIT_Y	601//582
const correction_t m_correction_table[] = { \
	{601,43},
	{597,42},
	{592,41},	
	{587,40},
	{583,39},
	{578,38},
	{574,37},
	{571,36},
	{570,35},
	{566,33},
	{562,32},
	{558,30},	
	{555,29},
	{552,28},
	{548,26},
	{544,24},	
	{540,22},
	{536,21},
	{532,19},
	{528,17},	
	{525,15},
	{523,14},
	{520,13},
	{517,12},
	{514,11},
	{512,10},
	{506,9},	
	{501,8},
	{495,7},
	{488,6},
	{474,4},
	{461,4},
	{453,3},
	{447,2},
	{440,1},
	{435,0},
};

#endif
/*----------------------------------------------------------------------------//
//                                 DEBUG VARIABLES                            //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                                UNUSED VARIABLES                            //
//----------------------------------------------------------------------------*/










/*$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\  $$\   $$\ $$$$$$$$\  $$$$$$\  $$$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\ $$ |  $$ |$$  _____|$$  __$$\ $$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|$$ |  $$ |$$ |      $$ /  $$ |$$ |  $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |      $$$$$$$$ |$$$$$\    $$$$$$$$ |$$ |  $$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |      $$  __$$ |$$  __|   $$  __$$ |$$ |  $$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\ $$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |$$ |  $$ |$$$$$$$$\ $$ |  $$ |$$$$$$$  |
\__|       \______/ \__|  \__| \______/ \__|  \__|\________|\__|  \__|\_______/ 
//============================================================================//
//                                   FUNCHEAD                                 //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                            GLOBAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
void drv_cypress_init(void);
void drv_cypress_re_init(void);
ret_code_t drv_cypress_read(drv_cypress_info_t *pInfo);
void drv_cypress_touch_reset(const char *caller, int caller_num);
void drv_cypress_touch_reset_fast(const char *caller, int caller_num);
void drv_cypress_mode_set(DRV_CYPRESS_ACTIVE_AREA_T new_area);
uint32_t drv_cypress_get_fw_version(void);
void drv_cypress_init_Baseline(void);
void drv_cypress_req_deep_sleep(uint8_t wake_of_sleep);
void drv_cypress_req_wake_up(void);
void drv_cypress_req_device_reset(void);
void drv_cypress_req_calibration(uint8_t senseMode);
void drv_cypress_req_suspend_scanning(void);
void drv_cypress_req_resume_scanning(void);
void drv_cypress_touch_power_down(void);
ret_code_t cypress_read_TT_INT(void);
void drv_cypress_power_on(void);
void drv_cypress_power_off(void);
void drv_cypress_ResetPin_Low(void);
void drv_cypress_ResetPin_High(void);
void drv_cypress_ResetPin_Low_Wait_High(void);

/*----------------------------------------------------------------------------//
//                             LOCAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
static ret_code_t cypress_init(void);

static ret_code_t is_TT_INT_low(uint32_t timeout_ms);
static ret_code_t is_TT_INT_high(uint32_t timeout_ms);
static int32_t cypress_Get_HID_Descriptor(void);
static ret_code_t check_valid_range(uint16_t x, uint16_t y);

static void show_wrbuff(uint8_t *wrbuff, uint16_t cnt, char *msg);
static ret_code_t cypress_Check_Reset_Sentinel(void);
static ret_code_t cypress_LaunchApp(void);
static uint16_t correct(const uint16_t y);



/*----------------------------------------------------------------------------//
//                            POINTER FUNCTION HEADS                          //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                             DEBUG FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                             UNUSED FUNCTION HEADS                          //
//----------------------------------------------------------------------------*/
//static ret_code_t cypress_Check_Interrupt_Routine(uint8_t checkpin, uint32_t us);
#if CYPRESS_FW_INCLUDED
static ret_code_t cypress_EnterBtld(void);
static ret_code_t cypress_Start_Bootloader(void);
static ret_code_t cypress_update(void);
static ret_code_t cypress_GetBootloaderInfo(void);
static int cypress_BootLoader_INIT(uint16_t row_size, uint8_t* metadata_row_buf);
static int cypress_Prog_Row(struct cyttsp5_hex_image *row_image);
static int cypress_Verify_Checksum(void);
static uint8_t *cypress_Get_Row(uint8_t *row_buf, uint8_t *image_buf, uint16_t size);
static int cypress_Parse_Row(uint8_t *row_buf, struct cyttsp5_hex_image *row_image);
static uint16_t cypress_Compute_CRC(uint8_t *buf, uint16_t size);
#endif










/*\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ 
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\_*/



/*
 $$$$$$\  $$\       $$$$$$\  $$$$$$$\   $$$$$$\  $$\                                   
$$  __$$\ $$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                                  
$$ /  \__|$$ |     $$ /  $$ |$$ |  $$ |$$ /  $$ |$$ |                                  
$$ |$$$$\ $$ |     $$ |  $$ |$$$$$$$\ |$$$$$$$$ |$$ |                                  
$$ |\_$$ |$$ |     $$ |  $$ |$$  __$$\ $$  __$$ |$$ |                                  
$$ |  $$ |$$ |     $$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |                                  
\$$$$$$  |$$$$$$$$\ $$$$$$  |$$$$$$$  |$$ |  $$ |$$$$$$$$\                             
 \______/ \________|\______/ \_______/ \__|  \__|\________|                            
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\  $$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |$$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |$$ /  \__|
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |\$$$$$$\  
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ | \____$$\ 
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |$$\   $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |\$$$$$$  |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__| \______/ 
//============================================================================//
//                                 GLOBAL FUNCTIONS                           //
//============================================================================*/
/*
================================================================================
   Function name : drv_cypress_init()
================================================================================
*/
void drv_cypress_init(void)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
#if CYPRESS_FW_INCLUDED
	#if	0//(MOKIBO_FDS_ENABLED)
	ret_code_t err_code;
	uint32_t saved_fw_ver;
	
	m_img_fw_ver = (uint8_t)cyttsp4_ver[3];		// Minor
	m_img_fw_ver |= (uint8_t)cyttsp4_ver[2] << 8; 	// Major
	
	err_code = drv_fds_read(DRV_FDS_PAGE_TOUCH_VER, (uint8_t *)&saved_fw_ver, sizeof(saved_fw_ver));
	//err_code = fds_read_stanley(FDS_FILE_ID_STANLEY,FDS_RECORD_KEY_M_TOUCH_VER, sizeof(saved_fw_ver), (uint8_t *)&saved_fw_ver);

	#if defined(DRV_CYPRESS_VER_TRACE)
	DEBUG("CYPRESS FW version: saved[0x%x] img[0x%x]", saved_fw_ver, m_img_fw_ver);
	#endif
	
	if((NRF_SUCCESS == err_code) || (FDS_ERR_NOT_FOUND == err_code)) {

		saved_fw_ver &= 0x00FFFFFF;
	
		if( (0 == saved_fw_ver) || (saved_fw_ver != m_img_fw_ver)) 
		{
			DEBUG("CYPRESS FW version MISMATCH [Saved:0x%x][img:0x%x]", saved_fw_ver, m_img_fw_ver);
			
			#if defined(DRV_CYPRESS_UPG_TRACE)
			DEBUG("MOKIBO CYPRESS Firmware updating...");
			#endif 

			drv_cypress_touch_reset(__func__, __LINE__);

			if(NRF_SUCCESS == cypress_update()) 
			{
				m_img_fw_ver |= 0xFD000000;
				
				#if defined(DRV_CYPRESS_VER_TRACE)
				DEBUG("Saving CYPRESS FW version[0x%x] in flash", m_img_fw_ver);
				#endif
				drv_fds_write(DRV_FDS_PAGE_TOUCH_VER, (uint8_t *)&m_img_fw_ver, sizeof(m_img_fw_ver));
			}
		}
	}
	#else	
	
	ret_code_t err_code;	
	
	m_img_fw_ver = (uint8_t)cyttsp4_ver[3];		// Minor
	m_img_fw_ver |= (uint8_t)cyttsp4_ver[2] << 8; 	// Major		
		

	drv_cypress_touch_reset(__func__, __LINE__);
	err_code = cypress_update();
	if(NRF_SUCCESS == err_code) 
	{
		m_img_fw_ver |= 0xFD000000;				
	}
	

	#endif
#endif

	drv_cypress_power_on();	// add by jaehong, 2019/06/20
	drv_cypress_touch_reset(__func__, __LINE__);

	nrf_delay_ms (100);
	
	if(cypress_init() != NRF_SUCCESS) {
		________________________________LOG_drvCypress_checkCypressUpdate(0,"CYPRESS DRIVER INIT Failed!!");
		INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30	
	}
	else 
	{		
		________________________________LOG_drvCypress_checkCypressUpdate(0,"CYPRESS DRIVER INIT");		
	}
}

/*
================================================================================
   Function name : drv_cypress_re_init()
================================================================================
*/
void drv_cypress_re_init(void)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t rv;

	rv = cypress_Check_Reset_Sentinel();

	// Read Reset Sentinel
    if (NRF_SUCCESS != rv)
    {
        DEBUG("Error! RESET Sentinel Failed[%d]", rv);
		INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30
		return;
    }
	else 
	{
		#if defined(DRV_CYPRESS_CMD_TRACE)
		DEBUG("RESET Sentinel read OK, buf[0: 0x%x, 1:0x%x]", rdbuff[0], rdbuff[1]);
		#endif
	}

	rv = cypress_LaunchApp();

    if (NRF_SUCCESS != rv)
    {
        DEBUG("TT Application Start Failed!![%d]", rv);
		INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30
		return;
    }
		
	#if defined(DRV_CYPRESS_CMD_TRACE)
	DEBUG("TT Application Start OK !!");
	#endif
}




static int8_t leftwriteCorrect(const uint16_t x , const uint16_t y)
{	
	
	


if(x>144)
{
	uint16_t reqX = (x-144) % 139;
	if(435<y && y<601)
	{
		if(1<=reqX&&reqX<=18)
		{
			return 1;
		}
		else if(19<=reqX&&reqX<=71)
		{
			return 0;
		}
		else if(72<=reqX&&reqX<=88)
		{
			return +1;
		}
		else if(84<=reqX&&reqX<=90)
		{
			return 0;
		}
		else if(91<=reqX&&reqX<=107)
		{
			return -1;
		}
		else if(108<=reqX&&reqX<=161)
		{
			return 0;
		}
		else if(162<=reqX&&reqX<=178+1)
		{
			return 1;
		}
		else
		{
			return 0;
		}	
	}
}
return 0;
	
	//if(x - )
}



//-------------------------------------------------------------------------------------------------------------------------
//			drv_cypress_read
//-------------------------------------------------------------------------------------------------------------------------
ret_code_t drv_cypress_read(drv_cypress_info_t *pInfo)//touchCnt, timestamp, drv_touch_finger_info_t
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	uint16_t correction;
	uint32_t i, valid_touch;
	uint8_t buf[256] = {0, };

	drv_cypress_len_t		*pLen; //len
	drv_cypress_header_t	*pHeader; // len, report_id, timestamp, touch_count
	drv_cypress_packet_t	*pPacket; //len, pHeader, body

	drv_touch_finger_info_t	 *pInfo_finger; //touchType touchID eventID tip x y pressure majorAxis minorAxis orient
	drv_cypress_touch_info_t *pPacketInfo; //touchType touchID eventID tip x y majoraxis minorAxis orientation
	
	if (NRF_SUCCESS != drv_twi_read(CYPRESS_ADDR, 0x0, buf, DRV_CYPRESS_DATA_REQ_LEN))
	{		
		//TWI실패
		return NRF_ERROR_INTERNAL;//3
	}
		
	pLen = (drv_cypress_len_t *)buf;
	if( ( pLen->len < DRV_CYPRESS_1_DATA_LEN) ) 
	{
		// 터치를 하지 않아서 터치 정보가 없을때
		return NRF_ERROR_NOT_FOUND;	//5
	}
	
	if( ( pLen->len > DRV_CYPRESS_DATA_REQ_LEN) ) 
	{ 
		//받아온 데이터 길이가 이상할때
		drv_cypress_touch_reset(__func__, __LINE__);
		return NRF_ERROR_INVALID_LENGTH;//9
	}
	
	// dump Received Cypress Packets
	#if	defined(DRV_CYPRESS_DUMP_TRACE)
		drv_cypress_buffer_dump((uint8_t *)&buf, pLen->len);
	#endif 

	// get Header from Cypress Received Packets
	pHeader = (drv_cypress_header_t *)buf;
	
	// mask out Touch Counts (bit4~bit0)
	pHeader->touch_count &= 0x1F;

	// less than 10 touches
	if (pHeader->touch_count > DRV_CYPRESS_MAX_COUNT) 
	{
		//터치 손가락갯수가 10개 넘음
		drv_cypress_touch_reset(__func__, __LINE__);		
		return NRF_ERROR_INTERNAL;//3
	}

	//받아온 패킷 저장
	pPacket = (drv_cypress_packet_t *)buf;
	
	//touch_count 8개까지.(break없음)	
	switch (pHeader->touch_count) 
	{
		case 1:
		case 2:
		case 3:
		case 4:		
		case 5:
		case 6:
		case 7:
		case 8:					
			for(i = 0, valid_touch = 0; i < pHeader->touch_count; i++) 
			{								

				#if 0					
					pPacket->body.info[i].x = pPacket->body.info[i].x + leftwriteCorrect(pPacket->body.info[i].x, pPacket->body.info[i].y);
				#endif

				//qwer열 터치 휜거 보정
				correction = correct(pPacket->body.info[i].y);
				if(pPacket->body.info[i].x > correction)
				{
					pPacket->body.info[i].x -= correction;
				}
			
				//클리핑 영역 벗어났는지
				if(NRF_SUCCESS != check_valid_range(pPacket->body.info[i].x, pPacket->body.info[i].y)) 
				{					
					continue; // 멈추고 다음손가락 체크함
				}
				
				
				pInfo_finger = &(pInfo->info[valid_touch]);//현재 손가락의 터치 데이터 복사
				pPacketInfo  = &(pPacket->body.info[i]);////touchType touchID eventID tip x y majoraxis minorAxis orientation
				if(pPacketInfo->eventID==2)//2번이벤트가 뭔지 모르겠음
				{
					pInfo_finger->touchType 	= pPacketInfo->touchType;
					pInfo_finger->touchID 		= valid_touch;	// <--> pPacketInfo->touchID;
					pInfo_finger->eventID 		= pPacketInfo->eventID;
					pInfo_finger->tip 			= pPacketInfo->tip;
					pInfo_finger->x 			= pPacketInfo->x; // = abs(4000 - pPacketInfo->x);
					pInfo_finger->y 			= pPacketInfo->y;
					pInfo_finger->presure 		= pPacketInfo->pressure;
					pInfo_finger->major_axis 	= pPacketInfo->major_axis;
					pInfo_finger->minor_axis 	= pPacketInfo->minor_axis;
					pInfo_finger->orient 		= pPacketInfo->orientation;
					//----------------------------------------------------------------------------------------------
					// Increse counter
					valid_touch++;
				}
				________________________________LOG_mTouch_touchDbTrace(1,"%d:{%04d,%04d}",i,pPacket->body.info[i].x,pPacket->body.info[i].y);
			}
			
			break;

		default:
			// INFO: do not support more than 4 touch			
		return NRF_ERROR_NOT_SUPPORTED;
	}

	if(valid_touch)
	{		
		pInfo->touchCnt = valid_touch;//손가락 번호
		pInfo->timestamp = pHeader->timestamp;
		return NRF_SUCCESS;//0
	}
	else
	{
		//validTouch 없음 14
		//클리핑된 영역안에 있을때 보통 리턴됨
		//return NRF_ERROR_NOT_FOUND; 
		return NRF_ERROR_NULL;
	}
}


//-------------------------------------------------------------------------------------------------------------------------
//			drv_cypress_touch_reset
//-------------------------------------------------------------------------------------------------------------------------
// Parade Touch Chip의 Reset(active low) 단자를 Low로 떨어뜨렸다가 HIgh로 올림.
void drv_cypress_touch_reset(const char *caller, int caller_num)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	#if	defined(DRV_CYPRESS_RESET_TRACE)
	DEB("drv_cypress_touch_reset()...\n");
	#endif
	//
	nrf_gpio_pin_dir_set(CFG_MOKIBO_TOUCH_INT_PIN, NRF_GPIO_PIN_DIR_INPUT);
	nrf_gpio_pin_dir_set(CFG_MOKIBO_TOUCH_RESET_PIN, NRF_GPIO_PIN_DIR_OUTPUT);

	// HW pull-up
	nrf_gpio_pin_write(CFG_MOKIBO_TOUCH_RESET_PIN, 0);
	nrf_delay_ms (40);

	nrf_gpio_pin_write(CFG_MOKIBO_TOUCH_RESET_PIN, 1);
	nrf_delay_ms (20);

	//
	#if	defined(DRV_CYPRESS_RESET_TRACE)
	DEB("%s():%d by %s():%d", __func__, __LINE__, caller, caller_num);
	#endif

}

//-------------------------------------------------------------------------------------------------------------------------
//			drv_cypress_touch_reset_fast
//-------------------------------------------------------------------------------------------------------------------------
void drv_cypress_touch_reset_fast(const char *caller, int caller_num)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	DEB("drv_cypress_touch_reset_fast()...\n");
	
	//
	nrf_gpio_pin_dir_set(CFG_MOKIBO_TOUCH_INT_PIN, NRF_GPIO_PIN_DIR_INPUT);
	nrf_gpio_pin_dir_set(CFG_MOKIBO_TOUCH_RESET_PIN, NRF_GPIO_PIN_DIR_OUTPUT);

	// TT_RST (Active Low)
	nrf_gpio_pin_write(CFG_MOKIBO_TOUCH_RESET_PIN, 0);
	//nrf_delay_ms(1);
	nrf_delay_us(500);
	nrf_gpio_pin_write(CFG_MOKIBO_TOUCH_RESET_PIN, 1);
	//
	
	//nrf_gpio_pin_dir_set(CFG_MOKIBO_TOUCH_DEBUG_PIN, NRF_GPIO_PIN_DIR_OUTPUT);
	//nrf_gpio_pin_write(CFG_MOKIBO_TOUCH_DEBUG_PIN, 1);
	//nrf_delay_ms (500);
	//nrf_gpio_pin_write(CFG_MOKIBO_TOUCH_DEBUG_PIN, 0);
}
//-------------------------------------------------------------------------------------------------------------------------
//			drv_cypress_touch_power_down ..... by Stanley
//-------------------------------------------------------------------------------------------------------------------------
void drv_cypress_touch_power_down(void)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	DEB("drv_cypress_touch_power_down()\n");
	//
	nrf_gpio_pin_dir_set(CFG_MOKIBO_TOUCH_INT_PIN, NRF_GPIO_PIN_DIR_INPUT);
	nrf_gpio_pin_dir_set(CFG_MOKIBO_TOUCH_RESET_PIN, NRF_GPIO_PIN_DIR_OUTPUT);
	// set LOW
	nrf_gpio_pin_write(CFG_MOKIBO_TOUCH_RESET_PIN, 0);

	// U6 power off - set LOW
	nrf_gpio_pin_dir_set(CFG_MOKIBO_TOUCH_PWR_PIN, NRF_GPIO_PIN_DIR_OUTPUT);
	nrf_gpio_pin_write(CFG_MOKIBO_TOUCH_PWR_PIN, 0);
	
}

//-------------------------------------------------------------------------------------------------------------------------
//			drv_cypress_buffer_dump
//-------------------------------------------------------------------------------------------------------------------------
#if	defined(DRV_CYPRESS_DUMP_TRACE)
void drv_cypress_buffer_dump(uint8_t *pData, uint32_t len)
{
	uint32_t i;
	int ret;
	uint8_t buf[256], *pBuf;
	
	pBuf = buf;
	for(i = 0; i < len; i++) 
	{
		if( !(i % 8) ) 
		{
			ret = sprintf((char *)pBuf, "\n");
			pBuf += ret;
		}
		ret = sprintf((char *)pBuf, " [0x%02X]", *pData++);
		pBuf += ret;
	}
	DEBUGS ("%s", buf);
}
#endif /* end of DRV_CYPRESS_DUMP_TRACE */

//-------------------------------------------------------------------------------------------------------------------------
//
//-------------------------------------------------------------------------------------------------------------------------
void drv_cypress_mode_set(DRV_CYPRESS_ACTIVE_AREA_T new_area)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
#if (AREA_PROPERTIE == AREA_PROPERTIE_ALWAYSFORCE_TO_WHOLE_AREA)
	//
#elif (AREA_PROPERTIE == AREA_PROPERTIE_ORIGINAL)
	m_active_area = new_area;
#elif (AREA_PROPERTIE == AREA_PROPERTIE_FLiMBS_20190121_MOD_TOUCH_WHOLE_AREA_WITH_FILTER_KEYS)
//
#endif
}

/*
================================================================================
   Function name : drv_cypress_get_fw_version()
================================================================================
*/
uint32_t drv_cypress_get_fw_version(void)
{
	return m_img_fw_ver;
}

#ifdef TRACE_CYPRESS_I2C_DATA
//-------------------------------------------------------------------------------------------------------------------------------
//			show_wrbuff
//-------------------------------------------------------------------------------------------------------------------------------
void show_wrbuff(uint8_t *wrbuff, uint16_t cnt, char *msg)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	uint16_t i;
	________________________________LOG_drvCypress_checkCypressUpdate(0,"");
	//SYSTEM_MSG("\n");
	________________________________LOG_drvCypress_checkCypressUpdate(-1,"%s : ",msg);
	
	SYSTEM_MSG(" 05"); // Command Register Address[7:0]
	for(i=0; i < cnt; i++)	// 여기서 cnt는  wrbuff[]에 있는 데이터의 길이이다.
	{
		________________________________LOG_drvCypress_checkCypressUpdate(-1," %02X", *wrbuff++);
	}
	//SYSTEM_MSG("\n");
}
#endif

#ifdef TRACE_CYPRESS_I2C_DATA
//-------------------------------------------------------------------------------------------------------------------------------
//			read_and_show_ack_from_touch
//-------------------------------------------------------------------------------------------------------------------------------
void read_and_show_ack_from_touch(uint8_t Ignore_TT_INT, char *msg)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	//-------------------------------------------------------------------------------------------------------------------------------
	ret_code_t rv;
    uint16_t cnt = 0;

	//-------------------------------------------------------------------------------------------------------------------------------
	//SYSTEM_MSG("\n");
	SYSTEM_MSG(msg);
	
	//-------------------------------------------------------------------------------------------------------------------------------
	//rv= cypress_Check_Interrupt_Routine(CY_PIN_LOW, 500000); // 500ms wait
	rv= is_TT_INT_low(500); // max 500-ms wait
	
	//
	if(Ignore_TT_INT) rv=NRF_SUCCESS;
	
	//
	if(rv==NRF_SUCCESS)
	{
		//-------------------------------------------------------------------------------------------------------------------------------
		//1. Get the input report length....
		memset(rdbuff,0, 2);
		//					0x24,		0x03,			  
		if (drv_twi_read(CYPRESS_ADDR, I2C_REG_INPUT_REP, rdbuff, 2) != NRF_SUCCESS)
		{			
			________________________________LOG_drvCypress_initBaseline_Funchead(2,"###Error: ?? Input Report Length ??");
			return;
		}
		//-------------------------------------------------------------------------------------------------------------------------------
		SYSTEM_MSGS("LEN: ");
		for(int i=0; i < 2; i++)
		{
			SYSTEM_MSGS(" %02X",rdbuff[i]);
		}
		//SYSTEM_MSG("\n");
///
		//-------------------------------------------------------------------------------------------------------------------------------
		cnt = *(uint16_t *)rdbuff;
		memset(rdbuff,0, cnt);
		if (drv_twi_read(CYPRESS_ADDR, I2C_REG_INPUT_REP, rdbuff, cnt) != NRF_SUCCESS)
		{
			________________________________LOG_drvCypress_initBaseline_Funchead(2,"###Error: read_and_show_ack_from_touch();");
			return;
		}
		//-------------------------------------------------------------------------------------------------------------------------------
		SYSTEM_MSG(" Full Packet: ");
		for(int i=0; i < cnt; i++)
		{
			SYSTEM_MSGS(" %02X",rdbuff[i]);
		}
		SYSTEM_MSG("\n\n");

		//-------------------------------------------------------------------------------------------------------------------------------
		//rv= cypress_Check_Interrupt_Routine(CY_PIN_HIGH, 5000);	// 1ms wait must
		rv= is_TT_INT_high(5); // max 5-ms wait
		if(rv != NRF_SUCCESS)
		{
			________________________________LOG_drvCypress_initBaseline_Funchead(2,"#ERROR: timeout - TT_INT pin is Not HIGH");
		}
		else
		{
			//________________________________LOG_drvCypress_initBaseline_Funchead(2,"baseline Init SUCCESS");
		}

		//-------------------------------------------------------------------------------------------------------------------------------
	}
	else
	{
		________________________________LOG_drvCypress_initBaseline_Funchead(2,"#ERROR: detection timeout - TT_INT pin is Not LOW"); 
	}
	
}
#endif


/*
================================================================================
   Function name : drv_cypress_init_Baseline()
================================================================================
*/
void drv_cypress_init_Baseline(void)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	________________________________LOG_drvCypress_initBaseline_Funchead(1,"drv_cypress_init_Baseline");

	#if 1
	ret_code_t rv;
    uint16_t cnt = 0;

	#ifdef TRACE_CYPRESS_I2C_DATA
		//SYSTEM_MSG("Requesting INIT_BASELINE: expectation= 06 00 1F 00 x9 xx\n");
	#endif //TRACE_CYPRESS_I2C_DATA

	// Launch Application if Report ID
	wrbuff[cnt++]  = 0x00;	// Output Report Register Address[15:8] = 00h
	wrbuff[cnt++]  = 0x06; 	// Length[7:0] = 06h
	wrbuff[cnt++]  = 0x00;	// Length[15:8] = 00h
	wrbuff[cnt++]  = 0x2F; 	// Report ID = 2Fh
	wrbuff[cnt++]  = 0x00;	// RSVD = 00h
	wrbuff[cnt++]  = 0x29;	// Command Code = 29h
	wrbuff[cnt++]  = 0x07;	// RSVD = 00h & SELF BTN MUT 
    
	// 실제 packet은:   {0x05 wrbuff[] } 로서  packet 길이는 (1+cnt)이다.
	//							0x24,		0x05,			wrbuf[],	cnt= 주의:wrbuff[]만의 length이다 !
	rv = drv_twi_write_burst(CYPRESS_ADDR, I2C_REG_OUTPUT_REP, &wrbuff[0], cnt);
	if (NRF_SUCCESS != rv) 
	{
		#ifdef TRACE_CYPRESS_I2C_DATA
			//SYSTEM_MSG("###Error: drv_cypress_init_Baseline() \n");
		#endif
		return;	
	}

	#ifdef TRACE_CYPRESS_I2C_DATA
		//show_wrbuff(wrbuff,cnt,"INIT BASELINE Request:");
		//SYSTEM_MSG("\t Ack Expectation: 05 00 xxxxxx(P) (T)0001000 \n");
		read_and_show_ack_from_touch(0,"INIT_BASELINE Ack:");
	#endif
	#endif

}

//-----------------------------------------------------------------------------------------------------------------------------------
//			drv_cypress_req_deep_sleep
//-----------------------------------------------------------------------------------------------------------------------------------
// wakeup= 0,  deep_sleep= 1
void drv_cypress_req_deep_sleep(uint8_t wake_or_sleep)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t rv;
    uint16_t cnt = 0;
	
	// Refer to 3.7.2 Deep-Sleep Command of TRM_CYTMA445A. CYTT21X31X
	// :CommandReg(0x05)			// Command Register Address [7:0]
	wrbuff[cnt++]  = 0x00; 			// Command Register Address [15:8]
	wrbuff[cnt++]  = wake_or_sleep; // ***Power State: wakeup=0x00, sleep=0x01
	wrbuff[cnt++]  = 0x08;			// Opcode = 1000b
	
// 실제 packet은:   {0x05 wrbuff[] } 로서  packet 길이는 (1+cnt)이다.
	//							0x24,		0x05,			wrbuf[],	cnt= 주의:wrbuff[]만의 length이다 !
	rv = drv_twi_write_burst(CYPRESS_ADDR, I2C_REG_COMMAND, &wrbuff[0], cnt);
			//	#define CYPRESS_ADDR		0x24	// i2c slave address
			//	#define I2C_REG_BASE    	(0x00)	//
			//	--- PIP(Packet Interface Protocol) Registers ---
			//	#define I2C_REG_HID     	(0x01)	// HID Descriptor Register 
			//	#define I2C_REG_REPORT_DES 	(0x02)	// Report Descriptor Register
			//	#define I2C_REG_INPUT_REP 	(0x03)	// Input Report Register
			//	#define I2C_REG_OUTPUT_REP 	(0x04) 	// Output Report Register
			//	#define I2C_REG_COMMAND 	(0x05) 	// Command Register (for Deep Sleep Request or Device Reset)
			//

	if (NRF_SUCCESS != rv) 
	{
		#ifdef TRACE_CYPRESS_I2C_DATA
			SYSTEM_MSG("Error! I2C write failed[%d]", rv);
		#endif //TRACE_CYPRESS_I2C_DATA
	}

	#ifdef TRACE_CYPRESS_I2C_DATA
		show_wrbuff(wrbuff,cnt,"DEEP SLEEP Request:");
		SYSTEM_MSG("\t Ack Expectation: 05 00 xxxxxx(P) (T)0001000 \n");
		if(wake_or_sleep==1)
		{
			// deep_sleep
			read_and_show_ack_from_touch(1,"DEEP SLEEP Ack:");
		} else {
			// wakeup
			read_and_show_ack_from_touch(0,"WAKEUP from DEEP SLEEP Ack:");
		}
	#endif //TRACE_CYPRESS_I2C_DATA

}

//-----------------------------------------------------------------------------------------------------------------------------------
//			drv_cypress_req_wake_up
//-----------------------------------------------------------------------------------------------------------------------------------
void drv_cypress_req_wake_up(void)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t rv;
    uint16_t cnt = 0;
	//
	#ifdef TRACE_CYPRESS_I2C_DATA
		SYSTEM_MSG("drv_cypress_req_wake_up:...\n");
	#endif
	
	// Refer to 3.7.2 Deep-Sleep Command of TRM_CYTMA445A. CYTT21X31X
	// :CommandReg(0x05)
	wrbuff[cnt++]  = 0x00; 	// Command Register Address [15:8]
	wrbuff[cnt++]  = 0x00; 	// ***Power State: On (00h)***
	wrbuff[cnt++]  = 0x08;	// Opcode = 1000b
    
	rv = drv_twi_write_burst(CYPRESS_ADDR, I2C_REG_COMMAND, &wrbuff[0], cnt);
	if (NRF_SUCCESS != rv) 
	{
		#ifdef TRACE_CYPRESS_I2C_DATA
			SYSTEM_MSG("drv_cypress_req_wakeup: Error! I2C write failed[%d]", rv);
		#endif
	}
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			drv_cypress_req_device_reset
//-----------------------------------------------------------------------------------------------------------------------------------
void drv_cypress_req_device_reset(void)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t rv;
    uint16_t cnt = 0;

	#ifdef TRACE_CYPRESS_I2C_DATA
		SYSTEM_MSG("drv_cypress_req_device_reset:...\n");
	#endif
	

	// Refer to 3.7.1 Device Reset (SRES) Command of TRM_CYTMA445A. CYTT21X31X
	// :CommandReg(0x05)
	wrbuff[cnt++]  = 0x00; 	// Command Register Address [15:8]
	wrbuff[cnt++]  = 0x00; 	// Report ID = 0000b
	wrbuff[cnt++]  = 0x01;	// Opcode = 0001b
    
	rv = drv_twi_write_burst(CYPRESS_ADDR, I2C_REG_COMMAND, &wrbuff[0], cnt);

	if (NRF_SUCCESS != rv) 
	{
		#ifdef TRACE_CYPRESS_I2C_DATA
			SYSTEM_MSG("drv_cypress_req_device_reset: Error! I2C write failed[%d]", rv);
		#endif
	}
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			drv_cypress_req_calibration
//-----------------------------------------------------------------------------------------------------------------------------------
void drv_cypress_req_calibration(uint8_t senseMode)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t rv;
    uint16_t cnt = 0;

	#ifdef TRACE_CYPRESS_I2C_DATA
		SYSTEM_MSG("Requesting INIT_CALIBRATION: expectation= xx xx 1F 00 x8 xx \n");
	#endif

	// Refer to 6.3.22 of 0e_TRM.pdf

	// I2C_REG_COMMAND(0x05)

	// Output Report Reg(0x04)
	//wrbuff[cnt++]  = 0x04; 	// Output Report Register Address[7:0] = 00h
	wrbuff[cnt++]  = 0x00; 	// Output Report Register Address[15:8] = 00h
	//
	wrbuff[cnt++]  = 0x06; 	// Length[7:0]
	wrbuff[cnt++]  = 0x00;	// Length[15:8]
	wrbuff[cnt++]  = 0x2F;	// Report ID = 2Fh
	wrbuff[cnt++]  = 0x00;	// RSVD = 00h 
#if 0	
	wrbuff[cnt++]  = 0x28;	// RSVD | Command Code = 28h 
	wrbuff[cnt++]  = senseMode;
#else // Billy
	wrbuff[cnt++]  = 0x29;
	wrbuff[cnt++]  = 0x01;
#endif
					// Sense Mode:	00h = Calibrate IDAC for mutual-cap
					//				01h = Calibrate IDAC for buttons
					//				02h = Calibrate IDAC for self-cap
					//				03h = Calibrate IDAC for mutual without calibrating hop frequencies
					//				04h = Calibrate IDAC only, no hop calibration, no attenuator change
					//
	rv = drv_twi_write_burst(CYPRESS_ADDR, I2C_REG_OUTPUT_REP, &wrbuff[0], cnt);
									//	#define CYPRESS_ADDR	0x24
									//	#define I2C_REG_BASE    	(0x00)
									//	#define I2C_REG_HID     	(0x01)	// HID Descriptor Register 
									//	#define I2C_REG_REPORT_DES 	(0x02)	// Report Descriptor Register
									//	#define I2C_REG_INPUT_REP 	(0x03)	// Input Report Register
									//	#define I2C_REG_OUTPUT_REP 	(0x04) 	// Output Report Register
									//	#define I2C_REG_COMMAND 	(0x05) 	// Command Register
	if (NRF_SUCCESS != rv) 
	{
		#ifdef TRACE_CYPRESS_I2C_DATA
			SYSTEM_MSG("###Error: drv_cypress_req_calibration()\n");
		#endif
	}

#if 1
	//rv= cypress_Check_Interrupt_Routine(CY_PIN_LOW, 500000); // 500ms wait
	rv= is_TT_INT_low(500); // max 500-ms wait
	if(rv==NRF_SUCCESS)
	{
		memset(rdbuff,0, 2);
		if (drv_twi_read(CYPRESS_ADDR, I2C_REG_INPUT_REP, rdbuff, 2) != NRF_SUCCESS)
		{
			SYSTEM_MSG("###Error: CALIBRATION_RESPONSE \n");
			return;
		}
		SYSTEM_MSG("CALIBRATION Response:");
		for(int i=0; i < 2; i++)
		{
			SYSTEM_MSG(" %02X",rdbuff[i]);
		}
		SYSTEM_MSG("\n");
		
#if 1 // by Stanley
		//
		cnt = *(uint16_t *)rdbuff;
		memset(rdbuff,0, cnt);
		if (drv_twi_read(CYPRESS_ADDR, I2C_REG_INPUT_REP, rdbuff, cnt) != NRF_SUCCESS)
		{
			SYSTEM_MSG("###Error: CALIBRATION_RESPONSE \n");
			return;
		}
		SYSTEM_MSG("more: ");
		for(int i=0; i < cnt; i++)
		{
			SYSTEM_MSG(" %02X",rdbuff[i]);
		}
		SYSTEM_MSG("\n");
			
#endif		


		//rv= cypress_Check_Interrupt_Routine(CY_PIN_HIGH, 5000);	// 1ms wait must
		rv= is_TT_INT_high(5); // max 5-ms wait
		if(rv != NRF_SUCCESS)
		{
			SYSTEM_MSG("#ERROR: detection timeout - TT_INT pin HIGH \n");
		}
	}
	else
	{
		SYSTEM_MSG("#ERROR: detection timeout - TT_INT pin LOW \n"); 
	}
#endif

}


//-----------------------------------------------------------------------------------------------------------------------------------
//			drv_cypress_req_suspend_scanning .... verified
//-----------------------------------------------------------------------------------------------------------------------------------
// 주의: 터치 잔재가 있으면, suspend 명령이 '거부'될 수 있다. 따라서, 잔재를 말끔히 없애야 한다.
//
void drv_cypress_req_suspend_scanning(void)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t rv;
    uint16_t cnt = 0;

	// Refer to 6.3.6 of 0e_TRM.pdf
	// Output Report Reg(0x04)
	wrbuff[cnt++]  = 0x00; 	// Output Report Register Address[15:8] = 00h
	//
	wrbuff[cnt++]  = 0x05; 	// Length[7:0]
	wrbuff[cnt++]  = 0x00;	// Length[15:8]
	wrbuff[cnt++]  = 0x2F;	// Report ID = 2Fh
	wrbuff[cnt++]  = 0x00;	// RSVD = 00h 
	wrbuff[cnt++]  = 0x03;	// RSVD | Command Code = 03h

	rv = drv_twi_write_burst(CYPRESS_ADDR, I2C_REG_OUTPUT_REP, &wrbuff[0], cnt);
			//	#define CYPRESS_ADDR	0x24
			//	#define I2C_REG_BASE    	(0x00)
			//	#define I2C_REG_HID     	(0x01)	// HID Descriptor Register 
			//	#define I2C_REG_REPORT_DES 	(0x02)	// Report Descriptor Register
			//	#define I2C_REG_INPUT_REP 	(0x03)	// Input Report Register
			//	#define I2C_REG_OUTPUT_REP 	(0x04) 	// Output Report Register
			//	#define I2C_REG_COMMAND 	(0x05) 	// Command Register
	//-----------------------------------------------------------------------------------------------------------------------------------
	if (NRF_SUCCESS != rv) 
	{
		SYSTEM_MSG("###Error: drv_cypress_req_suspend_scanning() \n");
		return;
	}
	//-----------------------------------------------------------------------------------------------------------------------------------
	#ifdef TRACE_CYPRESS_I2C_DATA
		//show_wrbuff(wrbuff,cnt,"SUSPEND_SCANNING Request:");
		//SYSTEM_MSG("\t Ack Expectation: 05 00 1F 00 {TGLxxx 0011} \n");
		read_and_show_ack_from_touch(0,"SUSPEND_SCANNING Ack:");
	#endif
	//-----------------------------------------------------------------------------------------------------------------------------------

}

//-----------------------------------------------------------------------------------------------------------------------------------
//			drv_cypress_req_resume_scanning ... verified
//-----------------------------------------------------------------------------------------------------------------------------------
void drv_cypress_req_resume_scanning(void)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t rv;
    uint16_t cnt = 0;

	// Refer to 6.3.7 of 0e_TRM.pdf
	// Output Report Reg(0x04)
	wrbuff[cnt++]  = 0x00; 	// Output Report Register Address[15:8] = 00h
	//
	wrbuff[cnt++]  = 0x05; 	// Length[7:0]
	wrbuff[cnt++]  = 0x00;	// Length[15:8]
	wrbuff[cnt++]  = 0x2F;	// Report ID = 2Fh
	wrbuff[cnt++]  = 0x00;	// RSVD = 00h 
	wrbuff[cnt++]  = 0x04;	// RSVD | Command Code = 04h

	rv = drv_twi_write_burst(CYPRESS_ADDR, I2C_REG_OUTPUT_REP, &wrbuff[0], cnt);
			//	#define CYPRESS_ADDR	0x24
			//	#define I2C_REG_BASE    	(0x00)
			//	#define I2C_REG_HID     	(0x01)	// HID Descriptor Register 
			//	#define I2C_REG_REPORT_DES 	(0x02)	// Report Descriptor Register
			//	#define I2C_REG_INPUT_REP 	(0x03)	// Input Report Register
			//	#define I2C_REG_OUTPUT_REP 	(0x04) 	// Output Report Register
			//	#define I2C_REG_COMMAND 	(0x05) 	// Command Register
	//-----------------------------------------------------------------------------------------------------------------------------------
	if (NRF_SUCCESS != rv) 
	{
		SYSTEM_MSG("###Error: drv_cypress_req_resume_scanning() \n");
		return;
	}
	//-----------------------------------------------------------------------------------------------------------------------------------
	#ifdef TRACE_CYPRESS_I2C_DATA
		//show_wrbuff(wrbuff,cnt,"RESUME_SCANNING Request:");
		//SYSTEM_MSG("\t Ack Expectation: 05 00 1F 00 {TGLxxx 0100} \n");
		read_and_show_ack_from_touch(0,"RESUME_SCANNING Ack:");
	#endif
	//-----------------------------------------------------------------------------------------------------------------------------------
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			
//-----------------------------------------------------------------------------------------------------------------------------------


















































/*

$$\       $$$$$$\   $$$$$$\   $$$$$$\  $$\                                             
$$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                                            
$$ |     $$ /  $$ |$$ /  \__|$$ /  $$ |$$ |                                            
$$ |     $$ |  $$ |$$ |      $$$$$$$$ |$$ |                                            
$$ |     $$ |  $$ |$$ |      $$  __$$ |$$ |                                            
$$ |     $$ |  $$ |$$ |  $$\ $$ |  $$ |$$ |                                            
$$$$$$$$\ $$$$$$  |\$$$$$$  |$$ |  $$ |$$$$$$$$\                                       
\________|\______/  \______/ \__|  \__|\________|                                      
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\  $$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |$$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |$$ /  \__|
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |\$$$$$$\  
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ | \____$$\ 
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |$$\   $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |\$$$$$$  |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__| \______/ 
//============================================================================//
//                                 LOCAL FUNCTIONS                            //
//============================================================================*/
//-----------------------------------------------------------------------------------------------------------------------------------
//			cypress_read_TT_INT
//-----------------------------------------------------------------------------------------------------------------------------------
ret_code_t cypress_read_TT_INT(void)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t pinValue = nrf_gpio_pin_read(CFG_MOKIBO_TOUCH_INT_PIN);
	________________________________LOG_drvCypress_FuncHeads(4,"nrf_gpio_pin_read(CFG_MOKIBO_TOUCH_INT_PIN:%d):%d",CFG_MOKIBO_TOUCH_INT_PIN,pinValue);
	return pinValue;	// P.30
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			waitGetLevel_TT_INT
//-----------------------------------------------------------------------------------------------------------------------------------
static ret_code_t waitGetLevel_TT_INT(uint8_t pinLevel, uint32_t ms)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	uint8_t pinValue=0;				//
	int32_t loop_count = ms*1000/100; 	// signed... by Stanley
	//
	do {
		pinValue= cypress_read_TT_INT();
		if(pinValue== pinLevel) break;
		//
		nrf_delay_us(100);
		loop_count--;
	} while(loop_count > 0);
	//
	if(loop_count <= 0)
	{
		//APP_ERROR_CHECK(NRF_ERROR_INVALID_STATE);
		return NRF_ERROR_INVALID_STATE;
	}
	return NRF_SUCCESS;
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			is_TT_INT_low .... instead of cypress_Check_Interrupt_Routine
//-----------------------------------------------------------------------------------------------------------------------------------
static ret_code_t is_TT_INT_low(uint32_t timeout_ms)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	return waitGetLevel_TT_INT(CY_PIN_LOW, timeout_ms); // 1ms wait
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			is_TT_INT_high .... instead of cypress_Check_Interrupt_Routine
//-----------------------------------------------------------------------------------------------------------------------------------
static ret_code_t is_TT_INT_high(uint32_t timeout_ms)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	return waitGetLevel_TT_INT(CY_PIN_HIGH, 1); // 1ms wait
}




/*
================================================================================
   Function name : cypress_LaunchApp()
================================================================================
*/
static ret_code_t cypress_LaunchApp(void)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t rv;
    uint16_t cnt = 0;

	// Refer to "6.2.6 Launch Application" of "TRM_CYTMA445A. CYTT21X31X"
	// Launch Application if Report ID
	wrbuff[cnt++]  = 0x00; 	// Output Report Register Addr 
	wrbuff[cnt++]  = 0x0B;  // Length[7:0] = 0Bh
	wrbuff[cnt++]  = 0x00;  // Length[15:8] = 00h
	wrbuff[cnt++]  = 0x40;  // Report ID = 40h
	wrbuff[cnt++]  = 0x00;  // RSVD = 00h
	wrbuff[cnt++]  = 0x01;  // SOP = 01h
	wrbuff[cnt++]  = 0x3B;  // Command Code = 3Bh
	wrbuff[cnt++]  = 0x00;	// Data Length[7:0] = 00h
	wrbuff[cnt++]  = 0x00;	// Data Length[15:8] = 00h
	wrbuff[cnt++]  = 0x20;	// CRC[7:0]
	wrbuff[cnt++]  = 0xC7;	// CRC[15:8]
	wrbuff[cnt++]  = 0x17;	// EOP = 17h 17
	
	

	rv = drv_twi_write_burst(CYPRESS_ADDR, I2C_REG_OUTPUT_REP, &wrbuff[0], cnt);
	
	if (NRF_SUCCESS != rv) 
	{
		DEB("Error! I2C write failed[%d] \n", rv);
		return NRF_ERROR_INTERNAL;
	}

	//rv= cypress_Check_Interrupt_Routine(CY_PIN_LOW, 1000);	// 1ms wait must
	rv= is_TT_INT_low(1); // max 1-ms wait
	if(rv != NRF_SUCCESS)
	{
		// Stanley.
		DEB("#Error: TT_INT is not LOW - cypress_LaunchApp()\n");
	}

	// Repeat Read HID Descriptor commands to verify the Report ID before proceeding.
	rv = drv_twi_read (CYPRESS_ADDR, I2C_REG_BASE, rdbuff, 2);
	if (NRF_SUCCESS != rv) 
	{
		DEB("Error! I2C read failed[%d] \n", rv);
		INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30
		return NRF_ERROR_INTERNAL;
	} 
	else 
	{
		#if	defined(DRV_CYPRESS_CMD_TRACE)
		DEB("read OK. 0x%02X, 0x%02X \n", rdbuff[0], rdbuff[1]);
		#endif		
	}

	//rv= cypress_Check_Interrupt_Routine(CY_PIN_HIGH, 1000);	// 1ms wait must
	rv= is_TT_INT_high(1); // max 1-ms wait
	if(rv != NRF_SUCCESS)
	{
		// Stanley.
		DEB("#Error: TT_INT is not HIGH - cypress_LaunchApp()\n");
	}
	
	return (NRF_SUCCESS);
}



//-----------------------------------------------------------------------------------------------------------------------------------
//			cypress_Check_Reset_Sentinel
//-----------------------------------------------------------------------------------------------------------------------------------
//	The reset sentinel is the first response sent by the TrueTouch device following every device reset. 
//	Before communication can proceed, the reset sentinel must be read. 
//	The value of the reset sentinel equals 0x0000.
//
static ret_code_t cypress_Check_Reset_Sentinel(void)
{	
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	________________________________LOG_drvCypress_checkCypressUpdate(0,"");
	ret_code_t rv;

	// is the TT_INT low ?
	//rv = cypress_Check_Interrupt_Routine(CY_PIN_LOW, 1000);	// 1ms wait must
	rv= is_TT_INT_low(1); // max 1-ms wait
	if(rv != NRF_SUCCESS)
	{
		// Stanley.
		________________________________LOG_drvCypress_checkCypressUpdate(1,"#Error: TT_INT is not LOW - cypress_Check_Reset_Sentinel()\n");
	}

	// buffer for the next reading
	memset(rdbuff, 0, 2);
	
	// is the sentinel data 0x0000 ?
	//					0x24			0x00	  rdbuff[]
	rv = drv_twi_read(CYPRESS_ADDR, I2C_REG_BASE, rdbuff, 2);
	if (rv != NRF_SUCCESS) 
	{
		________________________________LOG_drvCypress_checkCypressUpdate(1,"I2C read failed - cypress_Check_Reset_Sentinel()\n");
		return NRF_ERROR_INTERNAL;
	} 

	// is the TT_INT high?
	//rv = cypress_Check_Interrupt_Routine(CY_PIN_HIGH, 1000);	// 1ms wait must
	rv= is_TT_INT_high(1); // max 1-ms wait
	if(rv != NRF_SUCCESS)
	{
		________________________________LOG_drvCypress_checkCypressUpdate(1,"#Error: TT_INT is not HIGH - cypress_Check_Reset_Sentinel()\n");
	}

	//DEB("Read Reset Sentinel: sentinel[0]=0x%x, sentinel[1]=0x%x \n", rdbuff[0], rdbuff[1]);

	// the sentinel data should be 0x0000 ...
    if ( (rdbuff[0] != 0x00) && (rdbuff[1] != 0x00) )
    {
		________________________________LOG_drvCypress_checkCypressUpdate(1,"Reset Sentinel check failed - cypress_Check_Reset_Sentinel.\n");
		return NRF_ERROR_INTERNAL;
    }
	//
	return NRF_SUCCESS;
}




/*
================================================================================
   Function name : cypress_Get_HID_Descriptor()
================================================================================
*/
/*
The contents of the HID Descriptor register remain static except 
when the device transitions from the Bootloader application 
to the TrueTouch application and visa-versa.
*/
static int32_t cypress_Get_HID_Descriptor(void)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
    uint8_t bLength;
    int mode;
	ret_code_t rv;


	// Read HID Descriptor
	if (drv_twi_write (CYPRESS_ADDR, I2C_REG_HID, 0x00) != NRF_SUCCESS) {
        DEBUG("I2C write failed!!");
        return (-1);
    }
	
	//rv= cypress_Check_Interrupt_Routine(CY_PIN_LOW, 1000);	// 1ms wait must
	rv= is_TT_INT_low(1); // max 1-ms wait
	if(rv != NRF_SUCCESS)
	{
		DEB("#Error: TT_INT not LOW - cypress_Get_HID_Descriptor() \n");
	}

	// Read packet length and packet ID
	memset(rdbuff, 0, 3);
	if (drv_twi_read (CYPRESS_ADDR, I2C_REG_BASE, rdbuff, 3) != NRF_SUCCESS) {
        DEBUG("I2C read failed!!");
        return (-1);
    }

	#if	defined(DRV_CYPRESS_TRACE)
	DEBUG("read data: [0: 0x%x, 1: 0x%x, 2:0x%x]", rdbuff[0], rdbuff[1], rdbuff[2]);
	#endif
	// 0x20, 0x00, 0xFF/0xF7

	// Read the full packet to set the register pointer.
    bLength = rdbuff[0];
	memset(rdbuff, 0, DRV_CYPRESS_BUFFER_SIZE);
	if (drv_twi_read (CYPRESS_ADDR, I2C_REG_BASE, rdbuff, bLength) != NRF_SUCCESS) {
        DEBUG("I2C read failed!!");
        return (-1);
    }

	//rv=cypress_Check_Interrupt_Routine(CY_PIN_HIGH, 1000);	// 1ms wait must
	rv= is_TT_INT_high(1); // max 1-ms wait
	if(rv != NRF_SUCCESS)
	{
		DEB("#Error: TT_INT not HIGH - during cypress_Get_HID_Descriptor() \n");
	}

	#if	defined(DRV_CYPRESS_CMD_TRACE)
	for (int i=0; i<bLength; i++) {
		DEB(" 0x%02X", rdbuff[i]);
	}
	DEB("\n");
	#endif
	
	#if	defined(DRV_CYPRESS_TRACE)
	DEBUG("HID mode: 0x%x", rdbuff[2]);
	#endif

    if (rdbuff[2] == 0xFF)  // Bootlodaer mode
        mode = DRV_CYPRESS_MODE_BOOTLOADER;
    else if (rdbuff[2] == 0xF7) // Application mode
        mode = DRV_CYPRESS_MODE_OPERATION;
    else
        return (-1);
	//
    return (mode);
}
	

/*
================================================================================
   Function name : check_valid_range()
================================================================================
*/
static ret_code_t check_valid_range(uint16_t x, uint16_t y)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	
#if (AREA_PROPERTIE == AREA_PROPERTIE_ALWAYSFORCE_TO_WHOLE_AREA)
	return NRF_SUCCESS;
#elif (AREA_PROPERTIE == AREA_PROPERTIE_ORIGINAL)
	ret_code_t result;

	#ifdef USE_ANTI_GHOST_CLIPPING
	// Temporarily inserted by Stanley...
	if(x > CLIP_RIGHT_X ) return NRF_ERROR_NOT_SUPPORTED;
	if(y < CLIP_BOTTOM_Y) return NRF_ERROR_NOT_SUPPORTED;
	#endif //USE_ANTI_GHOST_CLIPPING

	switch(m_active_area)
	{
		case DRV_CYPRESS_ACTIVE_AREA_WHOLE:
			result = NRF_SUCCESS;
			break;

		case DRV_CYPRESS_ACTIVE_AREA_LEFT:
			result = (x < ACTIVE_AREA_LEFT_THRESHOLD) ? NRF_SUCCESS : NRF_ERROR_NOT_SUPPORTED;
			break;
		
		case DRV_CYPRESS_ACTIVE_AREA_RIGHT:
			result = (x > ACTIVE_AREA_RIGHT_THRESHOLD) ? NRF_SUCCESS : NRF_ERROR_NOT_SUPPORTED;
			break;
		
		default :
			result = NRF_ERROR_INVALID_STATE;
			DEBUG("OOPS[%d]", m_active_area);
			break;
	}
	return result;
#elif (AREA_PROPERTIE == AREA_PROPERTIE_FLiMBS_20190121_MOD_TOUCH_WHOLE_AREA_WITH_FILTER_KEYS)
	ret_code_t result;
	result = m_flimbs_touch_clipping_filterkey(x,y);
	return result;
#endif//AREA_PROPERTIE
		
}

/*
================================================================================
   Function name : correct()
================================================================================
*/
static uint16_t correct(const uint16_t y)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	uint16_t i, correction = 0;

	// To clarify and speed up the algorithm... newly added by Stanley: 2018.8.30
	if (y < LOWER_LIMIT_Y) return correction;
	if (y >= UPPER_LIMIT_Y) return m_correction_table[0].correction;

	// To speed up: prohibit the repeated calculation..... Stanley - 2018.8.27
	uint16_t limit = sizeof(m_correction_table) / sizeof(correction_t);

	//for(i=0; i<sizeof(m_correction_table)/sizeof(correction_t); i++)
	for (i = 0; i<limit; i++)
	{
		if(y > m_correction_table[i].y)
		{
			correction = m_correction_table[i].correction;
			break;
		}
	}
	return correction;

	
}



//-----------------------------------------------------------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------------------------------------------------------
void cypress_pwr(uint8_t OnOff)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	switch(OnOff)
	{
		case 0:	
		// 어느 경우에도, 전원은 끄지 않도록 한다.
			
#ifdef USE_CYPRESS_PWR_ON_OFF
			// turn off the power: @ P.25
		//	nrf_gpio_pin_dir_set(CFG_MOKIBO_TOUCH_PWR_PIN, NRF_GPIO_PIN_DIR_OUTPUT);
		//	nrf_gpio_pin_write(CFG_MOKIBO_TOUCH_PWR_PIN, 0); // P.25 Off
		//==> power-off된 cypress touch는, i2c bus를 GND로 붙잡아서 않된다.
#endif
		
#ifdef USE_CYPRESS_RESET_ON_OFF			
			// reset : @ P.29
			nrf_gpio_pin_dir_set(CFG_MOKIBO_TOUCH_RESET_PIN, NRF_GPIO_PIN_DIR_OUTPUT);
			nrf_gpio_pin_write(CFG_MOKIBO_TOUCH_RESET_PIN, 0); // P.29 Reset
#endif
		
			break;
		
		case 1:	// 전원이 꺼져 있다면, 켠다.

#ifdef USE_CYPRESS_PWR_ON_OFF
			// turn on the power : @ P.25
			nrf_gpio_pin_dir_set(CFG_MOKIBO_TOUCH_PWR_PIN, NRF_GPIO_PIN_DIR_OUTPUT);
			nrf_gpio_pin_write(CFG_MOKIBO_TOUCH_PWR_PIN, 1); // P.25 ON
			//==> power-off된 cypress touch는, i2c bus를 GND로 붙잡아서 않된다.
			DEB("==CYPRESS PWR ON==\n");
#endif
		
#ifdef	USE_CYPRESS_RESET_ON_OFF			
			// release reset : @ P.29
			nrf_gpio_pin_dir_set(CFG_MOKIBO_TOUCH_RESET_PIN, NRF_GPIO_PIN_DIR_OUTPUT);
			//
			nrf_gpio_pin_write(CFG_MOKIBO_TOUCH_RESET_PIN, 0); // P.29 Reset
			nrf_delay_ms(200); // for sure reset
			nrf_gpio_pin_write(CFG_MOKIBO_TOUCH_RESET_PIN, 1); // P.29 Release
			DEB("==CYPRESS RESET==\n");
#endif
			break;
		
		default:
			break;
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			drv_cypress_power_on
//-----------------------------------------------------------------------------------------------------------------------------------
void drv_cypress_power_on(void)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	// turn on the power : @ P.25
	nrf_gpio_pin_dir_set(CFG_MOKIBO_TOUCH_PWR_PIN, NRF_GPIO_PIN_DIR_OUTPUT);
	nrf_gpio_pin_write(CFG_MOKIBO_TOUCH_PWR_PIN, 1); // P.25 ON
	//==> power-off된 cypress touch는, i2c bus를 GND로 붙잡아서 않된다.
	DEB("==drv_cypress_power_on()==\n");
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			drv_cypress_power_off
//-----------------------------------------------------------------------------------------------------------------------------------
void drv_cypress_power_off(void)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	// turn on the power : @ P.25
	nrf_gpio_pin_dir_set(CFG_MOKIBO_TOUCH_PWR_PIN, NRF_GPIO_PIN_DIR_OUTPUT);
	nrf_gpio_pin_write(CFG_MOKIBO_TOUCH_PWR_PIN, 0); // P.25 Off
	//==> power-off된 cypress touch는, i2c bus를 GND로 붙잡아서 않된다.
	DEB("==drv_cypress_power_off()==\n");
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			drv_cypress_ResetPin_Low
//-----------------------------------------------------------------------------------------------------------------------------------
void drv_cypress_ResetPin_Low(void)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	// reset pin : @ P.29 --> make it 'low'
	nrf_gpio_pin_dir_set(CFG_MOKIBO_TOUCH_RESET_PIN, NRF_GPIO_PIN_DIR_OUTPUT);
	nrf_gpio_pin_write(CFG_MOKIBO_TOUCH_RESET_PIN, 0); // P.29 Reset Pin --> to 'low'
	DEB("==drv_cypress_ResetPin_Low()==\n");
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			drv_cypress_ResetPin_High
//-----------------------------------------------------------------------------------------------------------------------------------
void drv_cypress_ResetPin_High(void)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	// reset pin : @ P.29 --> make it 'high'
	nrf_gpio_pin_dir_set(CFG_MOKIBO_TOUCH_RESET_PIN, NRF_GPIO_PIN_DIR_OUTPUT);
	nrf_gpio_pin_write(CFG_MOKIBO_TOUCH_RESET_PIN, 1); // P.29 Reset Pin --> to 'High'
	DEB("==drv_cypress_ResetPin_High()==\n");
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			drv_cypress_ResetPin_Low_Wait_High()
//-----------------------------------------------------------------------------------------------------------------------------------
void drv_cypress_ResetPin_Low_Wait_High(void)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	drv_cypress_ResetPin_Low();
	nrf_delay_ms(200); // for sure reset
	drv_cypress_ResetPin_High();
	DEB("==drv_cypress_ResetPin_Low_Wait_High()==\n");
}

/*
================================================================================
   Function name : cypress_init()
================================================================================
*/
#if 0
static ret_code_t cypress_init(void)
{
	ret_code_t rv;

	// Stanley: U6 power on - set HIGH
	nrf_gpio_pin_dir_set(CFG_MOKIBO_TOUCH_PWR_PIN, NRF_GPIO_PIN_DIR_OUTPUT);
	//nrf_gpio_pin_write(CFG_MOKIBO_TOUCH_PWR_PIN, 1);
	
	// Read Reset Sentinel


	//rv = cypress_Check_Reset_Sentinel();
	
	//cypress_Get_HID_Descriptor() == DRV_CYPRESS_MODE_BOOTLOADER
    
	rv = cypress_LaunchApp();

        
	nrf_delay_ms(2000);
	return NRF_SUCCESS;
}

#else
static ret_code_t cypress_init(void)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	ret_code_t rv;

	// Stanley: U6 power on - set HIGH
//	nrf_gpio_pin_dir_set(CFG_MOKIBO_TOUCH_PWR_PIN, NRF_GPIO_PIN_DIR_OUTPUT);
//	nrf_gpio_pin_write(CFG_MOKIBO_TOUCH_PWR_PIN, 1);
	
	// Read Reset Sentinel


	rv = cypress_Check_Reset_Sentinel();

	
    if (NRF_SUCCESS != rv)
    {
        ________________________________LOG_drvCypress_checkCypressUpdate(1,"Error! RESET Sentinel Failed[%d]", rv);
		INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30
		return NRF_ERROR_INTERNAL;
    }
	else 
	{		
		________________________________LOG_drvCypress_checkCypressUpdate(1,"RESET Sentinel read OK, buf[0: 0x%x, 1:0x%x]", rdbuff[0], rdbuff[1]);		
	}

    // if running Application, then start bootloader
    if (cypress_Get_HID_Descriptor() == DRV_CYPRESS_MODE_BOOTLOADER)
    {
		________________________________LOG_drvCypress_checkCypressUpdate(1,"Running Bootloader! go to start Application");

		rv = cypress_LaunchApp();

        if (NRF_SUCCESS != rv)
        {
            ________________________________LOG_drvCypress_checkCypressUpdate(1,"TT Application Start  Failed!![%d]", rv);
			INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30
			return NRF_ERROR_INTERNAL;
        }
		
		
		________________________________LOG_drvCypress_checkCypressUpdate(1,"TT Application Start OK !!");
		
    }
	else
	{		
		________________________________LOG_drvCypress_checkCypressUpdate(1,"NO BootLoader");		
	}
			
	//cypress_get_system_infor();

	return NRF_SUCCESS;
}
#endif











/*$$$$$\   $$$$$$\  $$$$$$\ $$\   $$\ $$$$$$$$\ $$$$$$$$\ $$$$$$$\           
$$  __$$\ $$  __$$\ \_$$  _|$$$\  $$ |\__$$  __|$$  _____|$$  __$$\          
$$ |  $$ |$$ /  $$ |  $$ |  $$$$\ $$ |   $$ |   $$ |      $$ |  $$ |         
$$$$$$$  |$$ |  $$ |  $$ |  $$ $$\$$ |   $$ |   $$$$$\    $$$$$$$  |         
$$  ____/ $$ |  $$ |  $$ |  $$ \$$$$ |   $$ |   $$  __|   $$  __$$<          
$$ |      $$ |  $$ |  $$ |  $$ |\$$$ |   $$ |   $$ |      $$ |  $$ |         
$$ |       $$$$$$  |$$$$$$\ $$ | \$$ |   $$ |   $$$$$$$$\ $$ |  $$ |         
\__|       \______/ \______|\__|  \__|   \__|   \________|\__|  \__|         
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\ 
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__|
//============================================================================//
//                                 POINTER FUNCTIONS                          //
//============================================================================*/













/*$$$$$\  $$$$$$$$\ $$$$$$$\  $$\   $$\  $$$$$$\                             
$$  __$$\ $$  _____|$$  __$$\ $$ |  $$ |$$  __$$\                            
$$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |$$ /  \__|                           
$$ |  $$ |$$$$$\    $$$$$$$\ |$$ |  $$ |$$ |$$$$\                            
$$ |  $$ |$$  __|   $$  __$$\ $$ |  $$ |$$ |\_$$ |                           
$$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |$$ |  $$ |                           
$$$$$$$  |$$$$$$$$\ $$$$$$$  |\$$$$$$  |\$$$$$$  |                           
\_______/ \________|\_______/  \______/  \______/                            
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\ 
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__|
//============================================================================//
//                                 DEBUG FUNCTIONS                            //
//============================================================================*/
















/*\   $$\ $$\   $$\ $$\   $$\  $$$$$$\  $$$$$$$$\ $$$$$$$\                   
$$ |  $$ |$$$\  $$ |$$ |  $$ |$$  __$$\ $$  _____|$$  __$$\                  
$$ |  $$ |$$$$\ $$ |$$ |  $$ |$$ /  \__|$$ |      $$ |  $$ |                 
$$ |  $$ |$$ $$\$$ |$$ |  $$ |\$$$$$$\  $$$$$\    $$ |  $$ |                 
$$ |  $$ |$$ \$$$$ |$$ |  $$ | \____$$\ $$  __|   $$ |  $$ |                 
$$ |  $$ |$$ |\$$$ |$$ |  $$ |$$\   $$ |$$ |      $$ |  $$ |                 
\$$$$$$  |$$ | \$$ |\$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$  |                 
 \______/ \__|  \__| \______/  \______/ \________|\_______/                  
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\ 
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__|
//============================================================================//
//                                 UNUSED FUNCTIONS                           //
//============================================================================*/










#if CYPRESS_FW_INCLUDED
/*
================================================================================
   Function name : cypress_GetBootloaderInfo()
================================================================================
*/
static ret_code_t cypress_GetBootloaderInfo(void)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	________________________________LOG_drvCypress_checkCypressUpdate(0,"");
	ret_code_t rv;
    uint8_t bLength;
    ;
    uint16_t cnt = 0;

	DEBUGS ("Enter %s", __func__);

    wrbuff[cnt++] = 0x00;
    wrbuff[cnt++] = 0x0B;   // Command Length(LSB)
    wrbuff[cnt++] = 0x00;   // Command Length(MSB)
    wrbuff[cnt++] = 0x40;   // Report ID
    wrbuff[cnt++] = 0x00;   // Reserved
    wrbuff[cnt++] = 0x01;   // SOP(Start of Packet)
    wrbuff[cnt++] = 0x38;   // Command
    wrbuff[cnt++] = 0x00;
    wrbuff[cnt++] = 0x00;
    wrbuff[cnt++] = 0x9E;	// MSB
    wrbuff[cnt++] = 0x70;	// LSB
    wrbuff[cnt++] = 0x17;   // EOP(End of Packet)

	//uint16_t caledCRC;
	//caledCRC = drv_cypress_Compute_CRC(&wrbuff[5], 4); // 0x9E70
	//DEBUG("CRC: 9E:0x%x, 70:0x%x", (uint8_t)(caledCRC >> 8), (uint8_t)caledCRC);

	if (drv_twi_write_burst (CYPRESS_ADDR, I2C_REG_OUTPUT_REP, &wrbuff[0], cnt) != NRF_SUCCESS) {
        ________________________________LOG_drvCypress_checkCypressUpdate(1,"I2C write failed!!");
		INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30
		return NRF_ERROR_INTERNAL;
    }

	//rv= cypress_Check_Interrupt_Routine(CY_PIN_LOW, 1000);	// 1ms wait must
	rv= is_TT_INT_low(1); // max 1-ms wait
	if(rv != NRF_SUCCESS)
	{
		________________________________LOG_drvCypress_checkCypressUpdate(1,"#Error: TT_INT not LOW - cypress_GetBootloaderInfo()");
	}
	

	memset(rdbuff, 0, 2);
	if (drv_twi_read (CYPRESS_ADDR, I2C_REG_BASE, rdbuff, 2) != NRF_SUCCESS) {
        ________________________________LOG_drvCypress_checkCypressUpdate(1,"I2C read failed!!");
		INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30
		return NRF_ERROR_INTERNAL;
    }

    bLength = rdbuff[0];
	DEBUG("RX LEN: %d", bLength);
	memset(rdbuff, 0, DRV_CYPRESS_BUFFER_SIZE);
 	if (drv_twi_read (CYPRESS_ADDR, I2C_REG_BASE, rdbuff, bLength) != NRF_SUCCESS) {
        ________________________________LOG_drvCypress_checkCypressUpdate(1,"I2C read failed!!");
		INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30
		return NRF_ERROR_INTERNAL;
    }

	//rv= cypress_Check_Interrupt_Routine(CY_PIN_HIGH,1000);	// 1ms wait must
	rv= is_TT_INT_high(1); // max 1-ms wait
	if(rv != NRF_SUCCESS)
	{
		________________________________LOG_drvCypress_checkCypressUpdate(1,"#Error: TT_INT not HIGH - cypress_GetBootloaderInfo()");
	}
	
	if ( rdbuff[5] != 0x00 )
    {
		________________________________LOG_drvCypress_checkCypressUpdate(2,"read status(%x) failed!!", rdbuff[5]);		
        return (-1);
    }
	if(bLength == 0x13) {
		//CHECK STATUS
		//if ( rdbuff[5] != 0x00 )
		//{
		//    return (-1);
		//}
		
		#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
		________________________________LOG_drvCypress_checkCypressUpdate(1,"Status: 0x%x", rdbuff[5]);
		________________________________LOG_drvCypress_checkCypressUpdate(1,"Familly ID: 0x%x", rdbuff[8]);
		________________________________LOG_drvCypress_checkCypressUpdate(1,"Chip Rev ID: 0x%x", rdbuff[9]);
		uint16_t SiliconID = rdbuff[10] + (rdbuff[11] << 8);
		________________________________LOG_drvCypress_checkCypressUpdate(1,"SiliconID : 0x%x%x", SiliconID >> 8, SiliconID);
		________________________________LOG_drvCypress_checkCypressUpdate(1,"Chip Rev: 0x%x", rdbuff[12]);
		________________________________LOG_drvCypress_checkCypressUpdate(1,"BL Major: 0x%x", rdbuff[13]);
		________________________________LOG_drvCypress_checkCypressUpdate(1,"BL Minor: 0x%x", rdbuff[14]);
		#endif
	}
		
    return (NRF_SUCCESS);
}

/*
================================================================================
   Function name : cypress_BootLoader_INIT()
================================================================================
*/
static int cypress_BootLoader_INIT(uint16_t row_size, uint8_t* metadata_row_buf)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	________________________________LOG_drvCypress_checkCypressUpdate(0,"");
	ret_code_t rv;
    uint8_t bLength;
    uint16_t caledCRC;

	uint16_t data_length = sizeof(Security_Key) + row_size;

    uint16_t cnt = 0;
    uint8_t i;

    wrbuff[cnt++] = 0x00;
    // 0x08(basic command) + data length( 0x08(security key size) + meta data size ) + 0x02 (CRC) + 0x01(EOP)
    wrbuff[cnt++] = (0x08 + data_length + 0x02 + 0x01);   // Command Length(LSB)
    wrbuff[cnt++] = 0x00;   // Command Length(MSB)
    wrbuff[cnt++] = 0x40;   // Report ID
    wrbuff[cnt++] = 0x00;   // Reserved
    wrbuff[cnt++] = 0x01;   // SOP(Start of Packet)
    wrbuff[cnt++] = 0x48;   // Command
    wrbuff[cnt++] = (uint8_t)(data_length);
    wrbuff[cnt++] = (uint8_t)(data_length >> 8);

    // Add security key - total 8 bytes
    for (i = 0; i < sizeof(Security_Key); i++)
    {
        wrbuff[cnt++] = Security_Key[i];
    }

    // Add Metadata
    for (i = 0; i < row_size; i++)
    {
        wrbuff[cnt++] = metadata_row_buf[i];
    }

    caledCRC = cypress_Compute_CRC(&wrbuff[5], data_length + 4);    // Calculate CRC value from SOP to end of meta data

    wrbuff[cnt++] = (uint8_t)(caledCRC >> 8);
    wrbuff[cnt++] = (uint8_t)caledCRC;
    wrbuff[cnt++] = 0x17;   // EOP(End of Packet)

	show_wrbuff(wrbuff,cnt,"ack");
	________________________________LOG_drvCypress_checkCypressUpdate(2,"!!...!!SecuKeyLen=%d,dataLen=%d", sizeof(Security_Key), data_length);
	if (drv_twi_write_burst (CYPRESS_ADDR, I2C_REG_OUTPUT_REP, &wrbuff[0], cnt) != NRF_SUCCESS) 
	{
        ________________________________LOG_drvCypress_checkCypressUpdate(2,"I2C write failed!!");
        return (-1);
    }

	//rv= cypress_Check_Interrupt_Routine(CY_PIN_LOW, 1000);	// 1ms wait must
	rv= is_TT_INT_low(1); // max 1-ms wait
	if(rv != NRF_SUCCESS)
	{
		________________________________LOG_drvCypress_checkCypressUpdate(2,"#Error: TT_INT not LOW - cypress_BootLoader_INIT() \n");
	}
	

	if (drv_twi_read (CYPRESS_ADDR, I2C_REG_BASE, rdbuff, 2) != NRF_SUCCESS) 
	{
        ________________________________LOG_drvCypress_checkCypressUpdate(2,"I2C read failed!!");
        return (-1);
	}
    

    bLength = rdbuff[0];
	if (drv_twi_read (CYPRESS_ADDR, I2C_REG_BASE, rdbuff, bLength) != NRF_SUCCESS) {
        ________________________________LOG_drvCypress_checkCypressUpdate(2,"I2C read failed!!");
		INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30		
        return (-1);
    }

	show_wrbuff(rdbuff,bLength,"ack");
	

	//rv= cypress_Check_Interrupt_Routine(CY_PIN_HIGH, 1000);	// 1ms wait must
	rv= is_TT_INT_high(1); // max 1-ms wait
	if(rv != NRF_SUCCESS)
	{
		________________________________LOG_drvCypress_checkCypressUpdate(2,"#Error: TT_INT not HIGH - cypress_BootLoader_INIT() \n");		
	}

	//
    if ( rdbuff[5] != 0x00 )
    {
		________________________________LOG_drvCypress_checkCypressUpdate(2,"read status(%x) failed!!", rdbuff[5]);		
        return (-1);
    }

    return (NRF_SUCCESS);
}

/*
================================================================================
   Function name : cypress_Prog_Row()
================================================================================
*/
static int cypress_Prog_Row(struct cyttsp5_hex_image *row_image)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
    uint8_t bLength;
    uint16_t caledCRC;

    uint16_t data_length = row_image->row_size + 3;

    uint16_t cnt = 0;
    uint8_t i;
	ret_code_t rv;

    wrbuff[cnt++] = 0x00;
    // 0x08(basic command) + data length + meta data size ) + 0x02 (CRC) + 0x01(EOP)
    wrbuff[cnt++] = (0x08 + data_length + 0x02 + 0x01);   // Command Length(LSB)
    wrbuff[cnt++] = 0x00;   // Command Length(MSB)
    wrbuff[cnt++] = 0x40;   // Report ID
    wrbuff[cnt++] = 0x00;   // Reserved
    wrbuff[cnt++] = 0x01;   // SOP(Start of Packet)
    wrbuff[cnt++] = 0x39;   // Command
    wrbuff[cnt++] = (uint8_t)(data_length);       // Data length(LSB)
    wrbuff[cnt++] = (uint8_t)(data_length >> 8);  // Data length(MSB)
    wrbuff[cnt++] = row_image->array_id;    // Flash Array ID = 0x00
    wrbuff[cnt++] = (uint8_t)(row_image->row_num);       // Flash Row ID(LSB)
    wrbuff[cnt++] = (uint8_t)(row_image->row_num >> 8);  // Flash Row ID(MSB)

    // Add ROW data
    for (i = 0; i < (row_image->row_size); i++)
    {
        wrbuff[cnt++] = row_image->row_data[i];
    }

    caledCRC = cypress_Compute_CRC(&wrbuff[5], data_length + 4); // Calculate CRC value from SOP to end of ROW data

    wrbuff[cnt++] = (uint8_t)(caledCRC >> 8);
    wrbuff[cnt++] = (uint8_t)(caledCRC);
    wrbuff[cnt++] = 0x17;   // EOP(End of Packet)

	if (drv_twi_write_burst (CYPRESS_ADDR, I2C_REG_OUTPUT_REP, &wrbuff[0], cnt) != NRF_SUCCESS) {
        DEBUG("I2C write failed!!");
		INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30
        return (-1);
    }

	//rv= cypress_Check_Interrupt_Routine(CY_PIN_LOW, 1000);	// 1ms wait must
	rv= is_TT_INT_low(1); // max 1-ms wait
	if(rv != NRF_SUCCESS)
	{
		DEB("#Error: TT_INT not LOW - cypress_Prog_Row() \n");
	}

	if (drv_twi_read (CYPRESS_ADDR, I2C_REG_BASE, rdbuff, 2) != NRF_SUCCESS) {
        DEBUG("I2C read failed!!");
		INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30
        return (-1);
    }

    bLength = rdbuff[0];

	if(bLength == 0x0B) {
		if (drv_twi_read (CYPRESS_ADDR, I2C_REG_BASE, rdbuff, bLength) != NRF_SUCCESS) {
			DEBUG("I2C read failed!!");
			INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30
			return (-1);
		}

		//rv= cypress_Check_Interrupt_Routine(CY_PIN_HIGH, 1000);	// 1ms wait must
		rv= is_TT_INT_high(1); // max 1-ms wait
		if(rv != NRF_SUCCESS)
		{
			DEB("#Error: TT_INT not HIGH - cypress_Prog_Row() \n");
		}

		if ( bLength == 0x0B && rdbuff[5] != 0x00 )
		{
			return (-1);
		}
	}
	else
	{
		//rv= cypress_Check_Interrupt_Routine(CY_PIN_HIGH, 1000);	// 1ms wait must
		rv= is_TT_INT_high(1); // max 1-ms wait
		if(rv != NRF_SUCCESS)
		{
			DEB("#Error: TT_INT not HIGH - cypress_Prog_Row() \n");
		}

	}
	//DEBUG("!!...!! low_num=%d, Len=%d", row_image->row_num, bLength);

	return (NRF_SUCCESS);
}

/*
================================================================================
   Function name : cypress_Verify_Checksum()
================================================================================
*/
static int cypress_Verify_Checksum(void)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
    uint8_t bLength;
    uint16_t caledCRC;
	ret_code_t rv;
    uint16_t cnt = 0;

    wrbuff[cnt++] = 0x00;
    wrbuff[cnt++] = 0x0B;   // Command Length(LSB)
    wrbuff[cnt++] = 0x00;   // Command Length(MSB)
    wrbuff[cnt++] = 0x40;   // Report ID
    wrbuff[cnt++] = 0x00;   // Reserved
    wrbuff[cnt++] = 0x01;   // SOP(Start of Packet)
    wrbuff[cnt++] = 0x31;   // Command
    wrbuff[cnt++] = 0x00;   // Data length(LSB)
    wrbuff[cnt++] = 0x00;   // Data length(MSB)

    caledCRC = cypress_Compute_CRC(&wrbuff[5], 4); // Calculate CRC value from SOP to end of meta data

    wrbuff[cnt++] = (uint8_t)(caledCRC >> 8);
    wrbuff[cnt++] = (uint8_t)caledCRC;
    wrbuff[cnt++] = 0x17;   // EOP(End of Packet)

	if (drv_twi_write_burst (CYPRESS_ADDR, I2C_REG_OUTPUT_REP, &wrbuff[0], cnt) != NRF_SUCCESS) {
        DEBUG("I2C write failed!!");
		INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30
        return (-1);
    }

	//rv= cypress_Check_Interrupt_Routine(CY_PIN_LOW, 1000);	// 1ms wait must
	rv= is_TT_INT_low(1); // max 1-ms wait
	if(rv != NRF_SUCCESS)
	{
		DEB("#Error: TT_INT not LOW - cypress_Verify_Checksum() \n");
	}

	if (drv_twi_read (CYPRESS_ADDR, I2C_REG_BASE, rdbuff, 2) != NRF_SUCCESS) {
        DEBUG("I2C read failed!!");
		INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30
        return (-1);
    }

    bLength = rdbuff[0];
	if (drv_twi_read (CYPRESS_ADDR, I2C_REG_BASE, rdbuff, bLength) != NRF_SUCCESS) {
        DEBUG("I2C read failed!!");
		INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30
        return (-1);
    }

	//rv= cypress_Check_Interrupt_Routine(CY_PIN_HIGH, 1000);	// 1ms wait must
	rv= is_TT_INT_high(1); // max 1-ms wait
	if(rv != NRF_SUCCESS)
	{
		DEB("#Error: TT_INT not HIGH - cypress_Verify_Checksum() \n");
	}
	

    if ( (rdbuff[5] != 0x00) && (rdbuff[8] != 0x01) )
    {
        return (-1);
    }

    return (NRF_SUCCESS);
}

/*
================================================================================
   Function name : cypress_Get_Row()
================================================================================
*/
static uint8_t *cypress_Get_Row(uint8_t *row_buf, uint8_t *image_buf, uint16_t size)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	memcpy(row_buf, image_buf, size);
	return (image_buf + size);
}

/*
================================================================================
   Function name : cypress_Parse_Row()
================================================================================
*/
static int cypress_Parse_Row(uint8_t *row_buf, struct cyttsp5_hex_image *row_image)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	row_image->array_id = row_buf[0];
	row_image->row_num = (row_buf[1] << 8) + row_buf[2];
	row_image->row_size = (row_buf[3] << 8) + row_buf[4];

	if ( (row_image->row_size) > CY_FLASH_BLOCK_SIZE) {
		return (-1);
	}

	memcpy(row_image->row_data, &row_buf[5], row_image->row_size);

	return (NRF_SUCCESS);
}

/*
================================================================================
   Function name : cypress_Compute_CRC()
================================================================================
*/
static uint16_t cypress_Compute_CRC(uint8_t *buf, uint16_t size)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	uint16_t remainder = 0xFFFF;
	uint16_t xor_mask = 0x0000;
	uint16_t index;
	uint16_t byte_value;
	uint16_t table_index;
	uint16_t crc_bit_width = sizeof(uint16_t) * 8;

	/* Divide the message by polynomial, via the table. */
	for (index = 0; index < size; index++)
    {
		byte_value = (uint16_t)buf[index];
		table_index = (uint16_t)((byte_value >> 4) & 0x0F) ^ (remainder >> (crc_bit_width - 4));
		remainder = (uint16_t)crc_table[table_index] ^ (remainder << 4);
		table_index = (uint16_t)(byte_value & 0x0F) ^ (remainder >> (crc_bit_width - 4));
		remainder = (uint16_t)crc_table[table_index] ^ (remainder << 4);
	}

	/* Perform the final remainder CRC. */
	return (uint16_t)(remainder ^ xor_mask);
}

/*
================================================================================
   Function name : cypress_Start_Bootloader()
================================================================================
*/
static ret_code_t cypress_Start_Bootloader(void)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	________________________________LOG_drvCypress_checkCypressUpdate(0,"");
    uint16_t cnt = 0;
	ret_code_t rv;
	wrbuff[cnt++] = 0x00;
	wrbuff[cnt++] = 0x05;
	wrbuff[cnt++] = 0x00;
	wrbuff[cnt++] = 0x2F;
	wrbuff[cnt++] = 0x00;
	wrbuff[cnt++] = 0x01;

	if (NRF_SUCCESS != drv_twi_write_burst (CYPRESS_ADDR, I2C_REG_OUTPUT_REP, &wrbuff[0], cnt)) 
	{
        DEBUG("I2C write failed!!");
		INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30
		return NRF_ERROR_INTERNAL;
    }

	//rv= cypress_Check_Interrupt_Routine(CY_PIN_LOW, 1000);	// 1ms wait must
	rv= is_TT_INT_low(1); // max 1-ms wait
	if(rv != NRF_SUCCESS)
	{
		DEB("#Error: TT_INT not LOW - cypress_Start_Bootloader() \n");
	}

	// Repeat Read HID Descriptor commands to verify the Report ID before proceeding.
	memset(rdbuff, 0, 2);
	if(NRF_SUCCESS != drv_twi_read (CYPRESS_ADDR, I2C_REG_BASE, rdbuff, 2)) 
	{
        DEBUG("I2C read failed!!");
		INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30
		return NRF_ERROR_INTERNAL;
    }

	//rv= cypress_Check_Interrupt_Routine(CY_PIN_HIGH, 1000);	// 1ms wait must
	rv= is_TT_INT_high(1); // max 1-ms wait
	if(rv != NRF_SUCCESS)
	{
		DEB("#Error: TT_INT not HIGH - cypress_Start_Bootloader() \n");
	}

    if ( (rdbuff[0] != 0x00) && (rdbuff[1] != 0x00) )
    {
        DEBUG("Response Packet Error!!");
		return NRF_ERROR_INTERNAL;
    }
	
	DEBUG("Start Bootloader Complated!");
    return (NRF_SUCCESS);
}

/*
================================================================================
   Function name : cypress_EnterBtld()
================================================================================
*/
static ret_code_t cypress_EnterBtld(void)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	________________________________LOG_drvCypress_checkCypressUpdate(0,"");
    ret_code_t ret;

    // Reset & Restart
    ret = cypress_Check_Reset_Sentinel();
    if (NRF_SUCCESS != ret)
    {
        ________________________________LOG_drvCypress_checkCypressUpdate(1,"RESET Sentinel Failed[%d]", ret);
		INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30
		return NRF_ERROR_INTERNAL;
    }

    // if running Application, then start bootloader
    if (DRV_CYPRESS_MODE_OPERATION == cypress_Get_HID_Descriptor())
    {
		________________________________LOG_drvCypress_checkCypressUpdate(1,"Running Application! go to start Bootloader");

        if (NRF_SUCCESS != cypress_Start_Bootloader())
        {
            ________________________________LOG_drvCypress_checkCypressUpdate(1,"TT Bootloader Start Failed!!");
			INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30
			return NRF_ERROR_INTERNAL;
        }
    }

    return (NRF_SUCCESS);
}




#endif	//#if CYPRESS_FW_INCLUDED




/*
================================================================================
   Function name : cypress_update()
================================================================================
*/
#if CYPRESS_FW_INCLUDED
static ret_code_t cypress_update(void)
{
	________________________________LOG_drvCypress_FuncHeads(FUNCHEAD_FLAG,"");
	________________________________LOG_drvCypress_checkCypressUpdate(0,"");
	struct cyttsp5_hex_image row_image;
	uint8_t row_buf[CY_DATA_MAX_ROW_SIZE] = {0};
	uint16_t image_rec_size;

    uint8_t *p;
	uint8_t *last_row;

	const uint8_t *fw;
	uint16_t fw_size;

	fw = (cyttsp4_img);
	fw_size = sizeof(cyttsp4_img);

	image_rec_size = sizeof(struct cyttsp5_hex_image);
 
	
	________________________________LOG_drvCypress_checkCypressUpdate(1,"Cypress update from image");
	________________________________LOG_drvCypress_checkCypressUpdate(1,"cyttsp4_img size = %d", sizeof(cyttsp4_img));
	________________________________LOG_drvCypress_checkCypressUpdate(1,"image_size = %d", fw_size);
    ________________________________LOG_drvCypress_checkCypressUpdate(1,"image_rec_size = %d", image_rec_size);
    ________________________________LOG_drvCypress_checkCypressUpdate(1,"Divide result = %d.%d", (fw_size / image_rec_size), (fw_size % image_rec_size) );
	

	if (fw_size % image_rec_size != 0) {
		#if defined(DRV_CYPRESS_UPG_TRACE)
		________________________________LOG_drvCypress_checkCypressUpdate(1,"Firmware image is misaligned");
		#endif
		return __LINE__;
	}
	
    uint32_t total_row = (fw_size / image_rec_size);
	
	________________________________LOG_drvCypress_checkCypressUpdate(1,"Total Row = %d", total_row);
	
	
	________________________________LOG_drvCypress_checkCypressUpdate(1,"Send BL Loader Enter");

	
	if (NRF_SUCCESS != cypress_EnterBtld()) 
	{
		________________________________LOG_drvCypress_checkCypressUpdate(1,"Error cannot start Loader");
		return __LINE__;
	}

	if (cypress_GetBootloaderInfo() != NRF_SUCCESS) {
		________________________________LOG_drvCypress_checkCypressUpdate(1,"Error cannot read System information");
		return __LINE__;
	}
	
	// Get last row
	last_row = (uint8_t *)fw + fw_size - image_rec_size;
    cypress_Get_Row(row_buf, last_row, image_rec_size);
	
	cypress_Parse_Row(row_buf, &row_image);
	
	
	________________________________LOG_drvCypress_checkCypressUpdate(1,"<< LAST row_image record infomation >>");
	________________________________LOG_drvCypress_checkCypressUpdate(1,"array_id: 0x%x", row_image.array_id);
	________________________________LOG_drvCypress_checkCypressUpdate(1,"row_num: %d", row_image.row_num);
	________________________________LOG_drvCypress_checkCypressUpdate(1,"row_size: %d", row_image.row_size);
	________________________________LOG_drvCypress_checkCypressUpdate(1,"row_data: 0x%x ~ 0x%x", row_image.row_data[0], row_image.row_data[127]);
	

	//-----------------------------------------------------------------------------------//
	// Init bootloader 여기까진 잘 온ㄴ듯.
	if (cypress_BootLoader_INIT(row_image.row_size, row_image.row_data) != NRF_SUCCESS) {
		________________________________LOG_drvCypress_checkCypressUpdate(1,"Error cannot init Loader");
		return __LINE__;
	}
	
	//-----------------------------------------------------------------------------------//
	// Send bootloader block
	#if defined(DRV_CYPRESS_UPG_TRACE)
	________________________________LOG_drvCypress_checkCypressUpdate(1,"Send BL Loader Blocks");
	#endif
	
	p = (uint8_t *)fw;
	while (p < last_row) {
		/* Get row */
		memset(row_buf, 0, CY_DATA_MAX_ROW_SIZE);

		p = cypress_Get_Row(row_buf, p, image_rec_size);

		/* Parse row */
		if (cypress_Parse_Row(row_buf, &row_image) != NRF_SUCCESS) {
			________________________________LOG_drvCypress_checkCypressUpdate(1,"Error Parse Row");
			return __LINE__;
		}

		/* program row */
		if (cypress_Prog_Row(&row_image) != NRF_SUCCESS) {
			________________________________LOG_drvCypress_checkCypressUpdate(1,"Error Program Row");
			return __LINE__;
		}
	}
	//DEBUG("Send Total Row = %d", total_row);

	//-----------------------------------------------------------------------------------//
	// Exit loader //
	#if defined(DRV_CYPRESS_UPG_TRACE)
    ________________________________LOG_drvCypress_checkCypressUpdate(1,"Send BL Loader Finished!!");
	#endif

    if (cypress_LaunchApp() != NRF_SUCCESS) {
    	________________________________LOG_drvCypress_checkCypressUpdate(1,"Error on exit Loader");

    	/* verify app checksum */
    	
    	if (cypress_Verify_Checksum() != NRF_SUCCESS)
    	{
    		________________________________LOG_drvCypress_checkCypressUpdate(1,"Loader_verify_checksum fail!!");
			INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30
			return __LINE__;
        }
    	else
    	{
			#if defined(DRV_CYPRESS_UPG_TRACE)
    		________________________________LOG_drvCypress_checkCypressUpdate(1,"APP Checksum Verified!!");
			#endif
    	}
    }

	return NRF_SUCCESS;
}
#endif






#if 0 ////////////
/*
================================================================================
   Function name : cypress_Check_Interrupt_Routine()
================================================================================
*/
static ret_code_t cypress_Check_Interrupt_Routine(uint8_t pinLevel, uint32_t us)
{
	uint8_t pinValue=0;
	
	int32_t timeout = us / 100; // signed... by Stanley
	//
	do {
		pinValue= nrf_gpio_pin_read(CFG_MOKIBO_TOUCH_INT_PIN);	// P.30
		//DEB(" gpio pin(TT_INT): 0x%x\n",pinValue);
		
		if(pinValue == pinLevel) break;
		//
		nrf_delay_us(100);
		timeout--;
	} while(timeout > 0);
		
	if(timeout <= 0)
	{
		return (NRF_ERROR_INVALID_STATE);
	}
	return NRF_SUCCESS;
}

#endif



#if 0
/*
================================================================================
   Function name : cypress_infor()
================================================================================
*/
static void cypress_infor(uint8_t *data)
{
	DEBUGS ("PIP Major Version: %d", (int)data[0]);
	DEBUGS ("PIP Minor Version: %d", (int)data[1]);
	DEBUGS ("Touch Firmware Product Id: %d", (int)data[2]);
	DEBUGS ("Touch Firmware Major Version: %d", (int)data[4]);
	DEBUGS ("Touch Firmware Minor Version: %d", (int)data[5]);
	DEBUGS ("Touch Firmware Internal Revision Control Number: %d", (int)data[6]);
	DEBUGS ("Customer Specified Firmware/Configuration Version: %d", (int)data[10]);
	DEBUGS ("Bootloader Major Version: %d", (int)data[12]);
	DEBUGS ("Bootloader Minor Version: %d", (int)data[13]);
	DEBUGS ("Family ID: 0x%02x", (int)data[14]);
	DEBUGS ("Revision ID: 0x%02x", (int)data[15]);
	DEBUGS ("Silicon ID: 0x%02x", (int)data[16]);
	DEBUGS ("Cypress Manufacturing ID[0]: 0x%02x", (int)data[18]);
	DEBUGS ("Cypress Manufacturing ID[1]: 0x%02x", (int)data[19]);
	DEBUGS ("Cypress Manufacturing ID[2]: 0x%02x", (int)data[20]);
	DEBUGS ("Cypress Manufacturing ID[3]: 0x%02x", (int)data[21]);
	DEBUGS ("Cypress Manufacturing ID[4]: 0x%02x", (int)data[22]);
	DEBUGS ("Cypress Manufacturing ID[5]: 0x%02x", (int)data[23]);
	DEBUGS ("Cypress Manufacturing ID[6]: 0x%02x", (int)data[24]);
	DEBUGS ("Cypress Manufacturing ID[7]: 0x%02x", (int)data[25]);
	DEBUGS ("POST Result Code: 0x%02x", (int)data[26]);

	DEBUGS ("Number of X Electrodes: %d", (int)data[28]);
	DEBUGS ("Number of Y Electrodes: %d", (int)data[29]);
	DEBUGS ("Panel X Axis Length: %d", (int)data[30]);
	DEBUGS ("Panel Y Axis Length: %d", (int)data[32]);
	DEBUGS ("Panel X Axis Resolution: %d", (int)data[34]);
	DEBUGS ("Panel Y Axis Resolution: %d", (int)data[36]);
	DEBUGS ("Panel Pressure Resolution: %d", (int)data[38]);
	DEBUGS ("X_ORG: %d", (int)data[40]);
	DEBUGS ("Y_ORG: %d", (int)data[41]);
	DEBUGS ("Panel ID: %d", (int)data[42]);
	DEBUGS ("Buttons: 0x%02x", (int)data[43]);
	DEBUGS ("BAL SELF MC: 0x%02x", (int)data[44]);
	DEBUGS ("Max Number of Touch Records per Refresh Cycle: %d", (int)data[45]);
}

/*
================================================================================
   Function name : cypress_get_system_infor()
================================================================================
*/
static ret_code_t cypress_get_system_infor(void)
{
	uint8_t buf[64] = {0, };
	uint32_t packet_length = 0;

	// Read System Information Command
	buf[0] = 0x04;
	buf[1] = 0x00;
	buf[2] = 0x05;
	buf[3] = 0x00;
	buf[4] = 0x2F;
	buf[5] = 0x00;
	buf[6] = 0x02;
	
	if (drv_twi_write_burst (CYPRESS_ADDR, buf[0], &buf[1], 6) != NRF_SUCCESS) {
		DEBUGS ("Error! System infor cmd write failed.");
		INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30
		return NRF_ERROR_INTERNAL;
	} else {
		DEBUGS ("System infor cmd write OK.");
	}

	// System Information Command Response (packet length)
	if (drv_twi_read (CYPRESS_ADDR, 0x0, buf, 3) != NRF_SUCCESS) {
		DEBUGS ("Error! System infor cmd response read failed.");
		INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30
		return NRF_ERROR_INTERNAL;
	} else {
		DEBUGS ("System infor cmd response : 0x%02x, 0x%02x, 0x%02x", buf[0], buf[1], buf[2]);
	}

	// 51 Byte System Information Read
//	packet_length = (buf[1] << 8) | buf[0];
  	packet_length = 51;
	if (drv_twi_read (CYPRESS_ADDR, 0x0, buf, packet_length) != NRF_SUCCESS) {
		DEBUGS ("Error! System infor read failed.");
		INIT_I2C_COMM(); // <--- Added by Stanley 2018.9.27 19:30
		return NRF_ERROR_INTERNAL;
	} else {
		DEBUGS ("System infor read read OK. length=%d", packet_length);
//		for (int i=0; i<packet_length; i++) {
//			DEBUGS ("0x%02X,", buf[i]);
//		}
		DEBUGS ("");
	}

	cypress_infor(&buf[5]);

	return NRF_SUCCESS;
}



#if 0 //////////////////////////  delete drv_cypress_read_sentinel() /////////////////////
//-----------------------------------------------------------------------------------------------------------------------------------
//			drv_cypress_read_sentinel
//-----------------------------------------------------------------------------------------------------------------------------------
//	The reset sentinel is the first response sent by the TrueTouch device following every device reset. 
//	Before communication can proceed, the reset sentinel must be read. 
//	The value of the reset sentinel equals 0x0000.
//
static ret_code_t drv_cypress_read_sentinel(void)
{	
	ret_code_t rv;

	// is the TT_INT low ?
	//rv = cypress_Check_Interrupt_Routine(CY_PIN_LOW, 1000);	// 1ms wait must
	rv= is_TT_INT_low(1); // max 1-ms wait
	if(rv != NRF_SUCCESS)
	{
		// Stanley.
		DEB("#Error: TT_INT is not LOW - cypress_Check_Reset_Sentinel()\n");
	}

	// buffer for the next reading
	memset(rdbuff, 0, 2);
	
	// is the sentinel data 0x0000 ?
	//					0x24			0x00	  rdbuff[]
	rv = drv_twi_read(CYPRESS_ADDR, I2C_REG_BASE, rdbuff, 2);
	if (rv != NRF_SUCCESS) 
	{
		DEB("I2C read failed - cypress_Check_Reset_Sentinel()\n");
		return NRF_ERROR_INTERNAL;
	} 

	// is the TT_INT high?
	//rv = cypress_Check_Interrupt_Routine(CY_PIN_HIGH, 1000);	// 1ms wait must
	rv= is_TT_INT_high(1); // max 1-ms wait
	if(rv != NRF_SUCCESS)
	{
		DEB("#Error: TT_INT is not HIGH - cypress_Check_Reset_Sentinel()\n");
	}

	//DEB("Read Reset Sentinel: sentinel[0]=0x%x, sentinel[1]=0x%x \n", rdbuff[0], rdbuff[1]);

	// the sentinel data should be 0x0000 ...
    if ( (rdbuff[0] != 0x00) && (rdbuff[1] != 0x00) )
    {
		DEB("Reset Sentinel check failed - cypress_Check_Reset_Sentinel.\n");
		return NRF_ERROR_INTERNAL;
    }
	//
	return NRF_SUCCESS;
}
#endif /////////////////////////////// delete drv_cypress_read_sentinel() /////////////////////////





#endif












