/// \file drv_keboard.c
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/










/*\      $$\  $$$$$$\  $$$$$$$\  $$\   $$\ $$\       $$$$$$$$\  $$$$$$\  
$$$\    $$$ |$$  __$$\ $$  __$$\ $$ |  $$ |$$ |      $$  _____|$$  __$$\ 
$$$$\  $$$$ |$$ /  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$ /  \__|
$$\$$\$$ $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$$$$\    \$$$$$$\  
$$ \$$$  $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$  __|    \____$$\ 
$$ |\$  /$$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$\   $$ |
$$ | \_/ $$ | $$$$$$  |$$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$$\ \$$$$$$  |
\__|     \__| \______/ \_______/  \______/ \________|\________| \______/ 
//============================================================================//
//                              LOCAL MODULEL USED                            //
//============================================================================*/
#include "sdk_config.h" // mokibo_config.h && sdk_config.h
#if (MOKIBO_DEBUG == MOKIBO_DEBUG_ON)
	// should be placed after "sdk_config.h"
	#include "SEGGER_RTT.h"
#endif
#include "sdk_errors.h"        
#include "drv_keyboard.h"
#include "nrf_gpio.h"
#include "nrf_log.h"
#include "nrf_delay.h"
#include "m_timer.h"
#include "m_led.h"
#include "drv_keyboard.h"
#include "m_flimbs.h"
#ifdef FLIMBS_20190110_CHANGE_LUI_LALT_ON_CENTRAL_MAC
#include "m_comm.h"
#endif






/*$$$$\   $$$$$$\  $$\   $$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$\   $$\ $$$$$$$$\ 
$$  __$$\ $$  __$$\ $$$\  $$ |$$  __$$\\__$$  __|$$  __$$\ $$$\  $$ |\__$$  __|
$$ /  \__|$$ /  $$ |$$$$\ $$ |$$ /  \__|  $$ |   $$ /  $$ |$$$$\ $$ |   $$ |   
$$ |      $$ |  $$ |$$ $$\$$ |\$$$$$$\    $$ |   $$$$$$$$ |$$ $$\$$ |   $$ |   
$$ |      $$ |  $$ |$$ \$$$$ | \____$$\   $$ |   $$  __$$ |$$ \$$$$ |   $$ |   
$$ |  $$\ $$ |  $$ |$$ |\$$$ |$$\   $$ |  $$ |   $$ |  $$ |$$ |\$$$ |   $$ |   
\$$$$$$  | $$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$ |  $$ |$$ | \$$ |   $$ |   
 \______/  \______/ \__|  \__| \______/   \__|   \__|  \__|\__|  \__|   \__|   
//============================================================================//
//                                LOCAL CONSTANTS                             //
//============================================================================*/








/*$$$$$$\ $$\     $$\ $$$$$$$\  $$$$$$$$\ $$$$$$$\  $$$$$$$$\ $$$$$$$$\  $$$$$$\  
\__$$  __|\$$\   $$  |$$  __$$\ $$  _____|$$  __$$\ $$  _____|$$  _____|$$  __$$\ 
   $$ |    \$$\ $$  / $$ |  $$ |$$ |      $$ |  $$ |$$ |      $$ |      $$ /  \__|
   $$ |     \$$$$  /  $$$$$$$  |$$$$$\    $$ |  $$ |$$$$$\    $$$$$\    \$$$$$$\  
   $$ |      \$$  /   $$  ____/ $$  __|   $$ |  $$ |$$  __|   $$  __|    \____$$\ 
   $$ |       $$ |    $$ |      $$ |      $$ |  $$ |$$ |      $$ |      $$\   $$ |
   $$ |       $$ |    $$ |      $$$$$$$$\ $$$$$$$  |$$$$$$$$\ $$ |      \$$$$$$  |
   \__|       \__|    \__|      \________|\_______/ \________|\__|       \______/ 
//============================================================================//
//                                LOCAL TYPEDEFS                              //
//============================================================================*/
typedef struct
{
    uint8_t  now 		 				: 1;
    uint8_t  prv 		 				: 1;
    uint8_t  long_started 	: 1;	// Long push has started
    uint8_t  evt_active	 		: 1;
    uint8_t  long_evt_active: 1;
    uint8_t  rsvd 		 			: 3;
} key_bf_t; // key bitfield

typedef union 
{
	key_bf_t	bit;
	uint8_t     data;
} key_t;

typedef struct
{
	uint8_t					input;
	uint8_t					attr;
	uint32_t 				hotkey;
	uint32_t 				long_hotkey;
	M_TIMER_ID_TYPE	timer_id;
	uint32_t 				duration;
} hot_key_t;

typedef struct
{
	uint8_t		org;
	uint8_t		new;
} fn_key_t;









/*\    $$\  $$$$$$\  $$$$$$$\  $$$$$$\  $$$$$$\  $$$$$$$\  $$\       $$$$$$$$\ 
$$ |   $$ |$$  __$$\ $$  __$$\ \_$$  _|$$  __$$\ $$  __$$\ $$ |      $$  _____|
$$ |   $$ |$$ /  $$ |$$ |  $$ |  $$ |  $$ /  $$ |$$ |  $$ |$$ |      $$ |      
\$$\  $$  |$$$$$$$$ |$$$$$$$  |  $$ |  $$$$$$$$ |$$$$$$$\ |$$ |      $$$$$\    
 \$$\$$  / $$  __$$ |$$  __$$<   $$ |  $$  __$$ |$$  __$$\ $$ |      $$  __|   
  \$$$  /  $$ |  $$ |$$ |  $$ |  $$ |  $$ |  $$ |$$ |  $$ |$$ |      $$ |      
   \$  /   $$ |  $$ |$$ |  $$ |$$$$$$\ $$ |  $$ |$$$$$$$  |$$$$$$$$\ $$$$$$$$\ 
    \_/    \__|  \__|\__|  \__|\______|\__|  \__|\_______/ \________|\_______/
//============================================================================//
//                                   VARIABLES                                //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                                GLOBAL VARIABLES                            //
//----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------//
//                                 LOCAL VARIABLES                            //
//----------------------------------------------------------------------------*/

// output gpio array
static const uint8_t columns[KEYBOARD_NUM_OF_COLUMNS] = \
	{8, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24};

// input gpio array
static const uint8_t rows[KEYBOARD_NUM_OF_ROWS] = \
	{0, 1, 2, 3, 4, 5, 6, 7};		

//!These pointers deside which part of the keys_buffer  
static uint8_t prv_keys_buf[MAX_NUM_OF_KEYS];		

//!saves the current keys and which saves the transmitted one
static uint8_t keys_buf[MAX_NUM_OF_KEYS];	

//!< Number of keys in prv_keys_buf
static uint8_t prv_keys_n;		

//!< Number of keys in keys_buf
static uint8_t pressed_keys_n;	

//!A trick to quickly detecte if any keys is pressed
static uint32_t input_scan_vector;			

//!< Stores last created key packet. One byte is used for modifier keys, one for OEMs. Key values are USB HID keycodes.
static uint8_t m_key_buffer[PACKET_SIZE]; 

// hot key status 
static uint32_t m_hotkey = DRV_KEYBOARD_HOTKEY_NONE;


static const app_usbd_hid_kbd_modifier_t m_modifier_table[] = 
{
    APP_USBD_HID_KBD_MODIFIER_LEFT_CTRL,
    APP_USBD_HID_KBD_MODIFIER_LEFT_SHIFT,
    APP_USBD_HID_KBD_MODIFIER_LEFT_ALT,
    APP_USBD_HID_KBD_MODIFIER_LEFT_UI,
    APP_USBD_HID_KBD_MODIFIER_RIGHT_CTRL,
    APP_USBD_HID_KBD_MODIFIER_RIGHT_SHIFT,
    APP_USBD_HID_KBD_MODIFIER_RIGHT_ALT,
    APP_USBD_HID_KBD_MODIFIER_RIGHT_UI,
};

// hot key for return
static const hot_key_t m_hotkey_table[] = 
{
// 참조:
//		S2L(KKK)는  APP_USBD_HID_KBD_KKK 와 같다.
//		HOTK(JJJ)는 DRV_KEYBOARD_HOTKEY_JJJ 와 같다.
//	  EDGE(TTT)는 TTT_EDGE와 같다.
//		M_TMR(MMM)은 M_TIMER_ID_MMM과 같다.
//			#define P_EDGE		0x01							// push edge(release --> push)
//			#define R_EDGE		0x02							// release edge(push --> release)
//			#define LONG_KEY	0x04							// LONG PRESS KEY
//			#define A_EDGE		(P_EDGE|R_EDGE)		// any edge
//			#define RL_EDGE		(R_EDGE|LONG_KEY)	// release + long
//			#define L_EDGE		(LONG_KEY)				// long
//														//---------------	//-----------------
//  input	  				attr 			hot_key value			long press hot_key 	timer id                			duration
	{ S2L(RESET),			EDGE(P),	HOTK(RESET), 		HOTK(NONE), 			M_TMR(INVALID), 					IMMEDIATELY						},
	{ S2L(ERASEBOND),		EDGE(P),	HOTK(ERASE_BOND),	HOTK(NONE), 			M_TMR(INVALID), 					IMMEDIATELY						},
	{ S2L(HOUSE),			EDGE(P),	HOTK(HOUSE), 		HOTK(NONE), 			M_TMR(INVALID), 					IMMEDIATELY						},
	{ S2L(BT1),				EDGE(RL),	HOTK(BT1), 			HOTK(BT1_NEW), 			M_TMR(BT1_LONG_PRESS), 				BT_LONG_PRESS_TIME		},
	{ S2L(BT2),				EDGE(RL),	HOTK(BT2), 			HOTK(BT2_NEW), 			M_TMR(BT2_LONG_PRESS), 				BT_LONG_PRESS_TIME 		},
	{ S2L(BT3),				EDGE(RL),	HOTK(BT3), 			HOTK(BT3_NEW), 			M_TMR(BT3_LONG_PRESS), 				BT_LONG_PRESS_TIME 		},
	{ S2L(DONGLE),			EDGE(P),	HOTK(DONGLE), 		HOTK(NONE), 			M_TMR(DONGLE_LONG_PRESS),			BT_LONG_PRESS_TIME 		},
	{ S2L(BTRCHK),			EDGE(P),	HOTK(BTRCHK), 		HOTK(NONE), 			M_TMR(INVALID), 					IMMEDIATELY						},
	{ S2L(SWTAPP),			EDGE(P),	HOTK(SWTAPP), 		HOTK(NONE), 			M_TMR(INVALID), 					IMMEDIATELY						},
	{ S2L(VKBD),			EDGE(P),	HOTK(VKBD), 		HOTK(NONE), 			M_TMR(INVALID), 					IMMEDIATELY						},
	{ S2L(FIND),			EDGE(P),	HOTK(FIND), 		HOTK(NONE), 			M_TMR(INVALID), 					IMMEDIATELY						},
//A->P
	{ S2L(V_MUTE),			EDGE(A),	HOTK(V_MUTE), 			HOTK(NONE), 		M_TMR(INVALID), 					IMMEDIATELY						},
	{ S2L(V_UP),			EDGE(A),	HOTK(V_UP), 		HOTK(NONE), 			M_TMR(INVALID), 					IMMEDIATELY						},
	{ S2L(V_DOWN),			EDGE(A),	HOTK(V_DOWN), 		HOTK(NONE), 			M_TMR(INVALID), 					IMMEDIATELY						},
	{ S2L(CAPTURE),			EDGE(P),	HOTK(CAPTURE), 		HOTK(NONE), 			M_TMR(INVALID),						IMMEDIATELY						},
	{ S2L(RESET),			EDGE(P),	HOTK(RESET), 	HOTK(NONE), 			M_TMR(INVALID), 					IMMEDIATELY						},
	{ S2L(CERT_MODE),		EDGE(R),	HOTK(CERT_MODE), 	HOTK(NONE), 			M_TMR(INVALID), 					IMMEDIATELY						},
	{ S2L(CENTRAL_WINDOWS),	EDGE(P),	HOTK(CENTRAL_WINDOWS), 	HOTK(NONE), 		M_TMR(INVALID), 					IMMEDIATELY						},
	{ S2L(CENTRAL_ANDROID),	EDGE(P),	HOTK(CENTRAL_ANDROID), 	HOTK(NONE), 		M_TMR(INVALID), 					IMMEDIATELY						},
	{ S2L(CENTRAL_MAC),		EDGE(P),	HOTK(CENTRAL_MAC), 		HOTK(NONE), 		M_TMR(INVALID), 					IMMEDIATELY						},
	{ S2L(BACKWARD),		EDGE(P),	HOTK(BACKWARD), 		HOTK(NONE), 		M_TMR(INVALID), 					IMMEDIATELY						},
	{ S2L(LOCKSCREEN),		EDGE(P),	HOTK(LOCKSCREEN), 		HOTK(NONE), 		M_TMR(INVALID), 					IMMEDIATELY						},
	{ S2L(TOUCHLOCK),		EDGE(P),	HOTK(TOUCHLOCK), 		HOTK(NONE), 		M_TMR(INVALID), 					IMMEDIATELY						},	
	{ S2L(LANGUAGE_Z),		EDGE(P),	HOTK(LANGUAGE_Z), 			HOTK(NONE), 		M_TMR(INVALID), 					IMMEDIATELY						},
	{ S2L(LANGUAGE_X),		EDGE(P),	HOTK(LANGUAGE_X), 			HOTK(NONE), 		M_TMR(INVALID), 					IMMEDIATELY						},
	{ S2L(LANGUAGE_C),		EDGE(P),	HOTK(LANGUAGE_C), 			HOTK(NONE), 		M_TMR(INVALID), 					IMMEDIATELY						},
	{ S2L(CENTRAL_IOS),		EDGE(P),	HOTK(CENTRAL_IOS), 		HOTK(NONE), 		M_TMR(INVALID), 					IMMEDIATELY						},
};


static key_t m_hot_keys[M_HOTKEY_TABLE_MAX];

static const fn_key_t m_fn_keys_table[] = 
{ 
	// 참조:
	//		S2L(KKK)는  APP_USBD_HID_KBD_KKK 와 같다.
	//
	// function key detection table
	//		(key)					(Fn+key)
	{	S2L(ESC), 		S2L(HOUSE)			}, 
	{ 	S2L(F1), 		S2L(BT1)			}, 
	{ 	S2L(F2), 		S2L(BT2)			}, 
	{ 	S2L(F3), 		S2L(BT3)			},

	{ 	S2L(F4),		S2L(CENTRAL_WINDOWS)}, 
	{ 	S2L(F5),		S2L(CENTRAL_ANDROID)}, 
	{ 	S2L(F6),		S2L(CENTRAL_IOS)}, 

	{ 	S2L(F7), 		S2L(V_MUTE)		}, 
	{ 	S2L(F8),		S2L(V_DOWN)		}, 
	{ 	S2L(F9), 		S2L(V_UP)		}, 

	{ 	S2L(F10),		S2L(BTRCHK)		}, 
	{ 	S2L(F11),		S2L(CAPTURE)	}, 
	{ 	S2L(F12),		S2L(INSERT)		}, 

	{ 	S2L(UP), 		S2L(PAGEUP)		}, 
	{ 	S2L(DOWN), 		S2L(PAGEDOWN)	},
	{ 	S2L(LFT), 		S2L(HOME)		}, 
	{ 	S2L(RGHT), 		S2L(END)		}, 	

	{	S2L(0),			S2L(RESET)},

	{	S2L(TILDE),		S2L(FIND)		}, 
	{	S2L(TAB),		S2L(SWTAPP)		}, 
	{	S2L(BS),		S2L(BACKWARD)		}, 
	{	S2L(BSLH),		S2L(VKBD)		}, 
	{	S2L(DELETE),	S2L(LOCKSCREEN)		}, 
	{	S2L(CAPS),		S2L(TOUCHLOCK)		}, 
	{	S2L(Z), 		S2L(LANGUAGE_Z)	},
	{	S2L(X), 		S2L(LANGUAGE_X)	},
	{	S2L(C), 		S2L(LANGUAGE_C)	},
	{	S2L(INTERNATIONAL4), 		S2L(INTERNATIONAL5)	},
	{	S2L(M),			S2L(CENTRAL_MAC)}
};


/** Table containing the mapping between the key matrix and the HID Usage codes for each key. */
// Please refer to "R.MOK.FW.18.keyboard_map_v1.5.xlsx"

#if LAYOUT_OPTION == LAYOUT_OPTION_UNIQ
	static const uint8_t m_lookup[KEYBOARD_NUM_OF_COLUMNS * KEYBOARD_NUM_OF_ROWS] =
	{
		//0x87 international 1: Key_Ro
		//0x88 international 2: KatakanaHiragana
		//0x89 international 3: Key_Yen
		//0x8A international 4: Henkan 변환
		//0x8B international 5: Muhenkan 무변환
		//0x8C International 6: Japan Comma
		//0x90 Lang1 : Hangeul
		//0x91 Lang2 : Hanja
		//0x92 Lang3 : Katakna
		//0x93 Lang4 : Hiragana
		//0x94 Lang5 : Zenkakuhankaku

		//RAlt (한영Cmd) -> International 4 (+Fn:International5) : 
		//Rctrl(한자Opt) -> International 3 
		//Rfunc -> International 1
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// COLUMN(GPIO OUTPUT)
		// 8	     	11			12 			13			14				15			16			17				18				19			20			21		22			23			24
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// 참조:	S2L(KKK)는  APP_USBD_HID_KBD_KKK 와 같다.
		XXXX,			S2L(LFT),	S2L(F7),	S2L(F6),	XXXX,			S2L(L_CTRL),		XXXX,		S2L(INTERNATIONAL4),S2L(M),			S2L(BSLH),	S2L(SB),	XXXX,	S2L(L_UI),			S2L(L_ALT),	FUNC,      // GPIO Input 0
		XXXX,			S2L(DOWN),	S2L(1),		S2L(CAPS),	XXXX,			XXXX,				XXXX,		XXXX,				S2L(COMMA),		S2L(B),		S2L(C),		S2L(X),	S2L(INTERNATIONAL3),XXXX,		XXXX,      // GPIO Input 1
		S2L(UP),		S2L(RGHT),	S2L(F3),	S2L(F2),	S2L(DOT),		S2L(INTERNATIONAL1),S2L(L_SHFT),XXXX,				S2L(K),			S2L(N),		S2L(V),		S2L(D),	XXXX,				XXXX,		S2L(Z),    // GPIO Input 2
		S2L(2),			XXXX,		S2L(F4),	S2L(F1),	S2L(SLASH),		XXXX,				S2L(R_SHFT),XXXX,				S2L(L),			S2L(J),		S2L(G),		S2L(F),	XXXX,				XXXX,		S2L(A),    // GPIO Input 3
		S2L(3),			XXXX,		S2L(Q),		S2L(TAB),	S2L(COLON),		XXXX,				XXXX,		S2L(ENTER),			S2L(I),			S2L(Y),		S2L(H),		S2L(E),	XXXX,				XXXX,		S2L(S),    // GPIO Input 4
		S2L(BS),		XXXX,		S2L(F8),	S2L(F5),	S2L(QUOTE),		XXXX,				XXXX,		S2L(C_BRCKT),		S2L(O),			S2L(U),		S2L(T),		S2L(R),	XXXX,				XXXX,		S2L(W),    // GPIO Input 5
		S2L(F9),		S2L(DELETE),S2L(F12),S2L(TILDE),	S2L(P),			XXXX,				XXXX,		S2L(PLUS),			S2L(0),			S2L(8),		S2L(6),		S2L(4),	XXXX,				XXXX,		XXXX,      // GPIO Input 6
		S2L(F10),		XXXX,		S2L(F11),	S2L(ESC),	S2L(O_BRCKT),	XXXX,				XXXX,		XXXX,				S2L(U_SCORE),	S2L(9),		S2L(7),		S2L(5),	XXXX,				XXXX,		XXXX       // GPIO Input 7
	};
#else
	static const uint8_t m_lookup[KEYBOARD_NUM_OF_COLUMNS * KEYBOARD_NUM_OF_ROWS] =
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// COLUMN(GPIO OUTPUT)
		// 8	     	11			12 			13			14				15			16			17				18				19			20			21		22			23			24
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// 참조:	S2L(KKK)는  APP_USBD_HID_KBD_KKK 와 같다.
		XXXX,			S2L(LFT),	S2L(F7),	S2L(F6),	XXXX,			S2L(L_CTRL),		XXXX,		S2L(R_ALT),			S2L(M),			S2L(BSLH),	S2L(SB),	XXXX,	S2L(L_UI),			S2L(L_ALT),	FUNC,      // GPIO Input 0
		XXXX,			S2L(DOWN),	S2L(1),		S2L(CAPS),	XXXX,			XXXX,				XXXX,		XXXX,				S2L(COMMA),		S2L(B),		S2L(C),		S2L(X),	S2L(R_CTRL),		XXXX,		XXXX,      // GPIO Input 1
		S2L(UP),		S2L(RGHT),	S2L(F3),	S2L(F2),	S2L(DOT),		FUNC,				S2L(L_SHFT),XXXX,				S2L(K),			S2L(N),		S2L(V),		S2L(D),	XXXX,				XXXX,		S2L(Z),    // GPIO Input 2
		S2L(2),			XXXX,		S2L(F4),	S2L(F1),	S2L(SLASH),		XXXX,				S2L(R_SHFT),XXXX,				S2L(L),			S2L(J),		S2L(G),		S2L(F),	XXXX,				XXXX,		S2L(A),    // GPIO Input 3
		S2L(3),			XXXX,		S2L(Q),		S2L(TAB),	S2L(COLON),		XXXX,				XXXX,		S2L(ENTER),			S2L(I),			S2L(Y),		S2L(H),		S2L(E),	XXXX,				XXXX,		S2L(S),    // GPIO Input 4
		S2L(BS),		XXXX,		S2L(F8),	S2L(F5),	S2L(QUOTE),		XXXX,				XXXX,		S2L(C_BRCKT),		S2L(O),			S2L(U),		S2L(T),		S2L(R),	XXXX,				XXXX,		S2L(W),    // GPIO Input 5
		S2L(F9),		S2L(DELETE),S2L(F12),S2L(TILDE),	S2L(P),			XXXX,				XXXX,		S2L(PLUS),			S2L(0),			S2L(8),		S2L(6),		S2L(4),	XXXX,				XXXX,		XXXX,      // GPIO Input 6
		S2L(F10),		XXXX,		S2L(F11),	S2L(ESC),	S2L(O_BRCKT),	XXXX,				XXXX,		XXXX,				S2L(U_SCORE),	S2L(9),		S2L(7),		S2L(5),	XXXX,				XXXX,		XXXX       // GPIO Input 7
	};
#endif


/*----------------------------------------------------------------------------//
//                                 DEBUG VARIABLES                            //
//----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------//
//                                UNUSED VARIABLES                            //
//----------------------------------------------------------------------------*/






/*$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\  $$\   $$\ $$$$$$$$\  $$$$$$\  $$$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\ $$ |  $$ |$$  _____|$$  __$$\ $$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|$$ |  $$ |$$ |      $$ /  $$ |$$ |  $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |      $$$$$$$$ |$$$$$\    $$$$$$$$ |$$ |  $$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |      $$  __$$ |$$  __|   $$  __$$ |$$ |  $$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\ $$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |$$ |  $$ |$$$$$$$$\ $$ |  $$ |$$$$$$$  |
\__|       \______/ \__|  \__| \______/ \__|  \__|\________|\__|  \__|\_______/ 
//============================================================================//
//                                   FUNCHEAD                                 //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                            GLOBAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
ret_code_t drv_keyboard_init(drv_keyboard_event_handler_t handler);
bool drv_keyboard_new_packet(const uint8_t **p_packet, uint8_t *p_size);
void drv_keyboard_get_packet(uint8_t *p_packet, uint8_t *p_size);
uint32_t drv_keyboard_get_hotkey(void);
void drv_keyboard_prepare_sleep(void);
uint8_t drv_keyboard_get_volume(uint8_t input);
bool drv_keyboard_is_pressed(uint8_t input);
void drv_release_fn_del_colums(void);



/*----------------------------------------------------------------------------//
//                             LOCAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
static bool is_changed(void);
static bool read(uint8_t *buf, uint8_t *num);
static void fn_remap(uint8_t *keys, uint8_t num);
static void scan_hot_key(const uint8_t *buf, uint8_t len);
static void init_hot_key(void);
static void create(uint8_t *packet, uint8_t size);
static void add(uint8_t *buf, uint8_t key);

/*----------------------------------------------------------------------------//
//                            POINTER FUNCTION HEADS                          //
//----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------//
//                             DEBUG FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------//
//                             UNUSED FUNCTION HEADS                          //
//----------------------------------------------------------------------------*/























/*\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ 
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\_*/




















/*
 $$$$$$\  $$\       $$$$$$\  $$$$$$$\   $$$$$$\  $$\                                   
$$  __$$\ $$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                                  
$$ /  \__|$$ |     $$ /  $$ |$$ |  $$ |$$ /  $$ |$$ |                                  
$$ |$$$$\ $$ |     $$ |  $$ |$$$$$$$\ |$$$$$$$$ |$$ |                                  
$$ |\_$$ |$$ |     $$ |  $$ |$$  __$$\ $$  __$$ |$$ |                                  
$$ |  $$ |$$ |     $$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |                                  
\$$$$$$  |$$$$$$$$\ $$$$$$  |$$$$$$$  |$$ |  $$ |$$$$$$$$\                             
 \______/ \________|\______/ \_______/ \__|  \__|\________|                            
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\  $$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |$$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |$$ /  \__|
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |\$$$$$$\  
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ | \____$$\ 
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |$$\   $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |\$$$$$$  |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__| \______/ 
//============================================================================//
//                                 GLOBAL FUNCTIONS                           //
//============================================================================*/




/*==============================================================================//
   Function name : drv_keyboard_init()
//==============================================================================*/
ret_code_t drv_keyboard_init(drv_keyboard_event_handler_t keyboard_event_handler)
{
	________________________________LOG_drvKeyboard_FuncHeads(FUNCHEAD_FLAG, "");


	// Init local variables
	input_scan_vector 		= 0;

	// return if pins have not been defined
	if ( (0 == rows)  || (0 == columns) )
	{
  		DEB("drv_keyboard_init: rows & cols == 0 ! \n");
		return false;	
	}

	// column_pin_array:  .... 15열 + OUTPUT +
	for (uint_fast8_t i = 0; i<KEYBOARD_NUM_OF_COLUMNS; i++)
	{
		//as GPIO_PIN_CNF_DRIVE_S0S1 (normal cases) - Standard '0', standard '1'.. 
 
		nrf_gpio_cfg_output((uint32_t)columns[i]);
		
		
		// Set pin to be "Disconnected 0 and 1"
  		NRF_GPIO->PIN_CNF[(uint32_t)columns[i]] |= 0x400;	
		
		// Set pin to low
		nrf_gpio_pin_clear((uint32_t)columns[i]);
	}
	

	// row_pin_array: .... 8행 + INPUT + PULL-DOWN
	for (uint_fast8_t i = 0; i<KEYBOARD_NUM_OF_ROWS; i++)
	{
		nrf_gpio_cfg_input((uint32_t)rows[i], NRF_GPIO_PIN_PULLDOWN);
		
		// Prepare the magic number
		input_scan_vector |= (1U << rows[i]);	
	}

	// Detect if any input pin is high
	if (((NRF_GPIO->IN) & input_scan_vector) != 0) 
	{
		// If inputs are not all low while output are, there must be something wrong
  		DEB("drv_keyboard_init: ===ABNORMAL KEY INPUTS=== \n");
		return NRF_ERROR_INTERNAL;
	}
	else
	{
		// Clear the arrays
		prv_keys_n 		= 0;	
		pressed_keys_n 	= 0;
		
		for (uint_fast8_t i = 0; i<MAX_NUM_OF_KEYS; i++)
		{
			keys_buf[i] 	= 0;
			prv_keys_buf[i] = 0;
		}
	}
	
	init_hot_key();
	
	#if defined(DRV_KEYBOARD_FUNC_TRACE)
  		DEB("drv_keyboard_init: SUCCESS \n");
	#endif


	return NRF_SUCCESS;
}

/*
================================================================================
   Function name : drv_keyboard_new_packet()
================================================================================
*/
bool drv_keyboard_new_packet(const uint8_t **p_packet, uint8_t *p_size)
{
	________________________________LOG_drvKeyboard_FuncHeads(FUNCHEAD_FLAG, "");
	bool is_new 	= false;
	uint8_t len 	= 0;
	uint8_t buf[MAX_NUM_OF_KEYS] = {0, };	

	// Scan the key matrix, save the result in keys_buf
	if (read(buf, &len))
	{
		pressed_keys_n = len;
	
		memcpy(keys_buf, buf, MAX_NUM_OF_KEYS);
		
		fn_remap(keys_buf, MAX_NUM_OF_KEYS);

		scan_hot_key(keys_buf, pressed_keys_n);

		// Some keys have been pressed, check if there is key changes
		if (true == is_changed())
		{	
			*p_size  	= PACKET_SIZE;
			*p_packet  	= m_key_buffer;
	
			// Send keys, when no hotkeys were detected
			if(DRV_KEYBOARD_HOTKEY_NONE == m_hotkey)
			{
				create(m_key_buffer, PACKET_SIZE);
				is_new = true;
			}

			// Save the number of currently pressed keys
			prv_keys_n = pressed_keys_n;
			memcpy(prv_keys_buf, keys_buf, MAX_NUM_OF_KEYS);
		}
	}

	return is_new;
}

/*
================================================================================
   Function name : drv_keyboard_packet()
================================================================================
*/
void drv_keyboard_get_packet(uint8_t *p_packet, uint8_t *p_size)
{
	________________________________LOG_drvKeyboard_FuncHeads(FUNCHEAD_FLAG, "");
	if(NULL != p_packet)
	{
		*p_size = PACKET_SIZE;
		memcpy(p_packet, m_key_buffer, PACKET_SIZE);
	}
}

/*
================================================================================
   Function name : drv_keyboard_get_hotkey()
================================================================================
*/
uint32_t drv_keyboard_get_hotkey(void)
{
	________________________________LOG_drvKeyboard_FuncHeads(FUNCHEAD_FLAG, "");
	uint32_t val = m_hotkey;
	

	// TODO: Use stringizing(#) of preprocessor macros
#if defined(DRV_KEYBOARD_HOTKEY_TRACE)
	for(uint32_t i=0; i<DRV_KEYBOARD_HOTKEY_TYPE_MAX; i++)
	{
		static const char *String[DRV_KEYBOARD_HOTKEY_TYPE_MAX] = \
		{ \
			 "HOTKEY_RESET", \
			 "HOTKEY_CAPTURE", \
			 "HOTKEY_ERASE_BOND", \
			 "HOTKEY_HOUSE", \
			 "HOTKEY_BT1", \
			 "HOTKEY_BT2", \
			 "HOTKEY_BT3", \
			 "HOTKEY_DONGLE", \
			 "HOTKEY_BTRCHK", \
			 "HOTKEY_SWTAPP", \
			 "HOTKEY_VKBD", \
			 "HOTKEY_FIND", \
			 "HOTKEY_V_UP", \
			 "HOTKEY_V_DOWN",\
			 "HOTKEY_ONOFF", \
			 "HOTKEY_DEBUG", \
			 "HOTKEY_DEBUG2", \
			 "HOTKEY_DEBUG3", \
			 "HOTKEY_TEST_ENTER", \
			 "HOTKEY_BT1_NEW", \
			 "HOTKEY_BT2_NEW", \
			 "HOTKEY_BT3_NEW", \
			 "HOTKEY_CERT_MODE", \
			"CENTRAL_WINDOWS", \
			"CENTRAL_ANDROID", \
			"CENTRAL_MAC", \
			"HOTKEY_BACK", \
			"HOTKEY_TOUCHLOCK",\
			"HOTKEY_V_MUTE",\
		};

		if(m_hotkey & NBIT(i))
		{
			//DEBUG("%s is detected", String[i]);
			DEB("Hotkey: %s detected\n",String[i]);
		}
	}
#endif
	
	m_hotkey = DRV_KEYBOARD_HOTKEY_NONE; // Read clear

	return val;
}

/*
================================================================================
   Function name : drv_keyboard_prepare_sleep()
================================================================================
*/
void drv_keyboard_prepare_sleep(void)
{
	________________________________LOG_drvKeyboard_FuncHeads(FUNCHEAD_FLAG, "");
	DEBUG("Prepare Sleep");

	#if 0 // wake-up by xxx
		// set set to GPIO out P11
		nrf_gpio_pin_set((uint32_t) columns[1]);

		nrf_gpio_cfg_sense_set(rows[6], NRF_GPIO_PIN_SENSE_HIGH);
	#else
		for (uint_fast8_t i = 0; i<KEYBOARD_NUM_OF_COLUMNS; i++)
		{
			nrf_gpio_pin_clear((uint32_t)columns[i]);	// Set pin to low
		}
	#endif
}

/*
================================================================================
   Function name : drv_keyboard_get_volume()
================================================================================
*/
uint8_t drv_keyboard_get_volume(uint8_t input)
{
	________________________________LOG_drvKeyboard_FuncHeads(FUNCHEAD_FLAG, "");
	uint8_t i, rv = 0;
	
	for( i=0; i<M_HOTKEY_TABLE_MAX; i++)
	{
		//if(S2L(V_UP) == m_hotkey_table[i].input)
		if(input == m_hotkey_table[i].input)
		{
			// Found!!!
			break;
		}
	}

	if( i >= M_HOTKEY_TABLE_MAX)
	{
		DEBUG("OOPS!!");
	}
	else
	{
		rv = m_hot_keys[i].bit.now ? 1 : 0;
		//DEBUG("rv[%d]i[%d]status[%d]", rv, i, m_hot_keys[i].bit.now);
	}

	return rv;
}

/*
================================================================================
   Function name : drv_keyboard_is_pressed()
================================================================================
*/
bool drv_keyboard_is_pressed(uint8_t input)
{
	________________________________LOG_drvKeyboard_FuncHeads(FUNCHEAD_FLAG, "");
	bool rv = false;

	// If buffer has empty space, then insert to buffer m_key_buffer
	for (uint8_t i = PACKET_KEY_INDEX; i < PACKET_SIZE; i++)
	{
		if (m_key_buffer[i] == input)
		{
			rv = true;
			break;
		}
	}

	return rv;
}







/*
$$\       $$$$$$\   $$$$$$\   $$$$$$\  $$\                                   
$$ |     $$  __$$\ $$  __$$\ $$  __$$\ $$ |                                  
$$ |     $$ /  $$ |$$ /  \__|$$ /  $$ |$$ |                                  
$$ |     $$ |  $$ |$$ |      $$$$$$$$ |$$ |                                  
$$ |     $$ |  $$ |$$ |      $$  __$$ |$$ |                                  
$$ |     $$ |  $$ |$$ |  $$\ $$ |  $$ |$$ |                                  
$$$$$$$$\ $$$$$$  |\$$$$$$  |$$ |  $$ |$$$$$$$$\                             
\________|\______/  \______/ \__|  \__|\________|                            
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\ 
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__|*/
/*============================================================================*/
/*                              LOCAL FUNCTIONS                               */
/*============================================================================*/



/*
================================================================================
   Function name : init_hot_key()
================================================================================
*/
static void init_hot_key(void)
{
	________________________________LOG_drvKeyboard_FuncHeads(FUNCHEAD_FLAG, "");
	uint32_t i;
	
	m_hotkey = DRV_KEYBOARD_HOTKEY_NONE;

	for( i=0; i<M_HOTKEY_TABLE_MAX; i++)
	{
		m_hot_keys[i].data = 0;
	}
}

/*
================================================================================
   Function name : is_changed()
================================================================================
*/
static bool is_changed(void)
{
	________________________________LOG_drvKeyboard_FuncHeads(FUNCHEAD_FLAG, "");
	bool result = false;

	if (pressed_keys_n != prv_keys_n)
	{
		result = true;
	}
	else
	{
		for (uint_fast8_t i = 0; i<pressed_keys_n; i++)
		{
			if (keys_buf[i] != prv_keys_buf[i])
			{
				result = true;
			}
		}
	}

	return result;
}

/*
================================================================================
   Function name : read()
================================================================================
*/
static bool read(uint8_t *buf, uint8_t *num)
{
	________________________________LOG_drvKeyboard_FuncHeads(FUNCHEAD_FLAG, "");
	uint_fast8_t i;
	uint_fast8_t mask = 0;
	uint_fast8_t row_state[KEYBOARD_NUM_OF_COLUMNS];
	uint_fast8_t state;
	uint_fast8_t detected_n;
	bool is_ok = true;
	
	*num = 0;

	// Loop through each column
	for( i = 0; i < KEYBOARD_NUM_OF_COLUMNS; i++)
	{	
		// make the colum pin -HIGH-
		nrf_gpio_pin_set((uint32_t) columns[i]);
		
		// wait for a moment
    	nrf_delay_us(10);
		
		// If any input is high
		if (((NRF_GPIO->IN) & input_scan_vector) != 0)
		{
			detected_n 		= 0;
			row_state[i] 	= 0;
			
			// Loop through each row
			for (uint_fast8_t row = 0; row < KEYBOARD_NUM_OF_ROWS; row++)
			{		
				state = nrf_gpio_pin_read((uint32_t) rows[row]) ? 1 : 0;
				
				// Record pin state
				row_state[i] |= (state << row);				
				
				// If pin is high
				if (state)
				{											
					#if defined(DRV_KEYBOARD_PIO_TRACE)
   					DEBUGS("[%d:%d]", columns[i], rows[row]);
					#endif
					
					if (*num < MAX_NUM_OF_KEYS)
					{
						// m_lookup[15x8];
						*buf = m_lookup[row * KEYBOARD_NUM_OF_COLUMNS + i];
					
						if((m_comm_current_central_type() == M_COMM_CENTRAL_MAC) || (m_comm_current_central_type() == M_COMM_CENTRAL_IOS))
						{
							//window UI     ALT /// ALT     CTRL
							//MAC    ALT    UI  /// UI      ALT
							if(*buf == S2L(L_UI))
							{
								*buf = S2L(L_ALT);            
							}
							else if(*buf == S2L(L_ALT))
							{            
								*buf = S2L(L_UI);            
							}
							
							#if (LAYOUT_OPTION != LAYOUT_OPTION_UNIQ)
							{
								if(*buf == S2L(R_ALT))
								{
									*buf = S2L(R_UI);
								}
								else if(*buf == S2L(R_CTRL))
								{
									*buf = S2L(R_ALT);
								}
							}	
							#endif						
						}
						/*
						#ifdef FLIMBS_20190110_CHANGE_LUI_LALT_ON_CENTRAL_MAC						
						m_flimbs_change_lookup_table_when_read(buf);						
						#endif
						*/
						#if defined(DRV_KEYBOARD_KEY_TRACE)
    					DEBUGS("pressed[0x%x]\n\r", *buf);
						#endif
						
						// Record the pressed key if it's not 0
						if(*buf != 0)
						{							
							buf++;
							(*num)++;
						}
					}

					detected_n++;
				}
			}

			if (detected_n > 1)
			{
				// If there is ghosting
				if (mask & row_state[i])
				{
					#if defined(DRV_KEYBOARD_GHOST_TRACE)
  					DEBUG("ghost[%d]", detected_n);
					#endif
					is_ok = false;
				}
			}

			mask |= row_state[i];
		}

		// wait for a moment
		nrf_delay_us(10);

		// make the column pin -LOW-
		nrf_gpio_pin_clear((uint32_t) columns[i]);
	}

	return is_ok;
}

/*
================================================================================
   Function name : create()
================================================================================
*/
static void create(uint8_t *packet, uint8_t size)
{
	________________________________LOG_drvKeyboard_FuncHeads(FUNCHEAD_FLAG, "");
	packet[PACKET_MODIFIER_INDEX] = 0;
	packet[PACKET_RESERVED_INDEX] = 0;

	// Clear key_packet contents
	for (uint_fast8_t i = PACKET_KEY_INDEX; i < size; i++)
	{
		packet[i] = PACKET_NO_KEY;
	}

	// Loop through the currently pressed keys array
	for (uint_fast8_t i = 0; i < pressed_keys_n; i++)
	{	
		// Find 
		if (keys_buf[i] >= MODIFIER_HID_START && keys_buf[i] <= MODIFIER_HID_END) 
		{
			// Detect and set modifier key, see HID Usage Tables for more detail
  			packet[PACKET_MODIFIER_INDEX] |= m_modifier_table[keys_buf[i]-MODIFIER_HID_START];
			
			#if defined(DRV_KEYBOARD_FUNC_TRACE)
  			DEBUG("Modifier[0x%x] by [0x%x]", packet[PACKET_MODIFIER_INDEX], keys_buf[i]);
			#endif
		}
		else if (keys_buf[i] != 0)
		{
			// Add keys to the packet
			add(packet, keys_buf[i]);			
		}
	}

#if (LAYOUT_OPTION == LAYOUT_OPTION_UNIQ)
	if(m_comm_current_central_type() == M_COMM_CENTRAL_IOS)
	{
		
		
		bool l_shifted = packet[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] == APP_USBD_HID_KBD_MODIFIER_LEFT_SHIFT;
		bool r_shifted = packet[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] == APP_USBD_HID_KBD_MODIFIER_RIGHT_SHIFT; // Ctrl Alt 등 포함? 시프트하나 일때만?
		bool lr_shifted = packet[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] == APP_USBD_HID_KBD_MODIFIER_LEFT_SHIFT + APP_USBD_HID_KBD_MODIFIER_RIGHT_SHIFT;
		bool shifted = l_shifted || r_shifted || lr_shifted;

		if((packet[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] == 0 ) || (shifted == true))// Shift 만 눌린 상황에서동작한다.)
		{
			for(int i=DRV_KEYBOARD_PACKET_KEY_INDEX; i<DRV_KEYBOARD_MAX_NUM_OF_KEYS; i++)
			{
				if(packet[i] == PACKET_NO_KEY)
				{continue;}

				bool applyJisLayout = false; //변환을 할지말지 결정
				bool setShift = shifted; // 변환을 했으면 시프트를 쓸지말지 결정
				bool setAlt = false; // 변환을 했으면 Alt를 쓸지 말지 결정				
				
				if(!shifted && (packet[i] == APP_USBD_HID_KBD_TILDE))//kan)a
				{
					applyJisLayout = true;
					setShift = false;
					packet[i] = PACKET_NO_KEY;
				}
				else if(shifted && (packet[i] == APP_USBD_HID_KBD_2))
				{//shift 2 : @ -> "				
					applyJisLayout = true;
					setShift = true;
					packet[i] = APP_USBD_HID_KBD_QUOTE;
				}
				else if(shifted && (packet[i] == APP_USBD_HID_KBD_6))
				{
					applyJisLayout = true;
					setShift = true;
					packet[i] = APP_USBD_HID_KBD_7;
				}
				else if(shifted && (packet[i] == APP_USBD_HID_KBD_7))
				{
					applyJisLayout = true;
					setShift = false;
					packet[i] = APP_USBD_HID_KBD_QUOTE;
				}
				else if(shifted && (packet[i] == APP_USBD_HID_KBD_8))
				{
					applyJisLayout = true;
					setShift = true;
					packet[i] = APP_USBD_HID_KBD_9;
				}
				else if(shifted && (packet[i] == APP_USBD_HID_KBD_9))
				{
					applyJisLayout = true;
					setShift = true;
					packet[i] = APP_USBD_HID_KBD_0;
				}
				else if(shifted && (packet[i] == APP_USBD_HID_KBD_0))
				{
					applyJisLayout = true;
					setShift = true;
					packet[i] = PACKET_NO_KEY;
				}
				else if(shifted && (packet[i] == APP_USBD_HID_KBD_U_SCORE))
				{
					applyJisLayout = true;
					setShift = false;
					packet[i] = APP_USBD_HID_KBD_PLUS;
				}
				else if(shifted && (packet[i] == APP_USBD_HID_KBD_PLUS))
				{
					applyJisLayout = true;
					setShift = true;
					packet[i] = APP_USBD_HID_KBD_TILDE;
				}
				else if(!shifted && (packet[i] == APP_USBD_HID_KBD_PLUS))
				{	applyJisLayout = true;
					setShift = true;
					packet[i] = APP_USBD_HID_KBD_6;
				}
				else if(shifted && (packet[i] == APP_USBD_HID_KBD_O_BRCKT))
				{
					applyJisLayout = true;
					setShift = false;
					packet[i] = APP_USBD_HID_KBD_TILDE;
				}
				else if(shifted && (packet[i] == APP_USBD_HID_KBD_C_BRCKT))
				{
					applyJisLayout = true;
					setShift = true;
					packet[i] = APP_USBD_HID_KBD_O_BRCKT;
				}
				else if(shifted && (packet[i] == APP_USBD_HID_KBD_BSLH))
				{
					applyJisLayout = true;
					setShift = true;
					packet[i] = APP_USBD_HID_KBD_C_BRCKT;
				}
				else if(!shifted && (packet[i] == APP_USBD_HID_KBD_O_BRCKT))
				{
					applyJisLayout = true;
					setShift = true;
					packet[i] = APP_USBD_HID_KBD_2;
				}
				else if(!shifted && (packet[i] == APP_USBD_HID_KBD_C_BRCKT))
				{
					applyJisLayout = true;
					setShift = false;
					packet[i] = APP_USBD_HID_KBD_O_BRCKT;
				}
				else if(!shifted && (packet[i] == APP_USBD_HID_KBD_BSLH))
				{
					applyJisLayout = true;
					setShift = false;
					packet[i] = APP_USBD_HID_KBD_C_BRCKT;
				}
				else if(shifted && (packet[i] == APP_USBD_HID_KBD_COLON))
				{
					applyJisLayout = true;
					setShift = true;
					packet[i] = APP_USBD_HID_KBD_PLUS;
				}
				else if(shifted && (packet[i] == APP_USBD_HID_KBD_QUOTE))
				{
					applyJisLayout = true;
					setShift = true;
					packet[i] = APP_USBD_HID_KBD_8;
				}
				else if(!shifted && (packet[i] == APP_USBD_HID_KBD_QUOTE))
				{
					applyJisLayout = true;
					setShift = true;
					packet[i] = APP_USBD_HID_KBD_COLON;
				}
				
				else if(shifted && (packet[i] == APP_USBD_HID_KBD_INTERNATIONAL3)) //Rctrl 시프트 + 엔화 => |
				{
					applyJisLayout = true;
					setShift=true;
					packet[i] = APP_USBD_HID_KBD_BSLH;
				}
				else if(shifted && (packet[i] == APP_USBD_HID_KBD_INTERNATIONAL1)) // RFn 시프트 + 백슬래시 => _
				{
					applyJisLayout = true;
					setShift=true;
					packet[i] = APP_USBD_HID_KBD_U_SCORE;
				}
				else if(!shifted && (packet[i] == APP_USBD_HID_KBD_INTERNATIONAL3)) //Rctrl 엔화 => Alt + Y
				{
					applyJisLayout = true;
					setShift=false;
					setAlt = true;
					packet[i] = APP_USBD_HID_KBD_Y;
				}
				else if(!shifted && (packet[i] == APP_USBD_HID_KBD_INTERNATIONAL1)) // RFn 백슬래시 => 
				{
					applyJisLayout = true;
					setShift=false;
					packet[i] = APP_USBD_HID_KBD_BSLH;
				}

				if(applyJisLayout == true)
				{
					if(setShift == false) // 쉬프트를 안쓰는 키
					{
						if(l_shifted || r_shifted || lr_shifted)	
						{
							packet[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = 0;
						}
					}
					else if(setShift == true)//쉬프트를 쓰는 키
					{
						if(shifted == false)
						{
							packet[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_SHIFT;
						}
						else if(shifted == true)
						{
							//do nothing
						}
					}
					if(setAlt==true)
					{
						packet[DRV_KEYBOARD_PACKET_MODIFIER_INDEX] = APP_USBD_HID_KBD_MODIFIER_LEFT_ALT;
					}
					
					
				}
			}
		}
	}

#endif
}

/*
================================================================================
   Function name : scan_hot_key()
================================================================================
*/
static void scan_hot_key(const uint8_t *buf, uint8_t len)
{	
	________________________________LOG_drvKeyboard_FuncHeads(FUNCHEAD_FLAG, "");
	uint8_t pressed, index;
	
	// Clear all
	uint8_t keys[M_HOTKEY_TABLE_MAX] = {0, };
	
	// find pressed keys
	for ( pressed = 0; pressed < len; pressed++)
	{
		for( index = 0; index < M_HOTKEY_TABLE_MAX; index++)
		{
			if(buf[pressed] == m_hotkey_table[index].input)
			{
				keys[index]  = 1;
			}
		}
	}

	// handle hot keys
	for( index = 0; index < M_HOTKEY_TABLE_MAX; index++)
	{
		m_hot_keys[index].bit.now = keys[index];

		// key is changed
		if(m_hot_keys[index].bit.prv != m_hot_keys[index].bit.now)
		{
			// Store	
			m_hot_keys[index].bit.prv = m_hot_keys[index].bit.now;

			// Detect push edge
			if(m_hot_keys[index].bit.now && (m_hotkey_table[index].attr & P_EDGE))
			{
				m_hot_keys[index].bit.evt_active = 1;
			}

			// Detect release edge
			if((!m_hot_keys[index].bit.now) && (m_hotkey_table[index].attr & R_EDGE))
			{
				// if release edeg is activated togather with long key, ignore long key release
				if((m_hotkey_table[index].attr & LONG_KEY)) 
				{
					if(!m_timer_is_expired(m_hotkey_table[index].timer_id))
					{
						m_hot_keys[index].bit.evt_active = 1;
					}
				}
				else
				{
					m_hot_keys[index].bit.evt_active = 1;
				}
			}
			
			// Detect long press starting point
			if(m_hot_keys[index].bit.now && (m_hotkey_table[index].attr & LONG_KEY))
			{
				m_hot_keys[index].bit.long_started = 1;
			}
		}

		if(m_hotkey_table[index].attr & LONG_KEY)
		{
			// if key is released, refresh timer
			if(0 == m_hot_keys[index].bit.now)
			{
				m_hot_keys[index].bit.long_started = 0;	// reset by off 
				m_timer_set(m_hotkey_table[index].timer_id, m_hotkey_table[index].duration);
			}

			if(m_hot_keys[index].bit.long_started && m_timer_is_expired(m_hotkey_table[index].timer_id))
			{
				m_hot_keys[index].bit.long_started = 0; // to prevent countinuous event
				m_hot_keys[index].bit.long_evt_active = 1;
				#if defined(DRV_KEYBOARD_HOTKEY_TRACE)
				//DEBUG("");
				#endif
			}
		}

		if(m_hot_keys[index].bit.evt_active)
		{
			// close evt flag
			m_hot_keys[index].bit.evt_active = 0;
		
			// set corrensponding hot key bit
			m_hotkey |= m_hotkey_table[index].hotkey;
			DEBUG("m_hotkey= 0x%X",m_hotkey);
		}

		if(m_hot_keys[index].bit.long_evt_active)
		{
			// close evt flag
			m_hot_keys[index].bit.long_evt_active = 0;
		
			// set corrensponding hot key bit
			m_hotkey |= m_hotkey_table[index].long_hotkey;
			DEBUG("m_hotkey= 0x%X",m_hotkey);
		}
	}
}

/*
================================================================================
   Function name : fn_remap()
================================================================================
*/
static void fn_remap(uint8_t *keys, uint8_t num)
{
	________________________________LOG_drvKeyboard_FuncHeads(FUNCHEAD_FLAG, "");
	uint_fast8_t i, j, table_size; 
	
	bool fn_key_is_set 	= false;

	// Check that Fn key is pressed
	for (i = 0; i < num; i++)
	{	
		if (FUNC == keys[i]) 
		{					
			fn_key_is_set = true;
			#if defined(DRV_KEYBOARD_FUNC_TRACE)
  			DEBUG("Function Key is pressed");
			#endif
			// stop scanning, one function keys is enough
			break; 
		}
	}

	if(fn_key_is_set)
	{
		// Re-mapping 
		for (i = 0; i < num; i++)
		{
			table_size = sizeof(m_fn_keys_table)/sizeof(fn_key_t);

			for(j = 0; j < table_size; j++)
			{
				// TODO, Funtin key select setting
				if(keys[i] == m_fn_keys_table[j].org)
				{
					#if defined(DRV_KEYBOARD_FUNC_TRACE)
					DEBUG("[%d]===>[%d]", keys[i], m_fn_keys_table[j].new);
					#endif

					keys[i] = m_fn_keys_table[j].new;
				}
			}
		}
	}
}

/*
================================================================================
   Function name : add()
================================================================================
*/
static void add(uint8_t *buf, uint8_t key)
{
	________________________________LOG_drvKeyboard_FuncHeads(FUNCHEAD_FLAG, "");
	// If the key already exited in the packet, skip adding
	for (uint_fast8_t i = PACKET_KEY_INDEX; i < PACKET_SIZE; i++)
	{
		if (buf[i] == key)
		{
			return;
		}
	}
	
	// If buffer has empty space, then insert to buffer m_key_buffer
	for (uint_fast8_t i = PACKET_KEY_INDEX; i < PACKET_SIZE; i++)
	{
		if (buf[i] == PACKET_NO_KEY)
		{
			buf[i] = key;
			break;
		}
	}
}













/*$$$$$\  $$$$$$$$\ $$$$$$$\  $$\   $$\  $$$$$$\                                       
$$  __$$\ $$  _____|$$  __$$\ $$ |  $$ |$$  __$$\                                      
$$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |$$ /  \__|                                     
$$ |  $$ |$$$$$\    $$$$$$$\ |$$ |  $$ |$$ |$$$$\                                      
$$ |  $$ |$$  __|   $$  __$$\ $$ |  $$ |$$ |\_$$ |                                     
$$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |$$ |  $$ |                                     
$$$$$$$  |$$$$$$$$\ $$$$$$$  |\$$$$$$  |\$$$$$$  |                                     
\_______/ \________|\_______/  \______/  \______/          
$$$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\ $$$$$$$$\ $$$$$$\  $$$$$$\  $$\   $$\  $$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\\__$$  __|\_$$  _|$$  __$$\ $$$\  $$ |$$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|  $$ |     $$ |  $$ /  $$ |$$$$\ $$ |$$ /  \__|
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ $$\$$ |\$$$$$$\  
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |        $$ |     $$ |  $$ |  $$ |$$ \$$$$ | \____$$\ 
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\   $$ |     $$ |  $$ |  $$ |$$ |\$$$ |$$\   $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$$$$$\  $$$$$$  |$$ | \$$ |\$$$$$$  |
\__|       \______/ \__|  \__| \______/   \__|   \______| \______/ \__|  \__| \______/ 
//============================================================================//
//                                 DEBUG FUNCTION                             //
//============================================================================*/












/*\   $$\ $$\   $$\ $$\   $$\  $$$$$$\  $$$$$$$$\ $$$$$$$\                             
$$ |  $$ |$$$\  $$ |$$ |  $$ |$$  __$$\ $$  _____|$$  __$$\                            
$$ |  $$ |$$$$\ $$ |$$ |  $$ |$$ /  \__|$$ |      $$ |  $$ |                           
$$ |  $$ |$$ $$\$$ |$$ |  $$ |\$$$$$$\  $$$$$\    $$ |  $$ |                           
$$ |  $$ |$$ \$$$$ |$$ |  $$ | \____$$\ $$  __|   $$ |  $$ |                           
$$ |  $$ |$$ |\$$$ |$$ |  $$ |$$\   $$ |$$ |      $$ |  $$ |                           
\$$$$$$  |$$ | \$$ |\$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$  |                           
 \______/ \__|  \__| \______/  \______/ \________|\_______/                            
//============================================================================//
//                                 UNUSED FUNCTION                            //
//============================================================================*/

#if 0 // 'HALT'를 사용하지 않을 것이므로 다음은 사용하지 않는다
	//-----------------------------------------------------------------------------------------------------------------------------------
	//			drv_release_fn_del_colums
	//-----------------------------------------------------------------------------------------------------------------------------------
	// (1) [Fn+Del]에 의하여 되살아나야 하는 STM8 MCU를 위하여 (Halt되어 있는 STM8은 Fn+Del에 의하여 깨어날 수 있다)
	// (2) BLE(nRF52832) MCU가 Power-Down 될 때, Fn 과 Del 키가 속해 있는 colum pin들을 Release하기 위하여,
	// (3) Input Mode로 전환함.
	// (4) Power-down이 되었을 때에까지, port i/o mode의 효과가 남아 있을지는 의문이지만...
	void drv_release_fn_del_colums(void)
	{	
		// P.11을 INPUT Mode로 전환. DEL-key는 P.11열에 포함되어 있음. 원래 P.11은 Output mode로 사용됨.
		// Del-key: (row,col)=(P.06, P.11)
		nrf_gpio_cfg_input(11,NRF_GPIO_PIN_NOPULL );

		// P.24을 INPUT Mode로 전환. FN-key는 P.24열에 포함되어 있음. 원래 P.24는 Output mode로 사용됨.
		// Fn-key: (row,col)=(P.00, P.24)
		nrf_gpio_cfg_input(24,NRF_GPIO_PIN_NOPULL );
		
		
		//		// Set pin to High
		//		nrf_gpio_cfg_output(11);
		//		nrf_gpio_pin_set(11);
		//
		//		// Set pin to High
		//		nrf_gpio_cfg_output(24);
		//		nrf_gpio_pin_set(24);

	}	
#endif


















// End of file 
