/// \file drv_keboard.h
/// This file contains all required configurations for mokibo
/*
================================================================================
* Copyright (C) 2017-2018
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
================================================================================
*/




/*\   $$\ $$$$$$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$$$$$$\ $$\     $$\          
$$ |  $$ |\_$$  _|$$  __$$\\__$$  __|$$  __$$\ $$  __$$\\$$\   $$  |         
$$ |  $$ |  $$ |  $$ /  \__|  $$ |   $$ /  $$ |$$ |  $$ |\$$\ $$  /          
$$$$$$$$ |  $$ |  \$$$$$$\    $$ |   $$ |  $$ |$$$$$$$  | \$$$$  /           
$$  __$$ |  $$ |   \____$$\   $$ |   $$ |  $$ |$$  __$$<   \$$  /            
$$ |  $$ |  $$ |  $$\   $$ |  $$ |   $$ |  $$ |$$ |  $$ |   $$ |             
$$ |  $$ |$$$$$$\ \$$$$$$  |  $$ |    $$$$$$  |$$ |  $$ |   $$ |             
\__|  \__|\______| \______/   \__|    \______/ \__|  \__|   \__|             
//============================================================================//
//                                 REVISION HISTORY                           //
//============================================================================//

20171016_MJ
  Created initial version.

20171122_MJ
  Added APP_USBD_HID_KBD_L_CTRL... in app_usbd_hid_kbd_codes_t
  Changed names to shorten in app_usbd_hid_kbd_codes_t

20171218_MJ
  Added following keyis in app_usbd_hid_kbd_codes_t
  APP_USBD_HID_KBD_FIND        	 = 126
  APP_USBD_HID_KBD_MUTE        	 = 127
  APP_USBD_HID_KBD_V_UP        	 = 128
  APP_USBD_HID_KBD_V_DOWN       	 = 129
  APP_USBD_HID_KBD_RSVD0       	 = 165
  APP_USBD_HID_KBD_RSVD1       	 = 166
  APP_USBD_HID_KBD_RSVD2       	 = 167
  APP_USBD_HID_KBD_RSVD3       	 = 168
  APP_USBD_HID_KBD_RSVD4       	 = 169
  APP_USBD_HID_KBD_RSVD5       	 = 170
  APP_USBD_HID_KBD_RSVD6       	 = 171
  APP_USBD_HID_KBD_RSVD7       	 = 172
  APP_USBD_HID_KBD_RSVD8       	 = 173
  APP_USBD_HID_KBD_RSVD9       	 = 174
  APP_USBD_HID_KBD_RSVD10       	 = 175

20171221_MJ
  Added drv_keyboard_get_volume_down()/drv_keyboard_get_volume_up()

20171228_MJ
  Added DRV_KEYBOARD_HOTKEY_DEBUG 

20180122_MJ
  Renamed APP_USBD_HID_KBD_BACKSLASH to APP_USBD_HID_KBD_BSLH

20180125_MJ
  Created DRV_KEYBOARD_HOTKEY_BT1_NEW ~ DRV_KEYBOARD_HOTKEY_BT3_NEW 

20180212_MJ
  Change DRV_KEYBOARD_HOTKEY_DEBUG4 into  DRV_KEYBOARD_HOTKEY_TEST_ENTER
  Issue#: http://lab.innopresso.com/issues/118#change-556 

20180309_MJ
  Removed DRV_KEYBOARD_HOTKEY_MODECHANGE
  Added DRV_KEYBOARD_HOTKEY_CAPTURE

20180320_MJ
  Created drv_keyboard_is_pressed() 

20180413_MJ
  Created drv_keyboard_get_packet() 

20180808_Stanley
  cert_mode: 마우스 포인터를 사각형으로 우회전하게 하고, keyboard 문자를 입력하는 과정을 반복함
  The followings are added for MOKIBO CERTIFICATION TEST;
  DRV_KEYBOARD_HOTKEY_CERT_MODE..... in drv_keyboard.h
  m_KeyswitchDown()................. in m_touch.c
  m_KeyswitchUp()................... in m_touch.c
  m_MouseMove()..................... in m_touch.c
  m_cert_mode_enabled............... in m_event.c
  m_do_cert_mode_repetition()....... in m_event.c
  The Caller ....................... in keyboard_scan_timeout_handler() in mokibo.c
  S2L(CERT_MODE).................... in drv_keyboard.c
  "HOTKEY_CERT_MODE"................ in drv_keyboard.c
  You can activate the CERT_MODE by typing [Fn+0].
  To deactivate the mode, press the same key once more.
==============================================================================*/










/*$$$$$\  $$\    $$\ $$$$$$$$\ $$$$$$$\  $$\    $$\ $$$$$$\ $$$$$$$$\ $$\      $$\ 
$$  __$$\ $$ |   $$ |$$  _____|$$  __$$\ $$ |   $$ |\_$$  _|$$  _____|$$ | $\  $$ |
$$ /  $$ |$$ |   $$ |$$ |      $$ |  $$ |$$ |   $$ |  $$ |  $$ |      $$ |$$$\ $$ |
$$ |  $$ |\$$\  $$  |$$$$$\    $$$$$$$  |\$$\  $$  |  $$ |  $$$$$\    $$ $$ $$\$$ |
$$ |  $$ | \$$\$$  / $$  __|   $$  __$$<  \$$\$$  /   $$ |  $$  __|   $$$$  _$$$$ |
$$ |  $$ |  \$$$  /  $$ |      $$ |  $$ |  \$$$  /    $$ |  $$ |      $$$  / \$$$ |
 $$$$$$  |   \$  /   $$$$$$$$\ $$ |  $$ |   \$  /   $$$$$$\ $$$$$$$$\ $$  /   \$$ |
 \______/     \_/    \________|\__|  \__|    \_/    \______|\________|\__/     \__|
//============================================================================//
//                        FUNCTION CALLSTACK OVERVIEW                         //
//============================================================================//
//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------*/






/*\      $$\  $$$$$$\  $$$$$$$\  $$\   $$\ $$\       $$$$$$$$\  $$$$$$\  
$$$\    $$$ |$$  __$$\ $$  __$$\ $$ |  $$ |$$ |      $$  _____|$$  __$$\ 
$$$$\  $$$$ |$$ /  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$ /  \__|
$$\$$\$$ $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$$$$\    \$$$$$$\  
$$ \$$$  $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$  __|    \____$$\ 
$$ |\$  /$$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$\   $$ |
$$ | \_/ $$ | $$$$$$  |$$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$$\ \$$$$$$  |
\__|     \__| \______/ \_______/  \______/ \________|\________| \______/ 
//============================================================================//
//                                 MODULES USED                               //
//============================================================================*/
#ifndef __DRV_KEABOARD_H
#define __DRV_KEABOARD_H

#include <stdbool.h>





  /*$$$$\   $$$$$$\  $$\   $$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$\   $$\ $$$$$$$$\ 
$$  __$$\ $$  __$$\ $$$\  $$ |$$  __$$\\__$$  __|$$  __$$\ $$$\  $$ |\__$$  __|
$$ /  \__|$$ /  $$ |$$$$\ $$ |$$ /  \__|  $$ |   $$ /  $$ |$$$$\ $$ |   $$ |   
$$ |      $$ |  $$ |$$ $$\$$ |\$$$$$$\    $$ |   $$$$$$$$ |$$ $$\$$ |   $$ |   
$$ |      $$ |  $$ |$$ \$$$$ | \____$$\   $$ |   $$  __$$ |$$ \$$$$ |   $$ |   
$$ |  $$\ $$ |  $$ |$$ |\$$$ |$$\   $$ |  $$ |   $$ |  $$ |$$ |\$$$ |   $$ |   
\$$$$$$  | $$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$ |  $$ |$$ | \$$ |   $$ |   
 \______/  \______/ \__|  \__| \______/   \__|   \__|  \__|\__|  \__|   \__|   
 //============================================================================//
//                                 GLOBAL CONSTANTS                            //
//============================================================================*/


#define HOTK(NAME) 		(DRV_KEYBOARD_HOTKEY_##NAME)
#define S2L(VAL)			(APP_USBD_HID_KBD_##VAL)

#define DRV_KEYBOARD_MAX_SIMUL_KEYS   			6   /**@brief Maximum number of simultaneously held keys. */

#define DRV_KEYBOARD_PACKET_MODIFIER_INDEX 	(0)			//!< Index in the key packet where modifier keys such as ALT and Control are stored
#define DRV_KEYBOARD_PACKET_RESERVED_INDEX 	(1)			//!< Index in the key packet where OEMs can store information
#define DRV_KEYBOARD_PACKET_KEY_INDEX 		(2)			//!< Start index in the key packet where pressed keys are stored
#define DRV_KEYBOARD_MAX_NUM_OF_KEYS 		(6) 		//!< Maximum number of pressed keys kept in buffers
#define DRV_KEYBOARD_MAX_NUM_OF_ALL_KEYS 	(DRV_KEYBOARD_PACKET_KEY_INDEX+DRV_KEYBOARD_MAX_NUM_OF_KEYS)

// Currently maximum DRV_KEYBOARD_HOTKEY number is 32, bitfield of unsigned int
// When adding new HOT KEY
// 1. Increase DRV_KEYBOARD_HOTKEY_MAX 
// 2. Add new string drv_keyboard_get_hotkey()




#define DRV_KEYBOARD_HOTKEY_NONE		    0x00000000		//   HOTK(NONE)
#define DRV_KEYBOARD_HOTKEY_RESET		    0x00000001		// 0:HOTK(RESET) 			Fn + 1
#define	DRV_KEYBOARD_HOTKEY_CAPTURE     0x00000002		// 1:HOTK(CAPTURE) 			Capture screen(Fn10자리)
#define DRV_KEYBOARD_HOTKEY_ERASE_BOND	0x00000004		// 2:HOTK(ERASE_BOND) 		Fn + 3
#define	DRV_KEYBOARD_HOTKEY_HOUSE	    	0x00000008		// 3:HOTK(HOUSE) 			Fn + Esc, HOME key와 구별하려 HOUSE 이름 사용
#define	DRV_KEYBOARD_HOTKEY_BT1         0x00000010		// 4:HOTK(BT1) 				BT1(F1자리)
#define	DRV_KEYBOARD_HOTKEY_BT2         0x00000020		// 5:HOTK(BT2) 				BT2(F2자리)
#define	DRV_KEYBOARD_HOTKEY_BT3         0x00000040		// 6:HOTK(BT3) 				BT3(F3자리)
#define	DRV_KEYBOARD_HOTKEY_DONGLE      0x00000080		// 7:HOTK(DONGLE) 			Dongle(F4자리)
#define	DRV_KEYBOARD_HOTKEY_DONGLE_PSEUDO				//   ---pseudo--
#define	DRV_KEYBOARD_HOTKEY_BTRCHK          0x00000100		// 8:HOTK(BTRCHK) 			Battery check(F5자리)
#define	DRV_KEYBOARD_HOTKEY_SWTAPP          0x00000200		// 9:HOTK(SWTAPP) 			Switching App(F8자리)
#define	DRV_KEYBOARD_HOTKEY_VKBD            0x00000400		//10:HOTK(VKBD) 			Virtual keyboard(F9자리)
#define	DRV_KEYBOARD_HOTKEY_FIND            0x00000800		//11:HOTK(FIND) 			Search(F11자리)
#define	DRV_KEYBOARD_HOTKEY_V_UP            0x00001000		//12:HOTK(V_UP) 			Volume up(F7자리)
#define	DRV_KEYBOARD_HOTKEY_V_DOWN          0x00002000		//13:HOTK(V_DOWN) 			Volume down(F6자리)
#define	DRV_KEYBOARD_HOTKEY_ONOFF           0x00004000		//14:HOTK(ONOFF) 			On/Off: Fn + Del

//#define	DRV_KEYBOARD_HOTKEY_DEBUG           0x00008000		//15:HOTK(DEBUG) 			Fn + 4
//#define	DRV_KEYBOARD_HOTKEY_DEBUG2          0x00010000		//16:HOTK(DEBUG2) 			Fn + 58
//#define	DRV_KEYBOARD_HOTKEY_DEBUG3          0x00020000		//17:HOTK(DEBUG3) 			Fn + 6
#define DRV_KEYBOARD_HOTKEY_LANGUAGE_Z      0x00008000 //30
#define DRV_KEYBOARD_HOTKEY_LANGUAGE_X      0x00010000 //31
#define DRV_KEYBOARD_HOTKEY_LANGUAGE_C      0x00020000 //32
#define	DRV_KEYBOARD_HOTKEY_TEST_ENTER      0x00040000		//18:HOTK(TEST_ENTER) 		Fn + 7
#define	DRV_KEYBOARD_HOTKEY_BT1_NEW         0x00080000		//19:HOTK(BT1_NEW) 			Long press BT1
#define	DRV_KEYBOARD_HOTKEY_BT2_NEW         0x00100000		//20:HOTK(BT2_NEW) 			Long press BT2
#define	DRV_KEYBOARD_HOTKEY_BT3_NEW         0x00200000		//21:HOTK(BT3_NEW) 			Long press BT3
#define	DRV_KEYBOARD_HOTKEY_CERT_MODE     	0x00400000		//22:HOTK(CERT_MODE) 		Fn + 0  ...for certification testing ... repeat KBD+Mouse operatons
#define DRV_KEYBOARD_HOTKEY_CENTRAL_WINDOWS 0x00800000	//23:HOTK(CENTRAL_WINDOWS)	Fn + F5
#define DRV_KEYBOARD_HOTKEY_CENTRAL_ANDROID 0x01000000	//24:HOTK(CENTRAL_ANDRODI)	Fn + F6
#define DRV_KEYBOARD_HOTKEY_CENTRAL_MAC		  0x02000000	//25:HOTK(CENTRAL_MAC)		Fn + F7
#define DRV_KEYBOARD_HOTKEY_BACKWARD	    	0x04000000	//26:HOTK(BACK_WARD)		Fn + Backspace
#define DRV_KEYBOARD_HOTKEY_LOCKSCREEN      0x08000000 //27
#define DRV_KEYBOARD_HOTKEY_TOUCHLOCK       0x10000000 //28
#define DRV_KEYBOARD_HOTKEY_V_MUTE          0x20000000 //29
#define DRV_KEYBOARD_HOTKEY_CENTRAL_IOS     0x40000000 //29
#define DRV_KEYBOARD_HOTKEY_CUSTOM          0x80000000 //29
#define DRV_KEYBOARD_HOTKEY_TYPE_MAX 32

#define	APP_USBD_HID_KBD_HOUSE				    APP_USBD_HID_KBD_RSVD0		// HOME
#define	APP_USBD_HID_KBD_BT1          		APP_USBD_HID_KBD_RSVD1		// BT1
#define	APP_USBD_HID_KBD_BT2          		APP_USBD_HID_KBD_RSVD2		// BT2
#define	APP_USBD_HID_KBD_BT3          		APP_USBD_HID_KBD_RSVD3		// BT3
#define	APP_USBD_HID_KBD_DONGLE         	APP_USBD_HID_KBD_RSVD4		// Dongle
#define	APP_USBD_HID_KBD_BTRCHK         	APP_USBD_HID_KBD_RSVD5		// Battery check
#define	APP_USBD_HID_KBD_SWTAPP         	APP_USBD_HID_KBD_RSVD6		// SWitching App
#define	APP_USBD_HID_KBD_VKBD          		APP_USBD_HID_KBD_RSVD7		// Virtual Keyboard
#define	APP_USBD_HID_KBD_FIND           	APP_USBD_HID_KBD_RSVD8		// Find
#define	APP_USBD_HID_KBD_V_UP         		APP_USBD_HID_KBD_RSVD9		// Volume up
#define	APP_USBD_HID_KBD_V_DOWN         	APP_USBD_HID_KBD_RSVD10		// Volume down
// 주의: 이하는 mokibo-only keycode이다 !
#define	APP_USBD_HID_KBD_ONOFF          	APP_USBD_HID_KBD_RSVD11		// Mokibo On/Off
#define	APP_USBD_HID_KBD_RESET         		APP_USBD_HID_KBD_RSVD12		// HOTKEY 
#define	APP_USBD_HID_KBD_ERASEBOND    		APP_USBD_HID_KBD_RSVD13		// HOTKEY
#define APP_USBD_HID_KBD_LANGUAGE_Z       APP_USBD_HID_KBD_RSVD14		// HOTKEY
#define APP_USBD_HID_KBD_LANGUAGE_X       APP_USBD_HID_KBD_RSVD15		// HOTKEY
#define APP_USBD_HID_KBD_LANGUAGE_C       APP_USBD_HID_KBD_RSVD16		// HOTKEY
#define	APP_USBD_HID_KBD_TEST_ENTER		  	APP_USBD_HID_KBD_RSVD17		// HOTKEY
#define	APP_USBD_HID_KBD_CAPTURE			    APP_USBD_HID_KBD_RSVD18		// HOTKEY
#define	APP_USBD_HID_KBD_CERT_MODE			  APP_USBD_HID_KBD_RSVD19		// HOTKEY
#define	APP_USBD_HID_KBD_CENTRAL_WINDOWS	APP_USBD_HID_KBD_RSVD20		// Central Windows
#define	APP_USBD_HID_KBD_CENTRAL_ANDROID	APP_USBD_HID_KBD_RSVD21		// Central Android
#define	APP_USBD_HID_KBD_CENTRAL_MAC		  APP_USBD_HID_KBD_RSVD22		// Central MAC
#define APP_USBD_HID_KBD_BACKWARD			    APP_USBD_HID_KBD_RSVD23		// Backward
#define APP_USBD_HID_KBD_TOUCHLOCK        APP_USBD_HID_KBD_RSVD24
#define APP_USBD_HID_KBD_LOCKSCREEN       APP_USBD_HID_KBD_RSVD25
#define APP_USBD_HID_KBD_V_MUTE           APP_USBD_HID_KBD_RSVD26
#define	APP_USBD_HID_KBD_CENTRAL_IOS    	APP_USBD_HID_KBD_RSVD27		// Central Windows









/*$$$$$$\ $$\     $$\ $$$$$$$\  $$$$$$$$\ $$$$$$$\  $$$$$$$$\ $$$$$$$$\  $$$$$$\  
\__$$  __|\$$\   $$  |$$  __$$\ $$  _____|$$  __$$\ $$  _____|$$  _____|$$  __$$\ 
   $$ |    \$$\ $$  / $$ |  $$ |$$ |      $$ |  $$ |$$ |      $$ |      $$ /  \__|
   $$ |     \$$$$  /  $$$$$$$  |$$$$$\    $$ |  $$ |$$$$$\    $$$$$\    \$$$$$$\  
   $$ |      \$$  /   $$  ____/ $$  __|   $$ |  $$ |$$  __|   $$  __|    \____$$\ 
   $$ |       $$ |    $$ |      $$ |      $$ |  $$ |$$ |      $$ |      $$\   $$ |
   $$ |       $$ |    $$ |      $$$$$$$$\ $$$$$$$  |$$$$$$$$\ $$ |      \$$$$$$  |
   \__|       \__|    \__|      \________|\_______/ \________|\__|       \______/ 
//============================================================================//
//                                 GLOBAL TYPEDEFS                            //
//============================================================================*/

typedef void (*drv_keyboard_event_handler_t)(uint8_t *p_keys, uint8_t num, bool blocked);

// Copied from Nordic SDK14 App_usbd_hid_kbd.h  @brief HID keyboard codes.
typedef enum {
    APP_USBD_HID_KBD_A               = 4,  /**<KBD_A               code*/
    APP_USBD_HID_KBD_B               = 5,  /**<KBD_B               code*/
    APP_USBD_HID_KBD_C               = 6,  /**<KBD_C               code*/
    APP_USBD_HID_KBD_D               = 7,  /**<KBD_D               code*/
    APP_USBD_HID_KBD_E               = 8,  /**<KBD_E               code*/
    APP_USBD_HID_KBD_F               = 9,  /**<KBD_F               code*/
    APP_USBD_HID_KBD_G               = 10, /**<KBD_G               code*/
    APP_USBD_HID_KBD_H               = 11, /**<KBD_H               code*/
    APP_USBD_HID_KBD_I               = 12, /**<KBD_I               code*/
    APP_USBD_HID_KBD_J               = 13, /**<KBD_J               code*/
    APP_USBD_HID_KBD_K               = 14, /**<KBD_K               code*/
    APP_USBD_HID_KBD_L               = 15, /**<KBD_L               code*/
    APP_USBD_HID_KBD_M               = 16, /**<KBD_M               code*/
    APP_USBD_HID_KBD_N               = 17, /**<KBD_N               code*/
    APP_USBD_HID_KBD_O               = 18, /**<KBD_O               code*/
    APP_USBD_HID_KBD_P               = 19, /**<KBD_P               code*/
    APP_USBD_HID_KBD_Q               = 20, /**<KBD_Q               code*/
    APP_USBD_HID_KBD_R               = 21, /**<KBD_R               code*/
    APP_USBD_HID_KBD_S               = 22, /**<KBD_S               code*/
    APP_USBD_HID_KBD_T               = 23, /**<KBD_T               code*/
    APP_USBD_HID_KBD_U               = 24, /**<KBD_U               code*/
    APP_USBD_HID_KBD_V               = 25, /**<KBD_V               code*/
    APP_USBD_HID_KBD_W               = 26, /**<KBD_W               code*/
    APP_USBD_HID_KBD_X               = 27, /**<KBD_X               code*/
    APP_USBD_HID_KBD_Y               = 28, /**<KBD_Y               code*/
    APP_USBD_HID_KBD_Z               = 29, /**<KBD_Z               code*/
    APP_USBD_HID_KBD_1               = 30, /**<KBD_1               code*/
    APP_USBD_HID_KBD_2               = 31, /**<KBD_2               code*/
    APP_USBD_HID_KBD_3               = 32, /**<KBD_3               code*/
    APP_USBD_HID_KBD_4               = 33, /**<KBD_4               code*/
    APP_USBD_HID_KBD_5               = 34, /**<KBD_5               code*/
    APP_USBD_HID_KBD_6               = 35, /**<KBD_6               code*/
    APP_USBD_HID_KBD_7               = 36, /**<KBD_7               code*/
    APP_USBD_HID_KBD_8               = 37, /**<KBD_8               code*/
    APP_USBD_HID_KBD_9               = 38, /**<KBD_9               code*/
    APP_USBD_HID_KBD_0               = 39, /**<KBD_0               code*/
    APP_USBD_HID_KBD_ENTER           = 40, /**<KBD_ENTER           code*/
    APP_USBD_HID_KBD_ESC             = 41, /**<KBD_ESCAPE          code*/
    APP_USBD_HID_KBD_BS       		 	 = 42, /**<KBD_BACKSPACE       code*/
    APP_USBD_HID_KBD_TAB             = 43, /**<KBD_TAB             code*/
    APP_USBD_HID_KBD_SB              = 44, /**<KBD_SPACEBAR        code*/
    APP_USBD_HID_KBD_U_SCORE         = 45, /**<KBD_UNDERSCORE      code*/
    APP_USBD_HID_KBD_PLUS            = 46, /**<KBD_PLUS            code*/
    APP_USBD_HID_KBD_O_BRCKT         = 47, /**<KBD_OPEN_BRACKET    code*/
    APP_USBD_HID_KBD_C_BRCKT         = 48, /**<KBD_CLOSE_BRACKET   code*/
    APP_USBD_HID_KBD_BSLH            = 49, /**<KBD_BACKSLASH       code*/
    APP_USBD_HID_KBD_ASH             = 50, /**<KBD_ASH             code*/
    APP_USBD_HID_KBD_COLON           = 51, /**<KBD_COLON           code*/
    APP_USBD_HID_KBD_QUOTE           = 52, /**<KBD_QUOTE           code*/
    APP_USBD_HID_KBD_TILDE           = 53, /**<KBD_TILDE           code*/
    APP_USBD_HID_KBD_COMMA           = 54, /**<KBD_COMMA           code*/
    APP_USBD_HID_KBD_DOT             = 55, /**<KBD_DOT             code*/
    APP_USBD_HID_KBD_SLASH           = 56, /**<KBD_SLASH           code*/
    APP_USBD_HID_KBD_CAPS       	 	 = 57, /**<KBD_CAPS_LOCK       code*/
    APP_USBD_HID_KBD_F1              = 58, /**<KBD_F1              code*/
    APP_USBD_HID_KBD_F2              = 59, /**<KBD_F2              code*/
    APP_USBD_HID_KBD_F3              = 60, /**<KBD_F3              code*/
    APP_USBD_HID_KBD_F4              = 61, /**<KBD_F4              code*/
    APP_USBD_HID_KBD_F5              = 62, /**<KBD_F5              code*/
    APP_USBD_HID_KBD_F6              = 63, /**<KBD_F6              code*/
    APP_USBD_HID_KBD_F7              = 64, /**<KBD_F7              code*/
    APP_USBD_HID_KBD_F8              = 65, /**<KBD_F8              code*/
    APP_USBD_HID_KBD_F9              = 66, /**<KBD_F9              code*/
    APP_USBD_HID_KBD_F10             = 67, /**<KBD_F10             code*/
    APP_USBD_HID_KBD_F11             = 68, /**<KBD_F11             code*/
    APP_USBD_HID_KBD_F12             = 69, /**<KBD_F12             code*/
    APP_USBD_HID_KBD_PRNTSC		     	 = 70, /**<KBD_PRINTSCREEN     code*/
    APP_USBD_HID_KBD_SCROLL_LOCK     = 71, /**<KBD_SCROLL_LOCK     code*/
    APP_USBD_HID_KBD_PAUSE           = 72, /**<KBD_PAUSE           code*/
    APP_USBD_HID_KBD_INSERT          = 73, /**<KBD_INSERT          code*/
    APP_USBD_HID_KBD_HOME            = 74, /**<KBD_HOME            code*/
    APP_USBD_HID_KBD_PAGEUP          = 75, /**<KBD_PAGEUP          code*/
    APP_USBD_HID_KBD_DELETE          = 76, /**<KBD_DELETE          code*/
    APP_USBD_HID_KBD_END             = 77, /**<KBD_END             code*/
    APP_USBD_HID_KBD_PAGEDOWN        = 78, /**<KBD_PAGEDOWN        code*/
    APP_USBD_HID_KBD_RGHT          	 = 79, /**<KBD_RIGHT           code*/
    APP_USBD_HID_KBD_LFT           	 = 80, /**<KBD_LEFT            code*/
    APP_USBD_HID_KBD_DOWN            = 81, /**<KBD_DOWN            code*/
    APP_USBD_HID_KBD_UP              = 82, /**<KBD_UP              code*/
    APP_USBD_HID_KBD_KEYPAD_NUM_LOCK = 83, /**<KBD_KEYPAD_NUM_LOCK code*/
    APP_USBD_HID_KBD_KEYPAD_DIVIDE   = 84, /**<KBD_KEYPAD_DIVIDE   code*/
    APP_USBD_HID_KBD_KEYPAD_AT       = 85, /**<KBD_KEYPAD_AT       code*/
    APP_USBD_HID_KBD_KEYPAD_MULTIPLY = 85, /**<KBD_KEYPAD_MULTIPLY code*/
    APP_USBD_HID_KBD_KEYPAD_MINUS    = 86, /**<KBD_KEYPAD_MINUS    code*/
    APP_USBD_HID_KBD_KEYPAD_PLUS     = 87, /**<KBD_KEYPAD_PLUS     code*/
    APP_USBD_HID_KBD_KEYPAD_ENTER    = 88, /**<KBD_KEYPAD_ENTER    code*/
    APP_USBD_HID_KBD_KEYPAD_1        = 89, /**<KBD_KEYPAD_1        code*/
    APP_USBD_HID_KBD_KEYPAD_2        = 90, /**<KBD_KEYPAD_2        code*/
    APP_USBD_HID_KBD_KEYPAD_3        = 91, /**<KBD_KEYPAD_3        code*/
    APP_USBD_HID_KBD_KEYPAD_4        = 92, /**<KBD_KEYPAD_4        code*/
    APP_USBD_HID_KBD_KEYPAD_5        = 93, /**<KBD_KEYPAD_5        code*/
    APP_USBD_HID_KBD_KEYPAD_6        = 94, /**<KBD_KEYPAD_6        code*/
    APP_USBD_HID_KBD_KEYPAD_7        = 95, /**<KBD_KEYPAD_7        code*/
    APP_USBD_HID_KBD_KEYPAD_8        = 96, /**<KBD_KEYPAD_8        code*/
    APP_USBD_HID_KBD_KEYPAD_9        = 97, /**<KBD_KEYPAD_9        code*/
    APP_USBD_HID_KBD_KEYPAD_0        = 98, /**<KBD_KEYPAD_0        code*/
    APP_USBD_HID_KBD_KEYCODE_99   =99,  // [.]
    APP_USBD_HID_KBD_KEYCODE_100   =100, /*UK \ GER <(다른나라도 동일)*/
    APP_USBD_HID_KBD_KEYCODE_101   =101,
    APP_USBD_HID_KBD_KEYCODE_102   =102,
    APP_USBD_HID_KBD_KEYCODE_103   =103,
    APP_USBD_HID_KBD_KEYCODE_104   =104,
    APP_USBD_HID_KBD_KEYCODE_105   =105,
    APP_USBD_HID_KBD_KEYCODE_106   =106,
    APP_USBD_HID_KBD_KEYCODE_107   =107,
    APP_USBD_HID_KBD_KEYCODE_108   =108,
    APP_USBD_HID_KBD_KEYCODE_109  =109,
    APP_USBD_HID_KBD_INTERNATIONAL1 = 135, // 일본어 백슬래시 (시프트 왼쪽)
    APP_USBD_HID_KBD_INTERNATIONAL2 = 136, //일본어 히라가나 (스페이스 오른쪽오른쪽)
    APP_USBD_HID_KBD_INTERNATIONAL3 = 137, // 일본어 엔화 (백스페이스 왼쪽)
    APP_USBD_HID_KBD_INTERNATIONAL4 = 138, // 일본어 변환 (스페이스 오른쪽)
    APP_USBD_HID_KBD_INTERNATIONAL5 = 139, // 일본어 무변환 (스페이스 왼쪽)
    APP_USBD_HID_KBD_INTERNATIONAL6 = 140,
    APP_USBD_HID_KBD_INTERNATIONAL7 = 141,
    APP_USBD_HID_KBD_INTERNATIONAL8 = 142,
    APP_USBD_HID_KBD_INTERNATIONAL9 = 143,

		APP_USBD_HID_KBD_RSVD0       	 	 = 165, /**<KBD_RESERVED_0     code*/
		APP_USBD_HID_KBD_RSVD1       	 	 = 166, /**<KBD_RESERVED_1     code*/
		APP_USBD_HID_KBD_RSVD2       	 	 = 167, /**<KBD_RESERVED_2     code*/
		APP_USBD_HID_KBD_RSVD3       	 	 = 168, /**<KBD_RESERVED_3     code*/
		APP_USBD_HID_KBD_RSVD4       	 	 = 169, /**<KBD_RESERVED_4     code*/
		APP_USBD_HID_KBD_RSVD5       	 	 = 170, /**<KBD_RESERVED_5     code*/
		APP_USBD_HID_KBD_RSVD6       	 	 = 171, /**<KBD_RESERVED_6     code*/
		APP_USBD_HID_KBD_RSVD7       	 	 = 172, /**<KBD_RESERVED_7     code*/
		APP_USBD_HID_KBD_RSVD8       	 	 = 173, /**<KBD_RESERVED_8     code*/
		APP_USBD_HID_KBD_RSVD9       	 	 = 174, /**<KBD_RESERVED_9     code*/
		APP_USBD_HID_KBD_RSVD10       	 = 175, /**<KBD_RESERVED_10    code*/
		//
		APP_USBD_HID_KBD_RSVD11       	 = 176, /**<KBD_RESERVED_11  !! MOKIBO ONLY KEY code*/
		APP_USBD_HID_KBD_RSVD12       	 = 177, /**<KBD_RESERVED_12  !! MOKIBO ONLY KEY code*/
		APP_USBD_HID_KBD_RSVD13       	 = 178, /**<KBD_RESERVED_13  !! MOKIBO ONLY KEY code*/
		APP_USBD_HID_KBD_RSVD14       	 = 179, /**<KBD_RESERVED_14  !! MOKIBO ONLY KEY code*/
		APP_USBD_HID_KBD_RSVD15       	 = 180, /**<KBD_RESERVED_15  !! MOKIBO ONLY KEY code*/
		APP_USBD_HID_KBD_RSVD16       	 = 181, /**<KBD_RESERVED_16  !! MOKIBO ONLY KEY code*/
		APP_USBD_HID_KBD_RSVD17       	 = 182, /**<KBD_RESERVED_17  !! MOKIBO ONLY KEY code*/
		APP_USBD_HID_KBD_RSVD18       	 = 183, /**<KBD_RESERVED_18  !! MOKIBO ONLY KEY code*/
		APP_USBD_HID_KBD_RSVD19       	 = 184, /**<KBD_RESERVED_19  !! MOKIBO ONLY KEY code*/    
		APP_USBD_HID_KBD_RSVD20		      	= 185, /**<KBD_RESERVED_20  !! MOKIBO ONLY KEY code for central_windows*/
		APP_USBD_HID_KBD_RSVD21	      		= 186, /**<KBD_RESERVED_21  !! MOKIBO ONLY KEY code for central_android*/
		APP_USBD_HID_KBD_RSVD22		      	= 187, /**<KBD_RESERVED_22  !! MOKIBO ONLY KEY code for central_mac*/
		APP_USBD_HID_KBD_RSVD23		      	= 188, /**<KBD_RESERVED_23  !! MOKIBO ONLY KEY code for backward*/    
    APP_USBD_HID_KBD_RSVD24           = 189,
    APP_USBD_HID_KBD_RSVD25           = 190,    
    APP_USBD_HID_KBD_RSVD26          = 191,
    APP_USBD_HID_KBD_RSVD27          = 192,
    APP_USBD_HID_KBD_RSVD28          = 193,
    APP_USBD_HID_KBD_RSVD29          = 194,


		APP_USBD_HID_KBD_L_CTRL		 	 		 = 224,
		APP_USBD_HID_KBD_L_SHFT		 	 		 = 225,
		APP_USBD_HID_KBD_L_ALT		 	 		 = 226,
		APP_USBD_HID_KBD_L_UI		 	 			 = 227,
		APP_USBD_HID_KBD_R_CTRL		 	 		 = 228,
		APP_USBD_HID_KBD_R_SHFT	 	     	 = 229,
		APP_USBD_HID_KBD_R_ALT		 	 		 = 230,
		APP_USBD_HID_KBD_R_UI		     		 = 231,
} app_usbd_hid_kbd_codes_t;


// Copied from Nordic SDK14 App_usbd_hid_kbd.h * @brief HID keyboard modifier
typedef enum {
    APP_USBD_HID_KBD_MODIFIER_NONE          = 0x00,  /**< MODIFIER_NONE        bit*/
    APP_USBD_HID_KBD_MODIFIER_LEFT_CTRL     = 0x01,  /**< MODIFIER_LEFT_CTRL   bit*/
    APP_USBD_HID_KBD_MODIFIER_LEFT_SHIFT    = 0x02,  /**< MODIFIER_LEFT_SHIFT  bit*/
    APP_USBD_HID_KBD_MODIFIER_LEFT_ALT      = 0x04,  /**< MODIFIER_LEFT_ALT    bit*/
    APP_USBD_HID_KBD_MODIFIER_LEFT_UI       = 0x08,  /**< MODIFIER_LEFT_UI     bit*/
    APP_USBD_HID_KBD_MODIFIER_RIGHT_CTRL    = 0x10,  /**< MODIFIER_RIGHT_CTRL  bit*/
    APP_USBD_HID_KBD_MODIFIER_RIGHT_SHIFT   = 0x20,  /**< MODIFIER_RIGHT_SHIFT bit*/
    APP_USBD_HID_KBD_MODIFIER_RIGHT_ALT     = 0x40,  /**< MODIFIER_RIGHT_ALT   bit*/
    APP_USBD_HID_KBD_MODIFIER_RIGHT_UI      = 0x80,  /**< MODIFIER_RIGHT_UI    bit*/
} app_usbd_hid_kbd_modifier_t;

// Copied from Nordic SDK14 App_usbd_hid_kbd.h * @brief HID keyboard LEDs.
typedef enum {
    APP_USBD_HID_KBD_LED_NUM_LOCK     = 0x01,  /**< LED_NUM_LOCK    id*/
    APP_USBD_HID_KBD_LED_CAPS_LOCK    = 0x02,  /**< LED_CAPS_LOCK   id*/
    APP_USBD_HID_KBD_LED_SCROLL_LOCK  = 0x04,  /**< LED_SCROLL_LOCK id*/
    APP_USBD_HID_KBD_LED_COMPOSE      = 0x08,  /**< LED_COMPOSE     id*/
    APP_USBD_HID_KBD_LED_KANA         = 0x10,  /**< LED_KANA        id*/
} app_usbd_hid_kbd_led_t;








/*\    $$\  $$$$$$\  $$$$$$$\  $$$$$$\  $$$$$$\  $$$$$$$\  $$\       $$$$$$$$\ 
$$ |   $$ |$$  __$$\ $$  __$$\ \_$$  _|$$  __$$\ $$  __$$\ $$ |      $$  _____|
$$ |   $$ |$$ /  $$ |$$ |  $$ |  $$ |  $$ /  $$ |$$ |  $$ |$$ |      $$ |      
\$$\  $$  |$$$$$$$$ |$$$$$$$  |  $$ |  $$$$$$$$ |$$$$$$$\ |$$ |      $$$$$\    
 \$$\$$  / $$  __$$ |$$  __$$<   $$ |  $$  __$$ |$$  __$$\ $$ |      $$  __|   
  \$$$  /  $$ |  $$ |$$ |  $$ |  $$ |  $$ |  $$ |$$ |  $$ |$$ |      $$ |      
   \$  /   $$ |  $$ |$$ |  $$ |$$$$$$\ $$ |  $$ |$$$$$$$  |$$$$$$$$\ $$$$$$$$\ 
    \_/    \__|  \__|\__|  \__|\______|\__|  \__|\_______/ \________|\________|
/*============================================================================//
//                            GLOBAL  VARIABLES                               //
//============================================================================*/

/*----------------------------------------------------------------------------//
//                                GLOBAL VARIABLES                            //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                             GLOBAL DEBUG VARIABLES                         //
//----------------------------------------------------------------------------*/






 /*$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\  $$\   $$\ $$$$$$$$\  $$$$$$\  $$$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\ $$ |  $$ |$$  _____|$$  __$$\ $$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|$$ |  $$ |$$ |      $$ /  $$ |$$ |  $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |      $$$$$$$$ |$$$$$\    $$$$$$$$ |$$ |  $$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |      $$  __$$ |$$  __|   $$  __$$ |$$ |  $$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\ $$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |$$ |  $$ |$$$$$$$$\ $$ |  $$ |$$$$$$$  |
\__|       \______/ \__|  \__| \______/ \__|  \__|\________|\__|  \__|\_______/ 
/*============================================================================//
//                                 GLOBAL FUNCTIONS                           //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                            GLOBAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
extern ret_code_t drv_keyboard_init(drv_keyboard_event_handler_t handler);
extern bool drv_keyboard_new_packet(const uint8_t **p_packet, uint8_t *p_size);
extern void drv_keyboard_get_packet(uint8_t *p_packet, uint8_t *p_size);
extern uint32_t drv_keyboard_get_hotkey(void);
extern void drv_keyboard_prepare_sleep(void);
extern uint8_t drv_keyboard_get_volume(uint8_t input);
extern bool drv_keyboard_is_pressed(uint8_t input);
extern void drv_release_fn_del_colums(void);


/*----------------------------------------------------------------------------//
//                         GLOBAL DEBUG FUNCTION HEADS                        //
//----------------------------------------------------------------------------*/









/*\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ 
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\_*/



#endif // __TEMPLATE_H 

