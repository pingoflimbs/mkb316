/// \file drv_stm8.h
/// This file contains all required configurations for mokibo
/*==============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC.
* All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
==============================================================================*/



/*\   $$\ $$$$$$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$$$$$$\ $$\     $$\          
$$ |  $$ |\_$$  _|$$  __$$\\__$$  __|$$  __$$\ $$  __$$\\$$\   $$  |         
$$ |  $$ |  $$ |  $$ /  \__|  $$ |   $$ /  $$ |$$ |  $$ |\$$\ $$  /          
$$$$$$$$ |  $$ |  \$$$$$$\    $$ |   $$ |  $$ |$$$$$$$  | \$$$$  /           
$$  __$$ |  $$ |   \____$$\   $$ |   $$ |  $$ |$$  __$$<   \$$  /            
$$ |  $$ |  $$ |  $$\   $$ |  $$ |   $$ |  $$ |$$ |  $$ |   $$ |             
$$ |  $$ |$$$$$$\ \$$$$$$  |  $$ |    $$$$$$  |$$ |  $$ |   $$ |             
\__|  \__|\______| \______/   \__|    \______/ \__|  \__|   \__|             
//============================================================================//
//                            REVISION HISTORY                                //
//============================================================================//
//----------------------------------------------------------------------------//
20171104_Seo
	Created initial version based on JH.Seo work

20171109_MJ
	Updated code based on message3.txt

20171207_MJ
	Added "9 HOST MCU RESET" based on Message version: 3.2
	Added drv_stm8_set_reset_me()

20171212_MJ
	Added "10 LED_CENTER_BR" based on Message version: 3.3
	Added drv_stm8_set_rgb_led_brightness()

20171226_MJ
	Rewrote to simplfy
	Updated modules based on Message version: 3.4

20171228_MJ
	Added READ_DATA_MASK

20171228_MJ
	Updated modules based on Message version: 3.5

2018220_MJ
	Updated modules based on Message version: 3.7

20180410_MJ
	Updated modules based on Message version: 3.8

20170411_MJ
	Updated modules based on Message version: 3.9
//----------------------------------------------------------------------------*/



/*$$$$$\  $$\    $$\ $$$$$$$$\ $$$$$$$\  $$\    $$\ $$$$$$\ $$$$$$$$\ $$\      $$\ 
$$  __$$\ $$ |   $$ |$$  _____|$$  __$$\ $$ |   $$ |\_$$  _|$$  _____|$$ | $\  $$ |
$$ /  $$ |$$ |   $$ |$$ |      $$ |  $$ |$$ |   $$ |  $$ |  $$ |      $$ |$$$\ $$ |
$$ |  $$ |\$$\  $$  |$$$$$\    $$$$$$$  |\$$\  $$  |  $$ |  $$$$$\    $$ $$ $$\$$ |
$$ |  $$ | \$$\$$  / $$  __|   $$  __$$<  \$$\$$  /   $$ |  $$  __|   $$$$  _$$$$ |
$$ |  $$ |  \$$$  /  $$ |      $$ |  $$ |  \$$$  /    $$ |  $$ |      $$$  / \$$$ |
 $$$$$$  |   \$  /   $$$$$$$$\ $$ |  $$ |   \$  /   $$$$$$\ $$$$$$$$\ $$  /   \$$ |
 \______/     \_/    \________|\__|  \__|    \_/    \______|\________|\__/     \__|
//============================================================================//
//                        FUNCTION CALLSTACK OVERVIEW                         //
//============================================================================//
//----------------------------------------------------//PROTOCOLS


 ST8L I2C Register
 -----------------

 DATE: 2018.4.11
 version: 3.9

 I2C Slave Address	: 0x30
 I2C CLOCK			: 400K

------------------------------------------------------------------------------------------------
 Write command
------------------------------------------------------------------------------------------------
	R/W		CMD							DATA
(MSB)X  	XXXX						XXX(LSB)
------------------------------------------------------------------------------------------------
    1		0 No operation 
    1		1 FW_CMD					0(SLEEP), 1(RUN), 2(TEST), 3(RESET)
   	1 		2 LED_CUSTOM_RED_WRITE		XXX(Low or high 3bit) refer to "comment #4"
   	1		3 LED_CUSTOM_GREEN_WRITE	XXX(Low or high 3bit) refer to "comment #4"
   	1		4 LED_CUSTOM_BLUE_WRITE		XXX(Low or high 3bit) refer to "comment #4"
   	1		5 LED_CUSTOM_NIBBLE_WRITE	0(low 3bit), 1(high 3bit)
   	1		6 LED_COLOR_WRITE			0(Orange), 1(Red), 2(Green), 3(Blue), 4(White), 5(RSVD1), 6(RSVD2), 7(Custom)
    1		7 LED_STATUS_WRITE			0(Off), 1(On), 2(Slow blink), 3(Fast blink), 4(Dimming out), 5(Reset driver)
    1		8 HOST_MCU_RESET 			0~7: Delay time second
    1		9 LED_CENTER_BR 			0~7: Center RGB LED Brightness setting
    1		10 BATTERY_LEVEL_UPDATE 	Update with any value
    1		11~15 Reserved
------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------
 Read command
------------------------------------------------------------------------------------------------
	R/W		CMD							DATA
(MSB)X  	XXXX						XXX(LSB)
------------------------------------------------------------------------------------------------
 	0 		0 VERSION					XXXX-XXXX: refer to "comment #3"
 	0 		1 FW_STATUS					0(SLEEP), 1(RUN), 2(TEST) 3(RESET), 7(Initiailized)
   	0 		2 LED_CUSTOM_RED_READ		XXX(Low or high 3bit) refer to "comment #4"
   	0		3 LED_CUSTOM_GREEN_READ		XXX(Low or high 3bit) refer to "comment #4"
   	0		4 LED_CUSTOM_BLUE_READ		XXX(Low or high 3bit) refer to "comment #4"
   	0		5 LED_CUSTOM_NIBBLE_READ	0(low nibble), 1(high nibble) 
   	0		6 LED_COLOR_READ			0(Orange), 1(Red), 2(Green), 3(Blue), 4(White), 5(RSVD1), 6(RSVD2), 7(Custom)
    0		7 LED_STATUS_READ			0(Off), 1(On), 2(Slow blink), 3(Fast blink), 4(Dimming out), 5(Reset driver)
 	0 		8 TOUCH_CENTER_LEFT_STATUS	0(Off),	1(ON)
 	0 		9 TOUCH_CENTER_RIGHT_STATUS	0(Off),	1(ON)
 	0 		10 TOUCH_LEFT_STATUS		0(Off),	1(ON)
 	0 		11 TOUCH_RIGHT_STATUS		0(Off),	1(ON)
 	0 		12 BUTTON_LEFT_STATUS		0(Off),	1(ON)
 	0 		13 BUTTON_RIGHT_STATUS		0(Off),	1(ON)
 	0 		14 BATTERY_LEVEL_READ	    XXXX-XXXX refer to "comment #5"
 	0 		15 BATTERY_STATUS   		X2X1X0: X0(Charger status, 0: Charging, 1: Charging done, Toggle: Fault BAT)
 	  		                              		X1(ADC Status, 0: ADC Complete, 1: ADC in progress)
 	  		                              		X2(ADC Result, 0: low byte, 1: high byte)
------------------------------------------------------------------------------------------------
comment)
	1. Master에서 Read command 요청시 CMD만 정확히 보내면 됨/Slave에서는 DATA 무시함
	2. Slave 에서 Read command 응답시 응답 CMD 값과 DATA 채워서 보냄
	3. Slave 에서 version Read command 응답시에는 총 8bit DATA를 채워서 응답함
	4. Custom color는 HHHL-LLXX HHH(high 3bit) LLL(Low 3bit) XX(LSB0, LSB1 bit는0임)
	5. Slave 에서 BATTERY_LEVEL Read command 응답시에는 총 8bit DATA를 채워서 응답함
------------------------------------------------------------------------------------------------

//----------------------------------------------------//TIMERS
//----------------------------------------------------//VARS
//----------------------------------------------------//FUNCS
//----------------------------------------------------//INIT
//----------------------------------------------------//LOOP

//----------------------------------------------------------------------------*/






/*\      $$\  $$$$$$\  $$$$$$$\  $$\   $$\ $$\       $$$$$$$$\  $$$$$$\  
$$$\    $$$ |$$  __$$\ $$  __$$\ $$ |  $$ |$$ |      $$  _____|$$  __$$\ 
$$$$\  $$$$ |$$ /  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$ /  \__|
$$\$$\$$ $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$$$$\    \$$$$$$\  
$$ \$$$  $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$  __|    \____$$\ 
$$ |\$  /$$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$\   $$ |
$$ | \_/ $$ | $$$$$$  |$$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$$\ \$$$$$$  |
\__|     \__| \______/ \_______/  \______/ \________|\________| \______/ 
//============================================================================//
//                                 MODULES USED                               //
//============================================================================*/
#ifndef DRV_STM8_H
#define DRV_STM8_H






 /*$$$$\   $$$$$$\  $$\   $$\  $$$$$$\ $$$$$$$$\  $$$$$$\  $$\   $$\ $$$$$$$$\ 
$$  __$$\ $$  __$$\ $$$\  $$ |$$  __$$\\__$$  __|$$  __$$\ $$$\  $$ |\__$$  __|
$$ /  \__|$$ /  $$ |$$$$\ $$ |$$ /  \__|  $$ |   $$ /  $$ |$$$$\ $$ |   $$ |   
$$ |      $$ |  $$ |$$ $$\$$ |\$$$$$$\    $$ |   $$$$$$$$ |$$ $$\$$ |   $$ |   
$$ |      $$ |  $$ |$$ \$$$$ | \____$$\   $$ |   $$  __$$ |$$ \$$$$ |   $$ |   
$$ |  $$\ $$ |  $$ |$$ |\$$$ |$$\   $$ |  $$ |   $$ |  $$ |$$ |\$$$ |   $$ |   
\$$$$$$  | $$$$$$  |$$ | \$$ |\$$$$$$  |  $$ |   $$ |  $$ |$$ | \$$ |   $$ |   
 \______/  \______/ \__|  \__| \______/   \__|   \__|  \__|\__|  \__|   \__|   
 //============================================================================//
//                                 GLOBAL CONSTANTS                            //
//=============================================================================*/
#define	STM8_FW_STATUS_SLEEP  		0
#define	STM8_FW_STATUS_RUN			1
#define	STM8_FW_STATUS_TEST			2
#define	STM8_FW_STATUS_RESET		3
#define	STM8_FW_STATUS_MAX			4

#define	STM8_FW_STATUS_INITIAILIZED	7	// Valid in Read

#define	STM8_LED_OFF 				0
#define	STM8_LED_ON					1
#define	STM8_LED_SLOW_BLINK			2
#define	STM8_LED_FAST_BLINK			3
#define	STM8_LED_DIMMING_OUT		4
#define	STM8_LED_RESET				5
#define	STM8_LED_STATUS_MAX			6

#define	STM8_LED_COLOR_ORANGE 		0
#define	STM8_LED_COLOR_RED			1
#define	STM8_LED_COLOR_GREEN		2
#define	STM8_LED_COLOR_BLUE			3
#define	STM8_LED_COLOR_WHITE		4
#define	STM8_LED_COLOR_RSVD1		5
#define	STM8_LED_COLOR_RSVD2		6
#define	STM8_LED_COLOR_CUSTOM		7
#define	STM8_LED_COLOR_MAX			8

#define	STM8_TOUCH_OFF  			0
#define	STM8_TOUCH_ON				1
#define	STM8_TOUCH_MAX				2

#define	STM8_BUTTON_OFF				0
#define	STM8_BUTTON_ON				1
#define	STM8_BUTTON_MAX				2




/*$$$$$$\ $$\     $$\ $$$$$$$\  $$$$$$$$\ $$$$$$$\  $$$$$$$$\ $$$$$$$$\  $$$$$$\  
\__$$  __|\$$\   $$  |$$  __$$\ $$  _____|$$  __$$\ $$  _____|$$  _____|$$  __$$\ 
   $$ |    \$$\ $$  / $$ |  $$ |$$ |      $$ |  $$ |$$ |      $$ |      $$ /  \__|
   $$ |     \$$$$  /  $$$$$$$  |$$$$$\    $$ |  $$ |$$$$$\    $$$$$\    \$$$$$$\  
   $$ |      \$$  /   $$  ____/ $$  __|   $$ |  $$ |$$  __|   $$  __|    \____$$\ 
   $$ |       $$ |    $$ |      $$ |      $$ |  $$ |$$ |      $$ |      $$\   $$ |
   $$ |       $$ |    $$ |      $$$$$$$$\ $$$$$$$  |$$$$$$$$\ $$ |      \$$$$$$  |
   \__|       \__|    \__|      \________|\_______/ \________|\__|       \______/ 
//============================================================================//
//                                 GLOBAL TYPEDEFS                            //
//============================================================================*/
typedef uint8_t  STM8_FW_STATUS_t;
typedef uint8_t  STM8_TOUCH_STATUS_t; 
typedef uint8_t  STM8_BUTTON_STATUS_t; 

typedef enum 
{
	STM8_WRITE_NO_OPERATION 		= 0,
	STM8_WRITE_FW_CMD 				= 1,
	STM8_WRITE_LED_CUSTOM_RED		= 2,
	STM8_WRITE_LED_CUSTOM_GREEN		= 3,
	STM8_WRITE_LED_CUSTOM_BLUE		= 4,
	STM8_WRITE_LED_CUSTOM_NIBBLE	= 5,
	STM8_WRITE_LED_COLOR			= 6,
	STM8_WRITE_LED_STATUS			= 7,
	STM8_WRITE_RESET_ME				= 8,
	STM8_WRITE_LED_BR				= 9,
	STM8_WRITE_BATTERY_LEVEL_UPDATE	= 10,
} STM8_WRITE_CMD_t;

typedef enum 
{
	STM8_READ_VERSION				= 0,
	STM8_READ_FW_STATUS				= 1,
	STM8_READ_LED_CUSTOM_RED		= 2,
	STM8_READ_LED_CUSTOM_GREEN		= 3,
	STM8_READ_LED_CUSTOM_BLUE		= 4,
	STM8_READ_LED_CUSTOM_NIBBLE		= 5,
	STM8_READ_LED_COLOR				= 6,
	STM8_READ_LED_STATUS			= 7,
	STM8_READ_TOUCH_CENTER_LEFT		= 8,
	STM8_READ_TOUCH_CENTER_RIGHT	= 9,
	STM8_READ_TOUCH_LEFT			= 10, // MainPBA 6.51에서는 Left<-->Right이 바뀜
	STM8_READ_TOUCH_RIGHT			= 11, // MainPBA 6.51에서는 Left<-->Right이 바뀜
	STM8_READ_BUTTON_LEFT			= 12,
	STM8_READ_BUTTON_RIGHT			= 13,
	STM8_READ_BATTERY_LEVEL			= 14,
	STM8_READ_BATTERY_STATUS		= 15,
} STM8_READ_CMD_t;







/*\    $$\  $$$$$$\  $$$$$$$\  $$$$$$\  $$$$$$\  $$$$$$$\  $$\       $$$$$$$$\ 
$$ |   $$ |$$  __$$\ $$  __$$\ \_$$  _|$$  __$$\ $$  __$$\ $$ |      $$  _____|
$$ |   $$ |$$ /  $$ |$$ |  $$ |  $$ |  $$ /  $$ |$$ |  $$ |$$ |      $$ |      
\$$\  $$  |$$$$$$$$ |$$$$$$$  |  $$ |  $$$$$$$$ |$$$$$$$\ |$$ |      $$$$$\    
 \$$\$$  / $$  __$$ |$$  __$$<   $$ |  $$  __$$ |$$  __$$\ $$ |      $$  __|   
  \$$$  /  $$ |  $$ |$$ |  $$ |  $$ |  $$ |  $$ |$$ |  $$ |$$ |      $$ |      
   \$  /   $$ |  $$ |$$ |  $$ |$$$$$$\ $$ |  $$ |$$$$$$$  |$$$$$$$$\ $$$$$$$$\ 
    \_/    \__|  \__|\__|  \__|\______|\__|  \__|\_______/ \________|\________|
/*============================================================================//
//                            GLOBAL  VARIABLES                               //
//============================================================================*/

/*----------------------------------------------------------------------------//
//                                GLOBAL VARIABLES                            //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                             GLOBAL DEBUG VARIABLES                         //
//----------------------------------------------------------------------------*/




/*$$$$$$\ $$\   $$\ $$\   $$\  $$$$$$\  $$\   $$\ $$$$$$$$\  $$$$$$\  $$$$$$$\  
$$  _____|$$ |  $$ |$$$\  $$ |$$  __$$\ $$ |  $$ |$$  _____|$$  __$$\ $$  __$$\ 
$$ |      $$ |  $$ |$$$$\ $$ |$$ /  \__|$$ |  $$ |$$ |      $$ /  $$ |$$ |  $$ |
$$$$$\    $$ |  $$ |$$ $$\$$ |$$ |      $$$$$$$$ |$$$$$\    $$$$$$$$ |$$ |  $$ |
$$  __|   $$ |  $$ |$$ \$$$$ |$$ |      $$  __$$ |$$  __|   $$  __$$ |$$ |  $$ |
$$ |      $$ |  $$ |$$ |\$$$ |$$ |  $$\ $$ |  $$ |$$ |      $$ |  $$ |$$ |  $$ |
$$ |      \$$$$$$  |$$ | \$$ |\$$$$$$  |$$ |  $$ |$$$$$$$$\ $$ |  $$ |$$$$$$$  |
\__|       \______/ \__|  \__| \______/ \__|  \__|\________|\__|  \__|\_______/ 
/*============================================================================//
//                                 GLOBAL FUNCTIONS                           //
//============================================================================*/
/*----------------------------------------------------------------------------//
//                            GLOBAL FUNCTION HEADS                           //
//----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------//
//                         GLOBAL DEBUG FUNCTION HEADS                        //
//----------------------------------------------------------------------------*/
extern ret_code_t drv_stm8_init (void);
extern ret_code_t drv_stm8_write(STM8_WRITE_CMD_t cmd, uint8_t val);
extern ret_code_t drv_stm8_read(STM8_READ_CMD_t cmd, uint8_t *val);









/*\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ $$\ 
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |$$ |
\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\__|\_*/




#endif	/* END OF DRV_STM8_H */





