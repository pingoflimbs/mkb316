@echo OFF
@chcp 65001 1> NUL 2> NUL
::?? ???

::MKB316
cd ..
set DIR_HOME=%cd%

set YEAR=%date:~0,4%
set MONTH=%date:~5,2%
set DAY=%date:~8,2%
set DATE=%YEAR%%MONTH%%DAY%
set TEMPTIME=%time: =0%
set HOUR=%TEMPTIME:~0,2%
set MINUTE=%TEMPTIME:~3,2%
set SECONDS=%TEMPTIME:~6,2%
set TIME=%HOUR%%MINUTE%%SECONDS%
set DATETIME=%DATE%_%TIME%

set DIR_SRC_APP_PRJ=%DIR_HOME%\MKB316_NRF\examples\ble_peripheral\ble_app_hids_keyboard\pca10040\s132\arm5_no_packs
set DIR_KEIL5=C:\Keil_v5\UV4

cd %DIR_KEIL5%
@echo ON
UV4 -b %DIR_SRC_APP_PRJ%\ble_app_hids_keyboard_pca10040_s132.uvprojx -t"nrf52832_xxaa"

timeout /t 2
UV4 -f %DIR_SRC_APP_PRJ%\ble_app_hids_keyboard_pca10040_s132.uvprojx -t"nrf52832_xxaa"
@echo OFF




echo.
echo condition : %ERRORLEVEL%
echo.
if "%ERRORLEVEL%"=="0" (
  echo 1.OK
) else (
  echo 1.NG
  goto ERROROCCUR
)

echo.
echo.
echo 0	No Errors or Warnings
echo 1	Warnings Only
echo 2	Errors
echo 3	Fatal Errors
echo 11	Cannot open project file for writing
echo 12	Device with given name in not found in database
echo 13	Error writing project file
echo 15	Error reading import XML file
echo 20	Error converting project
echo.
echo.


echo      OOOOOOOOO          KKKKKKKKK    KKKKKKK
echo    OO:::::::::OO        K:::::::K    K:::::K
echo  OO:::::::::::::OO      K:::::::K    K:::::K
echo O:::::::OOO:::::::O     K:::::::K   K::::::K
echo O::::::O   O::::::O     KK::::::K  K:::::KKK
echo O:::::O     O:::::O       K:::::K K:::::K   
echo O:::::O     O:::::O       K::::::K:::::K    
echo O:::::O     O:::::O       K:::::::::::K     
echo O:::::O     O:::::O       K:::::::::::K     
echo O:::::O     O:::::O       K::::::K:::::K    
echo O:::::O     O:::::O       K:::::K K:::::K   
echo O::::::O   O::::::O     KK::::::K  K:::::KKK
echo O:::::::OOO:::::::O     K:::::::K   K::::::K
echo  OO:::::::::::::OO      K:::::::K    K:::::K
echo    OO:::::::::OO        K:::::::K    K:::::K
echo      OOOOOOOOO          KKKKKKKKK    KKKKKKK
@pause
                                            

:ERROROCCUR
echo NNNNNNNN        NNNNNNNN             GGGGGGGGGGGGG
echo N:::::::N       N::::::N          GGG::::::::::::G
echo N::::::::N      N::::::N        GG:::::::::::::::G
echo N:::::::::N     N::::::N       G:::::GGGGGGGG::::G
echo N::::::::::N    N::::::N      G:::::G       GGGGGG
echo N:::::::::::N   N::::::N     G:::::G              
echo N:::::::N::::N  N::::::N     G:::::G              
echo N::::::N N::::N N::::::N     G:::::G    GGGGGGGGGG
echo N::::::N  N::::N:::::::N     G:::::G    G::::::::G
echo N::::::N   N:::::::::::N     G:::::G    GGGGG::::G
echo N::::::N    N::::::::::N     G:::::G        G::::G
echo N::::::N     N:::::::::N      G:::::G       G::::G
echo N::::::N      N::::::::N       G:::::GGGGGGGG::::G
echo N::::::N       N:::::::N        GG:::::::::::::::G
echo N::::::N        N::::::N          GGG::::::GGG:::G
echo NNNNNNNN         NNNNNNN             GGGGGG   GGGG
@pause
